% This script will try to plot the potential for all dumps.

% Remembering that 0.5*dphi/dy = Ey and dphi/dx = Ex

% Fucking irritating zero lineout on axis! Need to interpolate over that I
% think.

datfol = '/media/kpoder/KRISMOOSE6/fbpic/Bond_STII_scan/ne040zf050';

Ex_s = load(fullfile(datfol, 'Ez_all'));  % Longitudinal field
%Ey_s = load(fullfile(datfol, 'Ey_all'));  % Transverse axis
%Bz_s = load(fullfile(datfol, 'Bx_all'));  % Other transverse axis
Er_s = load(fullfile(datfol, 'Er_all'));
Bt_s = load(fullfile(datfol, 'Bt_all'));
dx = mean(diff(Ex_s.grid_x(:,1)));
dy = mean(diff(Ex_s.grid_t(:,1)));
%%
phi_s = zeros(size(Ex_s.slicedata));

for i=10%1:size(Ex_s.slicedata,3)
    fprintf('Integrating file %i out of %i\n', i, size(Ex_s.slicedata,3));
    % This ought to help, I hope
    Ex_loc = Ex_s.slicedata(:,:,i)'; Er_loc = Er_s.slicedata(:,:,i)';
    Bt_loc = Bt_s.slicedata(:,:,i)';
    Ex_full = Ex_loc;
    Er_full = Er_loc;
    Bt_full = Bt_loc;
    
    %phi_ = intgrad2(-Ex_full, -0.5*Ey_full, dx, dy); phi_ = phi_ - phi_(end);
    phi_ = intgrad2(Ex_full, -Er_full+c*Bt_full, dx, dy); phi_ = phi_ - phi_(end);
    %phi_ = intgrad1(-Ex_full(indc,:), dx); phi_ = phi_ - phi_(end);
    
    phi_s(:,:,i) = phi_';%repmat(phi_', [1 375]);
    imagesc(Ex_s.grid_x(:,i), Ex_s.grid_t(:,i), phi_'*qe/(me*c^2));
end
grid_x = Ex_s.grid_x; grid_t = Ex_s.grid_t;
slicedata = phi_s;
%save(fullfile(datfol, 'phi_all'), 'slicedata', 'grid_x', 'grid_t');
fprintf('Done\n');
%%
figure(8);
%indc = round(0.5*(size(phi_s{2}.slicedata, 2)));
nav = 2;
for i=1:size(phi_s,3)
    phi_loc = phi_s(:,:,i)'*qe/(me*c^2);
    %phi_m(i) = max(max(phi_loc(indc-nav:indc+nav,:)));
    %imagesc(Ex_s.grid_x(:,i)-Ex_s.grid_x(1,i), Ex_s.grid_t(:,i), phi_loc);
    imagesc(Ex_s.grid_x(:,i), Ex_s.grid_t(:,i), phi_loc);
    colorbar;
    %set(gca, 'YLim', 1e-6*[-10 10], 'XLim', [2e-5 4.5e-5]);
    title(num2str(i)); pause(0.3);
    drawnow;
end