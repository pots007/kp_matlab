% highenergy_averaged_beams.m
%
% Try to make a plot where the high energy datasets are averaged...

savfol = fullfile(getDropboxPath, 'Writing', 'Gemini2015_2GeVpaper');
datfol = fullfile(getGDrive, 'Experiment', '2015_Gemini', 'EspecAnalysis',...
    '20150908r008', 'mat_manual');
shots = {[28:32]; [33:37]};

locs = GetFigureArray(2, 1, [0.03, 0.02, 0.21, 0.14], 0.15, 'down');
hfig = figure(12313); %hfig.Position = [1230, 500, 550, 200];
clf(hfig);
for ii=2:2
    % Need to interpolate onto the same grid...
    enax_com = linspace(300, 3200, 2500)*1e-3;
    Es = zeros(length(shots{ii}), length(enax_com));
    Qs = zeros(length(shots{ii}), 1);
    for k=1:length(shots{ii})
        fname = sprintf('20150908r008s%03i_Espec1.mat', shots{ii}(k));
        fdat = load(fullfile(datfol, fname));
        enax = fdat.shotdata.tracking_btrac(fdat.shotdata.alongscreen)*0.001;
        dE = gradient(enax);
        dQ_dE_E = smooth(1e-0*fdat.shotdata.dQ_dx'.*enax./dE);
        Es(k, :) = interp1(enax, dQ_dE_E, enax_com, 'linear', 0);
        Qs(k) = fdat.shotdata.totQ;
    end
    
    fprintf('Total charge: %2.2f +- %2.2f\n', mean(Qs), std(Qs));
    meanE = mean(Es, 1);
    stdE = std(Es, [], 1);%/sqrt(length(shots{ii}));
    ax = makeNiceAxes(hfig, locs(:,ii));
    
    shadedErrorBar(enax_com, meanE, stdE, [], 1)
    set(ax, 'YScale', 'lin', 'YLim', [1e-2, 125], 'XLim', [0.3, 3.1]);
    ylabel(ax, {'Charge per GeV per' '0.1% energy spread (pC)'});
    xlabel(ax, 'Energy (GeV)');
end
%% Plot low energy ones as well, replace the stuff in ax2
matroot = '/home/kpoder/GDrive/Experiment/2015_Gemini/EspecAnalysis/20150902r006/mat_manual';
flist = dir(fullfile(matroot, '*Espec1.mat'));

Es = []; Qs = [];
ax = makeNiceAxes(hfig, locs(:,1));
for k=50:55%1:length(flist)
    fdat = load(fullfile(matroot, flist(k).name));
    enax = fdat.shotdata.tracking_btrac(fdat.shotdata.alongscreen)*0.001;
    dE = gradient(enax);
    dQ_dE_E = smooth(1e-0*fdat.shotdata.dQ_dx'.*enax./dE);
    Es(end+1, :) = interp1(enax, dQ_dE_E, enax_com, 'linear', 0);
    Qs(end+1) = fdat.shotdata.totQ;
    %plot(ax, enax, dQ_dE_E, '--r')
end
fprintf('Total charge: %2.2f +- %2.2f\n', mean(Qs), std(Qs));
meanE = mean(Es, 1);
stdE = std(Es, [], 1);%/sqrt(length(shots{ii}));

shadedErrorBar(enax_com, meanE, stdE, [], 1)
set(ax, 'YScale', 'lin', 'YLim', [1e-2, 125], 'XLim', [0.3, 3.2]);
ylabel(ax, {'Charge per GeV per' '0.1% energy spread (pC)'});
xlabel(ax, 'Energy (GeV)');


setAllFonts(hfig, 9);