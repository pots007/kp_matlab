% focalSpots2015Gem.m

dfol = fullfile(getGDrive, 'Experiment', '2015_Gemini', 'FocusAnalysis');
fdat = load(fullfile(dfol, '20150908_LP_spot.mat'));
fdat = fdat.focdata;

%rowHeaders = {'1/e^2 major radius', '1/e^2 minor radius',...
%                         '1/e^2 radius', '1/e^2 encircled energy',...
%                         '1/e major radius', '1/e minor radius',...
%                         '1/e radius', '1/e encircled energy',...
%                         'FWHM major radius', 'FWHM minor radius',...
%                         'FWHM radius', 'FWHM encircled energy',...
%                         'x centroid location', 'y centroid location',...
%                         'Intensity (W/cm^2)'};%#ok<NASGU>

hist(fdat(5,:))
mean(fdat(5,:))

%% What would the difference in spot size distributions be??
% Phase front rms varying randomly:
g = linspace(-.15, .15, 512);
[X, Y] = meshgrid(g, g);
radius = sqrt(X.^2 + Y.^2) / 0.075;
theta = atan2(Y,X);
mask = radius < 1;
phase_rms = 1;
phase_offset = 2;
%beam_phase = randn(size(R))*phase_rms .* mask;

spot_sizes = zeros(100, 2);
tic
for k=1:size(spot_sizes, 1)
    fprintf('It %i/%i\n', k, size(spot_sizes, 1));
    coefs = randn(14, 1)*phase_rms + phase_offset;
    coefs(2:end) = 0;
    fprintf('\t%2.3f\n', coefs(1));
    
    
    % Zernike polynomials varying randomly, including defocus!
    aberration = zeros(length(g), length(g), 14);
    aberration(:,:,1) = 1/sqrt(3)*2*radius.^2 - ones(length(g)); %defocus
    aberration(:,:,2) = 1/sqrt(6)*radius.^2.*cos(2.*theta); %0 degree astig
    aberration(:,:,3) = 1/sqrt(6)*radius.^2.*sin(2.*theta); %45 astig
    aberration(:,:,4) = 1/sqrt(8)*(3.*radius.^2-2).*radius.*cos(theta); %0 coma
    aberration(:,:,5) = 1/sqrt(8)*(3.*radius.^2-2).*radius.*sin(theta); %90 coma
    aberration(:,:,6) = 1/sqrt(5)*6*radius.^4 - 6*radius.^1 + 1; %spherical
    %aberration(:,:,7) = radius.^3.*cos(3.*theta); %30 degree trefoil
    %aberration(:,:,8) = radius.^3.*sin(3.*theta); %0 degree trefoil
    aberration(:,:,9) = 1/sqrt(10)*(4*radius.^2-3).*radius.^2.*cos(2*theta); %0 degree secondary astigmatism
    aberration(:,:,10) = 1/sqrt(10)*(4*radius.^2-3).*radius.^2.*sin(2*theta); %45 degree secondary astigmatism
    %aberration(:,:,11) = radius.^4.*cos(4*theta); %0 degree tetrafoil
    %aberration(:,:,12) = radius.^4.*sin(4*theta); %22.5 degree tetrafoil
    aberration(:,:,13) = 1/sqrt(12)*(10*radius.^5-12*radius.^3+3*radius).*cos(theta); %Secondary x coma
    aberration(:,:,14) = 1/sqrt(12)*(10*radius.^5-12*radius.^3+3*radius).*sin(theta); %Secondary y coma
    
    for i=1:size(aberration, 3)
        aberration(:,:,i) = aberration(:, :, i) * coefs(i);
    end
    beam_phase = mask .* sum(aberration, 3);
    
    Npnts = 4096/2; fr = 80; frr = (0.5*Npnts-fr) : (0.5*Npnts+fr);
    spot_E = fftshift(fft2(mask.*exp(1j*beam_phase), Npnts, Npnts));
    spot_I = abs(spot_E(frr, frr)).^2;
    
    spot_sizes(k,:) = [widthfwhm(sum(spot_I,1)), widthfwhm(sum(spot_I, 2))];
    imagesc(spot_I); drawnow;
end
toc
hist(spot_sizes(:,1), 20)
