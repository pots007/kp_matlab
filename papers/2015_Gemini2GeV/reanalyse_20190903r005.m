% This script will analyse 20150903r005, one that I looked through
% manually and where the backtracking angle was calculated

date_ = '20150903';
run_ = '005';

saveplot = 1;

datfol = fullfile(getGDrive, 'Experiment', '2015_Gemini', 'EspecAnalysis', [date_ 'r' run_], 'mat_manual');
savfol = fullfile(getDropboxPath, 'Writing', 'Gemini2015_2GeVpaper', 'data');

% And redo what other scripts do in the analysis part
[nShots, runst] = getGem2015NumberofScanShots(date_, run_);
ttt = struct;
for i=1:nShots
    fprintf('Working on file %i out of %i\n', i, nShots);
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(str2double(date_), str2double(run_), i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).cellL = runst.cellL;
    ttt(i).exitenergy = getGemini2015ExitEnergy(str2double(date_), str2double(run_), i);
end

%% Now for the fun part. Find cutoff energies, with backtracked angles.
% First, interpolate all screen images onto a fixed grid. BUT the grid is
% in screen distance now.
screenBins1 = 0:0.1:300;
screenBins2 = 0:0.1:197;
scrmat1 = zeros(nShots, length(screenBins1));
scrmat2 = zeros(nShots, length(screenBins2));
btrack_thetas = zeros(nShots,1);
Qtots = nan(size(btrack_thetas)); Qhighs = nan(size(btrack_thetas));
for i=1:length(ttt)
    fnam1 = sprintf('%sr%ss%03i_Espec1.mat', date_, run_, i);
    try
        fdat = load(fullfile(datfol, fnam1));
        scrmat1(i,:) = smooth(interp1(fdat.shotdata.alongscreen, sum(fdat.shotdata.image,1), screenBins1, 'pchip', 0));
        Qtots(i) = fdat.shotdata.totQ;
        btrack_thetas(i) = fdat.shotdata.track_angle;
    catch err
        fprintf('No file for %i: %s\n', i, err.message);
    end
    fnam2 = sprintf('%sr%ss%03i_Espec2LE.mat', date_, run_, i);
    try
        fdat = load(fullfile(datfol, fnam2));
        scrmat2(i,:) = smooth(interp1(fdat.shotdata.alongscreen, sum(fdat.shotdata.image,1), screenBins2, 'pchip', 0));
        Qhighs(i) = fdat.shotdata.totQ;
        btrack_thetas(i) = fdat.shotdata.track_angle;
    catch err
        fprintf('No file for %i: %s\n', i, err.message);
    end
end
scrmat1 = fliplr(scrmat1); scrmat2 = fliplr(scrmat2);
%%
maxind = ones(nShots,2);
BG_reg1 = 30; BG_fact1 = 5;
BG_reg2 = 50; BG_fact2 = 5;
for i=1:nShots
    % Do it on a line by line basis here, as I changed the box size a lot!
    % First, Espec1s
    linespec = scrmat1(i,:);
    linespec = linespec(linespec~=0);
    if ~isempty(linespec)
        bgreg = (length(linespec)-BG_reg1):length(linespec);
        BGmean_(i) = mean(linespec(bgreg));
        BGstd_(i) = std(linespec(bgreg));
        BGfact_(i) = BGmean_(i) + BG_fact1*BGstd_(i);
        temp = find(scrmat1(i,:)>(BGfact_(i)), 1, 'last');
        if ~isempty(temp); maxind(i,1) = temp; end
    end
    linespec = scrmat2(i,:);
    linespec = linespec(linespec~=0);
    if ~isempty(linespec) 
        bgreg = (length(linespec)-BG_reg2):length(linespec);
        BGmean_(i) = mean(linespec(bgreg));
        BGstd_(i) = std(linespec(bgreg));
        BGfact_(i) = BGmean_(i) + BG_fact2*BGstd_(i);
        temp = find(scrmat2(i,:)>(BGfact_(i)), 1, 'last');
        if ~isempty(temp); maxind(i,2) = temp; end
    end
end
% Apply some corrections:
maxind([24, 25],2) = [1970, 1970];
% Check it out
figure(103); clf(103);
set(gca, 'NextPlot', 'add');
imagesc(scrmat1);
plot(maxind(:,1), 1:nShots, 'ok');
axis tight;
figure(104); clf(104);
set(gca, 'NextPlot', 'add');
imagesc(scrmat2);
plot(maxind(:,2), 1:nShots, 'ok');
axis tight;

%set(gca, 'CLim', [0 1e7]);
% And now find the energies for the locations found.
% Again assume this is the +1 mrad divergence part!
peakEns = nan(nShots, 2);
% And for shots, which don't have a btrac angle, assume average of all
% other ones!
btrack_thetas(btrack_thetas==0) = mean(btrack_thetas(btrack_thetas~=0));
for i=1:nShots
    %if i==2; continue; end;
    if maxind(i,1)~=1
        peakEns(i,1) = getGem2015ScreenEnergy(300-screenBins1(maxind(i,1)), 1, 1, btrack_thetas(i));
    end
    if maxind(i,2)~=1
        peakEns(i,2) = getGem2015ScreenEnergy(197-screenBins2(maxind(i,2)), 3, 1, btrack_thetas(i));
    end
end
figure(105); clf(105);
plot(peakEns, '-x');

clear peakEns_secondary;
%% Calculate total beam energy and recalculate high beam charges (>1GeV)

%Having found correct energies, this is an interesting exercise!
beamEns = zeros(nShots,2);

for i=1:nShots
    fprintf('Working on file %i out of %i\n', i, nShots);
    fnam1 = sprintf('%sr%ss%03i_Espec1.mat', date_, run_, i);
    try
        fdat = load(fullfile(datfol, fnam1));
        for k=1:length(fdat.shotdata.alongscreen)
            enAx(k) = getGem2015ScreenEnergy(fdat.shotdata.alongscreen(k), 1, 2, btrack_thetas(i));
        end
        ttemp = 1e-3*enAx.*fdat.shotdata.dQ_dx; % GeV*pC gives mJ answer
        beamEns(i,1) = sum(ttemp(~isnan(ttemp)));
        % And the charge beyond 1 GeV
        Qhighs(i) = sum(fdat.shotdata.dQ_dx(enAx>1000));
    catch err
        fprintf('No file for %i: %s\n', i, err.message);
    end
    fnam2 = sprintf('%sr%ss%03i_Espec2LE.mat', date_, run_, i);
    try
        fdat = load(fullfile(datfol, fnam2));
        enAx = [];
        for k=1:length(fdat.shotdata.alongscreen)
            enAx(k) = getGem2015ScreenEnergy(fdat.shotdata.alongscreen(k), 3, 2, btrack_thetas(i));
        end
        ttemp = 1e-3*enAx.*fdat.shotdata.dQ_dx; % GeV*pC gives mJ answer
        beamEns(i,2) = sum(ttemp(~isnan(ttemp)));
    catch err
        fprintf('No file for %i: %s\n', i, err.message);
    end
end

%% Now, all the preceding has been fun and sorting data out.
% Now let's make some calculations and analysis, roughly as per usual
%the cut:
peakEns(23,:) = nan;
pps = runst.pressure/31;
pp_unique = unique(pps);
%laserE = cell2mat({ttt.laserE})';
%pp_vs_ens1 = cell(size(pp_unique));
%pp_vs_Qtots = cell(size(pp_unique));
%pp_vs_Qhighs = cell(size(pp_unique));
clear pp_st; pp_st(length(pp_unique)) = struct;

laserE = cell2mat({ttt.laserE}');
% Trying out removing outside std values...
lm_las = laserE'>1;
%laserE_ = laserE(lm_las);
laserE_ = laserE(lm_las);
lm_las = abs(laserE' - mean(laserE_)) < 2*std(laserE_);

for i=1:length(pp_unique)
%    logm = pps==pp_unique(i) & Qtots>3 & ~isnan(peakEns(:,1));
%    pp_vs_ens1{i} = peakEns(logm,1);
%    pp_vs_Qtots{i} = Qtots(logm);
%    pp_vs_Qhighs{i} = Qhighs(logm);
    % New analysis structure:
    ll = pps==pp_unique(i);
    temppp = ll'.*(1:length(pps));
    pp_st(i).shots = temppp(temppp~=0);
    pp_st(i).pres = pp_unique(i);
    pp_st(i).peakEns1 = peakEns(ll,1);
    pp_st(i).peakEns1_logm = ~isnan(pp_st(i).peakEns1); % Tells us which shots are not nans
    pp_st(i).peakEns2 = peakEns(ll,2);
    pp_st(i).peakEns2_logm = ~isnan(pp_st(i).peakEns2); % Tells us which shots are not nans
    pp_st(i).Qtots = Qtots(ll);
    pp_st(i).Qhighs = Qhighs(ll);
    pp_st(i).laserE = laserE(ll);
    pp_st(i).laserE_logm = lm_las(pp_st(i).shots)'; %Tells us which shots of the pressure are good for laser  
end

%% Now we calculate the average values and errors at scan points:
%nanav = @(x) mean(x, 1, 'omitnan'); % Shortcut to not have to use the tedious logm-s
% Now go through each entry and check for all the associated log matrices:
pp_vs_ens = cell(length(pp_st), 2);
pp_vs_Q = cell(size(pp_vs_ens)); logm_tot = [];
logm_tot2 = false(nShots,1);
for i=1:length(pp_st)
    logm1 = pp_st(i).peakEns1_logm & pp_st(i).laserE_logm; % & pp_st(i).nes_logm;
    pp_vs_ens{i,1} = pp_st(i).peakEns1(logm1);
    pp_vs_Q{i,1} = pp_st(i).Qtots(logm1);
    logm2 = pp_st(i).peakEns2_logm & pp_st(i).laserE_logm; % & pp_st(i).nes_logm;
    pp_vs_ens{i,2} = pp_st(i).peakEns2(logm2);
    pp_vs_Q{i,2} = pp_st(i).Qhighs(logm2);
    logm_tot2(pp_st(i).shots) = logm1;
end
pp_vs_ens_mean  = cellfun(@mean, pp_vs_ens); pp_vs_ens_std  = cellfun(@(x) std(x)/sqrt(length(x)), pp_vs_ens);
pp_vs_Q_mean  = cellfun(@mean, pp_vs_Q); pp_vs_Q_std  = cellfun(@(x) std(x)/sqrt(length(x)), pp_vs_Q);
% Need to rearrange a few things due to the 0 length at the start


%% Create a struct for easier analysis later on
clear D0903r005;
D0903r005.EspecData1 = scrmat1;
D0903r005.EspecData2 = scrmat2;
D0903r005.peakEns1 = peakEns(:,1);
D0903r005.peakEns2 = peakEns(:,2);
D0903r005.nes = pps;
D0903r005.cellL = runst.cellL;
D0903r005.pp_unique = pp_unique;
D0903r005.Qhighs = Qhighs;
D0903r005.Qtots = Qtots;
D0903r005.pp_vs_ens1 = pp_vs_ens(:,1);
D0903r005.pp_vs_ens2 = pp_vs_ens(:,2);
D0903r005.pp_vs_Qtots = pp_vs_Q(:,1);
D0903r005.pp_vs_Qhighs = pp_vs_Q(:,2);
D0903r005.laserE = laserE;
D0903r005.logm_total = logm_tot2;%cell2mat(fliplr(logm_tot)');
D0903r005.beamEns = beamEns;
save(fullfile(savfol, 'run20150903r005_analysis'), 'D0903r005');