% plasmaLensFocusOptimisation.m
%
% Calculate the focal plane for a particular energy as a function of
% current, then figure out what the bandwidth of energy spread and
% divergence is

% Scan over some currents and some energies

Is = logspace(log10(400), log10(1000), 500); % Log space
Is = 200:1000;
% Plasma lens properties
lens.I = 800;
lens.start = 0.05;
lens.L = 0.1;
lens.R = 0.001;
% Electron beam:
gammas = [127, 524];
beam.gamma = 127;
beam.emittance = 1e-6/beam.gamma; % Need to use geometrical emittance here!
beam.sigma0 = 1e-6;

focData = zeros(length(Is), 4, 2);
tic
for gam=1:length(gammas)
    for I=1:length(Is)
        fprintf('Current %3i (%i)\n', I, length(Is));
        
        %fprintf('Current,energy: %i(%i), %i(%i)\n', I, length(Is), gamma, length(gammas));
        lens.I = Is(I);
        beam.gamma = gammas(gam);
        beam.emittance = 1e-6/beam.gamma; % Need to use geometrical emittance here!
        
        [beta, s, outStr] = betaFunctionSolve(lens, beam);
        if  outStr.spotSize2<100e-6
            focData(I,1,gam) = outStr.z_f;
            focData(I,2,gam) = outStr.spotSize2;
            focData(I,3,gam) = outStr.betaStar;
            focData(I,4,gam) = outStr.div;
        end
    end
end
toc

%% Replicate what Theresa did - do I understand things?

hfig = figure(32512); clf(hfig);
ax = makeNiceAxes(hfig);
BW_div = (0.5*beam.gamma*focData(:,4,1)).^2;
eBW_low = focussedEnergyFit(focData(:,1,1)-focData(:,3,1), Is);
eBW_high = focussedEnergyFit(focData(:,1,1)+focData(:,3,1), Is);
BW_el = 4*(eBW_high-eBW_low)./beam.gamma;
plot(ax, focData(:,1,1), BW_div, '.g', focData(:,1,1), BW_el, '.c',...
    focData(:,1,1), BW_div+BW_el, '.r');
set(ax, 'YLim', [0 .1])

% Seeeeeeems legit :)