% calcPlasmaLensFocus.m
%
% New way of calculating focus plane: using the matrix equations Theresa
% derived....

function f = calcPlasmaLensFocus(lens, beam)

Constants;
mu0 = 4*pi*1e-7;
dB_dr = mu0*lens.I/(2*pi*lens.R^2);
k = qe*dB_dr/(me*c*beam.gamma);
L = lens.L;
z0 = lens.start;

C = cos(L*sqrt(k));
S = sin(L*sqrt(k));

f = (sqrt(k)*z0*C + S)/(-sqrt(k)*C + k*z0*S);
f = f+lens.L+lens.start;