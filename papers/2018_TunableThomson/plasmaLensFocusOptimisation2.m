% Now try to do it the other way around: take a focal plane, track first
% and then optimise for optimum current.

%% Case 1: Know z_f and gamma, want I
% This is used to calculate how to focus an energy to a particular plane -
% this is the first step of the optimisation



%% Case 2: Know z_f and I, want gamma
% This is used to find the bandwidth: we know s_f'= s_f +- beta*, want to
% know the gamma now
