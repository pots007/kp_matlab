% Play around with the beta function and focussing!

% Plasma lens properties
lens.I = 600; 
lens.start = 0.05;
lens.L = 0.1;
lens.R = 0.001;
% Electron beam:
beam.gamma = 271.8/0.511; 
beam.emittance = 1e-6/beam.gamma; % Need to use geometrical emittance here!
beam.sigma0 = 1e-6;
beam.div = 1/beam.gamma;

[beta, s, outStr] = betaFunctionSolve(lens, beam, 1);

% Calculate the focal spot size
fprintf('Spot size=%2.2f um, betaFcn=%2.2f mm\n', ...
    1e6*outStr.spotSize2,outStr.betaStar*1e3);
fprintf('Divergence is %2.2f mrad\n', 1e3*outStr.div)

%plot(s, sqrt(beta(:,1)*emitt))
hfig = figure(2314235); clf(hfig); %hfig.Position = [500 200 900 350];
ax = makeNiceAxes(hfig);
area([lens.start, lens.start+lens.L], [0,1; 0,1], 'FaceColor', 0.9*[1 1 1], 'EdgeColor', 'none')
hh = plot(ax, s, 1e3*sqrt(beta(:,1)*beam.emittance), outStr.ss, 1e3*outStr.u(:,1), '--r');
set(gca, 'YLim', [0 1.05], 'XLim', [0 1.1], 'Layer', 'top');
set(hh, 'LineWidth', 2);
legend(hh, {'Spot size', 'Single particle trajectory'}, 'Location', 'ne', 'Edgecolor', 'none')
xlabel('s / m');
ylabel('\sigma / mm');
setAllFonts(gcf, 14);
%export_fig('/home/kpoder/DESYcloud/Writing/2018_TunableThomson/SpotSize', '-pdf', '-nocrop', hfig);
