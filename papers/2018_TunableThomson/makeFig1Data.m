% makeFig1Data.m
%
% Make the beam size trajectories for Fig1

% Plasma lens properties
lens.I = 500; 
lens.start = 0.05;
lens.L = 0.1;
lens.R = 0.001;
% Electron beam:
beam.gamma = 68.5; 
beam.emittance = 1e-6/beam.gamma; % Need to use geometrical emittance here!
beam.sigma0 = 1e-6;
beam.div = 1/beam.gamma;

gammas = 360:400;
% Save stuff into h5!
h5fname = '/home/kpoder/Writing/TunableThomson/data/fig1Data.h5';
if exist(h5fname, 'file'); delete(h5fname); end;
for g = 1:length(gammas)
    fprintf('Tracking energy %i out of %i\n', g, length(gammas));
    beam.gamma = gammas(g); 
    beam.emittance = 1e-6/beam.gamma; % Need to use geometrical emittance here!
    beam.sigma0 = 1e-6;

    [beta, s, outStr] = betaFunctionSolve(lens, beam, 0);
    h5create(h5fname, sprintf('/gamma%04.2f', beam.gamma), [length(s), 2], ...
        'Datatype', 'double');
    h5write(h5fname, sprintf('/gamma%04.2f', beam.gamma), [s beta(:,1)]);
end

