% findPlasmaLensFocus.m
%
% Function that calculates the energy focussed at a particular plane in
% space. The lens is represented by a lens structure. Note that the
% function calculates a value for k and extract gamma from there, so an
% array can be passed as lens.I.
%
% findPlasmaLensFocus(zf, lens)

function gamma = findPlasmaLensFocus(zf, lens)
Constants;
%lens.I = 1000; 
%lens.start = 0.05;
%lens.L = 0.04;
%lens.R = 0.001;

L = sym(lens.L);
f = sym(zf);
z = sym(lens.start);
syms k phi S C
phi = L*sqrt(k);
C =  1 - phi^2/2;% + phi^4/24;
S =  phi - phi^3/6;% + phi^5/120;
sols = solve( (sqrt(k)*z*C + S)/(-sqrt(k)*C + k*z*S) - f, k);

% It seems with using actual cos and sin values, it always returns a nice
% numeric approximation - this ought to be good enough for us!
ks = double(subs(sols));
lm = ~imag(ks);
if sum(lm)==0
    disp('No real valued solutions!')
end
k = ks(lm)

cc = qe*4*pi*1e-7/(2*pi*me*c);
gamma = cc*lens.I/(k*lens.R^2);