% betaFunctionSolve.m
%
% Function to solve for the beta function for a given plasma lens current.

function [beta,s, outStr] = betaFunctionSolve(lens, beam, singlePart)

if nargin<3; singlePart=0; end
Constants;
mu0 = 4*pi*1e-7;
dB_dr = mu0*lens.I/(2*pi*lens.R^2);
k = qe*dB_dr/(me*c*beam.gamma);
global magK
magK.k = k;
magK.l0 = lens.start;
magK.l1 = magK.l0 + lens.L;

% Assume first that we start at waist -> dbeta is 0!
beta0 = beam.sigma0^2/beam.emittance;
beta1 = 0; 
yin = [beta0; beta1];
options = odeset('RelTol', 2.5e-10, 'AbsTol', 1e-10, 'Events', @stopEvent,...
    'Refine', 8);
[s,beta] = ode45(@diffFcn, [0 2], yin, options);
if singlePart
    [outStr.ss,outStr.u] = ode45(@diffFcn2, [0, 1], [0, 1/beam.gamma], options);
end

% If we don't want extra return parameters, return now
if nargout<3; return; end

% Otherwise, prepare these. Attach inputs:
outStr.beam = beam;
outStr.lens = lens;
% Now find the focus.
betaA = beta(:,1); betaA(s<magK.l1) = 1e12;
[~,ind] = min(betaA);
outStr.spotSize = sqrt(beta(ind,1)*beam.emittance);
nTemp = 2*(size(beta,1) - ind);
outStr.focRegion = [s(end-nTemp:end), beta(end-nTemp:end,:)];
% Fit a parabola to the focal region - test showed get 1e-12 residuals to
% the fit, so pretty good. Then find the minimum of the function from its
% detivative.
vv = polyfit(outStr.focRegion(:,1), outStr.focRegion(:,2), 2);
outStr.z_f = -0.5*vv(2)/vv(1);
outStr.betaStar = polyval(vv, outStr.z_f);
outStr.spotSize2 = sqrt(outStr.betaStar*beam.emittance);
% Also think about returning the Twiss parameters along the propagation:
outStr.alpha = -0.5*beta(:,2);
outStr.beta = beta(:,1);
outStr.gamma = (1+outStr.alpha.^2)./outStr.beta;
outStr.div = sqrt(beam.emittance*outStr.gamma(ind));



function dd = diffFcn(s,y)
% y(1) = beta
% y(2) = dbeta/ds
% y(3) = d^2beta/ds^2
global magK;
dd = zeros(2,1);
dd(1) = y(2);
if s<magK.l1 && s>magK.l0
    K = magK.k;
else
    K = 0;
end
dd(2) = (4 - 4*y(1)^2*K + y(2)^2)/(2*y(1));

function dd = diffFcn2(s,y)
% For single particle orbit, Hill's equation
global magK;
dd = zeros(2,1);
dd(1) = y(2);
if s<magK.l1 && s>magK.l0
    K = magK.k;
else
    K = 0;
end
dd(2) = -K*y(1);

function [value, isterminal, direction] = stopEvent(s, y)
% Creates an event to stop the integration if we're past the focus.
global magK;
value      = double((y(2) > 25) && s > magK.l1);
isterminal = 1;   % Stop the integration
direction  = 0;
