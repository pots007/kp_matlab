% betaFunctionPlay.m

syms e
syms s real
syms b(s)
syms k real
syms g real
f = 0.5*b*diff(b,s,2) - 0.25*diff(b,s)^2 == 1;
Db = diff(b,s);

bc1 = Db(0) == 0;
bc2 = b(0) == e;

sol = dsolve(f, [bc1, bc2])
f2 = 0.5*b*diff(b,s,2) - 0.25*diff(b,s)^2 + b*2*k == 1;

bc3 = Db(0) == g;
s2 = dsolve(f2)