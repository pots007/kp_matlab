

% Scan over some currents and some energies
Is = 400:10:1000; % Linear space
Is = logspace(log10(400), log10(1000), 500); % Log space
gammas = 50:1:275;

% Plasma lens properties
lens.I = 800;
lens.start = 0.05;
lens.L = 0.04;
lens.R = 0.001;
% Electron beam:
beam.gamma = 200;
beam.emittance = 1e-6/beam.gamma; % Need to use geometrical emittance here!
beam.sigma0 = 1e-6;

fprintf('Scanning focal lengths! Expect to take %i seconds\n', ...
    round(length(Is)*length(gammas)*0.1));

focMap = nan(length(Is), length(gammas), 4);
tic
for I=1:length(Is)
    fprintf('Current %3i (%i)\n', I, length(Is));
    for gamma=1:length(gammas)
        %fprintf('Current,energy: %i(%i), %i(%i)\n', I, length(Is), gamma, length(gammas));
        lens.I = Is(I);
        beam.gamma = gammas(gamma);
        beam.emittance = 1e-6/beam.gamma; % Need to use geometrical emittance here!
        [beta, s, u, ss] = betaFunctionSolve(lens, beam);
        betaA = beta(:,1); betaA(s<lens.L+lens.start) = 1e7;
        [~,ind] = min(betaA);
        beamSize = sqrt(beta(ind,1)*beam.emittance);
        if s(ind) > (lens.L+lens.start+0.001) && s(ind)<0.99 && beamSize<100e-6
            focMap(I,gamma,1) = s(ind);
            focMap(I,gamma,2) = beamSize;
            focMap(I,gamma,3) = beta(ind,1);
            alphaTwiss = -0.5*beta(ind,2);
            gammaTwiss = (1+alphaTwiss^2)/beta(ind,1);
            theta = sqrt(beam.emittance*gammaTwiss);
            focMap(I,gamma,4) = theta;
        end
        
        % And try to limit the amount of useless solutions...
        if gamma>5 && isnan(focMap(I,gamma,1))
            fprintf('\tStopped at current,energy %i,%i\n', I, gamma);
            break;
        end
    end
end
toc

%save(fullfile(getMatlabGitPath, 'papers', '2018_TunableThomson/focMap'), 'focMap')
%%

hfig = figure(34212); clf(hfig); hfig.Position = [300 200 800 450];
locs = GetFigureArray(2, 1, [0.07 0.11 0.13 0.11], 0.01, 'across');
ax1 = makeNiceAxes(hfig, locs(:,1));
uimagesc(gammas, Is, focMap(:,:,1))
cb1 = colorbar('location', 'northoutside');
xlabel(cb1, 'Focus position / m');

ax2 = makeNiceAxes(hfig, locs(:,2));
uimagesc(gammas, Is, focMap(:,:,2)*1e6)
axis(ax1, 'tight'); axis(ax2, 'tight');
cb2 = colorbar('location', 'northoutside');
xlabel(cb2, 'Focus size / micron');

xlabel(ax1, '\gamma'); xlabel(ax2, '\gamma');
ylabel(ax1, 'Lens current / A'); ylabel(ax2, 'Lens current / A');
set([ax1, ax2], 'Layer', 'top');
set([cb1, cb2], 'LineWidth', 2);
ax2.YAxisLocation = 'right';
setAllFonts(hfig, 14);

%export_fig('/home/kpoder/DESYcloud/Writing/2018_TunableThomson/FocMap', '-pdf', '-nocrop', hfig);
%% Make some fits to allow better exploration of the data!
[Gfit, Ifit, Ffit] = prepareSurfaceData(gammas, Is', focMap(:,:,1));
%[~, ~, Sfit] = prepareSurfaceData(gammas, Is', focMap(:,:,2));
%[~, ~, Bfit] = prepareSurfaceData(gammas, Is', focMap(:,:,3));

% The fit for the position of the focus - looks shit
%focalPlaneFit = fit([Gfit,Ifit], Ffit, 'poly45');
%figure(4);plot(focalPlaneFit,[Gfit,Ifit],Ffit); set(gca, 'ZLim', [0 1])
intFData = focMap(:,:,1); intFData(isnan(intFData)) = 0;
focalPlaneInt = griddedInterpolant({Is,gammas},intFData, 'spline');

% Fit of what energy is focussed where. Doesn't work too well...
focussedEnergyFit = fit([Ffit,Ifit], Gfit, 'cubicinterp');
%figure(5);plot(focussedEnergyFit,[Ffit,Ifit],Gfit);
focussedEnergyInt = scatteredInterpolant(Ffit, Ifit, Gfit, 'linear');

% Turns out this fit is quite shit.
%betaFcnFit = fit([Gfit,Ifit], Bfit, 'poly22');
%figure(6); plot(betaFcnFit, [Gfit,Ifit],Bfit);set(gca, 'ZLim', [0 0.002])
% So instead construct an interpolation object
intBData = focMap(:,:,3); intBData(isnan(intBData)) = 0;
betaFcnInt = griddedInterpolant({Is,gammas},intBData, 'spline');
%betaFcnInt = scatteredInterpolant(Gfit, Ifit,Bfit, 'linear');

% This fit is shit as well!
%focussedSpotSizeFit = fit([Gfit, Ifit], Sfit, 'poly55');
%figure(6); plot(focussedSpotSizeFit, [Gfit,Ifit],Sfit);set(gca, 'ZLim', [0 1e-5])
intSData = focMap(:,:,2); intSData(isnan(intSData)) = 0;
focussedSpotSizeInt = griddedInterpolant({Is,gammas},intSData, 'linear');

% Not gonna try for this then - straight to fit.
intTData = focMap(:,:,4); intTData(isnan(intTData)) = 0;
thetaInt = griddedInterpolant({Is,gammas},intTData, 'spline');
%% Can now plot the energy at a particular point!

% Take s=30, for example.
hfig = figure(24145); clf(hfig); hfig.Position = [1200 500 600 350];
ax = makeNiceAxes(hfig);
plotI = 400:1000;
plotGammas = focussedEnergyFit(0.662*ones(size(plotI)), plotI);
plotPhotons = plotGammas.^2*1.53*4*1e-3;
%plotGammas = focussedSpotSizeInt(0.3*ones(size(plotI)), plotI);
h1 = plot(plotI, plotGammas, '-');
h2 = plot(plotI, plotGammas.^2*1.53*4*1e-3, '-r');
set([h1, h2], 'LineWidth', 2);
xlabel('Current / A');
ylabel({'\gamma', 'x-ray energy / keV'});
legend([h1, h2], {'Electron \gamma', 'x-ray energy'}, 'Location', 'nw');
setAllFonts(hfig, 14);
set(ax, 'XTick', 400:100:1000, 'YLim', [45 425]);
grid(ax, 'on');
makeNiceGridHG2(ax);

if overWrite
    dlmwrite('/home/kpoder/Writing/TunableThomson/data/focEns.txt',...
        [plotI; plotGammas;plotSpots*1e6]','delimiter',',','precision',6)
end
%export_fig('/home/kpoder/DESYcloud/Writing/2018_TunableThomson/FocEns', '-pdf', '-nocrop', hfig);

%% Calculate focal spot sizes properly
plotSpots = nan(size(plotI));
tic
for I=1:length(plotI)
    %if I>2; break; end
    fprintf('Current %3i (%i)\n', I, length(plotI));
    
    %fprintf('Current,energy: %i(%i), %i(%i)\n', I, length(Is), gamma, length(gammas));
    lens.I = plotI(I);
    beam.gamma = plotGammas(I);
    beam.emittance = 1e-6/beam.gamma; % Need to use geometrical emittance here!
    [beta, s, outStr] = betaFunctionSolve(lens, beam);
    betaA = beta(:,1); betaA(s<lens.L+lens.start) = 1e7;
    [~,ind] = min(betaA);
    beamSize = sqrt(beta(ind,1)*beam.emittance);
    %plotSpots(I) = beamSize;
    plotSpots(I) = outStr.spotSize2;
end
toc


%% Try to calculate the bandwidth within one beta fcn for each energy.
% For each energy, find the beta fucntion at focus.
plotI = 401:1000; % The currents we are interested in
plotGammas = focussedEnergyFit(0.662*ones(size(plotI)), plotI); % The gammas focussed to our interesting point
plotBetaFcns = betaFcnInt(plotI,plotGammas);
%plotSpots = focussedSpotSizeInt(plotI, plotGammas);
%plotSpots = focussedSpotSizeFit(plotGammas, plotI);
figure(6453);
plot(plotI, plotBetaFcns);
%plot(plotI, plotSpots);
% Almost acceptable - should do the map in log space soon!

% Anyways, for each current setting, find the focal plane. Then use the
% beta function to calculate energy on either side of the focal plane.
plotFocPlanes = focalPlaneInt(plotI, plotGammas);
BW_low = focussedEnergyFit(plotFocPlanes-plotBetaFcns, plotI);
BW_high = focussedEnergyFit(plotFocPlanes+plotBetaFcns, plotI);
plotThetas = thetaInt(plotI, plotGammas);

% Plotting:
hfig = figure(24145); clf(hfig); hfig.Position = [1200 500 600 350];
pos = [0.11 0.16 0.75 0.69];
ax = makeNiceAxes(hfig, pos); ax.Color = 'None'; hold(ax, 'on');
ax2 = axes('Parent', hfig, 'Position', pos, 'Color', 'None', 'YColor', 'r',...
    'YAxisLocation', 'right', 'XAxisLocation', 'top', 'NextPlot', 'add');
h1 = plot(ax2, plotI, plotThetas*1e3, 'r');
h2 = plot(ax, plotI, (BW_high-BW_low));
set([h1,h2], 'LineWidth', 2);
grid(ax, 'on');
set(ax2, 'XTickLabel', num2str(focussedEnergyFit(0.3*ones(size(ax.XTick)), ax.XTick)', '%1.0f'));

xlabel(ax, 'Current / A');
ylabel(ax, 'Bandwidth');
xlabel(ax2, 'Focussed \gamma');
ylabel(ax2, 'Divergence / mrad');
setAllFonts(hfig, 14);

% And now calculate the bandwidths
xBW_gamma = 2*(BW_high-BW_low)./plotGammas;
xBW_div = (0.5*plotGammas.*plotThetas).^2;
xBW = sqrt(xBW_gamma.^2+xBW_div.^2);
plot(plotI, xBW_gamma)

if overWrite
    dlmwrite('/home/kpoder/Writing/TunableThomson/data/focData.txt',...
        [plotI;plotGammas;plotPhotons;plotSpots;...
        (BW_high-BW_low);plotThetas;xBW]','delimiter',',','precision',6)
end
%export_fig('/home/kpoder/DESYcloud/Writing/2018_TunableThomson/Bandwidth', '-pdf', '-nocrop', hfig);
