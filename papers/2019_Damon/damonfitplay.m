% damonfitplay.m
%
%

fdat = load('DaMonTraces20181011.mat');
v = fdat.MaxTraceDaMonSignal;
u = fdat.MaxTraceDaMonCharge;

% p facs for charge, qs for signal:
q0_ = 3.209;
q1_ = 5.847e-5;
q2_ = 9.661e-5;

% 15.12.2017 Dirk : SIGNAL
p0_ = 6.783; p1_ = 6.232e-5; p2_ = 6.062e-10;
% %Simon, 01.12.2018
%p0_ = 7.1; p1_ = 5.232e-5; p2_ = 1.095e-9; 

sig_ = @(x) p2_*10.^(p0_ + p1_*x);
cha_ = @(x) q2_*10.^(q0_ + q1_*x);

% Filter out a region where the signals should be linear, ie no saturation
% and no noise level
logm = (sig_(v) > 10) & (sig_(v) < 20);
fprintf('Filtering leaves %i points out of %i\n', sum(logm), length(v));
uf = u(logm); vf = v(logm);

% Now define the fit function, arrived at through some maths...
ff = @(p0,p1,p2,q0,q1,q2,x) (log10(q2./p2)+q0-p0+q1*x)./p1;
ff2 = @(x, xdat) ff(x(1), x(2), x(3), x(4), x(5), x(6), xdat);

ff3 = @(p0,p1,p2,q0,q1,q2,x,y) p2*10.^(p0+p1*x)-(q2*10.^(q0+q1*y));
ff3 = @(p0,p1,p2,q0,q1,q2,x,y) (log10(p2)+(p0+p1*y))-(log10(q2)+(q0+q1*x));
% And try the fitting...
inpos = [p0_,p1_,p2_, q0_,q1_,q2_];
%ffit = fit(uf, vf, ff, 'Robust', 'LAR', 'StartPoint', inpos, ...
ffit = fit([uf, vf], zeros(size(uf)), ff3, 'StartPoint', inpos,...
    'Lower', inpos/100, 'Upper', inpos*100, 'TolFun', 1e-9, 'MaxIter', 1000,...
    'MaxFunEvals', 2000);
%ffi = lsqcurvefit(ff2, inpos, uf, vf);
sig = @(x) ffit.p2 * 10.^(ffit.p0 + ffit.p1*x);
cha = @(x) ffit.q2 * 10.^(ffit.q0 + ffit.q1*x);

plot(cha_(u), sig_(v), '.k', cha(u), sig(v), '.r', [0 45], [0, 45], '--k');

cc = coeffnames(ffit);
for i=1:6
    fprintf('%10s\t%2.4e\t%2.4e\t%2.4f\n', cc{i}, ffit.(cc{i}),...
        eval([cc{i} '_']), eval([cc{i} '_'])/ffit.(cc{i}));
end

