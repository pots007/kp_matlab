function [denoised_image,acc_weight] = ...
FNLD2D_Slow_RoundV3(noisy_image,grande,pequeno,w1coef,sigmacoef)

format long;

denoised_image = noisy_image*0;

side_add = grande + pequeno;

CoeffSigmaSquare = w1coef^2;
hSquare = sigmacoef^2;

[height,width] = size(noisy_image);

grandones = ones(2*grande+1,2*grande+1);

sss_maximus = sum(sum(grandones*(16000-0)));
sss_maximus = sss_maximus^2;

%% gettingt the round filter qith pequeno size

a = linspace(-pequeno,pequeno,2*pequeno+1);
a = repmat(a,2*pequeno+1,1);
b = linspace(-pequeno,pequeno,2*pequeno+1);
b=b';
b = repmat(b,1,2*pequeno+1);
c = a.^2 + b.^2;
round_filter = c<=pequeno^2;

%figure(50)
%imagesc(round_filter)

NPPequeno = sum(sum(round_filter));

%% a larger image to avoid the border problems...
padded_image = padarray(noisy_image,[side_add,side_add],'symmetric','both');

figure(1001)
subplot(1,2,1), imagesc(noisy_image),colormap(gray), title('initial image'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;
subplot(1,2,2), imagesc(padded_image),colormap(gray), title('padded image'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;

%% all image pixels cycle
for lili = 1:height
  
  tic
  line = lili
  of = height
  
  for coco = 1:width
 
    %ddd2 = grandones*1;
    
    %fixed patch for this (lili:coco)
    lia = lili + side_add - pequeno;
    lib = lili + side_add + pequeno;
    coa = coco + side_add - pequeno;
    cob = coco + side_add + pequeno;
    fixed_patch = padded_image(lia:lib,coa:cob);
    fixed_patch = fixed_patch.*round_filter;
    
    % all grande pixels arround (lili,coco) pixel cycle
    for Glili = -grande:grande
        
      for Gcoco = -grande:grande
          
        %LIX = Glili
        %COX = Gcoco
        
        %mobile patch for this (Glili:Gcoco)
        Glia = lia + Glili;
        Glib = lib + Glili;
        Gcoa = coa + Gcoco;
        Gcob = cob + Gcoco;
        mobile_patch = padded_image(Glia:Glib,Gcoa:Gcob);
        mobile_patch = mobile_patch.*round_filter;
      
        % test 1 with 45 degrees interval
        clear sss_vec_1;
        clear t1_angles;
        clear postest1;
        t1_angles = [-45,0,45,90,135,180,225,270,315,360];
        % ROT 000 degrees
        ROTpatch = mobile_patch;
        sss_vec_1(2) = sqdiff(fixed_patch,ROTpatch);
        % ROT 180 degrees
        ROTpatch = flipdim(mobile_patch,1);
        sss_vec_1(6) = sqdiff(fixed_patch,ROTpatch);
        % ROT 90 degrees
        ROTpatch = imrotate(mobile_patch,90,'crop');
        sss_vec_1(4) = sqdiff(fixed_patch,ROTpatch);
        % ROT 270 degrees
        ROTpatch = flipdim(ROTpatch,2);
        sss_vec_1(8) = sqdiff(fixed_patch,ROTpatch); 
        
        % ROT 045 degrees
        ROTpatch = imrotate(mobile_patch,45,'crop');
        sss_vec_1(3) = sqdiff(fixed_patch,ROTpatch);
        % ROT 135 degrees
        ROTpatch = imrotate(mobile_patch,135,'crop');
        sss_vec_1(5) = sqdiff(fixed_patch,ROTpatch);        
        % ROT 225 degrees
        ROTpatch = imrotate(mobile_patch,225,'crop');
        sss_vec_1(7) = sqdiff(fixed_patch,ROTpatch); 
        % ROT 315 degrees
        ROTpatch = imrotate(mobile_patch,315,'crop');
        sss_vec_1(9) = sqdiff(fixed_patch,ROTpatch);        
        % repeat borders to avod to much trouble
        sss_vec_1(1) = sss_maximus;
        sss_vec_1(10) = sss_maximus;
        % find minimum of test 1
        mintest = min(sss_vec_1);
        [dontcare postest1] = find(sss_vec_1==mintest);
        postest1 = postest1(1);
        %correct borders
        sss_vec_1(1) = sss_vec_1(9);
        sss_vec_1(10) = sss_vec_1(2);
        
        %aa1 = sss_vec_1
        
        % get the 3 lowest elements
        sss_vec_old = sss_vec_1(postest1-1:postest1+1);
        ttt_angles_old = t1_angles(postest1-1:postest1+1);
        
        control = 0;
        max_it = 5;
        it = 1;

        while control<2
          [ttt_angles_new,sss_vec_new,control] = refine_angle(ttt_angles_old,sss_vec_old,fixed_patch,mobile_patch); 
          sss_vec_old = sss_vec_new; %vector w/ 3 elements
          ttt_angles_old = ttt_angles_new; %vector w/ 3 elements
          it = it+1;
          if it == max_it
            control = 2;
          end
        end
        
       %sound(sin(linspace(-500*pi,500*pi,4000))');
       %sound(sin(linspace(-125*pi,125*pi,1000))');
   
        mintest = min(sss_vec_old);

        mintest = mintest/NPPequeno;
        thisGli = Glili+grande+1;
        thisGco = Gcoco+grande+1;
        ddd2(thisGli,thisGco) = mintest;
        
        %figure(19), imagesc(ddd2),  colorbar, drawnow;

      end
      
    end
    
    % calculating the weight outside the cycle
    wwwpq = ddd2-grandones.*CoeffSigmaSquare;
    aux = wwwpq>0.0;
    wwwpq = wwwpq.*aux;
    wwwpq = wwwpq./hSquare;
    wwwpq = exp(-wwwpq);
    
    wwwpq(grande+1,grande+1) = 0.0;
    wwwpq(grande+1,grande+1) = max(max(wwwpq));
    
      
    %figure(20), imagesc(wwwpq), colorbar, drawnow;
      
         
    % local Grande matrix
    % Gra_mat_fixed is the big matrix (2*grande+2)*(2*grande+1) that is centered in the pixel (lili,coco) 
    
    GGlia = lili + side_add - grande;
    GGlib = lili + side_add + grande;
    GGcoa = coco + side_add - grande;
    GGcob = coco + side_add + grande;
    Gra_mat_fixed = padded_image(GGlia:GGlib,GGcoa:GGcob);
    
    % C(p) the sum of the weights for normalization
    Cp = sum(sum(wwwpq));
    
   
    new_pixel = 1/Cp * sum(sum(double(Gra_mat_fixed).*wwwpq));
    denoised_image(lili,coco) = new_pixel;
    
    acc_weight(lili,coco) = Cp; 
    
    %figure(100), imagesc(denoised_image),colormap gray,  colorbar, caxis([4000 6000]), drawnow;
  
    % and I suppose that is it...
    % now it just needs to do it 70 milion times ...
      
    
    
      
  end
  toc
end

function sss = sqdiff(fixed_patch,mobile_patch)
 aaa = fixed_patch - mobile_patch;
 aaa = aaa.^2;
 
   %figure(333)
   %subplot(1,3,1), imagesc(fixed_patch), colorbar;
   %subplot(1,3,2), imagesc(mobile_patch), colorbar;
   %subplot(1,3,3), imagesc(aaa), colorbar, drawnow;
   
 
   
 sss = sum(sum(aaa));
 %sound(sin(linspace(-sss/1000*pi,sss/1000*pi,1000))');
 
function [ttt_angles_new,sss_vec_new,control] = refine_angle(ttt_angles_old,sss_vec_old,fixed_patch,mobile_patch)
%vectors w/ 3 elements
 
angles(1) = ttt_angles_old(1);
ss(1) = sss_vec_old(1);

angles(3) = ttt_angles_old(2);
ss(3) = sss_vec_old(2);

angles(5) = ttt_angles_old(3);
ss(5) = sss_vec_old(3);

angles(2) = (angles(1)+angles(3))/2; 
angles(4) = (angles(3)+angles(5))/2;

ROTpatch = imrotate(mobile_patch,angles(2),'crop');
ss(2) = sqdiff(fixed_patch,ROTpatch);
ROTpatch = imrotate(mobile_patch,angles(4),'crop');
ss(4) = sqdiff(fixed_patch,ROTpatch);

%find the minimum position
mintest = min(ss); %we know that the minimum is not in the first and last element of the vector
[dontcare postest] = find(ss==mintest);
postest_unicum = postest(1);

% ao many minimums for control (if more that one dont need to work more)
[dontcare,control] = size(postest);

%reduce the vectors to three relevant 3 elements 
ttt_angles_new = angles(postest_unicum-1:postest_unicum+1);
sss_vec_new = ss(postest_unicum-1:postest_unicum+1);
