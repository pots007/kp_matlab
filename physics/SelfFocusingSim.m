% Try to simulate the focal spot size as a function of distance and whatnot

% Based on Sprangle, IEEE 1987

function SelfFocusingSim
Constants;
global fstat;
fstat.a0 = 6;
fstat.R0 = 2/3*25e-6;
fstat.ne = 2e18;
k0 = 2*pi/8e-7;
fstat.V0 = (2*c/(k0*fstat.R0^2*fstat.a0^2))^2;
fstat.alpha = (omegapcalc(fstat.ne)*fstat.a0*fstat.R0/(4*c))^2
zR = pi*fstat.R0^2/8e-7;
z0 = 3e-3;
dR_dz = -fstat.R0*1/sqrt(1 + z0^2/zR^2)*2*(z0)/zR^2;
dx_dt = dR_dz/(fstat.a0*fstat.R0)*c;%*1e17;
%xin = 50e-6/(fstat.a0*fstat.R0);
Rin = fstat.R0*sqrt(1+z0^2/zR^2);
xin = Rin/(fstat.a0*fstat.R0);

options = odeset('RelTol', 2.5e-10, 'AbsTol', 1e-10);

[t, Ro] = ode45(@Vfun, [0 1e-10], [xin; dx_dt], options);
assignin('base', 'Ro', Ro);
figure(44); clf(44);
plot(t*c*1e3, fstat.a0*fstat.R0*Ro(:,1));
xlabel('z / mm'); ylabel('w / um');


function dd = Vfun(~,xx)
global fstat;
dd = zeros(2,1);
dd(1) = xx(2);
x = xx(1);
rt = sqrt(1+1/x^2);
dd(2) = fstat.V0*(1/x^3 - 16*fstat.alpha*x*(rt - 1 +2*log(2) - 2*log(rt+1)));
