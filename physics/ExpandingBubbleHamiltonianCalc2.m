% This is meant to give trajectories to bubble expansion thingy.

function f = ExpandingBubbleHamiltonianCalc2(fin)

%fin.R0 = 5;
%fin.eps = 0.009;
%fin.Rend = 1.2*fin.R0; % times the initial bubble size
%fin.tend = (fin.Rend/fin.R0-1)/fin.eps;
%z0 = 6;
%fin.gammap = 66.1;
%fin.betap = sqrt(1-1/fin.gammap^2);

solfun = @(t,y) SphericalBubbleHamiltonian_(t,y,fin);
opts = odeset('RelTol', 1e-9, 'AbsTol', 5e-9, 'MaxStep', 0.05);
[f.t,f.r] = ode45(solfun, [0 fin.tfin], [fin.z0; fin.pz0; fin.y0; 0], opts);
% Now that we have the trajectories, can calculate the Hamiltonian as a
% function of time


function dd = SphericalBubbleHamiltonian_(t, y, fin)
% y(1) = z, y(2) = pz, y(3) = y, y(4) = py
% The inputs here are ordinary momentum, the rate of change of position.
dd = zeros(4,1);
% gamma is defined in terms of ordinary momentum.
gamma = sqrt(1 + y(2)^2 + y(4)^2);
% And so are the rates of change of position.
% dz/dt
dd(1) = y(2)/gamma - fin.betap;
% dy/dt
dd(3) = y(4)/gamma;
if fin.eps~=0
    if t<fin.tend
        R = fin.R0*(1 + fin.eps*t);
    else
        R = fin.Rend;
    end
else
    R = fin.R0;
end
% Shape of radial derivative of potential, as in Kostyukov2004
r = sqrt(y(1)^2+y(3)^2);
%dphi_dr = -r/4*(tanh((r-R)/fin.d) - 1);
fr = 0.5*(1 + tanh((R - r)/fin.d));
% The equation below are for canonical momentum
% dPz/dt
%dd(2) = -(1 + y(2)/gamma)*(dphi_dr*y(1)/r);% + y(4)/gamma*(dphi_dr*y(3)/r);
% dPy/dt
%dd(4) = -(1 + y(2)/gamma)*(dphi_dr*y(3)/r); % THis is the same everywhere!
% p = P+A, so to get to rate of change of ordinary momentum need to add on
% time derivative of A
%dAdt = 2*(dphi_dr*y(1)/r)*dd(1);
%dd(2) = dd(2) + dAdt; dd(4) = dd(4) + dAdt;
% From Kostyukov 2009:
%dd(2) = fr*(-(1 + fin.betap)*y(1)/4 + y(4)/gamma*y(3)/4);
%dd(4) = -fr*(1 + y(2)/gamma)*(dphi_dr*y(3)/r);
dd(2) = fr*(y(4)/gamma*y(3)/4 - (1 + fin.betap)*y(1)/4);
dd(4) = -fr*(1 + y(2)/gamma)*y(3)/4;
%fprintf('%f\t%f\t%f\t%f\t%f\n', t, dd);