% This is meant to give trajectories to bubble expansion thingy.

function plotExpandingBubbleHamiltonian

fin.R0 = 5;
fin.eps = 0.009;
fin.Rend = 1.2*fin.R0; % times the initial bubble size
fin.tend = (fin.Rend/fin.R0-1)/fin.eps;
z0 = 6;
gammap = 66.1;
fin.betap = sqrt(1-1/gammap^2);
pz0 = 2;%2*
fin.betap/(1-fin.betap^2)
solfun = @(t,y) SphericalBubbleHamiltonian(t,y,fin);
opts = odeset('RelTol', 1e-8, 'AbsTol', 5e-8);
[t,r] = ode45(solfun, [0 1000], [z0; pz0; 4.8; 0], opts);
% Now that we have the trajectories, can calculate the Hamiltonian as a
% function of time

hfig = figure(112); clf(hfig);
tt = 0:0.01:2*pi;
plot(r(:,1), r(:,3), '-r', fin.R0*cos(tt), fin.R0*sin(tt), '--k',...
    fin.R0*(1+fin.tend*fin.eps)*cos(tt), fin.R0*(1+fin.tend*fin.eps)*sin(tt), '--k');
%plot(t, r(:,1:4));
%plot(t, sqrt(1 + r(:,2).^2 + r(:,4).^2), '-r');
axis image
set(gca, 'YLim', [-7 7]);

function dd = SphericalBubbleHamiltonian(t, y, fin)
% y(1) = z, y(2) = pz, y(3) = y, y(4) = py

dd = zeros(4,1);
gamma = sqrt(1 + y(2)^2 + y(4)^2);
% dz/dt
dd(1) = y(2)/gamma - fin.betap;
% dy/dt
dd(3) = y(4)/gamma;
if t<fin.tend;
    R = fin.R0*(1 + fin.eps*t);
else
    R = fin.Rend;
end
if y(1)^2+y(3)^2<=R^2
    % dpz/dt
    dd(2) = -0.5*(1+y(2)/gamma)*y(1)/2;
    % dy/dt
    dd(4) = -0.5*(1+y(2)/gamma)*y(3)/2;
end

