% This is meant to give trajectories to bubble expansion thingy.

function f = ExpandingBubbleHamiltonianCalc(fin)

%fin.R0 = 5;
%fin.eps = 0.009;
%fin.Rend = 1.2*fin.R0; % times the initial bubble size
%fin.tend = (fin.Rend/fin.R0-1)/fin.eps;
%z0 = 6;
%fin.gammap = 66.1;
%fin.betap = sqrt(1-1/fin.gammap^2);

solfun = @(t,y) SphericalBubbleHamiltonian(t,y,fin);
opts = odeset('RelTol', 1e-8, 'AbsTol', 5e-8);
[f.t,f.r] = ode45(solfun, [0 fin.tfin], [fin.z0; fin.pz0; fin.y0; 0], opts);
% Now that we have the trajectories, can calculate the Hamiltonian as a
% function of time

% hfig = figure(112); clf(hfig);
% tt = 0:0.01:2*pi;
% plot(r(:,1), r(:,3), '-r', fin.R0*cos(tt), fin.R0*sin(tt), '--k',...
%     fin.R0*(1+fin.tend*fin.eps)*cos(tt), fin.R0*(1+fin.tend*fin.eps)*sin(tt), '--k');
% %plot(t, r(:,1:4));
% %plot(t, sqrt(1 + r(:,2).^2 + r(:,4).^2), '-r');
% axis image
% set(gca, 'YLim', [-7 7]);

function dd = SphericalBubbleHamiltonian(t, y, fin)
% y(1) = z, y(2) = pz, y(3) = y, y(4) = py

dd = zeros(4,1);
Az = 0;%(y(1)^2 + y(3)^2 - fin.R0^2)/8*(y(1)^2+y(3)^2<fin.R0^2);
gamma = sqrt(1 + (y(2) + Az)^2 + y(4)^2);
% dz/dt
dd(1) = y(2)/gamma - fin.betap;
% dy/dt
dd(3) = y(4)/gamma;
if fin.eps~=0
    if t<fin.tend
        R = fin.R0*(1 + fin.eps*t);
    else
        R = fin.Rend;
    end
else
    R = fin.R0;
end
if (y(1)^2+y(3)^2)<=R^2
    % dpz/dt
    %dd(2) = -0.5*(1+y(2)/gamma)*y(1)/2; %From Kalmykov
    dd(2) = -y(1)/4*2 + y(4)/gamma*y(3)/4;
    
    
    % dpy/dt
    %dd(4) = -0.5*(1+y(2)/gamma)*y(3)/2; % From Kalmykov
    dd(4) = -(1 + y(2)/gamma)*y(3)/4; % THis is the same everywhere!
else
    %rho = sqrt(y(1)^2+y(3)^2);
    %thet = atan(y(3)/y(1));
    %dphidrho = -rho/4*(tanh((rho-fin.R0)/fin.d) - 1);
    %dd(2) = -dphidrho*cos(thet)*(2 + y(4)/gamma);
    %dd(4) = -(1+y(2)/gamma)*(dphidrho*sin(thet));
end

