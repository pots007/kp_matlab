fdat = load('GemTrackMat');

% Energies vector must be of length size(GemTrackMat, 2) and be a COLUMN vector
%entest = zeros(size(fdat.GemDeconvMat.mat, 2), 1);
% For funsies, make two peaks at some energies:
%entest(250:252) = 4;
%entest(50:52) = 4; entest(150:152) = 4; entest(100:102) = 4; entest(200:202) = 4; entest(1:2) = 4;
%entest(450:452) = 4; entest(400:402) = 4;
%entest(100:103) = 1; entest(123:134) = 1;
%entest( 30:60) = 5.3;
%entest(70:100) = exp(-(linspace(-10,10,31)/4).^2);
%entest(130:150) = exp(-(linspace(-10,10,21)/4).^2);
entest = (square(fdat.GemDeconvMat.energies/60,10)+1)';
scrvec = fdat.GemDeconvMat.mat*entest;
scrvec2 = zeros(size(scrvec));
scrvec2(175:185) = 0.5;

%scrvec = scrvec + .5*rand(size(scrvec));

hfig = figure(8); clf(hfig);
set(hfig, 'Color', 'w');
locs = GetFigureArray(1, 3, [0.07 0.05 0.07 0.1], 0.1, 'down');

%scrvec = smooth(scrvec,3);

ax(2) = axes('Parent', hfig, 'Position', locs(:,2));
plot(ax(2), fdat.GemDeconvMat.scrbins, scrvec, '-ok');
title(ax(2), 'Signal on screen');
set(ax(2), 'XDir', 'reverse');
xlabel(ax(2), 'Distance on screen from end / mm');

ax(1) = axes('Parent', hfig, 'Position', locs(:,1));
plot(ax(1), fdat.GemDeconvMat.energies, entest, '-^k');
title('Input spectrum');
xlabel(ax(1), 'Electron energy / MeV');

% ax(3) = axes('Parent', hfig, 'Position', locs(:,3));
% plot(ax(3), fdat.GemDeconvMat.energies, fdat.GemDeconvMat.mat\scrvec, '-^k');
% title('Deconcolved spectrum');
% xlabel(ax(3), 'Electron energy / MeV');
% %set(ax(2), 'YLim', [0 3]);

ax(3) = axes('Parent', hfig, 'Position', locs(:,3));
% Try out this Tikhonov thingy
A = sparse(fdat.GemDeconvMat.mat);
alpha = .1; gamma = alpha*eye(size(A));


tikh_x = (A'*A + gamma'*gamma)^(-1)*A'*scrvec;
plot(ax(3), fdat.GemDeconvMat.energies, tikh_x, '-ok');
%x = lsqlin(A, scrvec, gamma, 2.3*ones(size(scrvec)));
%x = linsolve(A, scrvec);
title('Deconvolved spectrum, normalisation');
xlabel(ax(3), 'Electron energy / MeV');

linkaxes(ax([1 3]), 'xy');

set(hfig, 'Position', [100 300 550 850]);
make_latex(hfig); setAllFonts(hfig, 14);
annotation('textbox', [0.2 0.95 0.6 0.05], 'String', fdat.GemDeconvMat.fname,...
    'FitBoxToText', 'on', 'HorizontalAlignment', 'center', 'FontSize', 13,'Interpreter', 'none');
