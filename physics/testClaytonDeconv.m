% testClaytonDeconv.m
%
% Test out the deconvolution method Clayton has in his PRL. It must be
% good, it got into PRL! Also, deconvolution must be done well, as they
% have a 20cm, 0.46T magnet! Let's try it out then. 
% 
% Using shot 034 from 0908r008.

datfol = fullfile(getGDrive, 'Experiment', '2015_Gemini', 'EspecAnalysis',...
    '20150908r008', 'mat_manual');
fdat = load(fullfile(datfol, '20150908r008s038_Espec1.mat'));

% The deconvolution is
% dN/dE = dN/dx * SIGMA_theta ( D(theta)*dx_dE(theta) ) 
% where D(theta) is the spread of angles D(theta) = exp(-(theta-theta_0)^2/div^2 )
% and dx_dE(theta) is the dispersion at angle theta

dQ_dx = fdat.shotdata.dQ_dx;
% Take theta_FWHM to be 1 mrad
FWHM_width = 2;
sigma_theta = FWHM_width/(2*sqrt(log(2)));
npnts = 100;
offset = fdat.shotdata.track_angle; 
divs = linspace(-2*sigma_theta, 2*sigma_theta, npnts)+offset;
D = exp(-(divs-offset).^2/sigma_theta^2);
% Iterate over pixels:
x_ax = fdat.shotdata.alongscreen;
E_ax = zeros(size(x_ax));
dQ_dE = zeros(size(x_ax));
ttemp = zeros(size(x_ax));
for pix = 1:length(x_ax)
    % Get the the different energies that hit this pixel at different angles
    Es = zeros(size(D));
    for thet = 1:length(D)
        Es(thet) = getGem2015ScreenEnergy(x_ax(pix), 1, 2, divs(thet));
    end
    % Get the dispersion, dx_dE. Using the inverse rule here
    dx_dE = 1./gradient(Es);
    % And do the deconvolution
    E_ax(pix) = getGem2015ScreenEnergy(x_ax(pix), 1, 2, offset);
    dQ_dE(pix) = dQ_dx(pix)*sum( D.*dx_dE );
    % SO I don't think the distribution of angles is random, I think I need
    % to choose that based on the pixel difference, ie dy.
    ttemp(pix) = sum(D.*dx_dE);
end
figure(7); plot(ttemp); set(gca, 'YLim', [0 200]);
% And plot some things
hfig = figure(44); clf(hfig);
plot(x_ax, dQ_dx); 
xlabel('Distance from high energy edge of screen / mm');
ylabel('Charge / a.u.');
set(gca, 'XDir', 'reverse')
hfig = figure(45); clf(hfig);
set(gca, 'NextPlot', 'add'); %./max(dQ_dE)
nfac = max(dQ_dE(E_ax>700));
hh(1) = plot(E_ax, dQ_dE./nfac, 'Tag', 'Deconvolved'); 
dQ_dEs = dQ_dx'./gradient(fdat.shotdata.tracking_btrac(x_ax));
%hh(2) = plot(fdat.shotdata.tracking_btrac(x_ax), dQ_dx./max(dQ_dx), 'Tag', 'Simple'); 
hh(2) = plot(fdat.shotdata.tracking_btrac(x_ax), dQ_dEs./max(dQ_dEs), 'Tag', 'Simple'); 
xlabel('Energy / MeV');
ylabel('Charge / a.u.');
legend(hh, get(hh, 'Tag'));
set(gca, 'YLim', [1e-3 1.1], 'YScale', 'log');