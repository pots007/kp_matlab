% testPointingOffsetSubAperture.m
%
% Quick script to test whether using a subaperture gives less spot offset
% than full aperture
%
n_ims = 100;
cl_small = zeros(n_ims, 2);
cl_big = zeros(size(cl_small));
beamsizes = [0.05 0.95];
tconf = rand(n_ims,2)*3;
for k=1:2
    for i=1:n_ims
        diameter = 2;
        FFTspace = 2^9;
        NFax = linspace(-diameter*0.5,diameter*0.5,FFTspace);
        x = repmat(NFax, FFTspace, 1);
        y = x';
        mask = (x.^2 + y.^2 <= (diameter*0.5*beamsizes(k))^2);
        radius = sqrt((x.^2 + y.^2));
        theta = atan2(y,x);
        
        phasefront = 2*radius.*cos(theta)*tconf(i,1) + 2*radius.*sin(theta)*tconf(i,2);
        
        wavefront = double(mask).*exp(1i*phasefront);
        FFTspace = 2^12;
        spot = fft2(wavefront, FFTspace, FFTspace);
        spot = fftshift(spot);
        spot = abs(spot).^2;
        minedge = floor(FFTspace*0.45);
        maxedge = ceil(FFTspace*0.55);
        spotzoom = spot(minedge:maxedge,minedge:maxedge);
        imagesc(spotzoom);
        if k==1
            cl_small(i,:) = centroidlocationsmatrix(spotzoom);
        else
            cl_big(i,:) = centroidlocationsmatrix(spotzoom);
        end
        drawnow;
    end
end
%
fprintf('\n\n Over %i random pointing shots:\n', n_ims);
fprintf('Beam\tstd(x)\tstd(y)\n');
fprintf('Big \t%2.3f\t%2.3f\n', std(cl_big));
fprintf('Small\t%2.3f\t%2.3f\n', std(cl_small));
fprintf('B/S  \t%2.3f\t%2.3f\n', std(cl_big)./std(cl_small));