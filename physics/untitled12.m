% Try to reproduce the injection model from Islam et al

R = 4;
D = 0.2;
eta = 0.1;
rm = R + D;
xi_ = linspace(-R-2, R+2, 1000);
rt_ = linspace(0.001, R+2, 500);
[xi,rt] = meshgrid(xi_, rt_);
rb = sqrt(R^2 - xi.^2);
rs = sqrt(rm^2 - xi.^2);
rho0 = -1*ones(size(xi));
% Calculate source terms for front half of bubble
lm_xip = xi_>=0;
rho1p = -rho0(:,lm_xip).*(R^2-xi(:,lm_xip).^2)./(rm^2-R^2);
rho2p = 0*rho0(:,lm_xip);
% Calculate source terms for back half of bubble
lm_xin = xi_<0;
rho1n = -rho0(:,lm_xin).*(R^2 - (1-eta)*xi(:,lm_xin).^2)./(rm^2-R^2);
rho2n = rho0(:,lm_xin)*eta;
rho0 = rho0.*(rt<rb);
rho1 = [rho1n rho1p].*(rb<=rt & rt<rs);
rho2 = [rho2n rho2p].*(rs<=rt & rt<rm);
rho = rho0+rho1+rho2;
imagesc(xi_, rt_, rho);
% Calculate pseudopotential
C1 = (rho1.^2-rho0.^2).*rb.^2/4;
C2 = rho2*rm.^2/4;
D0 = 2*C1.*log(rs./rb);
D1 = 2*C2.*log(rm./rs) - C1;
Phi0 = -rho0.*rt.^2/4.*(rt<rb);
Phi1 = (-rho1.*rt.^2/4 - 2*C1.*log(rs./rt) - D1).*(rb<=rt & rt<rs);
Phi2 = (-rho2.*rt.^2/4 - 2*C2.*log(rm./rt) + C2).*(rs<=rt & rt<rm);
Phi = Phi0+Phi1+Phi2;

imagesc(xi_, rt_, Phi); set(gca, 'CLim', [-.1 1])
