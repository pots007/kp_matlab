% tod_FFT_play.m
%
% Try to see what the phase of the E-field would look like in temporal
% space

syms w
syms w0
syms w2
syms w3
syms dw
syms t
syms a
syms tau

%S = exp(- (w-w0)^2/dw^2)*exp(1j*(w2*(w-w0)^2+w3*(w-w0)^3));

Et = exp(-t.^2 * a);% * exp(1j*w*x);

fourier(Et, t, w)
 