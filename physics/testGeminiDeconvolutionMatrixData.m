
% function deconvolveGemini2015Espec(fname, deconvmatrix)

if ismac
    datfol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150908r008';
    %datfol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150903r002';
elseif ispc
    datfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', '20150908r008');
end

flist = dir(fullfile(datfol, 'niceFigs', '*Espec1.mat'));

fdat = load(fullfile(datfol, 'niceFigs', flist(4).name));

hfig = figure(911); clf(hfig);
locs = GetFigureArray(1, 3, [0.06 0.03 0.1 0.13], 0.1, 'down');
ax(1) = axes('Parent', hfig, 'Position', locs(:,1));
imagesc(fdat.shotdata.image, 'Parent', ax(1));
title('Background subtracted signal region on screen');
set(ax(1), 'XTick', [], 'YTick', []);

% Plot the signal
ax(2) = axes('Parent', hfig, 'Position', locs(:,2));
set(gca, 'NextPlot', 'add');
%spec_line = smooth(sum(fdat.shotdata.image, 1));
spec_line = fdat.shotdata.dQ_dx;
scr_loc = fdat.shotdata.alongscreen;
dx = diff(scr_loc); dx = abs([dx(1) dx]);
spec_line = smooth(spec_line./dx);
fdatt = load('GemTrackMat');
logm = ~isnan(fdatt.GemDeconvMat.peakTracking);
track_spline = fit(fdatt.GemDeconvMat.peakTracking(logm)', fdatt.GemDeconvMat.energies(logm)', 'spline');
enaxis0 = track_spline(fdat.shotdata.alongscreen);
%dE = diff(enaxis0); dE = [dE(1) dE'];
%spec_line = spec_line./dE.*enaxis0';
h1 = plot(ax(2), scr_loc, spec_line, '--.');
% Set up a linear scr_vec, and interpolate the spectrum onto that grid
scr_loc_lin = linspace(0,240,300);
spec_line_lin = interp1(scr_loc, spec_line, scr_loc_lin, 'spline');
spec_line_lin(spec_line_lin<0) = 0;
h2 = plot(ax(2), scr_loc_lin, spec_line_lin, '-r');
set(ax(2), 'XDir', 'reverse');
legend([h1 h2], {'Data', 'Interpolation'}, 'Location', 'best');
xlabel('Distance from edge of screen / mm');
ylabel('$\mathrm{d}Q/\mathrm{d}x$');

% Now try to deconvolve the spectrum!
A = sparse(fdatt.GemDeconvMat.mat);
alpha = 10; gamma = alpha*eye(size(A));
tikh_x = (A'*A + gamma'*gamma)^(-1)*A'*spec_line_lin';
retr_ens = tikh_x.*fdatt.GemDeconvMat.energies';
ax(3) = axes('Parent', hfig, 'Position', locs(:,3));
set(ax(3), 'NextPlot', 'add');
hh(1) = plot(ax(3), fdatt.GemDeconvMat.energies, retr_ens, '-k');
spec_line(spec_line<0) = 0;
hh(2) = plot(ax(3), enaxis0, spec_line*1e1, '--r');
legend(hh, {'Data', 'Deconv'}, 'Location', 'best');
xlabel('Energy / MeV'); ylabel('Signal / a.u.');
make_latex(hfig); setAllFonts(hfig, 18);

% Plot the retrieved one alongside data
plot(ax(2), fdatt.GemDeconvMat.scrbins, fdatt.GemDeconvMat.mat*retr_ens*0.1, '--k');