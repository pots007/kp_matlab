% Let's run through a few different impact factors and calculate
% hamiltonians

fin.R0 = 5;
fin.Rend = 1.2*fin.R0;
fin.eps = 0.00;
fin.d = 0.1;
fin.tend = (fin.Rend/fin.R0-1)/fin.eps;
gammap = 30.1;
fin.betap = sqrt(1 - 1/gammap^2);
fin.z0 = 2;
fin.pz0 = 0;
fin.tfin = 1000;

% Write out the potential function, from Kostyukov 2004
phi0 = @(r,d) 1 +r.^2/4 -d.^2*pi^2/48 -0.25*r.*d.*log(1+exp(2*r./d)) -0.125*dilog(1+exp(2*r./d));

hfig = figure(112); clf(112);
set(hfig, 'Position', [100 100 800 600], 'Color', 'w');
locs = GetFigureArray(2, 2, 0.07, 0.06, 'across');
ax = [];
tt = 0:0.01:(2*pi);
it = 0;
for y0 = .4%:0.1:5.5
    it = it+1;
    clf(hfig); h = [];
    fin.y0 = y0;
    f = ExpandingBubbleHamiltonianCalc2(fin);
    gamma = sqrt(1+f.r(:,4).^2 + f.r(:,2).^2);
    % f.r is z, pz, y, py
    ax(1) = axes('Parent', hfig, 'Position', locs(:,1), 'NextPlot', 'add', 'Box', 'on');
    plot(ax(1), f.r(:,1), f.r(:,3), '.-r')
    plot(ax(1), fin.R0*cos(tt), fin.R0*sin(tt), '-.k')
    plot(ax(1), fin.R0*(1+fin.tend*fin.eps)*cos(tt), fin.R0*(1+fin.tend*fin.eps)*sin(tt), '--k');
    set(ax(1), 'XLim', [-11 11], 'YLim', [-11 11]);
    xlabel('$\xi$'); ylabel('y');
    % Now try to calculate the Hamiltonian
    H = zeros(size(f.t));
    tbub = f.t; tbub(tbub>fin.tend) = fin.tend;
    for i=1:length(f.t)
        r2 = f.r(i,1)^2 + f.r(i,3)^2;
        if fin.eps~=0
            if f.t(i)<fin.tend
                R0 = fin.R0*(1 + fin.eps*f.t(i));
            else
                R0 = fin.Rend;
            end
        else
            R0 = fin.R0;
        end
        phi = -(r2-R0^2)/8*(r2<=R0^2); % Calc phi, 0 outside the bubble!
        H(i) = gamma(i) - fin.betap*(f.r(i,2) + phi) - phi;
    end
    ax(2) = axes('Parent', hfig, 'Position', locs(:,2), 'NextPlot', 'add', 'Box', 'on');
    h(1) = plot(ax(2), f.t, f.r(:,2));
    ylabel(ax(2), '$p_z$'); xlabel(ax(2), 't');
    ax(3) = axes('Parent', hfig, 'Position', locs(:,3), 'NextPlot', 'add', 'Box', 'on');
    %plot(ax(3), f.t, gamma - fin.betap*f.r(:,2));
    h(2) = plot(ax(3), f.t, H); xlabel(ax(3), 't');
    ylabel(ax(3), 'H')
    ax(4) = axes('Parent', hfig, 'Position', locs(:,4), 'NextPlot', 'add', 'Box', 'on');
    h(3) = plot(ax(4), f.t, f.r(:,4));
    ylabel(ax(4), '$p_y$'); xlabel(ax(4), 't');
    title(ax(1), sprintf('$\\epsilon=%1.3f;\\:\\gamma_p = %2.1f;\\:d=%2.2f;\\:y_0=%2.2f$',...
        fin.eps, gammap, fin.d, y0));
    make_latex(hfig); setAllFonts(hfig, 10);
    set([ax h], 'LineWidth', 2); 
    drawnow;
    % export_fig(sprintf('fig_%2.2f', y0), '-png', '-nocrop', hfig);
    %export_fig(sprintf('fig_%04i', it), '-png', '-nocrop', hfig);
end
linkaxes(ax(2:4), 'x');
