%Based on Austin et al, Appl Opt. 48(19), 2009
%Script to calculate all the relevant angles and distances in the paper


%all distances in mm;
lambda0 = 400e-6;
Rc = 200;
Rf = 200;
thetC = 7;
thetF = 8;
D = 300; %lines per mm
alpha = -4;
beta = asind(sind(alpha) + D*lambda0);
Lcg = 110;

%Eq 6
cab = (cosd(alpha)/cosd(beta))^2;
Lsc = 0.5*Rc*Rf*(cab-1) / (Rc*(secd(thetF)-cosd(thetF))+Rf*(cab*secd(thetC)-cosd(thetC)))
%Eq 4
Lsc = 75;
Lfd = Rc*Rf*Lsc/(2*Lsc*(Rc*cosd(thetF)+Rf*cosd(thetC))-Rc*Rf);

%Eq 23
dSs_dthetF = 2*Lfd*Lsc*Rc*sind(thetF)/(2*Lsc*(Rc*cosd(thetF)+Rf*cosd(thetC))-Rc*Rf);
%Eq 24
dSt_dbeta = -2*Lfd*Rf*(2*Lsc*secd(thetC)-Rc)*cosd(alpha)^2*tand(beta)*secd(beta)^2;
dSt_dbeta = dSt_dbeta/(2*Lsc*(Rc*secd(thetF)+Rf*cab*secd(thetC))-Rc*Rf*cab);
%Eq 25
dSt_dthetF = -2*Lfd*Lsc*Rc*secd(thetF)*tand(thetF);
dSt_dthetF = dSt_dthetF/(2*Lsc*(Rc*secd(thetF)+Rf*cab*secd(thetC))-Rc*Rf*cab);
%Eq 8 and Eq 9 combined
dthetF_dbeta = dSt_dbeta/(dSs_dthetF - dSt_dthetF);

%Eq 16
Lgf = Rf*cosd(thetF)*(1-dthetF_dbeta);

%Eq 21 equaling Eq 8 for thetD
thetD = (dSs_dthetF*dthetF_dbeta+Lgf*tand(thetF))/(Lgf+Lfd-2*Lgf*Lfd/(Rf*cosd(thetF)));
thetD = atand(thetD);

figure(77);
cla;
set(gca, 'Position', [0.1 0.1 0.65 0.85], 'FontSize', 16);
quiver(0,0, Lsc, 0, 1, 'LineWidth',2);
grid minor;
hold on;
quiver(Lsc,0, -Lcg*cosd(2*thetC), -Lcg*sind(2*thetC),1, 'LineWidth',2);
x0 = Lsc-Lcg*cosd(2*thetC);
y0 = -Lcg*sind(2*thetC);
quiver(x0, y0, Lgf*cosd(-beta+alpha), Lgf*sind(-beta+alpha),1,...
    'LineWidth',2);
x0 = x0 + Lgf*cosd(-beta+alpha);
y0 = y0 + Lgf*sind(-beta+alpha);
quiver(x0, y0, -Lfd*cosd(2*thetF), -Lfd*sind(2*thetF),1, 'LineWidth',2);
hold off;
tbox = uicontrol('style', 'text', 'units', 'normalized', ...
    'Position', [0.75 0.1 0.2 0.85], ...
    'String', sprintf('L_sc=%2.1f mm\n', Lsc), ...
    'FontSize', 16);
parameters = {sprintf('Lines/mm=%i\n', D),...
    sprintf('L_sc=%2.1f mm\n', Lsc),...
    sprintf('theta_C=%2.1f\n', thetC),...
    sprintf('L_cg=%2.1f mm\n', Lcg),...
    sprintf('alpha=%2.1f\n', alpha),...
    sprintf('beta=%2.1f\n', beta),...
    sprintf('L_gf=%2.1f mm\n', Lgf),...
    sprintf('theta_F=%2.1f\n', thetF),...
    sprintf('L_fd=%2.1f mm\n', Lfd),...
    sprintf('theta_D=%2.1f\n', thetD)};
set(tbox, 'String', parameters);