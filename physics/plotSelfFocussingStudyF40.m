% plotSelfFocussingStudyF40.m
%
% Try to simulate the focal spot size as a function of distance and whatnot
%
% Based on Sprangle, IEEE 1987

function plotSelfFocussingStudyF40
Constants;
global fstat;
a0s = [1 2 4 6]*sqrt(2);
Rs = 1e-6*61.5./a0s; % 1.05 is a "spot is shit" factor
% And add the "elliptical spot":
eps = 1.3;
a0s(end+1) = a0s(1); a0s(end+1) = a0s(1);
Rs(end+1) = Rs(1)/sqrt(eps); Rs(end+1) = Rs(1)*sqrt(eps);
% Setting up graphics:
figpos = [100 100 1000 400];
hfig1 = figure(29); clf(hfig1); hfig1.Position = figpos;
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2)
ax1 = gca;
hfig2 = figure(291); clf(hfig2); hfig2.Position = figpos;
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2)
ax2 = gca;
% Setting parameters:    
fstat.ne = 3e18;
k0 = 2*pi/8e-7;
z0 = 1e-3;
fstat.V0 = (2*c/(k0*Rs(1)^2*a0s(1)^2))^2;
fstat.alpha0 = (omegapcalc(fstat.ne)*a0s(1)*Rs(1)/(4*c))^2;
%
plota0 = [1 2 5 6];
h = [];
sols = {};
for i=plota0
    fstat.a0 = a0s(i);
    fstat.R0 = Rs(i);
    zR = pi*fstat.R0^2/8e-7;
    dR_dz = -fstat.R0*1/sqrt(1 + z0^2/zR^2)*(z0)/zR^2;
    dx_dt = dR_dz/(fstat.a0*fstat.R0)*c; %*1e17;
    %xin = 50e-6/(fstat.a0*fstat.R0);
    Rin = fstat.R0*sqrt(1+z0^2/zR^2);
    xin = Rin/(fstat.a0*fstat.R0);
    fstat.V0 = (2*c/(k0*fstat.R0^2*fstat.a0^2))^2;
    fstat.alpha = (omegapcalc(fstat.ne)*fstat.a0*fstat.R0/(4*c))^2;

    options = odeset('RelTol', 2.5e-10, 'AbsTol', 1e-10);    
    [t, Ro] = ode45(@Vfun, [0 1e-10], [xin; dx_dt], options);
    h(end+1) = plot(ax1, t*c*1e3, fstat.a0*fstat.R0*Ro(:,1)*1e6, 'LineWidth', 2);
    sols(end+1, :) = {t*c*1e3, 1./Ro(:,1)};
end
xlabel(ax1, 'z / mm'); ylabel(ax1, 'w / micron');
set(ax1, 'YLim', [0 70]);
labs = {};
for i=plota0; labs{end+1} = sprintf('$w_0 = %2.0f \\: \\mu m $', Rs(i)*1e6); end;
legend(h, labs, 'Location', 'EastOutside');
make_latex(hfig1); setAllFonts(hfig1, 16);

plot(ax1, [0 30], 1e6*2^(7/6)*fstat.alpha^(1/6)*c/omegapcalc(fstat.ne)*[1 1], '--k', 'LineWidth', 2);
fprintf('P/Pc = %2.1f\n', fstat.alpha);
% And now plot the a0s:
hh(1) = plot(ax2, sols{1,1}, sols{1,2});
hh(2) = plot(ax2, sols{2,1}, sols{2,2});
tcommax = 0:0.001:30;
wx = interp1(sols{3,1}, sols{3,2}, tcommax);
wy = interp1(sols{4,1}, sols{4,2}, tcommax);
hh(3) = plot(ax2, tcommax, sqrt(wx.*wy));
labs{3} = 'Elliptical';
legend(hh, labs(1:3), 'Location', 'EastOutside');
set(hh, 'LineWidth', 2);
xlabel(ax2, 'Propagation / mm'); ylabel('$a_0$');
make_latex(hfig2); setAllFonts(hfig2, 16);

function dd = Vfun(~,xx)
global fstat;
dd = zeros(2,1);
dd(1) = xx(2);
x = xx(1);
rt = sqrt(1+1/x^2);
dd(2) = fstat.V0*(1/x^3 - 16*fstat.alpha*x*(rt - 1 +2*log(2) - 2*log(rt+1)));
