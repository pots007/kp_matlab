fstate = load('DESY2017_Espec3_BO_180A_Settings');

fstate.dat.Rin = []; fstate.dat.pin = [];
fstate.dat.noe = 250;
fstate.dat.Emin = 40; fstate.dat.Emax = 225;
fstate.dat.espace = 1; % 1 for log space
fstate.dat.ediv = [];
% For a beam of FWHM width of 1 mrad, find the 1/e width:
FWHM_width = 12;
sigma_theta = FWHM_width/(2*sqrt(log(2)));
% Values fall below 2% at +- 2sigma - should be OK for now
% Thus we sample from -2*sigma to +2*sigma, a total of 15 values:
offset = 0; npnts = 100;
divs = linspace(-2*sigma_theta, 2*sigma_theta, npnts)+offset;

% Check whether this tracking has already been done, if so, load it;
% Fname is fret_Noe_Emin_Emax_Espace_offset_npnts_FWHM
fname = sprintf('frets_%i_%i_%i_%i_%f_%i_%f', fstate.dat.noe, fstate.dat.Emin, fstate.dat.Emax, ...
    fstate.dat.espace, offset, npnts, FWHM_width);
if exist([fname '.mat'], 'file');
    disp('Loading previously tracked file!');
    fdat = load([fname '.mat']);
    fret = fdat.fret;
else
    disp('Starting tracking');
    fret = cell(size(divs));
    for i=1:length(divs)
        fprintf('Tracking for angle %1.2f mrad: %i out of %i\n', divs(i), i, length(divs));
        fstate.dat.divoff = divs(i);
        fret{i} = track_electrons2(fstate.dat);
    end
    datfol = fullfile(getMatlabGitPath, 'DESY', 'tracking');
    save(fullfile(datfol, [fname '.mat']), 'fret');
end

%% Set up a matrix, where energy is along rows and divergences are along columns:
trackmap = zeros(length(divs), fstate.dat.noe);
% Screen location
screenID = 1;
yl = fstate.dat.(sprintf('y%i', screenID));
zl = fstate.dat.(sprintf('z%i', screenID));
Ll = fstate.dat.(sprintf('l%i', screenID));
thetl = fstate.dat.(sprintf('thet%i', screenID));

for k=1:length(divs)
    fprintf('Figuring out angle %i out of %i\n', k, length(divs));
    % Find intersection with screen i:
    % Endpoints of screen:
    a = [zl yl]; b = a + Ll*[cosd(thetl) sind(thetl)];
    % Now select divergence, and then iterate through energies
    els = fret{k}.div.el;
    ppp = nan(2,length(els));
    for en = 1:length(els)
        % Look at this particular energy.
        R = els(en).r;
        [zi,yi] = intersections(R(:,3),R(:,2), [a(1) b(1)], [a(2) b(2)]);
        ppp(2,en) = els(en).energy*1.8712e21;
        if ~isempty(zi)
            ppp(1,en) = yi;
        end
    end
    if ~isempty(ppp)
        % Convert to along screen
        ppp(1,:) = (-yl + ppp(1,:))/sind(thetl);
    end
    trackmap(k,:) = ppp(1,:);
end

%% Now that we have the tracking results, set up a sensible range of pixels

% Take sensible as 1mm graduations on the espec screen:
scrBins = linspace(0,250,100); % This is from the HE edge of the screen!
energies = fret{1}.energies;
DESYTrackMat = 0.*ones(length(scrBins), length(energies));
% Now iterate over energies: set the intensity (or contribution) of each
% divergence angle to the appropriate value (based on it being Gaussian).
% Then find how much of a contribution it makes to the screen bins.
intTheta = 10*exp(-((divs-offset)./sigma_theta).^2);
intTheta = intTheta./sum(intTheta);
for en=1:size(trackmap,2)
    for thet=1:size(trackmap,1);
        % Is value is nan, we don't hit the screen at all!
        if isnan(trackmap(thet,en)); continue; end
        % Which bin on the screen do we hit?
        [~, binID] = min(abs(trackmap(thet,en) - scrBins));
        DESYTrackMat(binID, en) = DESYTrackMat(binID,en) + intTheta(thet);
    end
end
hfig = figure(9); clf(hfig);
colormap(jet_white(128));
%imagesc(energies, scrBins, GemTrackMat);
pcolor(energies, scrBins, medfilt2(DESYTrackMat)); shading flat;
set(gca, 'Box', 'on', 'Layer', 'top', 'LineWidth', 2);
xlabel('Electron energy / MeV'); ylabel('Position on screen / mm');
cb = colorbar;
ylabel(cb, 'Signal strength / a.u.');
make_latex(hfig); setAllFonts(hfig, 16);
DESYDeconvMat.mat = DESYTrackMat;
DESYDeconvMat.energies = energies;
DESYDeconvMat.scrbins = scrBins;
DESYDeconvMat.peakTracking = trackmap(round(0.5*size(trackmap,1)),:);
DESYDeconvMat.fname = [fname '_' num2str(length(scrBins))];
save(fullfile(getMatlabGitPath, 'DESY', 'tracking', 'DESYTrackMat'), 'DESYDeconvMat');