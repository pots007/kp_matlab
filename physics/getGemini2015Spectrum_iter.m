%function fout = getGemini2015Spectrum_iter(fname, plotflag)

plotflag = 1;
fname = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150908r008/niceFigs/20150908r008s038_Espec1.mat';
fname = fullfile(getGDrive, 'Experiment', '2015_Gemini', 'EspecAnalysis', '20150908r008',...
    'niceFigs', '20150908r008s038_Espec1.mat');
fdatt = load('GemTrackMat');

fdat = load(fname);
spec_line = fdat.shotdata.dQ_dx;
scr_loc = fdat.shotdata.alongscreen;
dx = diff(scr_loc); dx = abs([dx(1) dx]);
% Why did I do this?????
%spec_line = smooth(spec_line./dx);
spec_line = smooth(spec_line);

% Now find the energy valeus of each mm on screen
logm = ~isnan(fdatt.GemDeconvMat.peakTracking);
track_spline = fit(fdatt.GemDeconvMat.peakTracking(logm)', fdatt.GemDeconvMat.energies(logm)', 'spline');
enaxis0 = track_spline(fdat.shotdata.alongscreen);

% Set up a linear scr_vec, and interpolate the spectrum onto that grid
%scr_loc_lin = linspace(0,300,300); % Must be as long as that in makeGeminiDeConvolutionMatrix
%
%
scr_loc_lin = fdatt.GemDeconvMat.scrbins;
spec_line_lin = interp1(scr_loc, spec_line, scr_loc_lin, 'spline');
spec_line_lin(spec_line_lin<0) = 0; spec_line_lin(1:5) = 0;

%%

A = sparse(fdatt.GemDeconvMat.mat);
% These two lines are for testing out the retrival code
test_spec = 1*exp(-(energy_ax-1500).^2/30^2) + .0;
%spec_line = test_spec;
test_spec = (energy_ax>1900 & energy_ax < 2400)*10;
%spec_line_lin = (A*test_spec')';
% Tikhonov regularisation

alpha = 10; gamma = alpha*eye(size(A));
tikh_x = (A'*A + gamma'*gamma)^(-1)*A'*spec_line_lin';
retr_ens = tikh_x.*fdatt.GemDeconvMat.energies';

energy_spec = smooth(retr_ens); % The guess we have
energy_ax = fdatt.GemDeconvMat.energies;
screen_data = (smooth(spec_line_lin));

screen_ax = scr_loc_lin;

A = fdatt.GemDeconvMat.mat;

% Now try to iterate.... 

hfig = figure(19); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [100 100 900 600], 'Visible', bool2onoff(plotflag));
locs = GetFigureArray(3,2,0.05, 0.05, 'down'); ax = [];
titls = {'Data on screen',  'Guess/screen data', 'Retrieved energy + initial guess',...
    'Guess - screen data', 'Applied difference', 'Error'};
for i=1:6
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'Box', 'on');
end
set(ax(6), 'NextPlot', 'add');

screen_data = screen_data./max(screen_data);
screen_data = screen_data./max(screen_data);
plot(ax(1), screen_ax, screen_data);
dE = diff(energy_ax); dE = [dE(1) dE]';
dE0 = diff(enaxis0); dE0 = [dE0(1) dE0']';
err_prog = [];
linkaxes(ax([1,2,4]), 'x');
for i=1:500
    if i==1; energy_curr = energy_spec; end
    %en_scr = smooth(A*energy_curr, 2);
    en_scr = A*energy_curr;
    en_scr = en_scr./max(en_scr);
    tikh_x = (A'*A + gamma'*gamma)^(-1)*A'*(screen_data-en_scr);
    retr_ens = tikh_x.*energy_ax';
    % Try to weed out negative values
    retr_ens(retr_ens<0) = retr_ens(retr_ens<0)*0.05;
    energy_curr = energy_curr + 1*(retr_ens);
    % Ensure it's above zero!!!!
    %energy_curr = abs(energy_curr);
    if (i>50)
        fac = en_scr./screen_data; fac(isinf(fac) | isnan(fac)) = 1;
        %energy_curr = energy_curr + fac.*retr_ens*0.05;
    end
    energy_curr(energy_curr<0) = 0;
    % Plot things
    
    %plot(ax(2), energy_ax, energy_spec, '-k', energy_ax, energy_curr, '--b');
    plot(ax(3), energy_ax, energy_spec./sum(energy_spec.*dE), '-k',...
        energy_ax, energy_curr./sum(energy_curr.*dE), '--b',...
        enaxis0, spec_line./sum(spec_line.*dE0), '--r');
     %energy_ax, test_spec*0.0005, '--r');
    set(ax(3), 'XLim', [0 3200]);
    plot(ax(2), screen_ax, (en_scr+0)./(screen_data+0)-0, [0 250], [1 1]); set(ax(2), 'YLim', [-0.5 2.5]);
    plot(ax(4), screen_ax, abs(en_scr-screen_data));
    energy_spec_ = energy_spec./dE;
    %plot(ax(5), energy_ax, energy_spec_./(sum(energy_spec_)), '-k', energy_ax, energy_curr./(sum(energy_curr)), '--b');
    plot(ax(5), energy_ax, retr_ens);
    plot(ax(1), screen_ax, en_scr, '--r', screen_ax, screen_data);
    err_prog(i) = sum((en_scr-screen_data).^2);
    if i==1;
        hprog = plot(ax(6), i, sum(en_scr-screen_data), 'xk');
    else
        set(hprog, 'XData', 1:i, 'YData', err_prog);
    end
    title(ax(6), ['Error, min= ' num2str(min(err_prog))]);
    set(ax([1 2 3]), 'YTick', []);
    set(ax([1 2 4]), 'XDir', 'reverse');
    set(ax(1:2:6), 'XAxisLocation', 'top');
    for ii=1:5; title(ax(ii), titls{ii}); end;
    drawnow;
    if sum(diff(err_prog)>0)>25; 
        fprintf('Breaking on it %i, min err %f\n', i, min(err_prog));
        break; 
    end;
    pause(0.0001)
end

if ~plotflag; close(hfig); end;