% calcParabolaAngleChange.m
%
% Calculate the change in angle of the beam as one translates a parabola
% along the focussing beam direction.

f = 0.5; % Focal length in m
theta = 17.5; % Off axis angle, in degrees
dt = 4e-3; % Translation, in m

% This is all very dirty and lazy and filthy...
% Basically, say the focussin beam propagates along (1,0), ie in the
% positive x direction and focus is at (0,0). This means the beam 
% hits it at (-f, 0) to focus there. A beam along the parabola axis
% will also focus f away, and a beam would need to hit the parabola at
% (-f cos(theta), -f sin(theta)). Similarly for a beam coming on the other
% side of focus would hit it at (-f cos(2theta), -f sin(2theta)). Now just
% fit a curve through these points....

pp = [-1, 0; -cosd(theta), -sind(theta); -cosd(2*theta), -sind(2*theta)]*f;
parab = polyfit(pp(:,1), pp(:,2), 2);

% The tangent equation to this parabola is 
parab_tan = [2*parab(1), parab(2)];

% Now, let's assume we move the whole parabola dt. The equation of the
% initial ray hitting the parabola and focussing at 0,0 is
incoming = [tand(theta), f*tand(theta)];

% Shifting the parabola by dt in x causes it's equation to change. Say it
% started as ax2+bx+c=y. With x->x0+xo, we get the new equation as ax0^2 +
% (2axo + b) + (axo^2+bxo+c) = y. So the curvature term does not change,
% but the others do:
parab_shift = parab + [0, -2*parab(1)*dt, parab(1)*dt^2-parab(2)*dt];

% Find the x-coordinates of intersection...
xs = quadratic(parab(1), parab_shift(2) - incoming(1), parab_shift(3)-incoming(2));

% Assume it's leftmost one, and find the position of the beam hitting it:
y = polyval(parab, xs(1));

% And print the angle of the new beam:
dtheta = atan2(y, f);

fprintf('For a shift of %1.1f mm, the angle changes by %1.2f mrad\n', dt*1e3, dtheta*1e3)