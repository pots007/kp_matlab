% Should work for magnet tracking of simulation outputs...
% dd(1) = dx/dt = vx
% dd(2) = dy/dt = vy
% dd(3) = dz/dt = vz
% dd(4) = dvx/dt = q/(gamma*m)*(vy*Bz - vz*By)
% dd(5) = dvy/dt = q/(gamma*m)*(vz*Bx - vx*Bz)
% dd(6) = dvz/dt = q/(gamma*m)*(vx*By - vy*Bx)

function dd = trackerLorentz(~,y)
% y(1:3) are positions; v(4:6) are momenta
c = 299792458; %m/s
qe = 1.60217687e-19; % C
me = 9.10938215e-31; %kg
gamma = sqrt(1+(sum(y(4:6).^2))/(me*c)^2);%1/sqrt(1-(sum(y(4:6).^2))/c^2);
dd = zeros(6,1);
dd(1) = y(4)/(me*gamma);
dd(2) = y(5)/(me*gamma);
dd(3) = y(6)/(me*gamma);
[Bx,By,Bz] = trackerB(y(1:3));
dd(4) = qe*(y(5)*Bz/(me*gamma) - y(6)*By/(me*gamma));
dd(5) = qe*(y(6)*Bx/(me*gamma) - y(4)*Bz/(me*gamma));
dd(6) = qe*(y(4)*By/(me*gamma) - y(5)*Bx/(me*gamma));

end


