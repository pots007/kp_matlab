% calcPhaseMatchBanwidth.m
%
% For a given material, calculates the phasematching bandwidth.
%
% CALCPHASEMATCHBANDWIDTH(lambda0, L, material)
%           lambda0 - central wavelength of fundamental, in microns
%           L - crystal thickness, in microns
%           material - name of material.

function dl = calcPhaseMatchBandwidth(lambda0, L, material)

Constants;
%lambda0 = 0.8;
%L = 100;
%material = 'BBO';

no = @(l,pol) RefIndex(2*pi*c./l*1e6, material, 'o');
ne = @(l,pol) RefIndex(2*pi*c./l*1e6, material, 'e'); 
dl = 0.44*lambda0./L./(fprime(no, lambda0) - 0.5*fprime(ne, 0.5*lambda0));
dl = dl*1e3; % Return the answer in nm!!!