% Try out matlab inbuilt GA to calculate electron spectrum...

% We require a minimization function: we'll use a very simple difference
% for now

testGeminiDeconvolutionMatrixData;
% This gives us the retrieved energy guess, retr_ens

% So we'll set up the minimisation function to simply minimise the forward
% transform of the spectrum against data:
%%
energy_spec = smooth(retr_ens); % The guess we have
energy_ax = fdatt.GemDeconvMat.energies;
ener_fit = fit(energy_ax', smooth(retr_ens), 'spline');
%energy_spec = ones(size(retr_ens));
screen_data = (smooth(spec_line_lin));

screen_ax = scr_loc_lin;
scr_fit = fit(screen_ax', screen_data, 'cubicinterp');
A = fdatt.GemDeconvMat.mat;
figure(4);
plot(screen_ax, screen_data, screen_ax, scr_fit(screen_ax), '--k');
%plot(energy_ax, energy_spec, energy_ax, ener_fit(energy_ax), '--k');

%%
%fitnessFun = @(ens) sum(abs(A*ens'-screen_data).*(screen_ax'<100));
tic
screen_ax(1) = screen_ax(2);
fitnessFun = @(ens) sum((((A*ens')-screen_data)).^2);%.*(screen_ax'<100));
%fitnessFun = @(ens) sum((smooth(smooth(A*ens')-screen_data).*(screen_ax'<100)).^2);
%fitnessFun = @(ens) sum( ((A*ens') - screen_data).^2./screen_ax'.^2);
opts = gaoptimset('TimeLimit', 60, 'MutationFcn', {@mutationuniformKP, 0.1}, 'TolCon', 1e-7);
%opts = optimoptions('ga', 'MaxTime', 100, 'MutationFcn', {@mutationuniform, 0.05})
GAx = ga(fitnessFun, length(energy_spec), [], [], [], [], zeros(size(energy_spec)), [], [],[],opts);
 
%GAx = fminsearch(fitnessFun, energy_spec');
toc
plot(fdatt.GemDeconvMat.scrbins, smooth(A*GAx'), screen_ax, screen_data, '--k')