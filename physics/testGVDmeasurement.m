% testGVDmeasurement.m
%
% Testing out the effect of no-AR coating on the generation of spectral
% fringes. Also useful to test the timing and see what the smallest timing
% change we can see is.

lambdaC = 400e-9;
dlambda = 10e-9;
omegaC = 2*pi*c/lambdaC;
domega = 2*pi*c*dlambda/lambdaC^2;
N = 2^11;
omax = linspace(0.75*omegaC, 1.25*omegaC, 2^11);
amp = exp(-(omax-omegaC).^2/(domega)^2);

L = 1e-3;
n_O = @(w) RefIndex(w, 'Fused silica', 'O')*w*L/c;

dt = fprime(n_O, omegaC) - fprime(n_O, omega0);

E1 = amp.*exp(1j*(omax-omegaC)*dt);
E2 = amp;
E3 = amp*0.04^2.*exp(1j*(omax-omegaC)*2*dt);

% All fields.
fullE = E1 + E2 + E3;

plot(omax, abs(E1+E2+E3))

% And fft to see what happens
tt = fftX(omax, abs(fullE), N);
plot(tt(:,1),abs(tt(:,2)))