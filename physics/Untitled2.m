% Try to automate backtracking angle finding by minimising the difference
% between two traces

% some test data

%function FF = btracAutomTest(theta, enreg)
theta = 1;
datfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', ...
    '20150904r007', 'mat_manual');
f1 = load(fullfile(datfol, '20150904r007s034_Espec1.mat'));
f2 = load(fullfile(datfol, '20150904r007s034_Espec2LE.mat'));

sigg1 = [f1.shotdata.alongscreen; f1.shotdata.dQ_dx];
sigg2 = [f2.shotdata.alongscreen; f2.shotdata.dQ_dx];

tic
for i=1:length(sigg1)
    enax1(i) = getGem2015ScreenEnergy(sigg1(1,i), 1, 2, theta);
end
for i=1:length(sigg2)
    enax2(i) = getGem2015ScreenEnergy(sigg2(1,i), 3, 2, theta);
end
toc

% Find indices of energy region
enreg = [45 55; 100 130];
figure(4);
plot(enax1, sigg1(2,:), '-r', enax2, smooth(sigg2(2,:)), '-k');
plot(sigg1(1,:), sigg1(2,:), '-r', sigg2(1,:), smooth(sigg2(2,:)), '-k');
