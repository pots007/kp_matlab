function [Bx,By,Bz] = trackerB(r)
x = r(1); y = r(2); z = r(3);
Bx = 0.;
By = 0.;
Bz = 0.;
%Bz = exp(-(0.5-x).^8/0.04^8)*exp(-y.^8/0.01^8)*0.5;
if x>0.657 && x<0.727 && abs(y)<0.04 && abs(z)<0.05
    Bz = 0.7;
end
end