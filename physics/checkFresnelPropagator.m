% checkFresnelPropagator.m
% 
% A few sanity checks to ensure the fresnel propagator actually does what
% we think it does.
%
% These include a far-field image for a Gaussian and a top-hat beam, also
% ensuring the phase stays smooth all the way thorugh.

%% Part one, Gaussian beam propagation

hfig = figure(8970); clf(hfig);

lambda0 = 800e-9;
k = 2*pi/lambda0;
npnts = 2^10;
imGrid = linspace(-5, 5, npnts)*1e-3;
dx = mean(diff(imGrid));
[X,Y] = meshgrid(imGrid, imGrid);
R = sqrt(X.^2 + Y.^2);
w0 = 0.1e-3;
zR = pi*w0^2/lambda0;
% This is where we start the beam at:
z0 = -500e-3;
Rz = z0*( 1 + (zR/z0)^2 );
wz = w0*sqrt( 1 + (z0/zR)^2 );

E_gauss = exp(-R.^2/wz^2).*exp(-1j*k*R.^2/(2*Rz));

E_after = fresnelPropagator(E_gauss, dx, dx, -501e-3, lambda0);
imagesc(imGrid, imGrid, abs(E_after));
