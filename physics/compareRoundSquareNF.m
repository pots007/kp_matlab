% Have a look at total energy in the central spot of round and square
% pulses

D0 = 0.15;
f = 6;
fno = f/D0;
k0 = 2*pi/800e-9;
E0 = 15;
tau = 30e-15;
phaseerror = 1/20; % in lambda0

equalInt = false;

hfig = figure(78); clf(hfig);
locs = GetFigureArray(2,2, 0.07, 0.08, 'across');
x_ax = linspace(-0.2, 0.2, 1024); dx = mean(diff(x_ax));
[X,Y] = meshgrid(x_ax, x_ax);
% Round, top hat beam
% Random phase 
phase_er = rand(1024)*2*pi*phaseerror;

beam_c = X.^2 + Y.^2 < (D0/2)^2;
beam_r = abs(X)<(0.5*D0) & abs(Y)<(0.5*D0);

if equalInt 
    beam_I_frac = 1;
else
    beam_I_frac = sum(beam_r(:))/sum(beam_c(:));
end
beam_r = beam_r./sum(beam_r(:))*E0*beam_I_frac;
beam_c = beam_c./sum(beam_c(:))*E0;

% And normalise such that both of them have the same amount of energy...


%
ax(1) = axes('Parent', hfig, 'Position', locs(:,1), 'XAxisLocation', 'top');
imagesc(x_ax, x_ax, beam_c, 'Parent', ax(1));
set(ax(1), 'XAxisLocation', 'top');
ax(2) = axes('Parent', hfig, 'Position', locs(:,2));
imagesc(x_ax, x_ax, beam_r, 'Parent', ax(2));
set(ax(2), 'XAxisLocation', 'top', 'YAxisLocation', 'right');


npnts = 2^13;
lims = [-100 100];
ff_ax = f/k0*linspace(-pi/dx, pi/dx, npnts)*1e6; dff_ax = mean(diff(ff_ax));
spot_c = fftshift(fft2(beam_c.*exp(1i*phase_er), npnts, npnts));
spot_c_I = spot_c.*conj(spot_c);
spot_c_I = spot_c_I/max(spot_c_I(:))*sum(beam_c(:))*pi*0.25*D0^2/(tau*0.8e-7^2*fno^2)*1e-4;
ax(3) = axes('Parent', hfig, 'Position', locs(:,3), 'NextPlot', 'add');
imagesc(ff_ax, ff_ax, spot_c_I, 'Parent', ax(3));
colorbar
% And plot horizontal lineout
lineout = spot_c_I(round(npnts*0.5),:);
width_c_I = width1ee(lineout)*dff_ax;
lineout = lineout./max(lineout)*(lims(2)-lims(1)) + lims(1);
plot(ax(3), ff_ax, lineout, '-r', 'LineWidth', 1.5);


spot_r = fftshift(fft2(beam_r.*exp(1i*phase_er), npnts, npnts));
spot_r_I = spot_r.*conj(spot_r);
spot_r_I = spot_r_I/max(spot_r_I(:))*sum(beam_r(:))*pi*0.25*D0^2/(tau*0.8e-7^2*fno^2)*1e-4;

ax(4) = axes('Parent', hfig, 'Position', locs(:,4), 'NextPlot', 'add');
imagesc(ff_ax, ff_ax, spot_r_I, 'Parent', ax(4));
colorbar
% And plot horizontal lineout
lineout = spot_r_I(round(npnts*0.5),:);
width_r_I = width1ee(lineout)*dff_ax;
lineout = lineout./max(lineout)*(lims(2)-lims(1)) + lims(1);
plot(ax(4), ff_ax, lineout, '-r', 'LineWidth', 1.5);

set(ax(3:4), 'XLim', lims, 'YLim', lims);
% Energy enclosed:
enfac = exp(-2);
en_c = sum(sum(spot_c_I(spot_c_I>enfac*max(spot_c_I(:)))))./sum(spot_c_I(:));
en_r = sum(sum(spot_r_I(spot_r_I>enfac*max(spot_r_I(:)))))./sum(spot_r_I(:));
title(ax(3),sprintf('$e^{-2} $ = %1.2f, $E/E_0=%2.1f$\\%%',width_c_I, 100*en_c))
title(ax(4),sprintf('$e^{-2} $ = %1.2f, $E/E_0=%2.1f$\\%%',width_r_I, 100*en_r))
make_latex(hfig); setAllFonts(hfig, 14);
