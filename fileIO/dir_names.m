% return cell array with list of names in struct

function out = dir_names(fol)

fillist = dir(fol);

if (isempty(fillist))
    out = [];
else
    out = cell(size(fillist));
    for i=1:length(fillist)
        out{i} = fillist(i).name;
    end
end

end