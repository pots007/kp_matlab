function follist = getFolderList(input)
fols = dir(input);
it = 1;
follist = [];
for i=1:length(fols)
    if ~isempty(strfind(fols(i).name, '.'))
        ind = strfind(fols(i).name, '.');
        if ind(1) == 1
            continue;
        end
    else
        follist{it} = fols(i).name;
        it = it+1;
    end
end
