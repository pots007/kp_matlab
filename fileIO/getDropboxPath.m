function dpath = getDropboxPath
genn = fileparts(which('make_path_Kris.m'));
ind = strfind(genn, 'MATLAB');
dpath = genn(1:ind-1);
if strcmp(getComputerName, 'flalap1-ubuntu')
    dpath = '/home/kpoder/Dropbox';
elseif contains(getComputerName, 'kristjans-macbook-pro')
    dpath = '/Users/kpoder/Dropbox';
end