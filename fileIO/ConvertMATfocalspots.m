dlist = dir();
for i=3:length(dlist)
    flist = dir([dlist(i).name '/*.mat']);
    if isempty(flist) continue; end
    for k=1:length(flist)
        load([dlist(i).name '/' flist(k).name]);
        len = size(raw.image,4);
        for l=1:len
            imwrite(raw.image(:,:,:,l), sprintf('%s/%s_%04i_raw.png',dlist(i).name, flist(k).name(1:end-4), l), 'png');
        end
    end
end