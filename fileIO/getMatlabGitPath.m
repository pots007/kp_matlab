% getMatlabGitPath.m
%
% Returns the base folder for all git code

function gpath = getMatlabGitPath
gpath = fileparts(which('make_path_Kris.m'));
