% getFilesInFolders.m
%
% Makes a folder structure with a txt files of files contained in each of
% the bottom most subfolders. Should be useful to restore files from Box,
% after a massive format.

inn = 'F:\';
dest = '\fstruct\';
[~, stt] = system(['dir /s /b /o:n /a:d ' inn], '-echo');
fols = strsplit(stt, char(10));
if ~exist(fullfile(inn, dest), 'dir'); mkdir(fullfile(inn,dest)); end;
% Now do the actual stuff
for f = 1:length(fols)-1
    if sum(strcmp(fols{f}(1), {'.', '$'})); continue; end;
    flist = dir(fols{f});
    % If no files, but only folders, skip
    if sum(cell2mat({flist.isdir}))==length(flist); continue; end;
    newf = fullfile(inn, dest, fols{f}(length(inn):end));
    mkdir(newf);
    foutn = fopen(fullfile(newf, 'cont.txt'), 'w');
    totlines = 0;
    for i=1:length(flist)
        if sum(strcmp(flist(i).name(1), {'.', '$'})) || flist(i).isdir; continue; end;
        fprintf(foutn, '%s\r\n', flist(i).name);
        totlines = totlines + 1;
    end
    fclose(foutn);
    % But if we didn't actually print anything, delete it.
    if totlines==0; delete(fullfile(newf, 'cont.txt')); end;
end