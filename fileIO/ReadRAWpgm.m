% ReadRAWpgm.m
%
% Function to read DOOCS specific pgm files.
%
% KP, DESY, 2017

function imm = ReadRAWpgm(fname)
fid = fopen(fname, 'r');
ll = char(fread(fid, 15, 'uint8'))';
strss = strsplit(ll);
% The first one is something, second and third are image sizes!
% Fourth one is bit depth...
w = str2double(strss{2});
h = str2double(strss{3});
bitdepth = str2double(strss{4});
if bitdepth==255
    imm = fread(fid, w*h, 'uint8');
    imm = reshape(imm, [w h])';
else
    error(['Not sure what to do with bitdepth ' num2str(bitdepth)]);
end
fclose(fid);