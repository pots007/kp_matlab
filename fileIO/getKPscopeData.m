% Returns the data in a better format
% Time is in column 1, traces in 2-5

function dat = getKPscopeData(dataf)
dat = [];
%dataf = 'E:\GeminiEakins2016\Powerlite pulse\trace20160311_13_42_16_249.dat';
if ~exist(dataf, 'file')
    disp('File doesn''t exist');
    return;
end
fid = fopen(dataf, 'r');
dat = fread(fid, 'double');
fclose(fid);
dat = reshape(dat, length(dat)/5, 5);
