%returns file text as cell array

function filetex = getFileText(filename)
fid = fopen(filename, 'r');
lines = 0;
while (~feof(fid))
   lin = fgetl(fid); 
   lines = lines+1;
end
frewind(fid);
filen = cell(lines,1);
for i=1:lines
   lin = fgetl(fid); 
   filen{i} = lin;
   %disp(lin)
end
fclose(fid);
filetex = filen;

end
