% ---=== VIDEO2FLV ===---
%
% AUTHORS
% 	Vincent Garcia and Sylvain Boltz
%
%
% DATE
%   August 3, 2007
%
%
% DESCRIPTION
% 	The proposed script is a Matlab script doing the following tasks.
% 	1. Convert a video (MPEG, WMV, AVI) into a Adobe Flash Video (FLV). The
% 	   conversion process is performed using the FFMPEG application.
% 	2. Creation of a webpage (HTML) where the created FLV video is displayed
% 	   into the flash video player developped by Jeroen Wijering. The user 
% 	   has only to copy the content of the output folder into its web page
% 	   and to modify the HTML file to adapt to its needs.
% 	The script is (I hope) easy to understand and can be easily tuned either
% 	for the encoding process (see http://ffmpeg.mplayerhq.hu) or for the
% 	use of the flash player (see http://www.jeroenwijering.com).
%   Please consult the README file for more precision about the license and
%   legal considerations.
%
% 
% HOW TO USE THE SCRIPT
%   1. Change the input and output parameters (folder and name).
%   2. You can also change the video options but we do not recommand it!!!
%   3. Execute the script.
%   4. Enjoy =)



% THE INPUT VIDEO
input_video_folder = '.\';
input_video_name   = 'test3.mp4';


% THE OUTPUT VIDEO
output_video_folder = '.\';
output_video_name   = 'test3.flv';


% ENCODING VIDEO
% options
v_w    = 320;       % video width e.g. [320]
v_h    = 240;       % video height e.g. [240]
v_b    = 100;       % video bitrate  e.g. [100]
v_fr   = 25;        % frame rate e.g. [25]
v_qmin = 2;         % minimum video quantizer scale e.g. [2]
v_qmax = 5;         % maximum video quantizer scale e.g. [5]
v_in   = [input_video_folder input_video_name];
v_out  = [output_video_folder output_video_name];
% encode
v_size = sprintf('%dx%d',v_w,v_h);
encode_video = sprintf('ffmpeg -i "%s" -vcodec flv -s %s -b %d -r %d -qmin %d -qmax %d "%s"',v_in,v_size,v_b,v_fr,v_qmin,v_qmax,v_out);
system(encode_video)


% ENCODING FIRST IMAGE
% options
i_out = [output_video_folder strtok(output_video_name,'.') '.jpg'];
% encode
encode_video = sprintf('ffmpeg -i "%s" -s %s -f image2 -vcodec mjpeg -vframes 1 %s',v_in,v_size,i_out);
system(encode_video);


% COPY FLV PLAYER INTO OUTPUTE FOLDER
copyfile('flvplayer.swf',[output_video_folder 'flvplayer.swf']);


% CREATION OF HTML WEBPAGE
h_out = [output_video_folder strtok(output_video_name,'.') '.htm'];
fid = fopen(h_out, 'wt');
fprintf(fid,'<html>\n<body>');
fprintf(fid,'<object width="%d" height="%d">\n',v_w,v_h);
fprintf(fid,'<param name="movie" value="flvplayer.swf" />\n');
fprintf(fid,'<embed src="flvplayer.swf" width="%d" height="%d" allowfullscreen="true" allowscriptaccess="always" flashvars="&displayheight=%d&file=%s&image=%s&height%d&width=%d&frontcolor=0xCCCCCC&backcolor=0x000000&lightcolor=0xFFFFFF" />\n',v_w,v_h,v_h,v_out,i_out,v_h,v_w)
fprintf(fid,'</object>\n');
fprintf(fid,'</body>\n</html>');
fclose(fid);