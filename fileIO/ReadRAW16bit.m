% ReadRAW16bit.m
%
% Reads AVT camera raw files, and the ones from XIMEA cameras, or AVT file
% version written by DESY stuff
%
% I = ReadRAW16bit(filename, width, height)
% Arguments:
%           filename - obvious, needs to be full path
%           width - Image width in pixels
%           height - Image height in pixels
% 
% I = ReadRAW16bit(..., 'DESY') - read out images written by the DOOCS
% servers. In this case, both height and width can be empty as the image
% size is written to the file.
%
% I = ReadRAW16bit(..., byteorder) - byteorder is a string of either 'le'
% for low-endian files or 'he' for high-endian


function I = ReadRAW16bit(filename, width, height, varargin)

bands = 1;
precision = 'uint16';
offset = 0;
interleave = 'bip'; %smth
byteorder = 'ieee-be'; % Default to big endian files
if contains(varargin, 'le')
    byteorder = 'ieee-le';
end
if contains(varargin, 'DESY')
    fid = fopen(filename);
    size_str = fgetl(fid);
    size_num = str2double(strsplit(size_str));
    fseek(fid, 15, -1);
    width = size_num(1); height = size_num(2);
    I = reshape(fread(fid, width*height*2, 'uint16'), width, height)';
    fclose(fid);
else
    I = multibandread(filename, [height width bands], precision, offset, interleave, byteorder);
    I = flipud(I);
end

end
