dlist = dir();
for i=3:length(dlist)
    flist = dir([dlist(i).name '/*.avi']);
    if isempty(flist) continue; end
    for k=1:length(flist)
        dat = VideoReader([dlist(i).name '/' flist(k).name]);
        len = dat.NumberOfFrames;
        disp([dlist(i).name '/' flist(k).name]);
        for l=1:len
            datl = read(dat,l);
            imwrite(datl(:,:,1), sprintf('%s/%s_%04i_raw.png',dlist(i).name, flist(k).name(1:end-4), l), 'png');
        end
    end
end