---=== VIDEO2FLV ===---

AUTHORS
	Vincent Garcia and Sylvain Boltz
	
DESCRIPTION
	The proposed script is a Matlab script doing the following tasks.
	1. Convert a video (MPEG, WMV, AVI) into a Adobe Flash Video (FLV). The
	   conversion process is performed using the FFMPEG application.
	2. Creation of a webpage (HTML) where the created FLV video is displayed
	   into the flash video player developped by Jeroen Wijering. The user 
	   has only to copy the content of the output folder into its web page
	   and to modify the HTML file to adapt to its needs.
	The script is (I hope) easy to understand and can be easily tuned either
	for the encoding process (see http://ffmpeg.mplayerhq.hu) or for the
	use of the flash player (see http://www.jeroenwijering.com).
	 
LEGAL
	1. The enclosed script can be freely reused under a Creative Commons
	   License (http://creativecommons.org/licenses/by-nc-sa/2.0) :
	   You are free:
		    * to Share � to copy, distribute and transmit the work
		    * to Remix � to adapt the work
		Under the following conditions:
		    * Attribution. You must attribute the work in the manner specified
			  by the author or licensor (but not in any way that suggests that
			  they endorse you or your use of the work).
		    * Noncommercial. You may not use this work for commercial purposes.
		    * Share Alike. If you alter, transform, or build upon this work, you
			may distribute the resulting work only under the same or similar
			license to this one.
	2. The enclosed script use the FFMPEG program. Please visit the project
	   website http://ffmpeg.mplayerhq.hu for more precision about the license
	   and legal considerations. FFmpeg is licensed under the GNU Lesser
	   General Public License (LGPL). However, FFmpeg incorporates several
	   optional modules that are covered by the GNU General Public License
	   (GPL), notably libpostproc and libswscale. If those parts get used the
	   GPL applies to all of FFmpeg. Read the license texts to learn how this
	   affects programs built on top of FFmpeg or reusing FFmpeg.
	3. The enclosed script use the flash video player developped by Jeroen Wijering.
	   Please visit the website of the author http://www.jeroenwijering.com. The
	   player is under a Creative Commons License (see above for more details).  