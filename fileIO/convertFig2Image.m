% convertFig2Image

function convertFig2Image(fol, outputtype, varargin)

files = dir(fullfile(fol,'*.fig'));
if nargin==2
    outputfol = fol;
else
    outputfol = varargin{1};
end
for i=1:length(files)
   tic
   [~,fname,~] = fileparts(files(i).name);
   %namel = files(i).name(1:length(files(i).name)); 
   %existflag = dir(['png\' namel(1:length(namel)-3) 'png']);
   existflag = exist(fullfile(outputfol, [fname '.' outputtype]), 'file');
   if existflag
       disp(['File ' files(i).name ' already converted!']);
       continue
   else
       h1 = open(fullfile(fol, files(i).name));
       %set(h1, 'WindowStyle', 'docked');
       export_fig(fullfile(outputfol, [fname '.' outputtype]), '-nocrop', ['-' outputtype], h1);
       close(h1);
   end
   disp([num2str(length(files)-i) ' files left, approx ' num2str(toc*(length(files)-i)) 's']);
end