%Wrapper to read SPE files with one command

function data = ReadSPE(fname)

obj = SpeReader(fname);
data = read(obj);