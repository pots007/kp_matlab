%returns plasma density needed to shift wavelengths by a certain amount
%Inputs in nm, output in cm^-3
function ne = RamanShift(varargin)
if (nargin==3)
    gamma = 1+0.5*varargin{3}.^2;
else
    gamma = 1;
end
lambda0 = varargin{1};
lambda1 = varargin{2};
Constants;
lambda0 = lambda0*1e-9;
lambda1 = lambda1*1e-9;
wp = 2*pi*c.*(lambda0-lambda1)./(lambda0.*lambda1);
for i=1:length(wp)
    if (wp(i)<0)
        wp(i) = -wp(i);
    end
end
ne = epsilon0*me*wp.^2*gamma./qe^2;
ne = ne*1e-6;
end