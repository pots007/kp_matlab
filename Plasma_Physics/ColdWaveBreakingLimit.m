% Calculate the cold wavebreaking limit
% ne in cm-3


function [EWBcold, EWBrel] = ColdWaveBreakingLimit(ne)

Constants;
EWBcold = c*me/qe.*omegapcalc(ne);
EWBrel = EWBcold.*sqrt(2*(sqrt(ncrit(800)./(3*ne)) - 1));