%returns plasma frequency n rad/s
% ne in cm^-3;
function omegap = omegapcalc(ne)
e = 1.60217687e-19;
epsilon0 = 8.854187817e-12;
me = 9.10938215e-31;
omegap = ne*1e6*e^2./(epsilon0*me);
omegap = sqrt(omegap);
end