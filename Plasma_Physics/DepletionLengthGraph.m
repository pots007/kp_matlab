figure(99)
den = 1e17:5e16:1e19;
Lpd = 1.75e21./den*(c*30e-15);
h1 = plot(den, Lpd*1e3);
hold on;
Lpd = 1.75e21./den*(c*35e-15);
h2 = plot(den, Lpd*1e3, 'r');
Lpd = 1.75e21./den*(c*40e-15);
h3 = plot(den, Lpd*1e3, 'k');
hold off;
title ('Nonlinear pump depletion rate for different pulse lengths');
set(gca, 'XScale', 'log');
%set(gca, 'YScale', 'log');
set(gca, 'YLim', [0 50]);
xlabel('Plasma density (cm^{-3})');
ylabel('Pump depletion distance (mm)');
legend([h1 h2 h3], '30fs', '35fs', '40fs', 'Location', 'Northeast');