%returns Debye length in m
% ne in cm^-3, Te in K;
function l = debyelengthK(ne, Te)
e = 1.60217687e-19;
epsilon0 = 8.854187817e-12;
kB = 1.3806504e-23;
l = epsilon0*kB*Te./(e^2*ne*1e-6);
l = sqrt(l);
end