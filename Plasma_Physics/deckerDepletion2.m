% gives percentage of energy left in Gaussian pulse after propagating
% in plasma with density of ne
% tau is FWHM width of laser in fs

function enleft = deckerDepletion2(ne, tau, L, infrac)
Constants;
nc = ncrit(800);
w = tau/sqrt(4*log(2)); %this is 1/e=sigma for I - etching is from intensity not field
pul = @(x) exp(-(x/w).^2);
taup = L.*ne./(c.*nc)*1e15;
% We now find the x position for where the intensity falls to infrac:
%pulseend = tau/sqrt(log(2)); %the 1/ee for the FIELD
pulseend = w*sqrt(-log(infrac));
tot = integral(pul, -pulseend, pulseend);
% This starts depleting from 1/ee value of field...
if (length(ne) == 1)
    %Decker model for transmitted ..energy
    enfac = integral(pul, -pulseend, pulseend - taup);
else
    enfac = zeros(length(ne),1);
    for i=1:length(ne)
        %Decker model for transmitted ..energy
        enfac(i) = integral(pul, -pulseend, pulseend - taup(i));
    end
end
enfac(enfac<0) = 0;
enleft = enfac./tot;
end