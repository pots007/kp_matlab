%
% DephasingLength.m
%
% Returns dephasing length.
% Second optional argument is a0, third is wavelength (defaults to 800nm)
% If a_0 omitted, uses simple linear estimation
% If a0 is given, returns both relativistic and linear ones.

function [Ldp,Ldp_rel] = DephasingLength(ne, varargin)
Constants;
omegap = omegapcalc(ne);
if (nargin==1) %linear estimation
    a0 = [];
    lambda0 = 8e-7;
elseif nargin==2 %default to 3D nonlinear
    a0 = varargin{1}; 
    lambda0 = 8e-7;
elseif nargin==3
    a0 = varargin{1};
    lambda0 = varargin{2};
end
omega0 = 2*pi*c/lambda0;
Ldp = omega0^2*c*pi./omegap.^3;
if nargin>1
    Ldp_rel = 4/3*sqrt(a0)*omega0^2*c./omegap.^3;
else
    Ldp_rel = Ldp;
end