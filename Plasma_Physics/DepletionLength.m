% DepletionLength.m

% Returns depletion length for 3D nonlinear case.
% ne in units of cm^-3, tau in fs
% Second optional argument is a0, third is wavelength (defaults to 800nm)
% If a_0 omitted, uses simple linear estimation
% If a0 is given, returns both relativistic and linear ones.

function L_pd = DepletionLength(ne, tau, varargin)
Constants;
omegap = omegapcalc(ne);
if (nargin==2) %linear estimation
    a0 = 9/16;
    lambda0 = 8e-7;
elseif nargin==3 %default to 3D nonlinear
    a0 = varargin{1}; 
    lambda0 = 8e-7;
elseif nargin==4
    a0 = varargin{1};
    lambda0 = varargin{2};
end
omega0 = 2*pi*c/lambda0;
% 3D nonlinear
L_pd = omega0^2./omegap.^2.*c*tau*1e-15;
