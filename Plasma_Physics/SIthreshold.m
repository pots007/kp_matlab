%           SIthreshold.m
%
% Based on Mangles PRSTAB 2012
% ne in cm^-3, tau in fs, l in mm
% Returns energy needed in alpha*E

function E = SIthreshold(ne, tau, l, verbosity)
Constants;
nc = ncrit(800);
C = pi*epsilon0*me^2*c^5/qe^2;
taul = tau*1e-15 - (ne*l*1e-3)/(2*c*nc);
if verbosity; fprintf('\t Pulse has compressed to %3.2f fs\n', taul*1e15); end
if taul < 0.25*tau*1e-15 
    taul = 0.25*tau*1e-15; %To ensure pulse doesn't get too short and remain physical
    if verbosity; fprintf('\t Pulse length has been clamped at %3.2f fs! \n', taul*1e15); end
end;
Ldpl = DepletionLength(ne, tau);
if verbosity; 
    lll = {'shorter', 'longer'};
    fprintf('\t Depletion length is %2.2f mm, %s than the plasma length. \n', Ldpl*1e3, lll{1+ (Ldpl>l*1e-3)})
end
alphaE = C*(log(2*nc./(3*ne))-1).^3*nc./ne.*taul;
if Ldpl < l*1e-3
    if verbosity; fprintf('\t Gas length longer than depletion limit! Using the appropriate expression'); end
    alphaP = 17.4e9*ncrit(800)./(16*ne)*(log(2*ncrit(800)./(3*ne)) - 1).^3*1e-15;
    %alphaE = alphaP*tau;
end
% multiply by 2 to get total energy in pulse
E = alphaE; 