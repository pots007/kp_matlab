% quick calculation to see how much delay is induced due to electron beam
% getting injected later on in the acceleration stage.

%% One density

ne = 1e19;
omegap = omegapcalc(ne);
Lp = 0.0011;
Lacc = 0.0001:0.0001:0.001;
a0 = 4;
t_gr = (Lp-Lacc)./c.*(1-0.5*omegap.^2/omega0^2*(1-a0^2/4));
t_tot = t_gr + Lacc/c;
figure(7);
plot((Lp-Lacc)*1e3, (t_tot-Lp/c)*1e15);
xlabel('Injection delay from 0 (mm)')
ylabel('Electron beam delay (fs)')

%% Density scan

ne0 = (1:0.001:10)*1e18; 
Lp = 0.01;
Lacc0 = 0.0001:0.00005:0.009;
ne = repmat(ne0',1, length(Lacc0));
omegap = omegapcalc(ne);
Lacc = repmat(Lacc0, length(ne0), 1);
a0 = 4;
gamma = 1+0.25*a0^2;
%t_gr = (Lp-Lacc)./(c.*(1-0.5*omegap.^2/omega0^2*(1-a0^2/4)));
t_gr = (Lp-Lacc)./(c.*sqrt(1-omegap.^2/(gamma*omega0^2)));
t_tot = t_gr + Lacc/c;
figure(7);
imagesc((Lp-Lacc0)*1e3, ne0*1e-18, (Lp/c-t_tot)*1e15);
xlabel('Injection point in plasma (mm)')
ylabel('Plasma density (10^{18} cm^{-3})')
cb = colorbar;
ylabel(cb, 'Electron beam delay (fs)')
setAllFonts(7,20);

%% Using t_gr = d_phi/d_omega

ne0 = (1:0.01:10)*1e18; 
Lp = 0.001;
Lacc0 = 0.0001:0.00005:0.001;
ne = repmat(ne0',1, length(Lacc0));
omegap = omegapcalc(ne);
Lacc = repmat(Lacc0, length(ne0), 1);
a0 = 4;
% k = sqrt((omega0^2-omegap^2)/c^2) and with phi = k*L

dphi_domega = (Lp-Lacc)./c.*omega0./(sqrt(omega0^2-omegapcalc(ne).^2));
%t_gr = (Lp-Lacc)./c.*(1+0.5*omegap.^2/omega0^2*(1-a0^2/4));
%t_tot = t_gr + Lacc/c;
t_diff = Lp/c-dphi_domega;
figure(7);
imagesc((Lp-Lacc0)*1e3, ne0*1e-18, t_diff*1e15);
xlabel('Injection point in plasma (mm)')
ylabel('Plasma density (10^{18} cm^{-3})')
cb = colorbar;
ylabel(cb, 'Electron beam delay (fs)')
setAllFonts(7,20);
