% gives percentage of energy left in Gaussian pulse after propagating
% in plasma with density of ne
% tau is FWHM width of laser in fs

function enleft = deckerDepletion(ne, tau, L)
Constants;
nc = ncrit(800);
w = tau/sqrt(4*log(2)); %this is 1/e=sigma for I - etching is from intensity not field
pul = @(x) exp(-(x/w).^2);
taup = L.*ne./(c.*nc)*1e15;
pulseend = tau/sqrt(log(2)); %the 1/ee for the FIELD
tot = integral(pul, -1e4, pulseend);
% This starts depleting from 1/ee value of field...
if (length(ne) == 1)
    %Decker model for transmitted ..energy
    enfac = integral(pul, -1e4, pulseend - taup);
else
    enfac = zeros(length(ne),1);
    for i=1:length(ne)
        %Decker model for transmitted ..energy
        enfac(i) = integral(pul, -1e4, pulseend - taup(i));
    end
end
enleft = enfac./tot;
end