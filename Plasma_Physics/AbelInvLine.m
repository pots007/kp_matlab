function nline = AbelInvLine(phi, ymid, micperpixel, lambda)

% For moire, using
% n(r) - 1  = -1/pi*int^rf_r phi(y)dy/sqrt(y^2-r^2)

dy = micperpixel;
nline = zeros(ymid,1);
phiy = phi(1:ymid);
phiy = fliplr(phiy);
for i=1:length(nline)
    %iterate through every r coordinate starting from 0
    %set r
    integr = [];
    for k=i+1:length(nline)
        r = double(i)*dy;
        y = double(k)*dy;
        integr(k) = phiy(k)/sqrt(y^2-r^2);
    end
    nline(i) = dy*trapz(integr);
end
%n=1+k(rho/rho_atm)
%rho_atm=(P/RT)*Na
rho_atm=(101.325e3*6.022e23)/(8.31*273); %units m^-3

%ncrit for 800nm in m^-3
k = rho_atm/(2*ncrit(lambda)*1e6);

nline = nline*rho_atm/k;