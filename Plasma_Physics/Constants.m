%Constants.
%When run, puts constants into workspace, with normal symbols.
c = 299792458; %m/s
qe = 1.60217687e-19; % C
epsilon0 = 8.854187817e-12;
amu = 1.660538921e-27; %kg
Na= 6.022141e23;
me = 9.10938215e-31; %kg
h = 6.62606957e-34; %J/s
hbar = h/(2*pi); %J/s
kB = 1.3806488e-23; %J/K
omega0 = 2*pi*c/8e-7; %Use it so much...