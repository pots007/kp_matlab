% returns ncrit in cm-3 for lambda in nm

function nc = ncrit(lambda0)
Constants;
l0 = lambda0*1e-9;
nc = (2*pi*c./l0).^2.*epsilon0*me/qe^2;
nc = nc*1e-6;
end