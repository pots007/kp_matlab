function n = RefIndex(omega, material, pol)
%MYREFINDEX returns the refractive index of a material.
%	MYREFINDEX(OMEGA, MATERIAL) returns 
%   the refractive index of a MATERIAL with ordinary POLARIZATION in the spectral 
%   range given by OMEGA in inverse fs. (Hertz)
%
%
%	Materials currently supported are:
%   BBO, BBO2 (Hand. of Opt. BBO), KDP, FUSED SILICA, BK7, SF11l, AIR
%
%	Data cited from "Handbook of Optics" except:
%       'BBO' modified per Zhang et al., Optics Commun. 184 (2000), 485-491
%       'SF11' from Schott Glass catalog
%
%   Added air Date:2013/05/24 KP
%   changed by Joerg Schreiber
%   lambda -> omega, omega in inverse fs
% 
%	$Revision: 1.13 $ $Date: 2002/06/10 05:05:05 $
%
%	$Log: refindex.m,v $
%	Revision 1.13  2002/06/10 05:05:05  xg
%	Now calls unitconvert for conversion between units.
%	
%	Revision 1.12  2002/04/23 15:29:29  pat
%	Added Proustite.
%	
%	Revision 1.11  2002/01/31 15:52:28  pat
%	Fixed error in SF11
%	
%	Revision 1.10  2001/12/19 20:19:51  pat
%	Added SF11
%	
%
%	Revision 1.9  2001/11/15 21:32:54  zeekec
%	New ParseVarargin
%	
%	Revision 1.8  2001/10/18 19:14:39  zeekec
%	Help update.
%	
%	Revision 1.7  2001/09/20 19:36:26  xg
%	no message
%	
%	Revision 1.6  2001/09/05 00:21:05  zeekec
%	Added units and made other changes
%	
%	Revision 1.5  2001/08/21 14:40:58  zeekec
%	Added CVS keywords.
%	
%	v1.0, 7/21/01, Xun Gu, <xgu@iname.com>
%	v1.1, 8/13/01, Xun Gu, <xgu@iname.com>
%		BBO updated
%

%polarization = 'O';
polarization = pol;
% for the equations we need lambda in microns
%l = (2*pi*299.792./omega)*1e-3;
l = 2*pi*299792458./omega*1e6;

switch upper(material)
case 'BBO'
    % Sellmeier equation modified per Zhang et al., Optics Commun. 184 (2000), 485-491
    switch upper(polarization)
    case 'O'
        n = sqrt(2.7359 + 0.01878 ./ (l .^ 2 - 0.01822) - 0.01471 * l .^ 2 + ...
            0.0006081 * l .^ 4 - 0.00006740 * l .^ 6);
    case 'E'
        n = sqrt(2.3753 + 0.01224 ./ (l .^ 2 - 0.01667) - 0.01627 * l .^ 2 + ...
            0.0005716 * l .^ 4 - 0.00006305 * l .^ 6);
    otherwise
        error('Polarization not recognized')
    end
    if (min(l) < 0.22 || max(l) > 2.2)
        warning('Wavelength out of applicable range of the formula for BBO (0.22-2.2 microns)');
    end
    case 'CALCITE'
    % From http://www.u-oplaz.com/crystals/crystals30.htm
    switch upper(polarization)
    case 'O'
        n = sqrt(1.73358749 + 0.96464345*l.^2./(l.^2-1.94325203e-2) + ...
            1.82831454*l.^2./(l.^2-120)); 
    case 'E'
        n = sqrt(1.35859695 + 0.82427830*l.^2./(l.^2-1.06689543e-2) + ...
            0.14429128*l.^2./(l.^2-120) );
    otherwise
        error('Polarization not recognized')
    end
    if (min(l) < 0.22 || max(l) > 2.2)
        warning('Wavelength out of applicable range of the formula for BBO (0.22-2.2 microns)');
    end
case 'BBO2'
    switch upper(polarization)
    case 'O'
        n = sqrt(2.7405 + 0.0184 ./ (l.^2 - 0.0179) - 0.0155 * l.^2);
    case 'E'
        n = sqrt(2.3730 + 0.0128 ./ (l.^2 - 0.0156) - 0.0044 * l.^2);
    otherwise
        error('Polarization not recognized')
    end
    if (min(l) < 0.22 || max(l) > 1.06)
        warning('Wavelength out of applicable range of the formula for BBO (0.22-1.06 microns)');
    end
case 'PROUSTITE'
    switch upper(polarization)
    case 'O'
        n = sqrt(9.220 + 0.4454./(l.^2 - 0.1264) + 1733./(l.^2 - 1000));
    case 'E'
        n = sqrt(7.007 + 0.3230./(l.^2 - 0.1192) + 660./(l.^2 - 1000));
    otherwise
        error('Polarization not recognized')
    end
    if (min(l) < 0.6 || max(l) > 10.6)
        warning('Wavelength out of applicable range of the formula for Proustite (0.6-10.6 microns)');
    end
case 'KDP'
    switch upper(polarization)
    case 'O'
        n = sqrt(1 + 1.256618 * l .^ 2 ./ (l .^ 2 - 0.0084478168) + ...
            33.89909 * l .^ 2 ./ (l .^ 2 - 1113.904));
    case 'E'
        n = sqrt(1 + 1.131091 * l .^ 2 ./ (l .^ 2 - 0.00814598) + ...
            5.75675 * l .^ 2 ./ (l .^ 2 - 811.7537));
    otherwise
        error('Polarization not recognized')
    end
    if (min(l) < 0.4 || max(l) > 1.06)
        warning('Wavelength out of applicable range of the formula for KDP (0.4-1.06 microns)');
    end
case 'FUSED SILICA'
    switch upper(polarization)
    case 'O'
    case 'E'
        warning('Fused silica is an isotropic material.');
    otherwise
        error('Polarization not recognized')
    end
    n = sqrt(1 + 0.6961663 * l .^ 2 ./ (l .^ 2 - 0.0684043 ^ 2) + ...
        0.4079426 * l .^ 2 ./ (l .^ 2 - 0.1162414 ^ 2) + ...
        0.8974794 * l .^ 2 ./ (l .^ 2 - 9.896161 ^ 2));
    
    if (min(l) < 0.21 || max(l) > 3.71)
        warning('Wavelength out of applicable range of the formula for fused silica (0.21-3.71 microns)');
    end
    
case 'BK7'
    switch upper(polarization)
    case 'O'
    case 'E'
        warning('BK7 glass is an isotropic material.');
    otherwise
        error('Polarization not recognized')
    end
    % Melles Griot
    n = sqrt( 1+1.03961212*l.^2 ./ (l.^2 - 6.00069867e-3) + ...
                2.31792344e-1*l.^2 ./ (l.^2 - 2.00179144e-2) + ...
                1.01046945*l.^2 ./ (l.^2 - 1.03560653e2) );
    % original 
    %n = sqrt(2.2718929 - 1.0108077e-2 * l .^ 2 + 1.0592509e-2 * l .^ -2 + ...
    %    2.0816965e-4 * l .^ -4 - 7.6472538e-6 * l .^ -6 + ...
    %    4.9240911e-7 * l .^ -8);
    if (min(l) < 0.37 || max(l) > 1.01)
        warning('Wavelength out of applicable range of the formula for BK7 glass (0.37-1.01 microns)');
    end
case 'VACUUM'
    n = ones(size(l));
case 'SF11'
    switch upper(polarization)
    case 'O'
    case 'E'
        warning('SF11 glass is an isotropic material.');
    otherwise
        error('Polarization not recognized')
    end
    n = sqrt(3.0539614 - 1.1580432e-2 * l .^ 2 + 3.9199816e-2./l.^2 + ...
        2.9462812e-3./l.^4 - 2.0371019e-4./l.^5 + 2.7633569e-5./l.^6);
    if (min(l) < 0.40 || max(l) > 2.33)
        warning('Wavelength out of applicable range of the formula for BK7 glass (0.37-1.01 microns)');
    end
case 'AIR'   
    n = 1+ 1e-8*(5792105./(238.0185 - l.^-2) + 167917./(57.362 - l.^-2));
    if (min(l) < 0.3 || max(l) > 1.2)
        warning('Wavelength out of applicable range of the formula for air (0.3-1.2) microns)');
    end
otherwise
    error('Material not supported')
end

n(l<0)=0;

