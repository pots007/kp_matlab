function rho = AbelInversionLine(Phi, ymid, asmooth, micperpix, lambda)
%Phi = the phase from the last function (don't know why I changed the name)
%ymid = pixel of symmetry axis, if zero it can be found automatically or
%manually asmooth = the smoothing parameter for the spline fit micperpix =
%self-explanatory lambda = wavelength in nm (probably not necessary as
%800nm is hard-coded below) plotflag = set to 1 to plot the density at the
%end - I may have messed this bit up npoints = how many points to sample
%across the image - not tested so again may be a bit shit

Phi(isnan(Phi)) = 0;

%Phi = fliplr(Phi);
ysize = length(Phi)-1;

h = micperpix*1e-6;

%lambda = lambda*1e-9;

%rho_atm=(101.325e3*6.022e23)/(8.31*273); %units m^-3

Philine = Phi;
%Philineold = Philine;
%Philine = [fliplr(Philine) Philine];
yaxis = 1:length(Philine);
%Philine = csaps(yaxis, Philine, asmooth, yaxis);
%Philine = Philine(end/2 + 1:end);

%dPhidy = -gradient(Philine,1);
dPhidy = Philine;
%dPhidy = dPhidy/h;
% Remove singularities at r = 0
%dPhidy(1) = 0.1*dPhidy(2);

for r_index = 1:ysize-ymid
    for y_index = r_index:ysize - ymid+1

        y = (double(y_index) - 0.9)*h;
        r = (double(r_index) - 1.0)*h;
        integrand(y_index - r_index + 1) = dPhidy(y_index)*(y^2 - r^2)^-0.5;
    end
    
    %rho(r_index) = ((lambda*rho_atm)/(2*pi^2*k))*h*trapz(integrand);
    rho(r_index) = h*trapz(integrand);
    integrand(end) = [];
end


rho = fliplr(rho);

end
