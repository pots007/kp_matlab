% DepletionLength2.m
%
% Returns depletion length for 3D nonlinear case, but specifies the pulse
% length to be eroded as the length for which P is above P_crit
%
% ne in units of cm^-3, tau in fs
% Second optional argument is a0, third is wavelength (defaults to 800nm)
% If a_0 omitted, uses simple linear estimation
% If a0 is given, returns both relativistic and linear ones.

function [L_pd, L_var] = DepletionLength2(ne, tau, E0, lambda0)
Constants;
omegap = omegapcalc(ne);
if (nargin<4) %linear estimation
    lambda0 = 8e-7;
end
omega0 = 2*pi*c/lambda0;
Pcrit = 17.4e9*ncrit(800)./ne;
% Peak power:
P0 = E0/(tau*1e-15); % To first approximation...
tau0 = tau/(2*sqrt(log(2)));

tt = tau0*sqrt(log(P0./Pcrit));
% 3D nonlinear, etching model
L_pd = omega0^2./omegap.^2.*c*tau*1e-15;
L_var = omega0^2./omegap.^2.*c.*tt*2*1e-15;
