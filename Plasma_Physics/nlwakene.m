%matlab version of Alec Thomas's nlwake spreadsheet
% UPDATE Sept 2016 - changed the factor of 2 in calculating the sigma for
% intensity.

function out = nlwakene(a0, tau, ne)
%input parameters
%a0 = 0.1;%1.394;  %peak vector potential (i.e. NOT CYCLE-AVERGED!)
P = 1;   % P = 1 is a gaussian pulse, P > 1 for supergaussian
%ne = 4e18; % background electron density (cm-3)

lambda_L = 0.8;  % laser wavelength
%tau = 50e-15; %laser duration (s)
tau = tau/(sqrt(2*log(2)));
pol = 'linear'; % either 'linear' or 'circ'
%SIplot = true; %plot with SI units if true, normalised if not.

%%%  constants
e0 = 8.85e-12;
e = 1.6e-19;
me = 9.11e-31;
c = 3e8;

nc = 4*pi^2*e0*me*c^2/e^2/(lambda_L*1e-6)^2 / 1e6; %critical density (cm^-3)

%x_axis_limits_si = [-50, 150]; % x-axis limits for display
%x_axis_limits = [-1 5];
%
lambda_p = sqrt(nc/ne)*lambda_L;
ctau_si = (c * tau)/1e-6;
ctau = ctau_si /lambda_p;
omega_p = 2*pi*c/lambda_p;

% set up "grid";
dxi = 0.0005;
xi = -2:dxi:10;

% set up laser field
if strcmp(pol, 'linear')
    pol_fac = sqrt(2);
end
if strcmp(pol, 'circ');
    pol_fac = 1;
end
a = a0*exp(- ((xi-0.)/ctau).^(2*P))/pol_fac; % time averaged laser envelope

% initialise arrays
phi = zeros(size(xi));
phi2 = zeros(size(xi));
E = zeros(size(xi));
dn = zeros(size(xi));
phi(1) = 0.1;
phi2(1) = 0;
phi2(2) = 0;
E(1) = 0;
E(2) = 0;
dn(1) = 0;
dn(2) = 0;

% do numerical integration
% for(i = 2:length(phi)) %WHAT WAS THIS FOR ALEC?
%     if phi(i-1) < 0
%         phi(i) = phi(i-1) + 2*pi*dxi*sqrt( 1 - 1/(1+phi(i-1)) - phi(i-1));
%     else
%         phi(i) = phi(i-1) - 2*pi*dxi*sqrt(-(1 - 1/(1+phi(i-1)) - phi(i-1)));
%     end
% end

for(i = 3:length(phi2))
    
    phi2(i) = 2*phi2(i-1)-phi2(i-2)+(2*pi)^2/2*((1+a(i)^2)/(1+phi2(i-1))^2-1)*dxi^2;
    
end

for(i = 3:length(E))
    E(i) = (phi2(i) - phi2(i-1))/dxi;
end

for(i = 3:length(dn))
    dn(i) =(E(i)-E(i-1))/dxi/(2*pi)^2;
    
end

out.n = (dn + 1)*ne;  % plasma density
out.Phi_si =  phi2 * (me*c^2/e);
out.E_si = E * (me*c*omega_p/e);
out.xi = xi*lambda_p;
out.I = (a*pol_fac./(0.85 * lambda_L) ).^2 *1e18;
out.a = a;


% %% display data
% figure(1)
% if (SIplot)
%     n = (dn + 1)*ne;  % plasma density
%     Phi_si =  phi2 * (me*c^2/e);
%     E_si = E * (me*c*omega_p/e);
%     xi = xi*lambda_p;
%     I = (a*pol_fac./(0.85 * lambda_L) ).^2 *1e18;
%     subplot(411)
%     plot(xi, I, 'k', 'Linewidth', 2)
%     xlim(x_axis_limits_si);
%     set(gca, 'FontSize', 14')
%     ylabel('intensity [Wcm^{-2}]')
%     subplot(412)
%     plot(xi, n, 'b', 'Linewidth', 2)
%     xlim(x_axis_limits_si);
%     set(gca, 'FontSize', 14')
%     ylabel('plasma density [cm^{-2}]')
%     subplot(413)
%     plot(xi, -Phi_si, 'r', 'Linewidth', 2)
%     xlim(x_axis_limits_si);
%     set(gca, 'FontSize', 14')
%     ylabel( '-potential [V]')
%     subplot(414)
%     plot(xi, E_si, 'g',  'Linewidth', 2)
%     xlim(x_axis_limits_si);
%     set(gca, 'FontSize', 14')
%     ylabel( 'electric field [V/m]')
%     xlabel( '(z - ct) [\mum]')
% else
%     subplot(411)
%     plot(xi, a, 'k', 'Linewidth', 2)
%     xlim(x_axis_limits);
%     set(gca, 'FontSize', 14')
%     ylabel('a_0')
%     subplot(412)
%     plot(xi, dn, 'b', 'Linewidth', 2)
%     xlim(x_axis_limits);
%     set(gca, 'FontSize', 14')
%     ylabel('\delta n/n_0')
%     subplot(413)
%     plot(xi, -phi2, 'r', 'Linewidth', 2)
%     xlim(x_axis_limits);
%     set(gca, 'FontSize', 14')
%     ylabel( '- \phi')
%     subplot(414)
%     plot(xi, E, 'g',  'Linewidth', 2)
%     xlim(x_axis_limits);
%     set(gca, 'FontSize', 14')
%     ylabel( 'E / E_0')
%     xlabel( '\xi /\lambda_p')
% end
