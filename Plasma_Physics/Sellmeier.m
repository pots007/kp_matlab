%Sellmeier equation returning n as a function of lambda in microns.
%Arg 2 is either 'FS' for Fused silica or 'BK7' for BK7
function refrindex = Sellmeier(lambda, material)
if (strcmp(material, 'BK7'))
    B1 = 1.03961212;
    B2 = 0.231792344;
    B3 = 1.01046945;
    C1 = 6.00069867e-3;
    C2 = 2.00179144e-2;
    C3 = 1.03560653e2;
end
if (strcmp(material, 'FS'))
    B1 = 0.696166300;
    B2 = 0.407942600;
    B3 = 0.897479400;
    C1 = 4.67914826e-3;
    C2 = 1.35120631e-2;
    C3 = 97.9340025;
end
n2 = 1 + B1*lambda.^2./(lambda.^2-C1) + ...
    B2*lambda.^2./(lambda.^2-C2) + ...
    B3*lambda.^2./(lambda.^2-C3);
refrindex = sqrt(n2);
end