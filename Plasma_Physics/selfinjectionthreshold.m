% From Stuart's paper
% Mangles et al, PRSTAB 15(1) 2012
% Answer in watts

function powe = selfinjectionthreshold(ne)
Constants;
ne = ne.*1e6;
nc = (2*pi*c/8e-7)^2*epsilon0*me/qe^2;
powe = 17.4e9*nc./(ne.*16).*(log(2*nc./(3.*ne))-1).^3;
end