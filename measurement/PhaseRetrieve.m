function phase = PhaseRetrieve(datafile, intref, rotation, x, y, w, h, xfft, yfft, wfft, hfft)
%x, y, w, h defines the region of the input image that we will work with.
 
% data = double(imread(datafile));
data = double(datafile);
 
% if (intref == 0)
%     intref = zeros(size(data));
% else
%     intref = double(imread(intref));
% end
% %reference image
 
data = imrotate(data, rotation);
intref = imrotate(intref, rotation);
 
data = data(y:y+h, x:x+w);
intref = intref(y:y+h, x:x+w);
 
fftim = fft2(data);
fftref = fft2(intref);
 
if (max([xfft yfft wfft hfft]) == 0)
    imagesc(log(abs(fftshift(fftim))))
    rect = getrect;
    xfft = round(rect(1))
    yfft = round(rect(2))
    wfft = round(rect(3))
    hfft = round(rect(4))
end
%this if statement gives you the option of manually choosing the area of
%interest in the fft'd image if you input [xfft yfft wfft hfft] = [0 0 0
%0].
 
fftim = fftshift(fftim);
fftref = fftshift(fftref);
newfftim = zeros(size(data));
newfftref = zeros(size(data));
newfftim(yfft:yfft+hfft, xfft:xfft+wfft) = fftim(yfft:yfft+hfft, xfft:xfft+wfft);
newfftref(yfft:yfft+hfft, xfft:xfft+wfft) = fftref(yfft:yfft+hfft, xfft:xfft+wfft);
newfftim = fftshift(newfftim);
newfftref = fftshift(newfftref);
 
datafiltered = ifft2(newfftim);
reffiltered = ifft2(newfftref);
 
if(sum(sum(isnan(reffiltered))) > 0)
    reffiltered = ones(size(datafiltered));
end
 
phase = angle(datafiltered./reffiltered);
mag = abs(datafiltered./reffiltered);
phase = unwrap(phase, 5.5);
 
% Here you can spline-smooth the phase, make 'smoothing' smaller for more smoothing
smoothing = 1;
[ysize xsize] = size(phase);
xsize = 1:xsize;
ysize = 1:ysize;
x = {ysize,xsize};
[phase,p] = csaps(x,phase,smoothing,x);
 
imagesc(phase)
 
end