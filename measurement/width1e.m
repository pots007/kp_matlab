%Find the fwhm of the image given in
%Extrapolate exact FWHM locations between the two pixels with linear
%extrapolation

function fwhm = width1e(image)
[totY, totX] = size(image);
x = linspace(1,totX,totX);
%convert to microns
x = x;
%x = x*0.0778;
%x = x*0.797;
%x = x*0.13;
y = image;
%y = sum(image)/totY;
[HM, maxloc] = max(y);
HM = HM*exp(-1);
%Find first HM location.
i = 2;
while sign(y(i)-HM) == sign(y(i-1)-HM)
    i = i+1;
end  
slope = (HM-y(i-1)) / (y(i)-y(i-1));
x1 = x(i-1) + slope*(x(i)-x(i-1));
%Find second HM location. Assume it's after first one and after max value.
i = maxloc+1;
while ((sign(y(i)-HM) == sign(y(i-1)-HM)) & (i <= totX-1))
    i = i+1;
end
%Avoid NaN returns.
if (i ~= totX)
    slope = (HM-y(i-1)) / (y(i)-y(i-1));
    x2 = x(i-1) + slope*(x(i)-x(i-1));
    fwhm = abs(x2 - x1);
else
    disp('No second edge.')
    return
end
%plot(x,y);
end