
lambda = (0.371:0.001:1)*1e-6; omega = 2*pi*c./lambda;
materials = {'FUSED SILICA', 'BK7'};
hs = zeros(length(materials),2);
for i=1:2%length(materials)
    n = RefIndex(omega, materials{i}, 'O');
    energy = 100; % in MeV/c
    gamma = energy/0.511 + 1;
    beta = 1-1/gamma^2;
    figure(19); 
    epsilon = sqrt(n);
    %plot(lambda,acosd(1./(beta.*n)))
    % Emitted energy per frequency:
    %dE_dxdomega = qe^2./(4*pi*epsilon.*epsilon0*c^2).*omega.*(1-1./(beta^2*epsilon));
    %subplot(1,2,1)
    %plot(lambda, dE_dxdomega/hbar)
    alpha = qe^2*c*4*pi*1e-7/(2*h);
    dN_dxdlambda = 2*pi*alpha./lambda.^2.*(1-1./(beta^2*epsilon));
    n = @(l) RefIndex(2*pi*c./l, materials{i}, 'O');
    dN_dxdlambdaf = @(l) 2*pi*alpha./l.^2.*(1-1./(beta^2*n(l).^2));
    subplot(2,1,1); hold all;
    hs(i,1) = plot(lambda*1e9, dN_dxdlambda, 'LineWidth', 2); hold off;
    xlabel('Wavelength / nm'); ylabel('dN/dxd\lambda / photons/m^2')
    lims = [375 900]*1e-9;
    dN_dx = integral(dN_dxdlambdaf, lims(1), lims(2));
    subplot(2,1,2); hold all; 
    x = 0:0.01:3;
    hs(i,2) = plot(x, dN_dx*0.01*x, 'LineWidth', 2); hold off;
    xlabel('Glass thickness / cm');
    ylabel(sprintf('N (%3.0f nm < \\lambda < %3.0f nm)', lims*1e9));
end
subplot(2,1,1)
legend(hs(:,1), materials); set(gca, 'Box', 'on');
subplot(2,1,2)
legend(hs(:,2), materials, 'Location', 'NorthWest'), set(gca, 'Box', 'on');
setAllFonts(19,20);
export_fig('Cerenkov', '-transparent', '-pdf', '-nocrop', 19);