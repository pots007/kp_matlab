% getLaserPower.m

% Calculates laser power given a0 and w0

% Input arguments:  a0,
%                   w0x,
%                   w0y - if omitted, squares w0x
% w0 needs to be in SI!!!

function P = getLaserPower(a0, w0x, w0y)

Constants;
C = pi*epsilon0*me^2*c^3*omega0^2/(4*qe^2);
if nargin==2
    w0y = w0x;
end
P = a0.^2.*w0x.*w0y*C;

