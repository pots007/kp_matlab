%Script to do quick centroid calculation on a folder of data
%accepts matrices

function centroidlocs = centroidlocationsmatrix(I, BGfac)

%I = imread(filename);
[rows, cols] = size(I);
IMAGE = double(I);
%IMAGE = IMAGE - ones(rows, cols)*1000;
%Define BG level, under which pixels will  not count towards centroid.
if nargin<2 || BGfac>1
    BGfac = 0.15;
end
maxval = max(max(IMAGE));
BG = maxval*BGfac;
IMAGE(IMAGE<BG)=0;
%Find centroid by weighting pixels by intensity
x = ones(rows,1)*(1:cols);
y = (1:rows)'*ones(1,cols);
totcounts = sum(IMAGE(:));
xbar = sum(sum(IMAGE.*x))/totcounts;
ybar = sum(sum(IMAGE.*y))/totcounts;
centroidlocs(1) = xbar;
centroidlocs(2) = ybar;
end