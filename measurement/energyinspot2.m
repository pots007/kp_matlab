%energyinspot2.m
%5A data analysis tool
%Figuring out how much energy is within FWHM, e/1, plus spot size in x and
%y, also plotting everything out into file. 
%Major update on background subtraction 07102012
function energies = energyinspot2(filename)
I = imread(filename);
plotbool = false;
image = I;
[rows cols] = size(image);
BGwidth = 100;
%Top and bottom edges
TopEdge = image([1:BGwidth],:);
TopEdgeMean = mean(mean(TopEdge));
BottomEdge = image([rows-BGwidth:rows],:);
BottomEdgeMean = mean(mean(BottomEdge));
%Side edges
RightEdge = image([(BGwidth+1):rows-(BGwidth+1)],[1:BGwidth]);
RightEdgeMean = mean(mean(RightEdge));
LeftEdge = image([(BGwidth+1):rows-(BGwidth+1)],[cols-(BGwidth+1):cols]);
LeftEdgeMean = mean(mean(LeftEdge));
BGcount = mean([TopEdgeMean BottomEdgeMean LeftEdgeMean RightEdgeMean]);

%subtract BG from signal pixels and set all BG pixels to zero
BGMat = ones(rows, cols)*BGcount;
IMAGE = double(image) - BGMat;

%New bg method
RightEdge = image(:,1:BGwidth);
LeftEdge = image(:,cols-(BGwidth+1):cols);

Meancols = mean([RightEdge LeftEdge], 2);
BGMatrix = repmat(Meancols, 1,cols);
%IMAGE = double(image) - BGMatrix;
%Finding Max value and location
[Maxs, Maxind] = max(IMAGE);
[MaxVal, xMax] = max(max(IMAGE));
yMax =  Maxind(xMax);

%Find centroid by weighting pixels by intensity
x = ones(rows,1)*[1:cols];
y = [1:rows]'*ones(1,cols);
totcounts = sum(sum(IMAGE));
xbar = sum(sum(IMAGE.*x))/totcounts;
ybar = sum(sum(IMAGE.*y))/totcounts;

%Zoom in on focus
%For later, determine the zoom by the width of the line out of max
FocZoom = IMAGE([yMax-50:yMax+50],[xMax-50:xMax+50]);

%Centroid of the focal zoom (recycling variables)
[rows cols] = size(FocZoom);
x = ones(rows,1)*[1:cols];
y = [1:rows]'*ones(1,cols);
totcounts = sum(sum(FocZoom));
xbar = sum(sum(FocZoom.*x))/totcounts;
ybar = sum(sum(FocZoom.*y))/totcounts;
if (plotbool)
    %h = figure

end
%Now take lineout through the found centroid to get 
%FWHM, 1/e and 1/e^2 values
xbar = round(xbar);
ybar = round(ybar);
horlineout = FocZoom(ybar,:);
horfwhm = width2(horlineout);
verlineout = FocZoom(:,xbar)';
verfwhm = width2(verlineout);
totcounts = sum(sum(IMAGE));
Efwhm = sum(sum(FocZoom(FocZoom>MaxVal*0.5)))/totcounts;
%Efwhm = sum(sum(IMAGE(IMAGE>MaxVal*0.5)))/totcounts;
area = numel(FocZoom(FocZoom>MaxVal*0.5));
fwhmradius = 1*2*sqrt(area/pi);
%title(['Efwhm = ',num2str(Efwhm)]);
E1e= sum(sum(FocZoom(FocZoom>MaxVal*exp(-1))))/totcounts;
%probs doesn't work for lower quotatients
E1ee= sum(sum(FocZoom(FocZoom>MaxVal*exp(-2))))/totcounts;
energies(1) = Efwhm;
energies(2) = E1e;
energies(3) = horfwhm;
energies(4) = verfwhm;
energies(5) = fwhmradius;
if (plotbool)
    h = figure('units','normalized','outerposition',[0 0 1 1]);
    subplot(2,2,1)
    imagesc(IMAGE); hold on
    plot(xbar,ybar,'+')
    plot(xMax, yMax, '*')
    colorbar
    filenamestem = filename(1:length(filename)-4);
    title(filenamestem, 'Interpreter', 'none')
    subplot(2,2,3)
    plot(FocZoom(ybar,:))
    %plot(IMAGE(yMax,:))
    xlabel('x-lineout')
    title(['Horizontal FWHM value =' num2str(horfwhm) ' microns']);
    subplot(2,2,4)
    plot(FocZoom(:,xbar))
    %plot(IMAGE(:,xMax))
    xlabel('y-lineout')
    title(['Vertical FWHM value =' num2str(verfwhm) ' microns']);
    subplot(2,2,2)
    imagesc(FocZoom)
    colorbar; hold on
    plot(xbar, ybar, '*')
    title(['Energy in FWHM = ' num2str(Efwhm)])
    saveas(h, [filenamestem '.png'], 'png')
    close(h)
end
end