% calcIntensity.m
%
% Returns peak intensity and a0 value for the given pulse length, pulse
% energy and spot size
%
% Arguments:
%               energy:         total pulse energy, in J. This assumes all
%                               energy is within the pulse, so doesn't 
%                               account for crappy spots!
%               tau:            pulse FWHM intensity width, in s
%               spotsize:       spot size, in m
%               spotsizetype:   what parameter spotsize refers to, options
%                               are w0 (field 1/e width), FWHM (intensity
%                               FWHM width), wI (intensity 1/e width, field
%                               1/e^2 width). Default is w0.
%
% Returns intensity in SI units, ie W/m^2

function [peakI, a0] = calcIntensity(energy, tau, spotsize, spotsizetype, lambda0)
% Parse inputs
if nargin < 4 % using default
    w0 = spotsize;
    lambda0 = 800e-9;
elseif nargin < 5
    switch spotsizetype        
        case 'w0'
            w0 = spotsize;
        case 'FWHM'
            w0 = spotsize/sqrt(2*log(2));
        case 'wI'
            w0 = spotsize*sqrt(2);
        otherwise
            warning('Unknown spotsizetype argument. Defaulting to beam waist.');
    end
    lambda0 = 800e-9; % Default wavelength is Ti:Sapph
end

% The rest is easy!
peakI = 2*0.299*energy./(tau.*w0.^2);
a0 = 0.856*(lambda0*1e6).*sqrt(peakI*1e-22);
