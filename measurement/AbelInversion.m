function rho = AbelInversion(Phi, ymid, asmooth, micperpix, lambda, plotflag, npoints)
%Phi = the phase from the last function (don't know why I changed the name) 
%ymid = pixel of symmetry axis, if zero it can be found automatically or 
%manually asmooth = the smoothing parameter for the spline fit micperpix = 
%self-explanatory lambda = wavelength in nm (probably not necessary as 
%800nm is hard-coded below) plotflag = set to 1 to plot the density at the 
%end - I may have messed this bit up npoints = how many points to sample 
%across the image - not tested so again may be a bit shit

Phi(isnan(Phi)) = 0;

if (ymid == 0)

   % Find symmetry axis automatically
   ymidautoflag = 1;
    Phi = Phi - min(min(Phi));
   if (ymidautoflag == 1)
       % Auto ymid finder
       [zsize ysize] = size(Phi);
       yaxis = (1:ysize);
       zpoints = 0.1:0.1:0.9;
       zpoints = zpoints*zsize;
       zpoints = round(zpoints);
       Phi0 = Phi;
       Phi0(Phi0<0.4*max(max(Phi0))) = 0;
       for n = 1:length(zpoints)
        %   yres(n) = trapz(Phi(zpoints(n),:).*yaxis)./trapz(Phi(zpoints(n),:));
            yres(n) = sum(Phi0(zpoints(n),:).*yaxis)/sum(Phi0(zpoints(n),:));
       end
       yres(yres>ysize) = 0.5*ysize;
       ymid = round(mean((yres)));
       imagesc((Phi))
       line([ymid ymid], [0 zsize], 'color', 'white')
       hold on
       scatter(yres, zpoints)
       hold off
       drawnow
       pause(5)
   else
       % Interactive ymid finder
       imagesc(Phi);%caxis([0 2*mean(mean(Phi))])
       [ymid zmid] = ginput(1);
       ymid = round(ymid);
       line([ymid ymid], [0 1e4], 'color', 'white'); drawnow
   end

end
%Phi = fliplr(Phi);
ysize = length(Phi(1,:))-1;
zsize = length(Phi(:,1));
h = micperpix*1e-6;

lambda = lambda*1e-9;

%n=1+k(rho/rho_atm)
%rho_atm=(P/RT)*Na
rho_atm=(101.325e3*6.022e23)/(8.31*273); %units m^-3

%ncrit for 800nm in m^-3
k = rho_atm/(2*1748e24);

zindicies = linspace(1, zsize, npoints); zindicies = round(zindicies);

for z_counter = 1:npoints,

   z_index = zindicies(z_counter);
   Philine = Phi(z_index,:);
   Philine = Philine(ymid:ysize);
   Philineold = Philine;
   Philine = [fliplr(Philine) Philine];
   yaxis = 1:length(Philine);
   Philine = csaps(yaxis, Philine, asmooth, yaxis);
   Philine = Philine(end/2 + 1:end);
   yaxis = 1:length(Philine);

   % This plots the smoothed and unsmoothed phase for comparison
   plot(Philineold, '.')
   hold on
   plot(Philine, 'red')
   hold off

   dPhidy = -gradient(Philine,1);
   dPhidy = dPhidy/h;
   % Remove singularities at r = 0
   dPhidy(1) = 0.1*dPhidy(2);

   for r_index = 1:ysize-ymid,
       for y_index = r_index:ysize - ymid + 1
           y = (double(y_index) - 0.9)*h;
           r = (double(r_index) - 1.0)*h;
           integrand(y_index - r_index + 1) = dPhidy(y_index)*(y^2 - r^2)^-0.5;   
       end

       rho(z_counter, r_index) = ((lambda*rho_atm)/(2*pi^2*k))*h*trapz(integrand);
       integrand(end) = [];
   end

end

rho = [fliplr(rho) rho];
rho = imrotate(rho, -90);

yaxis = length(rho(:,1));
yaxis = 1:yaxis;
yaxis = yaxis*h*1e3;
zaxis = zindicies*h*1e3;

if (plotflag == 1)
   %pcolor(zaxis, yaxis, rho)
   size(yaxis)
   size(zaxis)
   size(rho)
   pcolor(zaxis, yaxis, rho); shading flat
   xlabel('z /mm')
   ylabel('y /mm')
   axis image xy
end

end
