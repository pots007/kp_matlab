%energyInSpot.n
%Figuring out how much energy is within FWHM, e/1 and 1/e^2
%Checks for saturation and dismisses if saturation occurs

image = I;
if (max(max(double(image))) == 255)
    disp('Image saturated. Stoppping.')
    return
end

%first evaluate background
%For this assume the spot is more or less centered, and that the image
%itself is at least 640 by 480, giving at least 10 free pixels near the
%edge of the CCD
%variable BGwidth is the width of the strip used for BG evaluation

[rows cols] = size(image);
BGwidth = 40;
%Top and bottom edges
TopEdge = image([1:BGwidth],:);
TopEdgeMean = mean(mean(TopEdge));
BottomEdge = image([rows-BGwidth:rows],:);
BottomEdgeMean = mean(mean(BottomEdge));
%Side edges
RightEdge = image([(BGwidth+1):rows-(BGwidth+1)],[1:BGwidth]);
RightEdgeMean = mean(mean(RightEdge));
LeftEdge = image([(BGwidth+1):rows-(BGwidth+1)],[cols-(BGwidth+1):cols]);
LeftEdgeMean = mean(mean(LeftEdge));
BGcount = mean([TopEdgeMean BottomEdgeMean LeftEdgeMean RightEdgeMean])

%subtract BG from signal pixels and set all BG pixels to zero
BGMat = ones(rows, cols)*BGcount;
IMAGE = double(image) - BGMat;
%IMAGE(IMAGE<0) = 0;
totzerofrac = length(find(IMAGE<0))/(rows*cols);

%Or use I=imhist to get most probable pixel counts, which must be true bg.

%Finding Max value and location
[Maxs, Maxind] = max(IMAGE);
[MaxVal, xMax] = max(max(IMAGE));
yMax =  Maxind(xMax);

%Find centroid by weighting pixels by intensity
x = ones(rows,1)*[1:cols];
y = [1:rows]'*ones(1,cols);
totcounts = sum(sum(IMAGE));
xbar = sum(sum(IMAGE.*x))/totcounts;
ybar = sum(sum(IMAGE.*y))/totcounts;

figure
subplot(2,2,1)
imagesc(IMAGE); hold on
plot(xbar,ybar,'+')
plot(xMax, yMax, '*')
colorbar
subplot(2,2,3)
plot( IMAGE(yMax,:))
xlabel('x-lineout')
subplot(2,2,4)
plot(IMAGE(:,xMax))
xlabel('y-lineout')

%Zoom in on focus
%For later, determine the zoom by the width of the line out of max
FocZoom = IMAGE([yMax-50:yMax+50],[xMax-50:xMax+50]);
subplot(2,2,2)
imagesc(FocZoom)
colorbar; hold on

%Centroid of the focal zoom (recycling variables)
[rows cols] = size(FocZoom);
x = ones(rows,1)*[1:cols];
y = [1:rows]'*ones(1,cols);
totcounts = sum(sum(FocZoom));
xbar = sum(sum(FocZoom.*x))/totcounts;
ybar = sum(sum(FocZoom.*y))/totcounts;
plot(xbar, ybar, '*')

%Now take lineout through the found centroid to get 
%FWHM, 1/e and 1/e^2 values
xbar = round(xbar);
ybar = round(ybar);
%xLinOut = FocZoom(ybar,:);
%yLinOut = FocZoom(:,xbar);
totcounts = sum(sum(IMAGE));
Efwhm= sum(sum(FocZoom(FocZoom>MaxVal*0.5)))/totcounts;
title(['Efwhm = ',num2str(Efwhm)]);
E1e= sum(sum(FocZoom(FocZoom>MaxVal*exp(-1))))/totcounts;
%probs doesn't work for lower quotatients
E1ee= sum(sum(FocZoom(FocZoom>MaxVal*exp(-2))))/totcounts;