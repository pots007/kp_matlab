% focusContourTrace.m
%
% Finds the contour of three relevant intensity levels of a focal spot,
% by performaing an outward 'silly floodfill' starting from the maximum
% point. Currently there is no interpolation, so for very steep gradients
% the extracted contour will be an under-estimate.
%
% FOCUSCONTOURTRACE(imm, levels)
%           imm - image to find contours on. Must be background corrected.
%           levels - vector of levels to trace, eg [0.5, 0.2]
%
% Returns: cell array of the same length as levels, with each cell a 2
% columns array of [x1,y1; x2, y2,...]
%
% KP, DESY.

function cc = focusContourTrace(imm, levels)


% Find x and y indices of the max position
[my,mx] = find(imm==max(imm(:)));
my = my(1); mx = mx(1);

cc = cell(length(levels));
for l = 1:length(levels)
    % Variables to keep our results:
    coords = [];
    level = levels(l)*max(imm(:));
    % This defines the step direction we will do in each quarter.
    stepx = [1, -1, -1, 1]; stepy = [1 1 -1 -1];
    for k=1:4
        % Start indices for the loop
        ix0 = mx + 0; iy0 = my + 0;
        for i=1:100
            iy = iy0; ix = ix0;
            while imm(iy,ix) > level; ix=ix+stepx(k); end
            coords(end+1,:) = [ix, iy];
            ix = ix0;
            while imm(iy,ix)>level; iy=iy+stepy(k); end
            coords(end+1,:) = [ix, iy];
            ix0 = ix0+stepx(k); iy0 = iy0+stepy(k);
            %if ii>10; disp([ix0,iy0, imm(iy0,ix0)]); end
            if imm(iy0,ix0) < level; break; end
        end
    end
    % Filter out the repeats due to lazy initial point setting:
    cc{l} = unique(coords, 'rows');
end