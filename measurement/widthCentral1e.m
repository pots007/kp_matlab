% This script returns the total width of a lineout, where the width 2
% sigma, with the central fraction containing 68.27% of the total energy.
% For a normal distribution, this corresponds to a width of 2*sigma.

function [width_, edgeL, edgeR] = widthCentral1e(lineout)
% Check we have a line
if ~isvector(lineout)
    error('Need a vector input');
end
if size(lineout,2)==1 % Ensure we have row vector
    lineout = lineout';
end
edgeLevel = 0.5*(1-erf(1/sqrt(2)));
% And now down to business
% Find the left hand edge first
cumsumL = cumsum(lineout);
edgeL = find(cumsumL>edgeLevel*sum(lineout),1);
cumsumR = cumsum(fliplr(lineout));
edgeR = length(lineout) - find(cumsumR>edgeLevel*sum(lineout),1);
width_ = edgeR-edgeL;