% createBackground.m
%
% The function returns an image with the defined signal region replaced by
% a BG - this way everything outside the signal region will always be equal
% to 0 after BG subtraction.
%
% This function will create a background based on the method chosen:
% 1: Will just average BG regions
% 2: will make a natural griddata fit
% 3: will make a linear griddata fit
% 4: Like 1, but doesn't average over 0 valued pixels

function BG = createBackground(image, sigroi, BG1, BG2, method)

dat = image;
ss = sigroi;
if ss(2)+ss(4)>size(dat,1) || ss(1)+ss(3)> size(dat,2)
    warning('Signal ROI is larger than image dimension!');
end
switch method
    case 1
        dat(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3)) = nan;
        if isempty(BG1) && isempty(BG2)
            % If both are undefined, just average everything apart from the signal region
            dat(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3)) = nan;
            BGval = mean(dat(~isnan(dat)));
        else
            % If either or both are defined, average those
            if ~isempty(BG1)
                tt1 = dat(BG1(2):BG1(2)+BG1(4),BG1(1):BG1(1)+BG1(3));
            else
                tt1 = [];
            end
            if ~isempty(BG2)
                tt2 = dat(BG2(2):BG2(2)+BG2(4),BG2(1):BG2(1)+BG2(3));
            else
                tt2 = [];
            end
            BGval = mean([tt1 tt2]);
        end
        dat(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3)) = BGval;
        BG = dat;
    case 2
        dat(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3)) = nan;
        [Xq,Yq] = meshgrid(1:size(dat,2), 1:size(dat,1));
        X = Xq(~isnan(dat)); Y = Yq(~isnan(dat));
        dat1 = dat(~isnan(dat));
        BG = griddata(X,Y,dat1,Xq,Yq, 'linear');
    case 3
        % Sooooooooooooooooooooooooooooooooooooooooooon
    case 4
        dat(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3)) = nan;
        if isempty(BG1) && isempty(BG2)
            % If both are undefined, just average everything apart from the signal region
            dat(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3)) = nan;
            logm = ~isnan(dat) & dat~=0; % Ensure the zeros aren't averaged over either
            BGval = mean(mean(dat(logm)));
        else
            % If either or both are defined, average those
            if ~isempty(BG1)
                tt1 = dat(BG1(2):BG1(2)+BG1(4),BG1(1):BG1(1)+BG1(3));
                logm = tt1~=0 & ~isnan(tt1); % In case a dumbuser overlaps signal and BG!
                BG1 = mean(mean(tt1(logm)));
            else
                BG1 = [];
            end
            if ~isempty(BG2)
                tt2 = dat(BG2(2):BG2(2)+BG2(4),BG2(1):BG2(1)+BG2(3));
                logm = tt2~=0 & ~isnan(tt2); % In case a dumbuser overlaps signal and BG!
                BG2 = mean(mean(tt2(logm)));
            else
                BG2 = [];
            end
            BGval = mean([BG1 BG2]);
        end
        % Only subtract from non-zero pixels!
        logm = image(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3)) ~= 0;
        dat(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3)) = BGval*logm;
        BG = dat;
end