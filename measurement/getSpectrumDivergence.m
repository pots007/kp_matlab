% getSpectrumDivergence.m
%
% Quick function to return the FWHM divergence of an Espec trace. Assumes
% dispersion is along the horizontal direction. Returns a sub pixel,
% interpolated result, and optionally a gross, pixel accurate value.
%
% [div, div0] = getSpectrumDivergence(image, threshold, smoothFlag)
% Arguments:
%           image - input image. Dispersion must be along horizontal
%               direction.
%           threshold - level of image maximum below which no divergence is
%               calculated           
%           smoothFlag - flag to use smoothing. 0 mean sno smoothing, 1
%               means each line is smoothed before calculation, 2 means
%               medfilt2 is applied to entire image before also smoothing
%               each line.

function [div, div0] = getSpectrumDivergence(image, threshold, smoothFlag)
% Parse arguments:
% Default threshold of 5%.
if nargin<2; threshold = 0.05; end
% Smooth by default, only each line.
if nargin<3; smoothFlag = 1; end
nCols = size(image,2);
div = nan(1, nCols);
div0 = nan(2, nCols);
if smoothFlag == 2; image = medfilt2(image); end
immax = max(image(:));
for col=1:nCols
    this_col = image(:,col);
    if smoothFlag>0; this_col = smooth(this_col); end
    if max(this_col) < threshold*immax; continue; end
    div(col) = widthfwhm(this_col);
    ind = find(this_col>0.5*max(this_col), 1, 'first');
    if ~isempty(ind); div0(1,col) = ind; end
    ind = find(this_col>0.5*max(this_col), 1, 'last');
    if ~isempty(ind); div0(2,col) = ind; end
end
div0 = diff(div0, 1, 1);