% Fits a square to a measured field map, popping out the central coordinate
% for best fit.

% map is magnet map, mag_x and mag_y are magnet dimensions in pixels

function fout = fitMagnettoMap(map, mag_x, mag_y)

mmap = map; 
[X,Y] = size(mmap);
tx = zeros(1,Y); ty = zeros(1,X);
for i=1:Y
    % move thorugh each line and figure out which initial index contains
    % most integrated signal
    ttemp = zeros(1,X-mag_x);
    for ind=1:X-mag_x
        ttemp(ind) = abs(sum(mmap(ind:ind+mag_x,i)));
    end
    [~,tx(i)] = max(ttemp);
end
for i=1:X
    % move thorugh each line and figure out which initial index contains
    % most integrated signal
    ttemp = zeros(1,Y-mag_y);
    for ind=1:Y-mag_y
        ttemp(ind) = abs(sum(mmap(i,ind:ind+mag_y)));
    end
    [~,ty(i)] = max(ttemp);
end
figure(5); clf;
imagesc(mmap');
hold on;
plot(tx,1:Y, 'k.');
plot(tx+mag_x,1:Y, 'k.');
plot(tx+0.5*mag_x, 1:Y, 'ko');
plot(1:X, ty, 'k.');
plot(1:X, ty+mag_y, 'k.');
plot(1:X, ty+0.5*mag_y, 'ko');
hold off;
axis image
fout.xmm = tx+0.5*mag_x; 
fout.ymm = ty+0.5*mag_y;
[fout.xm, fout.ym] = polyxpoly((tx+0.5*mag_x),(1:Y), (1:X), (ty+0.5*mag_y));