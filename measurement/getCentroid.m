% Returns intensity weighted centroid location.
% Second, optional, argument is threshold below which pixels are regarded

function centroidlocs = getCentroid(I, varargin)

[rows, cols] = size(I);
im = double(I);
%Define BG level, under which pixels will  not count towards centroid.
if nargin==1
    BGfac = 0.15;
elseif nargin==2
    BGfac = varargin{1};
end
maxval = max(im(:));
im(im<maxval*BGfac)=0;
%Find centroid by weighting pixels by intensity
x = ones(rows,1)*(1:cols);
y = (1:rows)'*ones(1,cols);
totcounts = sum(im(:));
xbar = sum(sum(im.*x))/totcounts;
ybar = sum(sum(im.*y))/totcounts;
centroidlocs = [xbar, ybar];
end