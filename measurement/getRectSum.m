% getRectSum(ax, factor)

% Returns the total enclosed sum, scaled according to factor.

function val = getRectSum(ax, fact)

pos = getrect(ax);
pos = round(pos);
him = findobj(ax, 'Type', 'image');
if isempty(him)
    error('Need a plotted image!');
end
if length(him)>1; him = him(1); end;
cdat = get(him, 'CData');
% for cases when the rect is outside the image
if pos(1)<1; pos(1) = 1; end;
if pos(2)<1; pos(2) = 1; end;
if pos(1)+pos(3)>size(cdat,2); pos(3) = size(cdat,2)-pos(1); end;
if pos(2)+pos(4)>size(cdat,1); pos(4) = size(cdat,1)-pos(2); end;

val = fact*sum(sum(cdat(pos(2):pos(2)+pos(4),...
    pos(1):pos(1)+pos(3))));