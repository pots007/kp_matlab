% testFocusContourTrace.m
%
% Trace out floodfill type contour seeking
if 1
    imfile = '/home/kpoder/python/FocalSpotViewer/TargetChamberFocalSpot_20180208T132304_251.raw';
    im = ReadRAW16bit(imfile, [], [], [], 'DESY');
    imm = im(410:590, 490:660);
else
    imfile = '/home/kpoder/python/FocalSpotViewer/TargetChamberFocalSpot_20170316T105328_826.tif';
    im = double(imread(imfile));
    imm = medfilt2(im);
    imm = imm - mean(mean(imm(500:end,:)));
end
hfig = figure(43231); clf(hfig);
axx = makeNiceAxes(hfig);
imagesc(imm);
axis tight;
colormap(jet_white(256));

% Level:
level = 0.5*max(imm(:));

% ----------- Using contourc
tic
C = contourc(imm, level*[1 1]);
x = C(1,2:end); y = C(2,2:end);
%Remove points from your contour that are not actually part of the main
%contour - this is necessary when looking at noisy data
logm = abs(x-mean(x)) < 2*std(x) & abs(y-mean(y)) < 2*std(y);
x = x(logm); y = y(logm);

toc


% -----------  Now start with new stuff
tic
% Find x and y indices of the max position
[my,mx] = find(imm==max(imm(:)));
my = my(1); mx = mx(1);
% Variables to keep our results:
coords = [];

% This defines the step direction we will do in each quarter.
stepx = [1, -1, -1, 1]; stepy = [1 1 -1 -1];
for k=1:4
    % Start indices for the loop
    ix0 = mx + 0; iy0 = my + 0;
    for i=1:100
        iy = iy0; ix = ix0;
        while imm(iy,ix) > level; ix=ix+stepx(k); end
        coords(end+1,:) = [iy, ix];
        ix = ix0;
        while imm(iy,ix)>level; iy=iy+stepy(k); end
        coords(end+1,:) = [iy, ix];
        ix0 = ix0+stepx(k); iy0 = iy0+stepy(k);
        %if ii>10; disp([ix0,iy0, imm(iy0,ix0)]); end
        if imm(iy0,ix0) < level; break; end
    end
end
% Filter out the repeats due to lazy initial point setting:
coords = unique(coords, 'rows');
toc

% Plot everything
plot(x, y, '.k');
plot(axx, mx, my, 'rx');
plot(axx, coords(:,2), coords(:,1), 'mo');
%set(axx, 'XLim', [0 140], 'YLim', [0 210]);

% And now fit ellipse to both contours, check how similar they are.
fita = fit_ellipse_plain(x, y);
fitb = fit_ellipse_plain(coords(:,2), coords(:,1));

it = {'a', 'b', 'phi', 'X0', 'Y0'};
fprintf('%10s\t%10s\t%10s\t%10s\n', '', 'contourc', 'mine', 'ratio');
for k=1:length(it)
    fprintf('%10s\t%10.2f\t%10.2f\t%10.3f\n', it{k}, fita.(it{k}),...
        fitb.(it{k}),fita.(it{k})/fitb.(it{k}));
end