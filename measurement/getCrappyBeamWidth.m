% This script returns the total width of a lineout, where the width is the
% distance between edges, outside of which there is {1/e} of total energy. So
% to both left and right there {1/e} of energy.

function [width_, edgeL, edgeR] = getCrappyBeamWidth(lineout, varargin)
% Check we have a line
if ~isvector(lineout)
    error('Need a vector input');
end
if size(lineout,2)==1 % Ensure we have row vector
    lineout = lineout';
end
% Parse inputs
if nargin==1
    edgeLevel = exp(-2);
elseif nargin==2
    edgeLevel = varargin{1};
end
% Ensure edgeLevel isn't a silly value
if edgeLevel>=1; edgeLevel = exp(-2); end
% And now down to business
% Find the left hand edge first
cumsumL = cumsum(lineout);
edgeL = find(cumsumL>edgeLevel*sum(lineout),1);
cumsumR = cumsum(fliplr(lineout));
edgeR = length(lineout) - find(cumsumR>edgeLevel*sum(lineout),1);
width_ = edgeR-edgeL;