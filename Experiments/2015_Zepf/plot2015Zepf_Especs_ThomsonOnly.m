% plot2015Zepf_EspecsThomsonOnly.m
%
% Plot the thomson signal measured. Also look at the way Gianluca did the
% integration. Finally, try fitting a 2D Gaussian and looking at the total
% yield in that

load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'BGfree_data.mat'));
fdat = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'shotdata'));
nshots = 49;

GData = zeros(1, nshots);
EData = zeros(2560, nshots);

for i=1:nshots
    fprintf('Working on shot %i\n', i);
    gdat = GDataraw(:,:,i);
    GData(i) = sum(sum(gdat(270:600, 250:700)));
    edat = EDataraw(:,:,i);
    EData(:,i) = sum(edat(800:1100, :), 1);
end
Esigsum = sum(EDataraw, 1); Esigsum = sum(Esigsum, 2);
Esigsum = squeeze(Esigsum);

saveplots = 0;
savfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'GammaFits');

%% And plot all gamma signals
% But bear in mind that the total signal should be normalised to total
% electron yield.... So plot two axes
hfig = figure(503); clf(hfig); hfig.Color = 'w';
set(gca, 'NextPlot', 'add');
G_threshold = 2.e7;
G_low_threshold = 0.6e7;
%plot(1:49, GData, 'o-', [1 49], G_threshold*[1 1], '--k');
[axx, h1, h2] = plotyy(1:49, GData, 1:49, GData./Esigsum');
set(axx(1), 'NextPlot', 'add');
%plot(axx(1), [1 49], G_threshold*[1 1], '--k');
%plot(axx(1), [1 49], G_low_threshold*[1 1], '--k');
% Non = zeros(1,49); Non([12:15 17:29 35:39 45:49]) = 1;
% Replaced with the centrally controlle done
Non = double(fdat.Non);
Non(Non==0) = nan;
him = imagesc(1:49, -2e7:1e7:2e8, repmat(Non, 10, 1), 'Parent', axx(1));
uistack(him, 'down', 4);
set(h1, 'Marker', 'o'); set(h2, 'Marker', 'd');
set(axx(1), 'YLim', [-1e7 1.6e8], 'Box', 'on', 'Layer', 'top');
set(axx(2), 'YLim', [-0.1 1.6]);
colormap([1 1 1; 0.8 0.8 0.8])
grid on; grid minor;
xlabel('Shot number');
ylabel(axx(1), 'Gamma yield');
ylabel(axx(2), 'Gamma yield/Total charge');
make_latex(hfig); setAllFonts(hfig, 14);

if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'gamma_yield_KP'), '-pdf', '-nocrop', hfig);
end

% Now look at the level of the noise of the 'cold' shots.
h1.Visible = 'off'; ylabel(axx(1), '');
set(axx(1), 'YLim', [-1e6 1.6e8*0.05], 'Box', 'on', 'Layer', 'top');
set(axx(2), 'YLim', [-0. 1.6*0.025], 'NextPlot', 'add', 'YTick', 0:0.01:0.04);
normGData = GData./Esigsum';
coldBG = mean(normGData(Noff));
coldBGstd = std(normGData(Noff));
plot(axx(2), [0 50], [1 1]*coldBG, '--k');
plot(axx(2), [0 50], [1 1]*(coldBG-coldBGstd), ':k');
plot(axx(2), [0 50], [1 1]*(coldBG+coldBGstd), ':k');
axes(axx(2));
shadedErrorBar([0 50], [1 1]*coldBG, [1 1]*coldBGstd, '-k', 1);
if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'gamma_yield_BG'), '-png', '-m4', '-nocrop', hfig);
end

% New logical matrix for the Non shots:
Non_abovenoise = fdat.Non & normGData > (coldBG + coldBGstd);
save(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'shotdata'), 'Non_abovenoise', '-append');

%% Now investigate Gianluca's method, ie cutting out the central, saturated peak

Noptshots = fdat.NS_optimum(fdat.NS_optimum~=0);
locs = GetFigureArray(4, 2, [0.04 0.04 0.04 0.04], 0.05, 'across');

hfig = figure(513); clf(hfig); hfig.Color = 'w';
hax = [];
for i=1:8
    mval = max(max(GDataraw(:,:,Noptshots(i))));
    sum(sum(GDataraw(:,:,Noptshots(i))==mval))
    hax(i) = axes('Parent', hfig, 'Position', locs(:,i));
    imagesc(GDataraw(250:600,250:700, Noptshots(i)), 'Parent', hax(i));
    axis(hax(i), 'image')
    title(hax(i), sprintf('Shot %i', Noptshots(i)));
    
end
colormap([parula(256); 1 0 0])
set(hax, 'XTick', [], 'YTick', []);
linkaxes(hax, 'xy');

if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'gamma_shots_sat'), '-pdf', '-nocrop', hfig);
end

%% Now look at the signal levels with the central saturated region masked out
mask_rect = [461 412 49 25];
mask_x = mask_rect(1):(mask_rect(1)+mask_rect(3));
mask_y = mask_rect(2):(mask_rect(2)+mask_rect(4));

GDataMasked = zeros(1, nshots);

for i=1:nshots
    fprintf('Working on shot %i\n', i);
    gdat = GDataraw(:,:,i);
    gdat(mask_y, mask_x) = 0;
    GDataMasked(i) = sum(sum(gdat(270:600, 250:700)));
end

hfig = figure(503); clf(hfig); hfig.Color = 'w';
set(gca, 'NextPlot', 'add');
him = imagesc(1:49, -2e7:1e7:2e8, repmat(Non, 10, 1));
hh_ = plot(1:49, GDataMasked, '-ok', 1:49, GData, '-o');

legend(hh_, {'Masked data', 'Not masked'}, 'Location', 'Northeast');
xlabel('Shot number');
ylabel('Gamma yield / a.u.');
set(gca, 'Box', 'on', 'Layer', 'top', 'XLim', [0 50], 'YLim', [-0.1e8 2e8]);
grid on; grid minor;
make_latex(hfig); setAllFonts(hfig, 14);
colormap([1 1 1; 0.8 0.8 0.8])
if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'gamma_yield_masked'), '-pdf', '-nocrop', hfig);
end

%% This part will fit a 2D Gaussian across the remaining pixels

% Let's make the mask, hardcoded as ever...
x_ax = 7; y_ax = 7;
x_sep = 23.85; y_sep = 24;
nx = 11; ny = 12;
r0 = [353, 327];
masks = false(1024, 1024, nx*ny);
pixeldata = zeros(ny, nx);
% The 6.3 is taken from the drawing: thus the axes correspond to mm.
pixelaxisx = (1:nx)*6.3; pixelaxisy = (1:ny)*6.3;
[XX,YY] = meshgrid(1:1024, 1:1024);
[pixdat(:,:,1),pixdat(:,:,2)] = meshgrid(pixelaxisx, pixelaxisy);
it = 1;
for xx=1:nx
    for yy = 1:ny
        x0 = r0(1) + x_sep*(xx-1); y0 = r0(2) + y_sep*(yy-1);
        masks(:,:,it) = ((XX-x0)/x_ax).^2 + ((YY-y0)/y_ax).^2 <=1;
        it = it+1;
    end
end
% The function I want to fit.
fitFcn1 = @(a,x0,xw,y0,yw,x,y) a*exp(   -((x-x0).^2/(2*xw^2) + (y-y0).^2/(2*yw^2)) );
% But the background isn't fit too well: add another gaussian for the BG
fitFcn2 = @(a1,x01,xw1,y01,yw1,a2,x02,xw2,y02,yw2,x,y) a1*exp( -((x-x01).^2/(2*xw1^2) + (y-y01).^2/(2*yw1^2)) ) +...
    a2*exp(   -((x-x02).^2/(2*xw2^2) + (y-y02).^2/(2*yw2^2)) );

% And a container to save all the data about the fits. Saving numerical
% data only: shotID, a, x0, xw, y0, yw, R^2
fitNData_1G = zeros(length(fdat.NS_optimum), 7);
fitNData_2G = zeros(length(fdat.NS_optimum), 12);
% And let's save the data as well: putting the calculated data and it into
% cell arrays.
fitData = cell(length(fdat.NS_optimum), 3);

% How many gaussians fit flag:
twofitflag = 1;

% This is an array of shot numbers of all shots with N beam on:
Non_shots = fdat.Non.*(1:49);
Non_shots = Non_shots(Non_shots~=0);

it_g = 1;
for i=Non_shots%fdat.NS_optimum%24%1:nshots
    hfig = figure(763); clf(hfig);
    if twofitflag
        ax1 = subplot(2, 3, [1 4]);
    else
        ax1 = subplot(2, 2, [1 3]);
    end
    set(ax1, 'NextPlot', 'add');
    gdat = GDataraw(:,:,i);
    gdat(gdat>64000) = nan;
    % Plotting to understand what is happening...
    imagesc(gdat);
    B = bwboundaries(sum(masks,3));
    for k = 1:length(B)
        boundary = B{k};
        plot(boundary(:,2), boundary(:,1), 'k', 'LineWidth', .2)
    end
    % This is for actually sorting out a fit.
    it = 1;
    for xx = 1:nx
        for yy = 1:ny
            pixel = GDataraw(:,:,i).*masks(:,:,it);
            if sum(sum(pixel > 64000)) > 2
                pixeldata(yy,xx) = nan;
            else
                pixeldata(yy,xx) = sum(pixel(:));
            end
            it = it+1;
        end
    end
    % And now try to do the fit:
    [x_,y_,z_] = prepareSurfaceData(pixelaxisx, pixelaxisy, pixeldata);
    % THis is for plotting later on
    [x_p, y_p] = meshgrid(pixelaxisx, pixelaxisy);
    % Turns out this actually works pretty well!
    startpoint = [6e6, 30, 7, 40, 8];
    startpoint2 = [6e6, 40, 7, 30, 7, 1e6, 30, 20, 40, 20];
    % UPDATE 2018 Feb - try to set the maximum of the gaussian to max val
    startpoint(1) = max(pixeldata(:));
    startpoint2(1) = max(pixeldata(:));
    startpoint2(6) = startpoint2(1)/6;
    if i==17; startpoint(1) = 2e6; end
    [hfit1, gof1, ostr1] = fit([x_, y_], z_, fitFcn1,...
        'Startpoint',startpoint , 'Tolfun', 1e-8);
    [hfit2, gof2, ostr2] = fit([x_, y_], z_, fitFcn2,...
        'Startpoint',startpoint2 , 'Tolfun', 1e-8, 'Lower', zeros(size(startpoint)));
    
    % Plot a lot of things now.
    axis image;
    set(ax1, 'CLim', [0 50000], 'XLim', [320 620], 'YLim', [300 610]);
    title(sprintf('Gamma signal for shot %i', i));
    colorbar; xlabel('Pixel'); ylabel('Pixel');
    if twofitflag
        ax2 = subplot(2,3,2);
        imagesc(pixelaxisx, pixelaxisy, pixeldata)
        axis image
        title('Data'); colorbar; xlabel('x / mm'); ylabel('y / mm');
        ax3 = subplot(2,3,3);
        imagesc(pixelaxisx, pixelaxisy, pixeldata)
        axis image
        title('Data'); colorbar; xlabel('x / mm'); ylabel('y / mm');
        
        ax4 = subplot(2,3,5);
        imagesc(pixelaxisx, pixelaxisy, reshape((hfit1(x_p(:), y_p(:))), [length(pixelaxisy),length(pixelaxisx)]))
        axis image; colorbar; xlabel('x / mm'); ylabel('y / mm');
        title(sprintf('Fit, one gaussian, $r^2=%2.3f$', gof1.rsquare)); 
        ax5 = subplot(2,3,6);
        imagesc(pixelaxisx, pixelaxisy, reshape((hfit2(x_p(:), y_p(:))), [length(pixelaxisy),length(pixelaxisx)]))
        axis image; colorbar; xlabel('x / mm'); ylabel('y / mm');
        title(sprintf('Fit, two gaussians, $r^2=%2.3f$', gof2.rsquare)); 
        
        set([ax1 ax2 ax3 ax4 ax5], 'Box', 'on', 'YDir', 'normal', 'layer', 'top');
        set([ax3 ax2 ax4 ax5], 'CLim', [0 7e6]);
        colormap(jet_white(256));
        make_latex(hfig);
        drawnow;
        % And save the numerical fit data along wth the pixel data
        fitNData_2G(it_g,:) = [i, hfit2.a1, hfit2.x01, hfit2.xw1, hfit2.y01, hfit2.yw1,...
            hfit2.a2, hfit2.x02, hfit2.xw2, hfit2.y02, hfit2.yw2, gof2.rsquare];
        fitNData_1G(it_g,:) = [i, hfit1.a, hfit1.x0, hfit1.xw, hfit1.y0, hfit1.yw, gof1.rsquare];
        fitData{it_g,1} = pixeldata;
        fitData{it_g,2} = reshape((hfit1(x_p(:), y_p(:))), [length(pixelaxisy),length(pixelaxisx)]);
        fitData{it_g,3} = reshape((hfit2(x_p(:), y_p(:))), [length(pixelaxisy),length(pixelaxisx)]);
    else
        ax2 = subplot(2,2,2);
        imagesc(pixelaxisx, pixelaxisy, pixeldata)
        axis image
        title('Data'); colorbar; xlabel('x / mm'); ylabel('y / mm');
        ax3 = subplot(2,2,4);
        imagesc(pixelaxisx, pixelaxisy, reshape((hfit1(x_p(:), y_p(:))), [length(pixelaxisy),length(pixelaxisx)]))
        axis image
        title('2D Gaussian fit'); colorbar; xlabel('x / mm'); ylabel('y / mm');
        set([ax1 ax2 ax3], 'Box', 'on', 'YDir', 'normal', 'layer', 'top');
        set([ax3 ax2], 'CLim', [0 5e6]);
        colormap(jet_white(256));
        drawnow;
        % And save the numerical fit data.
        fitNData_1G(it_g,:) = [i, hfit1.a, hfit1.x0, hfit1.xw, hfit1.y0, hfit1.yw, gof1.rsquare];
        fitData{it_g,1} = pixeldata;
        fitData{it_g,2} = reshape((hfit2(x_p(:), y_p(:))), [length(pixelaxisy),length(pixelaxisx)]);
    end
    make_latex(hfig); setAllFonts(hfig, 13);
    % And saving, if we want to.
    if saveplots
        export_fig(fullfile(savfol, sprintf('%iG_Shot_%i', 1+twofitflag, i)), '-png', '-nocrop', hfig);
        savefig(hfig, fullfile(savfol, 'fig', sprintf('%iG_Shot_%i', 1+twofitflag, i)));
    end
    it_g = it_g+1;
end
if 0
    save(fullfile(savfol, 'fitData'), 'fitNData_1G', 'fitNData_2G', 'fitData',...
        'pixelaxisx', 'pixelaxisy', 'fitFcn1', 'fitFcn2');
end
%% Just to show off, let's plot data and the fits

hfig = figure(764); clf(hfig);
hfig.Position = [70 250 1800 690];
% If data not loaded, load it
% load(fullfile(savfol, 'fitData'))
locs = GetFigureArray(9, 3, [0.04 0.02 0.02 0.02], 0.005, 'down');
clear axx;
it = 1;
for i=1:length(Non_shots)
    axi = mod(i-1, 9) + 1;
    if mod((i-1),9) == 0
        clf(hfig); 
        it = 1;
        clear axx;
    end
    for k=[2 1 3]
        axx(axi,k) = axes('Parent', hfig, 'Position', locs(:,it)); it = it+1;
        imagesc(pixelaxisx, pixelaxisy, fitData{i,k});
        axis image;
    end
    set(axx(axi,:), 'CLim', [0 max(max(cell2mat(get(axx(axi,:), 'CLim'))))]);
    title(axx(axi,2), sprintf('Shot %i', fitNData_2G(i,1)));
    
    colormap(jet_white(256));
    set(axx, 'XTickLabel', [], 'YTickLabel', []);
    ylabel(axx(1,2), 'One gaussian fit');
    ylabel(axx(1,1), 'Data');
    ylabel(axx(1,3), 'Two gaussian fits');
    make_latex(hfig); setAllFonts(hfig, 13);
    drawnow
    if saveplots
        if i==9
            export_fig(fullfile(savfol, 'Fits_all'), '-pdf', '-nocrop', hfig);
        elseif i==18 || i==27
            export_fig(fullfile(savfol, 'Fits_all'), '-pdf', '-nocrop', '-append', hfig);
        end
    end
end
%% And let's plot some things against some other things now.

hfig = figure(765); clf(hfig);
%plot(fitNData_2G(:,1), fitNData_2G(:,[4 6 9 11]), 'o-'); 
sig_w = sqrt(abs(prod(fitNData_2G(:,[4 6]),2)));
bg_w = sqrt(abs(prod(fitNData_2G(:,[9 11]), 2)));
scatter(sig_w, bg_w, 45, cellfun(@(x) sum(x(:)), fitData(:,3))', 'filled');
set(gca, 'XLim', [0 5], 'Box', 'on');
grid on;
colormap(hsv);
cb = colorbar;
xlabel('Signal width $w_s = \sqrt{w_xw_y}$ (mm)');
ylabel('Background width $w_s = \sqrt{w_xw_y}$ (mm)');
ylabel(cb, 'Signal integral (a.u.)');
tt = text(sig_w, bg_w, num2str(fitNData_2G(:,1)));
make_latex(hfig); setAllFonts(hfig, 14);
set(tt, 'FontSize', 8, 'VerticalAlignment', 'top');
if saveplots
    export_fig(fullfile(savfol, 'Fit_widths'), '-pdf', '-nocrop', hfig);
    export_fig(fullfile(savfol, 'Fit_widths'), '-png', '-nocrop', hfig);
end
%% And plot all gamma signals, but now with integral of fit!
% But bear in mind that the total signal should be normalised to total
% electron yield.... So plot two axes
Esigsum = sum(EDataraw, 1); Esigsum = sum(Esigsum, 2);
Esigsum = squeeze(Esigsum);
fitFcn2 = @(a1,x01,xw1,y01,yw1,a2,x02,xw2,y02,yw2,x,y) a1*exp( -((x-x01).^2/(2*xw1^2) + (y-y01).^2/(2*yw1^2)) ) +...
    a2*exp(   -((x-x02).^2/(2*xw2^2) + (y-y02).^2/(2*yw2^2)) );
[FX, FY] = meshgrid(-25:100, -25:100);
fitInts = zeros(2, size(fitNData_2G, 1));
for k=1:size(fitInts,2)
    fitInts(1,k) = fitNData_2G(k,1);
    f = fitNData_2G(k,:);
    fitSig = fitFcn2(f(2), f(3), f(4), f(5), f(6), f(7), f(8), f(9), f(10), f(11), FX, FY);
    fitInts(2,k) = sum(fitSig(:));
end

hfig = figure(503); clf(hfig); hfig.Color = 'w';
set(gca, 'NextPlot', 'add');
G_threshold = 2.e7;
G_low_threshold = 0.6e7;
%plot(1:49, GData, 'o-', [1 49], G_threshold*[1 1], '--k');
[axx, h1, h2] = plotyy(1:49, GData, 1:49, GData./Esigsum');
set(axx, 'NextPlot', 'add');
plot(axx(1), [1 49], G_threshold*[1 1], '--k');
plot(axx(1), [1 49], G_low_threshold*[1 1], '--k');

% Non = zeros(1,49); Non([12:15 17:29 35:39 45:49]) = 1;
% Replaced with the centrally controlle done
Non = double(fdat.Non);
Non(Non==0) = nan;
him = imagesc(1:49, -2e7:1e7:2e8, repmat(Non, 10, 1), 'Parent', axx(1));
uistack(him, 'down', 4);
set(h1, 'Marker', 'o'); set(h2, 'Marker', 'o');
% And plot the results from the fitting.... arbitrarily scaled!
plot(axx(1), fitInts(1,:), fitInts(2,:)*6e-2, 'd', 'Color', h1.Color);
plot(axx(2), fitInts(1,:), fitInts(2,:)*6e-2./Esigsum(fitInts(1,:))', 'd', 'Color', h2.Color);
set(axx(1), 'YLim', [-1e7 2.8e8], 'Box', 'on', 'Layer', 'top');
set(axx(2), 'YLim', [-0.1 1.6]);
colormap([1 1 1; 0.8 0.8 0.8])
grid on; grid minor;
xlabel('Shot number');
ylabel(axx(1), 'Gamma yield');
ylabel(axx(2), 'Gamma yield/Total charge');
make_latex(hfig); setAllFonts(hfig, 14);

if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'gamma_yield_KP'), '-pdf', '-nocrop', hfig);
end
