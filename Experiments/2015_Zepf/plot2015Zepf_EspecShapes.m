% plot2015Zepf_EspecShapes.m
%
% Aiming to look at whether the spectra look self-similar, when normalised
% to total charge and cutoff energy.

% Set up tracking
%trackfile = 'Tracking_20160524_KP_4mrad.mat';
trackfile = 'Tracking_20170309_KP_4mrad.mat';

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

% Load stuff
load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'BGfree_data.mat'));
fSdat = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'shotdata'));
datfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis',...
    '20151109r003', 'mat');
flist = dir(fullfile(datfol, '*.mat'));
nshots = 49;

laserE_S = cell2mat({fSdat.Zepf2015.laserE_S});
laserE_N = cell2mat({fSdat.Zepf2015.laserE_N});

saveplots = 0;
dEflag = 0;

savfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'SpectralShape');

%% Find cutoffs, and normalise to charge and cutoff

maxens = zeros(nshots, 1);
cutoffs = nan(nshots, 2); % This will contain cutoffs at 2% and 10%?
specs = zeros(nshots, 1572);
divs = nan(nshots, 1572); % This will contain divergences - defined only for higher than 2% of peak

for i=1:nshots
    fdat = load(fullfile(datfol, flist(i).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen)';
    dE = gradient(enax);
    if dEflag
        spec = smooth(sum(fdat.shotdata.image, 1)./dE);
    else
        spec = smooth(sum(fdat.shotdata.image, 1)./dE.*enax);
    end
    nfac = sum(spec); % Normalise to total charge
    specs(i, :) = spec./nfac;
    nfac = max(spec);
    spec = spec./nfac;
    % Find the index of the point closest to 2% level
    if dEflag
        [~, minind] = min(abs(spec - 0.02));
    else
        [~, minind] = min(abs(spec - 0.05));
    end
    if ~isempty(minind); maxens(i) = enax(minind); end;
    % And now for the divergences
    if i==1; continue; end;
    imm = fdat.shotdata.image;
    imm = medfilt2(imm);
    immmax = max(imm(:));
    for k=1:size(imm,2)
        if max(imm(:,k))<0.05*immmax; continue; end;
        try 
            divs(i,k) = widthfwhm(smooth(imm(:,k))'); 
        catch
            continue; 
        end
    end
end

%% 

hfig = figure(598); clf(hfig); %hfig.Position = [100 300 1300 350];
set(gca, 'NextPlot', 'add');

Noff_shots = fSdat.Noff.*(1:nshots);
Noff_shots = Noff_shots(Noff_shots~=0);
Noff_ens = laserE_S(fSdat.Noff);
clear hh;
% Plot with colors according to laser energy bins, and offset as well
colz = brewermap(9, 'Set1'); colz(6,:)= [];
% Have energies ranging from 10.5 to 16.3. Use 1.5J bins...
bins_ = 10.5:1.5:17;
% Trying out a crazy idea - bins with equal entries.... No physical ground
% for this, though
Noff_ens_s = sort(Noff_ens);
%bins_ = [Noff_ens_s([1:5:20]), 17]
comm_x0 = 0:0.001:1.2;
clear hh_means;
clear fdata;
for k=2%:(length(bins_)-1)
    logm = Noff_ens >= bins_(k) & Noff_ens <= bins_(k+1);
    s_loc = Noff_shots(logm);
    clear hh;
    for i=1:length(s_loc)
        shotid = s_loc(i);
        xaxx = enax./maxens(shotid); %xaxx(xaxx>1.2) = nan;
        hh(i) = plot(xaxx+k-1, specs(shotid, :), '--');
    end
    set(hh, 'Color',colz(k,:));
    
    % And now, try to add confusion by plotting the means
    % Need to interpolate all the shots onto a common x-grid...
    ydat = zeros(sum(logm), length(comm_x0));
    comm_x = comm_x0+k-1;
    for i=1:sum(logm)
        ydat(i,:) = interp1(get(hh(i), 'XData'), get(hh(i), 'YData'), comm_x);
    end
    if size(ydat,1) == 1
        tt = shadedErrorBar(comm_x, ydat, ones(size(ydat))*1e-6, {'-', 'Color', colz(k,:)}, 1);
    else
        tt = shadedErrorBar(comm_x, mean(ydat), std(ydat), {'-', 'Color', colz(k,:)}, 1);
    end
    hh_means(k) = tt.mainLine;
    
    fdata.energyBins(k).enRange = sprintf('%1.1f J < E < %1.1fJ', bins_(k:k+1));
    fdata.energyBins(k).normEnergyAxis = comm_x0;
    fdata.energyBins(k).normSignal_dQ_dE_E = mean(ydat);
    fdata.energyBins(k).normSignalStdDev_dQ_dE_E = std(ydat);
end
leglabs = {};
for i=1:(length(bins_)-1)
    leglabs{i} = sprintf('$ %1.1f\\, \\mathrm{J} < E < %1.1f\\, \\mathrm{J}$',...
        bins_(i:i+1));
end
legend(hh_means, leglabs, 'Location', 'North', 'Orientation', 'horizontal');

grid on; grid minor;
set(gca, 'Box', 'on', 'XLim', [0 length(bins_)-0.8], 'YLim', [-0.1 15]*1e-4);
set(hh_means, 'LineWidth', 1.5);
xlabel('Normalised energy');
ylabel('Signal / a.u.');
title('Average energy and total charge normalised cold shots');

make_latex(hfig); setAllFonts(hfig, 14);
if saveplots
    export_fig(fullfile(savfol, 'espec_norm_cold'),'-png', '-nocrop', hfig);
end

%% And now add all the hot shots to those plots...

Non_shots = fSdat.Non.*(1:nshots);
Non_shots = Non_shots(Non_shots~=0);
Non_ens = laserE_S(fSdat.Non);
Non_hot = fSdat.NS_optimum;

for k=1:(length(bins_)-1)
    logm = Non_ens > bins_(k) & Non_ens < bins_(k+1);
    s_loc = Non_shots(logm);
    hh = findobj(gca, 'Type', 'Line', 'Color', colz(k,:), 'LineStyle', '--');
    delete(hh);
    clear hh;
    if isempty(s_loc); continue; end;
    
    for i=1:length(s_loc)
        shotid = s_loc(i);
        xaxx = enax./maxens(shotid);
        % Plot super hot shots differently
        if isempty(find(Non_hot==shotid,1));
            hh(i) = plot(xaxx+k-1, specs(shotid, :), '--', 'Color', colz(k,:));
        else
            hh(i) = plot(xaxx+k-1, specs(shotid, :), '-k');
        end
    end
    
    fprintf('%i shots (%i superhot) in bin %i\n', length(s_loc),...
        length(findobj(gca, 'LineStyle', '-', 'Color', 'k')), k);
    
end
set(gca, 'XLim', [0 length(bins_)-0.8]);
axx_all = gca;
%set(gca, 'YScale', 'log', 'YLim', [1e-2 1]*15e-4)
title('Average energy and total charge normalised HOT shots');

if saveplots
    export_fig(fullfile(savfol, 'espec_norm_hot'),'-png', '-nocrop', hfig);
end

%% For extra clarity, plot all these in individual figures.

for k=1:3
    hfig = figure(590+k); clf(hfig); 
    hfig.Position = [100 100 1400 700];
    copyobj(axx_all, hfig);
    set(gca, 'XLim', [-0.87 0.2]+k);
    drawnow;
    if saveplots
        export_fig(fullfile(savfol, sprintf('espec_norm_hot%i', k)), '-png', '-nocrop', hfig);
    end
end



%%




%% Try again, but using every hot-hotshot as a reference and averaging shots around it


hfig = figure(596); clf(hfig); %hfig.Position = [100 300 1300 350];
locs = GetFigureArray(4, 2, [0.05 0.03 0.05 0.05], 0.01, 'across');

Noff_shots = fSdat.Noff.*(1:nshots);
Noff_shots = Noff_shots(Noff_shots~=0);
Noff_ens = laserE_S(fSdat.Noff);
clear hh;
% Plot with colors according to laser energy bins, and offset as well
colz = brewermap(9, 'Set1'); colz(6,:) = [];
comm_x0 = 0:0.001:1.2;
clear hh_means;
binW = 0.5;
hotE = laserE_S(fSdat.NS_optimum);

for k=1:length(hotE)
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add');
    logm = Noff_ens >= (hotE(k)-binW) & Noff_ens <= (hotE(k)+binW);
    s_loc = Noff_shots(logm);
    clear hh;
    for i=1:length(s_loc)
        shotid = s_loc(i);
        xaxx = enax./maxens(shotid); %xaxx(xaxx>1.2) = nan;
        hh(i) = plot(axx(k), xaxx, specs(shotid, :), '--');
    end
    set(hh, 'Color',colz(k,:));
    % And now, try to add confusion by plotting the means
    % Need to interpolate all the shots onto a common x-grid...
    %xdat = get(hh, 'XData');
    %ydat = get(hh, 'YData');
    ydat = zeros(sum(logm), length(comm_x0));
    comm_x = comm_x0;
    for i=1:sum(logm)
        ydat(i,:) = interp1(get(hh(i), 'XData'), get(hh(i), 'YData'), comm_x);
    end
    if size(ydat,1) == 1
        tt = shadedErrorBar(comm_x, ydat, ones(size(ydat))*1e-6, {'-', 'Color', colz(k,:)}, 1);
    else
        tt = shadedErrorBar(comm_x, mean(ydat), std(ydat), {'-', 'Color', colz(k,:)}, 1);
    end
    hh_means(k) = tt.mainLine;
    grid on; grid minor;
    text(1.15, 11e-4, {['Shot ' num2str(fSdat.NS_optimum(k))],...
        sprintf('E=%2.2fJ, $\\Delta$E=%2.1fJ', hotE(k), binW)}, 'HorizontalAlignment', 'right');
end

set(axx, 'Box', 'on', 'XLim', [0.2 1.2], 'YLim', [-0.1 13]*1e-4,...
    'YTickLabel', [], 'XTickLabel', []);
set(hh_means, 'LineWidth', 1.5);

make_latex(hfig); setAllFonts(hfig, 14);
if saveplots
    export_fig(fullfile(savfol, 'espec_normInd_cold'),'-png', '-nocrop', hfig);
end

%% And now add all the hot shots to those plots...

Non_shots = fSdat.Non.*(1:nshots);
Non_shots = Non_shots(Non_shots~=0);
Non_ens = laserE_S(fSdat.Non);
Non_hot = fSdat.NS_optimum;

for k=1:length(hotE)
    logm = Non_ens >= (hotE(k)-binW) & Non_ens <= (hotE(k)+binW);
    s_loc = Non_shots(logm);
    hh = findobj(axx(k), 'Type', 'Line', 'Color', colz(k,:), 'LineStyle', '--');
    delete(hh);
    clear hh;
    if isempty(s_loc); continue; end;
    
    for i=1%:length(s_loc)
        shotid = s_loc(i);
        xaxx = enax./maxens(shotid);
        % Plot super hot shots differently
        if isempty(find(Non_hot==shotid,1));
            hh(i) = plot(axx(k), xaxx, specs(shotid, :), '--', 'Color', colz(k,:));
        else
            hh(i) = plot(axx(k), xaxx, specs(shotid, :), '-k');
        end
    end
    
    fprintf('%i shots (%i superhot) in bin %i\n', length(s_loc),...
        length(findobj(gca, 'LineStyle', '-', 'Color', 'k')), k);
end
%set(gca, 'XLim', [0 length(bins_)-0.8]);
%set(gca, 'YScale', 'log', 'YLim', [1e-2 1]*15e-4)
%title('Average energy and total charge normalised HOT shots');
if saveplots
    export_fig(fullfile(savfol, 'espec_normInd_hot'),'-png', '-nocrop', hfig);
end

%%












%% Plot the divergences ina manner similar to above

hfig = figure(594); clf(hfig); %hfig.Position = [100 300 1300 350];
set(gca, 'NextPlot', 'add');

Noff_shots = fSdat.Noff.*(1:nshots);
Noff_shots = Noff_shots(Noff_shots~=0);
Noff_ens = laserE_S(fSdat.Noff);
clear hh;
% Plot with colors according to laser energy bins, and offset as well
colz = brewermap(9, 'Set1'); colz(6,:)= [];
% Have energies ranging from 10.5 to 16.3. Use 1.5J bins...
bins_ = 10.5:1.5:17;
% Trying out a crazy idea - bins with equal entries.... No physical ground
% for this, though
Noff_ens_s = sort(Noff_ens);
%bins_ = [Noff_ens_s([1:5:20]), 17]
comm_x0 = 0:0.001:1.2;
clear hh_means;
for k=1:(length(bins_)-1)
    logm = Noff_ens >= bins_(k) & Noff_ens <= bins_(k+1);
    s_loc = Noff_shots(logm);
    clear hh;
    for i=1:length(s_loc)
        shotid = s_loc(i);
        xaxx = enax/2000;%./maxens(shotid); %xaxx(xaxx>1.2) = nan;
        hh(i) = plot(xaxx, (divs(shotid, :))+(k-1)*20, '--');
    end
    set(hh, 'Color',colz(k,:));
    
    % And now, try to add confusion by plotting the means
    % Need to interpolate all the shots onto a common x-grid...
    ydat = zeros(sum(logm), length(comm_x0));
    comm_x = comm_x0;
    for i=1:sum(logm)
        ydat(i,:) = interp1(get(hh(i), 'XData'), get(hh(i), 'YData'), comm_x);
    end
    if size(ydat,1) == 1
        tt = shadedErrorBar(comm_x, ydat, ones(size(ydat))*1e-6, {'-', 'Color', colz(k,:)}, 1);
    else
        tt = shadedErrorBar(comm_x, mean(ydat, 'omitnan'), std(ydat, 'omitnan'), {'-', 'Color', colz(k,:)}, 1);
    end
    hh_means(k) = tt.mainLine;
end
leglabs = {};
for i=1:(length(bins_)-1)
    leglabs{i} = sprintf('$ %1.1f\\, \\mathrm{J} < E < %1.1f\\, \\mathrm{J}$',...
        bins_(i:i+1));
end
legend(hh_means, leglabs, 'Location', 'North', 'Orientation', 'horizontal');

grid on; grid minor;
set(gca, 'Box', 'on', 'XLim', [0.2 1.2], 'YLim', [10 120]);
set(hh_means, 'LineWidth', 1.5);
xlabel('Normalised energy');
ylabel('Divergence / a.u.');
title('Average energy and total charge normalised cold shots');

make_latex(hfig); setAllFonts(hfig, 14);
if saveplots
    export_fig(fullfile(savfol, 'espec_norm_cold'),'-png', '-nocrop', hfig);
end

%% And now add all the hot shots to those plots...

Non_shots = fSdat.Non.*(1:nshots);
Non_shots = Non_shots(Non_shots~=0);
Non_ens = laserE_S(fSdat.Non);
Non_hot = fSdat.NS_optimum;

for k=1:(length(bins_)-1)
    logm = Non_ens > bins_(k) & Non_ens < bins_(k+1);
    s_loc = Non_shots(logm);
    hh = findobj(gca, 'Type', 'Line', 'Color', colz(k,:), 'LineStyle', '--');
    delete(hh);
    clear hh;
    if isempty(s_loc); continue; end;
    
    for i=1:length(s_loc)
        shotid = s_loc(i);
        xaxx = enax./2000;%maxens(shotid);
        % Plot super hot shots differently
        if isempty(find(Non_hot==shotid,1));
            hh(i) = plot(xaxx, (divs(shotid, :))+(k-1)*20, '--', 'Color', colz(k,:));
        else
            hh(i) = plot(xaxx, (divs(shotid, :))+(k-1)*20, '-k');
        end
    end
    
    fprintf('%i shots (%i superhot) in bin %i\n', length(s_loc),...
        length(findobj(gca, 'LineStyle', '-', 'Color', 'k')), k);
end
set(gca, 'YLim', [10 85]);
axx_all = gca;
%set(gca, 'YScale', 'log', 'YLim', [1e-2 1]*15e-4)
title('Average energy and total charge normalised HOT shots');

if saveplots
    export_fig(fullfile(savfol, 'espec_norm_hot'),'-png', '-nocrop', hfig);
end

%% Denormalise everything

Noff_shots = fSdat.Noff.*(1:nshots);
Noff_shots = Noff_shots(Noff_shots~=0);
Noff_ens = laserE_S(fSdat.Noff);
clear hh;
% Have energies ranging from 10.5 to 16.3. Use 1.5J bins...
bins_ = 10.5:1.5:17;
% Trying out a crazy idea - bins with equal entries.... No physical ground
% for this, though
Noff_ens_s = sort(Noff_ens);
%bins_ = [Noff_ens_s([1:5:20]), 17]
comm_x0 = 0:0.001:1.2;
clear hh_means;
for k=1:(length(bins_)-1)
    logm = laserE_S >= bins_(k) & laserE_S <= bins_(k+1);
    s_loc = maxens(logm);
    fprintf('Mean energy in bin %i: %2.2f +- %2.2f\n', k, mean(s_loc), std(s_loc));
end

% And save them to a file for folks
load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'GammaFits', 'fitData'))
fdata.gammaFits = fitNData_2G;