% focalSpotSum_Zepf2015.m
%
% This is for the Zepf run, looking at the 'day of the data'


dataf = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'Focus', '20151109');
ax_calib = 0.2;

flist = dir(fullfile(dataf, '*F2*'));
cmap = brewermap(256, 'YlGnBu');
hfig = figure(8); colormap(8, cmap);
totim = zeros(301,301,length(flist));
elldata = zeros(length(flist), 4);
hfilter = ones(5)./5;
for i=1:1:(length(flist)+1)
    if i==(length(flist)+1)
        im = mean(totim(:,:,logical(elldata(:,4))),3);
    else
        %im = ReadRAW16bit(fullfile(dataf, flist(i).name), 640, 480);
        im = double(imread(fullfile(dataf, flist(i).name)));
        %im(220:260, 35:65) = 0; im(162:169, 106:110) = 0;
        %im(329, 62) = 0;
    end
    im = im-mean(mean(im(:,1:100)));
    im(im<0) = 0;
    im = imfilter(im,hfilter);
    im = im/max(im(:));
    figure(8);
    imagesc(im);
    C = contourc(im, 0.5*[1 1]);
    x = C(1,:);
    y = C(2,:);
    lengthx = length(x);
    k = 1;
    %Remove points from your contour that are not actually part of the main
    %contour - this is necessary when looking at noisy data
    while k < lengthx
        if(abs(x(k) - mean(x)) > 2*std(x) || abs(y(k) - mean(y)) > 2*std(y))
            x(k) = [];
            y(k) = [];
            lengthx = lengthx - 1;
        end
        k = k+1;
    end
    ell = fit_ellipse(x,y, gca);
    elldata(i,1) = ell.long_axis;
    elldata(i,2) = ell.short_axis;
    elldata(i,3) = ell.phi;
    elldata(i,4) = ell.long_axis < 1.5*mean(elldata(1:i-1,1));
    logx = (-150:150)+round(ell.X0_in);
    logy = (-150:150)+round(ell.Y0_in);
    if i<201
        totim(:,:,i) = imrotate(im(logy, logx), rad2deg(-ell.phi), 'nearest', 'crop');
    end
    drawnow;
    %f = getframe(gca);
    %[imm,map] = rgb2ind(f.cdata,256,'nodither');
    %imm(:,:,1,i) = rgb2ind(f.cdata,map,'nodither');
end
%
%imwrite(imm,map,'FocalSpots.gif','DelayTime',0,'LoopCount',inf);
%logm = (elldata(:,1)~=0);
logm = logical(elldata(:,4));
elldata = elldata(logm,:);
fprintf('Mean long axis: %2.2f, mean short axis: %2.2f, mean angle: %2.2f\n',...
    mean(elldata(1:end-1,1:3)));
fprintf('Mean long axis: %2.2f, mean short axis: %2.2f, mean angle: %2.2f\n',...
    ell.long_axis, ell.short_axis, ell.phi);
fprintf('Ratio of measured to summed long axis: %2.2f, short axis: %2.2f, mean angle: %2.2f\n',...
    mean(elldata(1:end-1,1:3))./[ell.long_axis ell.short_axis ell.phi]);
% And now make the final plot a wee bit nicer and save
title(sprintf('Compound image: a=%2.1f um, b = %2.1f um', ell.long_axis*ax_calib, ax_calib*ell.short_axis));
set(gca, 'XTick', [], 'YTIck', []);
setAllFonts(8,20); make_latex(hfig);
axis image;
%export_fig(fullfile(dataf(1:end-9), 'CompoundFocus'), '-nocrop', '-pdf', 8);
%% Now look whether this is Gaussian at all
figure(9); clf(9);
set(9, 'Color', 'w');
colormap(9, cmap);
spotim = sqrt(im);
cm = centroidlocationsmatrix(im);
xaxx = ax_calib*((1:size(spotim,2))-cm(1));
yaxx = ax_calib*((1:size(spotim,1))-cm(2));
imagesc(xaxx,yaxx,spotim);
set(gca, 'YDir', 'normal');
hold on;
plot(xaxx, spotim(151,:)*37-20, '--k');
plot(spotim(:,151)*37-20, yaxx, '--k');
hold off;
axis image;
set(gca, 'XLim', [-20 20], 'YLim', [-20 20]);
%spot2 = fresnelPropagator(sqrt(spotim), 0.88e-6, 0.88e-6, 0.00, 0.8e-6);
%imagesc(abs(spot2))

save('focalSpotData', 'im', 'xaxx', 'yaxx')