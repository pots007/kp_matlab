% analyseZepf2015_pointingEffects.m
%
% This script will use the centre point of the fitted gamma signal and try
% to correct for espec pointing offset while pointing essentially Fig3a.
%
% Will use an average of the 9 hottest shots for the hot ones and assume
% the cold ones all average out to be around 0.

% Load stuff
% Contains pointing offsets and integrated signal of the fit
fg = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'gamma_fit_analysis'));
% Data about shots on/off
shotData = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'shotdata'));
% List of espec files, for now. Will use the fixed spectra later on.
edatafol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis', ...
    '20151109r003', 'mat');
edatalist = dir(fullfile(edatafol, '*Espec.mat'));
% List of gamma files.
gdatafol = fullfile(getGDrive, 'Experiment', '2015_Zepf', '20151109r003');
gdatalist = dir(fullfile(gdatafol, '*GammaDet2.tif'));
% Simulation data from Matteo
simf = load(fullfile(getDropboxPath, 'Writing', 'Radiation_Reaction_Paper',...
    'Fig3A_plot_data20180414'));

divThet = 0.5;

% ---------------- Select the cold shots.
hotShots = [12 13 14 15 17 18 23 24 26];
runShots = 1:49;
coldShotPool = ~shotData.Non.*runShots;
coldShotPool = coldShotPool(coldShotPool~=0);
laserE_S = cell2mat({shotData.Zepf2015.laserE_S});
coldShotEnergies = laserE_S(coldShotPool);
hotCold = cell(length(hotShots),2);
enDiffLimit = 0.5; % The energy range we look into
for s=1:length(hotShots)
    hotCold{s,1} = hotShots(s);
    closestEns = laserE_S - laserE_S(hotShots(s));
    suitables = runShots(abs(closestEns) < enDiffLimit & ~shotData.Non_abovenoise);
    fprintf('For shot %i (E=%2.2fJ):\n', hotShots(s), laserE_S(hotShots(s)))
    for k=1:length(suitables)
        fprintf('\tshot %2i (E=%2.2fJ)\n', suitables(k), laserE_S(suitables(k)));
    end
    if hotShots(s)==15; suitables= suitables(1:2); end % Filter out the shite ones
    hotCold{s,2} = suitables;
end
%
coldShots = hotCold(:,2);

shotsToRemove = [23];
for k=1:length(shotsToRemove)
    extind = find(hotShots==shotsToRemove(k));
    if isempty(extind); continue; end
    hotShots(extind) = []; coldShots(extind) = [];
end
useFixedSpec = 0;


% Main analysis and whatnot.
edata = zeros(length(hotShots),3);
gdata = zeros(length(hotShots),3);
for s = 1:length(hotShots)
    % Calculate the energies in the cold shot
    coldEnergy = zeros(length(coldShots{s}), 3);
    coldGamma = zeros(size(coldShots{s}));
    coldQ = zeros(size(coldShots{s}));
    for k=1:length(coldShots{s})
        fdat = load(fullfile(edatafol, edatalist(coldShots{s}(k)).name));
        if useFixedSpec
            % Find the right element
            ind = find(coldShots{s}(k)==shotsWeHave);
            alongs = fSpec.fixedSpec(ind).alongscreen;
            spec = fSpec.fixedSpec(ind).spec';
        else
            alongs = fdat.shotdata.alongscreen;
            spec = fdat.shotdata.dQ_dx;
        end
        %alongs(alongs < 22) = 0; % avoiding the turnaround...
        enax = getZepf2015ScreenEnergy(alongs, 2, 4); % Using default direction, 4mrad for cold shots
        lowax = getZepf2015ScreenEnergy(alongs, 2, 4-divThet);
        higax = getZepf2015ScreenEnergy(alongs, 2, 4+divThet);
        coldEnergy(k,2) = sum(spec.*enax);
        % Then the lower limit
        %coldEnergy(k, 1) = sum(fdat.shotdata.dQ_dx'.*tracking.lowfit(fdat.shotdata.alongscreen));
        coldEnergy(k, 1) = sum(spec.*lowax);
        % And the upper limit
        %coldEnergy(k, 3) = sum(fdat.shotdata.dQ_dx'.*tracking.highfit(fdat.shotdata.alongscreen));
        coldEnergy(k, 3) = sum(spec.*higax);
        coldQ(k) = fdat.shotdata.totQ;
        fgamma = double(imread(fullfile(gdatafol, gdatalist(coldShots{s}(k)).name)));
        bg = mean(mean(fgamma(:,1:200)));
        coldGamma(k) = sum(sum(fgamma(300:600, 350:650) - bg));
    end
    % Now for the hot shot.
    fdat = load(fullfile(edatafol, edatalist(hotShots(s)).name));
    if useFixedSpec
        ind = find(hotShots(s)==shotsWeHave);
        alongs = fSpec.fixedSpec(ind).alongscreen;
        spec = fSpec.fixedSpec(ind).spec';
    else
        alongs = fdat.shotdata.alongscreen;
        spec = fdat.shotdata.dQ_dx;
    end
    
    % Find index of the gamma fit
    gamind = find(fg.fitShots==hotShots(s));
    % Needs to be a minus here: pointing lower means a higher actual energy
    gampoint = 4-fg.gamma_pointing_y(gamind);
    enax = getZepf2015ScreenEnergy(alongs, 2, gampoint); % Using default direction, 4mrad for cold shots
    lowax = getZepf2015ScreenEnergy(alongs, 2, gampoint-divThet);
    higax = getZepf2015ScreenEnergy(alongs, 2, gampoint+divThet);
    fprintf('Shot %i: pointing=%2.2f, enax(1000)=%3.0f\n', hotShots(s), gampoint, enax(1000));
    
    hotEnergy(2) = sum(spec.*enax);
    % Then the lower limit
    hotEnergy(1) = sum(spec.*lowax);
    % And the upper limit
    hotEnergy(3) = sum(spec.*higax);
    
    hotQ = fdat.shotdata.totQ;
    hotGamma = fg.gamma_yield_integral(gamind);
    edata(s, 2) = hotEnergy(2)./mean(coldEnergy(:,2));
    gdata(s, 2) = (hotGamma./hotQ)./(mean(coldEnergy(:,2))); % The one that GS uses
    % And now deal with errors
    hotEnErr = mean(abs(diff(hotEnergy)));
    coldEnErr = mean(mean(abs(diff(coldEnergy, 1, 2))));
    edata(s, 1) = edata(s,2) * sqrt(hotEnErr^2/hotEnergy(2)^2 + coldEnErr^2/mean(coldEnergy(:,2))^2);
    edata(s, 3) = edata(s,2) * sqrt(hotEnErr^2/hotEnergy(2)^2 + coldEnErr^2/mean(coldEnergy(:,2))^2);
    gdata(s, 1) = (hotGamma./hotQ)./(mean(coldEnergy(:, 1)));
    gdata(s, 3) = (hotGamma./hotQ)./(mean(coldEnergy(:, 3)));
end

% ------------ Plotting part
hfig = figure(3242); clf(hfig);
ax = makeNiceAxes(hfig);
colz = brewermap(9, 'set1');

% Apply some random value to to get rid of 1e-6
gdata = gdata*1e6;
% Do the normalisation
%gdata = gdata - min(gdata(:,2));
%gdata = gdata./max(gdata(:,2));
hg = errorbarxy(ax, 1-edata(:,2), gdata(:,2), edata(:,1), edata(:,3),...
     (gdata(:,1)-gdata(:,2)), (gdata(:,2)-gdata(:,3)), {'ks', 'k', 'k'});
set(ax, 'NextPlot', 'add')
hh = plot(ax, simf.x_plot, simf.y_plot_theo*1.9, 'd');
xlabel(ax, '1 - E_{Scatt. laser on}/E_{Scatt. laser off}');
ylabel(ax, {'Gamma yield /' ' Total reference beam energy'});
set(hg.hMain, 'Color', colz(1,:), 'MarkerFaceColor', colz(1,:));
set(hg.hErrorbar, 'LineWidth', 1.25, 'Color', colz(1,:))
set(hh, 'LineWidth', 1.25, 'Color', colz(2,:), 'MarkerFaceColor', colz(2,:))
hg.hMain.MarkerSize = 6; hh.MarkerSize = hg.hMain.MarkerSize + 1;

% Add legend
hleg = legend([hg.hMain, hh], {'Exp. results', 'Theor. calculation'}, ...
    'Location', 'se', 'Box', 'on', 'LineWidth', 1);

% And draw emphasis boxes
boxW = 0.02; boxH = .075; letts = 'bcd'; 
hls = [find(hotShots==14), find(hotShots==26), find(hotShots==18)];
for mm=1:length(hls)
    m = hls(mm);
    rectangle('Position', [1-edata(m,2)-boxW, (gdata(m,2)-boxH), 2*boxW, 2*boxH],...
        'Parent', ax, 'LineStyle', '--', 'LineWidth', 1.5);
    text(1-edata(m,2), (gdata(m,2)+boxH), letts(mm), 'Parent', ax, 'HorizontalAlignment', 'center',...
        'VerticalAlignment', 'bottom', 'FontSize', 12);
end
%set(ax, 'XLim', [-0.03 0.35], 'YLim', [-0.1 2.5]);
uistack([hg.hMain; hg.hErrorbar], 'top');
uistack(hh, 'top');
grid on;
tt = text(1-edata(:,2), gdata(:,2)-0.1, num2str(hotShots'));

%ht(1) = text(0.0, 2.3, 'a', 'FontWeight', 'Bold', 'Parent', ax);

%export_fig('/home/kpoder/GDrive/Experiment/2015_Zepf/AnalysisPresentations/Fig3a_new', '-pdf', hfig)