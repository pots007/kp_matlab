% save2015ZepfDataIntoText.m
%
% Write an ASCII delimited file for the data

% Let's make a spectrum which is easier to handle, clearly my points got
% lost in translation.

dataf = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'RR_experiment_KP_analysisdata'));
datafol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'SpectralShape');
% Plot thing to know what I'm saving :p

hfig = figure(87); clf(hfig);
cutoffs = [2070 2280 2670];

binID = 3;
enRange = dataf.fdata.energyBins(binID);
plot(enRange.normEnergyAxis*cutoffs(2), enRange.normSignal_dQ_dE_E./enRange.normEnergyAxis);

fid = fopen(fullfile(datafol, sprintf('spectra_dQ_dE_bin%i_allpointing.txt', binID)), 'w');
fprintf(fid, 'Energy_low(MeV)\tEnergy_axial(MeV)\tEnergy_high(MeV)\tdQ/dE(a.u.)\n');
for i=1:length(enRange.normEnergyAxis)
    for k=1:3
        fprintf(fid, '%2.6f\t', enRange.normEnergyAxis(i)*cutoffs(k));
    end
    fprintf(fid, '%2.6e\n', enRange.normSignal_dQ_dE_E(i)./enRange.normEnergyAxis(i));
end

fclose(fid);

% And now the format the theoreticians want
commEnAxis = 350:3000;
fnamestr = {'low', 'axial', 'high'};
for k=1:3
    fid = fopen(fullfile(datafol, sprintf('spectra_dQ_dE_bin%i_point_%s.txt', binID, fnamestr{k})), 'w');
    fprintf(fid, 'Energy(MeV)\tdQ/dE(a.u.)\n');
    locSignal = interp1(enRange.normEnergyAxis*cutoffs(k), enRange.normSignal_dQ_dE_E./enRange.normEnergyAxis, commEnAxis);
    for i=1:length(commEnAxis)
        fprintf(fid, '%2.6f\t', commEnAxis(i));
        fprintf(fid, '%2.6e\n', locSignal(i));
    end
    
end

fclose(fid);
