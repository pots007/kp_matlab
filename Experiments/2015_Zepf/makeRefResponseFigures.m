% makeRefResponseFigures.m
%
% Plot things we want to send the referee

%% Set up life
trackfile = 'Tracking_20170309_KP_4mrad.mat';

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

% Load stuff
load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'BGfree_data.mat'));
fSdat = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'shotdata'));
datfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis',...
    '20151109r003', 'mat');
flist = dir(fullfile(datfol, '*.mat'));
nshots = 49;

laserE_S = cell2mat({fSdat.Zepf2015.laserE_S});
laserE_N = cell2mat({fSdat.Zepf2015.laserE_N});

saveplots = 0;

%% Calculate necessary things.
maxens = zeros(nshots, 3);
beamEnInt = zeros(nshots,1);
GData = zeros(nshots,1);

colmap = [1 1 1; 0.8 0.8 0.8; 0.6471    0.8745    0.6510];

clear enax;
for i=1:nshots
    GData(i) = sum(sum(GDataraw(270:600, 250:700, i)));
    
    fdat = load(fullfile(datfol, flist(i).name));
    enax{1} = tracking.lowfit(fdat.shotdata.alongscreen)';
    enax{2} = tracking.valfit(fdat.shotdata.alongscreen)';
    enax{3} = tracking.highfit(fdat.shotdata.alongscreen)';
    dE = gradient(enax{2});
    spec = smooth(sum(fdat.shotdata.image, 1)./dE);
    beamEnInt(i) = sum(enax{2}.*fdat.shotdata.dQ_dx);
    
    nfac = sum(spec); % Normalise to peak charge density
    spec = spec./nfac;
    spec = spec./max(spec);
    % Find the index of the point closest to 10% level
    [~, minind] = min(abs(spec - 0.1));
    
    % OR try to find point which containts 99% of beam charge?
    beamCumSum = cumsum(spec);
    beamCumSum = beamCumSum./max(beamCumSum);
    %minind = find(beamCumSum>0.99, 1, 'first');
    %[ spec(minind) enax(minind)];
    if ~isempty(minind)
        for kk=1:3; maxens(i,kk) = enax{kk}(minind); end
    end
end
%figure(78);
%errorbar(1:nshots, maxens(:,2), maxens(:,3)-maxens(:,2), maxens(:,2)-maxens(:,1), 'o')

%% And plot cutoff energies against laser energy + G yield:
hfig = figure(545); clf(hfig); hfig.Color = 'w';
hfig.Position = [100 200 1000 400];
locs = GetFigureArray(2, 1, [0.03 0.03 0.15 0.1], 0.1, 'across');
offs = 0.05;
locs(3,1) = locs(3,1) - offs; 
locs(3,2) = locs(3,2) + offs; locs(1,2) = locs(1,2) - offs;
ax2 = axes('Parent', hfig, 'Position', locs(:,2), 'NextPlot', 'add');

clear hh hhh hleg;
% 0 for simply all shots, -1 for with one fit, 2 for two fits
hoffset = 00;
hh(1) = plot(laserE_S(fSdat.Noff), maxens(fSdat.Noff,2), 's');
hScat = scatter(laserE_S(fSdat.Non), maxens(fSdat.Non,2), 100,...
    log10(GData(fSdat.Non)), 'MarkerFaceColor', 'flat');
cb = colorbar;
ylabel(cb, '$$\log_{10}$$ (Gamma yield)');
[linfit, linfitErr] = fit(laserE_S(2:end)', maxens(2:end,2), 'poly1');
plotAx = 1:0.1:20;
%hhh(1) = plot(plotAx, linfit(plotAx), '--k');
en_error = [maxens(:,2)-maxens(:,1), maxens(:,3)-maxens(:,2)];
errs = mean(en_error(fSdat.Noff,:),2);
[linfit_off, linfit_offErr] = fit(laserE_S(fSdat.Noff)', maxens(fSdat.Noff,2), 'poly1',...
    'Weights', 1./(errs/mean(errs)));
hhh(1) = plot(1:20, linfit_off(1:20), '--', 'Color', hh(1).Color);
text(15, 1200+hoffset, sprintf('$R^2 = %2.2f$', linfit_offErr.rsquare), 'Color', hh(1).Color);
% And plot the prediction bands...
pred_on = predint(linfit_off, plotAx, 0.68);
pred_on2 = predint(linfit_off, plotAx, 0.95);
haa = area(plotAx, [zeros(length(plotAx),1) pred_on2(:,1) pred_on2(:,2)-pred_on2(:,1)]);
set(haa(1:2), 'FaceColor', 'none'); set(haa, 'EdgeColor', 'none');
set(haa(3), 'FaceColor', hh(1).Color, 'FaceAlpha', 0.2);
hleg(1) = hh(1); hleg(2) = hhh(end); hleg(3) = haa(3);
haa = area(plotAx, [zeros(length(plotAx),1) pred_on(:,1) pred_on(:,2)-pred_on(:,1)]);
set(haa(1:2), 'FaceColor', 'none'); set(haa, 'EdgeColor', 'none');
set(haa(3), 'FaceColor', hh(1).Color, 'Facealpha', 0.4);
hleg(4) = haa(3);
hleg(5) = hScat;
uistack([hh hhh], 'top');
uistack(hScat, 'top');
legend(hleg, {'Scatt laser off', 'Linear fit',...
    '$2\sigma$ bound', '$1\sigma$ bound','Scatt laser on'},...
    'Location', 'Southeast')

set(hh, 'MarkerSize', 10, 'LineWidth', 2);
set(hhh, 'LineWidth', 1.5);
set(gca, 'Box', 'on', 'LineWidth', 2, 'XLim', [10 18], 'YLim', [1100 1900], 'Layer', 'top');

ylabel('Cutoff energy / MeV');
xlabel('South beam uncompressed energy / J');
colormap(ax2, parula(256))

% Label the hot-hot shots
for k=1:length(fSdat.NS_optimum)
    indd = fSdat.NS_optimum(k);
    ht(k) = text(laserE_S(indd), maxens(indd,2), num2str(indd));
end
% And the second plot
ax1 = axes('Parent', hfig, 'Position', locs(:,1), 'NextPlot', 'add');
% Filter out the first, duff shot
GData(1) = nan;
h1 = plot(ax1, 1:49, GData./beamEnInt, '--k');
him = imagesc(1:49, (-2:1:20)*1e3, repmat(fSdat.Non, 10, 1), 'Parent', ax1);
uistack(him, 'bottom');
set(h1, 'Marker', 'o'); %set(h2, 'Marker', 'd');
set(ax1, 'YLim', [-10 1.6e3], 'XLim', [0 50], 'XTick', 0:5:50);
colormap(ax1, [1 1 1; 0.8 0.8 0.8])

xlabel(ax1, 'Shot number');
ylabel(ax1, 'Gamma yield/Ebeam energy');
set([ax1 ax2], 'Box', 'on', 'LineWidth', 2, 'Layer', 'top');
grid on; %grid minor;
% Text for the panels

make_latex(hfig); setAllFonts(hfig, 14);
text(ax1, 3, 1400, '(a)', 'FontWeight', 'bold', 'FontSize', 16);
text(ax2, 10.3, 1800, '(b)', 'FontWeight', 'bold', 'FontSize', 16);
set(ht, 'Interpreter', 'tex', 'FontSize', 10);

saveplots = 0;
if saveplots
    %if allflag==0; fext = 'all'; else; fext = 'coloured'; end
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'RefResponse1'),...
        '-png', '-nocrop', '-m3', hfig);
end
%% Now make the spectrometer RAW image, with acceptance overlaid.

hfig = figure(23783); clf(hfig);
set(hfig, 'Position', [300 100 1000 300]);
set(gca, 'NextPlot', 'add');

% Magnet has 3cm gap, 0.95m away - 30mrad acceptance?
accept_angle = 1000*0.03/0.95;

shotID = 18;
fdat = load(fullfile(datfol, flist(shotID).name));
imagesc(fdat.shotdata.alongscreen, fdat.shotdata.divax+14, fdat.shotdata.image); 
hrect = rectangle('Position', [0, -0.5*accept_angle, 200, accept_angle],...
    'EdgeColor', 'r', 'LineWidth', 3);

colormap(jet_white(256));
set(gca, 'XLim', [-5 205], 'YLim', [-20 20], 'Box', 'on');
ylabel('Divergence / mrad');
xlabel('Distance along screen / mm');

if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'RefResponse2'),...
        '-png', '-nocrop', '-m3', hfig);
end