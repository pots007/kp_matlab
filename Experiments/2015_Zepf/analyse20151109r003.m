% If set up properly, this will analyse 20151109r003, Zepf run data

date_ = '20151109';
run_ = '003';

nshots = 49;
ttt = struct;
%pres_ = [20*ones(1,2) 30*ones(1,3) 40*ones(1,3) 50*ones(1,2) 60*ones(1,3)];
%pres_ = [pres_ 80*ones(1,5) 90*ones(1,7) 100*ones(1,3)];

% Save as .figs
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 4);
% Save into the correct path
if ismac
    savepath = ['/Users/kristjanpoder/Experiments/2015_Zepf/EspecAnalysis/' date_ 'r' run_];
end

MagnetTrackerKP.fig.btrac.dat.outpath = fullfile(savepath, 'mat');
set(MagnetTrackerKP.fig.btrac.outpath_, 'String', fullfile(savepath, 'mat'));
datee = java.util.Date();
datee.setYear(2015-1900);
datee.setMonth(str2double(date_(5:6))-1);
datee.setDate(str2double(date_(7:8)));
MagnetTrackerKP.fig.input.date.setSelectedDate(datee);
MagnetTrackerKP.date_selected;
set(MagnetTrackerKP.fig.input.runbox, 'Value', str2double(run_));
MagnetTrackerKP.run_selected(str2double(run_));

%%

for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
%    ttt(i).shotID = i;
%    ttt(i).GSN = getGSN(str2double(date_), str2double(run_), i);
%    ttt(i).laserE = getenergy(ttt(i).GSN);
%    ttt(i).pres = pres_(i);
%    ttt(i).cellL = 12.5;
%    dat = getGemini2015Gas(str2double(date_), str2double(run_), i);
%    ttt(i).gastrace = dat(55000:60000, 1:2);
%    ttt(i).exitenergy = getGemini2015ExitEnergy(str2double(date_), str2double(run_), i);
%     ttt(i).Q1 = str2double(get(MagnetTrackerKP.fig.btrac.Q1_, 'String'));
%     ttt(i).Q2 = str2double(get(MagnetTrackerKP.fig.btrac.Q2_, 'String'));
%     ttt(i).Q3 = str2double(get(MagnetTrackerKP.fig.btrac.Q3_, 'String'));
%     ttt(i).E1 = str2double(get(MagnetTrackerKP.fig.btrac.E1_, 'String'));
%     ttt(i).E2 = str2double(get(MagnetTrackerKP.fig.btrac.E2_, 'String'));
%     ttt(i).E3 = str2double(get(MagnetTrackerKP.fig.btrac.E3_, 'String'));
    drawnow;
    toc
end
