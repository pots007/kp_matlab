% plot2015Zepf_EspecEnergyBins.m
%
% Zulf suggests looking at the integrated energy in different bands.
% Not a bad idea - I think in general the total energy in some energy bin
% should scale linearly with laser energy. So let's check this first: plot
% the energy contained in different parts of the spectrum as a function of
% laser energy, for the cold shots.

% Recyling code from plot2015Zepf_EspecOnly.m

% Set up tracking
%trackfile = 'Tracking_20160524_KP_4mrad.mat';
trackfile = 'Tracking_20170309_KP_4mrad.mat';

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
%tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
%tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');
fSdat = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'shotdata'));
datfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis',...
    '20151109r003', 'mat');
flist = dir(fullfile(datfol, '*.mat'));
nshots = 49;

laserE_S = cell2mat({fSdat.Zepf2015.laserE_S});
laserE_N = cell2mat({fSdat.Zepf2015.laserE_N});

saveplots = 0;

%%

cutoffs = zeros(nshots, 2);
beamEnBins = 500:500:2500;%[500 1000 1500 2000 2500];
nbins = length(beamEnBins)-1;
beamEnInts = zeros(nshots, nbins);
% Containts energy within the ranges specified above

for i=1:nshots
    fdat = load(fullfile(datfol, flist(i).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen)';
    dE = gradient(enax);
    spec = fdat.shotdata.dQ_dx./dE;
    
    nfac = max(spec); % Normalise to peak charge density
    %spec = spec./nfac;
    beamEn = spec.*enax.*dE;
    
    for k=1:nbins
        logm = enax > beamEnBins(k) & enax < beamEnBins(k+1);
        beamEnInts(i,k) = sum(beamEn(logm));
    end
    
    %specs(i, :) = spec;
    % Find the index of the point closest to 2% level
    [~, minind] = min(abs(spec - 0.1));
    if ~isempty(minind); maxens(i) = enax(minind); end;
    % Fill the edge width thing
    [~, minind] = min(abs(spec - 0.02));
    if ~isempty(minind); cutoffs(i,1) = enax(minind); end;
    [~, minind] = min(abs(spec - 0.1));
    if ~isempty(minind); cutoffs(i,2) = enax(minind); end; 
end

%%
hfig = figure(585); clf(hfig); set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add');

% Filter out too high laser energies as well - no data there... is this
% legit?
lm = fSdat.Noff & laserE_S<15;
plotS_E = 0:0.1:18;
clear hh;
clear coldFits;

for k=1:nbins
    coldFits{k} = fit(laserE_S(lm)', beamEnInts(lm,k), 'exp1');
    %coldFits{k} = fit(laserE_S(lm)', beamEnInts(lm,k), 'poly1');
    tt = confint(coldFits{k}, 0.68);
    %vv = polyval(tt(2,:)-mean(tt,1), plotS_E);
    hh(k) = plot(laserE_S(lm), beamEnInts(lm,k), 'd');
    plot(plotS_E, coldFits{k}(plotS_E), 'Color', hh(k).Color);
    %shadedErrorBar(plotS_E, coldFits{k}(plotS_E), vv, {'-', 'Color', hh(k).Color}, 1);
end
set(gca, 'XLim', [10 15.3], 'Box', 'on');
xlabel('South uncomp energy / J');
ylabel('Energy within Espec bin / a.u');
leglabs = {};
for i=1:nbins
    leglabs{i} = sprintf('$ %1.1f\\, \\mathrm{GeV} < \\mathcal{E} < %1.1f\\, \\mathrm{GeV}$',...
        beamEnBins(i:i+1)*1e-3);
end
legend(hh, leglabs, 'Location', 'Northwest');
grid on; grid minor;
make_latex(hfig); setAllFonts(hfig, 13);

%%

lm = fSdat.Non;
lmo = fSdat.NS_optimum;
for k=1:nbins
    set(hh(k), 'XData', laserE_S(lm), 'YData', beamEnInts(lm,k), 'Color', hh(k).Color);
    %hh(k) = plot(laserE_S(lm), beamEnInts(lm,k), 'd');
    plot(laserE_S(lmo), beamEnInts(lmo,k), 'd', ...
        'Color', hh(k).Color, 'MarkerFaceColor', hh(k).Color);
end
colzz = {hh.Color};
%% So now to to it off, I can plot the deltaE of hot shots against gamma signal.
 
% Run first section of plot2015Zepf_Especs_ThomsonOnly.m 

hfig = figure(595); clf(hfig); set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add');
clear hh;
for k=1:nbins
    dE_E = (coldFits{k}(laserE_S(lm)) - beamEnInts(lm,k));%./coldFits{k}(laserE_S(lm));
    hh(k) = plot(dE_E, GData(lm), 'd', 'Color', colzz{k});
    plot(coldFits{k}(laserE_S(lmo)) - beamEnInts(lmo,k), GData(lmo), 'd',...
        'Color', hh(k).Color, 'MarkerFaceColor', hh(k).Color);
end

hleg = legend(hh, leglabs, 'Location', 'Northeast');
hleg.Position = hleg.Position + [0 0.08 0 0];
grid on;  grid minor;
set(gca, 'Box', 'on')
ylabel('$\gamma$-signal / a.u.');
%xlabel('$\frac{\bar{\mathcal{E}} - \mathcal{E}}{\bar{\mathcal{E}}}$');
xlabel('$\bar{\mathcal{E}} - \mathcal{E}$ / a.u.');
make_latex(hfig); setAllFonts(hfig, 15);

%% Plot the same as above, but employing the fits.

load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'GammaFits', 'fitData'));
fitFcn2 = @(a1,x01,xw1,y01,yw1,a2,x02,xw2,y02,yw2,x,y) a1*exp( -((x-x01).^2/(2*xw1^2) + (y-y01).^2/(2*yw1^2)) ) +...
    a2*exp(   -((x-x02).^2/(2*xw2^2) + (y-y02).^2/(2*yw2^2)) );
[FX, FY] = meshgrid(-25:100, -25:100);
fitInts = zeros(3, nshots);
for k=1:nshots
    if sum(k==fitNData_2G(:,1))==0; continue; end
    kl = find(k==fitNData_2G(:,1), 1);
    fitInts(1,k) = fitNData_2G(kl,1);
    f = fitNData_2G(kl,:);
    fitSig = fitFcn2(f(2), f(3), f(4), f(5), f(6), f(7), f(8), f(9), f(10), f(11), FX, FY);
    fitInts(2,k) = sum(fitSig(:));
    fitInts(3,k) = sqrt(abs(f(4)*f(6)));
end

lm_r20 = fitNData_2G(:,12)>0.95;
lm_r2 = fitNData_2G(lm_r20, 1);

hfig = figure(595); clf(hfig); set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add');
clear hh;
for k=1:nbins
    dE_E = (coldFits{k}(laserE_S(lm_r2)) - beamEnInts(lm_r2,k));%./coldFits{k}(laserE_S(lm));
    hh(k) = plot(dE_E, fitInts(2,lm_r2), 'd', 'Color', colzz{k});
    plot(coldFits{k}(laserE_S(lmo)) - beamEnInts(lmo,k), fitInts(2,lmo), 'd',...
        'Color', hh(k).Color, 'MarkerFaceColor', hh(k).Color);
end

hleg = legend(hh, leglabs, 'Location', 'Northeast');
hleg.Position = hleg.Position + [0 0.08 0 0];
grid on;  grid minor;
set(gca, 'Box', 'on')
ylabel('$\gamma$-signal / a.u.');
%xlabel('$\frac{\bar{\mathcal{E}} - \mathcal{E}}{\bar{\mathcal{E}}}$');
xlabel('$\bar{\mathcal{E}} - \mathcal{E}$ / a.u.');
make_latex(hfig); setAllFonts(hfig, 15);

%% And for the shots with acceptable fit, plot the bin energy losses against 
% the fitted Gaussian width

hfig = figure(5950); clf(hfig); set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add');
clear hh;
for k=1:nbins
    dE_E = (coldFits{k}(laserE_S(lm_r2)) - beamEnInts(lm_r2,k));%./coldFits{k}(laserE_S(lm));
    hh(k) = plot(fitInts(3,lm_r2) ,dE_E, 'd', 'Color', colzz{k});
    plot(fitInts(3,lmo), coldFits{k}(laserE_S(lmo)) - beamEnInts(lmo,k), 'd',...
        'Color', hh(k).Color, 'MarkerFaceColor', hh(k).Color);
end

hleg = legend(hh, leglabs, 'Location', 'Northeast');
hleg.Position = [0.1481    0.1253    0.3805    0.1866];
grid on;  grid minor;
set(gca, 'Box', 'on', 'XLim', [0 5])
xlabel('Signal width $w_s = \sqrt{w_xw_y}$ (mm)');
ylabel('$\bar{\mathcal{E}} - \mathcal{E}$ / a.u.');
make_latex(hfig); setAllFonts(hfig, 12);
