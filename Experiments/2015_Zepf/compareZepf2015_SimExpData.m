% compareZepf2015_SimExpData.m
%
% Script to quickly compare results from simulations with the optimum,
% experimental results. This borrows heavily from
% RadiationReaction_fig3_rev1.m for the main traces of the experimental
% data.
%
% Set up graphics

hfig = figure(1110); clf(hfig);
papsize = [17 11];
set(hfig, 'Position', [100 100 papsize*50], 'Color', 'w', 'Paperunits', 'centimeters',...
    'Papersize', papsize);

locs = GetFigureArray(1, 1, [0.05 0.1 0.12 0.1], 0.1, 'across');
clear axx;
for k=1:size(locs,2)
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add');
end
set(axx, 'LineWidth', 1, 'Box', 'on');

% And set up the shots!!
hotShots = [14 17 15 26 18];
coldShots = {40, 10, [9,31], 10, [3,10,34]};
edatafol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis', ...
    '20151109r003', 'mat');
edatalist = dir(fullfile(edatafol, '*Espec.mat'));
% FROM plot2015Zepf_EspecOnly.m - set up life
trackfile = 'Tracking_20170309_KP_4mrad.mat';
% Trackfile, more recent, more sensible?
%trackfile = 'Tracking_20170802_KP_4mrad_05mrad_div.mat';

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

plotPerMeV = false;  % Whether we plot per MeV or per % energy spread

%% And now plot the comparison plots

compshots = [5 1 4 5]; % Relics from donor script

for k=1:length(axx)
    cla(axx(k));
    ind = compshots(k);
    fdat = load(fullfile(edatafol, edatalist(hotShots(ind)).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen);
    dE = gradient(enax);
    edatalist(hotShots(ind)).name
    hotShot = smooth(fdat.shotdata.dQ_dx'./dE);
    if ~plotPerMeV; hotShot = hotShot.*enax; else hotShot = hotShot*7e2; end;
    nfac_0 = sum(fdat.shotdata.dQ_dx);
    clear hh;
    hh(1) = plot(axx(k), enax, hotShot./nfac_0, 'r-');
    for m=1:length(coldShots{ind})
        fdat = load(fullfile(edatafol, edatalist(coldShots{ind}(m)).name));
        coldShot = smooth(fdat.shotdata.dQ_dx'./dE);
        if ~plotPerMeV; coldShot = coldShot.*enax; else coldShot = coldShot*7e2; end;
        nfac = sum(fdat.shotdata.dQ_dx);
        hh(end+1) = plot(axx(k), enax, coldShot./nfac, 'k-');
    end
    set(hh, 'LineWidth', 1); hh(1).LineWidth = 4;
    xlabel(axx(k), 'Electron energy (MeV)')
    if plotPerMeV
        ylabel(axx(k), 'Electrons per MeV (a.u.)');
    else
        ylabel(axx(k), {'Electrons per MeV' ' per % energy spread (a.u.)'})
    end
    %ht(k) = text(600, 0.1, ['' char(96+k) ''], 'Parent', axx(k), 'FontWeight', 'bold');
end

% And plot the ones we want:
if 1   % The pic stuff from Chris R
    CR_dat_ = csvimport(fullfile(getGDrive, 'Experiment', '2015_Zepf',...
        'MultiParticleSimulations', 'ChrisRidgers_e_spec_0_7.csv'));
    CR_dat = [cellfun( @(x) str2double(x), CR_dat_(5:end, 1)),...
        cellfun(@(x) str2double(x), CR_dat_(5:end,2))];
    if plotPerMeV
        CR_spec = CR_dat(:,2);
    else
        CR_spec = CR_dat(:,2).*CR_dat(:,1);
    end
    nfac = nfac_0/sum(CR_spec)*0.4;
    hh(end+1) = plot(axx(1), CR_dat(:,1), CR_spec.*nfac, '-b');
end

simtypes = {'LL', 'QED', 'SemiClass'};
simints = {'a0=19.2', 'a0=22.5', 'a0=25.8'};
savfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'MultiParticleSimulations',...
    'Matteo_20170816');
colz = brewermap(9, 'set1');
colz(6,:) = [];

for T = [1 3]
    for I =  1:3
        if 1 % Matteo's simulations
            simtype = simtypes{T};% 'SemiClassical';
            simint = simints{I}; %'a0=22.5';
            simfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'MultiParticleSimulations',...
                'Matteo_20170816', simint, simtype);
            flist = dir_(fullfile(simfol, '*.txt'));
            if sum(ishandle(hh_))>0; delete(hh_(ishandle(hh_))); end;
            hh_ = [];
            for k=1:length(flist)
                dat = importdata(fullfile(simfol, flist(k).name));
                nfac_ = 1.1*nfac_0/(sum(dat(:,2))*mean(diff(dat(:,1))));
                %hh_(end+1) = plot(axx(1), dat(:,1), dat(:,2).*dat(:,1)/(1.15*max(dat(:,2).*dat(:,1))));
                hh_(end+1) = plot(axx(1), dat(:,1), smooth(dat(:,2).*dat(:,1)*0.01*nfac_), '-', ...
                    'Color', colz(k+1,:));
                legnams = {'Sim, initial', '0 fs', '32 fs', '64 fs', '96 fs'};
            end
            if ishandle(hleg); delete(hleg); end;
            hleg = legend(axx(1), [hh([1,2,end]) hh_], [{'Hotshot, data', 'Coldshot, data', 'PIC'} legnams],...
                'Location', 'Northeast');
            title([simtype ' simulations with peak ' simint]);
            set([hh(2:end) hh_], 'LineWidth', 1.5);
            setAllFonts(hfig, 14);
            set(axx, 'XLim', [300 2500], 'YLim', [ 0 1.1]);
            drawnow;
            export_fig(fullfile(savfol, [simint '_' simtype]), '-png', '-nocrop', hfig);
            
        end
    end
end
set(axx, 'XLim', [300 2500], 'YLim', [ 0 1]);

%% NOTE THIS PART ONLY WORKS FOR the most recent data 20170822
% These are the days with the error bar data.


compshots = [5 1 4 5]; % Relics from donor script

for k=1:length(axx)
    cla(axx(k));
    ind = compshots(k);
    fdat = load(fullfile(edatafol, edatalist(hotShots(ind)).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen);
    dE = gradient(enax);
    edatalist(hotShots(ind)).name
    hotShot = smooth(fdat.shotdata.dQ_dx'./dE);
    if ~plotPerMeV; hotShot = hotShot.*enax; else hotShot = hotShot*7e2; end;
    nfac_0 = sum(fdat.shotdata.dQ_dx);
    clear hh;
    hh(1) = plot(axx(k), enax, hotShot./nfac_0, 'r-');
    for m=1:length(coldShots{ind})
        fdat = load(fullfile(edatafol, edatalist(coldShots{ind}(m)).name));
        coldShot = smooth(fdat.shotdata.dQ_dx'./dE);
        if ~plotPerMeV; coldShot = coldShot.*enax; else coldShot = coldShot*7e2; end;
        nfac = sum(fdat.shotdata.dQ_dx);
        hh(end+1) = plot(axx(k), enax, coldShot./nfac, 'k-');
    end
    set(hh, 'LineWidth', 1); hh(1).LineWidth = 4;
    xlabel(axx(k), 'Electron energy (MeV)')
    if plotPerMeV
        ylabel(axx(k), 'Electrons per MeV (a.u.)');
    else
        ylabel(axx(k), {'Electrons per MeV' ' per % energy spread (a.u.)'})
    end
    %ht(k) = text(600, 0.1, ['' char(96+k) ''], 'Parent', axx(k), 'FontWeight', 'bold');
end

% And plot the ones we want:
if 0   % The pic stuff from Chris R
    CR_dat_ = csvimport(fullfile(getGDrive, 'Experiment', '2015_Zepf',...
        'MultiParticleSimulations', 'ChrisRidgers_e_spec_0_7.csv'));
    CR_dat = [cellfun( @(x) str2double(x), CR_dat_(5:end, 1)),...
        cellfun(@(x) str2double(x), CR_dat_(5:end,2))];
    if plotPerMeV
        CR_spec = CR_dat(:,2);
    else
        CR_spec = CR_dat(:,2).*CR_dat(:,1);
    end
    nfac = nfac_0/sum(CR_spec)*0.4;
    hh(end+1) = plot(axx(1), CR_dat(:,1), CR_spec.*nfac, '-b');
end

simtypes = {'LL', 'QED', 'SemiClass', 'Perturbative'};
simints = {'a0=19.2', 'a0=22.5', 'a0=25.8'};
errtype = 0; % 1 for intensity errors, 0 for espec energy errors
errtypestr = {'SpecError', 'IntError'};
savfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'MultiParticleSimulations',...
    'Matteo_20170822');
colz = brewermap(9, 'set1');
colz(6,:) = [];
simEnaxis = 200:3000;

for T = 1:4
    for I =  2%1:3
        if 1 % Matteo's simulations
            simtype = simtypes{T};% 'SemiClassical';
            simint = simints{I}; %'a0=22.5';
            simfol = fullfile(savfol, simint, 'Medium', simtype);
            flist = dir_(fullfile(simfol, '*.txt'));
            if sum(ishandle(hh_))>0; delete(hh_(ishandle(hh_))); end;
            delete(findobj(axx(1), 'Tag', 'Errorbar'));
            hh_ = [];
            for k=1:length(flist)
                dat = importdata(fullfile(simfol, flist(k).name));
                nfac_ = 1.1*nfac_0/(sum(dat(:,2))*mean(diff(dat(:,1))));
                spec = interp1(dat(:,1), smooth(dat(:,2).*dat(:,1)*0.01*nfac_), simEnaxis);
                % Now check whether we have error data for this combo
                if errtype % Int error, try to estimate the worst case
                    % Fucking clidge, prone to breaking!
                    fn = flist(k).name;
                    datmf = fullfile(savfol, simint, 'Lower', [simtype '+5'],...
                        [fn(1:end-10) '+5_final.txt']);
                    datpf = fullfile(savfol, simint, 'Upper', [simtype '-5'],...
                        [fn(1:end-10) '-5_final.txt']);
                else
                    datmf = fullfile(savfol, simint, 'Lower', simtype, flist(k).name);
                    datpf = fullfile(savfol, simint, 'Upper', simtype, flist(k).name);
                end
                if k==1
                    %hh_(end+1) = plot(axx(1), dat(:,1), smooth(dat(:,2).*dat(:,1)*0.01*nfac_), '-', ...
                    %    'Color', colz(k+1,:));
                    hh_(end+1) = plot(axx(1), simEnaxis, spec, 'Color', colz(k+1,:));
                else
                    if exist(datmf) && exist(datpf) % The errors actually exist
                        datmf_ = importdata(datmf);
                        datpf_ = importdata(datpf);
                        % to common axis
                        datmf_i = interp1(datmf_(:,1)', smooth(datmf_(:,2).*datmf_(:,1)*0.01*nfac_), simEnaxis);
                        datpf_i = interp1(datpf_(:,1)', smooth(datpf_(:,2).*datpf_(:,1)*0.01*nfac_), simEnaxis);
                        hhh = shadedErrorBar(simEnaxis, spec, [datpf_i-spec; spec-datmf_i], {'Color', colz(k+1,:)}, 0.4);
                        set([hhh.patch hhh.edge], 'Tag', 'Errorbar');
                        
                        hh_(end+1) = hhh.mainLine;
                    end
                end
            end
            legnams = {'Sim, initial', '64 fs', '96 fs'};
            if ishandle(hleg); delete(hleg); end
            hleg = legend(axx(1), [hh([1,2,end]) hh_], [{'Hotshot, data', 'Coldshot, data', 'PIC'} legnams],...
                'Location', 'Northeast');
            title([simtype ' simulations with peak ' simint]);
            set([hh(2:end) hh_], 'LineWidth', 1.5);
            setAllFonts(hfig, 14);
            set(axx, 'XLim', [300 2500], 'YLim', [ 0 1.1]);
            drawnow;
            export_fig(fullfile(savfol, [simint '_' simtype '_' errtypestr{errtype+1}]), '-png', '-nocrop', hfig);
            
        end
    end
end
set(axx, 'XLim', [300 2500], 'YLim', [ 0 1]);


%% This will write out cold spectra for the highest RR seen

shotss = [3 10 34];
type = {'lower', 'medium', 'upper'};
type_track = {'lowfit', 'valfit', 'highfit'};
if 1
    hfig = figure(90091); clf(hfig);
    set(gca, 'NextPlot', 'add')
    hh = [];
    for t = 1:length(type)
        ydat = [];
        for k=shotss
            fdat = load(fullfile(edatafol, edatalist(k).name));
            enax = tracking.(type_track{t})(fdat.shotdata.alongscreen);
            dE = gradient(enax);
            ydat(end+1,:) = fdat.shotdata.dQ_dx'./dE;
        end
        meanSpec = mean(ydat, 1);
        fid = fopen(fullfile(savfol, ['AverageColdSpectra_shots_3_10_34' type{t} '.txt']), 'w');
        for k=1:1320%length(meanSpec)
            fprintf(fid, '%2.6f\t%2.6f\n', enax(k), meanSpec(k));
        end
        fclose(fid);
        hh(end+1) = plot(enax, meanSpec);
        set(gca, 'XLim', [300 2500]);
    end
    legend(hh, type);
end



%% THIS WORKS FOR 20171122 simulations only!!!!

compshots = [5 1 4 5]; % Relics from donor script

plotPerMeV = 0;
for k=1%1:length(axx)
    cla(axx);
    ind = compshots(k);
    fdat = load(fullfile(edatafol, edatalist(hotShots(ind)).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen);
    dE = gradient(enax);
    edatalist(hotShots(ind)).name
    hotShot = smooth(fdat.shotdata.dQ_dx'./dE);
    if ~plotPerMeV; hotShot = hotShot.*enax; else hotShot = hotShot*7e2; end;
    nfac_0 = sum(fdat.shotdata.dQ_dx);
    clear hh;
    hh(1) = plot(axx, enax, hotShot./nfac_0, 'r-');
    for m=1:length(coldShots{ind})
        fdat = load(fullfile(edatafol, edatalist(coldShots{ind}(m)).name));
        coldShot = smooth(fdat.shotdata.dQ_dx'./dE);
        if ~plotPerMeV; coldShot = coldShot.*enax; else coldShot = coldShot*7e2; end;
        nfac = sum(fdat.shotdata.dQ_dx);
        hh(end+1) = plot(axx, enax, coldShot./nfac, 'k-');
    end
    set(hh, 'LineWidth', 1); hh(1).LineWidth = 4;
    xlabel(axx, 'Electron energy (MeV)')
    if plotPerMeV
        ylabel(axx, 'Electrons per MeV (a.u.)');
    else
        ylabel(axx, {'Electrons per MeV' ' per % energy spread (a.u.)'})
    end
    %ht(k) = text(600, 0.1, ['' char(96+k) ''], 'Parent', axx(k), 'FontWeight', 'bold');
end

% Norm factor for later on - peak of cold spectrum.
ydat = cell2mat(get(findobj(axx, 'Color', 'k'), 'YData'));

datpath = fullfile(getGDrive, 'Experiment', '2015_Zepf',...
    'MultiParticleSimulations', 'Matteo_20171122');
fols = {'Shift_x', 'Shift_y'};
offsets = 2:2:8;
hhsim = [];
labels = {};
colss = brewermap(9, 'Set1');
for dir_=1:2
    for shift=1:4
        fname = sprintf('Shift%1.2fEnergySemiClass_final.txt', offsets(shift));
        datf = importdata(fullfile(datpath, fols{dir_}, fname));
        spec = smooth(datf(:,2));
        if ~plotPerMeV
            spec = spec.*datf(:,1);
            spec = 0.87*spec./9.3e6; % Fixed value to highlight abs change
        else
            spec = 1.05*spec/1.6e4; 
        end
        hhsim(end+1) = plot(datf(:,1), spec, '-', 'Color', colss(shift+1,:));
        labels{end+1} = sprintf('Shift %s, %i um', fols{dir_}(end), offsets(shift));
    end
end

set(hhsim, 'LineWidth', 1.5);
set(hhsim(1:4), 'LineStyle', '--');
set(hhsim(5:end), 'LineStyle', ':');
set(axx, 'XLim', [350 2300]);
legendflex(hhsim, labels, 'anchor', [3 3], 'nrow', 4, 'buffer', [20 20], 'FontSize', 14)
if plotPerMeV; fnam = 'Comparison_perMeV'; else; fnam = 'Comparison_renorm'; end;
export_fig(fullfile(datpath, fnam), '-pdf', '-nocrop', hfig);

