% Try to figure out how much divergence changes when tracking the flight
% path of electrons with different energies.

%% Load the data and track
ll = load('Gemini2015_Zepf_Settings_20170309');
load('owncolourmaps');
ll.dat.Rin = []; ll.dat.pin = [];
fret = track_electrons2(ll.dat);

%% Now do the maths

i=1; % Do screen 1
yl = ll.dat.(sprintf('y%i', i));
zl = ll.dat.(sprintf('z%i', i));
Ll = ll.dat.(sprintf('l%i', i));
thetl = ll.dat.(sprintf('thet%i', i));
% Find intersection with screen i:
% Endpoints of screen:
a = [zl yl]; b = a + Ll*[cosd(thetl) sind(thetl)];
% Now select central angle
div = 2;
els = fret.div(div).el;
% Column 3 is total distance from TCC
ppp = nan(3,length(els));
for en = 1:length(els)
    % Look at this particular energy.
    R = els(en).r;
    [zi,yi,iii] = intersections(R(:,3),R(:,2), [a(1) b(1)], [a(2) b(2)]);
    ppp(2,en) = els(en).energy*1.8712e21;
    if ~isempty(zi)
        ppp(1,en) = yi;
        % Silly integration:
        ei = floor(iii(1));
        ppp(3,en) = sum(sqrt( (R(2:ei,3)-R(1:ei-1,3)).^2 + (R(2:ei,2)-R(1:ei-1,2)).^2));
        % And add the last segment
        ppp(3,en) = ppp(3,en) + sqrt( (zi - R(ei,3))^2 + (yi - R(ei,2))^2);
    end
end

% Convert to distance along the screen
ppp(1,:) = (-yl + ppp(1,:))/sind(thetl);
logm = ~isnan(ppp(1,:));
divfit = fit(ppp(1,logm)', ppp(3,logm)', 'spline');
hfig = figure(786); clf(hfig);
set(gca, 'nextPlot', 'add', 'Box', 'on');
plot(ppp(1,:), ppp(3,:), 'x');
plot(0:300, divfit(0:300), '-r');
plot([0 300], [1 1]*zl, '--k');
xlabel('Distance along screen / mm');
ylabel('Total distance from TCC to screen / mm');

fname = fullfile(getDropboxPath, 'MATLAB', 'Experiments', '2015_Zepf', 'trackingDivergenceFit');
%save(fname, 'divfit');