% compareZepf2015_SimExpData5.m
%
% Script to quickly compare results from simulations with the optimum,
% experimental results. This borrows heavily from
% RadiationReaction_fig4_rev1.m for the main traces of the experimental
% data.
%
% This works for simulations from 20171222
%
% One panel, and shifts along x and y with different line styles.

% Set up graphics

hfig = figure(1115); clf(hfig);
papsize = [14 10];
set(hfig, 'Position', [100 100 papsize*50], 'Color', 'w', 'Paperunits', 'centimeters',...
    'Papersize', papsize);

locs = GetFigureArray(1, 1, [0.02 0.02 0.1 0.1], 0.1, 'across');
axx = axes('Parent', hfig, 'Position', locs, 'NextPlot', 'add',...
    'LineWidth', 1, 'Box', 'on');

% ------------ And set up the shots!!
hotShots = [14 17 15 26 18];
coldShots = {40, [10,33], [9,31], [10,33,44], [3,7,33,34]}; %NEWWWW

fSpec = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'MultiParticleSimulations',...
    'Kris_20171218', 'fixedSpectra'));
shotsWeHave = cell2mat({fSpec.fixedSpec.shot});

ID = 4;
hotShotID = hotShots(ID); % The shot ID of the hot shot.
coldShotID = coldShots{ID}; % Using the average cold spectra
plotCold = 1;

% ------------ Set up tracking
% FROM plot2015Zepf_EspecOnly.m - set up life
trackfile = 'Tracking_20170309_KP_4mrad.mat';

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');
simEnaxis = 200:3000;

plotPerMeV = true;  % Whether we plot per MeV or per % energy spread


%% Now the actual plotting

offsets = 0:2:8;
dirs = {'shots_10_33_44_SemiClass_shift_X', 'shots_10_33_44_SemiClass_shift_Y'};
simfol = fullfile(getGDrive, 'Experiment', '2015_Zepf',...
    'MultiParticleSimulations', 'Matteo_20171222');
colss = brewermap(9, 'Set1');

cla(axx);
ind = find(hotShotID==shotsWeHave);
enax = tracking.valfit(fSpec.fixedSpec(ind).alongscreen);
dE = gradient(enax);
hotShot = smooth(fSpec.fixedSpec(ind).spec'./dE);
if ~plotPerMeV; hotShot = hotShot.*enax; else hotShot = hotShot*5e2; end
nfac_0 = sum(fSpec.fixedSpec(ind).spec); %sum(fdat.shotdata.dQ_dx);

clear hhd;
hhd(1) = plot(axx, enax, hotShot./nfac_0, 'r-');
ydat = [];
for m=1:length(coldShotID)
    ind = find(coldShotID(m)==shotsWeHave);
    coldShot = smooth(fSpec.fixedSpec(ind).spec'./dE);
    if ~plotPerMeV; coldShot = coldShot.*enax; else coldShot = coldShot*5e2; end
    nfac = sum(fSpec.fixedSpec(ind).spec);% sum(fdat.shotdata.dQ_dx);
    ydat(:,end+1) = coldShot./nfac;
end
if plotCold; hhd(end+1) = plot(axx, enax, mean(ydat, 2), '-k'); end
datf = importdata(fullfile(simfol, dirs{1}, 'Energy_initial.txt'));
if plotPerMeV
    hh(1) = plot(axx, datf(:,1), smooth(datf(:,2))*1.07/1.6e4/1.4, '-c');
else
    hh(1) = plot(axx, datf(:,1), smooth(datf(:,2).*datf(:,1))*0.89/9.3e6, '-c');
end

% --------------------- Now plot the simulation stuff
hh = []; labels = [];
for d=1:2
    for o=1:length(offsets)
        fname = sprintf('P%1.2fEnergySemiClass_final.txt', offsets(o));
        datName = fullfile(simfol, dirs{d}, fname);
        datf = importdata(datName);
        %nfac_ = .88*nfac_0/(sum(simData(:,2))*mean(diff(simData(:,1))));
        % Onto a common grid we go
        %simSpec = interp1(simData(:,1), smooth(simData(:,2).*simData(:,1)*0.01*nfac_), simEnaxis);
        %hh(end+1) = plot(axx, simEnaxis, simSpec, 'Color', colss(o,:));
        spec = smooth(datf(:,2));
        if ~plotPerMeV
            spec = spec.*datf(:,1);
            spec = 0.89*spec./9.3e6; % Fixed value to highlight abs change
        else
            spec = 1.07*spec/1.6e4/1.4; 
        end
        hh(end+1) = plot(axx(1), datf(:,1), spec, '-', 'Color', colss(o,:));
        labels{end+1} = sprintf('Shift %s, %i um', dirs{d}(end), offsets(o));
    end
end
uistack(hh, 'top'); % Ensure the real data is on top of the sim results
% And add the legend!

set(hh, 'LineWidth', 1.5);
set(hh(1:5), 'LineStyle', '--');
set(hh(6:end), 'LineStyle', ':');
set(axx, 'XLim', [400 2300], 'YLim', [0 1]);
legendflex(hh, labels, 'anchor', [3 3], 'nrow', 5,...
    'buffer', [2 2], 'FontSize', 12)
set(hhd, 'LineWidth', 3);

% ------------- Making things nice
xlabel(axx, 'Electron energy (MeV)')
if plotPerMeV
    ylabel(axx, 'Electrons per MeV (norm.)');
else
    ylabel(axx, {'Electrons per MeV' ' per % energy spread (norm.)'})
end

make_tex(hfig);
%% Saving part
fnam = 'Comp_20171222_offsets';
if plotPerMeV; fnam = [fnam '_perMeV']; end
%export_fig(fullfile(simfol, fnam), '-pdf', '-nocrop', hfig);