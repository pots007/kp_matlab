load('GianlucaGemini2015_RadSafety.mat');
figure(8); clf;
hh = tracking.screen(1).heights;
hs = plot(hh(:,1), hh(:,2:4), '-o', 'LineWidth', 2);
xlabel('Electron energy / MeV');
ylabel('Distance from TCC line / mm');

%set(gca, 'XLim', [90 2100], 'XScale', 'log')
hold on;
plot(get(gca, 'XLim'), 870*[1 1], '--k');
hold off;
set(gca, 'XLim', [90 500])
legend(hs, {sprintf('%i mrad', tracking.divergences(1)); sprintf('%i mrad', tracking.divergences(2));...
    sprintf('%i mrad', tracking.divergences(3))}, 'Location', 'NorthEast');
setAllFonts(8, 24);