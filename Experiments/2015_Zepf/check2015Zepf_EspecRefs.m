% check2015Zepf_EspecRefs.m
%
% This will look and compare some espec references from 2015 Zepf run. This
% is to ensure that we have the correct reference for the day of the actual
% data, which is 20151109.

datfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'Espec_references');
% ref1 is from 20151019, the one Gianluca used.
ref1 = imread(fullfile(datfol, 'References_20151020',...
    '20151019_laserON_scintillatorsOFF_bleedingLanexIN_2200.tif'));
ref2 = imread(fullfile(datfol, 'References_20151105',...
    'new_camera_position_1957.tif'));

zpoint = [457 861];

hfig1 = figure(501); clf(hfig1);
hfig1.Color = 'w';
ax1 = subplot(2,1,1);
imagesc(ref1);

set(gca, 'CLim', [0 1500]);
colormap(gray);

ax2 = subplot(2,1,2);
imagesc(ref2);
set(gca, 'CLim', [0 2000]);
colormap(gray);
linkaxes([ax1 ax2], 'xy');

% Figuring out the angles:
% For ax1, top ruler is along line
% r1 = (322, 334), r2 = (2264, 130)
thet1 = atand((334-130)/(2264-322))
% For ax2, top ruler is along line
% r1 = (271, 308) and r2 = (2165, 131)
thet2 = atand((308-131)/(2165-271))

% Rotate so the angle of the black ruler is the same
ref2rot = imrotate(ref2, thet1-thet2, 'bilinear', 'crop');
set(findall(ax2, 'type', 'image'), 'cdata', ref2rot);

% Difficult to read, but the 50mm marker on the bottom image is at r = (266, 297)
% On the top one, it's at r = (556, 240)
r50t = [266 297];
r50b = [556 240];

% Centre of bolt on bottom is at r = (2302, 1181)
% Centre of bolt on top image is at r = 2495, 1164)
rbt = [2302 1181];
rbb = [2495 1164];
