% testGerchbergSaxton_Zepf2015.m
%
% Script to test out the Gerchberg-Saxton algorithm to try to improve on
% the focal spot analysis for the radiation reaction simulations.
%
% KP, Hamburg, 2017

% Set up paths - use the day with the focal plane scan
datfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'Focus', '20151012', 'Z-scan');
savfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'Focus', '20151012', 'Z-scan_propagations');
flist = dir(fullfile(datfol, '*.raw'));
% Find the planes of focus - is bigger numbers away from focus?????
fnames = {flist.name};
% Remove the varGain ones
logm = cellfun(@(x) isempty(x), strfind(fnames, 'varGain'));
foc_planes = cellfun(@(x) str2double(x(10:14)), fnames(logm));
foc_planes_un = unique(foc_planes);
% Make array with filenames and corresponding focal planes
focPlaneData = struct;
for k=1:length(foc_planes_un)
    focPlaneData(k).z = foc_planes_un(k);
    logm_ = cellfun(@(x) ~isempty(x), strfind(fnames(logm), num2str(foc_planes_un(k))));
    focPlaneData(k).fnames = fnames(logm_);
end

% Set up a suitable grid for FFTs
npnts = 2^9; % 512 points ought ot be enough!
dx = 0.2e-6; % Resolution of the images - 0.2 microns per pixel
xGrid = (1:640)*dx;
yGrid = (1:480)*dx;
imGrid = ((-(0.5*npnts-1):0.5*npnts) - 0.5)*dx;

% Plotting flag...
plotFlag = 1;
if plotFlag
    hfig = figure(8712); clf(hfig);
    locs = GetFigureArray(4, 2, [0.1 0.05 0.05 0.05], 0.05, 'across');
    clear axx;
    for k=1:size(locs,2)
        axx(k) = axes('Parent', hfig, 'Position', locs(:,k));
    end
   
end

% GIF making flag
makeGIF = false;
% End of data preparation.
%% Actual algorithm of sorts
inPlane = 13370;
outPlane = 13410;
% Find associated IDs
inID = find(foc_planes_un == inPlane, 1);
outID = find(foc_planes_un == outPlane, 1);
% Pick an image to start off with
inImID = 63;
inImage = ReadRAW16bit(fullfile(datfol, focPlaneData(inID).fnames{inImID}), 640, 480);
% Filter, to get rid of hotspots;
inImage = medfilt2(inImage);
% Remove BG!
bgregion = inImage(:, 540:end);
inImage = inImage - mean(bgregion(:));
if plotFlag;
    imagesc(xGrid, yGrid, inImage, 'Parent', axx(1));
    axis(axx(1), 'image');
    ylabel(axx(1), 'Input plane');
    title(axx(1), 'Measured spot')
end;
% Now set up the image, as ready to be propagated. Make it centered on the
% pupil.
cc = centroidlocationsmatrix(inImage);
inImage_ = interp2(xGrid-cc(1)*dx, yGrid-cc(2)*dx, inImage, imGrid, imGrid', 'linear', 0);
% And now make some BG! :)
imcnts = inImage_(inImage_~=0);
[N, pix] = histcounts(imcnts(:), 1000); pix = pix(1:end-1) + mean(diff(pix));
% Fit Gaussian to BG level, ie pixels with values of 2000 or less
pix_logm = pix<2000;
bgFit = fit(pix(pix_logm)', N(pix_logm)', 'gauss1');
% And now fill the zero locations with data from the fitted distribution.
inImage_lin = inImage_(:);
nZeros = sum(inImage_lin==0);
bgFills = bgFit.c1.*randn(nZeros,1) + bgFit.b1;
inImage_lin(inImage_lin==0) = bgFills;
%inImage_ = reshape(inImage_lin, npnts, npnts);
if plotFlag;
    imagesc(imGrid, imGrid, sqrt(abs(inImage_)), 'Parent', axx(2));
    axis(axx(2), 'image');
    title(axx(2), 'Measured spot')
end;

% Now prepare the output image - do ALLLLLLLLLLLLL of them :)

for imID = 2%:length(focPlaneData(outID).fnames)
    % First sort out filename, if we will save stuff later on
    fname = fullfile(savfol, sprintf('from_%i_%03i_to_%i_%03i', inPlane, inImID, outPlane, imID));
    outImage = ReadRAW16bit(fullfile(datfol, focPlaneData(outID).fnames{imID}), 640, 480);
    % Filter, to get rid of hotspots;
    outImage = medfilt2(outImage);
    % Remove BG!
    bgregion = outImage(:, 540:end);
    outImage = outImage - mean(bgregion(:));
    if plotFlag;
        imagesc(xGrid, yGrid, outImage, 'Parent', axx(5));
        axis(axx(5), 'image');
        ylabel(axx(5), 'Output plane');
        title(axx(5), 'Measured spot')
    end;
    % Now set up the image, as ready to be propagated. Make it centered on the
    % pupil.
    cc = centroidlocationsmatrix(outImage);
    outImage_ = interp2(xGrid-cc(1)*dx, yGrid-cc(2)*dx, outImage, imGrid, imGrid', 'linear', 0);
    if plotFlag;
        imagesc(imGrid, imGrid, sqrt(abs(outImage_)), 'Parent', axx(6));
        axis(axx(6), 'image');
        title(axx(6), 'Measured spot')
    end;
    
    % And now the actual method... or so
    deltaZ = (outPlane - inPlane)*1e-6;
    inFresnel_new = sqrt(abs(inImage_)); % Field is sqrt of intensity!!
    pause(1);
    for it = 1:50;
        tic;
        outFresnel = (fresnelPropagator(inFresnel_new, dx, dx, deltaZ, 800e-9));
        outFresnel_amp = abs(outFresnel);
        outFresnel_ang = angle(outFresnel);
        if plotFlag
            imagesc(imGrid, imGrid, outFresnel_amp, 'Parent', axx(7)); axis(axx(7), 'image');
            title(axx(7), 'Amplitude');
            imagesc(imGrid, imGrid, outFresnel_ang, 'Parent', axx(8)); axis(axx(8), 'image');
            title(axx(8), 'Phase');
        end
        % Now replace the amplitude of the forward propagated image with that
        % of the actual image
        outFresnel_new = sqrt(abs(outImage_)).*exp(1j*outFresnel_ang);
        inFresnel = fresnelPropagator(outFresnel_new, dx, dx, -deltaZ, 800e-9);
        inFresnel_amp = abs(inFresnel);
        inFresnel_ang = angle(inFresnel);
        if plotFlag
            imagesc(imGrid, imGrid, inFresnel_amp, 'Parent', axx(3)); axis(axx(3), 'image');
            title(axx(3), sprintf('Amplitude after %i iterations', it));
            imagesc(imGrid, imGrid, inFresnel_ang, 'Parent', axx(4)); axis(axx(4), 'image');
            title(axx(4), sprintf('Phase after %i iterations', it));
        end
        % And now replace the amplitude of the initial image with the actual
        % intensity.
        inFresnel_new = sqrt(abs(inImage_)).*exp(1j*inFresnel_ang);
        drawnow;
        %pause(0.3);
        linkaxes(axx([2,3,4,6,7,8]), 'xy')
        fprintf('Iteration %2i took %2.2f s\n', it, toc);
        if plotFlag && makeGIF
            % Capture the plot as an image
            frame = getframe(hfig);
            im = frame2im(frame);
            [imind,cm] = rgb2ind(im,256);
            % Write to the GIF File
            if it == 1
                imwrite(imind,cm,[fname '.gif'],'gif', 'Loopcount',inf);
            else
                imwrite(imind,cm,[fname '.gif'],'gif','WriteMode','append');
            end
        end
    end
    set(axx(2), 'XLim', [-20 20]*1e-6, 'YLim', [-20 20]*1e-6); drawnow;
    export_fig(fname, '-png', '-nocrop', hfig);
end

%% Take the field we just calculated, and propagate that
% This is to see what the focal spot looks like going through focus
% We also assume that there is a total of totalEnergy J of energ in the
% pulse, and a tau of 40 fs.
totalEnergy = 8; tau = 40e-15;
hfig2 = figure(8713); clf(hfig2);
locs = GetFigureArray(4, 3, 0.05, 0.05, 'across');
clear axxs;
z_planes = foc_planes_un - inPlane;
for i=1:length(z_planes)
    this_plane = fresnelPropagator(inFresnel, dx, dx, z_planes(i)*1e-6, 800e-9);
    axxs(i) = axes('Parent', hfig2, 'Position', locs(:,i));
    normf = totalEnergy/(tau*sum(sum(abs(this_plane).^2)) * dx^2) * 1e-4;
    imagesc(imGrid, imGrid, normf*abs(this_plane).^2, 'Parent', axxs(i));
    %imagesc(imGrid, imGrid, angle(this_plane), 'Parent', axxs(i));
    title(sprintf('\\Delta z = %i, z = %i', z_planes(i), foc_planes_un(i)));
    colorbar(axxs(i));
    axis(axxs(i), 'image');
end
limss = cell2mat(get(axxs, 'CLim'));
set(axxs, 'CLim', [min(limss(:)) max(limss(:))], 'XLim', [-20 20]*1e-6, ...
    'YLim', [-20 20]*1e-6);

%% If we used an intermediatory plane, check if there's a spot that looks like it
if abs(inID-outID) ~= 1
    hfig3 = figure(8714); clf(hfig3);
    hfig3.Position = [200 200 1400 500];
    locs = GetFigureArray(3, 1, [0.1 0.05 0.05 0.05], 0.05, 'across');
    clear axxx;
    for kk = 1:size(locs,2)
        axxx(kk) = axes('Parent', hfig3, 'Position', locs(:, kk));
    end
    medID = inID + 1;
    % Propagate to this distance
    deltaZ = focPlaneData(medID).z - inPlane;
    med_plane = fresnelPropagator(inFresnel, dx, dx, deltaZ*1e-6, 800e-9);
    % For this part, compare the intensities...
    imagesc(imGrid, imGrid, abs(med_plane).^2, 'Parent', axxx(1));
    title(axxx(1), 'Calculated intensity at this plane');
    % And for each of the measured spots, check if it suits...
    sigmaError = zeros(size(focPlaneData(medID).fnames));
    for k=1:length(focPlaneData(medID).fnames)
        medImage = ReadRAW16bit(fullfile(datfol, focPlaneData(medID).fnames{k}), 640, 480);
        % Filter, to get rid of hotspots;
        outImage = medfilt2(medImage);
        % Remove BG!
        bgregion = medImage(:, 540:end);
        medImage = medImage - mean(bgregion(:));
        % Now set up the image, as ready to be propagated. Make it centered
        % on the pupil.
        cc = centroidlocationsmatrix(outImage);
        medImage_ = interp2(xGrid-cc(1)*dx, yGrid-cc(2)*dx, outImage, imGrid, imGrid', 'linear', 0);
        cla(axxx(2));
        imagesc(imGrid, imGrid, medImage_, 'Parent', axxx(2));
        title(axxx(2), ['Actual intensity, ' focPlaneData(medID).fnames{k}]);
        dInt = abs(med_plane).^2 - medImage_;
        cla(axxx(3));
        imagesc(imGrid, imGrid, dInt, 'Parent', axxx(3));
        title(axxx(3), sprintf('Difference, \\Sigma Err = %2.2e', sum(dInt(:))) );
        sigmaError(k) = sum(dInt(:));
        pause(1);
        drawnow;
    end
end
