% makeZepf2015TrackingMap.m
%
% Construct a matrix of tracker electrons for different divergences and
% offset angles

% The final product, lookupSpline{scrID, divID}, encloses a 2D spline to
% calculate the energy at a given offset angle and a given distance from
% edge of screen.

fstate = load(fullfile('/home/kpoder/', 'matlab', 'MagnetTracker','Gemini2015_Zepf_Settings_20170309.mat'));

fstate.dat.Rin = []; fstate.dat.pin = [];
fstate.dat.noe = 150;
fstate.dat.espace = 0;
fstate.dat.ediv = 1;
divs = 2:0.25:6; % 4mrad either way....
%%
fret = cell(size(divs));

for i=1:length(divs)
    fprintf('Tracking for angle %1.2f mrad: %i out of %i\n', divs(i), i, length(divs));
    fstate.dat.divoff = divs(i);
    fret{i} = track_electrons2(fstate.dat);
end
save(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'trackingmap.mat'), 'fret');
% This has been moved to the 2015_Gemini GDrive folder to save space
%%
load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'trackingmap.mat'))
ens = fret{1}.energies;
locmap = zeros(length(divs), length(ens));
screenID = 1;
divID = 2; % The largest pointing

for divID=1:3
    for k=1:length(divs)
        yl = fstate.dat.(sprintf('y%i', screenID));
        zl = fstate.dat.(sprintf('z%i', screenID));
        Ll = fstate.dat.(sprintf('l%i', screenID));
        thetl = fstate.dat.(sprintf('thet%i', screenID));
        % Find intersection with screen i:
        % Endpoints of screen:
        a = [zl yl]; b = a + Ll*[cosd(thetl) sind(thetl)];
        % Now select divergence, and then iterate through energies
        els = fret{k}.div(divID).el;
        ppp = nan(2,length(els));
        for en = 1:length(els)
            % Look at this particular energy.
            R = els(en).r;
            [zi,yi] = intersections(R(:,3),R(:,2), [a(1) b(1)], [a(2) b(2)]);
            ppp(2,en) = els(en).energy*1.8712e21;
            if ~isempty(zi)
                ppp(1,en) = yi;
            end
        end
        if ~isempty(ppp)
            % Convert to along screen
            ppp(1,:) = (-yl + ppp(1,:))/sind(thetl);
        end
        locmap(k,:) = ppp(1,:)';
    end
    locationmaps{divID} = locmap;
end


%% Now make a lookuptable with pointing angle and distance from edge of screen as inputs

for scrID = 1
    for divID=1:3
        % For every angle, on every screen, find interpolate onto the
        % screenBins the energy we'd find at that distance
        screenBins = 0:0.1:fstate.dat.(sprintf('l%i', scrID));
        interpmap = zeros(size(locmap,1), length(screenBins));
        locmap = locationmaps{divID};
        for k=1:size(locmap, 1)
            hitlocs = locmap(k,:);
            logm = ~isnan(hitlocs);
            interpmap(k,:) = interp1(hitlocs(logm), ens(logm), screenBins, 'PCHIP');
        end
        lookupSpline{scrID, divID} = griddedInterpolant({divs, screenBins}, interpmap, 'spline', 'none');
    end
end
save(fullfile('/home/kpoder/', 'matlab', 'kp_matlab', 'Experiments', '2015_Zepf', 'Zepf2015lookupSpline'), 'lookupSpline');