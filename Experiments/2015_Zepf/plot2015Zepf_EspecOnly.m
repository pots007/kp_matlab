% plot2015Zepf_EspecOnly.m
%
% Look at the Especs, based on the data saved by MagnetTracker.
% This relies on the calibration file
%       20151105_ref_20170309_gridbasedCalibData.mat
% I think this shouldn't be too bad, but the rulers are really out of
% focus, so perhaps the higher edge is a bit off. Should factor that into
% the errors for later on!
%
% The tracking options to use:
%   Tracking_20160524_KP_0mrad.mat - For some reason, use 4.7mm height above axis
%   Tracking_20160524_KP_4mrad.mat - For some reason, use 4.7mm height above axis
%   Tracking_20170309_KP_0mrad.mat - Using 8mm screen edge height
%   Tracking_20170309_KP_4mrad.mat - Using 8mm screen edge height
%   Tracking_20170309_KP_8mrad.mat - Using 8mm screen edge height
%
% Not sure where the discrepancy above stems from.

% Set up tracking
%trackfile = 'Tracking_20160524_KP_4mrad.mat';
trackfile = 'Tracking_20170309_KP_4mrad.mat';

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

% Load stuff
load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'BGfree_data.mat'));
fSdat = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'shotdata'));
datfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis',...
    '20151109r003', 'mat');
flist = dir(fullfile(datfol, '*.mat'));
nshots = 49;

laserE_S = cell2mat({fSdat.Zepf2015.laserE_S});
laserE_N = cell2mat({fSdat.Zepf2015.laserE_N});

saveplots = 0;

%%
% Plot all extracted spectra in one plot...

hfig = figure(505); clf(hfig); hfig.Color = 'w';
set(gca, 'NextPlot', 'add');

edat = [];
%enax = fdat.shotdata.tracking_axial(fdat.shotdata.alongscreen+0);
for i = 1:length(flist)
    fdat = load(fullfile(datfol, flist(i).name));
    enax = tracking.highfit(fdat.shotdata.alongscreen)';
    dE = gradient(enax);
    plot(enax, fdat.shotdata.dQ_dx./dE.*enax);
    %edat(:,i) = fdat.shotdata.dQ_dx;
end
xlabel('Electron energy / MeV');
ylabel('dQ/(dE/E) / a.u.');
set(gca, 'Xlim', [500 3000], 'Box', 'on', 'FontSize', 20);
make_latex(hfig);

if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'espec_all_spectra'), '-pdf', '-nocrop', hfig);
end



%% As a first quick thing, plot montage of all S only shots.
% Plot them on a common colour axis as well.
Sshots = fSdat.Noff.*(1:49);
Sshots = Sshots(Sshots~=0);
nSshots = length(Sshots);

locs = GetFigureArray(nSshots, 1, [0.07 0.01 0.04 0.05], 0.005, 'across');
hfig = figure(515); clf(hfig); hfig.Color = 'w';
hax = [];
mvals = [];
enlines = [500 1000 1500 2200];
for k=1:length(enlines)
    [~, enind(k)] = min(abs(enax-enlines(k)));
end
for i = 1:nSshots
    hax(i) = axes('Parent', hfig, 'Position', locs(:,i));
    fdat = load(fullfile(datfol, flist(Sshots(i)).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen)';
    mvals(i) = max(max(fdat.shotdata.image));
    imagesc(fdat.shotdata.image');
    title(num2str(Sshots(i)));
    %edat(:,i) = fdat.shotdata.dQ_dx;
end
linkaxes(hax, 'xy');
set(hax, 'YDir', 'normal', 'XTick', [], 'Ytick', enind, 'YTickLabel', [],...
    'CLim', [0 max(mvals)], 'YLim', [1 1450]);
set(hax(1), 'YTickLabel', num2str(enlines'))
ylabel(hax(1), 'Energy / MeV');
make_latex(hfig); setAllFonts(hfig, 14);

if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'espec_all_refshots'), '-pdf', '-nocrop', hfig);
end

%% As a variation to this, plot them in increasing laser enegry order.

%% Now plot the variation of the ref shots.

% Decide what to plot
dEflag = 1;

Sshots = fSdat.Noff.*(1:49);
Sshots = Sshots(Sshots~=0);
% Cut out shot 1 from analysis - no ebeam.
% Sshots(1) = []; DONE THIS ELSEHWERE NOW!
nSshots = length(Sshots);

hfig = figure(525); clf(hfig); hfig.Color = 'w'; hfig.Position = [100 200 800 600];
ax1 = gca;
set(ax1, 'NextPlot', 'add');
% And make an inset...
ax2 = axes('Parent', hfig, 'Position', [0.5 0.5 0.4 0.4], 'Box', 'on', 'NextPlot', 'add');

specs = zeros(nSshots, 1572); % Hardcoded array size! What am I up to!
specs2 = zeros(size(specs)); % For use later on; unnormalised spectra.
for i = 1:nSshots
    fdat = load(fullfile(datfol, flist(Sshots(i)).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen)';
    dE = gradient(enax);
    spec = sum(fdat.shotdata.image,1);
    if dEflag; spec = spec./dE; end;
    nfac = sum(spec);
    specs2(i,:) = spec;
    spec = spec./nfac;
    specs(i, :) = spec;
    plot(ax1, enax, spec, '-', 'Color', [0.9373 0.7255 0.7255]);
    plot(ax2, enax, spec, '-', 'Color', [0.9373 0.7255 0.7255]);
end
signalRMS = std(specs,1);
signalmean = mean(specs,1);
h = area(ax1, enax, [signalmean-signalRMS; 2*signalRMS]', 'EdgeColor', 'w');
set(h(2), 'FaceColor', [1 1 1]*0.9);
set(h(1), 'FaceAlpha', 1, 'FaceColor', 'none');
uistack(h, 'down', 21);
h = area(ax2, enax, [signalmean-signalRMS; 2*signalRMS]', 'EdgeColor', 'w');
set(h(2), 'FaceColor', [1 1 1]*0.9);
set(h(1), 'FaceAlpha', 1, 'FaceColor', 'none');
uistack(h, 'down', 21);
plot(ax1, enax, signalmean, '-k');
plot(ax2, enax, signalmean, '-k');
if dEflag; limfac = 0.0009; else limfac = 0.002; end;
if dEflag
    % Saytake 2% level for dQ/dE curve:
    [~,minind] = min(abs(signalmean - 0.02*limfac));
    plot(ax2, enax(minind)*[0.9 1.1], 0.02*[1 1]*limfac, '-b', 'LineWidth', 2);
else
    [~,minind] = min(abs(signalmean - 0.1*limfac));
    plot(ax2, enax(minind)*[0.9 1.1], 0.1*[1 1]*limfac, '-b', 'LineWidth', 2);
end
% And for limits... this is annoying

text(2000, 0.09*limfac, '$\pm 10\%$ width', 'Color', 'b', 'Parent', ax2);
%set(ax1, 'XLim', [500 3000], 'Box', 'on', 'Layer', 'top', 'YLim', [-0.05 1]);
set(ax1, 'XLim', [500 3000], 'Box', 'on', 'Layer', 'top', 'YLim', [-0.05 1]*limfac);
set(ax2, 'XLim', [1600 2600], 'Box', 'on', 'Layer', 'top', 'YLim', [-0.01 .11]*limfac);
xlabel(ax1, 'Energy / MeV');
if dEflag; ylabel(ax1, 'dQ/dE / a.u.'); else ylabel(ax1, 'dQ/dx / a.u.'); end;
title(ax1, 'All reference shots, mean spectra');

grid(ax1, 'on'); grid(ax1, 'minor');
make_latex(hfig); setAllFonts(hfig, 14);

if saveplots
    if dEflag; fext = 'dQ_dE'; else fext = 'dQ_dx'; end
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', ['espec_all_refspectra_' fext]), '-pdf', '-nocrop', hfig);
end

%% Now find cutoff energies.
% Which method to use? let's start with Gianluca's 90% of peak charge
% density. I also assume he used the dQ/dE curve for this. So in line with
% before, let's just use the 2% level of maximum values.

% Plot all the retrieved energies out as well so I can check.
% This is done for all shots!

hfig = figure(535); clf(hfig); hfig.Color = 'w';
locs = GetFigureArray(1, 2, [0.05 0.02 0.07 0.07], 0.02, 'down');
axx(1) = axes('Parent', hfig, 'Position', locs(:,1), 'NextPlot', 'add');
axx(2) = axes('Parent', hfig, 'Position', locs(:,2), 'NextPlot', 'add');
set(axx(1), 'XLim', [0 25], 'XAxisLocation', 'top');
set(axx(2), 'XLim', [25 50]);

maxens = zeros(nshots, 1);
cutoffs = nan(nshots, 2); % This will contain cutoffs at 2% and 10%?
beamEnInt = zeros(nshots,1);
gammaax = enax./0.511;

colmap = [1 1 1; 0.8 0.8 0.8; 0.6471    0.8745    0.6510];
% Shot numbers:
colf = zeros(1, nshots);
colf(fSdat.NS_optimum) = 2;
colf = colf + fSdat.Non;

for i=1:nshots
    fdat = load(fullfile(datfol, flist(i).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen)';
    dE = gradient(enax);
    spec = sum(fdat.shotdata.image, 1)./dE;
    beamEnInt(i) = sum(gammaax.^2.*(spec.*dE));
    nfac = max(spec); % Normalise to peak charge density
    spec = spec./nfac;
    %specs(i, :) = spec;
    % Find the index of the point closest to 2% level
    [~, minind] = min(abs(spec - 0.1));
    if ~isempty(minind); maxens(i) = enax(minind); end;
    % Fill the edge width thing
    [~, minind] = min(abs(spec - 0.02));
    if ~isempty(minind); cutoffs(i,1) = enax(minind); end;
    [~, minind] = min(abs(spec - 0.1));
    if ~isempty(minind); cutoffs(i,2) = enax(minind); end;
    
    % Do all the plotting
    % First background, according to what type shot it was:
    imagesc(i+[-1 0], [0 6000], ones(10, 100)*colf(i), 'Parent', axx((i>25) + 1));
    % Spectrum
    plot(axx((i>25) + 1), spec+i-1, enax, '-', 'Color', 'k');
    % Reference 0 line
    plot(axx((i>25) + 1), i*[1 1], [0 6000], '--k');
    % Retrieved energy
    plot(axx((i>25) + 1), i+[-1 0], maxens(i)*[1 1], 'Color', [0.8353 0.1569 0.3373]);
end

colormap(colmap);

set(axx, 'YLim', [500 2700], 'Box', 'on', 'layer', 'top');
grid(axx(1), 'on'); grid(axx(1), 'minor');
grid(axx(2), 'on'); grid(axx(2), 'minor');

ylabel(axx(1), 'Energy / MeV'); ylabel(axx(2), 'Energy / MeV');
make_latex(hfig); setAllFonts(hfig, 14);

if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'espec_all_cutoffs'), '-pdf', '-nocrop', hfig);
end

%% And plot ALL cutoff energies against laser energy:
hfig = figure(545); clf(hfig); hfig.Color = 'w'; hfig.Position = [100 200 600 400];
set(gca, 'NextPlot', 'add');
clear hh;
% 0 for simply all shots, 1 for with one fit, 2 for two fits
allflag = 2;

hoffset = 00;
if allflag==0
    hh = plot(laserE_S(2:end), maxens(2:end), 'o');
else
    hh(1) = plot(laserE_S(fSdat.Noff), maxens(fSdat.Noff), 'o');
    hh(2) = plot(laserE_S(fSdat.Non), maxens(fSdat.Non), 'd');
    hh(3) = plot(laserE_S(fSdat.NS_optimum), maxens(fSdat.NS_optimum), 'd');
    set(hh(3), 'Color', hh(2).Color, 'MarkerFaceColor', hh(2).Color);
    legend(hh, {'North beam off', 'N beam on', 'Very high Thomson signal'},...
        'Location', 'Southeast')
end
[linfit, linfitErr] = fit(laserE_S(2:end)', maxens(2:end), 'poly1');
plot(1:20, linfit(1:20), '--k');
text(16, 1500+hoffset, sprintf('$R^2 = %2.2f$', linfitErr.rsquare), 'Color', 'k');
if allflag ==2 % Do separate fits
    [linfit_on, linfit_onErr] = fit(laserE_S(fSdat.Non)', maxens(fSdat.Non), 'poly1');
    [linfit_off, linfit_offErr] = fit(laserE_S(fSdat.Noff)', maxens(fSdat.Noff), 'poly1');
    plot(1:20, linfit_on(1:20), '--', 'Color', hh(2).Color);
    plot(1:20, linfit_off(1:20), '--', 'Color', hh(1).Color);
    text(16, 1400+hoffset, sprintf('$R^2 = %2.2f$', linfit_onErr.rsquare), 'Color', hh(2).Color);
    text(16, 1300+hoffset, sprintf('$R^2 = %2.2f$', linfit_offErr.rsquare), 'Color', hh(1).Color);
end

set(hh, 'MarkerSize', 10, 'LineWidth', 1);
set(gca, 'Box', 'on', 'LineWidth', 2, 'XLim', [10 18], 'YLim', [1100 1800]);

ylabel('Cutoff energy / MeV');
xlabel('South beam uncompressed energy / J');
make_latex(hfig); setAllFonts(hfig, 14);

if saveplots
    if allflag==0; fext = 'all'; else fext = 'coloured'; end
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', ['espec_vs_laserE_' fext]), '-pdf', '-nocrop', hfig);
end

%% Plot whet Juff suggested: beam charge, weighted by gamma^2 against thomson signal

hfig = figure(555); clf(hfig); hfig.Color = 'w'; hfig.Position = [100 200 600 400];
set(gca, 'NextPlot', 'add');
clear hh;
hh(1) = plot(beamEnInt(fSdat.Noff), GData(fSdat.Noff), 'o');
hh(2) = plot(beamEnInt(fSdat.Non), GData(fSdat.Non), 'd');
hh(3) = plot(beamEnInt(fSdat.NS_optimum), GData(fSdat.NS_optimum), 'd');
set(hh(3), 'Color', hh(2).Color, 'MarkerFaceColor', hh(2).Color);
legend(hh, {'North beam off', 'N beam on', 'Very high Thomson signal'},...
    'Location', 'Northwest')
[linfit, linfitErr] = fit(beamEnInt(2:end), GData(2:end)', 'poly1');
fitax = linspace(0, 2e15, 100);
plot(fitax, linfit(fitax), '--k');
text(6e14, 17e7, sprintf('$R^2 = %2.2f$', linfitErr.rsquare), 'Color', 'k');
[linfit_on, linfit_onErr] = fit(beamEnInt(fSdat.Non), GData(fSdat.Non)', 'poly1');
[linfit_off, linfit_offErr] = fit(beamEnInt(fSdat.Noff), GData(fSdat.Noff)', 'poly1');
plot(fitax, linfit_on(fitax), '--', 'Color', hh(2).Color);
plot(fitax, linfit_off(fitax), '--', 'Color', hh(1).Color);
text(6e14, 15.5e7, sprintf('$R^2 = %2.2f$', linfit_onErr.rsquare), 'Color', hh(2).Color);
text(6e14, 14e7, sprintf('$R^2 = %2.2f$', linfit_offErr.rsquare), 'Color', hh(1).Color);

set(hh, 'MarkerSize', 10, 'LineWidth', 1);
set(gca, 'Box', 'on', 'LineWidth', 2, 'XLim', [0 8e14], 'YLim', [-1e7 2e8]);

ylabel('Total gamma yield / a.u.');
xlabel('$\int{\gamma^2 \frac{\mathrm{d}Q}{\mathrm{d}\gamma} \,\mathrm{d}\gamma}$ / a.u.');
make_latex(hfig); setAllFonts(hfig, 14);

if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'gamma_vs_beamEn'), '-pdf', '-nocrop', hfig);
end

%% Now try to find a shot with a similar spectrum....

BS_i = 8;
shotnum = fSdat.NS_optimum(BS_i);
fdat = load(fullfile(datfol, flist(shotnum).name));
enax = tracking.valfit(fdat.shotdata.alongscreen)';
dE = gradient(enax);
spec = sum(fdat.shotdata.image,1);
if dEflag; spec = spec./dE; end; % For backwards compatibility to above....
moneyspec = spec;

difm = zeros(nSshots, 1);
for i=1:size(specs2, 1)
    % Find the difference between the money-shot and references:
    temp1 = smooth(moneyspec(enax>800 & enax<1300));
    temp2 = smooth(specs2(i, enax>800 & enax<1300));
    difm(i) = sum(abs(temp1-temp2));
end

[~, minind_] = min(difm)
plot(enax, moneyspec, 'k', enax, specs2(minind_, :))
set(gca, 'XLim', [350 2500])

%% Try to replicate Fig 3 GS made:

Esigsum = sum(EDataraw, 1); Esigsum = sum(Esigsum, 2);
Esigsum = squeeze(Esigsum)';

hfig = figure(555); clf(hfig); hfig.Color = 'w'; hfig.Position = [100 200 600 400];
refshotsID = 6;

fricEnLoss = 1 - maxens(fSdat.NS_optimum)./maxens(refshotsID);
fricGData = GData(fSdat.NS_optimum)./Esigsum(fSdat.NS_optimum);

plot(fricEnLoss, fricGData, 'x');

% And use the references we find in script plot2015Zepf_EspecsThomson.m,
% based on the nearest laser beam energy.
refshotsID2 = [32 3 40 31 10 34 9 10]; % Calculated 20170318

fricEnLoss = 1 - maxens(fSdat.NS_optimum)./maxens(refshotsID2);
fricGData = GData(fSdat.NS_optimum)./Esigsum(fSdat.NS_optimum);

plot(fricEnLoss, fricGData, 'x');

%% Another interlude: waterfall plots
% Plot spectra as fn of laser E, but with waterfall...

srtflag = 1;

[laserE_S_sort, laserE_S_sort_inds] = sort(laserE_S);

hfig = figure(565); clf(hfig); hfig.Color = 'w'; hfig.Position = [100 200 800 600];
ax1 = gca; set(ax1, 'NextPlot', 'add', 'Box', 'on');
for i=1:nshots
    fdat = load(fullfile(datfol, flist(i).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen)';
    dE = gradient(enax);
    spec = fdat.shotdata.dQ_dx;
    allspecs(:,i) = spec./dE.*enax;
end
if srtflag
    hh = waterfall(enax, laserE_S_sort, allspecs(:, laserE_S_sort_inds)');
    ylabel('South uncompressed energy / J');
    set(gca, 'YLim', [9 17]);
else
    hh = waterfall(enax, 1:49, allspecs');
    ylabel('Shot number');
    set(gca, 'YLim', [0 50]);
end
set(ax1, 'XLim', [500 3000], 'ZLim', [0 200], 'XDir', 'reverse');
xlabel('Energy / MeV');
%zlabel('Electron signal / a.u.');
zlabel('$\frac{\mathrm{d}Q}{\mathrm{d}E/E}$ / a.u.');
make_latex(hfig); setAllFonts(hfig, 14);
view(-66, 37);
if saveplots
    if srtflag; fext = 'sorted'; else fext = 'shots'; end;
    % export_fig does NOT like savng patch objects into pdf, so using png here
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', ['espec_all_wf_' fext]), '-png', '-nocrop', hfig);
end

%% Now let's try to figure out what Mangles suggested doing
% First, we'll construct an expected total beam energy integral value,
% based on nullshots
[W_E, W_Eer] = fit(laserE_S(fSdat.Noff)', beamEnInt(fSdat.Noff), 'poly1');
CI = confint(W_E, 0.68);
fprintf('R^2 for the W(C) fit is %1.2f\n', W_Eer.adjrsquare);
fprintf('With 1 sigma errors, have k = %2.3e +- %2.3e\n', W_E.p1, abs(W_E.p1-CI(1,1)));
fprintf('With 1 sigma errors, have y0 = %2.3e +- %2.3e\n', W_E.p2, abs(W_E.p2-CI(2,2)));
% Now we constuct the expectation value for cutoff energy
[C_E, C_Eer] = fit(laserE_S(fSdat.Noff)', maxens(fSdat.Noff), 'poly1');
CI = confint(C_E, 0.68);
fprintf('R^2 for the C(E) fit is %1.2f\n', C_Eer.adjrsquare);
fprintf('With 1 sigma errors, have k = %2.3e +- %2.3e\n', C_E.p1, abs(C_E.p1-CI(1,1)));
fprintf('With 1 sigma errors, have y0 = %2.3e +- %2.3e\n', C_E.p2, abs(C_E.p2-CI(2,2)));
% And now we make a fit to gamma signal as a function of expected beam energy
%[G_W, G_Wer] = fit(beamEnInt(fSdat.Noff), GData(fSdat.Noff)', 'poly1');
[G_W, G_Wer] = fit(W_E(laserE_S(fSdat.Noff)), GData(fSdat.Noff)', 'poly1');
CI = confint(G_W, 0.68);
fprintf('R^2 for the G(W) fit is %1.2f\n', G_Wer.adjrsquare);
fprintf('With 1 sigma errors, have k = %2.3e +- %2.3e\n', G_W.p1, abs(G_W.p1-CI(1,1)));
fprintf('With 1 sigma errors, have y0 = %2.3e +- %2.3e\n', G_W.p2, abs(G_W.p2-CI(2,2)));
% Quite large errors in this one now!

% Or actually, gamma signal as a function of expected beam energy
% So now let's do the plot he thinsks: (C-C_E)/(C_E) vs (G-G_W)

sss = 1:49;
hfig = figure(565); clf(hfig); hfig.Colo = 'w'; hfig.Position = [100 200 800 600];
set(gca, 'NextPlot', 'add', 'Box', 'on');
%plot(beamEnInt(fSdat.Noff), GData(fSdat.Noff), 'o');
xdat = (maxens(fSdat.Non) - C_E(laserE_S(fSdat.Non)))./C_E(laserE_S(fSdat.Non));
ydat = GData(fSdat.Non)' - G_W(W_E(laserE_S(fSdat.Non)));
plot(xdat, ydat, 'o');
ssss = sss(fSdat.Non);
for i=1:sum(fSdat.Non)
    text(xdat(i), ydat(i), num2str(ssss(i)));
end
%scatter((maxens(fSdat.Noff) - C_E(laserE_S(fSdat.Noff)))./C_E(laserE_S(fSdat.Noff)),...
%    GData(fSdat.Noff)' - G_W(W_E(laserE_S(fSdat.Noff))), 25, sss(fSdat.Noff));

grid on;
xlabel('$\frac{C - C(E)}{C(E)}$');
ylabel('$G - G(W)$');
make_latex(hfig); setAllFonts(hfig, 14);


%% Even more plotting - do the divergence for the hot shots! 
% If we had hit the lower energies or anything like this, we ought to be
% able to tell!

hfig = figure(565); clf(hfig); hfig.Position = [100 200 800 600];
set(gca, 'NextPlot', 'add', 'Box', 'on');

ccol = brewermap(9, 'Set1');
hh = [];
it = 1;
for i=fSdat.NS_optimum
    divv = nan(size(enax));
    fdat = load(fullfile(datfol, flist(i).name));
    imm = fdat.shotdata.image;
    imm = medfilt2(imm);
    for k=1:size(imm, 2)
        try 
            divv(k) = widthfwhm(smooth(imm(:,k))'); 
        catch
            continue; 
        end
    end
    hh(end+1) = plot(enax, smooth(divv), 'Color', ccol(it,:)); it = it+1;
end
set(gca, 'XLim', [300 2500], 'YLim', [10 35]);
legend(hh, num2str(fSdat.NS_optimum'));