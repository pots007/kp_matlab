% saveSimulationReferenceSpectra.m
%
% This will save spectra in Matteo's preferred format, for multiparticle
% simulations. This will do it for the high RR and for the medium RR we
% see, in the hope that we can explain the medium RR by spatial
% misalignment.
% And this for the medium RR we saw

% Set up life
edatafol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis', ...
    '20151109r003', 'mat');
savfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'MultiParticleSimulations',...
    'Kris_20171202');
edatalist = dir(fullfile(edatafol, '*Espec.mat'));
% FROM plot2015Zepf_EspecOnly.m - set up life
trackfile = 'Tracking_20170309_KP_4mrad.mat';
% Trackfile, more recent, more sensible?
%trackfile = 'Tracking_20170802_KP_4mrad_05mrad_div.mat';

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

% Load the divergence data...
fDiv = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'MultiParticleSimulations',...
    'Kris_20171202', 'divFitData'));

saveSpec = 1;
saveDiv = 1;

% This is for the high energy one.
shotss = [3,7,33,34];
type = {'lower', 'medium', 'upper'};
type_track = {'lowfit', 'valfit', 'highfit'};
if 1
    hfig = figure(90091); clf(hfig);
    set(gca, 'NextPlot', 'add')
    hh = [];
    for t = 1:length(type)
        ydat = [];
        for k=shotss
            fdat = load(fullfile(edatafol, edatalist(k).name));
            enax = tracking.(type_track{t})(fdat.shotdata.alongscreen);
            dE = gradient(enax);
            ydat(end+1,:) = fdat.shotdata.dQ_dx'./dE;
        end
        meanSpec = mean(ydat, 1);
        if saveSpec
            fid = fopen(fullfile(savfol, ['AverageColdSpectra_shots_3_7_33_34_' type{t} '.txt']), 'w');
            for k=1:1320%length(meanSpec)
                fprintf(fid, '%2.6f\t%2.6f\n', enax(k), meanSpec(k));
            end
            fclose(fid);
        end
        if saveDiv
            fid = fopen(fullfile(savfol, ['AverageColdDivergence_shots_3_7_33_34_' type{t} '.txt']), 'w');
            for k=1:1320%length(meanSpec)
                fprintf(fid, '%2.6f\t%2.6f\n', enax(k), fDiv.mean_div{2}(k));
            end
            fclose(fid);
            fopen(fullfile(savfol, ['AverageColdDivergenceFit_shots_3_7_33_34.txt']), 'w');
            coeffs = coeffvalues(fDiv.hFit{2});
            for k=1:length(coeffs)
                fprintf(fid, '%1.12e\n', coeffs(k));
            end
            fclose(fid);
        end
        hh(end+1) = plot(enax, meanSpec);
        set(gca, 'XLim', [300 2500]);
    end
    legend(hh, type);
end

%% This is for the high energy one.
shotss = [10,33,44];
type = {'lower', 'medium', 'upper'};
type_track = {'lowfit', 'valfit', 'highfit'};
if 1
    hfig = figure(90091); clf(hfig);
    set(gca, 'NextPlot', 'add')
    hh = [];
    for t = 1:length(type)
        ydat = [];
        for k=shotss
            fdat = load(fullfile(edatafol, edatalist(k).name));
            enax = tracking.(type_track{t})(fdat.shotdata.alongscreen);
            dE = gradient(enax);
            ydat(end+1,:) = fdat.shotdata.dQ_dx'./dE;
        end
        meanSpec = mean(ydat, 1);
        if saveSpec
            fid = fopen(fullfile(savfol, ['AverageColdSpectra_shots_10_33_44_' type{t} '.txt']), 'w');
            for k=1:1320%length(meanSpec)
                fprintf(fid, '%2.6f\t%2.6f\n', enax(k), meanSpec(k));
            end
            fclose(fid);
        end
        if saveDiv
            fid = fopen(fullfile(savfol, ['AverageColdDivergence_shots_10_33_44_' type{t} '.txt']), 'w');
            for k=1:1320%length(meanSpec)
                fprintf(fid, '%2.6f\t%2.6f\n', enax(k), fDiv.mean_div{1}(k));
            end
            fclose(fid);
            fopen(fullfile(savfol, ['AverageColdDivergenceFit_shots_10_33_44.txt']), 'w');
            coeffs = coeffvalues(fDiv.hFit{1});
            for k=1:length(coeffs)
                fprintf(fid, '%1.12e\n', coeffs(k));
            end
            fclose(fid);
        end
        hh(end+1) = plot(enax, meanSpec);
        set(gca, 'XLim', [300 2500]);
    end
    legend(hh, type);
end

%% Check! Also plot it for Matteo, so he can tell the difference
flist = dir(fullfile(savfol, 'AAverage*.txt'));
hfig = figure(8998); clf(hfig); set(hfig, 'Position', [1000 100 800 800]);
set(gca, 'NextPlot', 'add', 'LineWidth', 2);
hh = [];
for k=1:length(flist)
    dat = importdata(fullfile(savfol, flist(k).name));
    hh(end+1) = plot(dat(:,1), dat(:,2), '-', 'Tag', flist(k).name);
end
set(hh, 'LineWidth', 1.5);
set(hh(1:3), 'LineStyle', '--');
for i=1:3; set(hh(i+3), 'Color', get(hh(i), 'Color')); end
legs = {'High RR, low', 'High RR, med', 'High RR, high',...
        'Med RR, low', 'Med RR, med', 'Med RR, high'};
legend(hh, legs)
set(gca, 'Box', 'on', 'XLim', [350, 2600], 'YLim', [-0.002 0.2]);
ylabel('Electron spectrum / au');
xlabel('Electron energy / MeV')
make_latex(hfig); setAllFonts(hfig, 15);
%export_fig(fullfile(savfol, 'RR_comp'), '-pdf', '-nocrop', hfig);
%% compare to old
files = {'AverageColdSpectra_shots_3_7_33_34_medium.txt', 'old_cold_medium.txt'};
hfig = figure(8998); clf(hfig); set(gca, 'NextPlot', 'add');
hh = [];
for k=1:length(files)
    dat = importdata(fullfile(savfol, files{k}));
    hh(end+1) = plot(dat(:,1), dat(:,2), '-');
end
legend(hh, files)