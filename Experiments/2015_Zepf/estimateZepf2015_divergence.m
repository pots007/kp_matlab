% estimateZepf2015_divergence.m
%
% Using the tracked distances to the screen, we check what the energy
% dependent divergence of the beam turns out to be.

datfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis', ...
    '20151109r003', 'mat');
savfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'MultiParticleSimulations',...
    'Kris_20171202');
fnames = dir(fullfile(datfol, '*Espec.mat'));

load('trackingDivergenceFit.mat');

trackfile = 'Tracking_20170309_KP_4mrad.mat';

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');

%% Start analysis...

it = 0;
divEnData = [];
shotIDs = [3,7,10,33,34,44];
for shotID = shotIDs
    
    it = it+1;
    fdat = load(fullfile(datfol, fnames(shotID).name));
    %divEnData(:,2,it) = nan(length(fdat.shotdata.alongscreen),1);
    maxVal = max(fdat.shotdata.image(:));
    enax = tracking.valfit(fdat.shotdata.alongscreen);
    dE = gradient(enax);
    dtheta = mean(diff(fdat.shotdata.divax));
    spec = fdat.shotdata.dQ_dx'./dE.*enax;
    divEnData(:,3,it) = spec;%*2.5/max(spec);
    divEnData(:,1,it) = enax;
    % Restrict analysis to 1% of peak current...
    for k=1:size(divEnData, 1)
        thisColumn = fdat.shotdata.image(:,k);
        if max(thisColumn)>0.025*maxVal
            % The screen width is 100mm!!!!!!
% -----------------############## BIG mistake here before.
% The value that divfit gives is the total distance to the screen at that
% point on the screen. THUS, the below line is correct, but meaningless as
% it does not relate to the pixel size of the image. What needs to be done
% is below: recalibrate the pixel size, which is done by going back to
% screen height and then multiplying with the actual distance:
            calibFactor0 = 100/divfit(fdat.shotdata.alongscreen(k)); % WRONG            
            calibFactor(k) = dtheta*2243.5/divfit(fdat.shotdata.alongscreen(k)); % CORRECT
            calibChange(k) = calibFactor0/calibFactor(k);
            %divEnData(k,2,it) = widthfwhm(smooth(thisColumn)')*calibFactor;
            % Try a rolling 5 point average
            if k>size(divEnData,1)-5
                continue;
            end
            coll = mean(fdat.shotdata.image(:,k:k+5),2);
            divEnData(k,2,it) = widthfwhm(smooth(coll)')*calibFactor(k);
        end
    end
    % Tru to remove the annoyign bumppppp
    
    hfig = figure(783245); 
    if it==1; clf(hfig); end
    set(gca, 'NextPlot', 'add', 'Box', 'on', 'YLim', [0 2.5], 'XLim', [300 2700]);
    axx = gca;
    plot(divEnData(:,1,it), smooth(divEnData(:,2,it)), 'o-', 'Tag', 'div');
    plot(divEnData(:,1,it), divEnData(:,3,it)*2.5/max(divEnData(:,3,it)), '-r');
    xlabel('Energy (MeV)');
    ylabel({'FWHM divergence (mrad)', 'Electrons per MeV per 0.1% bandwidth (a.u.)'})
    legend(get(gca, 'Children'), {'Spectrum', 'Divergence'})
    grid on
    setAllFonts(gcf, 16);
    %export_fig(fullfile(savfol, sprintf('shot_%03i', shotID)), '-pdf', '-nocrop', gcf);
    if 0
        fid = fopen(fullfile(savfol, 'div_data', sprintf('shot_%03i.dat', shotID)), 'w');
        for k=1:size(divEnData, 1)
            fprintf(fid, '%2.5e, %2.5e, %2.5e\n', divEnData(k,:,it));
        end
        fclose(fid);
    end
    drawnow;
end

hObj = get(gca, 'Children');
legend(hObj([1 2 4 6 8]), {'Spectra', 'Shot 34', 'Shot 10', 'Shot 7', 'Shot 3'});


%% Now try to get the average divergence:

% Medium ref shots are 10,33,34
% Hard ref shots are 3,7,33,34
hfig = figure(7834); clf(hfig); hfig.Position = [120 140 1600 500];
set(gca, 'nextPlot', 'add');
reffs = {[10,33,44], [3,7,33,34]};
% Find which elements we need
clear logm0;
for k=1:2
    [~,~,logm0{k}] = intersect(reffs{k}, shotIDs);
end
mean_divs = [];
mean_spec = [];
for k=1:size(divEnData,3)
    spec = divEnData(:,3,k);
    div = divEnData(:,2,k);
    % Keep divergence data for places with meaningful spectral signal
    logm = spec<0.01*max(spec) | div<0.6;
    div(logm) = nan;
    mean_divs(:,k) = div;
    mean_specs(:,k) = spec;
end
% Now find the mean spectrum and divergence, and do a fit.
hh = [];
for k=1:2
    mean_div{k} = mean(mean_divs(:,logm0{k}), 2, 'omitnan');
    logm = ~isnan(mean_div{k});
    hFit{k} = fit(enax(logm), mean_div{k}(logm), 'poly5');
    mean_spec{k} = mean(mean_specs(:,logm0{k}),2);
    hh(1,k) = plot(enax, mean_div{k}, '-k');
    hh(2,k) = plot(enax, mean_spec{k}./enax*10, '-b');
    hh(3,k) = plot(enax, hFit{k}(enax), '-r');
    ffun = @(a,b,c,x) 1e-3*(a*exp(b*x) + c);
    %hFit2{k} = fit(enax(logm), mean_div{k}(logm), ffun,...
    %    'StartPoint', [1e-3, -1e-2, 0.7]);
end
plot(enax, (30.44*exp(-1e-2*enax) + 0.7), '-g')
legendflex(hh(:), {'Medium RR, div.', 'Medium RR, spec', 'Medium RR, fit',...
    'Hard RR, div.', 'Hard RR, spec', 'Hard RR, fit'}, 'anchor', [2,2],...
    'buffer', [10 10],'nrow', 3, 'FontSize', 14);

set(hh(:,1), 'LineStyle', '--');
set(hh, 'LineWidth', 2);
grid on;
set(gca, 'XLim', [350 2600], 'YLim', [0 2], 'Box', 'on', 'LineWidth', 2);
xlabel('Energy / MeV');
ylabel({'FWHM Divergence / mrad', 'Spectrum / a.u.'})
setAllFonts(hfig, 16); make_latex(hfig);
%export_fig(fullfile(savfol, 'DivFits'), '-pdf', '-nocrop', hfig);
%save(fullfile(savfol, 'divFitData'), 'hFit', 'mean_spec', 'mean_div', 'enax', 'reffs');