% compareZepf2015_SimExpData3.m
%
% Script to quickly compare results from simulations with the optimum,
% experimental results. This borrows heavily from
% RadiationReaction_fig4_rev1.m for the main traces of the experimental
% data.
%
% This works for simulations from 20171215.
%
% This will plot the comparison figure. Four panels with different models
% and their predictions.

% Set up graphics

hfig = figure(1115); clf(hfig);
papsize = [14 10];
set(hfig, 'Position', [100 100 papsize*50], 'Color', 'w', 'Paperunits', 'centimeters',...
    'Papersize', papsize);

locs = GetFigureArray(2, 2, [0.02 0.02 0.1 0.1], 0.1, 'across');
clear axx;
for k=1:4
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add');
end
set(axx, 'LineWidth', 1, 'Box', 'on');

% ------------ And set up the shots!!
hotShots = [14 17 15 26 18];
%coldShots = {40, 10, [9,31], 10, [3,10,34]}; % OLDDDD
coldShots = {40, [10,33], [9,31], [10,33,44], [3,7,33,34]}; %NEWWWW
edatafol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis', ...
    '20151109r003', 'mat');
edatalist = dir(fullfile(edatafol, '*Espec.mat'));
ID = 5;
hotShotID = hotShots(ID); % The shot ID of the hot shot.
coldShotID = coldShots{ID}; % Using the average cold spectra
plotCold = 1;

% ------------ Set up tracking
% FROM plot2015Zepf_EspecOnly.m - set up life
trackfile = 'Tracking_20170309_KP_4mrad.mat';

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');
simEnaxis = 200:3000;

plotPerMeV = false;  % Whether we plot per MeV or per % energy spread


%% Now the actual plotting

%simtypes = {'LL', 'QED', 'SemiClass', 'Perturbative'};
fstem = 'shots_3_7_33_34_';
simtypes = {'Perturb', 'LL', 'SemiClass', 'QED'};
simtypesDisp = {'Perturbative', 'LL', 'Semiclassical', 'QED'};
sima0s= {'', '_a0-5', '_a0+5'};
savenames = {'withoutLaserErr', 'withLaserErr'};

simfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'MultiParticleSimulations',...
    'Matteo_20171215');
colzz = brewermap(9, 'Set1');
simCol = colzz(3,:);
simID = 2; % 1 for normal laser a0, 2 to include laser intensity errors.

for k=1:length(axx)
    cla(axx(k));
    fdat = load(fullfile(edatafol, edatalist(hotShotID).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen);
    dE = gradient(enax);
    hotShot = smooth(fdat.shotdata.dQ_dx'./dE);
    if ~plotPerMeV; hotShot = hotShot.*enax; else hotShot = hotShot*7e2; end;
    nfac_0 = sum(fdat.shotdata.dQ_dx);
    clear hh;
    hh(1) = plot(axx(k), enax, hotShot./nfac_0, 'r-');
    ydat = [];
    for m=1:length(coldShotID)
        fdat = load(fullfile(edatafol, edatalist(coldShotID(m)).name));
        coldShot = smooth(fdat.shotdata.dQ_dx'./dE);
        if ~plotPerMeV; coldShot = coldShot.*enax; else coldShot = coldShot*7e2; end;
        nfac = sum(fdat.shotdata.dQ_dx);
        ydat(:,end+1) = coldShot./nfac;
        %hh(end+1) = plot(axx(k), enax, coldShot./nfac, 'k-');
    end
    if plotCold; hh(end+1) = plot(axx(k), enax, mean(ydat, 2), '-k'); end
    % --------------------- Now plot the simulation stuff
    %simfolname = fullfile(simfol, sima0, 'Medium', simtypes{k});
    %simFlist = dir(fullfile(simfolname, '*.txt'));
    % Choose the time delay we use: no 5 is 96fs, no 4 is 64 fs.
    %fn = simFlist(simID).name;
    datName = fullfile(simfol, 'Medium', ...
        [fstem 'Medium_' simtypes{k} '_64fs.txt']); 
    simData = importdata(datName);
    nfac_ = 1.1*nfac_0/(sum(simData(:,2))*mean(diff(simData(:,1))));
    simSpec = interp1(simData(:,1), smooth(simData(:,2).*simData(:,1)*0.01*nfac_), simEnaxis);
    datLowerFname = fullfile(simfol, 'Lower', ...
        [fstem 'Lower_' simtypes{k} sima0s{1 + (simID==2)*2} '_64fs.txt']);
    datUpperFname = fullfile(simfol, 'Upper', ...
        [fstem 'Upper_' simtypes{k} sima0s{1 + (simID==2)*1} '_64fs.txt']);
    % The below are for the case we only use the spectrum error
    %datLowerFname = fullfile(simfol, sima0, 'Lower', simtypes{k}, fn);
    %datUpperFname = fullfile(simfol, sima0, 'Upper', simtypes{k}, fn);
    % Open the data
    datLower = importdata(datLowerFname);
    datUpper = importdata(datUpperFname);
    % And interpolate it to a common energy grid
    datLowerPlot = interp1(datLower(:,1)', smooth(datLower(:,2).*datLower(:,1)*0.01*nfac_), simEnaxis);
    datUpperPlot = interp1(datUpper(:,1)', smooth(datUpper(:,2).*datUpper(:,1)*0.01*nfac_), simEnaxis);
    % And plot it
    axes(axx(k));
    hhh = shadedErrorBar(simEnaxis, simSpec, [smooth(datUpperPlot-simSpec)'; smooth(simSpec-datLowerPlot)'],...
        {'-', 'Color', simCol}, 1);
    hh(end+1) = hhh.mainLine;
    % And set a suitable level of alpha:
    hhh.patch.FaceAlpha = 0.4;
    % Seems the patches CANNNOT be saved in a sensible manner.
    delete(hhh.patch);
    % So try to use area instead
    ha = area(axx(k), simEnaxis, [smooth(datLowerPlot)'; smooth(datUpperPlot-datLowerPlot)']');
    set(ha(1), 'FaceColor', 'none', 'ShowBaseLine', 'off');
    ha(2).FaceColor = [0.8196    0.9216    0.8157];
    set(ha, 'EdgeColor', 'none');
    uistack(hh, 'top'); % Ensure the real data is on top of the sim results
    % And add the legend!
    hleg = legend(hh, {'Scatt. laser ON', 'Scatt. laser OFF', simtypesDisp{k}}, 'Location', 'NE', 'Box', 'off');
   
    if k==4 && 1
        % --------------------- And adding the PIC simulation results 
        %if simID==4
        %    PICdat = importdata(fullfile(simfol, 'PIC_energy_spec_after_64.txt'));
        %else
        %    PICdat = importdata(fullfile(simfol, 'PIC_energy_spec_after_96.txt'));
        %end
        %nfac_ = 1.1*nfac_0/(sum(PICdat(:,2))*mean(diff(PICdat(:,1))));
        %PICPlot = interp1(0.511*PICdat(:,1)', PICdat(:,2), simEnaxis);
        %hLegends = findobj(hfig, 'Type', 'Legend');
        %hh(end+1) = plot(axx(k), simEnaxis, PICPlot.*simEnaxis/8, '-b');
        %delete(hleg);
        %hleg = legend(hh, {'Scatt. laser ON', 'Scatt. laser OFF', simtypesDisp{k}, 'PIC'}, 'Location', 'NE', 'Box', 'off');
        %uistack(hh, 'top');
    end
    
    % ------------- Making things nice
    set(hh, 'LineWidth', 2); hh(1).LineWidth = 2;
    xlabel(axx(k), 'Electron energy (MeV)')
    if plotPerMeV
        ylabel(axx(k), 'Electrons per MeV (a.u.)');
    else
        ylabel(axx(k), {'Electrons per MeV' ' per % energy spread (a.u.)'})
    end
    ht(k) = text(410, 0.1, ['' char(96+k) ''], 'Parent', axx(k), 'FontWeight', 'bold');
    drawnow;
    
end


set(axx, 'XLim', [350 2200], 'YLim', [-0.02 1.2], 'Layer', 'top');
set(ht, 'FontSize', 13);

%% Saving part

export_fig(fullfile(simfol, ['Comp_20171215_' savenames{simID}]), '-pdf', '-nocrop', hfig);