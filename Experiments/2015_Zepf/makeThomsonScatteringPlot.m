% makeThomsonScatteringPlot.m

if ismac
    datfol = '/Users/kristjanpoder/Experiments/2015GeminiZepf/20151028/20151028r004/';
    savfol = '/Users/kristjanpoder/Experiments/2015GeminiZepf/20151028/';
end

figure(150); clf(150);
set(150, 'Color', 'w');
locs = GetFigureArray(10,3,[0.05 0.05 0.05 0.15], 0.05, 'across');
axss = zeros(30,1);
for i=1:30
    im = imread(fullfile(datfol, sprintf('20151028r004s%03i_GammaDet2.tif', i)));
    axss(i) = axes('Parent', 150, 'Position', locs(:,i));
    imagesc(im(375:475,400:500), 'Parent', axss(i));
end
set(axss, 'XTick', [], 'YTick', [], 'CLim', [00 400]);
%%
clf(150);
set(150, 'Color', 'w' ,'Position', [100 100 1000 600]);
ax1 = axes('Parent', 150, 'Position', [0.05 0.05 0.4 0.8]);
ax2 = axes('Parent', 150, 'Position', [0.5 0.05 0.4 0.8]);
nolas = zeros(1024);
for i=1:10
    nolas = nolas + double(imread(fullfile(datfol, sprintf('20151028r004s%03i_GammaDet2.tif', i))));
end
wlas = zeros(1024);
for i=21:30
    wlas = wlas + double(imread(fullfile(datfol, sprintf('20151028r004s%03i_GammaDet2.tif', i))));
end
imagesc(nolas(325:525,350:550)./max(wlas(:)), 'Parent', ax1);
title(ax1, 'Without laser');
imagesc(wlas(325:525,350:550)./max(wlas(:)), 'Parent', ax2);
cbar = colorbar('Peer', ax2, 'YTick', [0 1]);
ylabel(cbar, 'Gamma signal / a.u.');
title(ax2, 'With laser');
set([ax1 ax2], 'XTick', [], 'YTick', [], 'CLim', [0 1]);
colormap(parula);
setAllFonts(150, 40); make_latex(150);
set(cbar, 'YTick', [0 1]);
export_fig(fullfile(savfol, 'Thomson_signal'), '-pdf', '-nocrop', 150);