% checkThomsonFitQuality.m
%
% Try to look at the pointing during the hot shots' run

saveplots = 0;
savfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'GammaFits');


fdat = load(fullfile(getGDrive,'Experiment', '2015_Zepf', 'GammaFits', 'fitData'));
fittedShots = fdat.fitNData_2G(:,1);
fSdat = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'shotdata'));
shotsWeWant = fSdat.NS_optimum;
[~,iA,iB] = intersect(shotsWeWant, fittedShots);

plotXp = 5;
plotYp = 4;
shotID = 4;
plotX = 1;

fitFcn2 = @(a1,x01,xw1,y01,yw1,a2,x02,xw2,y02,yw2,x,y) a1*exp( -((x-x01).^2/(2*xw1^2) + (y-y01).^2/(2*yw1^2)) ) +...
    a2*exp(   -((x-x02).^2/(2*xw2^2) + (y-y02).^2/(2*yw2^2)) );
datXaxis = fdat.pixelaxisx;
datYaxis = fdat.pixelaxisy;
plotXaxis = (1:1000)*0.1;% - datXaxis(plotXp);
plotYaxis = (1:1000)*0.1;% - datYaxis(plotYp);

% Try to plot a lineout for both axis, for the fit and the data.

hfig = figure(8587); clf(hfig);
set(gca, 'NextPlot', 'add');
for s=1:length(iB)
    ff = fdat.fitNData_2G(iB(s), 2:end);
    if plotX
        fitSig = fitFcn2(ff(1), ff(2), ff(3), ff(4), ff(5), ff(6), ff(7), ff(8),...
            ff(9), ff(10), plotXaxis, datYaxis(plotYp));
        hh(s) = plot(plotXaxis, fitSig./max(fitSig), '-');
        plot(fdat.pixelaxisx, fdat.fitData{iB(s),1}(plotYp,:)./max(fitSig), '-o', 'Color', get(hh(s), 'Color'));
    else
        fitSig = fitFcn2(ff(1), ff(2), ff(3), ff(4), ff(5), ff(6), ff(7), ff(8),...
            ff(9), ff(10), datXaxis(plotXp), plotYaxis);
        hh(s) = plot(plotYaxis, fitSig./max(fitSig), '-');
    end
    %plot(plotXaxis, fitSig,...
    %    fdat.pixelaxisx, fdat.fitData{shotID,1}(plotYp,:), '-o')
    leglab{s} = num2str(shotsWeWant(s));
end
legend(hh, leglab);


%% Plot the fitting residuals for all shots, in one plot.
hfig = figure(795); clf(hfig);
locs = GetFigureArray(9, 3, [0.04 0.02 0.02 0.02], 0.01, 'across');
hfig.Position = [70 250 1800 690];
Non_shots = fSdat.Non.*(1:49);
Non_shots = Non_shots(Non_shots~=0);
for k=1:length(Non_shots)
    axs(k) = makeNiceAxes(hfig, locs(:,k));
    imagesc(fdat.fitData{k,3} - fdat.fitData{k,1})
    axis image
    title(axs(k), sprintf('Shot %i', fdat.fitNData_2G(k,1)));
    set(axs(k), 'CLim', [-1 1]*0.05*max(fdat.fitData{k,3}(:)));
end
colormap(brewermap(256, 'RdBu'));
set(axs, 'XTick', [], 'YTick', []);
if saveplots
    export_fig(fullfile(savfol, 'Fits_2G_residuals_5percent'), '-pdf', '-nocrop', hfig)
end

%% Also plot some results from the fitting: signal sizes

hfig = figure(765); clf(hfig);
%plot(fitNData_2G(:,1), fitNData_2G(:,[4 6 9 11]), 'o-'); 
sig_w = sqrt(abs(prod(fdat.fitNData_2G(:,[4 6]),2)));
bg_w = sqrt(abs(prod(fdat.fitNData_2G(:,[9 11]), 2)));

% Find the integral of the fitted signals
fitFcn2 = @(a1,x01,xw1,y01,yw1,a2,x02,xw2,y02,yw2,x,y) a1*exp( -((x-x01).^2/(2*xw1^2) + (y-y01).^2/(2*yw1^2)) ) +...
    a2*exp(   -((x-x02).^2/(2*xw2^2) + (y-y02).^2/(2*yw2^2)) );
[FX, FY] = meshgrid(0:1:80, 0:1:70);
fitInts = zeros(2, size(fdat.fitNData_2G, 1));
for k=1:size(fitInts,2)
    fitInts(1,k) = fdat.fitNData_2G(k,1);
    f = fdat.fitNData_2G(k,:);
    fitSig = fitFcn2(f(2), f(3), f(4), f(5), f(6), f(7), f(8), f(9), f(10), f(11), FX, FY);
    fitInts(2,k) = sum(fitSig(:));
end

% Esigsum is from plot2015_Zepf_Especs_ThomsonOnly.m
scatter(sig_w, bg_w, 45, fitInts(2,:)'./Esigsum(fdat.fitNData_2G(:,1)), 'filled');
set(gca, 'XLim', [0 5], 'Box', 'on');
grid on;
colormap(parula);
cb = colorbar;
xlabel('Signal width $w_s = \sqrt{w_xw_y}$ (mm)');
ylabel('Background width $w_s = \sqrt{w_xw_y}$ (mm)');
ylabel(cb, 'Signal integral (a.u.)');
tt = text(sig_w, bg_w, num2str(fdat.fitNData_2G(:,1)));
make_latex(hfig); setAllFonts(hfig, 14);
set(tt, 'FontSize', 12, 'VerticalAlignment', 'top');
if saveplots
    export_fig(fullfile(savfol, 'Fit_widths'), '-pdf', '-nocrop', hfig);
    export_fig(fullfile(savfol, 'Fit_widths'), '-png', '-nocrop', hfig);
end

%% Plot the total integral for each shot along with sum of signal of data
hfig = figure(769); clf(hfig);
ax = makeNiceAxes(hfig);

fit_sums = fitInts(2,:)'./Esigsum(fdat.fitNData_2G(:,1));
data_sums = zeros(size(fit_sums));
hasSat = logical(size(fit_sums));
for k=1:length(fit_sums)
    tt = fdat.fitData{k,1};
    hasSat(k) = sum(isnan(tt(:)));
    tt(isnan(tt)) = 2^16;
    data_sums(k) = sum(tt(:));
end
data_sums = data_sums./Esigsum(fdat.fitNData_2G(:,1));
%h1 = plot(Non_shots, data_sums, 'x');
%h2 = plot(Non_shots, fit_sums/35, 'o');
plot(data_sums, fit_sums/6.3^2, '.', 'MarkerSize', 38, 'LineWidth', 2);
plot(data_sums(hasSat), fit_sums(hasSat)/6.3^2, '.r', 'MarkerSize', 38, 'LineWidth', 2);
plot([0 0.4], [0 0.4], '--k');
%legend([h1 h2], {'Data', 'Fit'}, 'Location', 'NE', 'Box', 'on', 'EdgeColor', 'w');
xlabel('Norm yield from integral of chip / a.u.');
ylabel('Norm yield of integral of fit / a.u.');
grid on; grid minor;
setAllFonts(hfig, 14);
tt = text(double(data_sums)+0.01, double(fit_sums/6.3^2), num2str(fdat.fitNData_2G(:,1)));
if saveplots
    export_fig(fullfile(savfol, 'fit_vs_data'), '-png', '-m4', '-nocrop', hfig)
end

%% Plot the fit centre points as pointing, as scatter
hfig = figure(766); clf(hfig);
gamma_pointing_x = fdat.fitNData_2G(:,3)*5/4; 
gamma_pointing_y = fdat.fitNData_2G(:,5)*5/4; 
gamma_yield_integral = fitInts(2,:)'./Esigsum(fdat.fitNData_2G(:,1));
g_dat_ss = sort(gamma_yield_integral);
logm = gamma_yield_integral > g_dat_ss(end-9);
gamma_pointing_x = gamma_pointing_x - mean(gamma_pointing_x(logm));
gamma_pointing_y = gamma_pointing_y - mean(gamma_pointing_y(logm));
scatter(gamma_pointing_x(logm), gamma_pointing_y(logm), 45, gamma_yield_integral(logm), 'filled');
%set(gca, 'XLim', [-2 2], 'Box', 'on');
grid on;
colormap(parula);
cb = colorbar;
xlabel('Signal x pointing (mrad)');
ylabel('Signal y pointing (mrad)');
ylabel(cb, 'Signal integral (a.u.)');
tt = text(gamma_pointing_x, gamma_pointing_y, num2str(fdat.fitNData_2G(:,1)));
make_latex(hfig); setAllFonts(hfig, 14);
set(tt, 'FontSize', 10, 'VerticalAlignment', 'top');
if saveplots
    export_fig(fullfile(savfol, 'Fit_pointing'), '-pdf', '-nocrop', hfig);
    export_fig(fullfile(savfol, 'Fit_pointing'), '-png', '-nocrop', hfig);
end
fitShots = fdat.fitNData_2G(:,1);
save(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'gamma_fit_analysis'),...
    'gamma_pointing_x', 'gamma_pointing_y', 'gamma_yield_integral', 'fitShots')