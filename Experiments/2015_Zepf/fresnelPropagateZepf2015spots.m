% fresnelPropagateZepf2015spots.m
%
% Try to get Tamburini profiles

imID = 70;
imScale = 0.2e-6;
datfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'Focus', '20151102');
flist = dir(fullfile(datfol, '*newOAP_withBung*'));
imm = ReadRAW16bit(fullfile(datfol, flist(imID).name), 640, 480);

% Now do BG removal
BGim = imm;  BGim(100:380, 100:540) = nan; BG = mean(mean(BGim(~isnan(BGim))));
imm = imm - BG;
% Now do the calibration to intensity:
totIm = sum(imm(:))*(imScale*1e-6)^2;
%calfac = sqrt(pi*40e-15^2);
calfac = 8/totIm;
calf = calfac/(40e-15)*1e-4;
imm = imm*calf;
imm = 0.856*0.8*sqrt(abs(imm*1e-18));
ccc = centroidlocationsmatrix(imm);
xax = ((1:size(imm,2))-ccc(1))*imScale;
yax = ((1:size(imm,1))-ccc(2))*imScale;
imagesc(xax, yax, sqrt(abs(imm)));

U0 = sqrt(abs(imm));
newax = linspace(-30, 30, 1024)*1e-6;
newim = interp2(xax, yax, U0, newax, newax');

%%
hfig = figure(876); clf(hfig);
clear axx;
lins_x = zeros(1024, 9);
peakI = max(abs(newim(:)).^2);
for k=1:9
    axx(k) = subplot(3, 3, k);
    U1 = fresnelPropagator(newim, mean(diff(newax)), mean(diff(newax)), (k-1)*5e-6, 800e-9);
    imagesc(newax, newax, abs(U1).^2);
    title(sprintf('%i um, I_0/I=%1.3f', (k-1)*5, peakI/max(abs(U1(:)).^2)));
    lins_x(:,k) = smooth(abs(U1(:,512)).^2);
    cb = colorbar(axx(k));
end
limss = cell2mat(get(axx, 'CLim'));
set(axx, 'CLim', [min(limss(:)) max(limss(:))]);

