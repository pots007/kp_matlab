% plot2015Zepf_EspecMontages.m
%
% Just plot a lot of different electron beam montages
%
% Run the first section of plot2015Zepf_EspecOnly.m first!

%% As a first quick thing, plot montage of all S only shots.
% Plot them on a common colour axis as well.
Sshots = fSdat.Noff.*(1:49);
Sshots = Sshots(Sshots~=0);
nSshots = length(Sshots);

locs = GetFigureArray(nSshots, 1, [0.07 0.01 0.04 0.05], 0.005, 'across');
hfig = figure(515); clf(hfig); hfig.Color = 'w';
hax = [];
mvals = [];
enlines = [500 1000 1500 2200];
for k=1:length(enlines)
    [~, enind(k)] = min(abs(enax-enlines(k)));
end
for i = 1:nSshots
    hax(i) = axes('Parent', hfig, 'Position', locs(:,i));
    fdat = load(fullfile(datfol, flist(Sshots(i)).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen)';
    mvals(i) = max(max(fdat.shotdata.image));
    imagesc(fdat.shotdata.image');
    title(num2str(Sshots(i)));
    %edat(:,i) = fdat.shotdata.dQ_dx;
end
linkaxes(hax, 'xy');
set(hax, 'YDir', 'normal', 'XTick', [], 'Ytick', enind, 'YTickLabel', [],...
    'CLim', [0 max(mvals)], 'YLim', [1 1450]);
set(hax(1), 'YTickLabel', num2str(enlines'))
ylabel(hax(1), 'Energy / MeV');
make_latex(hfig); setAllFonts(hfig, 14);

if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'espec_all_refshots'), '-pdf', '-nocrop', hfig);
end

%% Now, all shots where N beam was on.
% Plot them on a common colour axis as well.
Sshots = fSdat.Non.*(1:49);
Sshots = Sshots(Sshots~=0);
nSshots = length(Sshots);

locs = GetFigureArray(nSshots, 1, [0.07 0.01 0.04 0.05], 0.005, 'across');
hfig = figure(515); clf(hfig); hfig.Color = 'w';
hax = [];
mvals = [];
enlines = [500 1000 1500 2200];
for k=1:length(enlines)
    [~, enind(k)] = min(abs(enax-enlines(k)));
end
for i = 1:nSshots
    hax(i) = axes('Parent', hfig, 'Position', locs(:,i));
    fdat = load(fullfile(datfol, flist(Sshots(i)).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen)';
    mvals(i) = max(max(fdat.shotdata.image));
    imagesc(fdat.shotdata.image');
    title(num2str(Sshots(i)));
    if ~isempty(find(fSdat.NS_optimum==Sshots(i), 1))
        title(num2str(Sshots(i)), 'Color', 'r');
    end
end
linkaxes(hax, 'xy');
set(hax, 'YDir', 'normal', 'XTick', [], 'Ytick', enind, 'YTickLabel', [],...
    'CLim', [0 max(mvals)], 'YLim', [1 1450]);
set(hax(1), 'YTickLabel', num2str(enlines'))
ylabel(hax(1), 'Energy / MeV');
make_latex(hfig); setAllFonts(hfig, 14);

if saveplots
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'espec_all_hotshots'), '-pdf', '-nocrop', hfig);
end
