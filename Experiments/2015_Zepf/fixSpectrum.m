% 
%
% 

function outline = fixSpectrum(inline, location)
xax = 1:length(inline);
logm = true(size(inline));
% This is the first place to fix: the high energy place
if location==1
    inline = smooth(inline);
    logm(1050:1110) = false; % 1125 works pretty goodddddd
    outline = interp1(xax(logm), inline(logm), xax, 'pchip');
end
% This is for the low energy crap
if location==2
    logm(570:780) = false;
    outline = interp1(xax(logm), inline(logm), xax, 'pchip');
end
% This is for high energy peaks....
if location==3
    logm(1060:1125) = false;
    logm([1:950 1200:end]) = false;
    pfit = polyfit(xax(logm), inline(logm), 9);
    outline = inline;
    outline(1050:1125) = polyval(pfit, xax(1050:1125));
end
