% compareZepf2015_SimExpData2.m
%
% For sim data from 20171122, the offset interactions.
%
% Script to quickly compare results from simulations with the optimum,
% experimental results. This borrows heavily from
% RadiationReaction_fig3_rev1.m for the main traces of the experimental
% data.
%
%
% Set up graphics

hfig = figure(1110); clf(hfig);
papsize = [20 9];
set(hfig, 'Position', [100 100 papsize*50], 'Color', 'w', 'Paperunits', 'centimeters',...
    'Papersize', papsize);

locs = GetFigureArray(3, 1, [0.05 0.03 0.12 0.1], 0.04, 'across');
clear axx;
for k=1:size(locs,2)
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add');
end
set(axx, 'LineWidth', 1, 'Box', 'on');

% And set up the shots!!
hotShots = [14 17 15 26 18];
coldShots = {40, 10, [9,31], 10, [3,10,34]};
% Edit 20171202
coldShots = {40, 10, [9,31], [10, 33], [3, 34, 33]};
edatafol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis', ...
    '20151109r003', 'mat');
edatalist = dir(fullfile(edatafol, '*Espec.mat'));
% FROM plot2015Zepf_EspecOnly.m - set up life
trackfile = 'Tracking_20170309_KP_4mrad.mat';
% Trackfile, more recent, more sensible?
%trackfile = 'Tracking_20170802_KP_4mrad_05mrad_div.mat';

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

plotPerMeV = false;  % Whether we plot per MeV or per % energy spread

%% THIS WORKS FOR 20171122 simulations only!!!!

compshots = [1 4 5]; % Relics from donor script

plotPerMeV = 0;
for k=1:length(axx)
    cla(axx(k));
    ind = compshots(k);
    fdat = load(fullfile(edatafol, edatalist(hotShots(ind)).name));
    enax = tracking.valfit(fdat.shotdata.alongscreen);
    dE = gradient(enax);
    edatalist(hotShots(ind)).name
    hotShot = smooth(fdat.shotdata.dQ_dx'./dE);
    if ~plotPerMeV; hotShot = hotShot.*enax; else hotShot = hotShot*7e2; end;
    nfac_0 = sum(fdat.shotdata.dQ_dx);
    clear hh;
    hh(1) = plot(axx(k), enax, hotShot./nfac_0, 'r-');
    for m=1:length(coldShots{ind})
        fdat = load(fullfile(edatafol, edatalist(coldShots{ind}(m)).name));
        coldShot = smooth(fdat.shotdata.dQ_dx'./dE);
        if ~plotPerMeV; coldShot = coldShot.*enax; else coldShot = coldShot*7e2; end;
        nfac = sum(fdat.shotdata.dQ_dx);
        hh(end+1) = plot(axx(k), enax, coldShot./nfac, 'k-');
    end
    set(hh, 'LineWidth', 1); hh(1).LineWidth = 4;
    xlabel(axx(k), 'Electron energy (MeV)')
    if plotPerMeV
        ylabel(axx(k), 'Electrons per MeV (a.u.)');
    else
        ylabel(axx(k), {'Electrons per MeV' ' per % energy spread (a.u.)'})
    end
    %ht(k) = text(600, 0.1, ['' char(96+k) ''], 'Parent', axx(k), 'FontWeight', 'bold');
end
%
% Norm factor for later on - peak of cold spectrum.
ydat = cell2mat(get(findobj(axx, 'Color', 'k'), 'YData'));

datpath = fullfile(getGDrive, 'Experiment', '2015_Zepf',...
    'MultiParticleSimulations', 'Matteo_20171122');
fols = {'Shift_x', 'Shift_y'};
offsets = 2:2:8;
hhsim = [];
labels = {};
colss = brewermap(9, 'Set1');
for dir_=1:2
    for shift=1:4
        fname = sprintf('Shift%1.2fEnergySemiClass_final.txt', offsets(shift));
        datf = importdata(fullfile(datpath, fols{dir_}, fname));
        spec = smooth(datf(:,2));
        if ~plotPerMeV
            spec = spec.*datf(:,1);
            spec = 0.87*spec./9.3e6; % Fixed value to highlight abs change
        else
            spec = 1.05*spec/1.6e4; 
        end
        hhsim(end+1) = plot(axx(1), datf(:,1), spec, '-', 'Color', colss(shift+1,:));
        for k = 2:length(axx)
            plot(axx(k), datf(:,1), spec, '-', 'Color', colss(shift+1,:));
        end
        labels{end+1} = sprintf('Shift %s, %i um', fols{dir_}(end), offsets(shift));
    end
end

set(hhsim, 'LineWidth', 1.5);
set(hhsim(1:4), 'LineStyle', '--');
set(hhsim(5:end), 'LineStyle', ':');
set(axx, 'XLim', [350 2300], 'YLim', [0 1]);
%legendflex(hhsim, labels, 'anchor', [3 3], 'nrow', 4, 'buffer', [20 20], 'FontSize', 14)
if plotPerMeV; fnam = 'Comparison_perMeV'; else; fnam = 'Comparison_renorm'; end;
%export_fig(fullfile(datpath, fnam), '-pdf', '-nocrop', hfig);



%% Find proper cold shots range...
hotss = [14, 17, 15, 26, 18];
Noff_shots = 1:49; Noff_shots = Noff_shots(fSdat.Noff);
for k=1:5
    lm = abs(laserE_S(Noff_shots) - laserE_S(hotss(k)))<0.375;
    disp(Noff_shots(lm))
end