% This is to set up a rough screen positions matrix for Zepf's run in 2015.

% Find the reference image 
refpath = '/Volumes/GeminiZepf2015/Espec_references/References_20151020';
reffile = '20151019_laserON_scintillatorsOFF_bleedingLanexIN_2200.tif';
refdat = double(imread(fullfile(refpath, reffile)));
% And load the tracking file and set up fit spline
ff = load('Tracking_20160524_KP_4mrad');
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');

%% Calibrate distance along screen
% Just plot it so we know what's happening
hfig = figure(8); clf(hfig);
imagesc(refdat); colormap(gray);
set(gca, 'CLim', [0 2500]);
% Let's find centroid location for the laser spot
centr_ref = refdat; centr_ref(centr_ref<2500) = 0;
[rows, cols] = size(centr_ref);
x = ones(rows,1)*(1:cols);
y = (1:rows)'*ones(1,cols);
totcounts = sum(centr_ref(:));
xbar = sum(sum(centr_ref.*x))/totcounts;
ybar = sum(sum(centr_ref.*y))/totcounts;
hold on; 
plot(xbar, ybar, 'or');
% Hurrah, seems about right!
% Now set up a subimage to calibrate distances.
subref = refdat(810:930, 490:1950);
rectangle('Position', [490 810 1460 120], 'EdgeColor', 'r', 'LineWidth', 2);
hold off;
%% Now do the calibration
clf(hfig);
ax1 = subplot(2,1,1);
imagesc(subref);
title('Red rectangle = calibration region')
ax2 = subplot(2,1,2);
[peakVals,peakLocs] = findpeaks(-smooth(sum(subref)), 'MinPeakDistance', 45);
plot(-smooth(sum(subref))); hold on;
plot(peakLocs, peakVals, 'kv', 'MarkerFaceColor', 'k'); hold off;
title('Peaks - cm graduations');

% Now make a fit for pixel into distance
distFit = fit(peakLocs, (1:length(peakLocs))'-1, 'spline');
% The pixel axis, plus the offset due to us cropping the image earlier
pixel_axis = (1:size(refdat,2)) - 490;
d_along_screen = distFit(pixel_axis);
% These values flip over below 0, but we don't care as there is no data
% there.
% This line ensures the laser axis is actually zero.
d_along_screen = d_along_screen - d_along_screen(round(xbar));

%% And now make the fit for energies:
en_along_screen = tracking.valfit(d_along_screen*10);
% And plot if
clf(hfig);
plot(1:size(refdat,2), en_along_screen, '-k');
xlabel('Pixel on image'); ylabel('Energy at pixel / MeV');
set(gca, 'YLim', [100 2200]);
title('Apparent energy on screen as a function of pixel on image');

% To get the spectrum now, just open the image, do a suitable background
% subtraction, and then full vertical binning. Then, as long as you haven't
% cropped the image, you can do
% plot(en_along_screen, yourSummedImage)
% And that will be your very rough, first order spectrum :)