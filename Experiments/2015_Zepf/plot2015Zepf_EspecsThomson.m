% plot2015Zepf_EspecsThomson.m
%
% Quickly plot the espec images and the Gamma scintillator images, just to
% ensure all shots show the expected spectra depending on N beaing on and
% off.

datfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', '20151109r003');
nshots = 49;

GData = zeros(1, nshots);
GDataraw = zeros(1024, 1024, nshots);
EData = zeros(2560, nshots);
EDataraw = zeros(2160, 2560, nshots, 'single');
divs = nan(2560, nshots);
div_th = 4e4;

if exist(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'BGfree_data.mat'), 'file')
    load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'BGfree_data.mat'));
else
    
    for i=1:nshots
        fprintf('Working on shot %i\n', i);
        gdat = double(imread(fullfile(datfol, sprintf('20151109r003s%03i_GammaDet2.tif', i))));
        gdat = medfilt2(gdat);
        gdat_bg = createBackground(gdat, [340 300 280 300], [], [], 2);
        gdat = gdat - gdat_bg;
        GDataraw(:,:,i) = gdat;
        GData(i) = sum(sum(gdat(270:600, 250:700)));
        edat = double(imread(fullfile(datfol, sprintf('20151109r003s%03i_Espec.tif', i))));
        edat = medfilt2(edat); edat = medfilt2(edat);
        edat_bg = createBackground(edat, [1 800 2559 300], [], [], 2);
        edat = edat - edat_bg;
        EDataraw(:,:,i) = single(edat);
        EData(:,i) = sum(edat(800:1100, :), 1);
    end
    %save('BGfree_data', 'EDataraw', 'GDataraw');
end
%%
    for k=1:2560
        if EData(k,i)<div_th; continue; end
        try
            divs(k, i) = widthfwhm(EDataraw(800:1100,:,k)');
        catch
        end
    end
%end

%% And plot all gamma signals
% But bear in mind that the total signal should be normalised to total
% electron yield....
Esigsum = sum(EDataraw, 1); Esigsum = sum(Esigsum, 2);
Esigsum = squeeze(Esigsum);
hfig = figure(503); clf(hfig); hfig.Color = 'w';
set(gca, 'NextPlot', 'add');
G_threshold = 2.e7;
G_low_threshold = 0.6e7;
%plot(1:49, GData, 'o-', [1 49], G_threshold*[1 1], '--k');
[axx, h1, h2] = plotyy(1:49, GData, 1:49, GData./Esigsum');
set(axx(1), 'NextPlot', 'add');
plot(axx(1), [1 49], G_threshold*[1 1], '--k');
plot(axx(1), [1 49], G_low_threshold*[1 1], '--k');
Non = zeros(1,49); Non([12:15 17:29 35:39 45:49]) = 1; 
Non(Non==0) = nan;
him = imagesc(1:49, -2e7:1e7:2e8, repmat(Non, 10, 1), 'Parent', axx(1));
uistack(him, 'down', 4);
set(h1, 'Marker', 'o'); set(h2, 'Marker', 'd');
set(axx(1), 'YLim', [-1e7 1.6e8], 'Box', 'on', 'Layer', 'top');
set(axx(2), 'YLim', [-0.1 1.6]);
colormap([1 1 1; 0.8 0.8 0.8])
grid on; grid minor;
xlabel('Shot number');
ylabel(axx(1), 'Gamma yield');
ylabel(axx(2), 'Gamma yield/Total charge');
make_latex(hfig); setAllFonts(hfig, 14);


%% Now plot all spectra, with the line color corresponding to the total
% counts on GData diagnostic (no BG subtraction!)
cols = flipud(parula(nshots));
[GData_sort, GData_sort_inds] = sort(GData);
hfig = figure(504); clf(hfig); hfig.Color = 'w';
ax1 = gca; set(ax1, 'NextPlot', 'add', 'Box', 'on');
for i=1:nshots
    ind_ = GData_sort_inds(i);
    plot(1:2560, EData(:, ind_), 'Color', cols(ind_,:));
end
set(ax1, 'XLim', [400 1200]);
cb = colorbar;
minCtick = round(min(GData), 2, 'significant');
maxCtick = round(max(GData), 2, 'significant');
set(cb, 'YTickLabel', linspace(minCtick, maxCtick, 6));
ylabel(ax1, 'Electron signal / a.u.');
xlabel(ax1, 'Pixel');
ylabel(cb, 'Gamma detector signal strength / a.u');
make_latex(hfig); setAllFonts(hfig, 14);

%%  Now colour the highly frictioned shots differently:
cols = parula(nshots);
[GData_sort, GData_sort_inds] = sort(GData);
hfig = figure(505); clf(hfig); hfig.Color = 'w';
ax1 = gca; set(ax1, 'NextPlot', 'add', 'Box', 'on');
colz = 'kr';
for i=1:nshots
    col_ = colz((GData(i) > G_threshold)+1);
    plot(1:2560, EData(:, i), 'Color', col_);
end
set(ax1, 'XLim', [400 2000]);
ylabel(ax1, 'Electron signal / a.u.');
xlabel(ax1, 'Pixel');
text(800, 5e5, 'Red = $\gamma$ signal above threshold');
make_latex(hfig); setAllFonts(hfig, 14);

%% Also plot the spectra according S laser E
cols = parula(nshots);
[laserE_sort, laserE_sort_inds] = sort(laserE);
hfig = figure(506); clf(hfig); hfig.Color = 'w';
ax1 = gca; set(ax1, 'NextPlot', 'add', 'Box', 'on');

for i=1:nshots
    ind_ = laserE_sort_inds(i);
    plot(1:2560, EData(:, ind_), 'Color', cols(ind_,:));
end
set(ax1, 'XLim', [400 1200]);
cb = colorbar;
minCtick = round(min(laserE), 2, 'significant');
maxCtick = round(max(laserE), 2, 'significant');
set(cb, 'YTickLabel', linspace(minCtick, maxCtick, 6), 'Ticks', linspace(0,1,6));
ylabel(ax1, 'Electron signal / a.u.');
xlabel(ax1, 'Pixel');
make_latex(hfig); setAllFonts(hfig, 14);

%%  Plot spectra as fn of laser E, but with waterfall...
hfig = figure(507); clf(hfig); hfig.Color = 'w';
ax1 = gca; set(ax1, 'NextPlot', 'add', 'Box', 'on');

hh = waterfall(1:2560, 1:49, EData');
set(ax1, 'XLim', [400 1200], 'YLim', [0 50], 'ZLim', [0 6e5]);
xlabel('Pixel');
ylabel('Shot number');
zlabel('Electron signal / a.u.');
make_latex(hfig); setAllFonts(hfig, 14);
view(-66, 37);

%%  Plot spectra as fn of laser E, but with waterfall...
[laserE_sort, laserE_sort_inds] = sort(laserE);
hfig = figure(508); clf(hfig); hfig.Color = 'w';
ax1 = gca; set(ax1, 'NextPlot', 'add', 'Box', 'on');

hh = waterfall(1:2560, laserE_sort, EData(:, laserE_sort_inds)');
set(ax1, 'XLim', [400 1200], 'YLim', [9 17], 'ZLim', [0 6e5]);
% And add arrows for the brightest shots: 12-15, 17-18, 24, 26
shotnos = [12:15 17 18 24 26]; ha = [];
for i=1:length(shotnos)
    ha(i) = arrow([420 laserE(shotnos(i)) 1.5e5], [420 laserE(shotnos(i)) 0e5]);
end
xlabel('Pixel');
ylabel('S laser energy / J');
zlabel('Electron signal / a.u.');
make_latex(hfig); setAllFonts(hfig, 14);

%% Plot the divergence as a fn of energy, red for shots with bright g-signal
cols = parula(nshots);
[GData_sort, GData_sort_inds] = sort(GData);
hfig = figure(509); clf(hfig); hfig.Color = 'w';
ax1 = gca; set(ax1, 'NextPlot', 'add', 'Box', 'on');
colz = 'kr';
for i=1:nshots
    col_ = colz((GData(i) > G_threshold)+1);
    plot(1:2560, divs(:, i), 'Color', col_);
end
set(ax1, 'XLim', [400 1200]);
ylabel(ax1, 'FWHM Divergence / a.u.');
xlabel(ax1, 'Pixel');
text(800, 5e5, 'Red = $\gamma$ signal above threshold');
make_latex(hfig); setAllFonts(hfig, 14);

%% 
% OK, let's now plot the eight shots, with beams, and plot the best
% reference (closest in S beam energy) next to it.
hfig = figure(510); clf(hfig); hfig.Color = 'w';
nHYshots = length(shotnos);
locs = GetFigureArray(1, nHYshots, [0.05 0.02 0.04 0.13], 0.03, 'down');
reflasE = laserE_S;
%reflasE([12:14 16:29]) = 0;
reflasE(GData>G_low_threshold) = 0;
reflasE(fSdat.Non) = 0;
twoD = 1;

ax = [];
divroi = 850:1000;
% Also print out the amount of G signal!
[~, Ginds] = sort(GData, 'descend');
for i=1:nHYshots
    shotno = shotnos(i);
    shotE = laserE_S(shotno);
    [~, refind] = min(abs(shotE - reflasE));
    refE = reflasE(refind);
    titl = sprintf('Shot no %i, E = %2.2f J, ref no %i, ref E = %2.2f J. G yield = %2.2e, \\# %i',...
        shotno, shotE, refind, refE, GData(shotno), find(Ginds==shotno, 1));
    disp(titl);
    
    % Now try to calculate the friction value for each of these, following
    % Gianluca's 90% method.
    ind_90_shot = find(EData(:,shotno)>0.1*max(EData(:,shotno)), 1, 'first');
    ind_90_ref = find(EData(:,refind)>0.1*max(EData(:,refind)), 1, 'first');
    
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add');
    if twoD
        imagesc([EDataraw(divroi,:,shotno); EDataraw(divroi,:,refind)]);
    %    plot(ind_90_shot*[1 1], [0 2*length(divroi)], '--g');
     %   plot(ind_90_ref*[1 1], [0 2*length(divroi)], '--r');
        text(400, -30, titl);
        text(395, 75, 'N beam ON', 'Color', 'g', 'HorizontalAlignment', 'right');
        text(395, 250, 'N beam OFF', 'Color', 'r', 'HorizontalAlignment', 'right');
        set(ax(i), 'YTick', []);
    else
        %text(400, , titl);
        hhs = plot(1:2560, EData(:, shotno), 1:2560, EData(:, refind));
        htitl = title(ax(i), titl);
        %set(htitl, 'Position', get(htitl, 'Position') - [0 2e1 0]);
        %get(htitl, 'Position').*[1 0.9999 1]
        legend(hhs, {'N beam ON', 'N beam OFF'});
    end
    
end
set(ax, 'XLim', [400 1200], 'YLim', [0 2*length(divroi)], 'XTick', [], 'YDir', 'reverse',...
    'Box', 'on', 'Color', 'none', 'Layer', 'top');
make_latex(hfig); setAllFonts(hfig, 11);
colormap(jet_white(256))
%export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'KP_BrightShot+refs'), '-pdf', '-nocrop', hfig);