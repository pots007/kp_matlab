% test2015Zepf_ExtraLanexRemoval.m
%
% Script to try and figure out how to remove the excess lanex piece from
% the data. Will try out removal from the 2D image, and also from only the
% spectra.
%
% UPDATE 20171218: Figured out some way. This script thus saves a new,
% fixed spectrum. Also does the same for the divergence. We alse write out
% new reference spectra for Matteo.
%% Shots that we need:
shotsWeNeed = [14 17 15 26 18 24];
shotsWeNeed = [40, [10,33], [9,31], [10,27,33,37,44], [3,7,33,34], shotsWeNeed];
shotsWeNeed = unique(shotsWeNeed);

datfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis', '20151109r003', 'mat');
savfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'MultiParticleSimulations', 'Kris_20171218');
flist = dir(fullfile(datfol, '*.mat'));
load('trackingDivergenceFit.mat');
specs = []; divs = []; divsdiff = [];
dtheta = 0.0441;
imms = zeros(193,1572,length(shotsWeNeed));
for k=1:length(shotsWeNeed)
    shot_ = shotsWeNeed(k);
    fdat = load(fullfile(datfol, flist(shot_).name));
    specs(end+1,:) = fdat.shotdata.dQ_dx;
    divs(end+1,:) = getSpectrumDivergence(fdat.shotdata.image, 0.05, 2);
    divsdiff(end+1,:) = diff(smooth(divs(end,:)));
    imms(:,:,k) = fdat.shotdata.image;
    calibFactor(k) = dtheta*2243.5/divfit(fdat.shotdata.alongscreen(k)); % CORRECT
end

%% First plot all relevant spectra, in separate panels
hfig = figure(243240); clf(hfig);
locs = GetFigureArray(2,7, [0.05 0.02 0.02 0.02], [0.02 0.04], 'down');
axx = [];
for k=1:length(shotsWeNeed)
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add');
    shot_ = shotsWeNeed(k);
    fdat = load(fullfile(datfol, flist(shot_).name));
    imagesc(medfilt2(fdat.shotdata.image), 'Parent', axx(k));
    ylabel(axx(k), num2str(shot_));
end
colormap(jet_white(256));
set(axx, 'XLim', [0 1400], 'YLim', [1 193], 'Box', 'on', 'Layer', 'top',...
    'YTick', []);
linkaxes(axx, 'xy');
make_tex(hfig);
%% Look at all the divergences, in separate panels...
hfig = figure(243241); clf(hfig);
locs = GetFigureArray(5,3, [0.05 0.02 0.02 0.02], [0.02 0.04], 'across');
axx = [];
for k=1:length(shotsWeNeed)
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add');
    plot(1:npnts, divs(k,:), '.k');
    plot(1:npnts, specs(k,:)/max(specs(k,:))*15+15, '-r');
    title(axx(k), num2str(shotsWeNeed(k)));
    % Plot a marker to show where the shit begin - about pixel 850
    mID = 850;
    plot(mID, divs(k,mID), 'rd', 'LineWidth', 3); 
    
    grid(axx(k), 'on');
end
set(axx, 'Box', 'on', 'XLim', [0 1400], 'YLim', [15 35]);
%export_fig(fullfile(savfol, 'Div_separate'), '-pdf', '-nocrop', hfig);
%% plot the divergences from Gemini2015, as a comparison. Run0809r008.
datfol2 = fullfile(getGDrive, 'Experiment', '2015_Gemini', 'EspecAnalysis',...
    '20150908r008', 'mat');
shotIDs = 28:36;
hfig = figure(243242); clf(hfig);
locs = GetFigureArray(5,3, [0.05 0.02 0.02 0.02], [0.02 0.04], 'across');
locs = GetFigureArray(5,3, [0.05 0.02 0.02 0.02], [0.02 0.04], 'across');
axx = [];
for k=1:length(shotIDs)
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add');
    fdat = load(fullfile(datfol2, sprintf('20150908r008s%03i_Espec1.mat', shotIDs(k))));
    plot(getSpectrumDivergence(fdat.shotdata.image, 0.05, 2), '.k');
    specc = sum(fdat.shotdata.image,1);
    plot(specc./max(specc)*20+5, '-r');
    %title(axx(k), num2str(shotsWeNeed(k)));
    % Plot a marker to show where the shit begin - about pixel 850
    %mID = 850;
    %plot(mID, divs(k,mID), 'rd', 'LineWidth', 3); 
    grid(axx(k), 'on');
end
set(axx, 'Box', 'on', 'XLim', [0 1950], 'YLim', [5 35]);
export_fig(fullfile(savfol, 'Div_HighEnRun'), '-pdf', '-nocrop', hfig);
%% ORRRR the tomography x-ray run....
datfol3 = fullfile(getGDrive, 'Experiment', '2015_Gemini', 'EspecAnalysis',...
    '20150916r005', 'mat');
shotIDs = randi(533, [1 15])
hfig = figure(243243); clf(hfig);
locs = GetFigureArray(5,3, [0.05 0.02 0.02 0.02], [0.02 0.04], 'across');
locs = GetFigureArray(5,3, [0.05 0.02 0.02 0.02], [0.02 0.04], 'across');
axx = [];
for k=1:length(shotIDs)
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add');
    fdat = load(fullfile(datfol3, sprintf('20150916r005s%03i_Espec1.mat', shotIDs(k))));
    plot(getSpectrumDivergence(fdat.shotdata.image, 0.05, 2), '.k');
    specc = sum(fdat.shotdata.image,1);
    plot(specc./max(specc)*20+5, '-r');
    title(axx(k), sprintf('20150916r005, s%03i', shotIDs(k)));
    %plot(mID, divs(k,mID), 'rd', 'LineWidth', 3); 
    grid(axx(k), 'on');
end
set(axx, 'Box', 'on', 'XLim', [0 1950], 'YLim', [5 35]);
export_fig(fullfile(savfol, 'Div_TomoRun'), '-pdf', '-nocrop', hfig);

% Also try looking at where the central part of the divergence is, at the
% point of it going weird - perhaps some spatial shite in the lanex?
%% This plots an espec with spectrum and divergence overlaid

testID = 33;

fdat = load(fullfile(datfol, flist(testID).name));

hfig = figure(591); clf(hfig);

set(gca, 'NextPlot', 'add')
imm = fdat.shotdata.image;
%imm(:,615:1085) = imm(:,615:1085)/1.2;
imagesc(medfilt2(imm));
spec = fdat.shotdata.dQ_dx;
%spec(618:1080) = spec(618:1080)/1.1;
plot(spec*500, '--r');
plot(sum(imm,1)*140/max(sum(imm,1)), 'k');
colormap(jet_white(256));
% Plot the div on the same image
maximm = max(imm(:));
imDiv = getSpectrumDivergence(imm, 0.05, 2);
%plot(1:length(spec), divims, '-k');
plot(1:length(spec), 5*imDiv, '-m', 'LineWidth', 2)


%% Look at removing high enegry kink...
% fdat = load(fullfile(datfol, flist(33).name));
% imm = medfilt2(fdat.shotdata.image);
% newimm = imm;
% NN = size(imm,2);
% xax = 1:NN;
% hfig = figure(2124); clf(hfig)
% for k=50:150
%     linee = smooth(imm(k,:));
%     sublinee = linee(900:1250);
%     subxax = xax(900:1250);
%     logm = true(size(xax));
%     logm(1070:1130) = false;
%     newlinee = interp1(xax(logm), linee(logm), xax, 'pchip');
%     newimm(k,:) =newlinee;
%     %plot(xax, linee, xax, newlinee);
%     %set(gca, 'XLim', [900 1250]);
%     %drawnow; pause(0.2);
% end
% The above seems to work - so let's try it out in a loop for all
% spectra...
hfig = figure(243245); clf(hfig);
locs = GetFigureArray(6,3, [0.05 0.02 0.02 0.02], [0.02 0.04], 'across');
axx = [];

newimm = zeros(size(imms));
% Make a structure with the fixed spectra. Later we add fixed divergence
% data to the same structure.
fixedSpec = struct;
for k=1:length(shotsWeNeed)
    % Fix the high energy part first...
    for l=50:150
        newimm(l,:,k) = fixSpectrum(imms(l,:,k), 1);
    end
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add');
    plot(sum(imms(:,:,k),1), '-k');
    newspec = sum(newimm(:,:,k),1);
    plot(newspec, '-r');
    newSpec2 = fixSpectrum(fixSpectrum(newspec,3),2);
    plot(newSpec2, '--m');
    %plot(sum(newimm(:,:,k),1), '-.c');
    title(axx(k), num2str(shotsWeNeed(k)));
    grid(axx(k), 'on');
    drawnow;
    fixedSpec(k).shot = shotsWeNeed(k);
    fixedSpec(k).spec = newSpec2*1e-6;
    fixedSpec(k).alongscreen = fdat.shotdata.alongscreen;
end
set(axx, 'Box', 'on', 'XLim', [900 1200], 'YTickLabel', [], 'XTickLabel', []);
linkaxes(axx, 'x');
make_tex(hfig)
%export_fig(fullfile(savfol, 'FixedSpectra'), '-pdf', '-nocrop', hfig);
%save(fullfile(savfol, 'fixedSpectra'), 'fixedSpec');

%% Look at the divergence. Again.
% We have saved the divergences in divs. Plot the line and then fit
% something to the first 600 pixels.

fSpec = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'MultiParticleSimulations',...
    'Kris_20171218', 'fixedSpectra'));
enax = tracking.valfit(fSpec.fixedSpec(1).alongscreen);
hfig = figure(243249); clf(hfig);
locs = GetFigureArray(5,3, [0.05 0.02 0.02 0.02], [0.02 0.04], 'across');
axx = [];

oldFit = @(x) 30.44*exp(-1e-2*x)+0.7;
expFit = @(a,b,c,x) a + b*exp(c.*x);
powFit = @(a,b,c,x) a + b*x.^c;
xax = 1:size(divs,2);
plotenax = 300:2500;
shotsWeHave = cell2mat({fSpec.fixedSpec.shot});
dtheta = 0.0441;
for k=1:length(shotsWeNeed)
    axx(k) = makeNiceAxes(hfig, locs(:,k));
    ind = find(shotsWeHave==shotsWeNeed(k),1);
    divreal = divs(k,:)'.*dtheta*2243.5./divfit(fSpec.fixedSpec(k).alongscreen);
    plot(axx(k), enax, divreal);
    % Now plot a power fit to the first 600 points.
    lm = enax<650 & ~isnan(divreal);
    fit1 = fit([enax(lm); (2000:2010)'], [divreal(lm); 0.7*ones(11,1)],...
        expFit, 'StartPoint', [0.7, 30, -1e-2]);
    %fit2 = fit(enax(lm), divreal(lm), powFit, 'StartPoint', [0.5, 1, -0.25]);
    plot(plotenax, oldFit(plotenax), '-m');
    plot(plotenax, fit1(plotenax), '-r');
    %plot(plotenax, fit2(plotenax), '-c');
    title(shotsWeNeed(k));
    drawnow;
    
end
linkaxes(axx, 'x');
set(axx, 'XLim', [300 2400], 'YLim', [0.5 2]);