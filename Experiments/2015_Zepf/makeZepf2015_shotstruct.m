% makeZepf2015_shotstruct.m
%
% Makes a structure with some important features: GSN, S energy, N energy,
% whether N was on, pulselengths etc.

% THis doesn't work for some reason?
%raws = csvimport(fullfile(getGDrive, 'Experiment', '2015_Zepf',...
%    'SHOT_141274_141325_USER_SELECTION',...
%    'DATA_GS00141297_GS00141280_USER_SELECTION.csv'),...
%    'noHeader', false, 'ignoreHSpace', true, 'uniformOutput', false);
% So using filthy xlsread instead...
[~,~,raws] = xlsread(fullfile(getGDrive, 'Experiment', '2015_Zepf',...
    'SHOT_141274_141325_USER_SELECTION',...
    'DATA_GS00141297_GS00141280_USER_SELECTION.xlsx'));

clear Zepf2015
Zepf2015(49) = struct;

% Shot 1 of run 3 is GSN 141275.
GSN_shot_offset = 141274;

Non = false(1,49); Non([12:15 17:29 35:39 45:49]) = 1;
Noff = ~Non;
Noff(1) = false; % This avoids us later plotting the first, duff shot.

NS_optimum = [12:15 17 18 24 26];

ttemp =  cellfun(@(x) strcmp(x, 'S_uncomp_nf_e'), raws(1,:));
S_uncomp_ind = find(ttemp);
ttemp =  cellfun(@(x) strcmp(x, 'N_uncomp_nf_e'), raws(1,:));
N_uncomp_ind = find(ttemp);
ttemp =  cellfun(@(x) strcmp(x, 'Id'), raws(1,:));
GSN_ind = find(ttemp);

raws_data = raws(2:end, :);
GSNs = cell2mat(raws_data(:, GSN_ind));

for i=1:49
    Zepf2015(i).Non = Non(i);
    Zepf2015(i).Noff = Noff(i);
    Zepf2015(i).NS_optimum = ~isempty(find(i==NS_optimum, 1));
    shotind = find(GSNs == (i+GSN_shot_offset));
    Zepf2015(i).laserE_S = raws_data{shotind, S_uncomp_ind};
    Zepf2015(i).laserE_N = raws_data{shotind, N_uncomp_ind};
    Zepf2015(i).GSN = i + GSN_shot_offset;
end

save(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'shotdata'), ...
    'Non', 'Noff', 'NS_optimum', 'Zepf2015');

%% And make a laser energy plot for future use:
hfig = figure(599); clf(hfig); hfig.Color = 'w';
set(gca, 'Nextplot', 'add', 'box', 'on');
laserE_S = cell2mat({Zepf2015.laserE_S});
E_S_ = [mean(laserE_S) std(laserE_S)];
laserE_N = cell2mat({Zepf2015.laserE_N});
Nlogm = laserE_N>5;
E_N_ = [mean(laserE_N(Nlogm)) std(laserE_N(Nlogm))];
hh = plot(1:49, laserE_S, 'x', 1:49, laserE_N, 'd');
hl(1) = plot([0 50], [E_S_(1)-E_S_(2) E_S_(1)-E_S_(2)], '--', 'Color', hh(1).Color);
hl(2) = plot([0 50], [E_S_(1)+E_S_(2) E_S_(1)+E_S_(2)], '--', 'Color', hh(1).Color);
hl(3) = plot([0 50], [E_N_(1)-E_N_(2) E_N_(1)-E_N_(2)], '--', 'Color', hh(2).Color);
hl(4) = plot([0 50], [E_N_(1)+E_N_(2) E_N_(1)+E_N_(2)], '--', 'Color', hh(2).Color);
hl(5) = plot([0 50], [E_N_(1) E_N_(1)], '-', 'Color', hh(2).Color);
hl(6) = plot([0 50], [E_S_(1) E_S_(1)], '-', 'Color', hh(1).Color);
grid on; grid minor;
set([hl hh'], 'LineWidth', 1.5);
set(gca, 'YLim', [8 18], 'XLim', [0 50], 'Layer', 'top', 'LineWidth', 1.5);
legend(hh, {'South beam', 'North beam'}, 'Location', 'North');
ylabel('Uncompressed energy / J');
xlabel('Shot number');
make_latex(hfig); setAllFonts(hfig, 14);
export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'laser_energies'), '-pdf', '-nocrop', hfig);