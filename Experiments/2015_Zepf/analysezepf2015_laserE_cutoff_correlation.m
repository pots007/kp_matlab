% analysezepf2015_laserE_cutoff_correlation.m
%
% Script to look through more espec data, and to plot the results with
% actual errors.

%% FROM plot2015Zepf_EspecOnly.m - set up life
trackfile = 'Tracking_20170309_KP_4mrad.mat';
trackfile = 'Tracking_20170802_KP_4mrad_05mrad_div.mat';

load('trackingDivergenceFit.mat');

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

% Load stuff
load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'BGfree_data.mat'));
fSdat = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'andata', 'shotdata'));
datfol = fullfile(getGDrive, 'Experiment', '2015_Zepf', 'EspecAnalysis',...
    '20151109r003', 'mat');
flist = dir(fullfile(datfol, '*.mat'));
nshots = length(flist); %49;

compF = 1;% 0.67*0.9;
laserE_S = cell2mat({fSdat.Zepf2015.laserE_S})*compF;
laserE_N = cell2mat({fSdat.Zepf2015.laserE_N});

GData = squeeze(sum(sum(GDataraw,1), 2) );

saveplots = 0;

%% FROM plot2015Zepf_EspecOnly.m -  Now find cutoff energies.
% Which method to use? let's start with Gianluca's 90% of peak charge
% density. I also assume he used the dQ/dE curve for this. So in line with
% before, let's just use the 2% level of maximum values.

% Plot all the retrieved energies out as well so I can check.
% This is done for all shots!

maxens = zeros(nshots, 3);
beamMoments = zeros(nshots, 4);
beamEnInt = zeros(nshots,1);
highEnQ = zeros(nshots,4);
highEnFrac = [1 0.95 0.9 0.8];

%colmap = [1 1 1; 0.8 0.8 0.8; 0.6471    0.8745    0.6510];

clear enax;
for i=1:nshots
    fdat = load(fullfile(datfol, flist(i).name));
    enax{1} = tracking.lowfit(fdat.shotdata.alongscreen)';
    enax{2} = tracking.valfit(fdat.shotdata.alongscreen)';
    enax{3} = tracking.highfit(fdat.shotdata.alongscreen)';
    dE = gradient(enax{2});
    spec = smooth(sum(fdat.shotdata.image, 1)./dE);
    % Find the beam moments. First, the spectrum weighted mean energy
    beamMoments(i,1) = sum(spec'.*enax{2}   )/sum(spec);
    beamMoments(i,2) = sum(spec'.*enax{2}.^2)/sum(spec);
    beamMoments(i,3) = moment(spec'.*enax{2},2)/sum(spec);
    beamMoments(i,4) = moment(spec,2)/sum(spec);
    %gammaax = enax./0.511;
    %beamEnInt(i) = sum(gammaax.^2.*(spec.*dE));
    nfac = sum(spec); % Normalise to total charge
    spec = spec./nfac;
    spec = spec./max(spec);
    %specs(i, :) = spec;
    % Find the index of the point closest to 10% level
    [~, minind] = min(abs(spec - 0.1));
    
    % OR try to find point which containts 99% of beam charge?
    beamCumSum = cumsum(spec);
    beamCumSum = beamCumSum./max(beamCumSum);
    %minind = find(beamCumSum>0.99, 1, 'first');
    %[ spec(minind) enax(minind)];
    if ~isempty(minind)
        for kk=1:3; maxens(i,kk) = enax{kk}(minind); end
    end
    % And fill in the numbers for the top edge charge fraction
    for k=1:4
        [~,mind2] = min(abs(enax{2} - highEnFrac(k)*maxens(i,2)));
        highEnQ(i,k) = sum(spec(mind2:end))./sum(spec);
    end
    fprintf('Shot: %2i, Cutoff: %4.0f, 0.8 lev: %4.0f\n', i, maxens(i,2), enax{2}(mind2));
    % Fix shot 13....
    if i==13
        maxens(i,2) = maxens(i,1);
    end
end
figure(78);
errorbar(1:nshots, maxens(:,2), maxens(:,3)-maxens(:,2), maxens(:,2)-maxens(:,1), 'o')

%% And plot ALL cutoff energies against laser energy:
hfig = figure(545); clf(hfig); hfig.Color = 'w'; hfig.Position = [100 200 600 400];
set(gca, 'NextPlot', 'add');
clear hh;
% 0 for simply all shots, 1 for with one fit, 2 for two fits
allflag = 11;

hoffset = 00;
if allflag==0
    hh = plot(laserE_S(2:end), maxens(2:end,2), 'o');
else
    hh(1) = plot(laserE_S(fSdat.Noff), maxens(fSdat.Noff,2), 's');
    hh(2) = plot(laserE_S(fSdat.Non), maxens(fSdat.Non,2), 'd');
    hh(3) = plot(laserE_S(fSdat.NS_optimum), maxens(fSdat.NS_optimum,2), 'd');
    set(hh(3), 'Color', hh(2).Color, 'MarkerFaceColor', hh(2).Color);
    legend(hh, {'North beam off', 'N beam on', 'Very high Thomson signal'},...
        'Location', 'Northwest', 'AutoUpdate', 'off')
    if allflag>2
        hScat = scatter(laserE_S(fSdat.Non), maxens(fSdat.Non,2), 100,...
            log10(GData(fSdat.Non)), 'MarkerFaceColor', 'flat');
        set(hh([2 3]), 'Visible', 'off');
        cb = colorbar;
        ylabel(cb, 'log of gamma yield');
    end
end
[linfit, linfitErr] = fit(laserE_S(2:end)', maxens(2:end,2), 'poly1');
plotAx = 1:0.1:20;
hhh(1) = plot(plotAx, linfit(plotAx), '--k');
text(16, 1500+hoffset, sprintf('$R^2 = %2.2f$', linfitErr.rsquare), 'Color', 'k');
if allflag >0 % Do separate fits
    en_error = [maxens(:,2)-maxens(:,1), maxens(:,3)-maxens(:,2)];
    [linfit_on, linfit_onErr] = fit(laserE_S(fSdat.Non)', maxens(fSdat.Non,2), 'poly1');
    errs = mean(en_error(fSdat.Noff,:),2);
    %errs(:) = 1;
    [linfit_off, linfit_offErr] = fit(laserE_S(fSdat.Noff)', maxens(fSdat.Noff,2), 'poly1',...
        'Weights', 1./(errs/mean(errs)));
    hhh(2) = plot(1:20, linfit_on(1:20), '--', 'Color', hh(2).Color);
    hhh(3) = plot(1:20, linfit_off(1:20), '--', 'Color', hh(1).Color);
    text(16, 1400+hoffset, sprintf('$R^2 = %2.2f$', linfit_onErr.rsquare), 'Color', hh(2).Color);
    text(16, 1300+hoffset, sprintf('$R^2 = %2.2f$', linfit_offErr.rsquare), 'Color', hh(1).Color);
    % And plot the prediction bands...
    pred_on = predint(linfit_off, plotAx, 0.68);
    pred_on2 = predint(linfit_off, plotAx, 0.95);
    haa = area(plotAx, [zeros(length(plotAx),1) pred_on2(:,1) pred_on2(:,2)-pred_on2(:,1)]);
    set(haa(1:2), 'FaceColor', 'none'); set(haa, 'EdgeColor', 'none');
    set(haa(3), 'FaceColor', hh(1).Color, 'FaceAlpha', 0.2);
    haa = area(plotAx, [zeros(length(plotAx),1) pred_on(:,1) pred_on(:,2)-pred_on(:,1)]);
    set(haa(1:2), 'FaceColor', 'none'); set(haa, 'EdgeColor', 'none');
    set(haa(3), 'FaceColor', hh(1).Color, 'Facealpha', 0.4);
    uistack([hh hhh], 'top');
end
if allflag>2; uistack(hScat, 'top'); end

set(hh, 'MarkerSize', 10, 'LineWidth', 2);
set(hhh, 'LineWidth', 1.5);
set(gca, 'Box', 'on', 'LineWidth', 2, 'XLim', [10 18], 'YLim', [1100 1900], 'Layer', 'top');

ylabel('Cutoff energy / MeV');
xlabel('South beam uncompressed energy / J');
make_latex(hfig); setAllFonts(hfig, 14);

% Label the hot-hot shots
for k=1:length(fSdat.NS_optimum)
    indd = fSdat.NS_optimum(k);
    ht(k) = text(laserE_S(indd), maxens(indd,2), num2str(indd));
end
Noff_shots = 1:49; Noff_shots = Noff_shots(fSdat.Noff);
for k=1:length(Noff_shots)
    indd = Noff_shots(k);
    ht(k) = text(laserE_S(indd), maxens(indd,2), num2str(indd));
end

saveplots = 0;
if saveplots
    if allflag==0; fext = 'all'; else; fext = 'coloured'; end
    %export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', ['espec_vs_laserE2_' fext]), '-pdf', '-nocrop', hfig);
    % Trying highres png
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', ['espec_vs_laserE2_' fext]),...
        '-png', '-nocrop', '-m3', hfig);
end

% Make a structure for GS
plotData.laserE_S = laserE_S;
plotData.laserE_N = laserE_N;
plotData.cutoffs = maxens(:,2);
plotData.GammaData = GData;
plotData.predBandXaxis = plotAx;
plotData.predBand68 = pred_on;
plotData.predBand95 = pred_on2;
plotData.Noff = fSdat.Noff;
plotData.Non = fSdat.Non;
plotData.ColdEnergyFit = linfit_off;
%% Do what Zepf suggested - look at the residuals
% Use the N beam off fit:
laserE_off = laserE_S(fSdat.Noff);
ens_off = maxens(fSdat.Noff, 2);
fit_ens = linfit_off(laserE_off);
residuals = ens_off - fit_ens;
fprintf('Standard deviation of Measured-Fitted is %2.1f MeV\n', std(residuals));

% But also look at whether either axis of the thomson stuff gives us
% information about the pointing - or espec energy....
fitData = load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'GammaFits', 'fitData'));
[optShots, inds] = intersect(fitData.fitNData_2G(:,1), fSdat.NS_optimum);
gammaPointing_x = fitData.fitNData_2G(inds, 3);
gammaPointing_y = fitData.fitNData_2G(inds, 5);
espec_ens = maxens(fSdat.NS_optimum, 2);
plot(espec_ens, gammaPointing_y, 'xr');%, espec_ens, gammaPointing_y, 'ok');

%% But also, look at the pointing stability of the high end of the spectrum

centInds = zeros(nshots, 1);
% Column 1 is high energy, col 2 is low energy
beamCent = zeros(nshots, 2);
dtheta = mean(diff(fdat.shotdata.divax));

for k=1:nshots
    fdat = load(fullfile(datfol, flist(k).name));
    imageDiv = mean(fdat.shotdata.image(:,1050:1100),2); % This about 1.4-1.6GeV
    [~,centInds(k)] = max(imageDiv);
    % This is proper divergence calculation, using the fitted distance from
    % TCC to the screens.
    beamCent(k,1) = centInds(k)*100/mean(divfit(fdat.shotdata.alongscreen(1050:1100)));
    imageDiv = mean(fdat.shotdata.image(:,1:50),2); % This is the low energy end..
    [~,centInds(k)] = max(imageDiv);
    beamCent(k,2) = centInds(k)*100/mean(divfit(fdat.shotdata.alongscreen(1:50)));
end

hfig = figure(555); clf(hfig);
hfig.Color = 'w'; hfig.Position = [100 200 600 400];
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2, 'Layer', 'top');

%plot(2:nshots, centInds(2:end)*dtheta, 'x')
area([0 nshots+1], std(beamCent(2:end,1))*[-1 2; -1 2;], 'EdgeColor', 'none', ...
    'FaceColor', [0.3 0.75 0.93]);
hDiv = plot(2:nshots, beamCent(2:end,1)-mean(beamCent(2:end,1)), 'rd');
plot(2:nshots, beamCent(2:end,2) - mean(beamCent(2:end,2)), 'ks');
text(35, 0.43, sprintf('$$\\sigma = $$ %1.2f mrad', std(beamCent(2:end,1))));

set(hDiv, 'LineWidth', 2);
xlabel('Shot number');
ylabel('Pointing offset from mean / mrad');
make_latex(hfig); setAllFonts(hfig, 14);
%export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'especPointing'),...
%        '-png', '-nocrop', '-m3', hfig);

%% Look at T at > 80%E_c

hfig = figure(5451); clf(hfig); hfig.Color = 'w';
hfig.Position = [100 200 1200 800];
locs = GetFigureArray(2,2,[0.03 0.02 0.1 0.1], [0.09 0.03], 'across');
%set(gca, 'NextPlot', 'add');
clear hh hh;
% 0 for simply all shots, 1 for with one fit, 2 for two fits
lims = [0.2, 1.2;
    0.7, 1.4;
    1, 2;
    1, 6]*1e-2;

for i=1:4
    axx(i) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add');
    allflag = 0;
    hh(1) = plot(laserE_S(fSdat.Noff), highEnQ(fSdat.Noff,i), 's');
    hh(2) = plot(laserE_S(fSdat.Non), highEnQ(fSdat.Non,i), 'd');
    hh(3) = plot(laserE_S(fSdat.NS_optimum), highEnQ(fSdat.NS_optimum,i), 'd');
    set(hh(3), 'Color', hh(2).Color, 'MarkerFaceColor', hh(2).Color);
    legend(hh, {'North beam off', 'N beam on', 'Very high Thomson signal'},...
        'Location', 'Northeast', 'AutoUpdate', 'off')
    if allflag==1
        hScat = scatter(laserE_S(fSdat.Non), highEnQ(fSdat.Non,i), 100,...
            log10(GData(fSdat.Non)), 'MarkerFaceColor', 'flat');
        set(hh([2 3]), 'Visible', 'off');
        cb = colorbar;
        ylabel(cb, 'log of gamma yield');
    end
    % Fitting
    plotAx = 1:0.1:20;
    %text(16, 1500, sprintf('$R^2 = %2.2f$', linfitErr.rsquare), 'Color', 'k');
    % Do separate fits
    [linfit_on, linfit_onErr] = fit(laserE_S(fSdat.Non)', highEnQ(fSdat.Non,i), 'poly1');
    [linfit_off, linfit_offErr] = fit(laserE_S(fSdat.Noff)', highEnQ(fSdat.Noff,i), 'poly1');
    hhh(1) = plot(1:20, linfit_on(1:20), '--', 'Color', hh(2).Color);
    hhh(2) = plot(1:20, linfit_off(1:20), '--', 'Color', hh(1).Color);
    %text(16, 1400+hoffset, sprintf('$R^2 = %2.2f$', linfit_onErr.rsquare), 'Color', hh(2).Color);
    %text(16, 1300+hoffset, sprintf('$R^2 = %2.2f$', linfit_offErr.rsquare), 'Color', hh(1).Color);
    % And plot the prediction bands...
    pred_on = predint(linfit_off, plotAx, 0.68);
    pred_on2 = predint(linfit_off, plotAx, 0.95);
    haa = area(plotAx, [zeros(length(plotAx),1) pred_on2(:,1) pred_on2(:,2)-pred_on2(:,1)]);
    set(haa(1:2), 'FaceColor', 'none'); set(haa, 'EdgeColor', 'none');
    set(haa(3), 'FaceColor', hh(1).Color, 'FaceAlpha', 0.2);
    haa = area(plotAx, [zeros(length(plotAx),1) pred_on(:,1) pred_on(:,2)-pred_on(:,1)]);
    set(haa(1:2), 'FaceColor', 'none'); set(haa, 'EdgeColor', 'none');
    set(haa(3), 'FaceColor', hh(1).Color, 'Facealpha', 0.4);
    uistack([hh hhh], 'top');
    
    if allflag>0; uistack(hScat, 'top'); end
    
    set(hh, 'MarkerSize', 10, 'LineWidth', 2);
    set(hhh, 'LineWidth', 1.5);
    set(gca, 'Box', 'on', 'LineWidth', 2, 'XLim', [10 18], 'YLim', lims(i,:), 'Layer', 'top');
    
    ylabel(sprintf('Fraction of charge above $%.2fE_c$', highEnFrac(i)));
    xlabel('South beam uncompressed energy / J');
    
    %make_latex(hfig); setAllFonts(hfig, 14);
    
    % Label the hot-hot shots
    for k=1:length(fSdat.NS_optimum)
        indd = fSdat.NS_optimum(k);
        ht(k) = text(laserE_S(indd), highEnQ(indd,i), num2str(indd));
    end
    Noff_shots = 1:49; Noff_shots = Noff_shots(fSdat.Noff);
    for k=1:length(Noff_shots)
        indd = Noff_shots(k);
        % ht(k) = text(laserE_S(indd), highEnQ(indd,i), num2str(indd));
    end
    
end
make_latex(hfig); setAllFonts(hfig, 14);

saveplots = 0;
if saveplots
    if allflag==0; fext = 'all'; else; fext = 'coloured'; end
    %export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', ['espec_vs_laserE2_' fext]), '-pdf', '-nocrop', hfig);
    % Trying highres png
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', ['espec_vs_laserE2_' fext]),...
        '-png', '-nocrop', '-m3', hfig);
end

%% Try to plot the beam moments against
hfig = figure(547); clf(hfig); hfig.Color = 'w'; hfig.Position = [100 200 600 400];
set(gca, 'NextPlot', 'add');
clear hh;
% 0 for simply all shots, 1 for with one fit, 2 for two fits
allflag = 11;

% Choose what we will plot.....
plotQ = beamMoments(:,1);
plotQ(1) = nan;

hh(1) = plot(laserE_S(fSdat.Noff), plotQ(fSdat.Noff), 's');
hh(2) = plot(laserE_S(fSdat.Non), plotQ(fSdat.Non), 'd');
hh(3) = plot(laserE_S(fSdat.NS_optimum), plotQ(fSdat.NS_optimum), 'd');
set(hh(3), 'Color', hh(2).Color, 'MarkerFaceColor', hh(2).Color);
legend(hh, {'North beam off', 'N beam on', 'Very high Thomson signal'},...
    'Location', 'Northwest', 'AutoUpdate', 'off')
%
hScat = scatter(laserE_S(fSdat.Non), plotQ(fSdat.Non), 100,...
    log10(GData(fSdat.Non)), 'MarkerFaceColor', 'flat');
set(hh([2 3]), 'Visible', 'off');
cb = colorbar;
ylabel(cb, 'log of gamma yield');

plotAx = 1:0.1:20;
%text(16, 1500+hoffset, sprintf('$R^2 = %2.2f$', linfitErr.rsquare), 'Color', 'k');

[linfit_on, linfit_onErr] = fit(laserE_S(fSdat.Non)', plotQ(fSdat.Non), 'poly1');
[linfit_off, linfit_offErr] = fit(laserE_S(fSdat.Noff)', plotQ(fSdat.Noff), 'poly1');
hhh(1) = plot(1:20, linfit_on(1:20), '--', 'Color', hh(2).Color);
hhh(2) = plot(1:20, linfit_off(1:20), '--', 'Color', hh(1).Color);
%text(16, 1400+hoffset, sprintf('$R^2 = %2.2f$', linfit_onErr.rsquare), 'Color', hh(2).Color);
%text(16, 1300+hoffset, sprintf('$R^2 = %2.2f$', linfit_offErr.rsquare), 'Color', hh(1).Color);
% And plot the prediction bands...
pred_on = predint(linfit_off, plotAx, 0.68);
pred_on2 = predint(linfit_off, plotAx, 0.95);
haa = area(plotAx, [zeros(length(plotAx),1) pred_on2(:,1) pred_on2(:,2)-pred_on2(:,1)]);
set(haa(1:2), 'FaceColor', 'none'); set(haa, 'EdgeColor', 'none');
set(haa(3), 'FaceColor', hh(1).Color, 'FaceAlpha', 0.2);
haa = area(plotAx, [zeros(length(plotAx),1) pred_on(:,1) pred_on(:,2)-pred_on(:,1)]);
set(haa(1:2), 'FaceColor', 'none'); set(haa, 'EdgeColor', 'none');
set(haa(3), 'FaceColor', hh(1).Color, 'Facealpha', 0.4);
uistack([hh hhh], 'top');
if allflag>2; uistack(hScat, 'top'); end

set(hh, 'MarkerSize', 10, 'LineWidth', 2);
set(hhh, 'LineWidth', 1.5);
set(gca, 'Box', 'on', 'LineWidth', 2, 'XLim', [10 18], 'Layer', 'top');

ylabel('Cutoff energy / MeV');
xlabel('South beam uncompressed energy / J');
make_latex(hfig); setAllFonts(hfig, 14);

% Label the hot-hot shots
for k=1:length(fSdat.NS_optimum)
    indd = fSdat.NS_optimum(k);
    ht(k) = text(laserE_S(indd), plotQ(indd), num2str(indd));
end

saveplots = 0;
if saveplots
    if allflag==0; fext = 'all'; else; fext = 'coloured'; end
    %export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', ['espec_vs_laserE2_' fext]), '-pdf', '-nocrop', hfig);
    % Trying highres png
    export_fig(fullfile(getGDrive, 'Experiment', '2015_Zepf', ['espec_vs_laserE2_' fext]),...
        '-png', '-nocrop', '-m3', hfig);
end
