% getZepf2015ScreenEnergy.m
%
% This function returns the energy for a given screen and a given pointing
% angle. 
%
% getGem2015ScreenEnergy(distToEdge, scrID, divID, pointing)
%   distToEdge - distance (in mm) from high energy edge of screen
%   divID      - beam divergence hits, only -1, 0 and 1 mrad (default 0)
%   pointing   - beam pointing offset, in range from -3 to 11 mrad (default 0)

function en = getZepf2015ScreenEnergy(distToEdge, divID, pointing)

% To speed things up!
persistent splineLookUpMatrix;
if isempty(splineLookUpMatrix)
    fdat = load('Zepf2015lookupSpline');
    splineLookUpMatrix = fdat.lookupSpline;
end

% Handle lazy users
if nargin==1
    divID = 2;
    pointing = 4;
elseif nargin==2
    pointing = 4;
end
% In case we want to do a vector
if isvector(distToEdge)
    pointing = repmat(pointing, size(distToEdge));
end

en = splineLookUpMatrix{divID}(pointing, distToEdge);