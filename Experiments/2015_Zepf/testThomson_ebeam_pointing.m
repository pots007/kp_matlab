% testThomson_ebeam_pointing.m
%
% Quick check to see whether we can correlate e-beam pointing (ie energy)
% and Thomson direction. The latter is taken from fits, assuming some
% distance to the screen.
%
% KP, DESY, 2018

% Load some data...
load(fullfile(getGDrive, 'Experiment', '2015_Zepf', 'GammaFits', 'fitData'));

% Let's define some axes
[X,Y] = meshgrid(-10:90, -10:90);
% And calculate the total signal integral for both fits
totsums = zeros(size(fitData,1),2);
for k=1:size(totsums,1)
    argss = num2cell(fitNData_1G(k,2:end-1));
    locsum = fitFcn1(argss{:}, X, Y);
    totsums(k,1) = sum(locsum(:));
    argss = num2cell(fitNData_2G(k,2:end-1));
    locsum = fitFcn2(argss{:}, X, Y);
    totsums(k,2) = sum(locsum(:));
end

% Now let's plot the positions of x0 - this is the position of the gamma
% beam on the detector.
hfig = figure(23513); clf(hfig);
axx = makeNiceAxes(hfig);

scatter(fitNData_2G(:,1), fitNData_2G(:,3), 40, log10(totsums(:,2)), 'filled')
%scatter(fitNData_2G(:,1), fitNData_2G(:,8), 40, log10(totsums(:,2)), 'filled')
cb = colorbar;
xlabel(axx, 'Shot number')
ylabel(axx, 'Position of beam center (mm)');
ylabel(cb, 'log_{10} Total signal');