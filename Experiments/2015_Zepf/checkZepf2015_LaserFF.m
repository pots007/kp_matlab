% checkZepf2015_LaserFF.m
%
% Check over Zepf run FF from upstairs...

datfol = '/home/kpoder/GDrive/Experiment/2015_Zepf/SHOT_141274_141325_USER_SELECTION/';
ffs = dir(fullfile(datfol, 'S_comp_ff*.png'));

% Plot all in one image

hfig = figure(124); clf(hfig); hfig.Position = [700 100, 1200 800];
locs = GetFigureArray(9,6, [0.04 0.04 0.04 0.04], 0.005, 'across');

axx = [];
for i=2:50
    imm = imread(fullfile(ffs(i).folder, ffs(i).name));
    imm(imm<0.1*max(imm(:))) = 0;
    axx(end+1) = makeNiceAxes(hfig, locs(:,i-1));
    imagesc(imm, 'Parent', axx(end));
    axis(axx(end), 'image');
    text(150, 40, num2str(i-1), 'Parent', axx(end));
    drawnow;
end
set(axx, 'XTick', [], 'YTick', [], 'Layer', 'top');
colormap(jet_white(256));
