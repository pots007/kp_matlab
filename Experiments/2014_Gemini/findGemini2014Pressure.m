function [pres, ne] = findGemini2014Pressure(filename, mix)
Constants;
data = importdata(filename);
smdata = smooth(data(:,2));
deriv = diff(smdata);
[~,ind] = min(deriv);
if (ind < 6000) || (ind > 6015)
    ind = 6006;
end
t0 = data(ind,1);
plot(data(:,1), smdata, t0, smdata(ind), 'or'); set(gca, 'XLim', [0.05 0.15]);
pres = - 50 + 12.5*smdata(ind);
ne = 2*101.325*pres/(kB*300)*1e-6;
if nargin == 1 
    return; 
end
if strcmpi(mix, 'He')
    ne = ne;
elseif strcmpi(mix, '5%')
    ne = ne/2*(0.95*2+0.05*10);
elseif strcmpi(mix, '2.5%')
    ne = ne/2*(0.975*2+0.025*10);
end
