D = 0.15;
F = 3;
lambda0 = 8e-7;
diaghole = 0.01; % in cm
pickoff = 0.025; % in cm
FFTspace = 128;
diameter = 2;
ax = linspace(-diameter*0.5,diameter*0.5,FFTspace);
x = repmat(ax, FFTspace, 1);
y = x';
mask = (x.^2 + y.^2 <= (diameter*0.5*0.65)^2);
FFTspace = 2^12;
spot = fftshift(fft2(mask, FFTspace, FFTspace));
cdat = abs(spot).^2;
Ccal = max(cdat(:));
lineout = diff(cdat(round(0.5*size(cdat,1)),:));
[~,ind] = max(lineout);
q_0 = 1.22*lambda0*F/D;
FFcal = 0.5*q_0/(round(0.5*size(cdat,1))-ind)*1e6;
NFcal = D/(diameter*0.65);

Nopt = 9;
NFs = zeros(size(mask,1), size(mask,2),Nopt);
% 1: normal NF
NFs(:,:,1) = mask;
% 2: Diagnostic hole clip
tempm = (x.^2 + (y-0.06/NFcal).^2 <= (0.5*diaghole/NFcal)^2);
NFs(:,:,2) = (~tempm).*mask;
% 3: Diagnostic hole w one on the other side as well
tempm = (x.^2 + (y+0.06/NFcal).^2 <= (0.5*diaghole/NFcal)^2);
NFs(:,:,3) = (~tempm).*NFs(:,:,2);
% 4: pickoff on other axis - same size as diagnostic hole
tempm = (y.^2 + (x+0.06/NFcal).^2 <= (0.5*diaghole/NFcal)^2);
NFs(:,:,4) = (~tempm).*NFs(:,:,2);
% 5: One diag hole and a one inch pick off
tempm = (x.^2 + (y+0.05/NFcal).^2 <= (0.5*pickoff/NFcal)^2);
NFs(:,:,5) = (~tempm).*NFs(:,:,2);
% 6: One diag hole and a one inch pick off - other axis
tempm = (y.^2 + (x+0.05/NFcal).^2 <= (0.5*pickoff/NFcal)^2);
NFs(:,:,6) = (~tempm).*NFs(:,:,2);
% 7: One diag hole and a one inch pick off with post
tempm = ones(size(mask));
tempm(20:40,61:68) = 0;
NFs(:,:,7) = tempm.*NFs(:,:,5);
% 8: One diag hole and a one inch pick off with post
tempm = ones(size(mask));
tempm(61:68,20:40) = 0;
NFs(:,:,8) = tempm.*NFs(:,:,6);
% 9: One diag hole and elliptic pickoff
tempm = (y.^2/3 + (x+0.06/NFcal).^2 <= (0.5*diaghole/NFcal)^2);
NFs(:,:,9) = (~tempm).*NFs(:,:,2);
%imagesc(NFs(:,:,9))

%Non-perfect wavefront
radius = sqrt((x.^2 + y.^2));
%radius = (radius);
theta = atan2(y,x);
wavefront = zeros(size(mask));
aberration(:,:,1) = 2*radius.^2 - ones(size(mask)); %defocus
coef(1) = 0;
aberration(:,:,2) = radius.^2.*cos(2.*theta); %0 degree astig
coef(2) = 1;
aberration(:,:,3) = radius.^2.*sin(2.*theta); %45 astig
coef(3) = 0.7;
aberration(:,:,4) = (3.*radius.^2-2).*radius.*cos(theta); %0 coma
coef(4) = 0.3;
aberration(:,:,5) = (3.*radius.^2-2).*radius.*sin(theta); %90 coma
coef(5) = 0.4;
aberration(:,:,6) = 6*radius.^4 - 6*radius.^1 + 1; %spherical
coef(6) = 0.1;
aberration(:,:,7) = radius.^3.*cos(3.*theta); %30 degree trefoil
coef(7) = 0;
aberration(:,:,8) = radius.^3.*sin(3.*theta); %0 degree trefoil
coef(8) = 0;
aberration(:,:,9) = (4*radius.^2-3).*radius.^2.*cos(2*theta); %0 degree secondary astigmatism
coef(9) = 0;
aberration(:,:,10) = (4*radius.^2-3).*radius.^2.*sin(2*theta); %45 degree secondary astigmatism
coef(10) = 0;
for i=1:10
   wavefront = wavefront + aberration(:,:,i).*coef(i); 
end
% Leave this in if want perfect wavefront
wavefront = zeros(size(mask));
load('owncolourmaps');
%% Plotting everything together

locs = GetFigureArray(Nopt,4,0.05, 0.05, 'down');
locs = locs + repmat([0 0 0.04 0]', 1, Nopt*4);
figure(10); clf;
axs = zeros(4,Nopt);
params = zeros(5,Nopt);
coun = 1;
lims = [-50 50];
for i=1:Nopt
    axs(1,i) = axes('Parent', 10, 'Position', locs(:,coun)); coun = coun + 1;
    imagesc(NFs(:,:,i));
    spot = fftshift(fft2(NFs(:,:,i).*exp(1i*wavefront), FFTspace, FFTspace));
    cdat = abs(spot).^2/Ccal;
    if (i==1) cdat0 = cdat; end;
    axs(2,i) = axes('Parent', 10, 'Position', locs(:,coun)); coun = coun + 1;
    spotax = linspace(-0.5*FFTspace, 0.5*FFTspace,FFTspace)*FFcal;
    imagesc(spotax, spotax, cdat); 
    params(1,i) = sum(sum(cdat(cdat>0.5)))/sum(cdat(:));
    params(2,i) = widthfwhm(cdat(round(0.5*FFTspace),:))*FFcal; %horizontal
    params(3,i) = widthfwhm(cdat(:,round(0.5*FFTspace))')*FFcal; %vertical
    params(4,i) = max(cdat(:));
    params(5,i) = params(2,i)/params(3,i);
    if (params(5,i)>1) params(5,i) = 1/params(5,i); end;
    title(sprintf('FWHM_h=%2.2f;  FWHM_v=%2.2f', params(2:3,i)));    
    axs(3,i) = axes('Parent', 10, 'Position', locs(:,coun)); coun = coun + 1;
    hlin = plot(spotax, cdat(round(0.5*FFTspace),:), 'r', ...
        spotax, cdat(:,round(0.5*FFTspace)), 'k',...
        spotax, cdat0(:,round(0.5*FFTspace)), '.b');
    title(sprintf('e = %2.3f', params(5,i)));
    axs(4,i) = axes('Parent', 10, 'Position', locs(:,coun)); coun = coun + 1;
    hlog = semilogy(spotax, cdat(round(0.5*FFTspace),:), 'r', ...
        spotax, cdat(:,round(0.5*FFTspace)), 'k',...
        spotax, cdat0(:,round(0.5*FFTspace)), '.b');
    xlabel('x / \mu m');
    title(sprintf('I=%2.2f;  FWHM_E=%2.2f%s', params(4,i), params(1,i)*100,' %'));
    drawnow;
end
linkaxes(axs(2:4,:), 'x');
set(axs(2,:), 'XLim', lims, 'YLim', lims);
set(axs(4,:), 'YLim', [1e-5 1]);
set(axs(1,:), 'XTickLabel', [], 'YTickLabel', []);
set(axs(:,2:Nopt), 'YTickLabel', []);
set(axs(1:3,:), 'XTickLabel', []);
set(get(axs(2,1), 'YLabel'), 'String', 'y / \mum');
set(get(axs(3,1), 'YLabel'), 'String', 'I / a.u.');
set(get(axs(4,1), 'YLabel'), 'String', 'log(I) / a.u.');
setAllFonts(10,10)
params
colormap(colmap.jet_white);
export_fig('ProbeSim', '-nocrop', '-transparent', '-pdf', 10);
%% Individual ones
locs = GetFigureArray(2,2,0.1, 0.05, 'down');
axss = zeros(4,1);
params = zeros(5,Nopt);
lims = [-50 50];
coun = 1;
for i=5:5%Nopt
    figure(10+i); clf;
    axss(coun) = axes('Parent', 10+i, 'Position', locs(:,coun)); coun = coun + 1;
    imagesc(NFs(:,:,i));
    spot = fftshift(fft2(NFs(:,:,i).*exp(1i*wavefront), FFTspace, FFTspace));
    cdat = abs(spot).^2/Ccal;
    if (i==1) cdat0 = cdat; end;
    axss(coun) = axes('Parent', 10+i, 'Position', locs(:,coun)); coun = coun + 1;
    hlog = semilogy(spotax, cdat(round(0.5*FFTspace),:), 'r', ...
        spotax, cdat(:,round(0.5*FFTspace)), 'k',...
        spotax, cdat0(:,round(0.5*FFTspace)), '.b');
    axss(coun) = axes('Parent', 10+i, 'Position', locs(:,coun)); coun = coun + 1;
    spotax = linspace(-0.5*FFTspace, 0.5*FFTspace,FFTspace)*FFcal;
    imagesc(spotax, spotax, cdat);    
    axss(coun) = axes('Parent', 10+i, 'Position', locs(:,coun)); coun = coun + 1;
    hlin = plot(spotax, cdat(round(0.5*FFTspace),:), 'r', ...
        spotax, cdat(:,round(0.5*FFTspace)), 'k',...
        spotax, cdat0(:,round(0.5*FFTspace)), '.b');
    params(1,i) = sum(sum(cdat(cdat>0.5)))/sum(cdat(:));
    params(2,i) = widthfwhm(cdat(round(0.5*FFTspace),:))*FFcal; %horizontal
    params(3,i) = widthfwhm(cdat(:,round(0.5*FFTspace))')*FFcal; %vertical
    params(4,i) = max(cdat(:))
end
set(axss(2:4), 'XLim', lims);
set(axss(3), 'YLim', lims);
set(axss(2), 'YLim', [1e-5 1]);
