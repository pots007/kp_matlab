IIshot = '20140910r005/20140910r005s029_Espec1.tif';
SIshot = '20140910r003/20140910r003s017_Espec1.tif';
IIim = imread(IIshot);
SIim = imread(SIshot);
figure (676); clf;

dosave = 1;
savfol = '/Users/kristjanpoder/Experiments/2014_Gemini/Data/20140910/';

clims = [600 1500];
ax1 = subplot(2,1,1);
imagesc(IIim');
title('II - 20140910r005s029 - ~3.5e18 cm^{-3}');
rectangle('Position', [173 446 778 190], 'EdgeColor', 'r')
set(gca, 'CLim', clims);
ax2 = subplot(2,1,2);
imagesc(SIim');
title('SI - 20140910r003s017 - ~3.5e18 cm^{-3}');
rectangle('Position', [175 448 778 190], 'EdgeColor', 'r');
set(gca, 'CLim', clims);
colormap(gray);

if dosave
    export_fig([savfol 'II_SI_comp'], '-nocrop', '-pdf', '-transparent', 676);
end

figure(676); clf;
clims = [300 1100];
IIzoom = IIim(173:173+778, 446:446+190);
SIzoom = SIim(175:175+778, 448:448+190);
ax1 = subplot(2,1,1);
imagesc(IIzoom');
title('II backgrounds');
rectangle('Position', [1 1 size(IIzoom,1) 40], 'EdgeColor', 'r')
rectangle('Position', [1 90 size(IIzoom,1) 100], 'EdgeColor', 'r')
set(gca, 'CLim', clims);
ax2 = subplot(2,1,2);
imagesc(SIzoom');
title('SI backgrounds');
rectangle('Position', [1 1 size(SIzoom,1) 45], 'EdgeColor', 'r')
rectangle('Position', [1 90 size(SIzoom,1) 100], 'EdgeColor', 'r')
set(gca, 'CLim', clims);
colormap(gray);

if dosave
    export_fig([savfol 'II_SI_comp'], '-nocrop', '-pdf', '-transparent', '-append', 676);
end

IIbg0 = mean([IIzoom(1:end, 1:40) IIzoom(1:end,90:190)],2);
SIbg0 = mean([SIzoom(1:end, 1:45) SIzoom(1:end,90:190)],2);


figure(676); clf;
clims = [0 300];
IIzoom = double(IIzoom) - repmat(smooth(IIbg0), 1, size(IIzoom,2));
SIzoom = double(SIzoom) - repmat(smooth(SIbg0), 1, size(SIzoom,2));
ax1 = subplot(2,1,1);
imagesc(IIzoom');
title('II - background + signal area');
%rectangle('Position', [1 1 size(IIzoom,1) 40], 'EdgeColor', 'r')
%rectangle('Position', [1 90 size(IIzoom,1) 100], 'EdgeColor', 'r')
rectangle('Position', [200 50 550 30], 'EdgeColor', 'g');
set(gca, 'CLim', clims);
ax2 = subplot(2,1,2);
imagesc(SIzoom');
title('SI - background + signal area');
%rectangle('Position', [1 1 size(SIzoom,1) 45], 'EdgeColor', 'r')
%rectangle('Position', [1 90 size(SIzoom,1) 100], 'EdgeColor', 'r')
rectangle('Position', [50 55 700 25], 'EdgeColor', 'g');
set(gca, 'CLim', clims);
colormap(gray);

if dosave
    export_fig([savfol 'II_SI_comp'], '-nocrop', '-pdf', '-transparent', '-append', 676);
end
%
figure(676); clf;
IIsignal = sum(IIzoom(:, 50:80),2);
IIsignal(1:200) = 0; IIsignal(750:end) = 0;
SIsignal = sum(SIzoom(:, 55:80),2);
SIsingla(1:50) = 0; SIsignal(750:end) = 0;
hs = plot(1:length(IIsignal), smooth(IIsignal), '--r', 1:length(SIsignal), smooth(SIsignal), '.-k');
legend(hs, {'II', 'SI'}, 'Location', 'Northwest')
title(sprintf('Ratio of integral of counts on II/SI = %2.2f', sum(IIsignal)/sum(SIsignal)));
ylabel('Full vertical binning of previous page');
setAllFonts(676,20);

if dosave
    export_fig([savfol 'II_SI_comp'], '-nocrop', '-pdf', '-transparent', '-append', 676);
end