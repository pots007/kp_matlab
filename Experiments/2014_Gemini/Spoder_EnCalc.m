% 
%All in cm 
E0 = 10; %J, should have that much on TCC
%E0 = E0/40; % FOR HP ALIGNEMNT TIME
tau = 40; %in fs
D0 = 15;
f = 300;
l = [150 200];
D = l/f*D0;
E1 = E0*0.1; %after holey wedge reflection
I = E0./(tau*1e-15*pi*0.25*D.^2);
I(2) = I(2)*0.1;
Ef = E1*(0.8./D).^2; % energy in an 8mm pickoff beam

B = 2*pi/8e-7*Ef.*I*1e-16*0.001;

fprintf('%25s\t%1.2f or %1.2f mJ\n', 'Energy in pickoff beam:', Ef*1e3);
fprintf('%25s\t%1.2f or %1.2f mJ\n', 'B-integral:', B);
