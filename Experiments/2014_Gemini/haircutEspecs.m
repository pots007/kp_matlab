datfol = '/Users/kristjanpoder/Experiments/2014_Gemini/Data/20140910/';
runs = getFolderList(datfol);
for i=1:length(runs)
    for l=1:2
        espec1s = dir([datfol runs{i} '/*' sprintf('Espec%i*', l)]);
        for k=1:length(espec1s)
            fprintf('%s\n', espec1s(k).name);
            espec = imread([datfol runs{i} '/' espec1s(k).name]);
            HC = G14_FilterPackInspector2(double(espec));
            imwrite(uint16(HC), [datfol runs{i} '/' espec1s(k).name(1:end-4) 'HC.tif'], 'TIFF')
        end
    end
end