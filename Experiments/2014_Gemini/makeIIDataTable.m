% col 1:  set pressure
% col 2:  run number
% col 3:  shot numbers
% col 4:  line pressures
% col 5:  South energy
% col 6:  pressure average from transducers
% col 7:  plasma density calculated from col 6.
IIrundata = cell(12,7,3);
pressures = [120,140,160,180,200,220,250,300,350,400,500,600];
for i=1:12
    IIrundata{i,1,1} = pressures(i);
    IIrundata{i,1,2} = pressures(i);
    IIrundata{i,1,3} = pressures(i);
end
% Pressure scans only.
% He ones:
for i=1:12 IIrundata{i,2,1} = 3; end
IIrundata{1,3,1} = 1:3;     %120
IIrundata{2,3,1} = 4:6;     %140
IIrundata{3,3,1} = 7:9;     %160
IIrundata{4,3,1} = 10:12;   %180
IIrundata{6,3,1} = 13:15;   %220
IIrundata{7,3,1} = 16:18;   %250
IIrundata{8,3,1} = 19:21;   %300
IIrundata{9,3,1} = 22:24;   %350
IIrundata{10,3,1}= 25:27;   %400
IIrundata{11,3,1}= 28:31;   %500

%He + 2.5% N2
for i=1:12 IIrundata{i,2,2} = 8; end
IIrundata{1,3,2} = 1:4;     %120
IIrundata{2,3,2} = 5:8;     %140
IIrundata{3,3,2} = 9:12;    %160
IIrundata{4,3,2} = 13:16;   %180
IIrundata{5,3,2} = 17:20;   %200
IIrundata{6,3,2} = 21:24;   %220
IIrundata{7,3,2} = 25:28;   %250
IIrundata{8,3,2} = 29:32;   %300
IIrundata{10,3,2}= 33:36;   %400
IIrundata{11,3,2}= 37:40;   %500
IIrundata{12,3,2}= 41:44;   %600

%He + 5% N2
for i=1:12 IIrundata{i,2,3} = 5; end
IIrundata{1,3,3} = 5:8;     %120
IIrundata{2,3,3} = 9:12;    %140
IIrundata{3,3,3} = 13:16;   %160
IIrundata{4,3,3} = 17:19;   %180
IIrundata{5,3,3} = 20:23;   %200
IIrundata{6,3,3} = 24:27;   %220
IIrundata{7,3,3} = 28:31;   %250
IIrundata{8,3,3} = 32:35;   %300
IIrundata{9,3,3} = 36:39;   %350
IIrundata{10,3,3}= 40:43;   %400
IIrundata{11,3,3}= 44:47;   %500
IIrundata{12,3,3}= 48:51;   %600

% Now fill in the line pressures.
% For He, reg2, reg 4 for others.
load Gemini_2014
for i=1:3
    for k=1:size(IIrundata,1)
        shoz = IIrundata{k,3,i};
        if isempty(shoz) continue; end;
        lines = zeros(size(shoz)); ens = zeros(size(shoz));
        for m=1:length(shoz)
            if i==1
                lines(m) = Gemini_2014.D20140910.run(IIrundata{1,2,i}).shot(shoz(m)).reg2pressure;
            else
                lines(m) = Gemini_2014.D20140910.run(IIrundata{1,2,i}).shot(shoz(m)).reg4pressure;
            end
            ens(m) = Gemini_2014.D20140910.run(IIrundata{1,2,i}).shot(shoz(m)).southenergy;
        end
        IIrundata{k,4,i} = lines;
        IIrundata{k,5,i} = ens;
    end
end
%%
% Now put in the plasma densities.
% Loop through the folders, find the pressures and take the mean.
targets = {'He', '2.5%', '5%'};
if ismac
    datfol = '/Users/kristjanpoder/Experiments/2014_Gemini/Data/20140910/';
end
for k=1:size(IIrundata,3)
    for i=1:size(IIrundata,1)
        shotz = IIrundata{i,3,k};
        if isempty(shotz)
            continue
        else
            press = zeros(2,length(shotz));
        end 
        for m=1:length(shotz)
            try
                fname = sprintf('%s20140910r%03i/20140910r%03is%03i_gas1.txt',...
                    datfol, IIrundata{i,2,k}, IIrundata{i,2,k}, shotz(m));
                [p(1),ne(1)] = findGemini2014Pressure(fname, targets{k});
            catch
            end
            try
                fname = sprintf('%s20140910r%03i/20140910r%03is%03i_gas2.txt',...
                    datfol, IIrundata{i,2,k}, IIrundata{i,2,k}, shotz(m));
                [p(2),ne(2)] = findGemini2014Pressure(fname, targets{k});
            catch
            end
            press(1,m) = mean(p);
            press(2,m) = mean(ne);
        end
        IIrundata{i,6,k} = press(1,:);
        IIrundata{i,7,k} = press(2,:);
    end
end

if ismac
    save('/Users/kristjanpoder/Dropbox/MATLAB/Experiments/2014_Gemini/IIrundata.mat', 'IIrundata');
end
%% Save all the x-ray images (res and filterpack) for the scan into one mat file



%%
makeplots = true;
if (makeplots)
    figure(6); clf;
    locs = GetFigureArray(1,3,0.05,0.05,'down');
    axs = zeros(3,1);
    for i=1:3
        totz = sum(cellfun(@length, IIrundata(:,4,i)));
        lines = []; asked = [];
        for k=1:12
            lines = [lines; IIrundata{k,4,i}'];
            asked = [asked; repmat(IIrundata{k,1,i}, length(IIrundata{k,4,i}),1)];
        end
        axs(i) = axes('Parent', 6, 'Position', locs(:,i));
        %hs = plot(1:totz, lines, 'ro', 1:totz, asked, 'kx');
        hs = plot(1:totz, lines./asked, 'ro')
        set(hs, 'MarkerSize', 5, 'LineWidth', 1.1);
    end
end