if ispc
    f1 = 'F:\Kristjan\Documents\Uni\Dropbox\MATLAB\experiments\2014_Gemini\Timing\DATA_00108750_00108271_USER_SELECTION.csv';
    filepath = 'F:\Kristjan\Documents\Uni\Dropbox\MATLAB\experiments\2014_Gemini\Timing\';
    f2 = 'F:\Kristjan\Documents\Uni\Dropbox\MATLAB\experiments\2014_Gemini\Timing\DATA_00109290_00108751_USER_SELECTION.csv';
elseif ismac
    f1 = '/Users/kristjanpoder/Dropbox/MATLAB/experiments/2014_Gemini/Timing/DATA_00108750_00108271_USER_SELECTION.csv';
    f2 = '/Users/kristjanpoder/Dropbox/MATLAB/experiments/2014_Gemini/Timing/DATA_00109290_00108751_USER_SELECTION.csv';
    filepath = '/Users/kristjanpoder/Dropbox/MATLAB/experiments/2014_Gemini/Timing/';
end

data1 = csvimport(f1);
data2 = csvimport(f2);
data_all = [data2; data1(2:end,:)];
GSNs = cellfun(@(x) str2double(x(2:end-1)), data_all(2:end,1));
Nens2 = cell2mat(data2(2:end,8));
Nens1 = zeros(length(data1)-1,1);
for i=1:length(Nens1)
    Nens1(i) = str2double(data1{i+1,8});
end
Nens = [Nens2; Nens1];
% Now that we have this, iterate through the shots that have appreciable N
% energy.
Nshots = sum(Nens<1);
Deltabeams = zeros(size(GSNs));
for i=1:length(GSNs)
    if Nens(i)<1
        continue;
    end
    try
        Stracef = dir(sprintf('%sS*%i.csv', filepath, GSNs(i)));
        Strace = importdata([filepath Stracef.name]);
        Sdata = Strace.data;
        Ntracef = dir(sprintf('%sN*%i.csv', filepath, GSNs(i)));
        Ntrace = importdata([filepath Ntracef.name]);
        Ndata = Ntrace.data;
        
        % Find halfpoint time for N
        k=1;
        while Ndata(k,2)-0.5*max(Ndata(:,2))<0; k = k+1; end
        slop = (Ndata(k,2) - Ndata(k-1,2))/(Ndata(k,1) - Ndata(k-1,1));
        xd = (0.5*max(Ndata(:,2))-Ndata(k-1,2))/slop;
        Nt = Ndata(k-1,1) + xd;
        % And for S
        k=1;
        while Sdata(k,2)-0.5*max(Sdata(:,2))<0; k = k+1; end
        slop = (Sdata(k,2) - Sdata(k-1,2))/(Sdata(k,1) - Sdata(k-1,1));
        xd = (0.5*max(Sdata(:,2))-Sdata(k-1,2))/slop;
        St = Sdata(k-1,1) + xd;
        Deltabeams(i) = St-Nt;
    catch
        continue;
    end
end
Deltabeams(Deltabeams==0) = nan;
figure(2)
plot(GSNs, Deltabeams, 'o');
save([filepath 'Timings.mat'], 'GSNs', 'Deltabeams');