%function RayTraceCurvedCrystal
%% All distance units in mm
Ds = 1000;
Dccd = 100;
pixel_size = 0.026;
no_pixels = 1024;
Lccd = pixel_size*no_pixels; %CCD size; can also just input a number if not sure
d = 1e3*4.371e-10; % For PET
R = 100; % Crystal radius
E0 = 6.5; % In keV;
% Set this to nonzero if rotating crystal such that E0 is not central
rot_angle = 0; % In degrees!
% set this to 0 for only central ray, 1 for edges as well, 2 for all rays onto
% the chip, 3 for all rays
plot_all = 1;
% If the beam divergence is limited, enter half cone angle in rad below
half_angle = 0.005; % In radians
% If the beam is illuminating the chip at a non zero angle to the axis,
% enter the offset angle below
offs_angle = 0; % In mrad!
%% Calculate wavelength and fix the crystal centre position
Constants;
lambda0 = 2*pi*c*hbar/(qe*1e3*E0)*1e3;
theta0 = asind(0.5*lambda0/d);
fprintf('Crystal angle should be %2.3f degrees\n', theta0);
P0 = [0 0];
offs_angle = offs_angle*1e-3;
% The crystal spec is still in the same place!
P1 = Ds*[1 0];
% The centre of the circle will be at
Centre = R*[sind(theta0+rot_angle) -cosd(theta0+rot_angle)] + P1;
% The central ray will reflect specularly at Bragg and make it to the CCD
% and hits at
CCD_centreE0 = Dccd*[cosd(2*theta0+2*rot_angle) sind(2*theta0+2*rot_angle)] + P1;
theta0c = 2*(theta0);
if rot_angle~=0
    fprintf('Crystal angle is actually %2.3f degrees\n', theta0+rot_angle);
end
CCD_centre = Dccd*[cosd(theta0c) sind(theta0c)] + P1;
% Slope for line of CCD plane is k=-1/tand(2*theta0)
% Ends of the CCD:
CCD_high = CCD_centre + 0.5*Lccd*[-sind(theta0c) cosd(theta0c)];
CCD_low = CCD_centre + 0.5*Lccd*[sind(theta0c) -cosd(theta0c)];
% Find out what angle would hit the CCD directly
fprintf('Rays at angle %2.3f mrad(%2.4f deg) will hit the CCD directly!\n',...
    atan(CCD_low(2)/CCD_low(1))*1e3, atand(CCD_low(2)/CCD_low(1)));
% Plot the central ray and CCD
figure(88); clf;
plot([0 P1(1)], [0 P1(2)], 'k', 'LineWidth', 1.5);
hold on;
hE0 = plot([P1(1) CCD_centreE0(1)], [P1(2) CCD_centreE0(2)], 'k', 'LineWidth', 1.5);
hCCD = plot([CCD_high(1) CCD_low(1)], [CCD_high(2) CCD_low(2)], 'r', 'LineWidth', 2.5);
% Plot the crystal as well
hcrys = plot(Centre(1)+R*cosd(90:0.1:165), Centre(2)+R*sind(90:0.1:165), 'b', 'LineWidth', 1.5);
hcen = plot(Centre(1), Centre(2), 'xb', 'LineWidth', 1.5); 
%plot([P1(1) Centre(1)], [P1(2) Centre(2)], 'k');
hold off;
axis equal
hleg = legend([hE0 hCCD hcrys], {sprintf('E_0 = %1.1f keV', E0), 'CCD', 'Crystal'}, 'Location', 'Southeast');
set(gca, 'XLim', [Ds-100 Ds+100], 'YLim', [-30 80]);

%% Now iterate through some angles to see which make it to the chip
% Smallest angle is hitting the very top of the crystal:
themin = (100+Centre(2))/Centre(1);
if offs_angle>themin %The beam is above the crystal
    error('No rays will hit the crystal!');
end
%themin = themin + offs_angle;
if themin < 0
    thet = (themin:-1e-5:3*themin)*180/pi;
else
    if half_angle~=0
        if themin<half_angle
            thet = (-themin:1e-5:themin)*180/pi;
        else
            thet = (-half_angle:1e-5:half_angle)*180/pi;
        end
    else
        thet = (-3*themin:1e-5:themin)*180/pi;
    end
end
thet = linspace(-half_angle,half_angle,1e3);
thet = thet + offs_angle;
thet = thet(thet<themin);
thet = thet*180/pi;
%thet  = thet+offs_angle;
dists = zeros(size(thet));
locs = zeros(size(thet));
hs = zeros(size(thet));
for i=1:length(thet)
    % Equation of the crystal circle is
    % y = yc + sqrt(r^2 - (x-xc)^2)
    % Equation for the ray is
    % y = y0 + kx; k = tand(thet) (assuming y0=0 below)
    % Intersection from solution to
    % x^2*(k^2-1)-x*(2*k*yc)+(yc^2+xc^2-r^2)=0
    % With the beam coming from not zero, replace y0 with
    % delta y = y0 -yc
    xin = quadratic(tand(thet(i))^2+1, -(2*tand(thet(i))*(-P0(2)+Centre(2))+2*Centre(1)), ...
        Centre(1)^2+(P0(2)-Centre(2))^2-R^2);
    Ploc = min(xin)*[1 tand(thet(i))] + P0; % The point the ray reflects off the crystal
    alpha = atand((Centre(2)-Ploc(2))/(Centre(1)-Ploc(1)));
    k = tand(180-2*abs(alpha)+2*thet(i));
    % The slope for the reflected ray is that above.
    % The intersection with the detector plane is given by
    % y0'-y0 = (k-k')*(x0'-x0), where the primed are quantites for the CCD
    % plane line, see slope above
    xD = (CCD_centre(2)-tand(90+theta0c)*CCD_centre(1) - Ploc(2) + k*Ploc(1))/(k-tand(90+theta0c));
    Dloc = [xD Ploc(2)+k*(xD-Ploc(1))];
    if (plot_all~=0)
        hold on;
        hs(i) = plot([P0(1) Ploc(1) Dloc(1)], [P0(2) Ploc(2) Dloc(2)], '--');
        hold off;
    end
    % Now check which rays make it onto the chip
    distan = sqrt((Dloc(1)-CCD_centre(1))^2+(Dloc(2)-CCD_centre(2))^2);
    if distan<0.5*Lccd
        dists(i) = sind(90+alpha-rot_angle-thet(i))*2*d;
        locs(i) = distan;
        if Dloc(1) > CCD_centre(1)
            locs(i) = -distan;
        end
    end
end
% if didn't want all rays, delete the useless ones.
if plot_all==2 || plot_all==1
    h1 = hs(dists==0);
    delete(h1);
    if plot_all==1
        h1 = hs(dists~=0);
        delete(h1(2:end-1));
    end
end

dists1 = dists(dists~=0);
thets = thet(dists~=0);
fprintf('Energies from %2.3f to %2.3f keV will hit the chip\n', h*c./(qe*max(dists1)), h*c./(qe*min(dists1)));
fprintf('Angles from %2.2f mrad(%2.4f deg) to %2.2f mrad(%2.4f deg) will hit the chip\n',...
    min(thets)*pi/0.18, min(thets), max(thets)*pi/0.18, max(thets))
%% Additionally plot the dlambda/dx curve
locs = locs(dists~=0);
ens = h*c./(qe.*dists1);
dlambda_dx = (dists1(2:end)-dists1(1:end-1))./(locs(2:end)-locs(1:end-1));
dE_dx = (ens(2:end)-ens(1:end-1))./(locs(2:end)-locs(1:end-1));
figure(89); clf;
%plot(locs(1:end-1),dlambda_dx, '-x');
h1 = plot(locs(1:end-1)/pixel_size, abs(dE_dx*pixel_size*1e3));
xlabel('Pixel')
ylabel('dE per pixel (eV)');
ax1 = gca;
ax2 = axes('Parent', 89, 'Position', get(ax1, 'Position'), 'XAxisLocation', 'top',...
    'YAxisLocation', 'right', 'XColor', 'r', 'YColor', 'r', 'Box', 'off', 'Color', 'none');
h2 = plot(ax2, locs, ens, 'xr');
set(ax2, 'XAxisLocation', 'top',...
    'YAxisLocation', 'right', 'XColor', 'k', 'YColor', 'k', 'Color', 'none')
set(ax2, 'XLim', get(ax1, 'XLim')*pixel_size);
ylim1 = get(ax1, 'YLim'); ylim2 = get(ax2, 'YLim');
set(ax1, 'YTick', linspace(ylim1(1),ylim1(2),8))
set(ax1, 'YTickLabel', num2str(get(ax1, 'YTick')',2))
set(ax2, 'YTick', linspace(ylim2(1),ylim2(2),8))
set(ax2, 'YTickLabel', num2str(get(ax2, 'YTick')',2))
xlim1 = get(ax1, 'XLim'); xlim2 = get(ax2, 'XLim');
set(ax1, 'XTick', linspace(xlim1(1),xlim1(2),7))
set(ax1, 'XTickLabel', num2str(get(ax1, 'XTick')','%1.0f'))
set(ax2, 'XTick', linspace(xlim2(1),xlim2(2),7))
set(ax2, 'XTickLabel', num2str(get(ax2, 'XTick')','%1.1f'))
ylabel(ax2, 'Photon energy (keV)');
xlabel(ax2, 'Distance on chip (mm)');
legend([h1 h2], {'Resolution on chip', 'Photon energy on pixel'}, 'Location', 'northeast')
setAllFonts(89,18);
