if ~exist('raws', 'var')
    [nums, texts, raws] = xlsread('F:\Kristjan\Documents\Uni\Imperial\2014_ManglesGemini\Shotsheet_20141013.xlsx');
end
rawdat = raws(3:end,:); %filter out the top text rows
% Also call the actual Ecat data s is not in the google sheet
ecatdata = GetGeminiShotsData('Aug2014');
dates = unique(cell2mat(rawdat(:,1)));
dates = dates(~isnan(dates));

for i=1:length(dates)
    Gemini_2014.(sprintf('D%i',dates(i))) = struct;
end
for i=1:length(dates)
    i
    lmat = cell2mat(rawdat(:,1)) == dates(i);
    datedata = rawdat(lmat,:);
    runs = unique(cell2mat(datedata(:,2)));
    for k=1:length(runs) %preallocate the structures
        Gemini_2014.(sprintf('D%i',dates(i))).run(k) = struct;
    end
    for k=1:length(runs)
        lmat = cell2mat(datedata(:,2)) == runs(k);
        rundata = datedata(lmat,:);
        for l=1:sum(lmat) %prealloctae the structure for shots;
            Gemini_2014.(sprintf('D%i',dates(i))).run(k).shot(l) = struct;
        end
        for l=1:sum(lmat)
            %Actually fill in the data.
            Gemini_2014.(sprintf('D%i',dates(i))).run(k).shot(l).setpressure = rundata{l,9};
            Gemini_2014.(sprintf('D%i',dates(i))).run(k).shot(l).GSN = rundata{l,5};
            Gemini_2014.(sprintf('D%i',dates(i))).run(k).shot(l).comment = rundata{l,20};
            Gemini_2014.(sprintf('D%i',dates(i))).run(k).shot(l).result = rundata{l,21};
            Gemini_2014.(sprintf('D%i',dates(i))).run(k).shot(l).target = rundata{l,8};
            lineno = find(cell2mat(ecatdata(:,1))==rundata{l,5});
            if ~isempty(lineno)
                Gemini_2014.(sprintf('D%i',dates(i))).run(k).shot(l).reg2pressure = str2double(ecatdata{lineno,5});
                % Added the line below 03/11/2014 for II analysis
                Gemini_2014.(sprintf('D%i',dates(i))).run(k).shot(l).reg4pressure = str2double(ecatdata{lineno,7});
                Gemini_2014.(sprintf('D%i',dates(i))).run(k).shot(l).southenergy = str2double(ecatdata{lineno,3});
                Gemini_2014.(sprintf('D%i',dates(i))).run(k).shot(l).time = ecatdata{lineno,2};
            end
        end
    end
end

save('F:\Kristjan\Documents\Uni\Dropbox\MATLAB\Experiments\2014_Gemini\Gemini_2014.mat', 'Gemini_2014');