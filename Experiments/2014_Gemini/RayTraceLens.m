function RayTraceLens

%F number of emitted rays
fnumber = 40;
%Radius of probe beam /mm
beamradius = 150;
%Order of things in beam path - d is free space, l is lens
types = {'d' 'l' 'd'  'l' 'd'};
%Details of path, for d the free-path distance, for l the focal length
param = [2500 6000 6350 150 500];

figure(101); clf;
nrays = 10;
thetamax = atan(1/(2*fnumber));
thetavec = linspace(0,thetamax,nrays);
hvec = [beamradius zeros(1,nrays-1)];

for n = 1:nrays
    rays(n) = struct;
end

%Set up rays
for n = 1:nrays
    rays(n).hvec = [hvec(n)];
    rays(n).thetavec = [thetavec(n)];
    rays(n).dvec = [0];
end

%Raytrace
for n = 1:length(types)
    for m = 1:nrays
        switch types{n}
            case 'l'
                rays(m) = PassThroughLens(rays(m), param(n));
            case 'd'
                rays(m) = PropagateFreeSpace(rays(m), param(n));
        end
    end
end

hold on

%Plot rays
for n = 1:nrays
    
    if (rays(n).thetavec(1) == 0)
        plot(rays(n).dvec, rays(n).hvec, 'r')
        plot(rays(n).dvec, -rays(n).hvec, 'r')
    else
        plot(rays(n).dvec, rays(n).hvec)
        plot(rays(n).dvec, -rays(n).hvec)
    end
    
end

%Plot lenses
dcurr = 0;
lcount = 0;
for n = 1:length(types)
    switch types{n}
        case 'l'
            line([dcurr dcurr], [-50 50], 'Color', 'black')
            lcount = lcount+1;
            lens_f(lcount) = param(n);
            lens_d(lcount) = dcurr;
        case 'd'
            dcurr = dcurr + param(n);
    end
end

%Find image distance and magnification
image = 0;
M = 1;
for n = 1:lcount
    u = lens_d(n) - image;
    f = lens_f(n);
    v = u*f/(u-f);
    image = lens_d(n) + v;
    M = M*v/u;
end

%Sort shit out
text(image*1.02, 0, ['M = ' num2str(M)], 'EdgeColor', 'black', 'BackgroundColor', 'white')
line([image image], [-50 50], 'LineStyle', '--', 'Color', 'black')
xlabel('Distance From Object /mm', 'FontSize', 14)
ylabel('Lateral Distance /mm', 'FontSize', 14)
title('Red = Laser, Blue = Image-rays, Black = Lenses, Dotted = Image', 'FontSize', 14)
set(gca, 'FontSize', 14)
set(gcf, 'Color', 'white')

hold off


end

function ray = PassThroughLens(ray, f)

hnew = ray.hvec(end);
thetanew = -ray.hvec(end)/f + ray.thetavec(end);
dnew = ray.dvec(end);
ray.hvec = [ray.hvec hnew];
ray.thetavec = [ray.thetavec thetanew];
ray.dvec = [ray.dvec dnew];

end

function ray = PropagateFreeSpace(ray, d)

hnew = ray.hvec(end) + d*ray.thetavec(end);
thetanew = ray.thetavec(end);
dnew = ray.dvec(end) + d;
ray.hvec = [ray.hvec hnew];
ray.thetavec = [ray.thetavec thetanew];
ray.dvec = [ray.dvec dnew];

end
