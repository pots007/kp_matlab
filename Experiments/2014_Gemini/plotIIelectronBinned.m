%% Binned version of above

if ismac
    datp = '/Users/kristjanpoder/Experiments/2014_Gemini/Data/20140910/';
end
load ('IIrundata')
figure(55); clf;
z = {'He', '2.5%', '5%'};
for i=1:3
    shots = []; press = [];
    for k=1:12
        shots = [shots IIrundata{k,3,i}];
        press = [press IIrundata{k,7,i}];       
    end
    [pres_sorted, I] = sort(press, 2, 'descend');
    shots_sorted = shots(I);
    spres{i} = [shots_sorted; pres_sorted*1e-18];
    runss(i) = IIrundata{k,2,i};
end


figure(55); clf;
ax_h = (1-0.07)/3;
ax_w = (1-0.1)/65;
bins = 11:-0.5:1;

% Electrons
wstart = 0.05; it = 1;
for i=1:length(bins)
    wcount = zeros(1,3);
    for m=1:length(z)
        temp = spres{m};
        for k=1:length(temp)
            if (temp(2,k)>bins(i) && temp(2,k)<bins(i)+0.5)
                %filename = sprintf('20131010r001s%03i_Espec2.raw', temp(1,k));
                filename = sprintf('%s20140910r%03i/20140910r%03is%03i_Espec1HC.tif', datp,...
                    runss(m),runss(m), temp(1,k))
                dat = imread(filename);
                dat = dat(150:end,450:640);
                %dat = imread([fol filename]);
                pressure = temp(2,k);
                heax(it) = axes('Position', [wstart+wcount(m)*ax_w, 0.05+(m-1)*ax_h, ax_w, ax_h-0.03],...
                    'XTick', [], 'YTick', []);
                imagesc(dat);
                it = it+1;
                hold on;
                %plot([1 400], [192 192], 'r', 'LineWidth', 1.3); %100 MeV line
                hold off;
                set(gca, 'XTick', [], 'YTick', [], 'CLim', [500 1200]);
                xlabel(sprintf('%2.2f', pressure));
                %title(sprintf('r%01is%03i', runss(m), temp(1,k)));
                wcount(m) = wcount(m) + 1;
                drawnow;
            end
        end
    end
    wstart = wstart + max(wcount)*ax_w;
end

for i=1:length(z)
    axes('Position', [0.04, 0.05+(i-1)*ax_h, 0.001, ax_h-0.03], ...
        'YTick', [], 'XTick', [], 'Color', 'none')
    ylabel(z{i});
    %axes('Position', [0.95, 0.05+(i-1)*ax_h, 0.001, ax_h-0.03], ...
     %   'XTick', [], 'Color', 'none', 'YColor', 'r',...
      %  'YAxisLocation', 'right', 'YLim', [0 640],...
       % 'YDir', 'normal', 'YTick', [0 640-192],...
        %'YTickLabel', [46 140]);
end
colormap(55, gray);
%%
% x-rays
figure(56); clf;
wstart = 0.05; it = 1;
for i=1:length(bins)
    wcount = zeros(1,3);
    for m=1:length(z)
        temp = spres{m};
        for k=1:length(temp)
            if (temp(2,k)>bins(i) && temp(2,k)<bins(i)+0.5)
                %filename = sprintf('20131010r001s%03i_Espec2.raw', temp(1,k));
                filename = sprintf('%s20140910r%03i/20140910r%03is%03i_x-rayHC.tiff', datp,...
                    runss(m),runss(m), temp(1,k));
                try
                    dat = imread(filename);
                catch
                    wcount(m) = wcount(m) + 1;
                end
                %dat = dat(:,:);
                %dat = imread([fol filename]);
                pressure = temp(2,k);
                hxax(it) = axes('Position', [wstart+wcount(m)*ax_w, 0.05+(m-1)*ax_h, ax_w, ax_h-0.03],...
                    'XTick', [], 'YTick', []);
                imagesc(dat);
                it = it+1;
                hold on;
                %plot([1 400], [192 192], 'r', 'LineWidth', 1.3); %100 MeV line
                hold off;
                set(gca, 'XTick', [], 'YTick', [], 'CLim', [500 1200]);
                xlabel(sprintf('%2.2f', pressure));
                wcount(m) = wcount(m) + 1;
                drawnow;
            end
        end
    end
    wstart = wstart + max(wcount)*ax_w;
end

for i=1:length(z)
    axes('Position', [0.04, 0.05+(i-1)*ax_h, 0.001, ax_h-0.03], ...
        'YTick', [], 'XTick', [], 'Color', 'none', 'Parent', 56)
    ylabel(z{i});
end
%%
%export_fig([datp 'II_especs'], '-nocrop', '-transparent', '-pdf', 55);
%export_fig([datp 'II_xrays'], '-nocrop', '-transparent', '-pdf', 56);