data = GetGeminiShotsData('Aug2014');
userName='iclaserplasma@gmail.com'; %your '...@gmail.com' email address; if empty you can enter one in the dialog box. 
password = 'Imperial.1';
%[aTokenDocs,aTokenSpreadsheet]=connectNAuthorize(userName);
[aTokenDocs,aTokenSpreadsheet]=connectNAuthorizeStatic(userName, password);
%pause(0.5);

if isempty(aTokenDocs) || isempty(aTokenSpreadsheet)
    warndlg('Could not obtain authorization tokens from Google.','');
    return;
end
disp('Logged in.');
sheetIwant = -1;
sheetlist = getSpreadsheetList(aTokenSpreadsheet);
if (~isempty(sheetlist))
    for i=1:length(sheetlist)
        if (strcmp('ManglesGemini2014_DataSheet', sheetlist(i).spreadsheetTitle))
            sheetIwant = i;
        end
    end
end

worksheetlist = getWorksheetList(sheetlist(sheetIwant).spreadsheetKey, aTokenSpreadsheet);
worksheetIwant = -1;
if (~isempty(worksheetlist))
    for i=1:length(worksheetlist)
        if (strcmp('ShotSheet', worksheetlist(i).worksheetTitle))
            worksheetIwant = i;
        end
    end
end

if (sheetIwant<0 || worksheetIwant <0)
    if (sheetIwant<0)
        error('Incorrect worksheet name!'); 
    else
        error('Incorrect worksheet name!');
    end
end
disp('Success')

for i=1:10%size(data,1)
    disp(['Iteration ' num2str(i)]);
    GSN = getWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, i, 5, aTokenSpreadsheet);
    j=1;
    if(isempty(GSN))
        continue;
    end
    try
        while(str2double(GSN) ~= data{j,1})
            j = j+1; 
        end
    catch err
       continue 
    end
    % Uncompressed energies
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, ...
        i, 25, sprintf('%2.3f', str2double(data{j,3})), aTokenSpreadsheet);

    % Shot times
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, ...
        i, 4, data{j,2}(13:end-1), aTokenSpreadsheet);
    
    %All pressures
    % Reg 2 pressures
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, ...
        i, 10, sprintf('%2.3f', str2double(data{j,5})), aTokenSpreadsheet);
    
%     editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
%         worksheetlist(worksheetIwant).worksheetKey, ...
%         i, 10, num2str(allpres(j)), aTokenSpreadsheet);
%     
%     
%     % Compressed energies
%     editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
%         worksheetlist(worksheetIwant).worksheetKey, ...
%         i, 25, num2str(allcomens(j)), aTokenSpreadsheet);
end
