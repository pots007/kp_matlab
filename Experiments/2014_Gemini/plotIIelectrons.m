load('IIrundata');
locs = GetFigureArray(12,3, 0.06, 0.005, 'across');
if ismac
    datp = '/Users/kristjanpoder/Experiments/2014_Gemini/Data/20140910/';
end
figure(54); clf;
it = 1;
xaxs = zeros(12,3); eaxs = zeros(size(xaxs));
for i=1:3
    for k=1:12
        shotz = IIrundata{k,3,i}; 
        if isempty(shotz) 
            it = it+1;
            continue; 
        end;
        %xaxs(k,i) = axes('Parent', 54, 'Position', locs(:,it)); it = it+1;
        eaxs(k,i) = axes('Parent', 54, 'Position', locs(:,it)); it = it+1;
        xrayz = []; ez = [];
        for m=1:length(shotz)
            fname = sprintf('%s20140910r%03i/20140910r%03is%03i_x-ray.spe', datp,...
                IIrundata{k,2,i},IIrundata{k,2,i}, shotz(m));
            try
                %razz = ReadSPE(fname);
            catch
                %razz = NaN(2048);
            end
            %xrayz = [xrayz; razz];
            fname = sprintf('%s20140910r%03i/20140910r%03is%03i_Espec1HC.tif', datp,...
                IIrundata{k,2,i},IIrundata{k,2,i}, shotz(m));
            dat = imread(fname);
            ez = [ez dat(:,450:640)];
        end
        %imagesc(xrayz, 'Parent', xaxs(k,i));
        imagesc(ez(150:end,:), 'Parent', eaxs(k,i));
        set([eaxs(eaxs~=0) xaxs(xaxs~=0)], 'XTick', [], 'YTick', []);
        if i==1
            title(sprintf('%1.1e', mean(IIrundata{k, 7, i})));
        end
        drawnow;
    end
end

axis(xaxs(xaxs~=0), 'image'); 
colormap(54, gray);
set(eaxs(eaxs~=0), 'CLim', [600 1200]);

