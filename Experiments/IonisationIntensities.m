%Returns Barrier Supression Intentensities for various elements.
%Levels from
%http://en.wikipedia.org/wiki/Ionization_energies_of_the_elements_%28data_page%29
%which is in turn from Handbook of Chemical Elements.

function IonisationIntensities(element)
c = 299792458; %m/s
qe = 1.60217687e-19; % C
epsilon0 = 8.854187817e-12;
%element = 'He';
heater_Imin = 9e15;
heater_Imax = 5.7e16;
main_I = 2.5e18;
%pulse intensities for ASTRA
switch element
    case {'H', 'Hydrogen'}
        levels = 13.6; %in ev;
        Z = 1;
    case {'He', 'Helium'}
        levels = [24.58741,54.41778]; %in eV.
        Z = 2;
    case {'N', 'Nitrogen'}
        levels = [14.53414,29.6013,47.44924,77.4735,97.8902, ...
            552.0718,667.046]; %in ev
        Z = 7;
    case {'O', 'Oxygen'}
        levels = [13.61806,35.11730,54.9355,77.41353,113.8990,...
            138.1197,739.29,871.4101];
        Z = 8;
    case {'Ne', 'Neon'}
        levels = [21.5646,40.96328,63.45,97.12,126.21,157.93,207.2759,...
            239.0989,1195.8286,1362.1995];
        Z = 10;
    case {'Ar', 'Argon'}
        levels = [15.75962,27.62967,40.74,59.81,75.02,91.009,124.323,...
            143.460,422.45,478.69,538.96,618.26,686.10,755.74,854.77,...
            918.03,4120.8857,4426.2296]; % in ev
        Z = 18;
    case {'Kr' 'Krypton'}
        levels = [13.99961,24.35985,36.950,52.5,64.7,78.5,111.0,125.802,...
            230.85,268.2,308,350,391,447,492,541,592,641,786,833,884,937,...
            998,1051,1151,1205.3,2928,3070,3227,3381];
        Z = 36;
    case {'C' 'Carbon'}
        levels = [11.26030,24.38332,47.8878,64.4939,392.087,489.99334];
        Z = 6;
    otherwise
        disp('Element not (yet) known');
        return;
end

Ibsi = zeros(length(levels)+1, 2);
Ibsi(1,1) = 0;
Ibsi(1,2) = 0;

for i=1:length(levels)
    
    Zeff = i;
    
    Ibsi(i+1,1) = Zeff;
    Ibsi(i+1,2) = c*epsilon0^3*pi^2/(2*qe^6*Zeff^2)*(levels(i)*qe).^4;
    %Intensity required for 'over-the-barrier' ionisation  i.e. E field
    %required for ionisation = E field of laser.
    
end

%Should check against tabulated values in Gibbon
figure(198);
plot(Ibsi(:,1), log10(Ibsi(:,2)*1e-4), '-x');
line([0 Z], [log10(heater_Imin), log10(heater_Imin)], 'Color', 'Red');
line([0 Z], [log10(heater_Imax), log10(heater_Imax)], 'Color', 'Red');
line([0 Z], [log10(main_I), log10(main_I)], 'Color', 'Green');
xlabel('Electron Number');
ylabel('log10(BSI/W/cm^2)');
title(element, 'FontSize', 16);
axis square;


fprintf('Ionisation intensities for %s:\n', element);
for i=1:length(levels)
    fprintf('Electron %i, Eion=%4.2f eV: %1.2e W/cm^2 -> a0 = %1.2f\n', i, levels(i), Ibsi(i+1,2)*1e-4, 0.856*0.8*sqrt(Ibsi(i+1,2)*1e-22));
end
end