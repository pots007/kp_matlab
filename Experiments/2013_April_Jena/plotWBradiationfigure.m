load divmap.mat
dates = {'130424', '130425', '130426', '130429', '130430', '130501'};
d = 2;
%timepres: col1 is shot number, col 2 is backing pressure, col 3 is datenum
timepres = GetTimePressure(dates{d});
timepres = timepres(timepres(:,3)~=0,:);
%timepres = reshape(timepres, length(timepres)/3, 3);
%cd('/Volumes/JENA1/Data/');
filen = dir([dates{d} '/spectrometer/*.FIT']);
%shottime will have datenum of spectrometer files and
%interpolated shot number and pressure
shottime = zeros(length(filen), 3);
for i=1:length(filen)
    shottime(i,1) = filen(i).datenum;
end
shottime(:,2) = interp1(timepres(:,3), timepres(:,1), shottime(:,1));
shottime(:,3) = interp1(timepres(:,3), timepres(:,2), shottime(:,1));
run([dates{d} '/spectrometer/Calibrate_' dates{d} '.m']);
scale = 2; %microns per pixel.
ending = [900,1182,0,772,0,0];
yaxis = linspace(size(Celamp,1),1,size(Celamp,1));
yaxis = scale*(yaxis - ending(d)) + 1000;
savfol = ['../Analysis/spectrometer/' dates{d}];
if (exist(savfol)~=7)
    mkdir(savfol)
end

QE = importdata('SXVR-H35_QE.csv');
QE = QE.data;

%%
figure(890);
set(890, 'WindowStyle', 'docked');
I = fitsread([dates{d} '/spectrometer/IMG744.FIT']);
I = imrotate(I, thet);
if (str2double(filen(i).name(4:length(filen(i).name-4))) < 862 && d==2)
    yaxis = scale*(yaxis - 580) + 1000;
end

qenorm = [QE(:,1) QE(:,2)/max(QE(:,2))];
normfac = interp1(QE(:,1), qenorm(:,2), xaxis);
normfac(normfac<0.1) = 0.1;
I_QEnorm = I./repmat(normfac, size(I,1), 1);
imagesc(yaxis, xaxis, I'./20000);
%pcolor(xaxis, yaxis, I_QEnorm./250000);
%shading flat;
xlabel('Position along nozzle ( \mu m)');
ylabel('Wavelength (nm)');
%title([filen(i).name ', shot~' num2str(shottime(i,2)) ', pres=' num2str(shottime(i,3))]);

set(gcf, 'Colormap', divmap);
set(gca, 'CLim', [0 1], 'XLim', [-500 3100], 'YLim', [360 990])
cb = colorbar;
ylabel(cb, 'Intensity / a.u.');
colormap(divmap)
setAllFonts(890, 22);



