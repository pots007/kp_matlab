% So I think we can figure out what angle of input beam we need to get the
% beam be perpendicular to axis int he middle! Crucially, this will tell us
% the length of the gas as well.

%% Produce a plot of the cone!

d0 = .5; % Hole diameter!

R0 = 6.5;
Rm = 5.8;
R1 = 5;
L0 = 3;

xc = (R1-0.5*d0)/tand(30);
coords = [0 R0; L0 R0; L0 Rm; L0+Rm-0.5*d0 0.5*d0;...
    xc 0.5*d0; 0 R1; 0 R0];

xx = L0+Rm-0.5*d0;
xxp = xx - 1/tand(30)*(R0-0.5*d0);
xl = [xx 0.5*d0; xxp R0];
xl = [xx 0.5*d0; 0 0.5*d0+tand(30)*xx];

figure(56);
plot(coords(:,1), coords(:,2), '-', 'LineWidth', 2);
hold on;
plot(xl(:,1), xl(:,2), '--k', 'LineWidth', 2);
hold off
axis image

set(gca, 'XLim', [-3 10], 'YLim', [0 7]);