% makes a .txt file for eahc shot for the exit energy Gentec
% Needs a shot sheet for times to shots conversion
% Also needs a specific fixed offset for each day as the clocks seem to be
% different.
%% Prepare the shot times data
Gentecfol = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\ExitEnergy\';
shotsheetfile = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\ShotSheets\Shotsheet_20150927.tsv';
ssheetraw = tdfread(shotsheetfile);
% Make a sensible table out of the shotsheet
ssheet = zeros(length(ssheetraw.Var2), 4);
for i=2:length(ssheet)
    % date
    temp = sscanf(ssheetraw.Tracking(i,:), '%f');
    if ~isempty(temp); ssheet(i,1) = temp; end;
    % run
    temp = sscanf(ssheetraw.Var2(i,:), '%f');
    if ~isempty(temp); ssheet(i,2) = temp; end;
    % shot
    temp = sscanf(ssheetraw.Var3(i,:), '%f');
    if ~isempty(temp); ssheet(i,3) = temp; end;
    % shot time
    % A bit shabby, but may work!
    if mean(uint8(ssheetraw.Var4(i,:)))~=32
        temp = datenum(ssheetraw.Var4(i,:), 'HH:MM:SS');
        ssheet(i,4) = temp;
    end
end
ssheet = ssheet(ssheet(:,1)~=0,:);
%% Prepare time offsets for each day
rawfiles = dir([Gentecfol '*.acq']);
for i=13%:length(rawfiles)
    if length(rawfiles(i).name)~=12; continue; end
    gentecdate = sscanf(rawfiles(i).name(1:end-4), '%i');
    switch (gentecdate)
        case 20150819
            dt = datenum('00:02:01', 'HH:MM:SS'); % Ecat is later!
        case 20150821
            dt = datenum('00:02:07', 'HH:MM:SS'); % Ecat is later!
        case 20150822
            dt = datenum('00:02:08', 'HH:MM:SS'); % Ecat is later!
        case 20150825
            dt = datenum('00:02:17', 'HH:MM:SS'); % Ecat is later!
        case 20150826
            dt = datenum('00:02:18', 'HH:MM:SS'); % Ecat is later!
        case 20150827
            dt = datenum('00:02:22', 'HH:MM:SS');
        case 20150828
            dt = datenum('00:02:24', 'HH:MM:SS');
        case 20150829
            dt = datenum('00:02:24', 'HH:MM:SS');
        case 20150903
            dt = datenum('00:02:37', 'HH:MM:SS');
        case 20150907
            dt = datenum('00:02:46', 'HH:MM:SS');
        case 20150908
            dt = datenum('00:02:49', 'HH:MM:SS');
        case 20150909
            dt = datenum('00:02:50', 'HH:MM:SS');
        otherwise
            continue;
    end
    gentecdata = parseGentecFile(rawfiles(i).name, 0);
    ssheetdate = ssheet(gentecdate==ssheet(:,1),:);
    % Open file for debug results
    fid = fopen([rawfiles(i).name(1:end-4) '_data.txt'], 'W');
    for k=1:length(gentecdata)
        %[~,~,~,HH,MM,SS] = datevec(gentecdata(k,1));
        %shottime = datenum(sprintf('%02i:%02i:%02i', HH,MM,SS), 'HH:MM:SS') + dt;
        shottime = gentecdata(k,1) + dt;
        shottime = shottime - floor(shottime);
        % Find the smallest time difference between shot time and ecat time
        ssheetloc = ssheetdate(:,4) - floor(ssheetdate(:,4));
        [mindt, ind] = min(abs(shottime-ssheetloc));
        [~,~,~,HHH,MMM,SSS] = datevec(mindt);
        fprintf('%i:%i:%i\t ind=%i\n', HHH,MMM,SSS, ind); %For debug
        if (3600*HHH+60*MMM+SSS)<3 % If we're close enough
            fprintf(fid, '%ir%03is%03i\t%f\n', ssheetdate(ind,1:3), gentecdata(k,2));
        end
    end
    fclose(fid);
end