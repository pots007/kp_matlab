% Make a list of all the runs that we have Gentec data for so we'd know
% what I can analyse and whatnot.

Gentecfol = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\ExitEnergy\';

flist = dir([Gentecfol '*_data.txt']);

totlines = 0;
for i=1:length(flist)
    dat = getFileText(flist(i).name);
    totlines = totlines+size(dat,1);
end

fprintf('Have a total of %i shots of Gentec data\n', totlines);

it = 1;
allshots = cell(totlines,1);
shotdata = cell(totlines,2);
runnames = cell(totlines,1);
for i=1:length(flist)
    dat = getFileText(flist(i).name);
    for k=1:size(dat,1)
        allshots{it} = dat{k};
        runnames{it} = dat{k}(1:12);
        shotdata{it,1} = dat{k}(1:16);
        shotdata{it,2} = str2double(dat{k}(18:end));        
        it = it+1;
    end
end

runlist = unique(runnames);

fprintf('The runs we have Gentec data for:\n');
for i=1:length(runlist)
    fprintf('%s\n', runlist{i});
end

% And write them all into a single file
fid = fopen(fullfile(Gentecfol, 'ExitEnergies.txt'), 'W');
for i=1:length(allshots)
    fprintf(fid, '%s\n', allshots{i});
end
fclose(fid);

% And create a mat out of them
ExitEnergies = shotdata;
save(fullfile(getDropboxPath, 'MATLAB', 'experiments', '2015_Gemini', 'ExitEnergies'),...
    'ExitEnergies');

    