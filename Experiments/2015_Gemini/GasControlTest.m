
serp = tcpip('192.168.255.1', 502);
set(serp, 'Terminator', 'CR/LF', 'Timeout', 1, 'InputBufferSize', 30000,...
    'ByteOrder', 'BigEndian');
fopen(serp);
tic;
nit = 100;
for i=1:nit
    % flush out useless buffer
    if serp.BytesAvailable~=0 disp(fread(serp, serp.BytesAvailable)); end;
    % read channels
    messag = int16([0 0 6 (int16(256*1)+int16(4)) int16(0020) int16(20)]);
    fwrite(serp, messag, 'int16');
    while serp.BytesAvailable<4; pause(0.0005); end
    reply = fread(serp, serp.BytesAvailable, 'uint8')';
    %turn 1 on;
    messag = int16([0 0 6 (int16(256*1)+int16(5)) (0019+1) 65280]);
    fwrite(serp, messag, 'int16');
    %while serp.BytesAvailable < 4; pause(0.001); end
    %reply = fread(serp, serp.BytesAvailable, 'uint8')';
    %turn 1 off;
    messag = int16([0 0 6 (int16(256*1)+int16(5)) (0019+1) 0]);
    fwrite(serp, messag, 'int16');
    %while serp.BytesAvailable < 4; pause(0.001); end
    %reply = fread(serp, serp.BytesAvailable, 'uint8')';
    %turn 2 on;
    messag = int16([0 0 6 (int16(256*1)+int16(5)) (0019+2) 65280]);
    fwrite(serp, messag, 'int16');
    %while serp.BytesAvailable < 4; pause(0.005); end
    %reply = fread(serp, serp.BytesAvailable, 'uint8')';
    %turn 1 off;
    messag = int16([0 0 6 (int16(256*1)+int16(5)) (0019+2) 0]);
    fwrite(serp, messag, 'int16');
    %while serp.BytesAvailable < 4; pause(0.005); end
    %reply = fread(serp, serp.BytesAvailable, 'uint8')';
end
tottime = toc;
fprintf('%i iterations takes %2.3f s; %2.3f s each\n', nit, tottime,tottime/nit);
fclose(serp);
clear serp;
