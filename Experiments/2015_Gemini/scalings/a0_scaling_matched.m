w0 = 50e-6;
nes = 1e16:1e16:1e20;
lambdap = 2*pi*c./omegapcalc(nes);
a0 = 2;
ap = a0*w0./lambdap;
ap2 = (a0^2*w0^2*omegapcalc(nes).^2./(4*c^2)).^(1/3);
plot(nes, ap, 'k',  nes, ap2, 'r')

%plot(nes, 2*sqrt(ap2).*c./omegapcalc(nes))
