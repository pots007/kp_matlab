

% Trying out these scaling laws Zulf plotted
% But taking etching velocity to be half of Decker value, based on
% Streeter's thesis simulation observations for the Gemini pulse.

% Define energies of the beams:
% Column 1 is for F40, Column 2 is for F20
load(fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', '20150908r008', 'rundata_analysis'));
savfol = fullfile(getDropboxPath, 'MATLAB', 'Experiments', '2015_Gemini', 'scalings');
E0 = [10 10];
tau = [45 45]*1e-15;
w0 = [35 20]*1e-4;

lambda0 = 0.8e-6;
mec2 = 0.511;

I0 = E0./(tau.*w0.^2);

a0 = 0.856*lambda0*1e6*sqrt(I0*1e-18);
eps = a0.^2./sqrt(1+a0.^2);
nes = logspace(17,20,40)';
lambdap = 2*pi*c./omegapcalc(nes)*1e2;
matched_a0(:,1) = a0(1).*(w0(1)./lambdap);
matched_a0(:,2) = a0(2).*(w0(2)./lambdap);
matched_a0_(:,1) = (a0(1).*w0(1)*1e-2.*omegapcalc(nes)./(2*c)).^(2/3);
matched_a0_(:,2) = (a0(2).*w0(2)*1e-2.*omegapcalc(nes)./(2*c)).^(2/3);

linEnGain = 2*ncrit(800)./nes*mec2;
linearEnGain(:,1) = 2*ncrit(800)./nes*eps(1)*mec2;
linearEnGain(:,2) = 2*ncrit(800)./nes*eps(2)*mec2;
threeDGain(:,1) = 2/3*ncrit(800)./nes.*a0(1)*mec2;
threeDGain(:,2) = 2/3*ncrit(800)./nes.*a0(2)*mec2;
threeDGainMatched(:,1) = 2/3*ncrit(800)./nes.*matched_a0(:,1)*mec2;
threeDGainMatched(:,2) = 2/3*ncrit(800)./nes.*matched_a0(:,2)*mec2;
threeDGainMatched_(:,1) = 2/3*ncrit(800)./nes.*matched_a0_(:,1)*mec2;
threeDGainMatched_(:,2) = 2/3*ncrit(800)./nes.*matched_a0_(:,2)*mec2;
% The fast phase velocity ones are basically multiplied by 1.5!
% Try the clamping as well!
hfig = figure(4); clf(hfig); set(hfig, 'Position', [100 100 800 600], 'Color', 'w');
ax1 = gca;
set(ax1, 'NextPlot', 'add', 'YScale', 'log', 'XScale', 'log',...
    'Position', [0.12 0.11 0.85 0.8], 'Box', 'on', 'LineWidth', .75);
titls = {'$\mathcal{E}_{lin}$',...
    '$\mathcal{E}_{3D}$, fixed $a_0$',...
    '$\mathcal{E}_{3Dmatch}, a_m = a_0w_0/\lambda_p$',...
    '$\mathcal{E}_{3Dmatch}, a_m = (a_0w_0\omega_p/2c)^{2/3}$',...
    '$\mathcal{E}_{3Dmatch}, a_m = (a_0w_0\omega_p/2c)^{2/3}$, faster',...
    'Gemini 20150908r008 data'};
h(1) = plot(ax1, nes, linEnGain, '-r');
h(2) = plot(ax1, nes, threeDGain(:,1), ':k');
h(3) = plot(ax1, nes, threeDGainMatched(:,1), '--k');
h(4) = plot(ax1, nes, threeDGainMatched_(:,1), '--b');
h(5) = plot(ax1, nes, 1.5*threeDGainMatched_(:,1), '-.b');
h(6) = plot(ax1, 1e18*D0908r008.pp_unique, cellfun(@mean, D0908r008.pp_vs_ens1), 'vg');
%h(6) = errorbar(ax1, 1e18*D0908r008.pp_unique,...
 %   cellfun(@mean, D0908r008.pp_vs_ens1), cellfun(@std, D0908r008.pp_vs_ens1), 'vg');

legend(h, titls, 'Location', 'NorthEast');
grid on;
set(h, 'LineWidth', 2);
ylabel('Energy gain / MeV');
xlabel('$n_e$ / $\mathrm{cm}^{-3}$');
title(ax1, ['$E_{f/20} = ' num2str(E0(2)) '$J, $E_{f/40} = ' num2str(E0(1)) '$J, $\tau = ' num2str(mean(tau*1e15)) '$ fs']);
set(gca, 'YLim', [1e2 1e5], 'XLim', [1e17 1e19]);
make_latex(hfig); setAllFonts(hfig,18);
%export_fig(fullfile(savfol, 'Scalings_ZN_fast+data'), '-pdf', '-nocrop', hfig);
%% 

% Now for the turning around part:
L_pd = 2.4/2*omega0^2./omegapcalc(nes).^2.*c*tau(1);
% Changing the factor just here is cheating a lot as using a slower ecthing 
% velocity lengthens depletion length! But physically, as
% Streeter points out in his thesis, it does depend on which point is
% chosen as "depleted". This may not be too unfair, though, coz all we're
% saying is that the laser stop driving a wake BEFORE all its FWHM energy
% is lost... Kind of OK, right?

L_d1 = 4/3*sqrt(matched_a0(:,1))*omega0^2*c./omegapcalc(nes).^3;
L_d2 = 2*sqrt(matched_a0_(:,1))*omega0^2*c./omegapcalc(nes).^3;
Lclamp1 = (L_pd./L_d1).^2; Lclamp1(Lclamp1>1) = 1;
Lclamp2 = (L_pd./L_d2).^2; Lclamp2(Lclamp2>1) = 1;

hfig = figure(4); clf(hfig); set(hfig, 'Position', [100 100 800 600], 'Color', 'w');
ax1 = gca;
set(ax1, 'NextPlot', 'add', 'YScale', 'log', 'XScale', 'log',...
    'Position', [0.12 0.11 0.85 0.8], 'Box', 'on', 'LineWidth', .75);

h(1) = plot(ax1, nes, linEnGain, '-r');
h(2) = plot(ax1, nes, threeDGain(:,1), ':k');
h(3) = plot(ax1, nes, threeDGainMatched(:,1), '--k');
h(4) = plot(ax1, nes, threeDGainMatched_(:,1), '--b');
h(5) = plot(ax1, nes, Lclamp2.^4.*1.5.*threeDGainMatched_(:,1), '-.b');
h(6) = plot(ax1, 1e18*D0908r008.pp_unique, cellfun(@mean, D0908r008.pp_vs_ens1), 'vg');
%h(6) = errorbar(ax1, 1e18*D0908r008.pp_unique,...
 %   cellfun(@mean, D0908r008.pp_vs_ens1), cellfun(@std, D0908r008.pp_vs_ens1), 'vg');

legend(h, titls, 'Location', 'NorthEast');
grid on;
set(h, 'LineWidth', 2);
ylabel('Energy gain / MeV');
xlabel('$n_e$ / $\mathrm{cm}^{-3}$');
title(ax1, ['$E_{f/20} = ' num2str(E0(2)) '$J, $E_{f/40} = ' num2str(E0(1)) '$J, $\tau = ' num2str(mean(tau*1e15)) '$ fs']);
set(gca, 'YLim', [1e2 1e5], 'XLim', [1e17 1e19]);
make_latex(hfig); setAllFonts(hfig,18);
%export_fig(fullfile(savfol, 'Scalings_ZN_fast+data'), '-pdf', '-nocrop', hfig);
