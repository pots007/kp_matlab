% matched_Scalings2.m

% This is a simple model to look at how maximum energy changes with plasma
% density, for a fixed total length

load(fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', '20150908r008', 'rundata_analysis'));
savfol = fullfile(getDropboxPath, 'MATLAB', 'Experiments', '2015_Gemini', 'scalings');

nes = logspace(16,19,100);
Lx = 0.015;

a0 = 2; w0 = 45e-6;
tau = 50e-15; 
lambdap = 2*pi*c./omegapcalc(nes);
matched_a0 = a0.*(w0./lambdap);
matched_a0_ = (a0.*w0.*omegapcalc(nes)./(2*c)).^(2/3);
a0s = [ones(size(nes)).*a0; matched_a0; matched_a0_];

hfig = figure(8); clf(hfig);
set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add');
hh = [];
for i=1:3
    Ld = 4/3*omega0^2*c.*sqrt(a0s(i,:))./omegapcalc(nes).^3;
    E_av = sqrt(a0s(i,:)).*me*c.*omegapcalc(nes)/qe.*(1 - 0.5*Lx./Ld);
    E_av(E_av<0) = 0;
    E_part = E_av.*Lx; % This is implicitly multiplied by qe so is in units of eV
    hh(i) = plot(nes, E_part*1e-9, '-', 'LineWidth', 2);
end
hh(end+1) = plot(1e18*D0908r008.pp_unique, 1e-3*cellfun(@mean, D0908r008.pp_vs_ens1), 'vg');
set(gca, 'XScale', 'log', 'Box', 'on', 'LineWidth', 1)
legend(hh, {'Fixed $a_0$',...
    '$a_m = a_0w_0/\lambda_p$',...
    '$a_m = (a_0w_0\omega_p/2c)^{2/3}$',...
    'Gemini 20150908r008 data'},...
    'Location', 'Northoutside');
xlabel('Plasma density / $\mathrm{cm}^{-3}$');
ylabel('Energy gain / GeV');
grid on;
text(1.5e17, .2, ['$L_{acc} = ' num2str(Lx*1e3) ' mm$']);
make_latex(hfig); setAllFonts(hfig, 18);