nes = logspace(17,19,100);
Lx = 0.01;

a0 = 2; w0 = 35e-6;
tau = 45e-15;
% Changing the factor just below here is cheating a lot! But physically, as
% Streeter points out in his thesis, it does depend on which point is
% chosen as "depleted". This may not be too unfair, though, coz all we're
% saying is that the laser stop driving a wake BEFORE all its FWHM energy
% is lost... Kind of OK, right?
L_pd = 2/2*omega0^2./omegapcalc(nes).^2.*c*tau;
lambdap = 2*pi*c./omegapcalc(nes);
a0_m1 = a0.*(w0./lambdap);
a0_m2 = (a0.*w0.*omegapcalc(nes)./(2*c)).^(2/3);

L_d = 2*sqrt(a0)*omega0^2*c./omegapcalc(nes).^3;
L_d1 = 2*sqrt(a0_m1)*omega0^2*c./omegapcalc(nes).^3;
L_d2 = 2*sqrt(a0_m2)*omega0^2*c./omegapcalc(nes).^3;

hfig = figure(9); clf(hfig);
set(gca, 'NextPlot', 'add', 'XScale', 'log');
plot(nes, L_pd, '-k');
plot(nes, L_d, '--k', nes, L_d1, '--b', nes, L_d2, '--r');