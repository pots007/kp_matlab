% matched_Scalings_L.m
%
% This is a simple model to look at how maximum energy changes with length
% for a given ne, taking into account depletion length

load(fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', '20150908r008', 'rundata_analysis'));
savfol = fullfile(getDropboxPath, 'MATLAB', 'Experiments', '2015_Gemini', 'scalings');

nes = 3e18;
Lx = (0.1:0.1:30)*1e-3;

a0 = 5; w0 = 45e-6;
tau = 70e-15; 
lambdap = 2*pi*c./omegapcalc(nes);
matched_a0 = a0.*(w0./lambdap);
matched_a0_ = (a0.*w0.*omegapcalc(nes)./(2*c)).^(2/3);
a0s = [ones(size(nes)).*a0; matched_a0; matched_a0_];

hfig = figure(9); clf(hfig);
set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add');
hh = [];
Efun = @(E0, Ld, x) E0 - E0./Ld.*x;
for i=1:3
    Ldph = 4/3*omega0^2*c.*sqrt(a0s(i))./omegapcalc(nes).^3;
    Ldpl = c*tau*ncrit(800)./nes;
    % This sets the total acceleration length to the shorter of depletion
    % length and cell length: if the laser is gone, we're done anyway!
    Llim = min([Ldpl*ones(size(Lx)); Lx]);
    %Llim = Lx*ones(size(Ldph));
    E_peak = me*c*omegapcalc(nes)/qe.*sqrt(a0s(i,:));
    fun = @(x) Efun(E_peak, Ldph, x);
    for nn = 1:length(Lx)
        E_part(nn) = integral(@(x) Efun(E_peak, Ldph, x), 0, Llim(nn));
    end
    E_part(E_part<0) = 0;
    hh(i) = plot(Lx*1e3, E_part*1e-9, '-', 'LineWidth', 2);
end
%hh(end+1) = plot(1e18*D0908r008.pp_unique, 1e-3*cellfun(@mean, D0908r008.pp_vs_ens1), 'vg');
set(gca, 'XScale', 'lin', 'Box', 'on', 'LineWidth', 1)
legend(hh, {'Fixed $a_0$',...
    '$a_m = a_0w_0/\lambda_p$',...
    '$a_m = (a_0w_0\omega_p/2c)^{2/3}$'},...
    'Location', 'Northoutside'); %'Gemini 20150908r008 data'},...
xlabel('Plasma length / mm');
ylabel('Energy gain / GeV');
grid on;
text(20, .2, ['$n_e = ' num2str(nes*1e-18) '\times10^{18}\,\mathrm{cm}^{-3}$']);
make_latex(hfig); setAllFonts(hfig, 18);