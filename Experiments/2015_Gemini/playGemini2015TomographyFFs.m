% Play with FF spots form tomography run

focalspotData = zeros(201,201,length(filen));

for i=1:length(filen)
    fprintf('Looking into file %i\n', i);
    ll = getGemini2015laserFF(20150916, 5, i);
    if ~isempty(ll); focalspotData(:,:,i) = double(ll); end;
end

%% Now that we have the data, try to make various analysis

FWHM_ens = zeros(size(focalspotData,3),1);
FWHM_Clength = zeros(size(focalspotData,3),1);

for i=1:size(focalspotData,3)
    tempim = focalspotData(:,:,i);
    tempim = tempim - mean(mean(tempim(1:50,1:50)));
    FWHM_ens(i) = sum(sum(tempim(tempim>0.5*max(tempim(:)))))/sum(tempim(:));
    C = contourc(tempim, 0.5*max(tempim(:))*[1 1]);
    FWHM_Clength(i) = size(C,2);
end

%% And the plot a lot of images

nx = 15; ny = 10;
locs = GetFigureArray(nx, ny, 0.01, 0.01, 'across');
[~, sort_inds_ens] = sort(FWHM_ens); sort_inds_ens = flipud(sort_inds_ens);
[~, sort_inds_Cs] = sort(FWHM_Clength);

sinds = sort_inds_Cs; 
figure(6); clf(6);
set(6, 'Position', [10 100 2100 1200]);
colormap(colmap.jet_white);
for i=1:nx*ny
    ax(i) = axes('Parent', 6, 'Position', locs(:,i));
    imagesc(focalspotData(:,:,sinds(i)), 'Parent', ax(i));
    text(20,20,num2str(sinds(i)));
    text(20,40,num2str(FWHM_Clength(sinds(i))));
    text(20,60,num2str(FWHM_ens(sinds(i))*100, '%2.1f'));
end