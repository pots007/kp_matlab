yax_ = 0.5e-3 - dd.yax*1e-3;
xo = 0.5e-3;
logm1 = yax_<xo-0.15e-3 & yax_>xo-0.5e-3;
l1 = fit(yax_(logm1)', dd.dens(logm1, 1), 'exp1');

logm2 = yax_>xo-0.17e-3 & yax_ <xo-0.1e-3;
l2 = fit(yax_(logm2)', dd.dens(logm2, 1), 'poly4');

clf;
set(gca, 'NextPlot', 'add');
plot(yax_, dd.dens(:,1));

%plot(l1);
%plot(l2);

x2 = xo-(0.1:0.001:0.158)*1e-3; 
vv2 = l2(x2);
x1 = xo-(0.158:0.001:0.5)*1e-3;
vv1 = l1(x1);
x3 = xo-(0:0.01:0.1)*1e-3;
vv3 = ones(size(x3))*2e18;
plot(x1, vv1, x2, vv2, x3, vv3);

yax_m = yax_*1e6;
l3 = polyfit(yax_m', dd.dens(:,1), 17);
plot(yax_, polyval(l3, yax_m), '--g');

%% try the tanh approach

cla; set(gca, 'NextPlot', 'add');
plot(yax_m, dd.dens(:,1)*0.97, '-k');
logm = yax_m>0;
denst_1 = 1e18*(1+tanh((yax_m-313)/55));
denst_2 = 1e18*(1-tanh((yax_m-313)/55));
dentest = @(h,x1,x2,w1,w2,x) h*(2+tanh((x-x1)/w1) + tanh((x-x2)/w2));
fittry = fit(yax_m(logm)', dd.dens(logm,1)*0.97, dentest,...
    'StartPoint', [5e17, 300, 300, 55, 65]);

mantry = dentest(5.033e17, 330, 295, 25, 95, yax_m);
plot(yax_m, mantry, '-r')
plot(yax_m, fittry(yax_m), '-g');