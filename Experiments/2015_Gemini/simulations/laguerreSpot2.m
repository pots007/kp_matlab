%% FIrst, take the compound image:

%focalSpotTotalSum;
%%
spotwidth = ax_calib*size(spotim,1);
spotax = linspace(-0.5*spotwidth, 0.5*spotwidth, size(spotim,1));
spotline = spotim(round(0.5*size(spotim,1)),:);
% Now for the crucial part: all laguerres are symmetrical about zero. So
% let's use one of the sides, or average
av_sides = 0;
if av_sides
    % If averaging:
    temp = round(0.5*size(spotim,1));
    temp = mean([spotline(1:temp); fliplr(spotline(temp:end))],1);
    spotline_ = [temp(1:end-1) fliplr(temp)];
else
    temp = round(0.5*size(spotim,1));
    temp = spotline(1:temp);
    %temp = fliplr(spotline(temp:end));
    spotline_ = [temp(1:end-1) fliplr(temp)];
end

figure(8);
plot(spotax,spotline_);

%% Let's also fit an ellipse to this to find it's w0
C = contourc(spotim, exp(-1)*[1 1]);
x = C(1,:);
y = C(2,:);
lengthx = length(x);
k = 1;
%Remove points from your contour that are not actually part of the main
%contour - this is necessary when looking at noisy data
while k < lengthx
    if(abs(x(k) - mean(x)) > 2*std(x) || abs(y(k) - mean(y)) > 2*std(y))
        x(k) = [];
        y(k) = [];
        lengthx = lengthx - 1;
    end
    k = k+1;
end
ell = fit_ellipse(x,y);
% At the moment, looking on the longer axis, so:
w0 = ell.a*ax_calib;

%% The fitting part
% Radial axis ought ot stay the same.
%r = -150:0.5:150;
r = spotax;
%w0 = 29;
X = r.^2/w0^2;
XX = 2*X;

polys = getLaguerreMatrix(X, XX, 5);

coeffs = polys'\spotline_';
% Round the coefficients to 3 decimal points - easier to insert into EPOCH
roundcoeffs = round(coeffs*1e3)/1e3;
polycoeffs = polyfit(spotax, spotline_, 9);

% And let's see how we did
hfig = figure(8); clf(hfig);
set(gca, 'NextPlot', 'add');
logm = 1:5:length(spotax);
hdat = plot(spotax(logm), spotline_(logm), 'or');
hfit = plot(spotax, polys'*roundcoeffs, '-k');
hpoly = plot(spotax, polyval(polycoeffs, spotax), '--g');

%% And finally, normalise eveything to have correct amount of energy
% Assume a Gaussian temporal profile (for now)
% int_-inf^inf exp(-ax^2) = sqrt(pi/a), so factor from temporal profile is
timefwhm = 45e-15;
tau = timefwhm/(sqrt(2*log(2)));
taufac = sqrt(pi*tau^2);
w1 = w0*0.5;
r2 = (0:.5:200)*1e-6;
X2 = r2.^2/(w0*1e-6)^2;
X2_ = r2.^2/(w1*1e-6)^2;
finalspotF40 = abs(getLaguerreMatrix(X2, 2*X2, 5)'*roundcoeffs);
finalspotF20 = abs(getLaguerreMatrix(X2_, 2*X2_, 5)'*roundcoeffs);
spatialfac = trapz(r2, r2.*finalspotF40');
plot(r2*1e6, finalspotF40, '-m');
plot(r2*1e6, finalspotF20, '-k');
% So total energy atm is 
totE = taufac*spatialfac;
scalefac = 10/totE;
