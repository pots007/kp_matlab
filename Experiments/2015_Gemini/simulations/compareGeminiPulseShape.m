% Try to compare the wake speeds for Gaussian pulse and the slightly weird
% one.

datfol = '/Volumes/KRISMOOSE3/Gem2015sim/';
simfols = {'Gem2015PulseTesta', 'Gem2015PulseTestc'};
savfol = fullfile(datfol, 'ComparePulseShapes', 'a_vs_c');

%% First first off, compare the total energies:

fNO = 1;
dataf = GetDataSDFchoose(fullfile(datfol, simfols{1}, 'data', sprintf('normal%04i.sdf', fNO)), 'ez');
totEn(1) = sum(dataf.Electric_Field.Ez.data(:).^2);
dataf = GetDataSDFchoose(fullfile(datfol, simfols{2}, 'data', sprintf('normal%04i.sdf', fNO)), 'ez');
totEn(2) = sum(dataf.Electric_Field.Ez.data(:).^2);

fprintf('In file normal%04i.sdf, the realistic pulse has %2.3f times the energy of Gaussian sim\n',...
    fNO, totEn(2)/totEn(1));

%% First off, plot the laser evolutions - straight off just comparing them.

nFiles = max([length(dir(fullfile(datfol, simfols{1}, 'data', 'normal*.sdf'))),...
    length(dir(fullfile(datfol, simfols{2}, 'data', 'normal*.sdf')))]);

sfig = figure(45); clf(sfig);
set(sfig, 'Color', 'w', 'Position', [100 100 1280 720]);
locs = GetFigureArray(4, 2, [0.05 0.02 0.05 0.1], 0.06, 'across');
colormap(jet_white(256));
ax = []; 
for i=1:nFiles
    fprintf('Plotting for file %i', i);
    axit = 1;
    clf(sfig);
    for ss = 1:2
        fname = fullfile(datfol, simfols{ss}, 'data', sprintf('normal%04i.sdf', i));
        ax(ss,1) = axes('Parent', sfig, 'Position', locs(:,axit)); axit = axit+1;
        tt = EVPlota0withphase(ax(ss,1), fname, []);
        fprintf('.');
        ax(ss,1) = tt.ax(1);
        set(findobj(ax(ss,1), 'Type', 'text'), 'Position', [0.05 0.93]);
        ax(ss,2) = axes('Parent', sfig, 'Position', locs(:,axit)); axit = axit+1;
        EVPlotDensityelectron(ax(ss,2), fname, []);
        fprintf('.');
        ax(ss,3) = axes('Parent', sfig, 'Position', locs(:,axit)); axit = axit+1;
        plotDist_fn(fname, 'electron', 'x_px');
        title(ax(ss,3), '');
        fprintf('.');
        ax(ss,4) = axes('Parent', sfig, 'Position', locs(:,axit)); axit = axit+1;
        EVPlotElectronSpectrum(ax(ss,4), fname, []);
        fprintf('.');
    end
    fprintf(' Done\n');
    % Sensible limits
    set(ax(:,1), 'YLim', [0 4.5]);
    set(ax(:,2), 'CLim', [0 0.025], 'YLim', [-50 50]);
    set(ax(:,4), 'YLim', [0 1e12], 'XLim', [0 max(max(cell2mat(get(ax(:,4), 'XLim'))))]);
    % And quick reminders
    text(-450, 2, 'Gaussian pulse', 'Parent', ax(1,1), 'Rotation', 90);
    text(-450, 2, 'Realistic pulse', 'Parent', ax(2,1), 'Rotation', 90);
    setAllFonts(sfig, 13);
    export_fig(fullfile(savfol, sprintf('normal%04i',i)), '-png', '-nocrop', sfig);
end

%% And now try to measure the speed of the pulse

% Look for the place where the accelerating field is zero - between the max
% and min of Ex.
hfig = figure(77); clf(hfig); ax1 = gca;
set(ax1, 'Nextplot', 'add')
hh = [];
for k=1:2
    load(fullfile(datfol, simfols{k}, 'Ex', 'longitudinal.mat'));
    logm = ~cellfun(@isempty, longitudinal(:,1), 'Uniformoutput', 1);
    longdat = longitudinal(logm, :);
    axnes = cell2mat(longdat(:,2))';
    for i=1:size(axnes,2); axnes(:,i) = smooth(axnes(:,i), 25); end
    [~, inds_front] = max(axnes(2500:end,:), [],1);
    [~, inds_back] = min(axnes(1000:end,:), [],1);
    inds_mid = zeros(size(inds_back));
    for i = 1:size(axnes,2)
        line_sec = axnes(1000+inds_back(i):2500+inds_front(i),i);
        %plot(line_sec); drawnow;
        [~,inds] = min(abs(line_sec));
        inds_mid(i) = inds + 1000 + inds_back(i);
    end
    figure(90000); clf;
    set(gca, 'NextPlot', 'add');
    imagesc(axnes);
    plot(1:size(axnes,2), 2500+smooth(inds_front), 'x');
    % And compare the speeds
    x_pos = cell2mat(longdat(:,5))*c;
    % This is back of the bubble
    y_pos0 = longdat{1,3}(2500+round(smooth(inds_front))) - 2.2e-5;
    %y_pos = longdat{1,3}(2500+inds_front) - 2.2e-5;
    % This one is using middle of the bubble
    y_pos = longdat{1,3}(round(smooth(inds_mid)))  - .7e-5;
    hh(k) = plot(ax1, x_pos, y_pos, '-o');
    yyyy{k} = [x_pos/c; y_pos; y_pos0];
end
nu_etch = 2e18/ncrit(800);
hh(3) = plot(ax1, [0 max(x_pos)], 2e-5 + [0 -1*max(x_pos)*nu_etch], '--k');

set(ax1, 'YDir', 'reverse', 'Box', 'on');
ylabel(ax1,'$z-v_gt$ / micron');
xlabel(ax1, 'x / mm');
legend(hh, {'Gaussian pulse', 'Real pulse', '$\nu_{etch}$'}, 'Location', 'nw');
make_latex(hfig); setAllFonts(hfig, 16);

%% Try plotting the same thing in terms of gamma

% Look for the place where the accelerating field is zero - between the max
% and min of Ex.
hfig = figure(78); clf(hfig); ax1 = gca;
set(ax1, 'Nextplot', 'add')
hh = [];
for k=1:2
    load(fullfile(datfol, simfols{k}, 'Ex', 'longitudinal.mat'));
    logm = ~cellfun(@isempty, longitudinal(:,1), 'Uniformoutput', 1);
    longdat = longitudinal(logm, :);
    axnes = cell2mat(longdat(:,2))';
    for i=1:size(axnes,2); axnes(:,i) = smooth(axnes(:,i), 25); end
    [~, inds_front] = max(axnes(2500:end,:), [],1);
    [~, inds_back] = min(axnes(1000:end,:), [],1);
    inds_mid = zeros(size(inds_back));
    for i = 1:size(axnes,2)
        line_sec = axnes(1000+inds_back(i):2500+inds_front(i),i);
        %plot(line_sec); drawnow;
        [~,inds] = min(abs(line_sec));
        inds_mid(i) = inds + 1000 + inds_back(i);
    end
    figure(90000); clf;
    set(gca, 'NextPlot', 'add');
    imagesc(axnes);
    plot(1:size(axnes,2), 2500+smooth(inds_front), 'x');
    % And compare the speeds
    x_pos = cell2mat(longdat(:,5))*c;
    y_pos = longdat{1,3}(2500+round(smooth(inds_front))) - 2.2e-5;
    %y_pos = longdat{1,3}(2500+inds_front) - 2.2e-5;
    y_pos = longdat{1,3}(round(smooth(inds_mid)))  - .7e-5;
    vbub = gradient(y_pos, mean(diff(x_pos/c)));
    vg = (1 - 0.5*2e18/ncrit(800));
    gammas = sqrt(1./(1 - (vg + vbub/c).^2));
    %gammas(real(gammas)==0) = nan;
    hh(k) = plot(ax1, x_pos, abs(gammas), '-o');
end
nu_etch = 2e18/ncrit(800);
%hh(3) = plot(ax1, [0 max(x_pos)], 2e-5 + [0 -1*max(x_pos)*nu_etch], '--k');
hh(3) = plot(ax1, [0 max(x_pos)], [1 1]*sqrt(ncrit(800)/2e18), '--k');
hh(4) = plot(ax1, [0 max(x_pos)], [1 1]*sqrt(ncrit(800)/(2e18*3)), ':k');

set(ax1, 'YDir', 'normal', 'Box', 'on', 'YLim', [0 100]);
ylabel(ax1,'$\gamma$');
xlabel(ax1, 'x / mm');
legend(hh, {'Gaussian pulse', 'Real pulse', '$\omega_L/\omega_p$','$\omega_L/(\sqrt{3}\lambda_p)$'}, 'Location', 'nw');
make_latex(hfig); setAllFonts(hfig, 16);