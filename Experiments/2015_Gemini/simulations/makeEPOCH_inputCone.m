% Try to fit a function to the density profile from FlashCone4d

ff = getFLASHData(fullfile(getBoxSync, 'FLASH', 'FlashCone4d', 'gascellcone_hdf5_chk_0050'), 1000);

%% Do some work!

hfig = figure(66); clf(hfig);
set(gca, 'nextPlot', 'add', 'box', 'on');

nav = 5;
lineout = mean(ff.dens(10:end-1,1:nav),2);
yax = linspace(ff.yaxis(1), ff.yaxis(2), size(ff.dens,1)-10);
plot(yax, lineout);

densfunc = @(a,b,c,d,e,f,g,h,x) (x>a).*(b.*exp(-(x-a)./c) + g.*exp(-(x-a)./h)) + (x<=a).*d.*tanh(1-(x-e)/f);

fitf = fit(yax', lineout, densfunc, 'StartPoint', [0.18 1e18 0.05 2.5e18 0.1 0.1 1e17 0.3]);
plot(yax, densfunc(0.19, 1.e18, 0.05, 2.5e18, 0.1, 0.1, 1e17, 0.3, yax), '--r')
plot(yax, fitf(yax), '--g');

%% Try a different route...
hfig = figure(66); clf(hfig);
set(gca, 'nextPlot', 'add', 'box', 'on');


yax2 = (yax*1e0);
lineout2 = fliplr(log(lineout'));
fitp = polyfit(yax, lineout2, 13);
fitp_sim = round(fitp, 14, 'significant');
plot(yax2, lineout2, '-k');
plot(yax2, polyval(fitp, yax2), '--m');
plot(yax2, polyval(fitp_sim, yax2), '--g');
%
clf;
densfit = exp(polyval(fitp_sim, yax2));
densfit(densfit>2.45e18) = 2.45e18;
simstr = [];
testax = (0:0.01:2)*1e0; 
testdens = zeros(1,length(testax));
for i=1:length(fitp_sim)
    simstr = sprintf('%s%+1.14e*xm^%i', simstr, fitp_sim(i), length(fitp_sim)-i);
    testdens = testdens + fitp_sim(i)*testax.^(length(fitp_sim)-i);
end
testdens_ = exp(testdens); testdens_(testdens_>2.45e18) = 2.45e18;
%set(gca, 'Nextplot', 'add');
plot(yax2, fliplr(lineout'), yax2, densfit, ':r');
plot(testax, (testdens_), '--g');