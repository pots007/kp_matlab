% Just to m ake sure we know what the Gemini pulse looked like...

datfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'SPODER', '20150827');
%fdat = load(fullfile(datfol, '20150827_190431_SEAFSPIDER_dazzler_mormalsetting'));
fdat = load(fullfile(datfol, '20150827_191357_SEAFSPIDER_dazzler_mormalsetting_spiderrealigned'));
%plot(fdat.seaf.buffer.lambda, fdat.seaf.buffer.redPhi, '-');
logm = 550:730;
sig_t = fdat.seaf.rec1d.t(logm);
sig_v = abs(fdat.seaf.rec1d.Et(logm));
w = sig_t(end) - sig_t(1);
figure(991); clf(991);
plot(-sig_t, sig_v, 'o');

hold on;
p = polyfit(sig_t, sig_v, 17);
p_round = round(p, 7, 'significant');
plot(-sig_t, polyval(p_round, sig_t), '-r', 'LineWidth', 2);
plot(sig_t, exp(-sig_t.^2./38^2), '--g');
hold off
p_str = [];
for i=1:length(p)
    p_str = [p_str num2str(p(i), '%+2.7e') '*xm^' num2str(length(p)-i)];
end
p_str
xlabel('Time / fs');ylabel('Amplitude / a.u.');
setAllFonts(gcf, 16); make_latex(gcf);
