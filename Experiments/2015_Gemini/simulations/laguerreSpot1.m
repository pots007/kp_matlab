% Play around with zeroth and first order Laguerre thingies

a0 = 3.85;
a_1 = [-3.85 -.5:0.5:1]; % Underscore index is for a^(i), denoting order of poly
a_0 = sqrt(a0^2-a_1.^2);

r = -120:1:120;
w0 = 19;
zR = pi*w0^2/0.8;

as = zeros(length(r), length(a_0));
for i=1:length(a_1)
    i
    a0 = a_0(i)*exp(-1*r.^2/w0^2).*laguerreL(0,2*r.^2./w0^2);
    a1 = a_1(i)*exp(-1*r.^2/w0^2).*laguerreL(1,2*r.^2./w0^2);
    a2 = 0;%-0.5*exp(-1*r.^2/w0^2).*laguerreL(2,2*r.^2./w0^2);
    as(:,i) = abs(a0.^1+a1.^1+a2.^1);
end

hfig = figure(5);  hfig.Color = 'w';

hh = plot(r, as);
legend(hh, num2str(a_1'), 'Location', 'best');
set(gca, 'XLim', [-110 110], 'XTick', linspace(-110, 110, 11))
xlabel('x / $\mu m$'); ylabel('$a_0$');
make_latex(hfig);
export_fig(fullfile(getDropboxPath, 'MATLAB', 'simulations', 'Gem2015Simulations', ...
    'CompareToPaper'), '-pdf', '-nocrop', hfig);
%% Rudimentary fit try:

NN = [exp(-1*r.^2/w0^2).*laguerreL(0,2*r.^2./w0^2);exp(-1*r.^2/w0^2).*laguerreL(1,2*r.^2./w0^2);exp(-1*r.^2/w0^2).*laguerreL(2,2*r.^2./w0^2)];