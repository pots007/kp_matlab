% Compare the simulations of vacuum propagation and the Flash gas ramp

% This was done with a 114 TW laser

n_crit = ncrit(800)*17.4e9/114e12;

datfol = '/Volumes/KRISMOOSE3/Gem2015Cone/';
fols = {'ConeGas', 'ConeVac'};
fne = load(fullfile(datfol, 'ConeGas', 'simulated_ne'));
ne_x = cell2mat(fne.nedata(:,1));
ne_ne = cell2mat(fne.nedata(:,2));


axpos = [0.14 0.18 0.72 0.72];
hfig = figure(35); clf(hfig);
ax1 = gca;
set(ax1, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2, 'Position', axpos, 'Color', 'none');
ax2 = axes('Parent', hfig, 'Position', axpos, 'Color', 'none', 'YAxisLocation', 'right',...
    'Box', 'on', 'NextPlot', 'add');
plot(ax2, ne_x, smooth(ne_ne,20), '-k')
plot([0 25], n_crit*[1 1], '--k');
hPcrit = text(19, n_crit, '$P = P_c$', 'Parent', ax2, 'VerticalAlignment', 'bottom');
set(ax2, 'YAxisLocation', 'right', 'Color', 'none');
ylabel('$n_e / \mathrm{cm}^{-3}$');
xlabel('x / mm');


% Vacuum intensity and spot size
f = load(fullfile(datfol, fols{2}, 'Ez', 'transverse'));
logm = cellfun(@isempty, f.transverse(:,1), 'UniformOutput', 1);
vac_data = f.transverse(~logm,:);
w0_vac = cell2mat(vac_data(:,6));
x_vac = cell2mat(vac_data(:,3)');
I0_vac = cell2mat(vac_data(:,2))*0.5*epsilon0*c*1e-4;
I0_vacmax = max(I0_vac,[],2);
x_vac = max(x_vac, [], 1);
% Gas prop intensity and spot size
f = load(fullfile(datfol, fols{1}, 'Ez', 'transverse'));
logm = cellfun(@isempty, f.transverse(:,1), 'UniformOutput', 1);
gas_data = f.transverse(~logm,:);
w0_gas = cell2mat(gas_data(:,6));
x_gas = cell2mat(gas_data(:,3)');
I0_gas = cell2mat(gas_data(:,2))*0.5*epsilon0*c*1e-4;
I0_gasmax = max(I0_gas,[],2);
x_gas = max(x_gas, [], 1);
h1 = plot(ax1, x_vac*1e3, I0_vacmax, '-b');
h2 = plot(ax1, x_gas*1e3, I0_gasmax, '--r');
linkaxes([ax1 ax2], 'x');
set([h1 h2], 'LineWidth', 2);
ylabel(ax1, 'I / $\mathrm{Wcm}^{-2}$');

make_latex(hfig); setAllFonts(hfig, 20);
%export_fig(fullfile(getDropboxPath, 'MATLAB', 'Experiments', '2015_Gemini', 'simulations', 'VacGasSim'), '-nocrop', '-pdf', hfig);
%%  

cla(ax1);
h1 = plot(ax1, x_vac*1e3, w0_vac*mean(diff(x_vac))*sqrt(2)*100, '-b');
h2 = plot(ax1, x_gas*1e3, w0_gas*mean(diff(x_gas))*sqrt(2)*100, '--r');
set(hPcrit, 'Position', [5 n_crit 0]);
linkaxes([ax1 ax2], 'x');
set([h1 h2], 'LineWidth', 2);
ylabel(ax1, '$w_0$ / micron');
%export_fig(fullfile(getDropboxPath, 'MATLAB', 'Experiments', '2015_Gemini', 'simulations', 'VacGasSim2'), '-nocrop', '-pdf', hfig);

%%
cla(ax1);
spotratio = w0_vac*mean(diff(x_vac))./(w0_gas*mean(diff(x_gas)));
h1 = plot(ax1, x_vac*1e3, spotratio, '-b');
[~,ind] = min(abs(spotratio-1.005));
plot(ax1, 1e3*x_vac(ind)*[1 1], [0 100], '--b');
htext = text(1e3*x_vac(ind), 1.2, '$w_g/w_0 = 1 \%$', 'Rotation', 90, 'VerticalAlignment', 'bottom', 'Parent', ax1);
set(hPcrit, 'Position', [5 n_crit 0]);
linkaxes([ax1 ax2], 'x');
set(h1, 'LineWidth', 2);
ylabel(ax1, '$w_{vac}/w_{gas}$');
make_latex(hfig); setAllFonts(hfig, 20);
set(ax1, 'YLim', [0.99 2.05], 'YColor', 'b');
export_fig(fullfile(getDropboxPath, 'MATLAB', 'Experiments', '2015_Gemini', 'simulations', 'VacGasSim3'), '-nocrop', '-pdf', hfig);
