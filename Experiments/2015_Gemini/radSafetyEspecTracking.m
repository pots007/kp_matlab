if ismac 
    load('/Users/kristjanpoder/Dropbox/ICT_release/version1.0/Gem2015_rad1.mat');
elseif ispc
    load('E:\Kristjan\Documents\Uni\Dropbox\ICT_release\version1.0\Gem2015_rad1.mat');
end
figure(991);
hs = plot(E_e_MeV, l, 'x');
hold on;
plot([0 E_e_MeV(end)], [2195 2195], '--k');
hold off;
set(hs, 'LineWidth', 2)
%set(gca, 'XLim', [0 150]);
legend(hs, {'-5 mrad', '0 mrad', '5 mrad'}, 'Location', 'Northeast');
xlabel('Electron energy / MeV');
ylabel('Distance above TCC / mm');
title('Electron deflections with proposed magnet')
setAllFonts(991,20);

%export_fig([getDropboxPath '/2015Gemini/RadProtection/electron_trajectories'],...
%   '-nocrop', '-transparent', '-pdf', 991);

% And now the data for RobC
fid = fopen(fullfile(getDropboxPath, '2015Gemini', 'RadProtection', 'electron_hit_locations.txt'), 'w');
for i=1:length(l)
    fprintf(fid,'%f\t%f\n\r', E_e_MeV(i), l(i));
end
fclose(fid);