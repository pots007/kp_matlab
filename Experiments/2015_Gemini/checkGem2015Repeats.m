% Check out how many shots are exact repeats of the previous:

% Inputs as strings!

function repeats = checkGem2015Repeats(date_, run_)

%date_ = '20150903';
%run_ = '006';
if ispc
    datafolder = fullfile('E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\Data',...
        date_, [date_ 'r' run_]);
elseif ismac
    datafolder = fullfile('/Volumes/Gemini2015/Data', date_, [date_ 'r' run_]);
end
% Repeats for espec1:
diagname = {'Espec1.tif', 'Espec2.tif', 'Espec2LE.raw'};
repeats = cell(size(diagname));
for diag = 1:3
    flist = dir(fullfile(datafolder, ['*' diagname{diag}]));
    if isempty(flist); repeats{diag} = []; end;
    if diag<3
        fprev = imread(fullfile(datafolder, flist(1).name));
    else
        fprev = ReadRAW16bit(fullfile(datafolder, flist(1).name), 1280, 960);
    end
    for i=2:length(flist)
        fprintf('Opening image %s, out of %i\n', flist(i).name, length(flist));
        % Read next image
        if diag<3
            fnext = imread(fullfile(datafolder, flist(i).name));
        else
            fnext = ReadRAW16bit(fullfile(datafolder, flist(i).name), 1280, 960);
        end
        % And compare:
        if fnext==fprev
            repeats{diag}{end+1} = flist(i).name;
        end
        fprev = fnext;
    end
end