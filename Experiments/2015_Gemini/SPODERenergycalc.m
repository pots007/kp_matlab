E0 = 11;
tau = 40e-15;
fno = 20;
L1 = 200; % in cm
D1 = L1/fno; % in cm
I1 = E0/(0.25*pi*D1^2*tau);
fprintf('\nIntensity on first optic at %i cm is %2.2e Wcm^-2\n', L1, I1);
L_c = 200; %distance of collimating optic
D_c = L_c/fno;
d_b = 0.5; % pick off beam diameter
E_b = E0*(d_b/D_c)^2;
fprintf('Energy in pickoff beam with d=%2.1f mm at L=%icm is E=%2.2f mJ\n', ...
    d_b*10, L_c, E_b*1e3);
fprintf('With one p-wedge this becomes E''=%2.2f mJ\n', E_b*0.04*1e3);
fprintf('With one s-wedge this becomes E''=%2.2f mJ\n', E_b*0.1*1e3);