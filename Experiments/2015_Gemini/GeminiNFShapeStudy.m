% This script aims to look at intensioty pattern and energy enclosed in the
% focal spot as the near field shape transitions from Gaussian to fully top
% hat (or SG order 100).
 
focallength = 6;
beamdiameter = 0.15; w0 = 0.5*beamdiameter;
lambda0 = 0.8;
% First, a Gaussian spot.
spotax = (-30:0.1:30)*1e-2;
[X,Y] = meshgrid(spotax, spotax);
R = sqrt(X.^2 + Y.^2);
orders = [2:4:22 100];
rad_int = zeros(length(orders), 200);
rad_NF = zeros(length(orders), length(spotax));
rad_en = zeros(size(rad_int));
for i=1:length(orders)
    ii = orders(i);
    NF = exp(-R.^ii/w0^ii);
    rad_NF(i,:) = NF(round(size(NF,1)),:);
    fff = fftshift(fft2(NF, 2^13, 2^13));
    NFspotax = (1:2^13)-2^12-1;
    logm = abs(NFspotax)<200;
    NFspotax = NFspotax(logm);
    NFspot = abs(fff(logm,logm)).^2;
    imagesc(NFspotax, NFspotax, NFspot);
    [CX, CY] = meshgrid(NFspotax, NFspotax);
    logm2 = NFspotax<0;
    NFspotradax = NFspotax(~logm2);
    rad_int(i,:) = NFspot(NFspotax==0,~logm2);
    %NFspoten = zeros(size(NFspotradax));
    %toten = sum(NFspot(:));
    toten = sum(abs(fff(:)).^2);
    for k=1:length(NFspotradax)
        logm = CX.^2 + CY.^2 < NFspotradax(k).^2;
        rad_en(i,k) = sum(sum(NFspot(logm)))./toten;
    end
    title(num2str(ii));
    drawnow;
end
FFTmax = 2*pi/0.001; % Maximum wave k at FF
k0 = 2*pi/8e-7;
NFspotcalib = FFTmax/k0*focallength/2^13;
NFspotradax = NFspotradax*NFspotcalib*1e6;
%%
hfig = figure(8); clf(hfig); set(hfig, 'Color', 'w', 'Position', [100 100 1000 800]);
locs = GetFigureArray(2,2, [0.05 0.1 0.1 0.1], 0.025, 'across');
axs(1) = axes('Parent', hfig, 'Position', locs(:,1), 'NextPlot', 'add', 'Box', 'on');
hh = plot(NFspotradax, rad_int./max(rad_int(:)));
set(hh, 'LineWidth', 2, 'LineStyle', '--');
set(axs(1), 'XLim', [0 100], 'YLim', [-0.05 1.05], 'LineWidth', 2);
leglabels = cell(size(orders));
for i=1:length(orders); leglabels{i} = ['$O = ' num2str(orders(i)) '$']; end;
legend(hh, leglabels, 'Location', 'Northeast');

axs(2) = axes('Parent', hfig, 'Position', locs(:,2), 'NextPlot', 'add', 'Box', 'on');
rad_int_norm = rad_int./repmat(max(rad_int,[],2),1,size(rad_int,2));
hh = plot(NFspotradax, rad_int_norm);
set(hh, 'LineWidth', 2, 'LineStyle', '--');
set(axs(2), 'XLim', [0 100], 'YLim', [-0.05 1.05], 'LineWidth', 2);
legend(hh, leglabels, 'Location', 'Northeast');
plot([0 100], 0.5*[1 1], '-k');
plot([0 100], exp(-1)*[1 1], '-r');
plot([0 100], exp(-2)*[1 1], '-g');
rectangle('Position', [45 0 20 0.02], 'Parent', axs(2));
subaxloc = [locs(1,2)+locs(3,2)/4 locs(2,2)+locs(4,2)/1.65 locs(3,2)/3 locs(4,2)/3];
subax = axes('Parent', hfig, 'Position', subaxloc);
plot(subax, NFspotradax, rad_int./repmat(max(rad_int,[],2),1,size(rad_int,2)));
set(subax, 'XLim', [45 65], 'LineWidth', 2);

axs(3) = axes('Parent', hfig, 'Position', locs(:,3), 'NextPlot', 'add', 'Box', 'on');
hh = plot(NFspotradax, rad_en);
set(hh, 'LineWidth', 2, 'LineStyle', '--');
set(axs(3), 'XLim', [0 100], 'YLim', [-0.05 1.05], 'LineWidth', 2);
legend(hh, leglabels, 'Location', 'Southeast');

axs(4) = axes('Parent', hfig, 'Position', locs(:,4), 'NextPlot', 'add', 'Box', 'on');
% Interpolate rad_int_norm and rad_en to a finer grid!
NFspotradax_f = linspace(min(NFspotradax), max(NFspotradax), 2^10);
rad_int_norm_f = interp1(NFspotradax', rad_int_norm', NFspotradax_f', 'pchip')';
rad_en_f = interp1(NFspotradax', rad_en', NFspotradax_f, 'pchip');
[~, eninds1] = min(abs(rad_int_norm_f - 0.5),[],2);
eninds1 = sub2ind([length(NFspotradax_f) 7], eninds1', 1:7);
[~, eninds2] = min(abs(rad_int_norm_f - exp(-1)),[],2);
eninds2 = sub2ind([length(NFspotradax_f) 7], eninds2', 1:7);
[~, eninds3] = min(abs(rad_int_norm_f - exp(-2)),[],2);
eninds3 = sub2ind([length(NFspotradax_f) 7], eninds3', 1:7);

h(1) = plot(orders, rad_en_f(eninds1), '-ok');
h(2) = plot(orders, rad_en_f(eninds2), '-xr');
h(3) = plot(orders, rad_en_f(eninds3), '-dg');
legend(h, {'FWHM', '$1/e$', '$1/e^2$'}, 'Location', 'Southeast');
set(axs(4), 'XLim', [0 100], 'YLim', [-0.05 1.05], 'LineWidth', 2);
set(h, 'LineWidth', 2);

% Make things a bit nicer
linkaxes(axs(1:3), 'xy');
set(axs(1:2), 'XTickLabel', []);
set(axs([2 4]), 'YAxisLocation', 'right');
ylabel(axs(1), 'Intensity / a.u.'); ylabel(axs(2), 'Normalised I');
ylabel(axs(3), 'Enclosed energy');  ylabel(axs(4), 'Enclosed energy');
xlabel(axs(3), '$r / \mu m$'); xlabel(axs(4), 'Supergaussian order');
text(105, 1.1, 'Supergaussian nearfield study', 'Parent', axs(1), 'HorizontalAlignment', 'center');
make_latex(hfig); setAllFonts(hfig, 16);

if ismac
    savfol = '/Users/kristjanpoder/Google Drive/Experiment/2015Gemini';
end
export_fig(fullfile(savfol, 'SuperGaussianNFs'), '-pdf', '-nocrop', hfig);