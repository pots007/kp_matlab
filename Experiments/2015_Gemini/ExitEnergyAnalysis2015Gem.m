% Look at energy transmission for the 20150826 length scan runs

if ismac
    datfol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis';
end

date_ = 20150826;
runs = 6:8;
% Want to get the lengths of the cell for that day
load('Gem2015LengthScans');
dat = cell(size(runs));
Ldates = cell2mat({LengthScans.date});
Lruns = cell2mat({LengthScans.run});

for i=1:length(runs)
    logm = Ldates==date_ & Lruns==runs(i);
    Ls = LengthScans(logm).cellL;
    ttemp = zeros(length(Ls), 4);
    for k=1:length(Ls)
        fprintf('Getting data for run %i, shot %i\n', i, k);
        ttemp(k,1) = Ls(k);
        ttemp(k,2) = getGemini2015ExitEnergy(date_, runs(i), k);
        ttemp(k,3) = getenergy(getGSN(date_, runs(i), k));
        gasdat = getGemini2015Gas(date_, runs(i), k);
        ttemp(k,4) = gasdat(60000,2);
    end
    dat{i} = ttemp;
end


%% First, let's check the densities were OK, and establish a sensible upper limit
% to eliminate outsiders
locs = GetFigureArray(1,3, [0.05 0.03 0.1 0.12], 0.1, 'down');
hfig = figure(87); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [750 400 600 700]);
ax = [];
exclude_frac = [4 3 3];
for i=1:3
    % To exclude bad n_e shots, we first cull out everthing outside std,
    % and then take a mean of the leftover data.
    nedat = dat{i}(:,4);
    nedat = nedat(abs(nedat-mean(nedat))<std(nedat));
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
    rectangle('Position', [0 mean(nedat)-exclude_frac(i)*std(nedat) 91 2*exclude_frac(i)*std(nedat)],...
        'Parent', ax(i), 'EdgeColor', 0.94*[1 1 1], 'FaceColor', 0.94*[1 1 1]);
    plot(ax(i), dat{i}(:,4), 'ok');
    plot(ax(i), [0 91], mean(nedat)*[1 1], '-k');
    title(sprintf('%i, run %i', date_, runs(i)));
    ylabel(ax(i), '$n_e / \mathrm{cm}^{-3}$');
end
xlabel(ax(3), 'Shot number');
set(ax, 'XLim', [-1 92]); set(ax(1:2), 'XTickLabel', []);
make_latex(hfig); setAllFonts(hfig, 16);

%% Now do something with the data:

hfig = figure(88); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [100 100 600 500]);
ax = axes('Parent', hfig, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
hh = []; leglabel = []; 
% Remove anomalous point:
dat{2}(42,2) = nan;
enDecker = @(a,b,c,x) a*(1 - erf((x-b)/c));
enDecker2 = @(a,b,c,d,x) a - d*erf((x-b)/c);
for i=1:3
    Ls = dat{i}(:,1);
    Ls_unique = unique(Ls);
    ens = cell(size(Ls_unique));
    nedat = dat{i}(:,4);
    nedat = nedat(abs(nedat-mean(nedat))<std(nedat));
    for k=1:length(Ls_unique)
        logm = Ls==Ls_unique(k) & ~isnan(dat{i}(:,2));
        logm = logm & dat{i}(:,2)~=0;
        logm = logm & abs(dat{i}(:,4)-mean(nedat))<exclude_frac(i)*std(nedat);
        ens{k} = dat{i}(logm,2)./dat{i}(logm,3);
    end
    % And remove the VERY high 34mm dataset for run 8
    if i==3; ens{12} = []; end;
    leglabel{end+1} = ['$n_e = ' num2str(mean(nedat)*1e-18, '%2.1f') '\times 10^{18} \mathrm{cm}^{-3}$'];
    %plot(ax, Ls, dat{i}(:,2), '^');
    hh(end+1) = errorbar(ax, Ls_unique+(2-i)*0.3, cellfun(@mean, ens), cellfun(@std, ens), 'o', 'LineWidth', 2);
    %hh(i) = errorbar(ax, Ls_unique*mean(nedat)*1e-18, cellfun(@mean, ens), cellfun(@std, ens), 'o', 'LineWidth', 2);
    mean_ens = cellfun(@mean, ens);
    %ff = fit(Ls_unique(~isnan(mean_ens)), mean_ens(~isnan(mean_ens)), 'exp1');
    %[ff, goff] = fit(Ls_unique(~isnan(mean_ens)), mean_ens(~isnan(mean_ens)), enDecker, 'Startpoint', [2.5e-6, 10, 10])
    [ff, goff] = fit(Ls_unique(~isnan(mean_ens)), mean_ens(~isnan(mean_ens)), enDecker2, 'Startpoint', [2.5e-6, 10, 10, 2.5e-6]);
    hh(end+1) = plot([0; Ls_unique], ff([0; Ls_unique]), ['-' get(hh(i), 'Color')]);
    leglabel{end+1} = ['Fit, $R^2 = ' num2str(goff.adjrsquare, '%2.2f') '$'];
end
legend(hh, leglabel, 'Location', 'east');
xlabel('Cell length / mm'); ylabel('Transmission fraction / a.u.');
make_latex(hfig); setAllFonts(hfig, 16);
