% estimate2015GeminiHighEnEmittance.m
%
% Look at data from 20150908r008 and try to calculate the high enegry tail
% emittance, or at least give a rough number.

datfol = fullfile(getGDrive, 'Experiment', '2015_Gemini', 'EspecAnalysis',...
    '20150908r008', 'mat_manual');
flist = dir(fullfile(datfol, '*Espec1.mat'));
flist = {flist.name}';

%calculateTotalDist2Screen;
zl = 1268.2;


% ------------ Set up tracking
% FROM plot2015Zepf_EspecOnly.m - set up life
trackfile = 'Tracking_20170309_KP_4mrad.mat';

ff = load(trackfile);
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

%%
hfig = figure(2431251); clf(hfig);
locs = GetFigureArray(5,3, [0.04 0.04 0.05 0.04], [0.03 0.06], 'across');
flistp = flipud(flist);
clear axx;
for k=1:size(locs,2)
    axx(k) = makeNiceAxes(hfig, locs(:,k));
    fdat = load(fullfile(datfol, flistp{k}));
    dtheta = abs(mean(diff(fdat.shotdata.divax)));
    enax = fdat.shotdata.tracking_btrac(fdat.shotdata.alongscreen);
    divv_raw = getSpectrumDivergence(fdat.shotdata.image, 0.1, 2);
    divv = divv_raw'./divfit(fdat.shotdata.alongscreen)*zl*dtheta;
    plot(axx(k), enax, (divv));
    title(axx(k), flistp{k}(13:16));
    drawnow;
end
set(axx, 'XLim', [200 2700], 'YLim', [0.7 4])

%% Rough numbers:

E = 2500; % MeV
gamm = E/0.511;
bet = sqrt(1-1/gamm^2);
div = 1.5; % FWHM divergence in mrad
ss = 1e-3; % FWHM source size in mm
useRMS = 0;
if useRMS
    div = div*2*log(2);
    ss = ss*2*log(2);
end
geom_e = sqrt(ss^2*div^2);
norm_e = gamm*bet*geom_e;

fprintf('\nGeometrical emittance: %2.2e mm mrad \n', geom_e);
fprintf('Normalised emittance: %2.2f mm mrad \n', norm_e);
fprintf('Normalised emittance: %2.2f pi mm mrad \n', norm_e/pi);