% This script will plot the images and radial profiles of Gaussian focus,
% Airy pattern and airy pattern with some wavefront crap in it. It will
% also plot the lineouts and energy enclosed as a function of radial
% distance, all for the glory of the thesis.
 
focallength = 6;
beamdiameter = 0.15;
lambda0 = 0.8;
% First, a Gaussian spot.
spotax = -100:0.1:100;
[X,Y] = meshgrid(spotax, spotax);
w0 = 1.35*2*lambda0*focallength/(pi*beamdiameter);
GaussSpot = exp(-X.^2/w0^2-Y.^2/w0^2);
logm = spotax<0;
GaussSpotrad_ax = spotax(~logm);
GaussSpotrad_int = GaussSpot(spotax==0, ~logm).^2;
GaussSpotrad_en = 1 - GaussSpotrad_int;
hfig = figure(34); clf(hfig);
% Need the square as we're plotting intensity!!!!!
imagesc(spotax, spotax, GaussSpot.^2);
 
% Now, an Airy one
R = sqrt(X.^2 + Y.^2);
airyax = pi*beamdiameter/(lambda0*focallength)*R;
AirySpot = (2*besselj(1,airyax)./airyax);
AirySpotrad_ax = spotax(~logm);
AirySpotrad_int = AirySpot(spotax==0, ~logm).^2;
tempax = airyax(spotax==0, ~logm);
AirySpotrad_en = 1 - besselj(0,tempax).^2 - besselj(1,tempax).^2;
imagesc(spotax, spotax, AirySpot.^2);

if ismac
    datfol = '/Volumes/Gemini2015/Ecat/S_uncomp_nf';
end

date_ = 20150903;
run_ = 5;
shot_ = 25;
 
GSN = getGSN(date_, run_, shot_);
fname = sprintf('GS%08i.png', GSN);
im = double(imread(fullfile(datfol, fname)));
im = imrotate(im, 45, 'bilinear', 'crop');

hfig = figure(8); clf(hfig); set(hfig, 'Color', 'w');
locs = GetFigureArray(2,2, [0.05 0.05 0.1 0.05], 0.05, 'across');
axs(1) = axes('Parent', hfig, 'Position', locs(:,1), 'NextPlot', 'add');
imagesc(im, 'Parent', axs(1));
axis image;
cc = centroidlocationsmatrix(im);
plot(cc(1), cc(2), '+r');
plot(sin(0:0.1:7)*155+55+155, cos(0:0.1:7)*155+75+155, '--w', 'Linewidth', 2);

lineout = im(round(cc(2)),:);
lineoutax = (1:size(im,2))-0.5*size(im,2);
SGfun = @(b,x) b(1)*exp(-((x-b(2)./b(3)).^b(4)));
%bb = nlinfit(lineoutax, lineout, SGfun, [6e4 0 150 10])
axs(2) = axes('Parent', hfig, 'Position', locs(:,2), 'NextPlot', 'add');
hhh(1) = plot(axs(2), lineoutax, lineout, '-k');
hhh(2) = plot(axs(2), lineoutax, 6e4*exp(-((lineoutax/155)).^10), '-r');
legend(hhh, {'Data', 'Supergaussian order=10'}, 'Location', 'south', 'Box', 'off');
%text(0, 1e4, 'SG order 10', 'Parent', axs(2), 'HorizontalAlignment', 'center');

axs(3) = axes('Parent', hfig, 'Position', locs(:,3), 'NextPlot', 'add');
fff = fftshift(fft2(im, 2^13, 2^13));
NFspotax = (1:2^13)-2^12-1;
logm = abs(NFspotax)<100;
NFspotax = NFspotax(logm);
NFspotint = abs(fff(logm,logm)).^2;
NFspotint = NFspotint./max(NFspotint(:));
%
FFTmax = 2*pi/(0.15/310); % Maximum wave k at FF
k0 = 2*pi/8e-7;
NFspotcalib = FFTmax/k0*focallength/2^13;
NFspotax = NFspotax*NFspotcalib*1e6;
imagesc(NFspotax, NFspotax, NFspotint, 'Parent', axs(3));
axis image
[CX, CY] = meshgrid(NFspotax, NFspotax);
logm2 = NFspotax<0;
NFspotradax = NFspotax(~logm2);
NFspotradint = NFspotint(NFspotax==0,~logm2);
NFspoten = zeros(size(NFspotradax));
%toten = sum(abs(fff(:)).^2);
toten = sum(NFspotint(:));
for i=1:length(NFspoten)
    logm = CX.^2 + CY.^2 < NFspotradax(i).^2;
    NFspoten(i) = sum(sum(NFspotint(logm)))./toten;
end

axs(4) = axes('Parent', hfig, 'Position', locs(:,4), 'NextPlot', 'add');
h(1) = plot(NFspotradax, NFspotradint, '-k');
h(2) = plot(NFspotradax, NFspoten, '--k');
h(3) = plot(AirySpotrad_ax, AirySpotrad_int, '-r');
h(4) = plot(AirySpotrad_ax, AirySpotrad_en, '--r');
h(5) = plot(GaussSpotrad_ax, GaussSpotrad_int, '-b');
h(6) = plot(GaussSpotrad_ax, GaussSpotrad_en, '--b');
legend(h(1:2:6), {'Gemini NF', 'Airy', 'Gaussian'}, 'Location', 'east');
set(axs, 'Box', 'on', 'LineWidth', 2);
set(axs(1), 'XTickLabel', [], 'YTickLabel', []);
set(axs(2), 'XTickLabel', []);
set(axs(4), 'XLim', [0 100], 'YLim', [-0.05 1.05]);
xlabel(axs(4), '$r / \mu m$'); ylabel(axs(4), '$E/E_0$');
xlabel(axs(3), '$x / \mu m$'); ylabel(axs(3), '$y / \mu m$');
ylabel(axs(2), 'Counts');
make_latex(hfig); setAllFonts(hfig, 18);
if ismac
    savfol = '/Users/kristjanpoder/Google Drive/Experiment/2015Gemini';
end
export_fig(fullfile(savfol, 'GeminiNF_farfield'), '-pdf', '-nocrop', hfig);