% getGemini2015ExitMode.m

% Returns the data for Gemini 2015 exit mode image

% Accepts either date, run, shot or correctly formatted shot string
% (YYYYMMDDrRRRsSSS)

function im = getGemini2015ExitMode(date,run,shot)
if nargin==1 && ischar(date) && length(date)==16
    shotstr = date;
elseif nargin==3
    shotstr = sprintf('%ir%03is%03i', date, run, shot);
else
    trace = nan;
    return;
end
if ispc
    datapath = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\Data';
elseif ismac
    datapath = '/Volumes/Gemini2015/Data';
end
fname = fullfile(datapath, shotstr(1:8), shotstr(1:12), [shotstr '_ExitMode.tif']);
try
    im = double(imread(fname));
catch err
    disp(err.message);
    im = nan(1024);
end
end