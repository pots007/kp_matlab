% getGem2015ExitEnergy.m

% Returns exit enegry reading for particular shot
% Returns empty if no data for that shot

% Accepts either date, run, shot or correctly formatted shot string
% (YYYYMMDDrRRRsSSS)

function energy = getGemini2015ExitEnergy(date, run, shot)
load('ExitEnergies');
%date = 20150826;
%run = 2;
%shot = 4;
if nargin==1 && ischar(date) && length(date)==16
    shotstr = date;
elseif nargin==3
    shotstr = sprintf('%ir%03is%03i', date, run, shot);
else
    energy = nan;
    return;
end

% Fix 20151011, apparently have more than one entry for some shots...
ind = strcmp(ExitEnergies(:,1), shotstr);%.*(1:size(ExitEnergies,1))'

if sum(ind)==0
    energy = nan;
else
    energy = ExitEnergies{ind,2};
end

