% getGemini2015laserFF.m

% Returns image of Ecat recorded FF image upstairs
% Returns empty if no data

% Accepts either date, run, shot or correctly formatted shot string
% (YYYYMMDDrRRRsSSS)

function im = getGemini2015laserFF(date,run,shot)
if nargin==1 && ischar(date) && length(date)==16
    shotstr = date;
elseif nargin==3
    shotstr = sprintf('%ir%03is%03i', date, run, shot);
else
    im = [];
    return;
end

if ispc
    datapath = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\Ecat\S_comp_ff';
elseif ismac
    datapath = '/Volumes/Gemini2015/Ecat/S_comp_ff';
end

GSN = getGSN(str2double(shotstr(1:8)), str2double(shotstr(10:12)), str2double(shotstr(14:16)));
fname = fullfile(datapath, sprintf('GS%08i.png', GSN));
try
    im = imread(fname);
catch err
    disp(['File for ' shotstr ', GSN ' num2str(GSN) ' not found: ' err.message]);
    im = [];
end
end