% using theta = lambda/(pi*w0)
% theta is full cone angle

w0 = (5:0.05:30)*1e-6;
dists = 1500:250:3000;
dens_p = @(w0) epsilon0*me*(2*pi*c./(w0*qe)).^2;
I_D = @(D) 15/(pi*0.25*40e-15*D.^2);
% Plot beam diameter at different distances from exit of plasma for various
% spot sizes
D = repmat(dists',1,length(w0))*0.8e-6./(pi*repmat(w0,length(dists),1));
figure(89); set(89, 'Position', [175 150 900 650]); clf;
hs = plot(w0*2e6, D);
set(hs, 'LineWidth', 2.5);
grid on;
for i=1:length(dists)
    labs{i} = sprintf(' d = %i mm', dists(i));
end
hleg = legend(hs, labs);
ax1 = gca;
set(ax1, 'Position', [0.13 0.11 0.725 0.775], 'YLim', [10 160])
xticks = get(ax1, 'XTick');
for i=1:length(xticks)
    labs{i} = sprintf('%1.1f', dens_p(xticks(i)*1e-6)*1e-24);
end
yticks = get(ax1, 'YTick');
for i=1:length(yticks)
    labsy{i} = sprintf('%1.1f', I_D(yticks(i)*0.1)*1e-12);
end
ax2 = axes('Parent', 89, 'Position', get(ax1, 'Position'), 'YAxisLocation', 'right',...
    'XAxisLocation', 'top', 'Box', 'off', 'XLim', get(ax1, 'XLim'), ...
    'XTick', get(ax1, 'XTick'), 'XTickLabel', labs, 'YLim', get(ax1, 'YLim'),...
    'YTick', get(ax1, 'YTick'), 'YTickLabel', labsy, ...
    'Color', 'none');
xlabel(ax1, 'Spot size / \mu m');
ylabel(ax1, 'Beam diameter / mm');
xlabel(ax2, 'Plasma density / 10^{18} cm^{-3}');
ylabel(ax2, 'Intensity / 10^{12} Wcm^{-2}');

setAllFonts(89, 20);
if ispc
    savfol = 'F:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\';
end
export_fig([savfol 'BeamDivergence'], '-pdf', '-transparent', '-nocrop', 89);