% moveEcatData.m

% This script will take the laser diagnsotics in their folders and add them
% to the standard target area data collection, wnsuring that the renaming
% and path are kept the same. This relies entirely on Jon's script to
% connect up dates and GSNs (ie our bookkeeping during the experiment).

% Set up paths to all the right places
if ispc
    dfol = 'Y:\Ecat\';
    dfol = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\Ecat\';
    sfol = 'Y:\Data\';
elseif ismac
    dfol = '/Volumes/Gemini2015/Ecat/';
    sfol = '/Volumes/Gemini2015/Data/';
end

% Figure out the diagnostics we have and use these as the diagnostic names
% later on
dlist = dir(dfol); dlist = dlist(3:end); % Filter out '.' and '..'
dlist = dlist(cell2mat({dlist.isdir}));
dnames = {dlist.name};

% Load the data structure;
load('2015LaserData.mat');

errcount = 0;
% Now look into the ecat data folders and start to move things over
for diag=1:length(dlist)
    flist = dir(fullfile(dfol, dnames{diag}));
    for it=1:length(flist)
        if flist(it).isdir; continue; end;
        GSN = str2double(flist(it).name(3:end-4));
        ind = find(laserdata.GSN==GSN);
        if isempty(ind); continue; end;
        dd = laserdata.date(ind); rr = laserdata.run(ind); ss = laserdata.shot(ind);
        fstruct = sprintf('%i/%ir%03i/%ir%03is%03i_', dd,dd,rr,dd,rr,ss);
        [~,~,ext] = fileparts(flist(it).name);
        spath = [sfol fstruct dnames{diag} ext];
        disp([fullfile(dfol, dnames{diag}, flist(it).name) '   ' spath]);
        try
            movefile(fullfile(dfol, dnames{diag}, flist(it).name), spath);
        catch err
            errcount = errcount+1;
            disp(err.message);
        end
    end
end