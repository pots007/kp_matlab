

date_ = '20150926';
run_ = '002';
runtype = 0; %1 for pressure scan, 0 for length scan

if ismac
    datfol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/';
    savfol = '/Users/kristjanpoder/Google Drive/Experiment/2015Gemini/EspecAnalysis/';
end
fullfile(getDropboxPath, 'MATLAB', 'Experiments', '2015_Gemini', 'RunAnalysisScripts', '20150926r004_pL.mat')
imm = zeros(1024,1024,89);

shotlogm = 31:61;
for i=shotlogm
    fprintf('Reading image %i\n', i);
    tempim = getGemini2015ExitMode(str2double(date_), str2double(run_), i);
    imm(:,:,i) = tempim;
end

if runtype
    scanQ = ppLL(shotlogm,2);
else
    scanQ = ppLL(shotlogm,1);
end
Q_unique = unique(scanQ);
%% And plot all these!

Q_numel = zeros(size(Q_unique));
for i=1:length(Q_unique); Q_numel(i) = sum(scanQ==Q_unique(i)); end
hfig = figure(6); clf(hfig); set(hfig, 'Color', 'w', 'Position', [100 100 1200 800]);
locs = GetFigureArray(length(Q_numel), max(Q_numel), [0.04 0.0025 0.0025 0.01], 0.01, 'down');
colormap(jet_white(128));
axit = 1;
ax = -ones(length(Q_numel),max(Q_numel));
for i=1:length(Q_unique)
    shotinds = find(scanQ==Q_unique(i));
    for k=1:Q_numel(i)
        locsind = (i-1)*max(Q_numel) + k;
        ax(i,k) = axes('Parent', hfig, 'Position', locs(:,locsind));% axit = axit+1;
        imagesc(imm(:,:,shotinds(k)), 'Parent', ax(i,k));
        axis image;
        drawnow;
    end
    title(ax(i,1), num2str(Q_unique(i)));
end
ax = ax(ishandle(ax));
clims = cell2mat(get(ax, 'CLim')); clim_max = mean(clims(:,2));
set(ax, 'XTick', [], 'YTick', [], 'YDir', 'normal', 'FontSize', 20, 'CLim', [0 clim_max]);
make_latex(hfig);
export_fig(fullfile(savfol, [date_ 'r' run_], 'ExitModeMontage'), '-pdf', '-nocrop', hfig);
fprintf('Finished saving image!\n');