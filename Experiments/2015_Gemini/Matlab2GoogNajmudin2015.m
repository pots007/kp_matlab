dat = GetGeminiShotsData('Aug2015');
GSNs = cell2mat(dat(:,1));

if ~exist('google_tokens.mat', 'file')
    error('Get the Google tokens first!)');
else
    load('google_tokens.mat');
    % to refresh the Docs access token (usually expires after 1 hr) you'd call
    aTokenDocs=refreshAccessToken(client_id,client_secret,rTokenDocs);
    
    % to refresh the Spreadsheet access token (usually expires after 1 hr) you'd call
    aTokenSpreadsheet=refreshAccessToken(client_id,client_secret,rTokenSpreadsheet);
end

if isempty(aTokenDocs) || isempty(aTokenSpreadsheet)
    warndlg('Could not obtain authorization tokens from Google.','');
    return;
end
disp('Logged in.');
sheetIwant = -1;
sheetlist = getSpreadsheetList(aTokenSpreadsheet);
if (~isempty(sheetlist))
    for i=1:length(sheetlist)
        if (strcmp('NajmudinGemini2015_DataSheet', sheetlist(i).spreadsheetTitle))
            sheetIwant = i;
        end
    end
end

worksheetlist = getWorksheetList(sheetlist(sheetIwant).spreadsheetKey, aTokenSpreadsheet);
worksheetIwant = -1;
if (~isempty(worksheetlist))
    for i=1:length(worksheetlist)
        if (strcmp('ShotSheet', worksheetlist(i).worksheetTitle))
            worksheetIwant = i;
        end
    end
end

if (sheetIwant<0 || worksheetIwant <0)
    if (sheetIwant<0)
        error('Incorrect worksheet name!');
    else
        error('Incorrect worksheet name!');
    end
end
disp('Found correct worksheet.')

for i=6500:7000%size(data,1) 
    % Iterate over rows in shotsheet!
    disp(['Looking into row ' num2str(i)]);
    GSNraw = getWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, i, 5, aTokenSpreadsheet);
    GSN = sscanf(GSNraw, '%i');
    if (isempty(GSN)) % If this particular row doesn't contain a GSN!
        continue;
    end
    % Find which line of data corresponds to this GSN
    lno = find(GSNs==GSN);
    if isempty(lno)
        disp('This GSN has no Ecat data!');
        continue;
    end
    
    % Uncompressed energies
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, ...
        i, 22, sprintf('%2.3f', str2double(dat{lno,3})), aTokenSpreadsheet);
    
    % Shot times
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, ...
        i, 4, dat{lno,2}, aTokenSpreadsheet);
    
    % Pulse lengths, mainly for a laugh!
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, ...
        i, 23, dat{lno,8}, aTokenSpreadsheet);
    
end
