% This script ought to look at transmitted energy diagnostic for 0903 runs


% This is most exciting, actually, we have some zero shots!!!!!

% Zero shots first, shots 1-6 in run 4
% Wait something funny is happening here: only shots 1:5 are actually
% vacuum! Thus we must have fucked something up with the shotsheet. FFS,
% someone had ONE job!
ens_zero_gentec = zeros(1,5); ens_zero_Ecat = zeros(1,5);
for i=1:5
    GSN = getGSN(20150903, 4, i);
    ens_zero_Ecat(i) = getenergy(GSN);
    ens_zero_gentec(i) = getGemini2015ExitEnergy(20150903,4,i);
end
ens_zero1 = [mean(ens_zero_gentec) std(ens_zero_gentec)];
ens_zero2 = [mean(ens_zero_gentec./ens_zero_Ecat) std(ens_zero_gentec./ens_zero_Ecat)];
savfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'ExitEnergy', '20150903_');
saving = true;
%% Let's get data for all runs
nshots = [46 36 50 28 76 22];
D0903 = struct;
for i=1:length(nshots)
    D0903.run(i).run = i + 1;
    D0903.run(i).nshots = nshots(i);
    for k=1:nshots(i)
        D0903.run(i).ens_gentec(k) = getGemini2015ExitEnergy(20150903, i+1, k);
        D0903.run(i).ens_Ecat(k) = getenergy(getGSN(20150903, i+1, k));
    end
end

%% Run 2: pressure scan 20mm

nshots_ = 46;
% Get data for all shots: gas pressure and ecat energy and gentec energy
% and exitmode
pres2 = zeros(1,nshots_); gentec = zeros(1,nshots_); ecat = zeros(1,nshots_);
exitmodes = zeros(1024,1024,nshots_);
for ss = 1:nshots_
    fprintf('Extracting data for shots %i\n', ss);
    gentec(ss) = getGemini2015ExitEnergy(20150903, 2, ss);
    ecat(ss) = getenergy(getGSN(20150903, 2, ss));
    exitmodes(:,:,ss) = getGemini2015ExitMode(20150903, 2, ss);
    gasdat = getGemini2015Gas(20150903, 2, ss);
    if ~isempty(gasdat); pres2(ss) = mean(gasdat(gasdat(:,1)<498 & gasdat(:,1)>490, 2)); end
end
% And fix some errors:
ecat(5) = 16.33; % laser died, something awfully wrong, not sure what happened 
pres2(3) = mean(pres2([1 2 4])); % File missing for this one...
% And fix the issue of me being silly: Juffalo's calibration applies to the
% pressures we write down in the shotsheet; I inserted it to the gas
% diagnostic. The cross calibration factor is:
pres2 = pres2/0.8713;
% Hardcode the shots where things changed - actually, managed some buggy code below
% Indices of things changing:
inds = (2:nshots_).*(abs(diff(pres2)) > 1e17); 
inds = inds(inds~=0); inds = [1 inds nshots_+1];
shotranges = cell(numel(inds)-1,1);
for i=1:length(shotranges); shotranges{i} = (inds(i)):(inds(i+1)-1); end;
% And now to plotting things - making errors etc
entrans2 = zeros(length(shotranges), 2); entrans2_ = zeros(length(shotranges), 2);
pres2_ = zeros(size(shotranges));
for i=1:length(shotranges)
    shots_ = shotranges{i};
    pres2_(i) = mean(pres2(shots_));
    % Avoid NANs everywhere...
    logm = shots_( ~isnan(gentec(shots_)) & ~isnan(ecat(shots_)));
    entrans2(i,1) = mean(gentec(logm));
    entrans2(i,2) = std(gentec(logm));
    entrans2_(i,1) = mean(gentec(logm)./ecat(logm));
    entrans2_(i,2) = std(gentec(logm)./ecat(logm));
end

% Plot these, with initial data points
hfig = figure(78); clf(hfig);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
h1 = plot([0 pres2], [ens_zero1(1) gentec], 'o');
h2 = errorbar([0; pres2_], [ens_zero1(1); entrans2(:,1)], [ens_zero1(2); entrans2(:,2)], 'o');
cla;
h1 = plot([0 pres2], [ens_zero2(1) gentec./ecat], 'x');
h2 = errorbar([0; pres2_], [ens_zero2(1); entrans2_(:,1)], [ens_zero2(2); entrans2_(:,2)], 'o');
set([h1 h2], 'LineWidth', 2, 'MarkerSize', 10);
xlabel('$n_e / \mathrm{cm}^{-3}$'); ylabel('T / a.u.'); 
title('Run 2: p-scan at L=20 mm');
make_latex(hfig); setAllFonts(hfig, 16);
if saving; export_fig([savfol 'r002_transmission'], '-nocrop', '-pdf', hfig); end
%% Run 5: pressure scan 20mm

nshots_ = 28;
% Get data for all shots: gas pressure and ecat energy and gentec energy
% and exitmode
pres5 = zeros(1,nshots_); gentec = zeros(1,nshots_); ecat = zeros(1,nshots_);
exitmodes = zeros(1024,1024,nshots_);
for ss = 1:nshots_
    fprintf('Extracting data for shots %i\n', ss);
    gentec(ss) = getGemini2015ExitEnergy(20150903, 5, ss);
    ecat(ss) = getenergy(getGSN(20150903, 5, ss));
    exitmodes(:,:,ss) = getGemini2015ExitMode(20150903, 5, ss);
    gasdat = getGemini2015Gas(20150903, 5, ss);
    if ~isempty(gasdat); pres5(ss) = mean(gasdat(gasdat(:,1)<498 & gasdat(:,1)>490, 2)); end
end
% And fix some errors:
pres5 = pres5/0.8713;
%pres(3) = mean(pres([1 2 4])); % File missing for this one...
% Hardcode the shots where things changed - actually, managed some buggy code below
% Indices of things changing:
inds = (2:nshots_).*(abs(diff(pres5)) > 1e17); 
inds = inds(inds~=0); inds = [1 inds nshots_+1];
shotranges = cell(numel(inds)-1,1);
for i=1:length(shotranges); shotranges{i} = (inds(i)):(inds(i+1)-1); end;
% And now to plotting things - making errors etc
entrans5 = zeros(length(shotranges), 2); entrans5_ = zeros(length(shotranges), 2);
pres5_ = zeros(size(shotranges));
for i=1:length(shotranges)
    shots_ = shotranges{i};
    pres5_(i) = mean(pres5(shots_));
    % Avoid NANs everywhere...
    logm = shots_( ~isnan(gentec(shots_)) & ~isnan(ecat(shots_)));
    entrans5(i,1) = mean(gentec(logm));
    entrans5(i,2) = std(gentec(logm));
    entrans5_(i,1) = mean(gentec(logm)./ecat(logm));
    entrans5_(i,2) = std(gentec(logm)./ecat(logm));
end

% Plot these, with initial data points
hfig = figure(78); clf(hfig);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
h1 = plot([0 pres5], [ens_zero1(1) gentec], 'o');
h2 = errorbar([0; pres5_], [ens_zero1(1); entrans5(:,1)], [ens_zero1(2); entrans5(:,2)], 'o');
cla;
h1 = plot([0 pres5], [ens_zero2(1) gentec./ecat], 'x');
h2 = errorbar([0; pres5_], [ens_zero2(1); entrans5_(:,1)], [ens_zero2(2); entrans5_(:,2)], 'o');
set([h1 h2], 'LineWidth', 2, 'MarkerSize', 10);
xlabel('$n_e / \mathrm{cm}^{-3}$'); ylabel('T / a.u.'); 
title('Run 5: p-scan at L=12.5 mm');
make_latex(hfig); setAllFonts(hfig, 16);
if saving; export_fig([savfol 'r005_transmission'], '-nocrop', '-pdf', hfig); end
%% Plot both length scans on same graph
hfig = figure(78); clf(hfig);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
if 1
    h1 = errorbar([0; pres2_], [ens_zero2(1); entrans2_(:,1)], [ens_zero2(2); entrans2_(:,2)], 'o');
    h2 = errorbar([0; pres5_], [ens_zero2(1); entrans5_(:,1)], [ens_zero2(2); entrans5_(:,2)], 'o');
    xlabel('$n_e / \mathrm{cm}^{-3}$');
    fnam = 'pscans_ne';
else
    h1 = errorbar([0; 2*pres2_], [ens_zero2(1); entrans2_(:,1)], [ens_zero2(2); entrans2_(:,2)], 'o');
    h2 = errorbar([0; 1.25*pres5_], [ens_zero2(1); entrans5_(:,1)], [ens_zero2(2); entrans5_(:,2)], 'o');
    xlabel('$n_a / \mathrm{cm}^{-2}$');
    fnam = 'pscans_na';
end
set([h1 h2], 'LineWidth', 2, 'MarkerSize', 10);
legend([h1 h2], {'L=20 mm', 'L=12.5 mm'}, 'Location', 'Southwest')
ylabel('T / a.u.');  title('p-scans');
make_latex(hfig); setAllFonts(hfig, 16);
if saving; export_fig([savfol fnam], '-nocrop', '-pdf', hfig); end

%% And now try to do length scans: run 3
nshots_ = 35;
% Get data for all shots: gas pressure and ecat energy and gentec energy
% and exitmode
pres3 = zeros(1,nshots_); gentec = zeros(1,nshots_); ecat = zeros(1,nshots_);
exitmodes = zeros(1024,1024,nshots_);
Ls3 = reshape(repmat((42:-4:10), 4, 1), [1 36]); Ls3 = Ls3(1:end-1);
Ls3_ = fliplr(unique(Ls3));
for ss = 1:nshots_
    fprintf('Extracting data for shot %i\n', ss);
    gentec(ss) = getGemini2015ExitEnergy(20150903, 3, ss);
    ecat(ss) = getenergy(getGSN(20150903, 3, ss));
    gasdat = getGemini2015Gas(20150903, 3, ss);
    if ~isempty(gasdat); pres3(ss) = mean(gasdat(gasdat(:,1)<498 & gasdat(:,1)>490, 2)); end
    try
        exitmodes(:,:,ss) = getGemini2015ExitMode(20150903, 3, ss);
    catch 
    end
end
%
entrans3 = zeros(length(Ls3_), 2); entrans3_ = zeros(length(Ls3_), 2);
pres3_ = zeros(size(Ls3_));
for i=1:length(Ls3_)
    shots_ = (1:nshots_).*(Ls3_(i)==Ls3); shots_ = shots_(shots_~=0);
    pres3_(i) = mean(pres3(shots_));
    % Avoid NANs everywhere...
    logm = shots_( ~isnan(gentec(shots_)) & ~isnan(ecat(shots_)));
    entrans3(i,1) = mean(gentec(logm));
    entrans3(i,2) = std(gentec(logm));
    entrans3_(i,1) = mean(gentec(logm)./ecat(logm));
    entrans3_(i,2) = std(gentec(logm)./ecat(logm));
end
% Plot these, with initial data points
hfig = figure(78); clf(hfig);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
h1 = plot([0 Ls3], [ens_zero1(1) gentec], 'o');
h2 = errorbar([0 Ls3_], [ens_zero1(1); entrans3(:,1)], [ens_zero1(2); entrans3(:,2)], 'o');
cla;
h1 = plot([0 Ls3-0.5], [ens_zero2(1) gentec./ecat], 'x');
h2 = errorbar([0 Ls3_], [ens_zero2(1); entrans3_(:,1)], [ens_zero2(2); entrans3_(:,2)], 'o');
set([h1 h2], 'LineWidth', 2, 'MarkerSize', 10);
xlabel('L / mm'); ylabel('T / a.u.'); 
title({'Run 3: L-scan at p=40 mbar' ['$n_e = ' num2str(40/31, '%2.1f') '\times 10^{18} \mathrm{cm}^{-3}$']});
make_latex(hfig); setAllFonts(hfig, 16);
if saving; export_fig([savfol 'r003_transmission'], '-nocrop', '-pdf', hfig); end

%% And now try to do length scans: run 4
nshots_ = 50;
% Get data for all shots: gas pressure and ecat energy and gentec energy
% and exitmode
pres4 = zeros(1,nshots_); gentec = zeros(1,nshots_); ecat = zeros(1,nshots_);
exitmodes = zeros(1024,1024,nshots_);
Ls4 = reshape(repmat((42:-4:6), 4, 1), [1 40]); Ls4 = [42*ones(1,5) Ls4];
Ls4_ = fliplr(unique(Ls4));
for ss = 6:nshots_
    fprintf('Extracting data for shot %i\n', ss);
    gentec(ss) = getGemini2015ExitEnergy(20150903, 4, ss);
    ecat(ss) = getenergy(getGSN(20150903, 4, ss));
    gasdat = getGemini2015Gas(20150903, 4, ss);
    if ~isempty(gasdat); pres4(ss) = mean(gasdat(gasdat(:,1)<498 & gasdat(:,1)>490, 2)); end
    try
        exitmodes(:,:,ss) = getGemini2015ExitMode(20150903, 4, ss);
    catch 
    end
end
gentec = gentec(6:end); ecat = ecat(6:end);
%
entrans4 = zeros(length(Ls4_), 2); entrans4_ = zeros(length(Ls4_), 2);
pres4_ = zeros(size(Ls4_));
for i=1:length(Ls4_)
    shots_ = (1:(nshots_-5)).*(Ls4_(i)==Ls4); shots_ = shots_(shots_~=0);
    pres4_(i) = mean(pres4(shots_));
    % Avoid NANs everywhere... Additionally, avoid zeros on gentec
    logm = shots_( ~isnan(gentec(shots_)) & ~isnan(ecat(shots_)) & gentec(shots_)~=0);
    entrans4(i,1) = mean(gentec(logm));
    entrans4(i,2) = std(gentec(logm));
    entrans4_(i,1) = mean(gentec(logm)./ecat(logm));
    entrans4_(i,2) = std(gentec(logm)./ecat(logm));
end
% Plot these, with initial data points
hfig = figure(78); clf(hfig);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
h1 = plot([0 Ls4], [ens_zero1(1) gentec], 'o');
h2 = errorbar([0 Ls4_], [ens_zero1(1); entrans4(:,1)], [ens_zero1(2); entrans4(:,2)], 'o');
cla;
h1 = plot([0 Ls4-0.5], [ens_zero2(1) gentec./ecat], 'x');
h2 = errorbar([0 Ls4_], [ens_zero2(1); entrans4_(:,1)], [ens_zero2(2); entrans4_(:,2)], 'o');
set([h1 h2], 'LineWidth', 2, 'MarkerSize', 10);
xlabel('L / mm'); ylabel('T / a.u.'); 
title({'Run 4: L-scan at p=50 mbar' ['$n_e = ' num2str(50/31, '%2.1f') '\times 10^{18} \mathrm{cm}^{-3}$']});
make_latex(hfig); setAllFonts(hfig, 16);
if saving; export_fig([savfol 'r004_transmission'], '-nocrop', '-pdf', hfig); end

%% And now try to do length scans: run 6
nshots_ = 76;
% Get data for all shots: gas pressure and ecat energy and gentec energy
% and exitmode
pres6 = zeros(1,nshots_); gentec = zeros(1,nshots_); ecat = zeros(1,nshots_);
exitmodes = zeros(1024,1024,nshots_);
Ls6 = [12.5 12.5 12.5 3 3 3 4 4 4 6 6 6 8 8 8 10 10 10 12 12 12 14 14 14 16 16 16 18 18 18 20 20 20 22 22 22 24 18 18 18 18 18 18 20 20 20 20 20 20 20 20 22 22 22 22 22 28 28 24 24 24 26 26 26 28 28 28 30 30 30 35 35 35 40 40 40];
Ls6_ = fliplr(unique(Ls6));
for ss = 1:nshots_
    fprintf('Extracting data for shot %i\n', ss);
    gentec(ss) = getGemini2015ExitEnergy(20150903, 6, ss);
    ecat(ss) = getenergy(getGSN(20150903, 6, ss));
    gasdat = getGemini2015Gas(20150903, 6, ss);
    if ~isempty(gasdat); pres6(ss) = mean(gasdat(gasdat(:,1)<498 & gasdat(:,1)>490, 2)); end
    try
        exitmodes(:,:,ss) = getGemini2015ExitMode(20150903, 6, ss);
    catch 
    end
end
% Fix errors:
ecat(44) = nan; ecat(43) = 18.13;
%
entrans6 = zeros(length(Ls6_), 2); entrans6_ = zeros(length(Ls6_), 2);
pres6_ = zeros(size(Ls6_));
for i=1:length(Ls6_)
    % So in addition to everything else, we forgot about the superbox for 10 shots...
    shots_ = (1:nshots_).*(Ls6_(i)==Ls6); shots_ = shots_(shots_~=0);
    pres6_(i) = mean(pres6(shots_));
    % Avoid NANs everywhere... Additionally, avoid zeros on gentec
    logm = shots_( ~isnan(gentec(shots_)) & ~isnan(ecat(shots_)) & gentec(shots_)~=0 & abs(pres6(shots_)-2.05e18)<1e17);
    entrans6(i,1) = mean(gentec(logm));
    entrans6(i,2) = std(gentec(logm));
    entrans6_(i,1) = mean(gentec(logm)./ecat(logm));
    entrans6_(i,2) = std(gentec(logm)./ecat(logm));
end
% Plot these, with initial data points
hfig = figure(78); clf(hfig);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
h1 = plot([0 Ls6], [ens_zero1(1) gentec], 'o');
h2 = errorbar([0 Ls6_], [ens_zero1(1); entrans6(:,1)], [ens_zero1(2); entrans6(:,2)], 'o');
cla;
h1 = plot([0 Ls6-0.5], [ens_zero2(1) gentec./ecat], 'x');
h2 = errorbar([0 Ls6_], [ens_zero2(1); entrans6_(:,1)], [ens_zero2(2); entrans6_(:,2)], 'o');
set([h1 h2], 'LineWidth', 2, 'MarkerSize', 10);
xlabel('L / mm'); ylabel('T / a.u.'); 
title({'Run 6: L-scan at p=80 mbar' ['$n_e = ' num2str(80/31, '%2.1f') '\times 10^{18} \mathrm{cm}^{-3}$']});
make_latex(hfig); setAllFonts(hfig, 16);
if saving; export_fig([savfol 'r006_transmission'], '-nocrop', '-pdf', hfig); end

%% And now try to do length scans: run 7
nshots_ = 22;
% Get data for all shots: gas pressure and ecat energy and gentec energy
% and exitmode
pres7 = zeros(1,nshots_); gentec = zeros(1,nshots_); ecat = zeros(1,nshots_);
exitmodes = zeros(1024,1024,nshots_);
Ls7 = [40 40 40 36 36 36 32 32 32 28 28 28 26 26 26 24 24 24 22 22 22 22];
Ls7_ = fliplr(unique(Ls7));
for ss = 1:nshots_
    fprintf('Extracting data for shot %i\n', ss);
    gentec(ss) = getGemini2015ExitEnergy(20150903, 7, ss);
    ecat(ss) = getenergy(getGSN(20150903, 7, ss));
    gasdat = getGemini2015Gas(20150903, 7, ss);
    if ~isempty(gasdat); pres6(ss) = mean(gasdat(gasdat(:,1)<498 & gasdat(:,1)>490, 2)); end
    try
        exitmodes(:,:,ss) = getGemini2015ExitMode(20150903, 7, ss);
    catch 
    end
end
% Fix errors:
%ecat(44) = nan; ecat(43) = 18.13;
%
entrans7 = zeros(length(Ls7_), 2); entrans7_ = zeros(length(Ls7_), 2);
pres7_ = zeros(size(Ls7_));
for i=1:length(Ls7_)
    % So in addition to everything else, we forgot about the superbox for 10 shots...
    shots_ = (1:nshots_).*(Ls7_(i)==Ls7); shots_ = shots_(shots_~=0);
    pres7_(i) = mean(pres7(shots_));
    % Avoid NANs everywhere... Additionally, avoid zeros on gentec
    logm = shots_( ~isnan(gentec(shots_)) & ~isnan(ecat(shots_)) & gentec(shots_)~=0);% & abs(pres7(shots_)-2.05e18)<1e17);
    entrans7(i,1) = mean(gentec(logm));
    entrans7(i,2) = std(gentec(logm));
    entrans7_(i,1) = mean(gentec(logm)./ecat(logm));
    entrans7_(i,2) = std(gentec(logm)./ecat(logm));
end
% Plot these, with initial data points
hfig = figure(78); clf(hfig);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
h1 = plot([0 Ls7], [ens_zero1(1) gentec], 'o');
h2 = errorbar([0 Ls7_], [ens_zero1(1); entrans7(:,1)], [ens_zero1(2); entrans7(:,2)], 'o');
cla;
h1 = plot([0 Ls7-0.5], [ens_zero2(1) gentec./ecat], 'x');
h2 = errorbar([0 Ls7_], [ens_zero2(1); entrans7_(:,1)], [ens_zero2(2); entrans7_(:,2)], 'o');
set([h1 h2], 'LineWidth', 2, 'MarkerSize', 10);
xlabel('L / mm'); ylabel('T / a.u.'); 
title({'Run 7: L-scan at p=90 mbar' ['$n_e = ' num2str(90/31, '%2.1f') '\times 10^{18} \mathrm{cm}^{-3}$']});
make_latex(hfig); setAllFonts(hfig, 16);
if saving; export_fig([savfol 'r007_transmission'], '-nocrop', '-pdf', hfig); end

%% Plot all length scans:
hfig = figure(78); clf(hfig);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
pps = [40 50 80 90]/31*1e18;
Lf = [0 4 5 7]; %Length fudge factor :)
Lf = zeros(1,4)*3;
if 0
    h1 = errorbar([0 Ls3_], [ens_zero2(1); entrans3_(:,1)], [ens_zero2(2); entrans3_(:,2)], 'o');
    h2 = errorbar([0 Ls4_], [ens_zero2(1); entrans4_(:,1)], [ens_zero2(2); entrans4_(:,2)], 'o');
    h3 = errorbar([0 Ls6_], [ens_zero2(1); entrans6_(:,1)], [ens_zero2(2); entrans6_(:,2)], 'o');
    h4 = errorbar([0 Ls7_], [ens_zero2(1); entrans7_(:,1)], [ens_zero2(2); entrans7_(:,2)], 'o');
    fnam = 'Lscans_ne'; xlabel('L / mm');
else
    h1 = errorbar([0 pps(1)*(Ls3_+Lf(1))], [ens_zero2(1); entrans3_(:,1)], [ens_zero2(2); entrans3_(:,2)], 'o');
    h2 = errorbar([0 pps(2)*(Ls4_+Lf(2))], [ens_zero2(1); entrans4_(:,1)], [ens_zero2(2); entrans4_(:,2)], 'o');
    h3 = errorbar([0 pps(3)*(Ls6_+Lf(3))], [ens_zero2(1); entrans6_(:,1)], [ens_zero2(2); entrans6_(:,2)], 'o');
    h4 = errorbar([0 pps(4)*(Ls7_+Lf(4))], [ens_zero2(1); entrans7_(:,1)], [ens_zero2(2); entrans7_(:,2)], 'o');    
    fnam = 'Lscans_na'; xlabel('$n_a / \mathrm{cm}^{-2}$');
end
for i=1:4; leglabs{i} = sprintf('$n_e=%2.1e $', pps(i)); end;
legend([h1 h2 h3 h4], leglabs);
ylabel('T / a.u.'); 
title('Transmission for length scans');
make_latex(hfig); setAllFonts(hfig, 16);
set([h1 h2 h3 h4], 'LineWidth', 1.5, 'MarkerSize', 7);
if saving; export_fig([savfol fnam], '-nocrop', '-pdf', hfig); end

%% All scans, with fudge for lengths, areal density 
hfig = figure(78); clf(hfig);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);

Lf = [2 4 5 5]; %Length fudge factor :)
Lf = [0 0 0 0];
h1 = errorbar([0 0.1*pps(1)*(Ls3_+Lf(1))], [ens_zero2(1); entrans3_(:,1)], [ens_zero2(2); entrans3_(:,2)], 'o');
h2 = errorbar([0 0.1*pps(2)*(Ls4_+Lf(2))], [ens_zero2(1); entrans4_(:,1)], [ens_zero2(2); entrans4_(:,2)], 'o');
h3 = errorbar([0 0.1*pps(3)*(Ls6_+Lf(3))], [ens_zero2(1); entrans6_(:,1)], [ens_zero2(2); entrans6_(:,2)], 'o');
h4 = errorbar([0 0.1*pps(4)*(Ls7_+Lf(4))], [ens_zero2(1); entrans7_(:,1)], [ens_zero2(2); entrans7_(:,2)], 'o');
h5 = errorbar([0; 2*pres2_], [ens_zero2(1); entrans2_(:,1)], [ens_zero2(2); entrans2_(:,2)], 'o');
h6 = errorbar([0; 1.25*pres5_], [ens_zero2(1); entrans5_(:,1)], [ens_zero2(2); entrans5_(:,2)], 'o');
for i=1:4; leglabs{i} = sprintf('$n_e=%2.1e $', pps(i)); end;
leglabs{5} = 'L = 20 mm'; leglabs{6} = 'L = 12.5 mm';
legend([h1 h2 h3 h4 h5 h6], leglabs);
make_latex(hfig); setAllFonts(hfig, 16);
set([h1 h2 h3 h4 h5 h6], 'LineWidth', 1.5, 'MarkerSize', 7);
set(gca, 'XLim', [-0.05 15e18]);