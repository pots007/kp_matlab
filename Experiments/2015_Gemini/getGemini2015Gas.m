% getGemini2015Gas.m

% Returns the data for Gemini 2015 Gas traces, in handy ne
% Returns empty if no data for that shot

% UPDATE 25/04/2016: Using Juffalo's Density calibration
% UPDATE 22/07/2016: Using corrected Juff' cal (his one is for shotsheet pressures!)

% Accepts either date, run, shot or correctly formatted shot string
% (YYYYMMDDrRRRsSSS)

function [trace, data] = getGemini2015Gas(date,run,shot)
if nargin==1 && ischar(date) && length(date)==16
    shotstr = date;
elseif nargin==3
    shotstr = sprintf('%ir%03is%03i', date, run, shot);
else
    trace = nan;
    return;
end
if ispc
    datapath = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\Data';
elseif ismac
    datapath = '/Volumes/Gemini2015/Data';
end
fname = fullfile(datapath, shotstr(1:8), shotstr(1:12), [shotstr '_GasPressure.txt']);
try
    data = importdata(fname);
    if isempty(data); trace = []; return; end
    if isstruct(data); data = data.data; end;
    t = data(:,1);
    V1 = (data(:,2)-4)*12.5;
    V1 = V1*1e18/24.8;%31;
    V2 = (data(:,3)-4)*12.5;
    V2 = V2*1e18/24.8;%31;
    %[~, ind] = min(abs(t-490));
    %ne = V1(ind);
    trace = [t V1 V2];
catch err
    disp(['Error getting data for ' shotstr ': ' err.message]);
    trace = [];
end
end