dpath = getDropboxPath;
datpath = [dpath 'Writing/RadiationGemini2015/'];
fils = dir_names([datpath '*.txt']);
% Photons first
mu_en_rho = importdata('SoftTissuePhotons.dat');

% Charged particles
dE_dx = importdata('SoftTissueElectrons.dat');
% Convert MeV cm^-2/g to Jcm^-2/kg
dE_dx(:,2) = dE_dx(:,2)*(1e6*qe)/1e-3;

% Neutrons
dose_E = importdata('SoftTissueNeutrons.dat');
% Approximate area, assume beam get 10 times larger in the shielding
S = (0.01*5*10)^2*0.25*pi*1e4;

sims = {'concrete_100cm','concrete_10010cm','concrete_10020cm','concrete_110cm','concrete_120cm'};
%sims = {'lead_70cm', 'lead_80cm', 'lead_90cm', 'lead_7020cm', 'lead_7020cmH'};
% dose will be phot_f, phot_r, neut_f, neut_r, part_f, part_r
dose = zeros(6,length(sims));
for i=1:length(sims)
    photons_f = importdata([datpath sims{i} '_photons_front.txt']);
    nitems  = sum(photons_f(2:end,1) < max(mu_en_rho(:,1)));
    % interpolate the energy absorption to histogram points
    int_endeps = interp1(mu_en_rho(:,1), mu_en_rho(:,3),photons_f(2:nitems,1));
    % Factor 1e9*qe to convert from MeV/g to J/kg
    %dose(1,i) = sum(int_endeps.*photons_f(2:nitems,1).*photons_f(2:nitems,2))*1e9*qe
    dose(1,i) = sum(photons_f(2:end,1).*photons_f(2:end,2))*1e6*qe;
    photons_r = importdata([datpath sims{i} '_photons_rear.txt']);
    %dose(2,i) = sum(int_endeps.*photons_r(2:nitems,1).*photons_r(2:nitems,2))*1e9*qe
    dose(2,i) = sum(photons_r(2:end,1).*photons_r(2:end,2))*1e6*qe;
    if (exist([datpath sims{i} '_particles_front.txt'], 'file'))
        particles_f = importdata([datpath sims{i} '_particles_front.txt']);
        nitems  = sum(particles_f(2:end,1) < max(dE_dx(:,1)));
        int_endeps = interp1(dE_dx(:,1), dE_dx(:,2),particles_f(2:nitems,1));
        % Divide by 20 as have one shot per 20s
        %dose(5,i) = sum(int_endeps.*particles_f(2:nitems,2))/20;
        dose(5,i) = sum(particles_f(2:end,1).*particles_f(2:end,2))*1e6*qe;
    else
        dose(5,i) = 0;
    end
    if (exist([datpath sims{i} '_particles_rear.txt'], 'file'))
        particles_r = importdata([datpath sims{i} '_particles_rear.txt']);
        nitems  = sum(particles_r(2:end,1) < max(dE_dx(:,1)));
        int_endeps = interp1(dE_dx(:,1), dE_dx(:,2),particles_r(2:nitems,1));
        %dose(6,i) = sum(int_endeps.*particles_r(2:nitems,2))/20;
        dose(6,i) = sum(particles_r(2:end,1).*particles_r(2:end,2))*1e6*qe;
    else
        dose(6,i) = 0;
    end
    
    neutrons_f = importdata([datpath sims{i} '_neutrons_front.txt']);
    nitems  = sum(neutrons_f(2:end,1) < max(dose_E(:,1)));
    int_endeps = interp1(dose_E(:,1), dose_E(:,2),neutrons_f(2:nitems,1));
    W_r = [neutrons_f(2:nitems,1) ones(nitems-1,1)*5];
    W_r(W_r(:,1)>0.1&W_r(:,1)<2,2) = 20;
    W_r(W_r(:,1)>2&W_r(:,1)<20,2) = 10;
    %dose(3,i) = sum(int_endeps.*neutrons_f(2:nitems,2).*W_r(:,2))*S;
    dose(3,i) = sum(neutrons_f(2:end,1).*neutrons_f(2:end,2))*1e6*qe*20;
    neutrons_r = importdata([datpath sims{i} '_neutrons_rear.txt']);
    nitems  = sum(neutrons_r(2:end,1) < max(dose_E(:,1)));
    int_endeps = interp1(dose_E(:,1), dose_E(:,2),neutrons_r(2:nitems,1));
    %dose(4,i) = sum(int_endeps.*neutrons_r(2:nitems,2).*W_r(:,2))*S;
    dose(4,i) = sum(neutrons_r(2:end,1).*neutrons_r(2:end,2))*1e6*qe*20;
end

% The simulations are for only 0.16pC, realistically have 3 order of
% magnitude more! 1e3 to get to mSv.
dose = dose.*500*1e3
%%

fprintf('%15s & %16s & %16s & %20s & %20s & %20s & %10s\n', '',...
    '100cm concrete', '100cm conc+10cm Pb',...
    '100cm conc+20cm Pb', '10cm Pb+100cm conc', '20cm Pb+100cm conc', '70cm Pb');
parts = {'photons,front', 'photons,rear', 'neutrons,front', 'neutrons,rear',...
    'charged,front', 'charged,rear'};
for i=1:6
    fprintf('%15s & %16.3f & %16.3f & %20.3f & %20.3f & %20.3f & %10.3f\n', parts{i}, dose(i,:));
end
fprintf('%15s & %16.3f & %16.3f & %20.3f & %20.3f & %20.3f & %10.3f\n\n', 'Total front', sum(dose(1:2:6,:)));
fprintf('%15s & %16.3f & %16.3f & %20.3f & %20.3f & %20.3f & %10.3f\n\n', 'Total rear', sum(dose(2:2:6,:)));
%% Clean terminal version

fprintf('%15s\t%16s\t%16s\t%20s\t%20s\t%20s\t%10s\n', '', '100cm concrete', '100cm conc+10cm Pb',...
    '100cm conc+20cm Pb', '10cm Pb+100cm conc', '20cm Pb+100cm conc', '70cm Pb');
for i=1:6
    fprintf('%15s\t%16.3f\t%16.3f\t%20.3f\t%20.3f\t%20.3f\t%10.3f\n', parts{i}, dose(i,:));
end
fprintf('%15s\t%16.3f\t%16.3f\t%20.3f\t%20.3f\t%20.3f\t%10.3f\n', 'Total', sum(dose));