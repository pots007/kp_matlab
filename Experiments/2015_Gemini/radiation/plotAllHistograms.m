dpath = getDropboxPath;
datpath = [dpath 'Writing/RadiationGemini2015/'];
fils = dir_names([datpath '*.txt']);

sims = {'concrete_100cm', 'concrete_10010cm', 'concrete_10020cm',...
    'concrete_110cm', 'concrete_120cm'};

legs = {'100cm Con', '100cm Con + 10cm Pb', '100cm Con + 20cm Pb', ...
    '10cm Pb + 100cm Con', '20cm Pb + 100cm Con'};

parts = {'_photons_front', '_photons_rear', '_neutrons_front', '_neutrons_rear',...
    '_particles_front', '_particles_rear'};

titls = {'photons, front', 'photons, rear', 'neutrons, front', 'neutrons, rear',...
    'charged, front', 'charged, rear'};

cols = {'r', 'k', 'g', 'b', 'm', 'c'};
data = cell(6,7);
figure(780); clf;
set(780, 'Position', [1000 600 800 600]);
set(gca, 'NextPlot', 'add', 'YScale', 'log');

for i=1:6
    figure(780); clf;
    set(gca, 'NextPlot', 'add', 'YScale', 'log', 'Box', 'on');
    hs = zeros(1,6);
    for k=1:5
        if (exist([datpath sims{k} parts{i} '.txt'], 'file'))
            dat = importdata([datpath sims{k} parts{i} '.txt']);
            %data{i,k} = dat;
            hs(k) = stairs(dat(:,1), dat(:,2), cols{k}, 'LineWidth', 1.5);
        end
        %data{i,k} = dat;
        %hs(k) = stairs(dat(:,1), dat(:,2), cols{k});
    end
    title(titls{i});
    set(gca, 'XLim', [0 50]);
    if (i==2) set(gca, 'XLim', [0 100]); end
    if (i==4) set(gca, 'XLim', [0 20]); end
    if (i==5) set(gca, 'XLim', [0 100]); end
    if (i==6) set(gca, 'XLim', [0 500]); end
    legend(hs, legs(1:5), 'Location', 'NorthEast');
    xlabel('Energy / MeV');
    ylabel('Counts');
    setAllFonts(780,24);
    export_fig([datpath parts{i}(2:end)], '-nocrop', '-pdf', '-transparent', 780);
    pause(.5)
end
%% The higher energy part

sims = {'lead_70cm', 'lead_80cm', 'lead_90cm', 'lead_7020cm', 'lead_7020cmH'};

legs = {'70cm Pb', '80cm Pb', '90cm Pb', '70cm Pb + 20cm Plex', '70cm Pb + 20cm Borotron'};

for i=1:6
    figure(780); clf;
    set(gca, 'NextPlot', 'add', 'YScale', 'log', 'Box', 'on');
    hs = zeros(1,6);
    for k=1:5
        if (exist([datpath sims{k} parts{i} '.txt'], 'file'))
            dat = importdata([datpath sims{k} parts{i} '.txt']);
            %data{i,k} = dat;
            hs(k) = stairs(dat(:,1), dat(:,2), cols{k}, 'LineWidth', 1.5);
        end
        %data{i,k} = dat;
        %hs(k) = stairs(dat(:,1), dat(:,2), cols{k});
    end
    title(titls{i});
    set(gca, 'XLim', [0 50]);
    if (i==2) set(gca, 'XLim', [0 10]); end
    if (i==4) set(gca, 'XLim', [0 20]); end
    if (i==5) set(gca, 'XLim', [0 100]); end
    if (i==6) set(gca, 'XLim', [0 10]); end
    legend(hs, legs, 'Location', 'NorthEast');
    xlabel('Energy / MeV');
    ylabel('Counts');
    setAllFonts(780,24);
    export_fig([datpath parts{i}(2:end) '_high'], '-nocrop', '-pdf', '-transparent', 780);
    pause(.5)
end
