% Compare the stopping powers of Pb and standard concrete

conc = importdata('Concrete_CSDA.pl');
conc = conc.data;
Pb  = importdata('PB_CSDA.pl');
Pb = Pb.data;

figure(78); clf;
h1 = plot(conc(:,1), conc(:,2)/2.4, '-ok');
set(gca, 'NextPlot', 'add');
h2 = plot(Pb(:,1), Pb(:,2)/11.34, '-or');
set(gca, 'XScale', 'log', 'YScale', 'lin');
ylabel('CSDA range / cm')
xlabel('Electron energy / MeV');
legend([h1 h2], {'Concrete', 'Lead'}, 'Location', 'NorthWest');

setAllFonts(78,20);

export_fig('CSDA_ranges.pdf', '-nocrop', '-transparent', '-pdf', 78);
