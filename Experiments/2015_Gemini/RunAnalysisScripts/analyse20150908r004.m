% If set up properly, this will analyse 20150907r006

date_ = '20150908';
run_ = '004';

nshots = 43;
ttt = struct;
os = @(x) ones(1,x);
pres_ = [50.4*os(3) 56.8*os(4) 64.5*os(4) 73.0*os(6) 85.4*os(7) 100*os(6) 125*os(7) 150*os(6)];
pres_(41) = 135;

% Save as .figs
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 4);
% Save into the correct path
if ismac
    savepath = ['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_];
end

MagnetTrackerKP.fig.btrac.dat.outpath = fullfile(savepath, 'mat');
set(MagnetTrackerKP.fig.btrac.outpath_, 'String', fullfile(savepath, 'mat'));
datee = java.util.Date();
datee.setYear(2015-1900);
datee.setMonth(str2double(date_(5:6))-1);
datee.setDate(str2double(date_(7:8)));
MagnetTrackerKP.fig.input.date.setSelectedDate(datee);
MagnetTrackerKP.date_selected;
set(MagnetTrackerKP.fig.input.runbox, 'Value', str2double(run_));
MagnetTrackerKP.run_selected(str2double(run_));

%%

for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(str2double(date_), str2double(run_), i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).pres = pres_(i);
    ttt(i).cellL = 20;
    dat = getGemini2015Gas(str2double(date_), str2double(run_), i);
    if ~isempty(dat);
        ttt(i).gastrace = dat(55000:60000, 1:2);
    else
        ttt(i).gastrace = 0;
    end
    ttt(i).exitenergy = getGemini2015ExitEnergy(str2double(date_), str2double(run_), i);
    ttt(i).Q1 = MagnetTrackerKP.fig.btrac.dat.QandE(1,1);
    ttt(i).Q2 = MagnetTrackerKP.fig.btrac.dat.QandE(2,1);
    ttt(i).Q3 = MagnetTrackerKP.fig.btrac.dat.QandE(3,1);
    ttt(i).E1 = MagnetTrackerKP.fig.btrac.dat.QandE(1,2);
    ttt(i).E2 = MagnetTrackerKP.fig.btrac.dat.QandE(2,2);
    ttt(i).E3 = MagnetTrackerKP.fig.btrac.dat.QandE(3,2);
    drawnow;
    toc
end

corrections = [];
for i=1:size(corrections,1); ttt(corrections(i,1)).laserE = corrections(i,2); end;

if ismac
    save(['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_ '/rundata'], 'ttt');
end
