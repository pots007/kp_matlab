% If set up properly, this will analyse 20150910r006


nshots = 32;
ttt = struct;
% Save as .figs
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 3);
% Save into the correct path
MagnetTrackerKP.fig.btrac.dat.outpath = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150910r004/fig/';
set(MagnetTrackerKP.fig.btrac.outpath_, 'String',...
    '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150910r004/fig/');
datee = java.util.Date();
datee.setYear(2015-1900);
datee.setMonth(9-1);
datee.setDate(10);
MagnetTrackerKP.fig.input.date.setSelectedDate(datee);
MagnetTrackerKP.date_selected;
set(MagnetTrackerKP.fig.input.runbox, 'Value', 4);
MagnetTrackerKP.run_selected(4);

for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(20150910, 4, i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).pres = 80;
    ttt(i).cellL = 20;
    ttt(i).Q1 = str2double(get(MagnetTrackerKP.fig.btrac.Q1_, 'String'));
    ttt(i).Q2 = str2double(get(MagnetTrackerKP.fig.btrac.Q2_, 'String'));
    ttt(i).Q3 = str2double(get(MagnetTrackerKP.fig.btrac.Q3_, 'String'));
    ttt(i).E1 = str2double(get(MagnetTrackerKP.fig.btrac.E1_, 'String'));
    ttt(i).E2 = str2double(get(MagnetTrackerKP.fig.btrac.E2_, 'String'));
    ttt(i).E3 = str2double(get(MagnetTrackerKP.fig.btrac.E3_, 'String'));
    toc
end

if ismac
    save('/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150910r004/rundata', 'ttt');
end