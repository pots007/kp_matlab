% This ought to do the fitting for acceleration gradients for us, for runs
% 4, 5 and 6 from 0902.
fnamesload = {fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', '20150902r004', 'EspecPeakEns'),...
    fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', '20150902r006', 'EspecPeakEns'),...
    fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', '20150902r005', 'EspecPeakEns')};
savfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', 'AccelerationFits');
faccFit = fullfile(savfol, 'AccFits.mat');
if exist(faccFit, 'file')
    fdat = load(faccFit); accFits = fdat.accFits;
else
    %accFits = struct;
end


hfig = 89;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 600 450]);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'Position', [0.18 0.17 0.8 0.8]); ax1 = gca;
ax1h = [];
posns = [200 800 600 450; 900 200 600 450; 900 800 600 450];
nes = [80 70 100]/31;
for i=2%1:3
    fload = load(fnamesload{i});
    ffnames = fieldnames(fload);
    ff = fload.(ffnames{1});
    hfig = 89+i;
    figure(hfig); clf(hfig);
    set(hfig, 'Color', 'w', 'Position', posns(i,:));
    set(gca, 'NextPlot', 'add', 'Box', 'on');
    % errorbar for simplistic analysis
    logm = ff.Ls<17;
    if i==3; logm = ff.Ls<13; end;
    %h1 = errorbar(edata(3,logm), edata(6,logm), edata(5,logm), edata(7,logm), 'o', 'LineWidth', 1.5);
    h1 = plot(ff.Ls(ff.peakEns>300)-0.2, ff.peakEns(ff.peakEns>300), '.k', 'MarkerSize', 10, 'LineWidth', 2);
    logmav = 1:11; % This reduces the data to up to 16mm
    if i==3; logmav = 1:8; end;
    L_vs_ens_mean  = cellfun(@mean, ff.L_vs_ens); L_vs_ens_std  = cellfun(@std, ff.L_vs_ens);
    h2 = errorbar(ff.L_unique(logmav), L_vs_ens_mean(logmav), L_vs_ens_std(logmav), '-*', 'LineWidth', 2, 'Color', 0.34*[1 1 1]);
    %ax1h(i) = errorbar(ax1, ff.L_unique(logmav)+(2-i)*0.25, L_vs_ens_mean(logmav), L_vs_ens_std(logmav), '-*', 'LineWidth', 2);
    ax1h(i) = plot(ax1, ff.L_unique(logmav)+(2-i)*0.25, L_vs_ens_mean(logmav), '-*', 'LineWidth', 2);
    % Now add a fit to the acceleration: a normal parabola
    parabfit = polyfit(ff.Ls(logm), ff.peakEns(logm)', 2);
    parabfit_ = fit(ff.Ls(logm)', ff.peakEns(logm), 'poly2');
    plotL1 = 3:0.1:16;
    h3 = plot(plotL1, polyval(parabfit, plotL1), '--r', 'LineWidth', 2.5);
    % And now, reflect the accelerating part about the max and fit a parabola
    % to that: effectively a onesided parabola fit
    if i==1 %|| i==2
        logm_ = ff.Ls<10;
        ensloc = ff.peakEns(logm_); Lloc = ff.Ls(logm_);
        [Lloc, inds] = sort(Lloc); ensloc = ensloc(inds);
        logm3 = (Lloc==3)*13; logm4 = (Lloc==4)*11;
        logm5 = (Lloc==5)*9; logm6 = (Lloc==6)*7;
        logm7 = (Lloc==7)*5; logm8 = (Lloc==8)*3;
        logm9 = (Lloc==9)*1;
        templens = [Lloc Lloc+logm3+logm4+logm5+logm6+logm7+logm8+logm9];
        tempens = [ensloc' ensloc'];
    elseif i==2
        logm_ = ff.Ls<11;
        ensloc = ff.peakEns(logm_); Lloc = ff.Ls(logm_);
        [Lloc, inds] = sort(Lloc); ensloc = ensloc(inds);
        logm3 = (Lloc==3)*14; logm4 = (Lloc==4)*12;
        logm5 = (Lloc==5)*10; logm6 = (Lloc==6)*8;
        logm7 = (Lloc==7)*6; logm8 = (Lloc==8)*4;
        logm9 = (Lloc==9)*1; logm10= (Lloc==10)*1;
        templens = [Lloc Lloc+logm3+logm4+logm5+logm6+logm7+logm8+logm9];
        tempens = [ensloc' ensloc'];
    elseif i==3
        logm_ = ff.Ls<7;
        ensloc = ff.peakEns(logm_); Lloc = ff.Ls(logm_);
        [Lloc, inds] = sort(Lloc); ensloc = ensloc(inds);
        logm3 = (Lloc==3)*7; logm4 = (Lloc==4)*5;
        logm5 = (Lloc==5)*3; logm6 = (Lloc==6)*1;
        templens = [Lloc Lloc+logm3+logm4+logm5+logm6];
        tempens = [ensloc' ensloc'];
    end
    accelfit = polyfit(templens, tempens, 2);
    accelfit_ = fit(templens', tempens', 'poly2');
    plotL2 = 3:0.1:17;
    h4 = plot(plotL2, polyval(accelfit, plotL2), '--m', 'LineWidth', 2.5);
    xlabel('Cell length / mm');
    ylabel('Maximum energy / MeV');
    legend([h1 h2 h3 h4], {'Shot $\mathcal{E}$', 'Average at each length', 'Fit to acc. and dec.', 'Fit to acc. only'},...
        'Location', 'South', 'Box', 'off');
    set(gca, 'Position', [0.18 0.17 0.8 0.8], 'Box', 'on', 'LineWidth', 2,...
        'XLim', [2 20], 'YLim', [400 1800]);
    % Add plasma density and Ep fit value
    text(19, 1700, ['$n_e = ' num2str(nes(i), '%2.1f') ' \times 10^{18} \mathrm{cm}^{-3}$'], 'HorizontalAlignment', 'right');

    make_latex(hfig);
    setAllFonts(hfig,20);
    %export_fig(fullfile(savfol, ffnames{1}), '-pdf', '-nocrop', hfig);
    % Line below saves it into the good folder!
    %export_fig(fullfile(getDropboxPath, 'Writing', '2016_StudentSeminar', 'PeakEn_vs_celLL_fits'), '-pdf', '-nocrop', hfig);
    % And let's output the numbers, in useful terms:
    %E_peak_acc = [parabfit(2) accelfit(2)];
    E_peak_acc = [parabfit_.p2 accelfit_.p2];
    %L_dephasing = -E_peak_acc./(2*[parabfit(1) accelfit(1)]);
    L_dephasing = -E_peak_acc./(2*[parabfit_.p1 accelfit_.p1]);
    % And the errors as well
    cints = [confint(parabfit_) confint(accelfit_)];
    E_peak_errors = [mean(abs(cints(:,2)-E_peak_acc(1))) mean(abs(cints(:,5)-E_peak_acc(2)))];
    fit_types = {'Full length scan', 'Acceleration only'};
    fprintf('\n%20s\t%10s\t%10s\t%10s\n', 'Fit type', 'E_p (GeV/m)', 'E_err (GeV/m)', 'L_d (mm)');
    for k=1:2
        fprintf('%20s\t%10.0f\t%10.1f\t%10.1f\n', fit_types{k}, E_peak_acc(k), E_peak_errors(k), L_dephasing(k));
    end
    
    % Save these data into a collage file!
    if sum(strcmpi({accFits.run}, ffnames{1}))
        indd = sum(strcmpi({accFits.run}, ffnames{1}).*(1:length(accFits)));
    else 
        indd = length(accFits)+1;
    end
    accFits(indd).run = ffnames{1};
    accFits(indd).ne = nes(i);
    accFits(indd).laserE = mean(ff.laserE);
    accFits(indd).accE = E_peak_acc(2);
    accFits(indd).accEerr = E_peak_errors(2);
    accFits(indd).accLdph = L_dephasing(2);
    accFits(indd).fullE = E_peak_acc(1);
    accFits(indd).fullEerr = E_peak_errors(1);
    accFits(indd).fullLdph = L_dephasing(1);
end

legend(ax1, ax1h, {'$n_e = 2.6\times 10^{18} \mathrm{cm}^{-3}$',...
    '$n_e = 3.2\times 10^{18} \mathrm{cm}^{-3}$',...
    '$n_e = 2.3\times 10^{18} \mathrm{cm}^{-3}$'}, 'Location', 'South', 'Box', 'off');
xlabel(ax1, 'Cell length / mm');
ylabel(ax1, 'Maximum energy / MeV');
setAllFonts(89, 20); make_latex(89);    
%export_fig(fullfile(savfol, 'D0902_all'), '-pdf', '-nocrop', 89);
save(faccFit, 'accFits');