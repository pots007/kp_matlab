% If set up properly, this will analyse 20150908r008

date_ = '20150908';
run_ = '008';

nshots = 40;
ttt = struct;
pres_ = [40*ones(1,3) 50*ones(1,3) 56.8*ones(1,3) 64.5*ones(1,3) 73.9*ones(1,5)];
pres_ = [pres_ 85.4*ones(1,5) 100*ones(1,5) 85.4*ones(1,5) 92*ones(1,8)];

% Save as .figs
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 4);
% Save into the correct path
if ismac
    savepath = ['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_];
end

% This is with a very tight ROI to get rid of noise near the high energy
% end. This box misses out Shot 39 as it is well outside the frame.
MagnetTrackerKP.fig.btrac.dat.outpath = fullfile(savepath, 'mat_tight');
set(MagnetTrackerKP.fig.btrac.outpath_, 'String', fullfile(savepath, 'mat_tight'));
datee = java.util.Date();
datee.setYear(2015-1900);
datee.setMonth(str2double(date_(5:6))-1);
datee.setDate(str2double(date_(7:8)));
MagnetTrackerKP.fig.input.date.setSelectedDate(datee);
MagnetTrackerKP.date_selected;
set(MagnetTrackerKP.fig.input.runbox, 'Value', str2double(run_));
MagnetTrackerKP.run_selected(str2double(run_));

%%

for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(str2double(date_), str2double(run_), i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).pres = pres_(i);
    dat = getGemini2015Gas(str2double(date_), str2double(run_), i);
    ttt(i).gastrace = dat(55000:60000, 1:2);
    ttt(i).cellL = 20;
    ttt(i).exitenergy = getGemini2015ExitEnergy(str2double(date_), str2double(run_), i);
    ttt(i).Q1 = str2double(get(MagnetTrackerKP.fig.btrac.Q1_, 'String'));
    ttt(i).Q2 = str2double(get(MagnetTrackerKP.fig.btrac.Q2_, 'String'));
    ttt(i).Q3 = str2double(get(MagnetTrackerKP.fig.btrac.Q3_, 'String'));
    ttt(i).E1 = str2double(get(MagnetTrackerKP.fig.btrac.E1_, 'String'));
    ttt(i).E2 = str2double(get(MagnetTrackerKP.fig.btrac.E2_, 'String'));
    ttt(i).E3 = str2double(get(MagnetTrackerKP.fig.btrac.E3_, 'String'));
    drawnow;
    toc
end

if ismac
    save(['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_ '/rundata'], 'ttt');
end
%save(fullfile(savepath, 'rundata'),'ttt');
%convertFig2Image(fullfile(savepath, 'fig'), 'png', savepath);