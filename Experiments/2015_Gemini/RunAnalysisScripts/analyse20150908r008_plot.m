%This ought to look at the file analysed by especPeakFinding.m

%20150826r006, Gemini

if ismac
    datafol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150908r008/mat';
    savfol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150908r008';
elseif ispc
    datafol = 'E:\Kristjan\Google Drive\Experiment\2015Gemini\EspecAnalysis\20150908r008\mat';
    savfol = 'E:\Kristjan\Google Drive\Experiment\2015Gemini\EspecAnalysis\20150908r008\';
end

rundat = load(fullfile(savfol, 'rundata'));

flist = dir(fullfile(datafol, '*Espec1.mat'));
fnames1 = {flist.name};
flist = dir(fullfile(datafol, '*Espec2LE.mat'));
fnames2 = {flist.name};

edata = zeros(11,length(fnames1));
poo = zeros(size(fnames1));

% And for other tracking files:
ff = load('tracking_4mrad_1mrad_beam');
alongs = ff.tracking.screen(1).alongscreen;
logm = ~isnan(alongs);
lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');
% Switch to on Axis tracking, ie from file, or this
axtrack = 0;
%%
for i=1:length(fnames1)
    fprintf('File %i out of %i\n', i, length(fnames1));
    ll = load(fullfile(datafol, fnames1{i}));
    poo(i) = ll.shotdata.poo;
    if poo(i); continue; end
    shotno = str2double(fnames1{i}(14:16));
    edata(1,i) = shotno;
    edata(2,i) = rundat.ttt(i).laserE;
    edata(3,i) = rundat.ttt(i).pres;
    if isempty(rundat.ttt(i).exitenergy)
        edata(4,i) = nan;
    else
        edata(4,i) = rundat.ttt(i).exitenergy;
    end
    if ~isfield(ll.shotdata, 'max'); continue; end;
    if axtrack
        edata(5,i) = ll.shotdata.max.low;
        edata(6,i) = ll.shotdata.max.val;
        edata(7,i) = ll.shotdata.max.high;
        edata(8,i) = ll.shotdata.peak.low;
        edata(9,i) = ll.shotdata.peak.val;
        edata(10,i) = ll.shotdata.peak.high;
    else
        edata(6,i) = valfit(ll.shotdata.alongscreen(ll.shotdata.max.pixel));
        edata(5,i) = lowfit(ll.shotdata.alongscreen(ll.shotdata.max.pixel))-edata(6,i);
        edata(7,i) = -highfit(ll.shotdata.alongscreen(ll.shotdata.max.pixel))+edata(6,i);
        edata(9,i) = valfit(ll.shotdata.alongscreen(ll.shotdata.peak.pixel));
        edata(8,i) = lowfit(ll.shotdata.alongscreen(ll.shotdata.peak.pixel))-edata(9,i);
        edata(10,i) = -highfit(ll.shotdata.alongscreen(ll.shotdata.peak.pixel))+edata(9,i);
    end
    edata(11,:) = ll.shotdata.totQ;
end
poo = logical(poo);
edata = edata(:, ~poo);

%% Look at the gas as well!
totgas = zeros(40, 3001);
for i=1:40
    disp(i);
    dat = getGemini2015Gas(20150908, 8, i);
    plot(dat(:,1), dat(:,2), 'k-')
    totgas(i,:) = interp1(dat(:,1), dat(:,2), 400:0.1:700);
    drawnow;
end
% THis index here is common to all traces, and is just before the discharge dip
tgas = 400:0.1:700;
[~,ind] = min(abs(tgas-500.7));
shotpres = totgas(:,ind);

%% And now for plotting all kinds of things

%Plot the maximum and peak energy as a function of length
hfig = 87;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
set(gca, 'NextPlot', 'add');
%errorbar for simplistic analysis
hmax = errorbar(shotpres, edata(6,:), edata(5,:), edata(7,:), 'o');
hpeak = errorbar(shotpres, edata(9,:), edata(8,:), edata(10,:), 'or');
set([hmax, hpeak], 'MarkerSize', 9, 'LineWidth', 1.5);
%Average each length point!
press = unique(shotpres);
Es = zeros(size(press));
for i=1:length(press)
    logm = shotpres-press(i)<2e17;
    Es(i) = mean(edata(6,logm));
end
%h2 = plot(press, Es, '-k', 'LineWidth', 3);
%And add the dephasing length!

xlabel('Plasma density / $\mathrm{cm}^{-3}$');
ylabel('Electron energy / MeV');
legend([hmax hpeak], {'Max', 'Peak'}, 'Location', 'Northwest');
if axtrack
    title('Energies on screen 1, on axis tracking');
    fname = 'energies_ne';
else
    title('Energies on screen 1, 4 mrad above axis');
    fname = 'energies_ne_4mrad';
end
set(gca, 'Position', [0.12 0.13 0.85 0.79], 'Box', 'on', 'LineWidth', 2);
make_latex(hfig);
setAllFonts(hfig,20);
export_fig(fullfile(savfol, fname), '-pdf', '-nocrop', hfig);

%% To normalize things out, factor laser energies into the plot as well

hfig = 87;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
set(gca, 'NextPlot', 'add');
delfact = 0.6*0.9;
h1 = scatter(shotpres, edata(6,:), 75, edata(2,:)*delfact, 'd');
h2 = scatter(shotpres, edata(9,:), 75, edata(2,:)*delfact, 'o');
legend([h1 h2], {'Max', 'Peak'}, 'Location', 'Northwest');

cb = colorbar('Peer', gca);
ylabel(cb, 'Energy on target / J');
ylabel(gca, 'Electron energy / MeV');
xlabel(gca, 'Plasma density / $\mathrm{cm}^{-3}$');
set(gca, 'Box', 'on', 'LineWidth', 2, 'Position', [0.15 0.15 0.7 0.8]);
make_latex(hfig); setAllFonts(hfig,20);
export_fig(fullfile(savfol, 'energies_ne_laserEn'), '-pdf', '-CMYK', '-nocrop', hfig);

%% Make it fancier by having the spectra on linear graphs as well.

hfig = 88;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
set(gca, 'NextPlot', 'add');
load('owncolourmaps'); colormap(hfig, colmap.jet_white);

eldata = cell(length(fnames1), 3);
ff = load('tracking_onAxis_2mrad_beam.mat');
alongs = ff.tracking.screen(1).alongscreen;
logm = ~isnan(alongs);
lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');
linenaxis = linspace(300, 1300, 1000);
ycrop = 50:150;
[L_sort,inds] = sort(edata(3,:)); % Sort to lengths!
press = unique(edata(3,:));

for i=1:length(inds)
    ll = load(fullfile(datafol, fnames1{inds(i)}));
    if ll.shotdata.poo; continue; end;
    enaxis = valfit(ll.shotdata.alongscreen);
    inputim = ll.shotdata.image(ycrop,:);
    dE = [diff(enaxis); 1];
    outputim = zeros(length(ycrop), length(linenaxis));
    for k=1:length(ycrop)
        outputim(k,:) = interp1(enaxis, inputim(k,:), linenaxis);
    end
    imagesc(linspace(i-1,i,length(ycrop)), linenaxis, outputim');
    errorbar(edata(, edata(6,i), edata(5,i), edata(7,i), 'o', 'LineWidth', 1.);
    plot(i-1+0.5, edata(6,inds(i)), 'ok');
    plot(i-1+0.5, edata(9,inds(i)), 'xr');
    plot(i-1+0.5, 1200+(shotpres(inds(i))-2e18)/1e18*100, '*k');
    if i>1
        if L_sort(i-1)~=L_sort(i)
            plot((i-1)*[1 1], [min(linenaxis) min(linenaxis)+50], '--k');
            Lwidth = sum(edata(3,:)==L_sort(i-1));
            text(i-1-0.5*Lwidth, min(linenaxis)-50, num2str(L_sort(i-1)),...
                'HorizontalAlignment', 'center');
        end
    end
    if i==length(inds)
        text(i-0.5*Lwidth, min(linenaxis)-50, num2str(L_sort(i-1)),...
            'HorizontalAlignment', 'center');
    end
    drawnow;
end
plot([0 70], [1200 1200], '-k');
text(71, 1200, '2'); text(71,1300, '3');
text(76, 1100, '$n_e / 10^{18} \mathrm{cm}^{-3}$', 'Rotation', 90);
ylabel('$\mathcal{E}$ / MeV');
text(0.5*i, min(linenaxis)-100, 'Cell length / mm', 'HorizontalAlignment', 'center');
set(gca, 'XLim', [0 70], 'YLim', [min(linenaxis) max(linenaxis)], 'XTick', [],...
    'Box', 'on', 'LineWidth', 2);
setAllFonts(hfig,20); make_latex(hfig);

export_fig(fullfile(savfol, 'LengthScan'), '-pdf', '-nocrop', hfig);
%% Then, the peak energy as a function of plasma density
hfig = 77;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
errorbar(edata(3,:), edata(6,:), edata(5,:), edata(7,:), 'o', 'LineWidth', 1.5);
xlabel('Plasma density / $10^{19} \: \mathrm{cm}^{-3}$');
ylabel('Maximum energy / MeV');
set(gca, 'Position', [0.12 0.13 0.85 0.85], 'Box', 'on', 'LineWidth', 2);
make_latex(hfig);
setAllFonts(hfig,20);
export_fig(fullfile(savfol, 'MaxEn_ne'), '-pdf', '-nocrop', hfig);


%% Now maximum energy for different z positions

hfig = 77;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
set(gca, 'NextPlot', 'add');
Fix the odd 1 micron error
edata(4,edata(4,:)==-8967) = -8966;
z_s = unique(edata(4,:));
colz = 'kbrgm';
for i=1:5
    logm = edata(4,:)==z_s(i);
    dens = edata(3:7,logm);
    [~,inds] = sort(dens(1,:));
    hh(i) = plot(dens(1,inds), tsmovavg(dens(4,inds), 's', 3, 2), ['d--' colz(i)]);
    legentry{i} = sprintf('z = %1.0f', round(z_s(i)+9964));
end
errorbar((edata(4,:))+9964, edata(6,:), edata(5,:), edata(7,:), 'o');
legend(hh, legentry, 'Location', 'northeast');
set(hh, 'LineWidth', 1.5);
set(gca, 'Position', [0.12 0.13 0.85 0.8], 'Box', 'on', 'LineWidth', 2);
xlabel('Plasma density / $10^{19} \: \mathrm{cm}^{-3}$');
ylabel(gca, 'Maximum energy / MeV');
make_latex(hfig);
setAllFonts(hfig,20);
export_fig(fullfile(savfol, 'MaxEn_zs_ne'), '-pdf', '-nocrop', hfig);