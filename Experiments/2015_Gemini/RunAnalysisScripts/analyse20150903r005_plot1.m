% This script looks at data saved by MagnetTracker. It then tries to find
% maximum energy, by looking for first position above a certain amount of
% background. We then set the peak energy to be that of an electron with an
% offset angle of -1 mrad. This is a very rudimentary instrument function
% approach and should be approved upon at a later time.

if ismac
    datfol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150903r005';
    savfol = '/Users/kristjanpoder/Google Drive/Experiment/2015Gemini/EspecAnalysis/20150903r005';
end
% Load first one, set up data structures
filen = dir(fullfile(datfol, 'mat', '*Espec1.mat'));
fdat = load(fullfile(datfol, 'mat', filen(1).name));
EspecData = zeros(length(filen), size(fdat.shotdata.image,2));
DivData = zeros(length(filen), size(fdat.shotdata.image,1));
neData = zeros(length(filen), 5001);
focalspotData = zeros(201,201,length(filen));
% Set up tracking
ff = load('tracking_onAxis_1mrad_beam.mat');
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

%% And now loop through and load data
load(fullfile(datfol, 'rundata.mat'));
for i=1:length(filen)
    fprintf('Looking into file %i\n', i);
    fdat = load(fullfile(datfol, 'mat', filen(i).name));
    tempim = medfilt2(fdat.shotdata.image);
    EspecData(i,:) = sum(tempim,1);
    %DivData(i,:) = sum(tempim,2)';
    neData(i,:) = smooth(ttt(i).gastrace(:,2))';
    ll = getGemini2015laserFF(20150902, 6, i);
    if ~isempty(ll); focalspotData(:,:,i) = double(ll); end;
end

%% And interpolate to a linear grid
npnts = 1500;
enaxis0 = tracking.valfit(fdat.shotdata.alongscreen);
dE = diff(enaxis0); dE = [dE(1) dE'];
EspecDataCorr = EspecData./repmat(dE, length(filen), 1);
enaxis = linspace(min(enaxis0), max(enaxis0), npnts);
EspecDatalin = interp1(enaxis0, EspecData', enaxis', 'pchip');

% Now find the peak energy for each shot!
bgregion = 1920:size(EspecData,2);
BGmean_ = mean(EspecData(:, bgregion),2);
BGstd_ = std(EspecData(:, bgregion), 0, 2);
BGfact_ = BGmean_ + 8*BGstd_;
maxind = ones(size(BGmean_));
for i=1:length(filen)
    temp = find(EspecData(i,:)>(BGfact_(i)), 1, 'last');
    if isempty(temp); continue; end; 
    maxind(i) = temp;
end
% The threshold is lowered to find the monoen shots; this means we'll need
% to manually set a few of these to zeros
maxind(1:6) = 1;

peakEns = tracking.lowfit(fdat.shotdata.alongscreen(maxind));
peakEns_apparent = tracking.valfit(fdat.shotdata.alongscreen(maxind));
peakEns(1:6) = NaN; 
peakEns_apparent(1:6) = NaN;
% Try to overlay these on shot data!
figure(3); clf(3);
set(gca, 'NextPlot', 'add', 'YDir', 'normal');
imagesc(enaxis0, 1:length(filen), EspecDatalin');
plot(peakEns_apparent, 1:length(filen), 'dk');
colormap(3, 'parula');
axis tight;
set(gca, 'CLim', [0 7e4]);
%% And making a massive image of spectra
hfig = figure(4); clf(4);
set(4, 'Color', 'w', 'Position', [100 150 2100 1100]);
locs = GetFigureArray(6, 5, 0.01, 0.01, 'across');
for i=1:length(filen)
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add', 'Box', 'on');
    plot(enaxis0, EspecData(i,:), '-k', 'LineWidth', 1.5);
    plot(peakEns_apparent(i)*[1 1], [-3000 40000], '--r', 'LineWidth', 1.5);
    text(400, -1500, num2str(i), 'FontSize', 16);
end
set(ax, 'XTickLabel', [], 'YTIckLabel', [], 'XLim', [200 2000], 'YLim', [-3000 10000],...
    'LineWidth', 1.5);
export_fig(fullfile(savfol, 'AllShotsPeaks_Espec1'), '-nocrop', '-pdf', 4);

%% Now, all the preceding has been fun and sorting data out. 
% Now let's make some calculations and analysis, roughly as per usual

pps = cell2mat({ttt.pres})/31;
Qhighs = cell2mat({ttt.Q3}); % The high energy charge
Qtots = cell2mat({ttt.Q1}); % The high energy charge
pp_unique = unique(pps); 
pp_vs_ens = cell(size(pp_unique));
pp_vs_Qtots = cell(size(pp_unique));
pp_vs_Qhighs = cell(size(pp_unique));
for i=1:length(pp_unique)
    logm = pps==pp_unique(i);
    pp_vs_ens{i} = peakEns(logm);
    pp_vs_Qtots{i} = Qtots(logm);
    pp_vs_Qhighs{i} = Qhighs(logm);
end
pp_vs_ens_mean  = cellfun(@mean, pp_vs_ens); L_vs_ens_std  = cellfun(@std, pp_vs_ens);
pp_vs_Qtots_mean  = cellfun(@mean, pp_vs_Qtots); L_vs_Qtots_std  = cellfun(@std, pp_vs_Qtots);
pp_vs_Qhighs_mean  = cellfun(@mean, pp_vs_Qhighs); L_vs_Qhighs_std  = cellfun(@std, pp_vs_Qhighs);
laserE = cell2mat({ttt.laserE})';
laserEfact = laserE./mean(laserE(~isnan(laserE)));
laserEfact(isnan(laserEfact)) = 1;

%% Also make a massive spectra plot to highlight the issues here!
pp_numel = zeros(size(pp_unique));
for i=1:length(pp_unique); pp_numel(i) = sum(pps==pp_unique(i)); end
hfig = figure(6); clf(hfig); set(hfig, 'Color', 'w', 'Position', [100 100 1200 800]);
locs = GetFigureArray(max(pp_numel), 1, [0.04 0.01 0.01 0.07], 0.01, 'across');
colormap(colmap.jet_white);
enlines = [500 900 1400 1900]; eninds = zeros(size(enlines));
for i=1:length(enlines)
    [~,eninds(i)] = min(abs(tracking.lowfit(fdat.shotdata.alongscreen)-enlines(i)));
end
firstim = true;
for i=1:length(pp_unique)
    clf(hfig);
    ax = -ones(max(pp_numel),1);
    shotinds = find(pps==pp_unique(i));
    for k=1:pp_numel(i)
        ax(k) = axes('Parent', hfig, 'Position', locs(:,k));
        fdat = load(fullfile(datfol, 'mat', filen(shotinds(k)).name));
        tempim = medfilt2(fdat.shotdata.image);
        imagesc(tempim', 'Parent', ax(k));
        title(ax(k), ['Shot ' num2str(shotinds(k))]);
        drawnow;
    end
    ax = ax(ishandle(ax));
    clims = cell2mat(get(ax, 'CLim')); clim_max = mean(clims(:,2));
    set(ax, 'XTick', [], 'YTick', eninds, 'YDir', 'normal', 'FontSize', 20, 'CLim', [0 clim_max]);
    set(ax(1), 'YTickLabel', enlines); 
    ylabel(ax(1), ['$E_{p=' num2str(pp_unique(i)) '\mathrm{mbar}}$ / MeV'])
    set(ax(2:end), 'YTickLabel', []);
    make_latex(hfig);
    if firstim; firstim = false; end;
    if firstim 
        export_fig(fullfile(savfol, 'Spectra_all_Espec1'), '-nocrop', '-pdf', hfig);
    else
        export_fig(fullfile(savfol, 'Spectra_all_Espec1'), '-nocrop', '-pdf', '-append', hfig);
    end
end
%% Quickly plot the gas densities, just to confirm superbox was fine
hfig = figure(33); clf(33);  set(hfig, 'Color', 'w');
plot(neData'*1e-18);
ylabel('$n_e / 10^{18}\mathrm{cm}^{-3}$');
grid on;
setAllFonts(hfig, 20); make_latex(hfig);
set(gca, 'XTickLabel', []);
export_fig(fullfile(savfol, 'GasTraces'), '-nocrop', '-pdf', hfig);
%% And the laser energies as well:
hfig = figure(33); clf(33); set(33, 'Color', 'w');
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
laserE_ = laserE(laserE>5);
rectangle('Position', [0 mean(laserE_)-std(laserE_) length(filen)+1 2*std(laserE_)],...
    'EdgeColor', 0.94*[1 1 1], 'FaceColor', 0.94*[1 1 1]);
plot([0 length(filen)+1], mean(laserE_)*[1 1], '--k', 'LineWidth', 2);
plot(1:length(filen), laserE, 'ok', 'MarkerSize', 5, 'LineWidth', 1.5);
ylabel('Laser energy / J'); xlabel('Shot');
setAllFonts(hfig, 20); make_latex(hfig);
set(gca, 'XLim', [-1 length(filen)+2]);
export_fig(fullfile(savfol, 'LaserEnergies'), '-nocrop', '-pdf', hfig);

%% Plot peak energy as a function of length

for i=1:3
    hfig = figure(33+i); clf(hfig); 
    set(hfig, 'Color', 'w', 'Position', [400 400 800 600]);
    set(gca, 'NextPlot', 'add', 'Box', 'on');
    %hens = myplotyy(gca, Ls, peakEns, laserE, 'dk','or');
    switch i
        case 1
            hens = plot(pps, peakEns, 'dk');
            hens_err = errorbar(pp_unique, pp_vs_ens_mean, L_vs_ens_std, '-ro');
            legend([hens hens_err], {'Shot $\mathcal{E}$', '$\mathcal{E}\pm \sigma_{\mathcal{E}}$'}, 'Location', 'Northwest');
            ylabel('Peak energy / MeV');
            title('Peak electron energy vs $n_e$, Espec1');
            fname = 'PeakEn_vs_ne_Espec1';
        case 2
            hens = plot(pps, Qtots, 'dk');
            hens_err = errorbar(pp_unique, pp_vs_Qtots_mean, L_vs_Qtots_std, '-ro');
            legend([hens hens_err], {'Shot $\mathcal{Q}$', '$\mathcal{Q}\pm \sigma_{\mathcal{Q}}$'}, 'Location', 'Northwest');
            ylabel('$Q_{tot}$ / MeV');
            title('Total beam charge vs $n_e$');
            fname = 'Qtot_vs_ne';
        case 3
            hens = plot(pps, Qhighs, 'dk');
            hens_err = errorbar(pp_unique, pp_vs_Qhighs_mean, L_vs_Qhighs_std, '-ro');
            legend([hens hens_err], {'Shot $\mathcal{Q}$', '$\mathcal{Q}\pm \sigma_{\mathcal{Q}}$'}, 'Location', 'Northwest');
            ylabel('$Q_{\mathcal{E}>900MeV}$ / MeV');
            title('High energy charge vs $n_e$');
            fname = 'Qhigh_vs_ne';
        case 4
            
    end
    set(hens, 'MarkerSize', 10, 'LineWidth', 1.5);
    set(hens_err, 'MarkerSize', 10, 'LineWidth', 2);
    xlabel('$n_e / 10^{18} \mathrm{cm}^{-3}$');
    grid on;
    setAllFonts(hfig, 20); make_latex(hfig);
    set(gca, 'Position', [0.15 0.13 0.77, 0.79]);
    export_fig(fullfile(savfol, fname), '-nocrop', '-pdf', hfig);
end