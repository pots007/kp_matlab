% Compare pressure scans for 12.5mm and 20mm long cells

datfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis');
clear f;
f{1} = load(fullfile(datfol, '20150903r005', 'rundata_analysis'));
f{2} = load(fullfile(datfol, '20150908r008', 'rundata_analysis'));
f{3} = load(fullfile(datfol, '20150908r002', 'rundata_analysis'));

hfig = figure(67); clf(hfig);
set(hfig, 'Color', 'w');
% plot the different lengths with different symbols, with color scale
% showing the amount of laser energy

% fn1 = fieldnames(f1); fn1 = fn1{1};
% fn2 = fieldnames(f2); fn2 = fn2{1};
% fn3 = fieldnames(f3); fn3 = fn3{1};
% cla; % Scatter plot first
% set(gca, 'NextPlot', 'add');
% h1 = scatter(f1.(fn1).nes, f1.(fn1).peakEns2, 25, f1.(fn1).laserE, 'd');
% h2 = scatter(f2.(fn2).nes, f2.(fn2).peakEns2, 25, f2.(fn2).laserE, 'o');

clf(hfig);
set(gca, 'NextPlot', 'add');
%h1 = plot(f1.(fn1).pp_unique, cellfun(@mean, f1.(fn1).pp_vs_ens1, 'UniformOutput', 1), 'o-');
%h2 = plot(f2.(fn2).pp_unique, cellfun(@mean, f2.(fn2).pp_vs_ens1, 'UniformOutput', 1), 'o-');
hh = [];
colz = 'krb';
for i=1:3
    %h1 = errorbar(f1.(fn1).pp_unique, cellfun(@mean, f1.(fn1).pp_vs_ens1, 'UniformOutput', 1),...
    %    cellfun(@std, f1.(fn1).pp_vs_ens1, 'UniformOutput', 1), 'o--');
    fni = fieldnames(f{i}); fni = fni{1};
    hh(i) = errorbar(f{i}.(fni).pp_unique+0.025*(i==3), cellfun(@mean, f{i}.(fni).pp_vs_ens1, 'UniformOutput', 1),...
         cellfun(@std, f{i}.(fni).pp_vs_ens1, 'UniformOutput', 1), ['d--' colz(i)]);
    P_las = 0.67*0.9*mean(f{i}.(fni).laserE)/50e-15;
    P_crit = 17.9e9*ncrit(800)./(f{i}.(fni).pp_unique*1e18);
    %hh(i) = errorbar(P_las./P_crit, cellfun(@mean, f{i}.(fni).pp_vs_ens1, 'UniformOutput', 1),...
    %     cellfun(@std, f{i}.(fni).pp_vs_ens1, 'UniformOutput', 1), 'd--');
    
    %     h2 = errorbar(f2.(fn2).pp_unique, cellfun(@mean, f2.(fn2).pp_vs_ens1, 'UniformOutput', 1),...
%         cellfun(@std, f2.(fn2).pp_vs_ens1, 'UniformOutput', 1), 'd--');
%     h3 = errorbar(f3.(fn3).pp_unique+0.025, cellfun(@mean, f3.(fn3).pp_vs_ens1, 'UniformOutput', 1),...
%         cellfun(@std, f3.(fn3).pp_vs_ens1, 'UniformOutput', 1), 'v--');
     
end
legend([hh], {'$L = 12.5$ mm', '$L = 20$ mm', '$L=20$ mm, 2/3 E'}, ...
        'Location', 'Northwest');
set([hh], 'LineWidth', 2);
ylabel('$\mathcal{E}$ [MeV]');
xlabel('$n_e$ $[10^{18}\mathrm{cm}^{-3}]$');
make_latex(hfig); setAllFonts(hfig, 20);
set(gca, 'Box', 'on', 'LineWidth', 2, 'XLim', [1.25 3.5]);
grid on; drawnow;
xg = get(gca, 'XGridHandle'); yg = get(gca, 'YGridHandle');
set([xg yg], 'LineWidth', 1);
if 1
    fnam = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', ...
        'PressurePlots', 'comp_0908r008_0903r005');
    export_fig(fnam, '-pdf', '-nocrop', hfig);
    savefig(hfig, fnam);
end