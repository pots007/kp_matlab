% If set up properly, this will analyse 20150902r005


date_ = '20150826';
run_ = '008';

nshots = 91;
ttt = struct;
% Save as .mat for peak analysis
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 4);
% Save into the correct path
if ismac
    savepath = ['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_];
end
os = @(x) ones(1,x);
Ls = [42*os(14) 38*os(5) 34*os(7) 30*os(4) 26*os(4) 22*os(4)];
Ls = [Ls 20*os(4) 18*os(4) 16*os(4) 14*os(6) 12*os(11) 10*os(10) 8*os(10) 4*os(4)];

MagnetTrackerKP.fig.btrac.dat.outpath = fullfile(savepath, 'mat');
set(MagnetTrackerKP.fig.btrac.outpath_, 'String', fullfile(savepath, 'mat'));
datee = java.util.Date();
datee.setYear(2015-1900);
datee.setMonth(str2double(date_(5:6))-1);
datee.setDate(str2double(date_(7:8)));
MagnetTrackerKP.fig.input.date.setSelectedDate(datee);
MagnetTrackerKP.date_selected;
set(MagnetTrackerKP.fig.input.runbox, 'Value', str2double(run_));
MagnetTrackerKP.run_selected(str2double(run_));

%%

% -------######## Important note!
% Every length has a few shots where the background is very high near the
% high energy end, making automatic peaks recognition very tricky. So once
% this script has been run, it's worth making the signal region smaller and
% then overwriting these.
for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(str2double(date_), str2double(run_), i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).pres = 59;
    dat = getGemini2015Gas(str2double(date_), str2double(run_), i);
    ttt(i).gastrace = dat(55000:60000, 1:2);
    ttt(i).cellL = Ls(i);
    ttt(i).Q1 = str2double(get(MagnetTrackerKP.fig.btrac.Q1_, 'String'));
    ttt(i).Q2 = str2double(get(MagnetTrackerKP.fig.btrac.Q2_, 'String'));
    ttt(i).Q3 = str2double(get(MagnetTrackerKP.fig.btrac.Q3_, 'String'));
    ttt(i).E1 = str2double(get(MagnetTrackerKP.fig.btrac.E1_, 'String'));
    ttt(i).E2 = str2double(get(MagnetTrackerKP.fig.btrac.E2_, 'String'));
    ttt(i).E3 = str2double(get(MagnetTrackerKP.fig.btrac.E3_, 'String'));
    toc
end

corrections = [34 8.94];
for i=1:size(corrections,1); ttt(corrections(i,1)).laserE = corrections(i,2); end;
  

if ismac
    save(['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_ '/rundata'], 'ttt');
end