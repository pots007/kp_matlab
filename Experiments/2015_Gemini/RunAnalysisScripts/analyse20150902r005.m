% If set up properly, this will analyse 20150902r005


date_ = '20150822';
run_ = '003';

nshots = 150;
ttt = struct;
% Save as .mat for peak analysis
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 4);
% Save into the correct path
if ismac
    savepath = ['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_];
end

Ls(1:5)   = 42; Ls(6:10)  = 40; Ls(11:13) = 38; Ls(14:18) = 36;
Ls(19:23) = 34; Ls(24:31) = 32; Ls(32:38) = 30; Ls(39:44) = 28;
Ls(45:51) = 26; Ls(52:58) = 24; Ls(59:67) = 22; Ls(68:73) = 20;
Ls(74:82) = 18; Ls(83:92) = 16; Ls(93:104) =14; Ls(105:114)=12;
Ls(115:122)=10; Ls(123:133)= 8; Ls(134:141)= 6; Ls(142:145)= 4;
Ls(146:150)= 3;

MagnetTrackerKP.fig.btrac.dat.outpath = fullfile(savepath, 'mat');
set(MagnetTrackerKP.fig.btrac.outpath_, 'String', fullfile(savepath, 'mat'));
datee = java.util.Date();
datee.setYear(2015-1900);
datee.setMonth(str2double(date_(5:6))-1);
datee.setDate(str2double(date_(7:8)));
MagnetTrackerKP.fig.input.date.setSelectedDate(datee);
MagnetTrackerKP.date_selected;
set(MagnetTrackerKP.fig.input.runbox, 'Value', str2double(run_));
MagnetTrackerKP.run_selected(str2double(run_));

%%
for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(str2double(date_), str2double(run_), i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).pres = 40;
    dat = getGemini2015Gas(str2double(date_), str2double(run_), i);
    ttt(i).gastrace = dat(55000:60000, 1:2);
    ttt(i).cellL = Ls(i);
    ttt(i).exitenergy = getGemini2015ExitEnergy(str2double(date_), str2double(run_), i);
    ttt(i).Q1 = MagnetTrackerKP.fig.btrac.dat.QandE(1,1);
    ttt(i).Q2 = MagnetTrackerKP.fig.btrac.dat.QandE(2,1);
    ttt(i).Q3 = MagnetTrackerKP.fig.btrac.dat.QandE(3,1);
    ttt(i).E1 = MagnetTrackerKP.fig.btrac.dat.QandE(1,2);
    ttt(i).E2 = MagnetTrackerKP.fig.btrac.dat.QandE(2,2);
    ttt(i).E3 = MagnetTrackerKP.fig.btrac.dat.QandE(3,2);
    toc
end
    
if ismac
    save(['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_ '/rundata'], 'ttt');
end