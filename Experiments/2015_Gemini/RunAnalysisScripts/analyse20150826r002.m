% If set up properly, this will analyse 20150902r005


date_ = '20150826';
run_ = '002';

nshots = 92;
ttt = struct;
% Save as .mat for peak analysis
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 4);
% Save into the correct path
if ismac
    savepath = ['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_];
end
os = @(x) ones(1,x);
Ls = [42*os(3) 38*os(3) 34*os(7) 32*os(7) 30*os(5) 28*os(5) 26*os(5) 24*os(5)];
Ls = [Ls 22*os(5) 20*os(5) 18*os(8) 16*os(5) 14*os(5) 12*os(4) 10*os(4) 8*os(6) 6*os(5) 4*os(5)];

MagnetTrackerKP.fig.btrac.dat.outpath = fullfile(savepath, 'mat_20160808');
set(MagnetTrackerKP.fig.btrac.outpath_, 'String', fullfile(savepath, 'mat_20160808'));
datee = java.util.Date();
datee.setYear(2015-1900);
datee.setMonth(str2double(date_(5:6))-1);
datee.setDate(str2double(date_(7:8)));
MagnetTrackerKP.fig.input.date.setSelectedDate(datee);
MagnetTrackerKP.date_selected;
set(MagnetTrackerKP.fig.input.runbox, 'Value', str2double(run_));
MagnetTrackerKP.run_selected(str2double(run_));

%%

for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(str2double(date_), str2double(run_), i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).pres = 80;
    dat = getGemini2015Gas(str2double(date_), str2double(run_), i);
    ttt(i).gastrace = dat(55000:60000, 1:2);
    ttt(i).cellL = Ls(i);
    ttt(i).Q1 = MagnetTrackerKP.fig.btrac.dat.QandE(1,1);
    ttt(i).Q2 = MagnetTrackerKP.fig.btrac.dat.QandE(2,1);
    ttt(i).Q3 = MagnetTrackerKP.fig.btrac.dat.QandE(3,1);
    ttt(i).E1 = MagnetTrackerKP.fig.btrac.dat.QandE(1,2);
    ttt(i).E2 = MagnetTrackerKP.fig.btrac.dat.QandE(2,2);
    ttt(i).E3 = MagnetTrackerKP.fig.btrac.dat.QandE(3,2);
    toc
end

corrections = [86 10.32];
for i=1:size(corrections,1); ttt(corrections(i,1)).laserE = corrections(i,2); end;
  

if ismac
    save(['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_ '/rundata'], 'ttt');
end