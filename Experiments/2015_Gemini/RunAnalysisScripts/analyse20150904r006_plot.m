
% This script looks at data saved by MagnetTracker. It then tries to find
% maximum energy, by looking for first position above a certain amount of
% background. We then set the peak energy to be that of an electron with an
% offset angle of -1 mrad. This is a very rudimentary instrument function
% approach and should be approved upon at a later time.

date_ = '20150904'; run_ = '006';

datfol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150904r006';
savfol = '/Users/kristjanpoder/Google Drive/Experiment/2015Gemini/EspecAnalysis/20150904r006';

% Load first one, set up data structures
filen1 = dir(fullfile(datfol, 'mat', '*Espec1.mat'));
fdat1 = load(fullfile(datfol, 'mat', filen1(1).name));
EspecData1 = zeros(length(filen1), size(fdat1.shotdata.image,2));
DivData = zeros(length(filen1), size(fdat1.shotdata.image,1));

filen2 = dir(fullfile(datfol, 'mat', '*Espec2LE.mat'));
fdat1 = load(fullfile(datfol, 'mat', filen2(1).name));
EspecData2 = zeros(length(filen2), size(fdat1.shotdata.image,2));
DivData2 = zeros(length(filen2), size(fdat1.shotdata.image,1));

% Set up tracking
% This is using the offset tracking calculated with II shots' offset
ff = load('tracking_onAxis_1mrad_beam.mat');
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking1.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking1.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking1.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');
scrID = 3;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking2.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking2.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking2.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');
%% And now loop through and load data
load(fullfile(datfol, 'rundata.mat'));
for i=1:length(filen1)
    fprintf('Looking into file %i\n', i);
    fdat1 = load(fullfile(datfol, 'mat', filen1(i).name));
    tempim = medfilt2(fdat1.shotdata.image);
    EspecData1(i,:) = sum(tempim,1);
    DivData1(i,:) = sum(tempim,2)';    
    fdat2 = load(fullfile(datfol, 'mat', filen2(i).name));
    tempim = medfilt2(fdat2.shotdata.image);
    EspecData2(i,:) = sum(tempim,1);
    DivData2(i,:) = sum(tempim,2)';    
end

%% And interpolate to a linear grid - Espec1 first
npnts = 1500;
alongscreens = fdat1.shotdata.alongscreen;
logm = alongscreens>0;
enaxis0 = tracking1.valfit(alongscreens(logm));
%dE = diff(enaxis0); dE = [dE(1) dE'];
%EspecDataCorr = EspecData1(:,logm)./repmat(dE, length(filen), 1);

enaxis = linspace(min(enaxis0), max(enaxis0), npnts);
EspecDatalin = interp1(enaxis0, EspecData1(:,logm)', enaxis', 'pchip');

% Now find the peak energy for each shot!
bgregion = 1870:size(EspecData1,2);
BGmean_ = mean(EspecData1(:, bgregion),2);
BGstd_ = std(EspecData1(:, bgregion), 0, 2);
BGfact_ = BGmean_ + 7*BGstd_;
maxind = ones(size(BGmean_));
for i=1:length(filen1)
    temp = find(EspecData1(i,:)>(BGfact_(i)), 1, 'last');
    if isempty(temp); continue; end; 
    maxind(i) = temp;
end
% Whilst this works generally well, it's shit for a few shots. Fixing these
% manually below:
maxind(71) = 1620;
peakEns1 = tracking1.lowfit(fdat1.shotdata.alongscreen(maxind));
peakEns_apparent = tracking1.valfit(fdat1.shotdata.alongscreen(maxind));
peakEns1(maxind==1) = nan; peakEns1(1:19) = nan;
peakEns_apparent(isnan(peakEns1)) = nan;
% Try to overlay these on shot data!
figure(3); clf(3);
set(gca, 'NextPlot', 'add', 'YDir', 'normal');
imagesc(enaxis0, 1:length(filen1), EspecDatalin');
plot(peakEns_apparent, 1:length(filen1), 'dk');
colormap(3, 'parula');
axis tight;
set(gca, 'CLim', [0 1e5]);

%% And interpolate to a linear grid - Espec2 second
% It is a very messy reuse of variables, but I think it ought to work
npnts = 1200;
alongscreens = fdat2.shotdata.alongscreen;
logm = alongscreens>0;
enaxis0 = tracking2.valfit(alongscreens(logm));
%dE = diff(enaxis0); dE = [dE(1) dE'];
%EspecDataCorr = EspecData1(:,logm)./repmat(dE, length(filen), 1);

enaxis = linspace(min(enaxis0), max(enaxis0), npnts);
EspecDatalin = interp1(enaxis0, EspecData2(:,logm)', enaxis', 'pchip');

% Now find the peak energy for each shot!
bgregion = 1100:size(EspecData2,2);
BGmean_ = mean(EspecData2(:, bgregion),2);
BGstd_ = std(EspecData2(:, bgregion), 0, 2);
BGfact_ = BGmean_ + 7*BGstd_;
maxind = ones(size(BGmean_));
for i=1:length(filen2)
    temp = find(EspecData2(i,:)>(BGfact_(i)), 1, 'last');
    if isempty(temp); continue; end; 
    maxind(i) = temp;
end
% Whilst this works generally well, it's shit for a few shots. Fixing these
% manually below:
maxind(64) = 1;
peakEns2 = tracking2.lowfit(fdat2.shotdata.alongscreen(maxind));
peakEns_apparent = tracking2.valfit(fdat2.shotdata.alongscreen(maxind));
peakEns2(maxind==1) = nan; %peakEns(1:19) = nan;
peakEns_apparent(isnan(peakEns2)) = nan;
% Try to overlay these on shot data!
figure(3); clf(3);
set(gca, 'NextPlot', 'add', 'YDir', 'normal');
imagesc(enaxis0, 1:length(filen2), EspecDatalin');
plot(peakEns_apparent, 1:length(filen2), 'dk');
colormap(3, 'parula');
axis tight;
set(gca, 'CLim', [0 3e5]);

%% And making a massive image of spectra
hfig = figure(4); clf(4);
set(4, 'Color', 'w', 'Position', [100 150 2100 1100]);
locs = GetFigureArray(10,7, 0.01, 0.01, 'across');
for i=1:length(filen)
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add', 'Box', 'on');
    plot(enaxis0, EspecData1(i,:), '-k', 'LineWidth', 1.5);
    plot(peakEns_apparent(i)*[1 1], [-3000 40000], '--r', 'LineWidth', 1.5);
    text(400, 0, num2str(i), 'FontSize', 16);
end
set(ax, 'XTickLabel', [], 'YTIckLabel', [], 'XLim', [200 2000], 'YLim', [-3000 40000],...
    'LineWidth', 1.5);
export_fig(fullfile(datfol, 'AllShotsPeaks'), '-nocrop', '-pdf', 4);

%% Now, all the preceding has been fun and sorting data out. 
% Now let's make some calculations and analysis, roughly as per usual

nes = cell2mat({ttt.gastrace}); nes = nes(:,2:2:end);
pps = nes(end,:);
Qhighs = cell2mat({ttt.Q3}); % The high energy charge
Qtots = cell2mat({ttt.Q1}); % The high energy charge
[~,pp_unique] = hist(pps, 20);
pp_vs_ens1 = cell(size(pp_unique)); pp_vs_ens2 = cell(size(pp_unique));
pp_vs_Qtots = cell(size(pp_unique));
pp_vs_Qhighs = cell(size(pp_unique));
shotIDs = cell(size(pp_unique)); shotnums = 1:length(filen1);
binwidth = mean(diff(pp_unique));
for i=1:length(pp_unique)
    logm = abs(pps-pp_unique(i))<0.5*binwidth;
    logm1 = logm & ~isnan(peakEns1'); sum(logm);
    pp_vs_ens1{i} = peakEns1(logm1);
    logm2 = logm & ~isnan(peakEns2'); sum(logm);
    pp_vs_ens2{i} = peakEns2(logm2);
    pp_vs_Qtots{i} = Qtots(logm);
    pp_vs_Qhighs{i} = Qhighs(logm);
    shotIDs{i} = shotnums(logm);
end
pp_vs_ens1_mean  = cellfun(@mean, pp_vs_ens1); pp_vs_ens1_std  = cellfun(@std, pp_vs_ens1);
pp_vs_ens2_mean  = cellfun(@mean, pp_vs_ens2); pp_vs_ens2_std  = cellfun(@std, pp_vs_ens2);
pp_vs_Qtots_mean  = cellfun(@mean, pp_vs_Qtots); pp_vs_Qtots_std  = cellfun(@std, pp_vs_Qtots);
pp_vs_Qhighs_mean  = cellfun(@mean, pp_vs_Qhighs); L_vs_Qhighs_std  = cellfun(@std, pp_vs_Qhighs);
laserE = cell2mat({ttt.laserE})';
% Some weird fuckup in Ecat...
laserE(51) = 15.69;
laserEfact = laserE./mean(laserE(~isnan(laserE)));
laserEfact(isnan(laserEfact)) = 1;

%% And the laser energies as well:
hfig = figure(33); clf(33); set(33, 'Color', 'w');
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
rectangle('Position', [0 mean(laserE)-std(laserE) length(filen)+1 2*std(laserE)],...
    'EdgeColor', 0.94*[1 1 1], 'FaceColor', 0.94*[1 1 1]);
plot([0 length(filen)+1], mean(laserE)*[1 1], '--k', 'LineWidth', 2);
plot(1:length(filen), laserE, 'ok', 'MarkerSize', 5, 'LineWidth', 1.5);
ylabel('Laser energy / J'); xlabel('Shot');
title(['$\bar{E} = ' num2str(mean(laserE), '%2.1f') '\pm' num2str(std(laserE), '%2.1f') '$J'])
setAllFonts(hfig, 20); make_latex(hfig);
set(gca, 'XLim', [-1 length(filen)+2]);
export_fig(fullfile(savfol, 'LaserEnergies'), '-nocrop', '-pdf', hfig);

%% Plot peak energy as a function of length

%cellL = 10;
cellL = mean(cell2mat({ttt.cellL}));
for i=1:4
    hfig = figure(33+i); clf(hfig); 
    set(hfig, 'Color', 'w', 'Position', [400 100 800 600]);
    set(gca, 'NextPlot', 'add', 'Box', 'on');
    %hens = myplotyy(gca, Ls, peakEns, laserE, 'dk','or');
    switch i
        case 1
            hens = plot(pps, peakEns1, 'dk');
            hens_err = errorbar(pp_unique, pp_vs_ens1_mean, pp_vs_ens1_std, '-ro');
            legend([hens hens_err], {'Shot $\mathcal{E}$', '$\mathcal{E}\pm \sigma_{\mathcal{E}}$'}, 'Location', 'Northeast');
            ylabel('Peak energy / MeV');
            title(['Peak espec1 energy vs $n_e$, L=' num2str(cellL) ' mm']);
            fname = 'PeakEn_Espec1_vs_ne';
        case 2
            hens = plot(pps, Qtots, 'dk');
            hens_err = errorbar(pp_unique, pp_vs_Qtots_mean, pp_vs_Qtots_std, '-ro');
            legend([hens hens_err], {'Shot $\mathcal{Q}$', '$\mathcal{Q}\pm \sigma_{\mathcal{Q}}$'}, 'Location', 'Northeast');
            ylabel('$Q_{tot}$ / pC');
            title(['Total beam charge vs $n_e$, L=' num2str(cellL) ' mm']);
            fname = 'Qtot_vs_ne';
        case 3
            hens = plot(pps, Qhighs, 'dk');
            hens_err = errorbar(pp_unique, pp_vs_Qhighs_mean, L_vs_Qhighs_std, '-ro');
            legend([hens hens_err], {'Shot $\mathcal{Q}$', '$\mathcal{Q}\pm \sigma_{\mathcal{Q}}$'}, 'Location', 'Northeast');
            ylabel('$Q_{\mathcal{E}>900MeV}$ / pC');
            title(['High energy charge vs $n_e$, L=' num2str(cellL) ' mm']);
            fname = 'Qhigh_vs_ne';
        case 4
            hens = plot(pps, peakEns2, 'dk');
            hens_err = errorbar(pp_unique, pp_vs_ens2_mean, pp_vs_ens2_std, '-ro');
            legend([hens hens_err], {'Shot $\mathcal{E}$', '$\mathcal{E}\pm \sigma_{\mathcal{E}}$'}, 'Location', 'Northeast');
            ylabel('Peak energy / MeV');
            title(['Peak espec2 energy vs $n_e$, L=' num2str(cellL) ' mm']);
            fname = 'PeakEn_Espec2_vs_ne';
    end
    set(hens, 'MarkerSize', 10, 'LineWidth', 1.5);
    set(hens_err, 'MarkerSize', 10, 'LineWidth', 2);
    xlabel('$n_e$');
    grid on;
    setAllFonts(hfig, 20); make_latex(hfig);
    set(gca, 'Position', [0.15 0.13 0.77, 0.79], 'XLim', [0.5 6.]*1e18);
    %export_fig(fullfile(savfol, fname), '-nocrop', '-pdf', hfig);
end

%% Create a struct for easier analysis later on
D0904r006.EspecData1 = EspecData1;
D0904r006.EspecData2 = EspecData2;
D0904r006.peakEns1 = peakEns1;
D0904r006.peakEns2 = peakEns2;
D0904r006.nes = pps;
D0904r006.cellL = cellL;
D0904r006.pp_unique = pp_unique;
D0904r006.Qhighs = Qhighs;
D0904r006.Qtots = Qtots;
D0904r006.pp_vs_ens1 = pp_vs_ens1;
D0904r006.pp_vs_ens2 = pp_vs_ens2;
D0904r006.pp_vs_Qtots = pp_vs_Qtots;
D0904r006.pp_vs_Qhighs = pp_vs_Qhighs;
D0904r006.laserE = laserE;
%save(fullfile(savfol, 'rundata_analysis'), 'D0904r006');
%% Also make a massive spectra plot to highlight the issues here!
pp_numel = cellfun(@length,shotIDs);

hfig = figure(6); clf(hfig); set(hfig, 'Color', 'w', 'Position', [100 100 1200 800]);
locs = GetFigureArray(max(pp_numel), 1, [0.04 0.01 0.01 0.07], 0.01, 'across');
colormap(jet_white(128));
enlines = [500 900 1200 1500]; eninds = zeros(size(enlines));
for i=1:length(enlines)
    [~,eninds(i)] = min(abs(tracking1.lowfit(fdat1.shotdata.alongscreen)-enlines(i)));
end
firstim = true;
for i=1:length(pp_unique)
    clf(hfig);
    ax = -ones(max(pp_numel),1);
    shotinds = shotIDs{i};
    if isempty(shotIDs{i}); continue; end;
    for k=1:pp_numel(i)
        ax(k) = axes('Parent', hfig, 'Position', locs(:,k));
        fdat = load(fullfile(datfol, 'mat', filen1(shotinds(k)).name));
        tempim = medfilt2(fdat.shotdata.image);
        imagesc(tempim', 'Parent', ax(k));
        title(ax(k), ['Shot ' num2str(shotinds(k))]);
        drawnow;
    end
    ax = ax(ishandle(ax));
    try
        clims = cell2mat(get(ax, 'CLim')); 
    catch
        clims = get(ax, 'CLim'); 
    end
    clim_max = mean(clims(:,2));
    set(ax, 'XTick', [], 'YTick', eninds, 'YDir', 'normal', 'FontSize', 20, 'CLim', [0 clim_max]);
    set(ax(1), 'YTickLabel', enlines); 
    ylabel(ax(1), ['$E_{n_e=' num2str(pp_unique(i), '%2.2e') '\mathrm{cm}^{-3}}$ / MeV'])
    set(ax(2:end), 'YTickLabel', []);
    make_latex(hfig);
    if firstim; firstim = false; end;
    if firstim 
        export_fig(fullfile(savfol, 'Spectra_all'), '-nocrop', '-pdf', hfig);
    else
        export_fig(fullfile(savfol, 'Spectra_all'), '-nocrop', '-pdf', '-append', hfig);
    end
end