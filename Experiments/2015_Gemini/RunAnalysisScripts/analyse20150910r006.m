% If set up properly, this will analyse 20150910r006


nshots = 34;
ttt = struct;
% Save as .figs
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 3);
% Save into the correct path
MagnetTrackerKP.fig.btrac.dat.outpath = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150910r006/fig/';
set(MagnetTrackerKP.fig.btrac.outpath_, 'String',...
    '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150910r006/fig/');

for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(20150910, 6, i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).pres = 80;
    ttt(i).cellL = 15;
    ttt(i).Q1 = str2double(get(MagnetTrackerKP.fig.btrac.Q1_, 'String'));
    ttt(i).Q2 = str2double(get(MagnetTrackerKP.fig.btrac.Q2_, 'String'));
    ttt(i).Q3 = str2double(get(MagnetTrackerKP.fig.btrac.Q3_, 'String'));
    ttt(i).E1 = str2double(get(MagnetTrackerKP.fig.btrac.E1_, 'String'));
    ttt(i).E2 = str2double(get(MagnetTrackerKP.fig.btrac.E2_, 'String'));
    ttt(i).E3 = str2double(get(MagnetTrackerKP.fig.btrac.E3_, 'String'));
    toc
end
% Need to fix the pressures for shots 3-7:
if ismac
    save('/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150910r006/rundata', 'ttt');
end