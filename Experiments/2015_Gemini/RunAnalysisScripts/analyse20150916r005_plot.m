% Playing around with data from the tomorgaphy run in 2015 Gemini

if ismac
    datfol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150916r005/';
    savfol = '/Users/kristjanpoder/Google Drive/Experiment/2015Gemini/EspecAnalysis/20150916r005';
end
% Load first one, set up data structures
filen = dir(fullfile(datfol, 'mat', '*Espec1.mat'));
fdat = load(fullfile(datfol, 'mat', filen(1).name));
load(fullfile(datfol, 'rundata'));
EspecData = zeros(length(filen), size(fdat.shotdata.image,2));
DivData = zeros(length(filen), size(fdat.shotdata.image,1));
neData = zeros(length(filen), 5001);
focalspotData = zeros(201,201,length(filen));
% Set up tracking
ff = load('tracking_onAxis_1mrad_beam.mat');
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

%% And now loop through and load data
for i=1:length(filen)
    fprintf('Looking into file %i\n', i);
    fdat = load(fullfile(datfol, 'mat', filen(i).name));
    EspecData(i,:) = sum(fdat.shotdata.image,1);
    DivData(i,:) = sum(fdat.shotdata.image,2)';
    neData(i,:) = smooth(ttt(i).gastrace(:,2))';
    ll = getGemini2015laserFF(20150916, 5, i);
    if ~isempty(ll); focalspotData(:,:,i) = double(ll); end;
end

%% And interpolate to a linear grid, plot it
npnts = 1500;
enaxis0 = tracking.valfit(fdat.shotdata.alongscreen);
enaxis = linspace(min(enaxis0), max(enaxis0), npnts);
EspecDatalin = interp1(enaxis0, EspecData', enaxis', 'pchip');

%% Now find the peak energy for each shot!
bgregion = 1925:size(EspecData,2);
BGmean_ = mean(EspecData(:, bgregion),2);
BGstd_ = std(EspecData(:, bgregion), 0, 2);
BGfact_ = BGmean_ + 6*BGstd_;
maxind = ones(size(BGmean_));
for i=1:533
    temp = find(EspecData(i,:)>(BGfact_(i)), 1, 'last');
    if isempty(temp); continue; end; 
    maxind(i) = temp;
end
peakEns = tracking.lowfit(fdat.shotdata.alongscreen(maxind));
peakEns_apparent = tracking.valfit(fdat.shotdata.alongscreen(maxind));
% Try to overlay these on shot data!
figure(3); clf(3);
set(gca, 'NextPlot', 'add', 'YDir', 'normal');
imagesc(enaxis0, 1:533, EspecDatalin');
plot(peakEns_apparent, 1:533, 'dk');
colormap(3, 'parula');
axis tight;
set(gca, 'CLim', [0 1e5]);

%% Plot some stufffffffff

laserE = cell2mat({ttt.laserE});
logm1 = neData(:,4900)>3e18;
logm2 = neData(:,4900)>3.25e18;
logm3 = neData(:,4900)>3.65e18;
figure(4); clf(4);
set(gca, 'NextPlot', 'add');
hh(1) = plot(laserE.*neData(:,4900)', peakEns, 'o');
hh(2) = plot(laserE(logm1).*neData(logm1,4900)', peakEns(logm1), 'rd');
hh(3) = plot(laserE(logm2).*neData(logm2,4900)', peakEns(logm2), 'ko');
hh(3) = plot(laserE(logm3).*neData(logm3,4900)', peakEns(logm3), 'gx');

%% Plot scatter of E_peak vs laserE

figure(5); clf(5);
set(gca, 'NextPlot', 'add', 'Box', 'on');
logm = laserE>2;
scatter(laserE(logm), peakEns(logm), 35, 1e-18*20/31*neData(logm,4900), 'filled');
pfit = polyfit(laserE(logm)', peakEns(logm), 1);
cb = colorbar;
plot(12:0.1:18, polyval(pfit, 12:0.1:18), '--m', 'LineWidth', 2);
xlabel('Laser energy / J');
ylabel('Peak energy / MeV');
ylabel(cb, '$n_e / 10^{18}\mathrm{cm}^{-3}$');
make_latex(5); setAllFonts(5, 18);
export_fig(fullfile(savfol, 'PeakE_vs_laserE_vs_ne'), '-nocrop', '-pdf', 5);

%% Plot plain old energies vs shot number, with color coding for laser E

figure(5); clf(5); set(5, 'Color', 'w', 'Position', [100 100 1100 600]);
set(gca, 'NextPlot', 'add', 'Box', 'on');
shotno = 1:533;
logm = laserE>2;
scatter(shotno(logm), peakEns(logm), 55, laserE(logm), 'filled');
%pfit = polyfit(laserE(logm)', peakEns(logm), 1);
cb = colorbar;
%plot(12:0.1:18, polyval(pfit, 12:0.1:18), '--m', 'LineWidth', 2);
xlabel('Shot number');
ylabel('Peak energy / MeV');
ylabel(cb, '$E_{amp}$ / J');
make_latex(5); setAllFonts(5, 18);
export_fig(fullfile(savfol, 'PeakE_vs_shot_vs_laserE'), '-nocrop', '-pdf', 5);

%% Plot more about the laser
% Try to look at energy enclosed and length of FWHM contour

FWHM_ens = zeros(size(focalspotData,3),1);
FWHM_Clength = zeros(size(focalspotData,3),1);

for i=1:size(focalspotData,3)
    tempim = focalspotData(:,:,i);
    tempim = tempim - mean(mean(tempim(1:50,1:50)));
    FWHM_ens(i) = sum(sum(tempim(tempim>0.5*max(tempim(:)))))/sum(tempim(:));
    C = contourc(tempim, 0.5*max(tempim(:))*[1 1]);
    FWHM_Clength(i) = size(C,2);
end

hfig = figure(8); clf(hfig); set(hfig, 'Color', 'w', 'Position', [100 100 1100 800]);
locs = GetFigureArray(2, 2, [0.05 0.1 0.1 0.1], 0.025, 'across');
clear axs;
axs(1) = axes('Parent', hfig, 'Position', locs(:,1), 'NextPlot', 'add');
plot(1:length(FWHM_ens), FWHM_ens, 'ok');

axs(2) = axes('Parent', hfig, 'Position', locs(:,2), 'NextPlot', 'add');
plot(1:length(FWHM_ens), FWHM_Clength, 'ok'); ylim([100 400]);

axs(3) = axes('Parent', hfig, 'Position', locs(:,3), 'NextPlot', 'add');
plot(1:length(FWHM_ens), 1e-18*20/31*neData(:,4900), 'ok');

axs(4) = axes('Parent', hfig, 'Position', locs(:,4), 'NextPlot', 'add');
plot(1:length(FWHM_ens), laserE, 'ok'); ylim([12 18])

set(axs, 'Box', 'on', 'LineWidth', 2, 'XLim', [0 550]);
set(axs([2 4]), 'YAxisLocation', 'right');
set(axs(1:2), 'XTickLabel', []);
ylabel(axs(1), '$E_{FWHM}/E_0$');
ylabel(axs(2), 'FWHM contour length / pix');
ylabel(axs(3), '$n_e / 10^{18}\mathrm{cm}^{-3}$');
ylabel(axs(4), '$E_{amp}$ / J');
xlabel(axs(3), 'Shot number'); xlabel(axs(4), 'Shot number');
make_latex(hfig); setAllFonts(hfig, 18);
export_fig(fullfile(savfol, 'LaserStuff'), '-nocrop', '-pdf', hfig);