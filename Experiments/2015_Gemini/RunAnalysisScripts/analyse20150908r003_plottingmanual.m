% This script will analyse 201509803r004, one that I looked through
% manually and where the backtracking angle was calculated
% This run has good exit energy data as well!
date_ = '20150908';
run_ = '003';

datfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', [date_ 'r' run_], 'mat_manual');
savfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', [date_ 'r' run_]);
flist1 = dir(fullfile(datfol, '*Espec1.mat'));
flist2 = dir(fullfile(datfol, '*Espec2LE.mat'));

% And redo what other scripts do in the analysis part
nShots = 67;
ttt = struct;
for i=1:nShots
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(str2double(date_), str2double(run_), i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).cellL = 12.5;
    dat = getGemini2015Gas(str2double(date_), str2double(run_), i);
    if ~isempty(dat);
        ttt(i).gastrace = dat(55000:60000, 1:2);
    else
        ttt(i).gastrace = 0;
    end
    ttt(i).exitenergy = getGemini2015ExitEnergy(str2double(date_), str2double(run_), i);
end

%% First, plot angles

btrack_thetas = []; cellL = [];
Qhighs = zeros(1,nShots);
Qtots = zeros(1,nShots);
for i=1:nShots
    try
        fnam1 = sprintf('%sr%ss%03i_Espec1.mat', date_, run_, i);
        fdat = load(fullfile(datfol, fnam1));
        btrack_thetas(i) = fdat.shotdata.track_angle;
        Qtots(i) = fdat.shotdata.totQ;
        %cellL = fdat.shotdata.track_cellL;
        % But do high charges as well
        fnam2 = sprintf('%sr%ss%03i_Espec2LE.mat', date_, run_, i);
        fdat = load(fullfile(datfol, fnam2));
        Qhighs(i) = fdat.shotdata.totQ;
    catch err
        fprintf('No file for %i: %s\n', i, err.message);
    end
end
% Not that interesting after all!

%% Now look at the divergence

% plot for random shot the divergence as a function of energy
fdat = load(fullfile(datfol, flist2(19).name));
im = fdat.shotdata.image;
yax = fdat.shotdata.divax; ddiv = abs(mean(diff(yax)));
enax = fdat.shotdata.tracking_btrac(fdat.shotdata.alongscreen);
divdat = nan(size(im,2), 2);
for i=1:size(im,2)
    divdat(i,1) = enax(i);
    try
        divdat(i,2) = widthfwhm(im(:,i)')*ddiv;
    catch
    end
end
plot(divdat(:,1), divdat(:,2), divdat(:,1), 2000./divdat(:,1), '-k')
ylim([0 5]); xlim([0 2200])

%% Now for the fun part. Find cutoff energies, with backtracked angles.
% First, interpolate all screen images onto a fixed grid. BUT the grid is
% in screen distance now.
screenBins = 0:0.1:300;
scrmat1 = zeros(nShots, length(screenBins));
btrack_thetas = [];
for i=1:nShots
    fnam1 = sprintf('%sr%ss%03i_Espec1.mat', date_, run_, i);
    try
    fdat = load(fullfile(datfol, fnam1));
    scrmat1(i,:) = smooth(interp1(fdat.shotdata.alongscreen, sum(fdat.shotdata.image,1), screenBins, 'pchip', 0));
    btrack_thetas(i) = fdat.shotdata.track_angle;
    catch
        btrack_thetas(i) = 0;
    end
end
scrmat1 = fliplr(scrmat1);
% Now do the same energy finding exercise
%bgregion = 2750:size(scrmat1,2);
%BGmean_ = mean(scrmat1(:,bgregion),2);
%BGstd_ = std(scrmat1(:,bgregion),[],2);
%BGfact_ = BGmean_ + 8*BGstd_;
maxind = ones(size(scrmat1,1),1);
for i=1:length(maxind)
    % Do it on a line by line basis here, as I changed the box size a lot!
    linespec = scrmat1(i,:);
    linespec = linespec(linespec~=0);
    if isempty(linespec); continue; end;
    bgreg = (length(linespec)-30):length(linespec);
    BGmean_(i) = mean(linespec(bgreg));
    BGstd_(i) = std(linespec(bgreg));
    BGfact_(i) = BGmean_(i) + 10*BGstd_(i);
    temp = find(scrmat1(i,:)>(BGfact_(i)), 1, 'last');
    if isempty(temp); continue; end;
    maxind(i) = temp;
end
% Apply some corrections:
%maxind([15 24]) = [1950 1975];
% Check it out
figure(3); clf(3);
set(gca, 'NextPlot', 'add');
imagesc(scrmat1);
plot(maxind, 1:nShots, 'ok');
axis tight;
%set(gca, 'CLim', [0 1e7]);
% And now find the energies for the locations found.
% Again assume this is the +1 mrad divergence part!
peakEns1 = nan(1,nShots);
for i=1:nShots
    %if i==2; continue; end;
    peakEns1(i) = getGem2015ScreenEnergy(300-screenBins(maxind(i)), 1, 1, btrack_thetas(i));
end
%% Now, all the preceding has been fun and sorting data out.
% Now let's make some calculations and analysis, roughly as per usual
fpres = load('Gem2015PressureScans');

pps = fpres.PressureScans(15).pressure'/31;
pp_unique = unique(pps);
laserE = cell2mat({ttt.laserE})';
pp_vs_ens1 = cell(size(pp_unique));
pp_vs_Qtots = cell(size(pp_unique));
pp_vs_Qhighs = cell(size(pp_unique));
clear pp_st; pp_st(length(pp_unique)) = struct;
for i=1:length(pp_unique)
    logm = pps==pp_unique(i) & Qtots>3 & ~isnan(peakEns1);
    pp_vs_ens1{i} = peakEns1(logm);
    pp_vs_Qtots{i} = Qtots(logm);
    pp_vs_Qhighs{i} = Qhighs(logm);
    % New analysis structure:
    ll = pps==pp_unique(i);
    pp_st(i).pres = pp_unique(i);
    pp_st(i).peakEns1 = peakEns1(ll);
    pp_st(i).peakEns1_logm = ~isnan(pp_st(i).peakEns1);
    pp_st(i).Qtots = Qtots(ll);
    pp_st(i).Qhighs = Qhighs(ll);
    pp_st(i).laserE = laserE(ll);
    temppp = ll.*(1:length(pps));
    pp_st(i).shots = temppp(temppp~=0);
end
pp_vs_ens_mean  = cellfun(@mean, pp_vs_ens1); pp_vs_ens_std  = cellfun(@std, pp_vs_ens1);
pp_vs_Qtots_mean  = cellfun(@mean, pp_vs_Qtots); pp_vs_Qtots_std  = cellfun(@std, pp_vs_Qtots);
pp_vs_Qhighs_mean  = cellfun(@mean, pp_vs_Qhighs); pp_vs_Qhighs_std  = cellfun(@std, pp_vs_Qhighs);

laserEfact = laserE./mean(laserE(~isnan(laserE)));
laserEfact(isnan(laserEfact)) = 1;

%% Check for inconsistent data points:

% First the third pressure:
ens_loc = pp_vs_ens1{3};
ens_loc_sub = ens_loc([2,3,5:end]);
ens_std = std(ens_loc_sub); ens_mean = mean(ens_loc_sub);
% How many std deviations away are we?
abs((ens_loc([1,4])-ens_mean)/ens_std)
% seems we can exlude these - 3 sigma is 1% chance it's real
pp_vs_ens1{3} = ens_loc([2,3,5:end]);
% and now for the fourth pressure
ens_loc = pp_vs_ens1{4};
ens_loc_sub = ens_loc([1,3,4,6:end]);
ens_std = std(ens_loc_sub); ens_mean = mean(ens_loc_sub);
% How many std deviations away are we?
abs((ens_loc([2,5])-ens_mean)/ens_std)
% OK - no way these should be included!
pp_vs_ens1{4} = ens_loc_sub;
ens_loc = pp_vs_ens1{5};
ens_loc_sub = ens_loc(2:end);
ens_std = std(ens_loc_sub); ens_mean = mean(ens_loc_sub);
% How many std deviations away are we?
abs((ens_loc(1)-ens_mean)/ens_std)
% OK - 5 sigma is still pretty far away!
pp_vs_ens1{5} = ens_loc_sub;
% And recalculating the means
pp_vs_ens_mean  = cellfun(@mean, pp_vs_ens1); pp_vs_ens_std  = cellfun(@std, pp_vs_ens1);

%% Quickly plot the gas densities, just to confirm superbox was fine
neData = [];
for i=1:length(ttt)
    neData(i,:) = smooth(ttt(i).gastrace(:,2))';
end
for i=1:length(pp_st)
    pp_st(i).nes_traces = neData(pp_st(i).shots, :);
    pp_st(i).nes_vals = mean(neData(pp_st(i).shots, end-100:end), 2);
end
hfig = figure(33); clf(33);  set(hfig, 'Color', 'w');
%plot(neData'*1e-18);
set(gca, 'NextPlot', 'add', 'Box', 'on');
hnes = [];
for i=1:length(pp_st)
    %plot(pp_st(i).nes_traces'*1e-18);
    plot(pp_st(i).nes_vals*1e-18, 'x--');
end
ylabel('$n_e / 10^{18}\mathrm{cm}^{-3}$');
grid on;
setAllFonts(hfig, 20); make_latex(hfig);
set(gca, 'XTickLabel', []);
export_fig(fullfile(savfol, 'GasTraces'), '-nocrop', '-pdf', hfig);

%% And the laser energies as well:
hfig = figure(33); clf(33); set(33, 'Color', 'w');
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
laserE_ = laserE(laserE>5);
ns = length(laserE);
rectangle('Position', [0 mean(laserE_)-std(laserE_) ns+1 2*std(laserE_)],...
    'EdgeColor', 0.94*[1 1 1], 'FaceColor', 0.94*[1 1 1]);
plot([0 ns+1], mean(laserE_)*[1 1], '--k', 'LineWidth', 2);
plot(1:ns, laserE, 'ok', 'MarkerSize', 5, 'LineWidth', 1.5);
ylabel('Laser energy / J'); xlabel('Shot');
setAllFonts(hfig, 20); make_latex(hfig);
set(gca, 'XLim', [-1 ns+2]);
export_fig(fullfile(savfol, 'LaserEnergies'), '-nocrop', '-pdf', hfig);

%%
for i=2%:3
    hfig = figure(33+i); clf(hfig);
    set(hfig, 'Color', 'w', 'Position', [400 400 800 600]);
    set(gca, 'NextPlot', 'add', 'Box', 'on');
    switch i
        case 1
            logm = Qtots>3;
            hens = plot(pps(logm), peakEns1(logm), 'dk', pps(~logm), peakEns1(~logm), 'db');
            hens_err = errorbar(pp_unique, pp_vs_ens_mean, pp_vs_ens_std, '-ro');
            legend([hens' hens_err], {'Shot $\mathcal{E}$', 'Shot $\mathcal{E}$, $Q<3$ pC',...
                '$\mathcal{E}\pm \sigma_{\mathcal{E}}$'}, 'Location', 'Northeast');
            ylabel('Peak energy / MeV');
            title('Peak electron energy vs $n_e$, Espec2LE');
            fname = 'PeakEn_vs_L_Espec2_btracked';
        case 2
            hens = plot(pps, Qtots, 'dk');
            hens_err = errorbar(pp_unique, pp_vs_Qtots_mean, pp_vs_Qtots_std, '-ro');
            legend([hens hens_err], {'Shot $\mathcal{Q}$', '$\mathcal{Q}\pm \sigma_{\mathcal{Q}}$'}, 'Location', 'Northwest');
            ylabel('$Q_{tot}$ / pC');
            title('Total beam charge vs $n_e$');
            fname = 'Qtot_vs_ne';
        case 3
            hens = plot(pps, Qhighs, 'dk');
            hens_err = errorbar(pp_unique, pp_vs_Qhighs_mean, pp_vs_Qhighs_std, '-ro');
            legend([hens hens_err], {'Shot $\mathcal{Q}$', '$\mathcal{Q}\pm \sigma_{\mathcal{Q}}$'}, 'Location', 'Northwest');
            ylabel('$Q_{\mathcal{E}>900MeV}$ / MeV');
            title('High energy charge vs $n_e$');
            fname = 'Qhigh_vs_ne';
        case 4
            
    end
    set(hens, 'MarkerSize', 10, 'LineWidth', 1.5);
    set(hens_err, 'MarkerSize', 10, 'LineWidth', 2);
    xlabel('$n_e$ / $10^{18} \mathrm{cm}^{-3}$');
    grid on;
    setAllFonts(hfig, 20); make_latex(hfig);
    set(gca, 'Position', [0.15 0.13 0.77, 0.79]);
    export_fig(fullfile(savfol, fname), '-nocrop', '-pdf', hfig);
end

%% Create a struct for easier analysis later on
clear D0908r003;
D0908r003.EspecData2 = scrmat1;
%D0903r004.EspecData2 = EspecData2;
D0908r003.peakEns2 = peakEns1;
%D0903r004.peakEns2 = peakEns2;
D0908r003.nes = pps;
D0908r003.cellL = 12.5;
D0908r003.pp_unique = pp_unique;
D0908r003.Qhighs = Qhighs;
D0908r003.Qtots = Qtots;
D0908r003.pp_vs_ens1 = pp_vs_ens1;
%D0903r004.L_vs_ens2 = L_vs_ens2;
D0908r003.pp_vs_Qtots = pp_vs_Qtots;
D0908r003.pp_vs_Qhighs = pp_vs_Qhighs;
D0908r003.laserE = laserE;
save(fullfile(savfol, 'rundata_analysis'), 'D0908r003');