% This filw will compare II and SI runs from 20150908
% r002 is with pure helium, that is SI
% r003 is with 10% CO2 mix

% Compare pressure scans for 12.5mm and 20mm long cells

datfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis');
clear f;
f{1} = load(fullfile(datfol, '20150908r002', 'rundata_analysis'));
f{2} = load(fullfile(datfol, '20150908r003', 'rundata_analysis'));

saveplots = 1;

hfig = figure(67); clf(hfig);
set(hfig, 'Color', 'w');

clf(hfig);
set(gca, 'NextPlot', 'add');

hh = [];
colz = 'krb';
% Iterate over the different species
for i=1:2
    fni = fieldnames(f{i}); fni = fni{1};
    nes = f{i}.(fni).pp_unique;
    if i==1; nes = nes/31; end;
    hh(i) = errorbar(nes + 0.01*(i==1), cellfun(@mean, f{i}.(fni).pp_vs_ens1, 'UniformOutput', 1),...
         cellfun(@(x) std(x)/sqrt(length(x)), f{i}.(fni).pp_vs_ens1, 'UniformOutput', 1), ['d--' colz(i)]);
    P_las = 0.67*0.9*mean(f{i}.(fni).laserE)%/50e-15
    std(f{i}.(fni).laserE)
    P_crit = 17.9e9*ncrit(800)./(f{i}.(fni).pp_unique*1e18);   
end
% And the linear energy gain as well
nes_ = 1e17:1e17:4e18;
hh(end+1) = plot(nes_*1e-18, 2*ncrit(800)./nes_*0.511, '-m');

legend([hh], {'Helium', 'He + $10\%\, \mathrm{CO}_2$', '$\Delta W = 2\gamma_p^2m_ec^2$'}, ...
        'Location', 'ne', 'Box', 'off');
set([hh], 'LineWidth', 2);
ylabel('$\mathcal{E}$ [MeV]');
xlabel('$n_e$ $[10^{18}\mathrm{cm}^{-3}]$');    
make_latex(hfig); setAllFonts(hfig, 20);
set(gca, 'Box', 'on', 'LineWidth', 2, 'XLim', [1.25 3.5], 'YLim', [400 2200]);
if exist('dEdat', 'var')
    %plot(dEdat(1,:)*1e-18, abs(dEdat(2,:)), '-.b');
end

grid on; drawnow;
makeNiceGridHG2(gca);
if saveplots
    fnam = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', ...
        'PressurePlots', 'comp_0908_SI_II');
    %export_fig(fnam, '-pdf', '-nocrop', hfig);
    %savefig(hfig, fnam);
end

%% In an old fashioned way, charge:
hfig = figure(67); clf(hfig);
set(hfig, 'Color', 'w');

clf(hfig);
set(gca, 'NextPlot', 'add');

hh = [];
colz = 'krb';
% Iterate over the different species
for i=1:2
    fni = fieldnames(f{i}); fni = fni{1};
        nes = f{i}.(fni).pp_unique;
    if i==1; nes = nes/31; end;
    hh(i) = errorbar(nes+0.026*(i==2), cellfun(@mean, f{i}.(fni).pp_vs_Qtots, 'UniformOutput', 1),...
         cellfun(@std, f{i}.(fni).pp_vs_Qtots, 'UniformOutput', 1), ['d--' colz(i)]);
    P_las = 0.67*0.9*mean(f{i}.(fni).laserE)/50e-15;
    P_crit = 17.9e9*ncrit(800)./(f{i}.(fni).pp_unique*1e18);   
end
legend([hh], {'Helium', 'He + $10\%\, \mathrm{CO}_2$'}, ...
        'Location', 'NorthWest', 'Box', 'off');
set([hh], 'LineWidth', 2);
ylabel('$Q$ [pC]');
xlabel('$n_e$ $[10^{18}\mathrm{cm}^{-3}]$');    
make_latex(hfig); setAllFonts(hfig, 20);
set(gca, 'Box', 'on', 'LineWidth', 2, 'XLim', [1.25 3.5]);
grid on; drawnow;
xg = get(gca, 'XGridHandle'); yg = get(gca, 'YGridHandle');
set([xg yg], 'LineWidth', 1);
if saveplots
    fnam = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', ...
        'PressurePlots', 'comp_0908_SI_II_Q');
    export_fig(fnam, '-pdf', '-nocrop', hfig);
    savefig(hfig, fnam);
end

%% In an old fashioned way, high energy charge:
hfig = figure(67); clf(hfig);
set(hfig, 'Color', 'w');

clf(hfig);
set(gca, 'NextPlot', 'add');

hh = [];
colz = 'krb';
% Iterate over the different species
for i=1:2
    fni = fieldnames(f{i}); fni = fni{1};
    nes = f{i}.(fni).pp_unique;
    if i==1; nes = nes/31; end;
    hh(i) = errorbar(nes, cellfun(@mean, f{i}.(fni).pp_vs_Qhighs, 'UniformOutput', 1),...
         cellfun(@std, f{i}.(fni).pp_vs_Qhighs, 'UniformOutput', 1), ['d--' colz(i)]);
    P_las = 0.67*0.9*mean(f{i}.(fni).laserE)/50e-15;
    P_crit = 17.9e9*ncrit(800)./(f{i}.(fni).pp_unique*1e18);   
end
legend([hh], {'Helium', 'He + $10\%\, \mathrm{CO}_2$'}, ...
        'Location', 'NorthWest', 'Box', 'off');
set([hh], 'LineWidth', 2);
ylabel('$Q_\mathcal{E}$ [MeV]');
xlabel('$n_e$ $[10^{18}\mathrm{cm}^{-3}]$');    
make_latex(hfig); setAllFonts(hfig, 20);
set(gca, 'Box', 'on', 'LineWidth', 2, 'XLim', [1.25 3.5]);
grid on; drawnow;
xg = get(gca, 'XGridHandle'); yg = get(gca, 'YGridHandle');
set([xg yg], 'LineWidth', 1);
if saveplots
    fnam = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', ...
        'PressurePlots', 'comp_0908_SI_II_Qhigh');
    export_fig(fnam, '-pdf', '-nocrop', hfig);
    savefig(hfig, fnam);
end