% This will fit accelerating gradients to 20150903 length scans, runs 4 and
% 6. Run 4 is at 50mbar, run 6 at 80mbar, both at the highest energies we
% ever shot.

fnamesload = {fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', '20150902r005', 'rundata_analysis'),...
    fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', '20150903r006', 'rundata_analysis')};
savfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', 'AccelerationFits');
faccFit = fullfile(savfol, 'AccFits.mat');
if exist(faccFit, 'file')
    fdat = load(faccFit); accFits = fdat.accFits;
else
    %accFits = struct;
end
plotsave = 1;

hfig = 89;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 600 450]);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'Position', [0.18 0.17 0.8 0.8]); ax1 = gca;
ax1h = [];
posns = [200 800 600 450; 900 200 600 450];
nes = [100 80]/31;
for i=1
    fload = load(fnamesload{i});
    ffnames = fieldnames(fload);
    ff = fload.(ffnames{1});
    hfig = 89+i;
    figure(hfig); clf(hfig);
    set(hfig, 'Color', 'w', 'Position', posns(i,:));
    set(gca, 'NextPlot', 'add', 'Box', 'on');
    h1 = plot(ff.cellL(ff.peakEns1>300)-0.2, ff.peakEns1(ff.peakEns1>300), '.k', 'MarkerSize', 10, 'LineWidth', 2);
    % And some errorbar stuff
    if i==1; 
        logmav = 1:length(ff.L_unique); 
        logm = 1:length(ff.cellL) & ~isnan(peakEns1) & ff.peakEns1>300;
    elseif i==2
        logmav = ff.L_unique<21;
        logm = ff.cellL<21 & ~isnan(ff.peakEns1') & ff.peakEns1'>300;
    end
    L_vs_ens_mean  = cellfun(@mean, ff.L_vs_ens1); L_vs_ens_std  = cellfun(@std, ff.L_vs_ens1);
    h2 = errorbar(ff.L_unique(logmav), L_vs_ens_mean(logmav), L_vs_ens_std(logmav), '-*', 'LineWidth', 2, 'Color', 0.34*[1 1 1]);
    %ax1h(i) = errorbar(ax1, ff.L_unique(logmav)+(2-i)*0.25, L_vs_ens_mean(logmav), L_vs_ens_std(logmav), '-*', 'LineWidth', 2);
    ax1h(i) = plot(ax1, ff.L_unique(logmav)+(2-i)*0.25, L_vs_ens_mean(logmav), '-*', 'LineWidth', 2);
    % Now add a fit to the acceleration: a normal parabola
    parabfit = polyfit(ff.cellL(logm), ff.peakEns1(logm), 2);
    parabfit_ = fit(ff.cellL(logm)', ff.peakEns1(logm)', 'poly2');
    % And plot the bastard
    plotL1 = 2:0.1:10;
    h3 = plot(plotL1, polyval(parabfit, plotL1), '--r', 'LineWidth', 2.5);
    if i==2 %|| i==2
        logm_ = ff.cellL<31;
        ensloc = ff.peakEns1(logm_); Lloc = ff.cellL(logm_);
        [Lloc, inds] = sort(Lloc); ensloc = ensloc(inds);
        logm6 = (Lloc==6)*58;  logm10 = (Lloc==10)*54;
        logm14 = (Lloc==14)*50; logm18 = (Lloc==18)*46;
        logm22 = (Lloc==22)*42; logm26 = (Lloc==26)*38;
        logm30 = (Lloc==30)*34;
        templens = [Lloc logm6+logm10+logm14+logm18+logm22+logm26+logm30];
        tempens = [ensloc' ensloc'];
        plotL2 = 2:0.1:10;
    elseif i==1
        logm_ = ff.cellL<6.1 & ~isnan(ff.peakEns1) & ff.peakEns1>300;
        ensloc = ff.peakEns1(logm_)'; Lloc = ff.cellL(logm_);
        [Lloc, inds] = sort(Lloc); ensloc = ensloc(inds);
        logm6 = (Lloc==3)*10;  logm10 = (Lloc==4)*9;
        logm14 = (Lloc==5)*8; logm18 = (Lloc==6)*7;
        templens = [Lloc logm6+logm10+logm14+logm18];
        tempens = [ensloc' ensloc'];
        plotL2 = 2:0.1:10;
    end
    accelfit = polyfit(templens, tempens, 2);
    accelfit_ = fit(templens', tempens', 'poly2');
    
    h4 = plot(plotL2, polyval(accelfit, plotL2), '--m', 'LineWidth', 2.5);
    xlabel('Cell length / mm');
    ylabel('Maximum energy / MeV');
    hleg = legend([h1 h2 h3 h4], {'Shot $\mathcal{E}$', 'Average at each length', 'Fit to acc. and dec.', 'Fit to acc. only'},...
        'Location', 'Southeast', 'Box', 'off');
    set(gca, 'Position', [0.18 0.17 0.8 0.8], 'Box', 'on', 'LineWidth', 2,...
        'XLim', [2 12], 'YLim', [400 2500]);
    if i==2; set(gca, 'XLim', [2 20], 'YLim', [400 2200]); end;
    % Add plasma density and Ep fit value
    text(2.5, 2100, ['$n_e = ' num2str(nes(i), '%2.1f') ' \times 10^{18} \mathrm{cm}^{-3}$'],...
        'HorizontalAlignment', 'left');

    make_latex(hfig); setAllFonts(hfig, 20);
    if plotsave; export_fig(fullfile(savfol, [ffnames{1} '_manual']), '-pdf', '-nocrop', hfig); end
    % And let's output the numbers, in useful terms:
    %E_peak_acc = [parabfit(2) accelfit(2)];
    E_peak_acc = [parabfit_.p2 accelfit_.p2];
    %L_dephasing = -E_peak_acc./(2*[parabfit(1) accelfit(1)]);
    L_dephasing = -E_peak_acc./(2*[parabfit_.p1 accelfit_.p1]);
    % And the errors as well
    cints = [confint(parabfit_) confint(accelfit_)];
    E_peak_errors = [mean(abs(cints(:,2)-E_peak_acc(1))) mean(abs(cints(:,5)-E_peak_acc(2)))];
    fit_types = {'Full length scan', 'Acceleration only'};
    fprintf('\n%20s\t%10s\t%10s\t%10s\n', 'Fit type', 'E_p (GeV/m)', 'E_err (GeV/m)', 'L_d (mm)');
    for k=1:2
        fprintf('%20s\t%10.0f\t%10.1f\t%10.1f\n', fit_types{k}, E_peak_acc(k), E_peak_errors(k), L_dephasing(k));
    end
    % Save these data into a collage file!
    if sum(strcmpi({accFits.run}, ffnames{1}))
        indd = sum(strcmpi({accFits.run}, ffnames{1}).*(1:length(accFits)));
    else 
        indd = length(accFits)+1;
    end
    accFits(indd).run = ffnames{1};
    accFits(indd).ne = nes(i);
    accFits(indd).laserE = mean(ff.laserE);
    accFits(indd).accE = E_peak_acc(2);
    accFits(indd).accEerr = E_peak_errors(2);
    accFits(indd).accLdph = L_dephasing(2);
    accFits(indd).fullE = E_peak_acc(1);
    accFits(indd).fullEerr = E_peak_errors(1);
    accFits(indd).fullLdph = L_dephasing(1);
end

legend(ax1, ax1h, {'$n_e = 3.2\times 10^{18} \mathrm{cm}^{-3}$'}, 'Location', 'South', 'Box', 'off');
xlabel(ax1, 'Cell length / mm');
ylabel(ax1, 'Maximum energy / MeV');
setAllFonts(89, 20); make_latex(89);    
if plotsave; export_fig(fullfile(savfol, 'D0902_all_manual'), '-pdf', '-nocrop', 89); end;
save(faccFit, 'accFits');