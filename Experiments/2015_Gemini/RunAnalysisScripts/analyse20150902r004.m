% If set up properly, this will analyse 20150902r004


date_ = '20150902';
run_ = '004';

nshots = 69;
ttt = struct;
% Save as .mat for peak analysis
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 4);
% Save into the correct path
if ismac
    savepath = ['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_];
end

os = @(x) ones(1,x);
Ls = [3*os(5) 4*os(3) 5*os(3) 6*os(4) 8*os(3) 10*os(3) 12*os(3) 14*os(3)];
Ls = [Ls 16*os(3) 18*os(3) 20*os(3) 22*os(3) 24*os(3) 26*os(3) 28*os(3)];
Ls = [Ls 30*os(3) 32*os(3) 34*os(3) 38*os(3) 42*os(3) 9*os(3) 7*os(3)];

MagnetTrackerKP.fig.btrac.dat.outpath = fullfile(savepath, 'mat');
set(MagnetTrackerKP.fig.btrac.outpath_, 'String', fullfile(savepath, 'mat'));
datee = java.util.Date();
datee.setYear(2015-1900);
datee.setMonth(str2double(date_(5:6))-1);
datee.setDate(str2double(date_(7:8)));
MagnetTrackerKP.fig.input.date.setSelectedDate(datee);
MagnetTrackerKP.date_selected;
set(MagnetTrackerKP.fig.input.runbox, 'Value', str2double(run_));
MagnetTrackerKP.run_selected(str2double(run_));

%%
for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(20150902, 4, i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).pres = 80;
    dat = getGemini2015Gas(str2double(date_), str2double(run_), i);
    ttt(i).gastrace = dat(55000:60000, 1:2);
    ttt(i).cellL = Ls(i);
    ttt(i).Q1 = str2double(get(MagnetTrackerKP.fig.btrac.Q1_, 'String'));
    ttt(i).Q2 = str2double(get(MagnetTrackerKP.fig.btrac.Q2_, 'String'));
    ttt(i).Q3 = str2double(get(MagnetTrackerKP.fig.btrac.Q3_, 'String'));
    ttt(i).E1 = str2double(get(MagnetTrackerKP.fig.btrac.E1_, 'String'));
    ttt(i).E2 = str2double(get(MagnetTrackerKP.fig.btrac.E2_, 'String'));
    ttt(i).E3 = str2double(get(MagnetTrackerKP.fig.btrac.E3_, 'String'));
    drawnow;
    toc
end
% Fix some missing energies
ttt(2).laserE = 9.33; ttt(18).laserE = 9.59; ttt(20).laserE = 9.59;
ttt(24).laserE = 10.22; ttt(30).laserE = 9.04; ttt(33).laserE = 9.55;
ttt(36).laserE = 8.88; ttt(50).laserE = 9.34; ttt(53).laserE = 9.29;
ttt(57).laserE = 8.72; ttt(60).laserE = 9.20; ttt(66).laserE = 9.41;
ttt(69).laserE = 9.18;

if ismac
    save(['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_ '/rundata'], 'ttt');
end