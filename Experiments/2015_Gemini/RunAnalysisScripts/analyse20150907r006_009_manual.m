% Analyse the 20150907r6-9 II pressure scans. The four runs constitute a
% length scan at many pressures, effectively a matrix of some nature. Will
% try to fit a parabola to each of the pressures along the 4 runs.

% Runs 6,7,8,9; From the shotsheet so may need to check with GasPressure
Ls = [10 15 20 30];
press = {[20 20 20 30 30 30 35 35 35 40 40 40 50 50 50 60 60 60 70 70 70 80 80 80 100 100 100 120 120 120 140 140 140],...
    [30 30 30 40 40 40 50 50 50 45 45 45 60 60 60 70 70 70 80 80 80 100 100 100 120 120 120],...
    [30 30 30 40 40 40 50 50 50 60 60 60 70 70 70 80 80 80 100 100 100 120 120 120],...
    [30 30 30 40 40 40 40 36 50 50 50 60 60 60 70 70 70 80 80 80 100 100 100 120 120 120]};

datfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis');
compres = [40 50 60 70];
screenBins = 0:0.1:300;
% Iterate over lengths
eldata = cell(length(Ls), length(compres));
eldata_ind = cell(size(eldata));
eldata_val = cell(size(eldata));
laserE = cell(size(eldata));
for ll = 1:length(Ls)
    for pp = 1:length(compres)
        % Figure out shot numbers we need
        logm = (press{ll}==compres(pp)).*(1:length(press{ll}));
        logm = logm(logm~=0);
        % Now try to load the datafiles and work something out with them
        shotlogm = ones(size(logm));
        for ss=1:length(logm)
            fnam = fullfile(datfol, ['20150907r00' num2str(ll+5)], 'mat_manual',...
                sprintf('20150907r%03is%03i_Espec1.mat', ll+5, logm(ss)));
            laserE{ll,pp}(ss) = getenergy(getGSN(20150907, ll+5, logm(ss)));
            if exist(fnam, 'file')
                fdat = load(fnam);
                temp0 = smooth(interp1(fdat.shotdata.alongscreen, sum(fdat.shotdata.image,1), screenBins, 'pchip'));
                eldata{ll,pp}(ss,:) = temp0;
                logm_ = temp0~=0;
                temp = temp0(logm_);
                bgreg = (length(temp)-25):length(temp);
                BGfact = mean(temp(bgreg)) + 3*std(temp(bgreg));
                tempp = find(temp0>BGfact, 1, 'first');
                if isempty(tempp); continue; end;
                eldata_ind{ll,pp}(ss) = tempp;
                eldata_val{ll,pp}(ss) = getGem2015ScreenEnergy(screenBins(tempp), 1, 1, 8);
            else
                shotlogm(ss) = 0;
                continue;
            end
        end
        eldata{ll,pp} = eldata{ll,pp}(logical(shotlogm),:);
        eldata_ind{ll,pp} = eldata_ind{ll,pp}(logical(shotlogm));
        eldata_val{ll,pp} = eldata_val{ll,pp}(logical(shotlogm));
    end
end

% Plot the traces and the retrieved cut-off energies
hfig = figure(7); clf(hfig);
set(gca, 'NextPlot', 'add');
it = 1;
for ll = 1:length(Ls)
    for pp = 1:length(compres)
        for ss = 1:length(eldata_ind{ll,pp})
            imagesc(1:length(screenBins), it, eldata{ll,pp}(ss,:));
            plot(eldata_ind{ll,pp}(ss), it, 'ok');
            it = it+1;
        end
    end
end

%% And now plot the actual data
hfig = figure(624); clf(hfig); hh = [];
%set(hfig, 'Position', [300 250 1000 400], 'Color', 'w');
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
for pp=1:length(compres)
    %hh(pp) = plot(Ls, cellfun(@mean, eldata_val(:,pp)), '-o', 'Tag', ...
    vdat = cellfun(@mean, eldata_val(:,pp));
    verr = cellfun(@(x) std(x)/sqrt(length(x)), eldata_val(:,pp));
    hh(pp) = errorbar(Ls(~isnan(vdat)) + -0.1+0.1*pp, vdat(~isnan(vdat)), ...
        verr(~isnan(vdat)), '--o', 'Tag', ...
    sprintf('$n_e=%2.1f$', compres(pp)/31));
end
%legend(hh, get(hh, 'Tag'), 'Location', 'nw', 'Box', 'on', 'EdgeColor', 'none');
set(hh, 'LineWidth', 2);
ylabel('Electron energy / MeV');
xlabel('Cell length / mm');
grid on; makeNiceGridHG2(gca);
set(gca, 'XLim', [8 32]);
meanlaserE = mean(cellfun(@mean, laserE(:)))
make_latex(hfig); setAllFonts(hfig, 18);
if 1
    thesisfigure(hfig, 'halfpage', 'SI_II_LscanE', 'Chapter6', 'pdf', 450);
end

%% Now try to plot the charge

% Iterate over pressures for each dataset
eldata = cell(length(Ls), length(compres));
eldata_ind = cell(size(eldata));
eldata_valQ = cell(size(eldata));
laserE = cell(size(eldata));
for ll = 1:length(Ls)
    for pp = 1:length(compres)
        % Figure out shot numbers we need
        logm = (press{ll}==compres(pp)).*(1:length(press{ll}));
        logm = logm(logm~=0);
        % Now try to load the datafiles and work something out with them
        shotlogm = ones(size(logm));
        for ss=1:length(logm)
            fnam = fullfile(datfol, ['20150907r00' num2str(ll+5)], 'mat_manual',...
                sprintf('20150907r%03is%03i_Espec1.mat', ll+5, logm(ss)))
            laserE{ll,pp}(ss) = getenergy(getGSN(20150907, ll+5, logm(ss)));
            if exist(fnam, 'file')
                fdat = load(fnam);
                %temp0 = smooth(interp1(fdat.shotdata.alongscreen, sum(fdat.shotdata.image,1), screenBins, 'pchip'));
                %eldata{ll,pp}(ss,:) = temp0;
                %logm_ = temp0~=0;
                %temp = temp0(logm_);
                %bgreg = (length(temp)-25):length(temp);
                %BGfact = mean(temp(bgreg)) + 3*std(temp(bgreg));
                %tempp = find(temp0>BGfact, 1, 'first');
                %if isempty(tempp); continue; end;
                %eldata_ind{ll,pp}(ss) = tempp;
                eldata_valQ{ll,pp}(ss) = fdat.shotdata.totQ;
            else
                shotlogm(ss) = 0;
                continue;
            end
        end
        %eldata{ll,pp} = eldata{ll,pp}(logical(shotlogm),:);
        %eldata_ind{ll,pp} = eldata_ind{ll,pp}(logical(shotlogm));
        eldata_valQ{ll,pp} = eldata_valQ{ll,pp}(logical(shotlogm));
    end
end

%% Plot the traces and the retrieved cut-off energies
hfig = figure(623); clf(hfig); hh = [];
set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
for pp=1:length(compres)
    %hh(pp) = plot(Ls, cellfun(@mean, eldata_val(:,pp)), '-o', 'Tag', ...
    vdat = cellfun(@mean, eldata_valQ(:,pp));
    verr = cellfun(@(x) std(x)/sqrt(length(x)), eldata_valQ(:,pp));
    hh(pp) = errorbar(Ls(~isnan(vdat)) + -0.1+0.1*pp, vdat(~isnan(vdat)), ...
        verr(~isnan(vdat)), '--o', 'Tag', ...
    sprintf('$n_e=%2.1f$', compres(pp)/31));
end
legend(hh(1:4), get(hh(1:4), 'Tag'), 'Location', 'northwest', 'Orientation', 'vertical',...
    'Box', 'on', 'EdgeColor', 'none');
set(hh, 'LineWidth', 2);
ylabel('Charge / pC');
xlabel('Cell length / mm');
grid on; makeNiceGridHG2(gca);
set(gca, 'XLim', [8 32]);
make_latex(hfig); setAllFonts(hfig, 18);
if 1
    thesisfigure(hfig, 'halfpage', 'SI_II_LscanQ', 'Chapter6', 'pdf', 450);
end

%% And look at exit modes as well

rr = 4;
for i=13%:length(press{rr})
    EM = getGemini2015ExitMode(20150907, 5+rr, i);
    figure(88);
    imagesc(medfilt2(EM)); drawnow;
end