% If set up properly, this will analyse 20150826r006


date_ = '20150826';
run_ = '006';

nshots = 71;
ttt = struct;
Ls(1:4)   = 42; Ls(5:8) = 38; Ls(9:12) = 34; Ls(13:16) = 30;
Ls(17:20) = 26; Ls(21:24) = 22; Ls(25:28) = 20; Ls(29:32) = 18;
Ls(33:36) = 16; Ls(37:40) = 14; Ls(41:44) = 12; Ls(45:48) = 10;
Ls(49:56) = 8;  Ls(57:60) = 6;  Ls(61:65) = 4;  Ls(66:71) = 3;

% Save as .mat for peak analysis
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 4);
% Save into the correct path
if ismac
    savepath = ['/Users/kristjanpoder/Google Drive/Experiment/2015Gemini/EspecAnalysis/' date_ 'r' run_];
end

MagnetTrackerKP.fig.btrac.dat.outpath = fullfile(savepath, 'mat_20160808');
set(MagnetTrackerKP.fig.btrac.outpath_, 'String', fullfile(savepath, 'mat_20160808'));
datee = java.util.Date();
datee.setYear(2015-1900);
datee.setMonth(str2double(date_(5:6))-1);
datee.setDate(str2double(date_(7:8)));
MagnetTrackerKP.fig.input.date.setSelectedDate(datee);
MagnetTrackerKP.date_selected;
set(MagnetTrackerKP.fig.input.runbox, 'Value', str2double(run_));
MagnetTrackerKP.run_selected(str2double(run_));

%%
for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(str2double(date_), str2double(run_), i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).pres = 84.6;
    dat = getGemini2015Gas(str2double(date_), str2double(run_), i);
    ttt(i).gastrace = dat(55000:60000, 1:2);
    ttt(i).cellL = Ls(i);
    ttt(i).exitenergy = getGemini2015ExitEnergy(str2double(date_), str2double(run_), i);
    ttt(i).Q1 = MagnetTrackerKP.fig.btrac.dat.QandE(1,1);
    ttt(i).Q2 = MagnetTrackerKP.fig.btrac.dat.QandE(2,1);
    ttt(i).Q3 = MagnetTrackerKP.fig.btrac.dat.QandE(3,1);
    ttt(i).E1 = MagnetTrackerKP.fig.btrac.dat.QandE(1,2);
    ttt(i).E2 = MagnetTrackerKP.fig.btrac.dat.QandE(2,2);
    ttt(i).E3 = MagnetTrackerKP.fig.btrac.dat.QandE(3,2);
    toc
    drawnow; 
end

corrections = [27 8.88; 31 9.12; 44 9.97; 47 9.54];
for i=1:size(corrections,1); ttt(corrections(i,1)).laserE = corrections(i,2); end;

if ismac
    save(['/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/' date_ 'r' run_ '/rundata'], 'ttt');
end