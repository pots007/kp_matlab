% This ought to look at the file analysed by especPeakFinding.m

% 20150826r006, Gemini

if ismac
    datafol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150826r006/mat';
    savfol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150826r006';
elseif ispc
    datafol = 'E:\Kristjan\Google Drive\Experiment\2015Gemini\EspecAnalysis\20150826r006\mat';
    savfol = 'E:\Kristjan\Google Drive\Experiment\2015Gemini\EspecAnalysis\20150826r006\';
end

rundat = load(fullfile(savfol, 'rundata'));

flist = dir(fullfile(datafol, '*Espec1.mat'));
fnames = {flist.name};
saving = 0;
%
edata = zeros(11,length(fnames));
poo = zeros(size(fnames));
%%
for i=1:length(fnames)
    fprintf('File %i out of %i\n', i, length(fnames));
    ll = load(fullfile(datafol, fnames{i}));
    poo(i) = ll.shotdata.poo;
    if poo(i); continue; end
    shotno = str2double(fnames{i}(14:16));
    edata(1,i) = shotno;
    edata(2,i) = rundat.ttt(i).laserE;
    edata(3,i) = rundat.ttt(i).cellL;
    if isempty(rundat.ttt(i).exitenergy)
        edata(4,i) = nan;
    else
        edata(4,i) = rundat.ttt(i).exitenergy;
    end
    if ~isfield(ll.shotdata, 'max'); continue; end;
    edata(5,i) = ll.shotdata.max.low;
    edata(6,i) = ll.shotdata.max.val;
    edata(7,i) = ll.shotdata.max.high;
    edata(8,i) = ll.shotdata.peak.low;
    edata(9,i) = ll.shotdata.peak.val;
    edata(10,i) = ll.shotdata.peak.high;
    edata(11,:) = ll.shotdata.totQ;
end
poo = logical(poo);
edata = edata(:, ~poo);

%% Look at the gas as well!
totgas = zeros(71, 3001);
tgas = 400:0.1:700;
for i=1:71
    disp(i);
    dat = getGemini2015Gas(20150826, 6, i);
    %plot(dat(:,1), dat(:,3), 'k-')
    totgas(i,:) = interp1(dat(:,1), dat(:,3), tgas);
end
% This index here is common to all traces, and is just before the discharge dip
[~,ind] = min(abs(tgas-500.7));
shotpres = totgas(:,ind);

%% And now for plotting all kinds of things

% Plot the maximum energy as a function of length
hfig = 87;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
% errorbar for simplistic analysis
h1 = errorbar(edata(3,:), edata(6,:), edata(5,:), edata(7,:), 'o', 'LineWidth', 1.);
% Average each length point!
Ls = unique(edata(3,:));
Es = zeros(size(Ls));
for i=1:length(Ls)
    logm = edata(3,:)==Ls(i);
    Es(i) = mean(edata(6,logm));
end
hold on;
h2 = plot(Ls, Es, '-k', 'LineWidth', 3);
% And add the dephasing length!
hold off;
xlabel('Cell length / mm');
ylabel('Maximum energy / MeV');
%legend([h1 h2], {'Data', [num2str(npnts) ' point moving average']}, 'Location', 'Northeast');
set(gca, 'Position', [0.12 0.13 0.85 0.85], 'Box', 'on', 'LineWidth', 2);
make_latex(hfig);
setAllFonts(hfig,20);
export_fig(fullfile(savfol, 'MaxEn_ne_all'), '-pdf', '-nocrop', hfig);

%% Now zoom in on 3:20, and fit something to it - for the maximum energies of the low Q bunch
hfig = 89;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
set(gca, 'NextPlot', 'add', 'Box', 'on');
% errorbar for simplistic analysis
logm = edata(3,:)<17;
h1 = errorbar(edata(3,logm), edata(6,logm), edata(5,logm), edata(7,logm), 'o', 'LineWidth', 1.5);
% Average each length point!
Ls = unique(edata(3,:));
Es = zeros(size(Ls));
for i=1:length(Ls)
    logm_ = edata(3,:)==Ls(i);
    Es(i) = mean(edata(6,logm_));
end
logmav = 1:8; % This reduces the data to up to 16mm
h2 = plot(Ls(logmav), Es(logmav), '-k', 'LineWidth', 2);
% Now add a fit to the acceleration: a normal parabola
parabfit = polyfit(edata(3,logm), edata(6,logm), 2);
plotL1 = 3:0.1:16;
h3 = plot(plotL1, polyval(parabfit, plotL1), '--r', 'LineWidth', 2.5);
% And now, reflect the accelerating part about the max and fit a parabola
% to that: effectively a onesided parabola fit
logm_ = edata(3,:)<9;
logm3 = (edata(3,logm_)==3)*12; logm4 = (edata(3,logm_)==4)*10;
logm6 = (edata(3,logm_)==6)*6; logm8 = (edata(3,logm_)==8)*2;
templens = [edata(3,logm_) edata(3,logm_)+logm3+logm4+logm6+logm8];
tempens = [edata(6,logm_) edata(6,logm_)];
accelfit = polyfit(templens, tempens, 2);
plotL2 = 3:0.1:8;
h4 = plot(plotL2, polyval(accelfit, plotL2), '--m', 'LineWidth', 2.5);
xlabel('Cell length / mm');
ylabel('Maximum energy / MeV');
legend([h1 h2 h3 h4], {'Data', 'Average at each length', 'Fit to acc. and dec.', 'Fit to acc. only'},...
    'Location', 'South');
set(gca, 'Position', [0.12 0.13 0.85 0.85], 'Box', 'on', 'LineWidth', 2,...
    'XLim', [2 17], 'YLim', [400 1200]);
make_latex(hfig);
setAllFonts(hfig,20);
export_fig(fullfile(savfol, 'MaxEn_ne_fits_firstbunch'), '-pdf', '-nocrop', hfig);

%% Make it fancier by having the spectra on linear graphs as well.

hfig = 88;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 1200 600]);
set(gca, 'NextPlot', 'add');
load('owncolourmaps'); colormap(hfig, colmap.jet_white);

eldata = cell(length(fnames), 3);
ff = load('tracking_onAxis_2mrad_beam.mat');
alongs = ff.tracking.screen(1).alongscreen;
logm = ~isnan(alongs);
lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');
linenaxis = linspace(300, 1300, 1000);
ycrop = 50:150;
[L_sort,inds] = sort(edata(3,:)); % Sort to lengths!
Ls = unique(edata(3,:));

for i=1:length(inds)
    ll = load(fullfile(datafol, fnames{inds(i)}));
    %if ll.shotdata.poo; continue; end;
    enaxis = valfit(ll.shotdata.alongscreen);
    inputim = ll.shotdata.image(ycrop,:);
    dE = [diff(enaxis); 1];
    outputim = zeros(length(ycrop), length(linenaxis));
    for k=1:length(ycrop)
        outputim(k,:) = interp1(enaxis, inputim(k,:), linenaxis);
    end
    imagesc(linspace(i-1,i,length(ycrop)), linenaxis, outputim');
    %errorbar(edata(, edata(6,i), edata(5,i), edata(7,i), 'o', 'LineWidth', 1.);
    plot(i-1+0.5, edata(6,inds(i)), 'ok');
    plot(i-1+0.5, edata(9,inds(i)), 'xr');
    plot(i-1+0.5, 1200+(shotpres(inds(i))-2e18)/1e18*100, '*k');
    if i>1
        if L_sort(i-1)~=L_sort(i)
            plot((i-1)*[1 1], [min(linenaxis) min(linenaxis)+50], '--k');
            Lwidth = sum(edata(3,:)==L_sort(i-1));
            text(i-1-0.5*Lwidth, min(linenaxis)-50, num2str(L_sort(i-1)),...
                'HorizontalAlignment', 'center');
        end
    end
    if i==length(inds)
        text(i-0.5*Lwidth, min(linenaxis)-50, num2str(L_sort(i-1)),...
                'HorizontalAlignment', 'center');
    end
    drawnow;
end
plot([0 70], [1200 1200], '-k');
text(71, 1200, '2'); text(71,1300, '3');
text(76, 1100, '$n_e / 10^{18} \mathrm{cm}^{-3}$', 'Rotation', 90);
ylabel('$\mathcal{E}$ / MeV');
text(0.5*i, min(linenaxis)-100, 'Cell length / mm', 'HorizontalAlignment', 'center');
set(gca, 'XLim', [0 70], 'YLim', [min(linenaxis) max(linenaxis)], 'XTick', [],...
    'Box', 'on', 'LineWidth', 2);
setAllFonts(hfig,20); make_latex(hfig);
%export_fig(fullfile(savfol, 'LengthScan'), '-pdf', '-nocrop', hfig);
%% Then, the peak energy as a function of plasma density
% Plot the peak energy as a function of length
hfig = 89;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
set(gca, 'NextPlot', 'add');
% errorbar for simplistic analysis
h1 = errorbar(edata(3,:), edata(9,:), edata(8,:), edata(10,:), 'o', 'LineWidth', 1.);
% Average each length point!
Ls = unique(edata(3,:));
Es = zeros(size(Ls));
for i=1:length(Ls)
    logm = edata(3,:)==Ls(i);
    Es(i) = mean(edata(9,logm));
end
h2 = plot(Ls, Es, '-k', 'LineWidth', 2);
xlabel('Cell length / mm');
ylabel('Peak energy / MeV');
legend([h1 h2], {'Data', 'Average at each length'}, 'Location', 'Southeast');
set(gca, 'Position', [0.12 0.13 0.85 0.85], 'Box', 'on', 'LineWidth', 2);
make_latex(hfig);
setAllFonts(hfig,20);
export_fig(fullfile(savfol, 'PeakEn_length'), '-pdf', '-nocrop', hfig);

%% Now, zoom in on 12-20 mm, and plot the peak energies. This is the high charge bunch
hfig = 90;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
set(gca, 'NextPlot', 'add', 'Box', 'on');
% errorbar for simplistic analysis
logm = edata(3,:)>11 & edata(3,:)<21;
h1 = errorbar(edata(3,logm), edata(9,logm), edata(8,logm), edata(10,logm), 'o', 'LineWidth', 1.5);
% Average each length point!
Ls = unique(edata(3,:));
Es = zeros(size(Ls));
for i=1:length(Ls)
    logm_ = edata(3,:)==Ls(i);
    Es(i) = mean(edata(9,logm_));
end

logmav = 6:10; % This reduces the data to up to 16mm
h2 = plot(Ls(logmav), Es(logmav), '-k', 'LineWidth', 2);
% Now add a fit to the acceleration: a normal parabola
plotL1 = 3:0.1:16;
% And now, reflect the accelerating part about the max and fit a parabola
% to that: effectively a onesided parabola fit
%logm_ = edata(3,:)<9;
logm12 = (edata(3,logm)==12)*16; logm14 = (edata(3,logm)==14)*12;
logm16 = (edata(3,logm)==16)*8; logm18 = (edata(3,logm)==18)*4;
templens = [edata(3,logm) edata(3,logm)+logm12+logm14+logm16+logm18];
tempens = [edata(9,logm) edata(9,logm)];
accelfit = polyfit(templens-10, tempens, 2);
plotL2 = 12:0.1:20;
h4 = plot(plotL2, polyval(accelfit, plotL2-10), '--m', 'LineWidth', 2.5);
xlabel('Cell length / mm');
ylabel('Maximum energy / MeV');
legend([h1 h2 h4], {'Data', 'Average at each length', 'Fit to acc. only'},...
    'Location', 'SouthEast');
set(gca, 'Position', [0.12 0.13 0.85 0.85], 'Box', 'on', 'LineWidth', 2,...
    'XLim', [11 21], 'YLim', [250 650]);
make_latex(hfig);
setAllFonts(hfig,20);
%export_fig(fullfile(savfol, 'MaxEn_ne_fits_firstbunch'), '-pdf', '-nocrop', hfig);

%% Now, the exiting energy as a function of length
hfig = 91;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
set(gca, 'NextPlot', 'add', 'Box', 'on');
h1 = plot(edata(3,:), edata(4,:), 'ko', 'MarkerSize', 10, 'LineWidth', 2);
logm = ~isnan(edata(4,:)) & edata(4,:)~=0;
plotL = linspace(min(edata(3,logm)), max(edata(3,logm)), 50);
f1 = polyfitn(edata(3,logm), edata(4,logm), 1);
h2 = plot(plotL, polyval(f1.Coefficients, plotL), '-r');
f2 = polyfitn(edata(3,logm), edata(4,logm), 2);
h3 = plot(plotL, polyval(f2.Coefficients, plotL), '-m');
set([h1 h2 h3], 'LineWidth', 2);
xlabel('Gas cell length / mm');
ylabel('Energy on detector / J');

setAllFonts(hfig, 20); make_latex(hfig);