% This script will analyse 201509803r004, one that I looked through
% manually and where the backtracking angle was calculated

date_ = '20150826';
run_ = '006';

saveplot = 1;

datfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', [date_ 'r' run_], 'mat_20160808');
savfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', [date_ 'r' run_]);

% And redo what other scripts do in the analysis part
[nShots, runst] = getGem2015NumberofScanShots(date_, run_);
ttt = struct;
for i=1:nShots
    fprintf('Working on file %i out of %i\n', i, nShots);
    ttt(i).shotID = i;
    ttt(i).GSN = getGSN(str2double(date_), str2double(run_), i);
    ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).cellL = runst.cellL;
    dat = getGemini2015Gas(str2double(date_), str2double(run_), i);
    if ~isempty(dat);
        ttt(i).gastrace = dat(55000:60000, 1:3);
    else
        ttt(i).gastrace = 0;
    end
    ttt(i).exitenergy = getGemini2015ExitEnergy(str2double(date_), str2double(run_), i);
end

%% Now for the fun part. Find cutoff energies, with backtracked angles.
% First, interpolate all screen images onto a fixed grid. BUT the grid is
% in screen distance now.
screenBins1 = 0:0.1:300;
screenBins2 = 0:0.1:197;
scrmat1 = zeros(nShots, length(screenBins1));
scrmat2 = zeros(nShots, length(screenBins2));
btrack_thetas = zeros(nShots,1);
Qtots = nan(size(btrack_thetas)); Qhighs = nan(size(btrack_thetas));
for i=1:length(ttt)
    fnam1 = sprintf('%sr%ss%03i_Espec1.mat', date_, run_, i);
    try
        fdat = load(fullfile(datfol, fnam1));
        scrmat1(i,:) = smooth(interp1(fdat.shotdata.alongscreen, sum(fdat.shotdata.image,1), screenBins1, 'pchip', 0));
        Qtots(i) = fdat.shotdata.totQ;
        btrack_thetas(i) = fdat.shotdata.track_angle;
    catch err
        fprintf('Error for file %i: %s\n', i, err.message);
    end
    fnam2 = sprintf('%sr%ss%03i_Espec2LE.mat', date_, run_, i);
    try
        fdat = load(fullfile(datfol, fnam2));
        scrmat2(i,:) = smooth(interp1(fdat.shotdata.alongscreen, sum(fdat.shotdata.image,1), screenBins2, 'pchip', 0));
        Qhighs(i) = fdat.shotdata.totQ;
        btrack_thetas(i) = fdat.shotdata.track_angle;
    catch err
        fprintf('Error for file %i: %s\n', i, err.message);
    end
end
scrmat1 = fliplr(scrmat1); scrmat2 = fliplr(scrmat2);
%%
maxind = ones(nShots,2);
BG_reg1 = 30; BG_fact1 = 7;
BG_reg2 = 150; BG_fact2 = 5;
for i=1:nShots
    % Do it on a line by line basis here, as I changed the box size a lot!
    % First, Espec1s
    linespec = scrmat1(i,:);
    linespec = linespec(linespec~=0);
    if ~isempty(linespec); 
        bgreg = (length(linespec)-BG_reg1):length(linespec);
        BGmean_(i) = mean(linespec(bgreg));
        BGstd_(i) = std(linespec(bgreg));
        BGfact_(i) = BGmean_(i) + BG_fact1*BGstd_(i);
        temp = find(scrmat1(i,:)>(BGfact_(i)), 1, 'last');
        if ~isempty(temp); maxind(i,1) = temp; end
    end
    linespec = scrmat2(i,:);
    linespec = linespec(linespec~=0);
    if ~isempty(linespec); 
        bgreg = (length(linespec)-BG_reg2):length(linespec);
        BGmean_(i) = mean(linespec(bgreg));
        BGstd_(i) = std(linespec(bgreg));
        BGfact_(i) = BGmean_(i) + BG_fact2*BGstd_(i);
        temp = find(scrmat2(i,:)>(BGfact_(i)), 1, 'last');
        if ~isempty(temp); maxind(i,2) = temp; end
    end
end
% Apply some corrections:
%maxind([15 24]) = [1950 1975];
% Check it out
figure(103); clf(103);
set(gca, 'NextPlot', 'add');
imagesc(scrmat1);
plot(maxind(:,1), 1:nShots, 'ok');
axis tight;
figure(104); clf(104);
set(gca, 'NextPlot', 'add');
imagesc(scrmat2);
plot(maxind(:,2), 1:nShots, 'ok');
axis tight;

%set(gca, 'CLim', [0 1e7]);
% And now find the energies for the locations found.
% Again assume this is the +1 mrad divergence part!

% This is from when the espec1 had its filter, so assume a flat 8 mrad
% offset angle!
peakEns = nan(nShots, 2);
for i=1:nShots
    %if i==2; continue; end;
    if maxind(i,1)~=1
        peakEns(i,1) = getGem2015ScreenEnergy(300-screenBins1(maxind(i,1)), 1, 1, 8);
    end
    if maxind(i,2)~=1
        peakEns(i,2) = getGem2015ScreenEnergy(197-screenBins2(maxind(i,2)), 3, 1, 8);
    end
end
figure(105); clf(105);
plot(peakEns, '-x');

%% And the laser energies as well:
hfig = figure(33); clf(33); set(33, 'Color', 'w');
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
laserE = cell2mat({ttt.laserE}');
lm_las = laserE'>1;
laserE_ = laserE(lm_las);
ns = nShots;
rectangle('Position', [0 mean(laserE_)-std(laserE_) ns+1 2*std(laserE_)],...
    'EdgeColor', 0.94*[1 1 1], 'FaceColor', 0.94*[1 1 1]);
plot([0 ns+1], mean(laserE_)*[1 1], '--k', 'LineWidth', 2);
plot(1:ns, laserE, 'ok', 'MarkerSize', 5, 'LineWidth', 1.5);
ylabel('Laser energy / J'); xlabel('Shot');
setAllFonts(hfig, 20); make_latex(hfig);
set(gca, 'XLim', [-1 ns+2]);
if saveplot; export_fig(fullfile(savfol, 'LaserEnergies'), '-nocrop', '-pdf', hfig); end

%% Now, all the preceding has been fun and sorting data out.
% Now let's make some calculations and analysis, roughly as per usual

Ls = runst.cellL;
pp_unique = unique(Ls);
laserE = cell2mat({ttt.laserE})';
%pp_vs_ens1 = cell(size(pp_unique));
%pp_vs_Qtots = cell(size(pp_unique));
%pp_vs_Qhighs = cell(size(pp_unique));
clear pp_st; pp_st(length(pp_unique)) = struct;
for i=1:length(pp_unique)
%    logm = pps==pp_unique(i) & Qtots>3 & ~isnan(peakEns(:,1));
%    pp_vs_ens1{i} = peakEns(logm,1);
%    pp_vs_Qtots{i} = Qtots(logm);
%    pp_vs_Qhighs{i} = Qhighs(logm);
    % New analysis structure:
    ll = Ls==pp_unique(i);
    temppp = ll'.*(1:length(Ls));
    pp_st(i).shots = temppp(temppp~=0);
    pp_st(i).pres = pp_unique(i);
    pp_st(i).peakEns1 = peakEns(ll,1);
    pp_st(i).peakEns1_logm = ~isnan(pp_st(i).peakEns1); % Tells us which shots are not nans
    pp_st(i).peakEns2 = peakEns(ll,2);
    pp_st(i).peakEns2_logm = ~isnan(pp_st(i).peakEns2); % Tells us which shots are not nans
    pp_st(i).Qtots = Qtots(ll);
    pp_st(i).Qhighs = Qhighs(ll);
    pp_st(i).laserE = laserE(ll);
    pp_st(i).laserE_logm = lm_las(pp_st(i).shots)'; %Tells us which shots of the pressure are good for laser    
end

%% Quickly plot the gas densities, just to confirm superbox was fine
neData = [];
for i=1:nShots
    if ttt(i).gastrace~=0
        neData(i,:) = smooth(ttt(i).gastrace(:,3))';
    end
end
figure(106); clf(106);
plot(neData');
for i=1:length(pp_st)
    pp_st(i).nes_traces = neData(pp_st(i).shots, :);
    pp_st(i).nes_vals = mean(neData(pp_st(i).shots, end-100:end), 2);
    % This needs automatic rejection written into it!
    pp_st(i).nes_logm = pp_st(i).nes_vals~=0;
end
hfig = figure(33); clf(33);  set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add', 'Box', 'on');
hnes = [];
for i=1:length(pp_st)
    %plot(pp_st(i).nes_traces'*1e-18);
    plot(pp_st(i).nes_vals*1e-18, 'x--');
end
ylabel('$n_e / 10^{18}\mathrm{cm}^{-3}$');
grid on;
setAllFonts(hfig, 20); make_latex(hfig);
set(gca, 'XTickLabel', []);
if saveplot; export_fig(fullfile(savfol, 'GasTraces'), '-nocrop', '-pdf', hfig); end

%% Now we calculate the average values and errors at scan points:
%nanav = @(x) mean(x, 1, 'omitnan'); % Shortcut to not have to use the tedious logm-s
% Now go through each entry and check for all the associated log matrices:
pp_vs_ens = cell(length(pp_st), 2);
pp_vs_Q = cell(size(pp_vs_ens));
for i=1:length(pp_st)
    logm1 = pp_st(i).peakEns1_logm & pp_st(i).laserE_logm & pp_st(i).nes_logm;
    pp_vs_ens{i,1} = pp_st(i).peakEns1(logm1);
    pp_vs_Q{i,1} = pp_st(i).Qtots(logm1);
    logm2 = pp_st(i).peakEns2_logm & pp_st(i).laserE_logm & pp_st(i).nes_logm;
    pp_vs_ens{i,2} = pp_st(i).peakEns2(logm2);
    pp_vs_Q{i,2} = pp_st(i).Qhighs(logm2);
end
pp_vs_ens_mean  = cellfun(@mean, pp_vs_ens); pp_vs_ens_std  = cellfun(@std, pp_vs_ens);
pp_vs_Q_mean  = cellfun(@mean, pp_vs_Q); pp_vs_Q_std  = cellfun(@std, pp_vs_Q);

%%
for i=1%:3
    hfig = figure(33+i); clf(hfig);
    set(hfig, 'Color', 'w', 'Position', [400 100 800 600]);
    set(gca, 'NextPlot', 'add', 'Box', 'on');
    switch i
        case 1 % Energy variations for all worthy shots
            %logm = Qtots>3;
            %hens = plot(pps(logm), peakEns(logm,2), 'dk', pps(~logm), peakEns(~logm,2), 'db');
            hens = plot(Ls, peakEns(:,2), 'dk');
            hens_err = errorbar(pp_unique, pp_vs_ens_mean(:,2), pp_vs_ens_std(:,2), '-ro');
            legend([hens' hens_err], {'Shot $\mathcal{E}$', 'Shot $\mathcal{E}$, $Q<3$ pC',...
                '$\mathcal{E}\pm \sigma_{\mathcal{E}}$'}, 'Location', 'Northeast');
            ylabel('Peak energy / MeV');
            title('Peak electron energy vs $n_e$, Espec2LE');
            fname = 'PeakEn_vs_L_Espec2_btracked';
        case 2
            hens = plot(Ls, Qtots, 'dk');
            hens_err = errorbar(pp_unique, pp_vs_Q_mean(:,1), pp_vs_Q_std(:,1), '-ro');
            legend([hens hens_err], {'Shot $\mathcal{Q}$', '$\mathcal{Q}\pm \sigma_{\mathcal{Q}}$'}, 'Location', 'Northwest');
            ylabel('$Q_{tot}$ / pC');
            title('Total beam charge vs $n_e$');
            fname = 'Qtot_vs_ne';
        case 3
            hens = plot(Ls, Qhighs, 'dk');
            hens_err = errorbar(pp_unique, pp_vs_Q_mean(:,2), pp_vs_Q_std(:,2), '-ro');
            legend([hens hens_err], {'Shot $\mathcal{Q}$', '$\mathcal{Q}\pm \sigma_{\mathcal{Q}}$'}, 'Location', 'Northwest');
            ylabel('$Q_{\mathcal{E}>900MeV}$ / MeV');
            title('High energy charge vs $n_e$');
            fname = 'Qhigh_vs_ne';
        case 4
            
    end
    set(hens, 'MarkerSize', 10, 'LineWidth', 1.5);
    set(hens_err, 'MarkerSize', 10, 'LineWidth', 2);
    xlabel('L / mm');
    grid on;
    setAllFonts(hfig, 20); make_latex(hfig);
    set(gca, 'Position', [0.15 0.13 0.77, 0.79]);
    if saveplot; export_fig(fullfile(savfol, fname), '-nocrop', '-pdf', hfig); end
end


%% Now look at the divergence

% plot for random shot the divergence as a function of energy
fdat = load(fullfile(datfol, flist2(19).name));
im = fdat.shotdata.image;
yax = fdat.shotdata.divax; ddiv = abs(mean(diff(yax)));
enax = fdat.shotdata.tracking_btrac(fdat.shotdata.alongscreen);
divdat = nan(size(im,2), 2);
for i=1:size(im,2)
    divdat(i,1) = enax(i);
    try
        divdat(i,2) = widthfwhm(im(:,i)')*ddiv;
    catch
    end
end
plot(divdat(:,1), divdat(:,2), divdat(:,1), 2000./divdat(:,1), '-k')
ylim([0 5]); xlim([0 2200])

%% Create a struct for easier analysis later on
clear D0826r006;
D0826r006.EspecData1 = scrmat1;
D0826r006.EspecData2 = scrmat2;
D0826r006.peakEns1 = peakEns(:,1);
D0826r006.peakEns2 = peakEns(:,2);
D0826r006.nes = Ls;
D0826r006.cellL = runst.cellL;
D0826r006.pp_unique = pp_unique;
D0826r006.Qhighs = Qhighs;
D0826r006.Qtots = Qtots;
D0826r006.pp_vs_ens1 = pp_vs_ens(:,1);
D0826r006.pp_vs_ens2 = pp_vs_ens(:,2);
D0826r006.pp_vs_Qtots = pp_vs_Q(:,1);
D0826r006.pp_vs_Qhighs = pp_vs_Q(:,2);
D0826r006.laserE = laserE;
save(fullfile(savfol, 'rundata_analysis'), 'D0826r006');