% Returns the number of shots in a pressure or length scan

function [nShots, runstruct] = getGem2015NumberofScanShots(date_, run_)
% date_ = '20150903';
% run_ = '002';
if ischar(date_)
    date_ = str2double(date_);
end

if ischar(run_)
    run_ = str2double(run_);
end

load('Gem2015PressureScans');
load('Gem2015LengthScans');
% Try to find it in the pressurescans first
nind = find(cell2mat({PressureScans.date})==date_ & cell2mat({PressureScans.run}) == run_);
if isempty(nind)
    nind = find(cell2mat({LengthScans.date})==date_ & cell2mat({LengthScans.run}) == run_);
else
    nShots = length(PressureScans(nind).pressure);
    runstruct = PressureScans(nind);
    return;
end

if isempty(nind)
    nShots = [];
else
    nShots = length(LengthScans(nind).cellL);
    runstruct = LengthScans(nind);
end
