L = 200;
L1 = 490;
L0 = 510;
x = 0:1:3000;
den = zeros(size(x));
rho0 = 2.5;
rhoL = 1.5;
rhoH = 4;
for i=1:length(x)
    if x(i)> 10
        if x(i)<510
            den(i) = (x(i)-10)/500*rho0;
        else
            den(i) = rho0;
        end
    else
        den(i) = 0;
    end
    %density = if (x lt (L0+L1+L), if(x gt (L0+L1), (rhoH-rho0)*(x-L0-L1)/L+rho0, density(electron)), rho0)
    if x(i) < (L0+L1+L)
        if x(i) > (L0+L1)
            den(i) = (rhoH-rho0)*(x(i)-L0-L1)/L+rho0;
        end
    else
        den(i) = rho0;
    end
    %density = if (x lt (L0+L1+2*L), if(x gt (L0+L1+L), (rhoH-rhoL)*(L0+L1+2*L-x)/L+rhoL, density(electron)), rhoL)
    if x(i)<(L0+L1+2*L)
        if x(i) > (L0+L1+L)
            den(i) = (rhoH-rhoL)*(L0+L1+2*L-x(i))/L+rhoL;
        end
    else
        den(i) = rhoL;
    end
    %density = if (x lt (L0+L1+4*L), if(x gt (L0+L1+3*L), (rhoH-rhoL)*(x-L0-L1-3*L)/L+rhoL, density(electron)), rho0)
    if x(i) < (L0+L1+4*L)
        if x(i) > (L0+L1+3*L)
            den(i) = (rhoH-rhoL)*(x(i)-L0-L1-3*L)/L+rhoL;
        end
    else
        den(i) = rho0;
    end
    %density = if (x lt (L0+L1+5*L), if(x gt (L0+L1+4*L), (rhoH-rho0)*(L0+L1+5*L-x)/L+rho0, density(electron)), rho0)
    if x(i) < (L0+L1+5*L)
        if x(i) > (L0+L1+4*L)
            den(i) = (rhoH-rho0)*(L0+L1+5*L-x(i))/L+rho0;
        end
    else
        den(i) = rho0;
    end
end
figure(888); clf;
den = den*1e23;
h1 = plot(x, den, '.');
x2 = 750:1:2000;
denshort = den(sum((x==750).*x):sum((x==2000).*x));
pfit = polyfit(x2, denshort, 16);
hold on;
h2 = plot(x2, polyval(pfit, x2), '.-r');
xlim([0 2100]);
pfitR = zeros(size(pfit));
for i=1:length(pfit)
    fprintf('%+2.15e*xm^%i', pfit(i), length(pfit)-i);
    temps = sprintf('%2.15e', pfit(i));
    pfitR(i) = str2double(temps);
end
fprintf('\n');
h3 = plot(x2, polyval(pfitR, x2), '.-g');
set([h1 h2 h3], 'LineWidth', 1.5);
xlabel('Propagation distance / \mu m');
ylabel('Plasma density / m^{-3}');
legend([h1 h2 h3], {'Density', 'Polyfit', 'Polyfit, trunc'}, 'Location', 'NorthWest');
setAllFonts(888, 22);
