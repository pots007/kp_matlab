function dat = parseGentecFile(filename, structflag)
%filename = '20150822.acq';
if nargin==1
    structflag = 1;
end
dataf = getFileText(filename);
dat = struct;
it = 1;
for i=1:length(dataf)
    A = sscanf(dataf{i}, '%i%i:%i:%i,%s %s %s %s %f')';
    if isempty(A); continue; end;
    %dat(it).shotID = A(1);
    dat(it).energy = A(end);
    tt = isspace(dataf{i}).*(1:length(dataf{i}));
    inds = tt(tt~=0);
    forma = 'HH:MM:SS, dddd, mmmm dd, yyyy';
    dat(it).datenum = datenum(dataf{i}(inds(1):inds(end)), forma);
    it = it+1;
end
if ~structflag % return matrix instead of struct
    matdat = zeros(length(dat),2);
    for i=1:length(dat)
        matdat(i,1) = dat(i).datenum;
        matdat(i,2) = dat(i).energy;
    end
    dat = matdat;
end

