E0 = 15;
tau = 40e-15;
fno = 40;
L1 = 230;
D1 = L1/fno; % in cm;
I1 = E0/(pi*D1^2*0.25*tau);
fprintf('Beam D = %2.2f cm, I = %2.2e Wcm^-2\n', D1, I1);

% Thresholds in von der Linde JOSA B
% Fused silica  @ 1e13 Wcm-2
% Glass         @ 2.8e13 Wcm-2

% But what glass???????? 'Optical glass' = BK7