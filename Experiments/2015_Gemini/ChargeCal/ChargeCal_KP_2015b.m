% Do the charge calibrations for different days. Load all the IPs, then do
% fancy-pancy background fitting and find total charge in the beam. Then,
% so same for Lanex images for those shots and produce a number for
% calibration. Look over saturation issues for now; just give a very rough
% estimate as to how much of a lower estimate our number would be.

% -----   THIS VERSION USES THE WARP REFERENCES AS SET UP IN MagnetTracker

%% First, let's load the data
% Imageplates first
if ispc
    IP(1).IP_QE = ReadRAW16bit('E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\2015ChargeCalibration\20150828\Cali_1_1\Cali_1_1.img', 2750, 500)';
    IP(2).IP_QE = ReadRAW16bit('E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\2015ChargeCalibration\20150904_chargecal\20150904_cc\20150904_cc.img', 1250, 3250);
    IP(3).IP_QE = ReadRAW16bit('E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\2015ChargeCalibration\20150926_cc_1\20150926_cc_1.img', 2750, 1250)';
    lanexfol = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\Data';
elseif ismac
    %IP(1).IP_QE = ReadRAW16bit('/Users/kristjanpoder/Experiments/2015_Gemini/2015ChargeCalibration/26082015_Gemini/26082015_Gemini.img', 2125, 1375)';
    IP(1).IP_QE = ReadRAW16bit('/Volumes/Gemini2015/Data/ChargeCalibration/Cali_1_1/Cali_1_1.img',...
        2750, 500)';
    IP(2).IP_QE = ReadRAW16bit('/Volumes/Gemini2015/2015ChargeCalibration/20150904_chargecal/20150904_cc/20150904_cc.img',...
        1250, 3250);
    IP(3).IP_QE = ReadRAW16bit('/Volumes/Gemini2015/2015ChargeCalibration/20150926_cc_1/20150916_cc_1.img',...
        2750, 1250)';
    lanexfol = '/Volumes/Gemini2015/Data/';
end
lanex(1).scr(1).raw = double(imread(fullfile(lanexfol, '20150828',...
    '20150828r002', '20150828r002s001_Espec1.tif')));
lanex(1).scr(2).raw = ReadRAW16bit(fullfile(lanexfol, '20150828',...
    '20150828r002', '20150828r002s001_Espec2LE.raw'), 1280, 960)';
lanex(2).scr(1).raw = double(imread(fullfile(lanexfol, '20150904',...
    '20150904r001', '20150904r001s005_Espec1.tif')));
lanex(2).scr(2).raw = flipud(ReadRAW16bit(fullfile(lanexfol, '20150904',...
    '20150904r001', '20150904r001s005_Espec2LE.raw'), 1280, 960));
lanex(3).scr(1).raw = double(imread(fullfile(lanexfol, '20150926',...
    '20150926r006', '20150926r006s006_Espec1.tif')));
lanex(3).scr(2).raw = double(imread(fullfile(lanexfol, '20150926',...
    '20150926r006', '20150926r006s006_Espec2LE.tif')))';

titls = {'20150828', '20150904', '20150926'};
savfol = fullfile(getDropboxPath, 'Writing', 'ChargeCal_KP_2015');
load('owncolourmaps');

PSL = @(im,R,S,G,L) (R/100)^2*(4000/S)*10.^(L*(im./G-0.5));
R = 100; S = 5000; G=2^16-1; L = 5;

for i=1:3
    IP(i).IPraw = PSL(IP(i).IP_QE, R, S, G, L);
end

%% Now, let's plot the RAW IP images if we want - in QEs
figure(201); clf;
set(201, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(3,1, [0.1 0.05 0.05 0.05], 0.05, 'across');
axs = zeros(1,3);
for i=1:3
    axs(i) = axes('Parent', 201, 'Position', locs(:,i));
    imagesc(IP(i).IP_QE);
    title(axs(i), titls{i});
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^16]);
setAllFonts(201,26); make_latex(201);
colormap(201, parula);
export_fig(fullfile(savfol, 'IP_rawsQL'), '-pdf', '-nocrop', 201);


%% Now, let's plot the RAW IP images if we want - PSLs here
figure(201); clf;
set(201, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(3,1, [0.1 0.05 0.05 0.05], 0.05, 'across');
axs = zeros(1,3);
for i=1:3
    axs(i) = axes('Parent', 201, 'Position', locs(:,i));
    imagesc(IP(i).IPraw);
    title(axs(i), titls{i});
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^8]);
setAllFonts(201,20); make_latex(201);
colormap(201, parula);
export_fig(fullfile(savfol, 'IP_rawsPSL'), '-pdf', '-nocrop', 201);

%% Let's get to business: crop out regions of IP in all images
% Define the regions
%IP(1).scr(1).roi = [390 25 960 1820];
%IP(1).scr(2).roi = [22 10 325 690];
IP(1).scr(1).roi = [25 38 320 2460];
IP(1).scr(2).roi = [25 38 320 2460];
IP(2).scr(1).roi = [605 77 480 2050];
IP(2).scr(2).roi = [10 1520 475 1000];
IP(3).scr(1).roi = [520 30 460 2330];
IP(3).scr(2).roi = [10 10 480 1615];
% And create subimages of these
for i=1:3
    for k=1:2
        rr = IP(i).scr(k).roi;
        IP(i).scr(k).dat = IP(i).IPraw(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3));
    end
end
% And try to plot as many as we have.
figure(201); clf;
set(201, 'Color', 'w', 'Position', [100 100 1000 700]);
colormap(201, parula);
locs = GetFigureArray(3,1, [0.1 0.05 0.05 0.05], 0.05, 'across');
axs = zeros(1,3);
for i=1:3
    axs(i) = axes('Parent', 201, 'Position', locs(:,i));
    imagesc(IP(i).IP_QE);
    title(axs(i), titls{i});
    rectangle('Position', IP(i).scr(1).roi, 'EdgeColor', 'm', 'LineWidth', 2);
    rectangle('Position', IP(i).scr(2).roi, 'EdgeColor', 'm', 'LineWidth', 2);
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^16]);
setAllFonts(201,20); make_latex(201);
export_fig(fullfile(savfol, 'IP_rois_QL'), '-pdf', '-nocrop', 201);

%% Now define the signal and background regions on all IP images
% Define signals; assume everything else is background to fit surface over
%IP(1).scr(1).sigroi = [535 265 370 510];
%IP(1).scr(2).sigroi = [160 75 94 78];
IP(1).scr(1).sigroi = [80 85 135 1660];
IP(1).scr(2).sigroi = [80 85 135 1660];
IP(2).scr(1).sigroi = [255 60 85 1875];
IP(2).scr(2).sigroi = [190 91 200 910];
IP(3).scr(1).sigroi = [85 45 260 2240];
IP(3).scr(2).sigroi = [160 1 308 1575];
% And plot them again
figure(202); clf;
set(202, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.05 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 202, 'Position', locs(:,i));
    dat = IP(ii(i)).scr(kk(i)).dat; 
    dat(dat==PSL(2^16-1,R,S,G,L)) = nan; 
    imagesc(dat);
    title(axs(i), [titls{ii(i)} ' IP ' num2str(kk(i))]);
    rectangle('Position', IP(ii(i)).scr(kk(i)).sigroi, 'EdgeColor', 'k', 'LineWidth', 2);
end
colormap(colmap.jet_white);
%set(axs, 'CLim', [0 2^8]);
setAllFonts(202,20); make_latex(202);
export_fig(fullfile(savfol, 'IP_signalrois'), '-pdf', '-nocrop', 202);

%% Now do the background subtraction: fit a surface to everything apart from signal

method = [2 2 2; 2 2 2];
for i=1:3
    for k=1:2
        fprintf('Fitting IP %i, screen %i\n', i, k);
        dat = IP(i).scr(k).dat;
        rr = IP(i).scr(k).sigroi;
        IP(i).scr(k).bg = createBackground(dat, rr, [], [], method(k,i));
    end
end

%% And plot the backgrounds, so we can estimate how good they are
figure(203); clf;
set(203, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.05 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 203, 'Position', locs(:,i));
    imagesc(IP(ii(i)).scr(kk(i)).bg);
    title(axs(i), [titls{ii(i)} ' IP ' num2str(kk(i))]);
end
%set(axs, 'CLim', [0 2^16]);
setAllFonts(203,20); make_latex(203);
export_fig(fullfile(savfol, 'IP_signalBGs'), '-pdf', '-nocrop', 203);


%% Plot the IPs with the background subtracted

figure(203); clf;
set(203, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.11 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 203, 'Position', locs(:,i));
    dat = IP(ii(i)).scr(kk(i)).dat-IP(ii(i)).scr(kk(i)).bg;
    rr = IP(ii(i)).scr(kk(i)).sigroi; 
    fprintf('IP %i, scr %i: total signal counts = %2.2e ; Total IP Q = %2.2f pC\n',...
        ii(i), kk(i), sum(dat(:)), sum(IP(ii(i)).scr(kk(i)).dat(:))*6900/20*qe*1e12);
    totQ = sum(dat(:))*6900/20*qe*1e12;
    IP(ii(i)).scr(kk(i)).Qtot = totQ;
    ttemp = dat;
    ttemp(IP(ii(i)).scr(kk(i)).dat==PSL(2^16-1,R,S,G,L)) = nan;
    IP(ii(i)).scr(kk(i)).Nsatpixels = sum(isnan(ttemp(:)));
    IP(ii(i)).scr(kk(i)).Qsatpixels = sum(sum(dat(isnan(ttemp))))*6900/20*qe*1e12;
    imagesc(dat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3)));
    title(axs(i), {[titls{ii(i)} ' IP ' num2str(kk(i))] sprintf('$Q_{tot}=%2.0f pC$', totQ)});
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^8]); 
colormap(colmap.jet_white);
cb = colorbar('Peer', axs(1), 'Location', 'North');
set(cb, 'Position', [0.05 0.08 0.9 0.02])
xlabel(cb, 'Charge / PSL')
setAllFonts(203,20); make_latex(203);
export_fig(fullfile(savfol, 'IP_signalrois'), '-pdf', '-nocrop', 203);



%% Let's do it properly now, for images where we warp the screen back

% ----------   20150904, screen 1

%  I won't delete the code for earlier, but it doesn't serve much use, as
%  when using it with MagnetTracker, one needs to take into account that
%  the warping doesn't exactly conserve integral ( I think ). In any case,
%  spatial extent etc changes, so take this into account here.

% The following should be valid for 0902r004 up to 20150911. On the latter
% date the screen has moved, so should do a new warp.

% For CC for pre 0902r003, can use the same warp calibration. This will be
% done below.

warpdat = load('/Users/kristjanpoder/Experiments/2015_Gemini/EspecRefs/Screen1_20150902r002s030CalibData');

ss = warpdat.datt.warpdat.cropsize;
cropim = lanex(2).scr(1).raw(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3));
rotim = imrotate(cropim, warpdat.datt.warpdat.thet);
warpim = imwarp(rotim, warpdat.datt.warpdat.q.tform, 'cubic');
warpim = warpim./warpdat.datt.warpdat.q.J2;
logmx = warpdat.datt.warpdat.q.x>0 & warpdat.datt.warpdat.q.x<warpdat.datt.warpdat.q.screenx(3);
logmy = warpdat.datt.warpdat.q.y>0 & warpdat.datt.warpdat.q.y<warpdat.datt.warpdat.q.screeny(3);
lanexim = warpim(logmy,logmx);

rr = [430 205 1235 65]; 
lanexBG = createBackground(lanexim, rr, [], [], 2);
lanexdat = lanexim - lanexBG; 
lanexdat = lanexdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3));

%% And now plot the stuff - screen 1 of 0904

% Middle of first peak on IP is 687, on lanex is 466.
% Index of half-full-height of last peaks on IP is 1829, on lanex is 1210.
% Index of middle peak on IP is 1478, lanex is 977.
cal(1).IProi = [1 30 687 45]; cal(1).lanexroi = [1 20 466 35]; cal(1).col = 'g';
cal(1).IProi = [1 1 687 84]; cal(1).lanexroi = [1 1 466 64]; cal(1).col = 'g';
cal(2).IProi = [687 30 380 20]; cal(2).lanexroi = [466 19 248 25]; cal(2).col = 'm';
cal(2).IProi = [687 1 380 85]; cal(2).lanexroi = [466 1 248 65]; cal(2).col = 'm';
cal(3).IProi = [1 1 1875 85]; cal(3).lanexroi = [1 1 1235 65]; cal(3).col = 'k';
% This is at peak1+0.76*(endHM-peak1), width = (endHM-peak2)*0.37
cal(3).IProi = [1555 1 101 85]; cal(3).lanexroi = [1031 1 86 65]; cal(3).col = 'k';

figure(206); clf(206);
set(206, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(1, 2, [0.05 0.15 0.05 0.05], 0.05, 'across');
ax1 = axes('Parent', 206, 'Position', locs(:,1));
IPdat = IP(2).scr(1).dat-IP(2).scr(1).bg;
rr = IP(2).scr(1).sigroi; 
IPdat = IPdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3))';
imagesc(IPdat, 'Parent', ax1);
%rectangle('Parent', ax1, 'Position', IProi, 'EdgeColor', 'k', 'LineWidth', 2);
cb = colorbar('Peer', ax1, 'Location', 'West');
set(cb, 'Position', [0.86 locs(2,1) 0.02 locs(4,1)]);
ylabel(cb, 'Charge / PSL');
ylabel(ax1, 'Image plate');

ax2 = axes('Parent', 206, 'Position', locs(:,2));
imagesc(lanexdat, 'Parent', ax2);
%rectangle('Parent', ax2, 'Position', lanexroi, 'EdgeColor', 'k', 'LineWidth', 2);
cb = colorbar('Peer', ax2, 'Location', 'West');
set(cb, 'Position', [0.86 locs(2,2) 0.02 locs(4,2)]);
ylabel(cb, 'Charge / Counts');
ylabel(ax2, 'Espec1 camera, warped');

colormap(206, colmap.jet_white);


set([ax1 ax2], 'XTick', [], 'YTick', []);
set(ax2, 'CLim', [min(lanexdat(:)) 4000])

for i=1:3
    IProi = cal(i).IProi;
    lanexroi = cal(i).lanexroi;
    rectangle('Parent', ax1, 'Position', cal(i).IProi, 'EdgeColor', cal(i).col, 'LineWidth', 2);
    rectangle('Parent', ax2, 'Position', cal(i).lanexroi, 'EdgeColor', cal(i).col, 'LineWidth', 2);
    totIP_Q = sum(sum(IPdat(IProi(2):IProi(2)+IProi(4),...
        IProi(1):IProi(1)+IProi(3))))*6900/20*1e12*qe;
    totlanex_C = sum(sum(lanexdat(lanexroi(2):lanexroi(2)+lanexroi(4),...
        lanexroi(1):lanexroi(1)+lanexroi(3))));
    cal(i).calfact = totIP_Q/totlanex_C;
    text(650-(650*(2-i)), 92, sprintf('%2.2e pC/count', cal(i).calfact), 'Parent', ax1,...
        'Color', cal(i).col, 'FontSize', 20);
end

setAllFonts(206,30); make_latex(206);
if saving; export_fig(fullfile(savfol, 'Screen1, mid'), '-pdf', '-nocrop', 206); end;
%% Let's do it properly now, for images where we warp the screen back

% ----------   20150904, screen 2


% The following should be valid from 0902r004

% For CC for pre 0902r003, can use the same warp calibration. This will be
% done below.

warpdat = load('/Users/kristjanpoder/Experiments/2015_Gemini/EspecRefs/Screen3_20150819_espec2lerefCalibData');

ss = warpdat.datt.warpdat.cropsize;
cropim = lanex(2).scr(2).raw(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3));
rotim = imrotate(cropim, warpdat.datt.warpdat.thet);
warpim = imwarp(rotim, warpdat.datt.warpdat.q.tform, 'cubic');
warpim = warpim./warpdat.datt.warpdat.q.J2;
logmx = warpdat.datt.warpdat.q.x>0 & warpdat.datt.warpdat.q.x<warpdat.datt.warpdat.q.screenx(3);
logmy = warpdat.datt.warpdat.q.y>0 & warpdat.datt.warpdat.q.y<warpdat.datt.warpdat.q.screeny(3);
lanexim = warpim(logmy,logmx);

rr = [1 200 1160 180]; 
lanexBG = createBackground(lanexim, rr, [], [], 2);
lanexdat = lanexim - lanexBG; 
lanexdat = lanexdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3));

%% And now plot the stuff - screen 2 of 0904
lanexroi = [1 1 1150 180];

figure(206); clf(206);
set(206, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(1, 2, [0.05 0.15 0.05 0.05], 0.05, 'across');
ax1 = axes('Parent', 206, 'Position', locs(:,1));
IPdat = IP(2).scr(2).dat-IP(2).scr(2).bg;
rr = IP(2).scr(2).sigroi; 
IPdat = IPdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3))';
imagesc(fliplr(IPdat), 'Parent', ax1);
%rectangle('Parent', ax1, 'Position', IProi, 'EdgeColor', 'k', 'LineWidth', 2);
cb = colorbar('Peer', ax1, 'Location', 'West');
set(cb, 'Position', [0.86 locs(2,1) 0.02 locs(4,1)]);
ylabel(cb, 'Charge / PSL');
ylabel(ax1, 'Image plate');

ax2 = axes('Parent', 206, 'Position', locs(:,2));
imagesc(lanexdat, 'Parent', ax2);
rectangle('Parent', ax2, 'Position', lanexroi, 'EdgeColor', 'k', 'LineWidth', 2);
cb = colorbar('Peer', ax2, 'Location', 'West');
set(cb, 'Position', [0.86 locs(2,2) 0.02 locs(4,2)]);
ylabel(cb, 'Charge / Counts');
ylabel(ax2, 'Espec1 camera, warped');

colormap(206, colmap.jet_white);

set([ax1 ax2], 'XTick', [], 'YTick', []);
%set(ax2, 'CLim', [min(lanexdat(:)) 4000])


totIP_Q = sum(IPdat(:))*6900/20*1e12*qe;
totlanex_C = sum(sum(lanexdat(lanexroi(2):lanexroi(2)+lanexroi(4),...
        lanexroi(1):lanexroi(1)+lanexroi(3))));
calfact = totIP_Q/totlanex_C;
text(450, 214, sprintf('%2.2e pC/count', calfact), 'Parent', ax1,...
    'Color', 'k', 'FontSize', 20);

setAllFonts(206,30); make_latex(206);
if saving; export_fig(fullfile(savfol, 'Screen1, mid'), '-pdf', '-nocrop', 206); end;
%% And finally, pop out a table of the total charges and calibration numbers

% Estimate the error by saying at most, the saturated IP pixels could have
% held twice the PSL they scanned out as... very dodgy but will do it
% properly later on. In fact, this only really affects the 20150904 espec1
% calibration.
oversaturationfactor = 2;

ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
fprintf('\n%10s\t%5s\t%5s\t%9s\t%9s\t%6s\n', 'Date', 'Scr', 'Q/pC', 'C_tot',...
    'Cal', 'Error');
for i=1:6
    Qtot = IP(ii(i)).scr(kk(i)).Qtot;
    Ctot = lanex(ii(i)).scr(kk(i)).Ctot;
    err_est = (oversaturationfactor-1)*IP(ii(i)).scr(kk(i)).Qsatpixels/Qtot*100;
    fprintf('%10s\t%5i\t%5.1f\t%9.2e\t%9.2e\t%6.0f\n', titls{ii(i)}, kk(i),...
        Qtot, Ctot, Qtot/Ctot, err_est); 
end
fprintf('%10s\t%5i\t%5.1f\t%9.2e\t%9.2e\t%6.0f\n', titls{2}, 1,...
        totIP_Q, totlanex_C, calfact, 0); 

% And a latex formatted one if we so please
if 0
    fprintf('\n%10s & %5s & %5s & %9s & %9s & %6s\n', 'Date', 'Scr',...
        'Q/pC', 'C_tot', 'Cal', 'Error');
    for i=1:6
        Qtot = IP(ii(i)).scr(kk(i)).Qtot;
        Ctot = lanex(ii(i)).scr(kk(i)).Ctot;
        fprintf('%10s & %5i & %5.1f & %9.2e & %9.2e & %6.0f\n', ...
            titls{ii(i)}, kk(i), Qtot, Ctot, Qtot/Ctot, 0.05);
    end
end
   