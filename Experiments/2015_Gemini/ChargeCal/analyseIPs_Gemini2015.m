% This function return a structure containing useful things about charge
% calibration image plate shots.

function IP = analyseIPs_Gemini2015(plotsave)

Constants;
%% First, let's load the data
% Imageplates first
if ispc
    IP(1).IP_QE = ReadRAW16bit('E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\2015ChargeCalibration\20150828\Cali_1_1\Cali_1_1.img', 2750, 500)';
    IP(2).IP_QE = ReadRAW16bit('E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\2015ChargeCalibration\20150904_chargecal\20150904_cc\20150904_cc.img', 1250, 3250);
    IP(3).IP_QE = ReadRAW16bit('E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\2015ChargeCalibration\20150926_cc_1\20150926_cc_1.img', 2750, 1250)';
    lanexfol = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\Data';
elseif ismac
    %IP(1).IP_QE = ReadRAW16bit('/Users/kristjanpoder/Experiments/2015_Gemini/2015ChargeCalibration/26082015_Gemini/26082015_Gemini.img', 2125, 1375)';
    IP(1).IP_QE = ReadRAW16bit('/Volumes/Gemini2015/Data/ChargeCalibration/Cali_1_1/Cali_1_1.img',...
        2750, 500)';
    IP(2).IP_QE = ReadRAW16bit('/Volumes/Gemini2015/2015ChargeCalibration/20150904_chargecal/20150904_cc/20150904_cc.img',...
        1250, 3250);
    IP(3).IP_QE = ReadRAW16bit('/Volumes/Gemini2015/2015ChargeCalibration/20150926_cc_1/20150916_cc_1.img',...
        2750, 1250)';
    lanexfol = '/Volumes/Gemini2015/Data/';
end
% lanex(1).scr(1).raw = double(imread(fullfile(lanexfol, '20150828',...
%     '20150828r002', '20150828r002s001_Espec1.tif')));
% lanex(1).scr(2).raw = ReadRAW16bit(fullfile(lanexfol, '20150828',...
%     '20150828r002', '20150828r002s001_Espec2LE.raw'), 1280, 960)';
% lanex(2).scr(1).raw = double(imread(fullfile(lanexfol, '20150904',...
%     '20150904r001', '20150904r001s005_Espec1.tif')));
% lanex(2).scr(2).raw = flipud(ReadRAW16bit(fullfile(lanexfol, '20150904',...
%     '20150904r001', '20150904r001s005_Espec2LE.raw'), 1280, 960));
% lanex(3).scr(1).raw = double(imread(fullfile(lanexfol, '20150926',...
%     '20150926r006', '20150926r006s006_Espec1.tif')));
% lanex(3).scr(2).raw = double(imread(fullfile(lanexfol, '20150926',...
%     '20150926r006', '20150926r006s006_Espec2LE.tif')))';

titls = {'20150828', '20150904', '20150926'};
savfol = fullfile(getDropboxPath, 'Writing', 'ChargeCal_KP_2015');
load('owncolourmaps');

PSL = @(im,R,S,G,L) (R/100)^2*(4000/S)*10.^(L*(im./G-0.5));
R = 100; S = 5000; G=2^16-1; L = 5;

for i=1:3
    IP(i).IPraw = PSL(IP(i).IP_QE, R, S, G, L);
end

%plotsave = true;

%% Now, let's plot the RAW IP images if we want - in QEs
figure(201); clf;
set(201, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(3,1, [0.1 0.05 0.05 0.05], 0.05, 'across');
axs = zeros(1,3);
for i=1:3
    axs(i) = axes('Parent', 201, 'Position', locs(:,i));
    imagesc(IP(i).IP_QE);
    title(axs(i), titls{i});
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^16]);
setAllFonts(201,26); make_latex(201);
colormap(201, parula);
if plotsave; export_fig(fullfile(savfol, 'IP_rawsQL'), '-pdf', '-nocrop', 201); end

%% Now, let's plot the RAW IP images if we want - PSLs here
figure(201); clf;
set(201, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(3,1, [0.1 0.05 0.05 0.05], 0.05, 'across');
axs = zeros(1,3);
for i=1:3
    axs(i) = axes('Parent', 201, 'Position', locs(:,i));
    imagesc(IP(i).IPraw);
    title(axs(i), titls{i});
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^8]);
setAllFonts(201,20); make_latex(201);
colormap(201, parula);
if plotsave; export_fig(fullfile(savfol, 'IP_rawsPSL'), '-pdf', '-nocrop', 201); end

%% Let's get to business: crop out regions of IP in all images
% Define the regions
%IP(1).scr(1).roi = [390 25 960 1820];
%IP(1).scr(2).roi = [22 10 325 690];
IP(1).scr(1).roi = [25 38 320 2460];
IP(1).scr(2).roi = [25 38 320 2460];
IP(2).scr(1).roi = [605 77 480 2050];
IP(2).scr(2).roi = [10 1520 475 1000];
IP(3).scr(1).roi = [520 30 460 2330];
IP(3).scr(2).roi = [10 10 480 1615];
% And create subimages of these
for i=1:3
    for k=1:2
        rr = IP(i).scr(k).roi;
        IP(i).scr(k).dat = IP(i).IPraw(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3));
    end
end
% And try to plot as many as we have.
figure(201); clf;
set(201, 'Color', 'w', 'Position', [100 100 1000 700]);
colormap(201, parula);
locs = GetFigureArray(3,1, [0.1 0.05 0.05 0.05], 0.05, 'across');
axs = zeros(1,3);
for i=1:3
    axs(i) = axes('Parent', 201, 'Position', locs(:,i));
    imagesc(IP(i).IP_QE);
    title(axs(i), titls{i});
    rectangle('Position', IP(i).scr(1).roi, 'EdgeColor', 'm', 'LineWidth', 2);
    rectangle('Position', IP(i).scr(2).roi, 'EdgeColor', 'm', 'LineWidth', 2);
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^16]);
setAllFonts(201,20); make_latex(201);
if plotsave; export_fig(fullfile(savfol, 'IP_rois_QL'), '-pdf', '-nocrop', 201); end

%% Now define the signal and background regions on all IP images
% Define signals; assume everything else is background to fit surface over
%IP(1).scr(1).sigroi = [535 265 370 510];
%IP(1).scr(2).sigroi = [160 75 94 78];
IP(1).scr(1).sigroi = [80 85 135 1660];
IP(1).scr(2).sigroi = [80 85 135 1660];
IP(2).scr(1).sigroi = [255 60 85 1875];
IP(2).scr(2).sigroi = [190 91 200 910];
IP(3).scr(1).sigroi = [85 45 260 2240];
IP(3).scr(2).sigroi = [160 1 308 1575];
% And plot them again
figure(202); clf;
set(202, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.05 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 202, 'Position', locs(:,i));
    dat = IP(ii(i)).scr(kk(i)).dat; 
    dat(dat==PSL(2^16-1,R,S,G,L)) = nan; 
    imagesc(dat);
    title(axs(i), [titls{ii(i)} ' IP ' num2str(kk(i))]);
    rectangle('Position', IP(ii(i)).scr(kk(i)).sigroi, 'EdgeColor', 'k', 'LineWidth', 2);
end
colormap(colmap.jet_white);
%set(axs, 'CLim', [0 2^8]);
setAllFonts(202,20); make_latex(202);
if plotsave; export_fig(fullfile(savfol, 'IP_signalrois'), '-pdf', '-nocrop', 202); end

%% Now do the background subtraction: fit a surface to everything apart from signal

method = [2 2 2; 2 2 2];
for i=1:3
    for k=1:2
        fprintf('Fitting IP %i, screen %i\n', i, k);
        dat = IP(i).scr(k).dat;
        rr = IP(i).scr(k).sigroi;
        IP(i).scr(k).bg = createBackground(dat, rr, [], [], method(k,i));
    end
end

%% And plot the backgrounds, so we can estimate how good they are
figure(203); clf;
set(203, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.05 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 203, 'Position', locs(:,i));
    imagesc(IP(ii(i)).scr(kk(i)).bg);
    title(axs(i), [titls{ii(i)} ' IP ' num2str(kk(i))]);
end
%set(axs, 'CLim', [0 2^16]);
setAllFonts(203,20); make_latex(203);
if plotsave; export_fig(fullfile(savfol, 'IP_signalBGs'), '-pdf', '-nocrop', 203); end

%% Plot the IPs with the background subtracted

figure(203); clf;
set(203, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.11 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 203, 'Position', locs(:,i));
    dat = IP(ii(i)).scr(kk(i)).dat-IP(ii(i)).scr(kk(i)).bg;
    rr = IP(ii(i)).scr(kk(i)).sigroi; 
    fprintf('IP %i, scr %i: total signal counts = %2.2e ; Total IP Q = %2.2f pC\n',...
        ii(i), kk(i), sum(dat(:)), sum(IP(ii(i)).scr(kk(i)).dat(:))*6900/20*qe*1e12);
    totQ = sum(dat(:))*6900/20*qe*1e12;
    IP(ii(i)).scr(kk(i)).Qtot = totQ;
    ttemp = dat;
    ttemp(IP(ii(i)).scr(kk(i)).dat==PSL(2^16-1,R,S,G,L)) = nan;
    IP(ii(i)).scr(kk(i)).Nsatpixels = sum(isnan(ttemp(:)));
    IP(ii(i)).scr(kk(i)).Qsatpixels = sum(sum(dat(isnan(ttemp))))*6900/20*qe*1e12;
    imagesc(dat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3)));
    title(axs(i), {[titls{ii(i)} ' IP ' num2str(kk(i))] sprintf('$Q_{tot}=%2.0f pC$', totQ)});
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^8]); 
colormap(colmap.jet_white);
cb = colorbar('Peer', axs(1), 'Location', 'North');
set(cb, 'Position', [0.05 0.08 0.9 0.02])
xlabel(cb, 'Charge / PSL')
setAllFonts(203,20); make_latex(203);
export_fig(fullfile(savfol, 'IP_signalrois'), '-pdf', '-nocrop', 203);

if ~plotsave
    close(201);
    close(202);
    close(203);
end
