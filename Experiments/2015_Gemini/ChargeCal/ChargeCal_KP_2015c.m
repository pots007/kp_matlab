% Do the charge calibrations for different days. Load all the IPs, then do
% fancy-pancy background fitting and find total charge in the beam. Then,
% so same for Lanex images for those shots and produce a number for
% calibration. Look over saturation issues for now; just give a very rough
% estimate as to how much of a lower estimate our number would be.

% -----   THIS VERSION USES THE WARP REFERENCES AS SET UP IN MagnetTracker

% Start off with analysing the image plates.
IP = analyseIPs_Gemini2015(false);

lanex(1).scr(1).raw = double(imread(fullfile(lanexfol, '20150828',...
    '20150828r002', '20150828r002s001_Espec1.tif')));
lanex(1).scr(2).raw = ReadRAW16bit(fullfile(lanexfol, '20150828',...
    '20150828r002', '20150828r002s001_Espec2LE.raw'), 1280, 960)';
lanex(2).scr(1).raw = double(imread(fullfile(lanexfol, '20150904',...
    '20150904r001', '20150904r001s005_Espec1.tif')));
lanex(2).scr(2).raw = flipud(ReadRAW16bit(fullfile(lanexfol, '20150904',...
    '20150904r001', '20150904r001s005_Espec2LE.raw'), 1280, 960));
lanex(3).scr(1).raw = double(imread(fullfile(lanexfol, '20150926',...
    '20150926r006', '20150926r006s006_Espec1.tif')));
lanex(3).scr(2).raw = double(imread(fullfile(lanexfol, '20150926',...
    '20150926r006', '20150926r006s006_Espec2LE.tif')))';

saving = false;
%% Let's do it properly now, for images where we warp the screen back

% ----------   20150904, screen 1

% For CC for pre 0902r003, can use the same warp calibration. This will be
% done below.

warpdat = load('/Users/kristjanpoder/Experiments/2015_Gemini/EspecRefs/Screen1_20150902r002s030CalibData');

ss = warpdat.datt.warpdat.cropsize;
cropim = lanex(2).scr(1).raw(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3));
rotim = imrotate(cropim, warpdat.datt.warpdat.thet);
warpim = imwarp(rotim, warpdat.datt.warpdat.q.tform, 'cubic');
warpim = warpim./warpdat.datt.warpdat.q.J2;
logmx = warpdat.datt.warpdat.q.x>0 & warpdat.datt.warpdat.q.x<warpdat.datt.warpdat.q.screenx(3);
logmy = warpdat.datt.warpdat.q.y>0 & warpdat.datt.warpdat.q.y<warpdat.datt.warpdat.q.screeny(3);
lanexim = warpim(logmy,logmx);

rr = [440 205 1225 65]; 
lanexBG = createBackground(lanexim, rr, [], [], 2);
lanexdat = lanexim - lanexBG; 
lanexdat = lanexdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3));
lanexoffset = rr(1);
% Mask out noise creation noise.
lanexdat(lanexdat<0.01*max(lanexdat(:))) = 0;

%% Now plot and analyse

% Middle of first peak on IP is 687, on lanex is 466.
% Index of half-full-height of last peaks on IP is 1829, on lanex is 1210.
% Index of middle peak on IP is 1478, lanex is 977.
cal(1).IProi = [1 30 687 45]; cal(1).lanexroi = [1 20 456 35]; cal(1).col = 'g';
cal(1).IProi = [1 1 687 84]; cal(1).lanexroi = [1 1 456 64]; cal(1).col = 'g';
cal(2).IProi = [687 30 380 20]; cal(2).lanexroi = [ 456 19 248 25]; cal(2).col = 'm';
cal(2).IProi = [687 1 380 85]; cal(2).lanexroi = [456 1 238 65]; cal(2).col = 'm';
cal(3).IProi = [1 1 1875 85]; cal(3).lanexroi = [1 1 1235 65]; cal(3).col = 'b';
% This is at peak1+0.76*(endHM-peak1), width = (endHM-peak2)*0.37
cal(3).IProi = [1555 1 101 85]; cal(3).lanexroi = [1031 1 86 65]; cal(3).col = 'b';
cal(4).IProi = [3 2 1862 84]; cal(4).lanexroi = [3 2 1222 64]; cal(4).col = 'k';

figure(207); clf(207);
set(207, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(1, 3, [0.02 0.18 0.02 0.05], 0.05, 'across');
ax1 = axes('Parent', 207, 'Position', locs(:,1));
IPdat = IP(2).scr(1).dat-IP(2).scr(1).bg;
IPdat(IPdat<0.01*(max(IPdat(:)))) = 0;
rr = IP(2).scr(1).sigroi - [0 0 0 10]; 
IPdat = IPdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3))';
imagesc(IPdat, 'Parent', ax1);
cb = colorbar('Peer', ax1, 'Location', 'West');
set(cb, 'Position', [0.83 locs(2,1) 0.02 locs(4,1)]);
ylabel(cb, {'Charge' '/ PSL'});
ylabel(ax1, 'Image plate');

ax2 = axes('Parent', 207, 'Position', locs(:,2));
imagesc(lanexdat, 'Parent', ax2);
cb = colorbar('Peer', ax2, 'Location', 'West');
set(cb, 'Position', [0.83 locs(2,2) 0.02 locs(4,2)]);
ylabel(cb, {'Charge' '/ Counts'});
ylabel(ax2, 'Espec1, warped');

colormap(207, colmap.jet_white);
set(ax2, 'CLim', [min(lanexdat(:)) 4000])

for i=1:4
    IProi = cal(i).IProi;
    lanexroi = cal(i).lanexroi;
    rectangle('Parent', ax1, 'Position', cal(i).IProi, 'EdgeColor', cal(i).col, 'LineWidth', 2);
    rectangle('Parent', ax2, 'Position', cal(i).lanexroi, 'EdgeColor', cal(i).col, 'LineWidth', 2);
    totIP_Q = sum(sum(IPdat(IProi(2):IProi(2)+IProi(4),...
        IProi(1):IProi(1)+IProi(3))))*6900/20*1e12*qe;
    totlanex_C = sum(sum(lanexdat(lanexroi(2):lanexroi(2)+lanexroi(4),...
        lanexroi(1):lanexroi(1)+lanexroi(3))));
    cal(i).calfact = totIP_Q/totlanex_C;
    text(550-(550*(2-i)), 95, sprintf('%2.2e pC/ct', cal(i).calfact), 'Parent', ax1,...
        'Color', cal(i).col, 'FontSize', 20);
end

totIP_Q = sum(IPdat(:))*6900/20*1e12*qe;
totlanex_C = sum(sum(lanexdat(lanexroi(2):lanexroi(2)+lanexroi(4),...
        lanexroi(1):lanexroi(1)+lanexroi(3))));
calfact = totIP_Q/totlanex_C;

ax3 = axes('Parent', 207, 'Position', locs(:,3));
IP_lineout = sum(IPdat, 1)*6900/20*1e12*qe;
lanex_lineout = sum(lanexdat,1);

lfac = length(IP_lineout)/length(lanex_lineout);
hfac = max(IP_lineout)/max(lanex_lineout);
hfac = 0.045*6900/20*1e12*qe;

hh = plot(ax3, 1:length(IP_lineout), smooth(IP_lineout), '-k',...
    lfac*(1:length(lanex_lineout)), hfac*smooth(lanex_lineout), '--r');
axis(ax3, 'tight');
set(ax3, 'YLim', [0 0.4]);

set([ax1 ax2 ax3], 'XTick', [], 'YTick', []);

ax4 = axes('Parent', 207, 'Position', locs(:,3), 'Box', 'off', 'Color', 'none',...
    'YAxisLocation', 'right');

% Need to interpolate the IP FWB image onto the lanex grid.
IP_lineout_interp = interp1(1:length(IP_lineout), IP_lineout, linspace(1, length(IP_lineout), length(lanex_lineout)));
% Need to ensure integral stays the same!!!!!!
IP_lineout_interp = IP_lineout_interp*sum(IP_lineout)/sum(IP_lineout_interp);
calibdata = smooth(IP_lineout_interp)./smooth(lanex_lineout);
% Let's crop out beginning and end parts of the calibration trace - these
% have poor signal and hence will ruin the fit!
logm = [25:450 500:925];% 1025:1175];
calibdatafit = polyfit(logm+lanexoffset, calibdata(logm)', 1)
hold on;
hh2 = plot(ax4, 1:length(lanex_lineout), calibdata, '-b');
hh3 = plot(ax4, logm, calibdata(logm)', '.b', 'MarkerSize', 13);
hh4 = plot(ax4, [1 length(lanex_lineout)], calfact*[1 1], '--k');
hh5 = plot(ax4, 1:length(lanex_lineout), polyval(calibdatafit, (1:length(lanex_lineout))+lanexoffset), '-b');
hold off;

% Legends and pretty things
legend(hh, {'IP, FWB  ', 'Lanex, FWB'}, 'Location', 'South', 'EdgeColor', 'w', 'Orientation', 'horizontal');
hleg2 = legend(hh2, 'Calibration', 'Location', 'North', 'EdgeColor', 'w');
set(hleg2, 'Box', 'off')
set([ax1 ax2 ax3], 'XTick', [], 'YTick', []);
ylims = [1.e-6 5.5e-6];
set(ax4, 'XTick', [], 'YLim', ylims, 'XLim', [1 length(lanex_lineout)]);%, ...
   % 'YTick', ylims, 'YTickLabel', num2str(ylims', '%1.1e'));
set([hh; hh3; hh4; hh5], 'LineWidth', 2);
ylabel(ax4, {'Calibration' 'pC/count'}); 

setAllFonts(207,30); make_latex(207);
if saving; 
    export_fig(fullfile(savfol, '20150904_Screen1_calib'), '-pdf', '-nocrop', 207); 
    datt = warpdat.datt;
    datt.Qfit = calibdatafit;
    save('/Users/kristjanpoder/Experiments/2015_Gemini/EspecRefs/Screen1_20150902r002s030CalibData_fit', ...
        'datt');
end

%% Let's do it properly now, for images where we warp the screen back

% ----------   20150904, screen 2  -------------------

% For CC for pre 0902r003, can use the same warp calibration. This will be
% done below.

warpdat = load('/Users/kristjanpoder/Experiments/2015_Gemini/EspecRefs/Screen3_20150819_espec2lerefCalibData');

ss = warpdat.datt.warpdat.cropsize;
cropim = lanex(2).scr(2).raw(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3));
rotim = imrotate(cropim, warpdat.datt.warpdat.thet);
warpim = imwarp(rotim, warpdat.datt.warpdat.q.tform, 'cubic');
warpim = warpim./warpdat.datt.warpdat.q.J2;
logmx = warpdat.datt.warpdat.q.x>0 & warpdat.datt.warpdat.q.x<warpdat.datt.warpdat.q.screenx(3);
logmy = warpdat.datt.warpdat.q.y>0 & warpdat.datt.warpdat.q.y<warpdat.datt.warpdat.q.screeny(3);
lanexim = warpim(logmy,logmx);

rr = [1 200 575 150]; 
lanexBG = createBackground(lanexim, rr, [], [], 1);
lanexdat = lanexim - lanexBG; 
lanexdat = lanexdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3));
lanexoffset = rr(1);

%% And now plot and fit linear calibration

lanexroi = [1 1 575 150];

% Cut out negative numbers created with BG subtraction. This is legit if
% done later on as well, the important thing is to get the BG off the
% signal region.
lanexdat(lanexdat<0.01*max(lanexdat(:))) = 0;

cal(1).IProi = [2 2 453 199]; cal(1).lanexroi = [2 2 286 149]; cal(1).col = 'g';
%cal(1).IProi = [1 1 687 84]; cal(1).lanexroi = [1 1 466 64]; cal(1).col = 'g';
cal(2).IProi = [456 2 455 199]; cal(2).lanexroi = [289 2 286 149]; cal(2).col = 'm';
%cal(2).IProi = [687 1 380 85]; cal(2).lanexroi = [466 1 248 65]; cal(2).col = 'm';
cal(3).IProi = [1 1 910 200]; cal(3).lanexroi = [1 1 575 150]; cal(3).col = 'k';

figure(206); clf(206);
set(206, 'Color', 'w', 'Position', [1200 100 1000 700]);
locs = GetFigureArray(1, 3, [0.02 0.15 0.02 0.05], 0.05, 'across');
ax1 = axes('Parent', 206, 'Position', locs(:,1));
IPdat = IP(2).scr(2).dat-IP(2).scr(2).bg;
rr = IP(2).scr(2).sigroi; 
IPdat = IPdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3))';
imagesc(fliplr(IPdat), 'Parent', ax1);
%rectangle('Parent', ax1, 'Position', IProi, 'EdgeColor', 'k', 'LineWidth', 2);
cb = colorbar('Peer', ax1, 'Location', 'West');
set(cb, 'Position', [0.86 locs(2,1) 0.02 locs(4,1)]);
ylabel(cb, {'Charge'  '/ PSL'});
ylabel(ax1, 'IP');

ax2 = axes('Parent', 206, 'Position', locs(:,2));
imagesc(lanexdat, 'Parent', ax2);
rectangle('Parent', ax2, 'Position', lanexroi, 'EdgeColor', 'k', 'LineWidth', 2);
cb = colorbar('Peer', ax2, 'Location', 'West');
set(cb, 'Position', [0.86 locs(2,2) 0.02 locs(4,2)]);
ylabel(cb, {'Charge ' '/ Counts'});
ylabel(ax2, 'Espec2LE, warped');

colormap(206, colmap.jet_white);

set([ax1 ax2], 'XTick', [], 'YTick', []);
%set(ax2, 'CLim', [min(lanexdat(:)) 4000])

for i=1:3
    IProi = cal(i).IProi;
    lanexroi = cal(i).lanexroi;
    rectangle('Parent', ax1, 'Position', cal(i).IProi, 'EdgeColor', cal(i).col, 'LineWidth', 2);
    rectangle('Parent', ax2, 'Position', cal(i).lanexroi, 'EdgeColor', cal(i).col, 'LineWidth', 2);
    totIP_Q = sum(sum(IPdat(IProi(2):IProi(2)+IProi(4),...
        IProi(1):IProi(1)+IProi(3))))*6900/20*1e12*qe;
    totlanex_C = sum(sum(lanexdat(lanexroi(2):lanexroi(2)+lanexroi(4),...
        lanexroi(1):lanexroi(1)+lanexroi(3))));
    cal(i).calfact = totIP_Q/totlanex_C;
    text(300-(300*(2-i)), 222, sprintf('%2.2e pC/ct', cal(i).calfact), 'Parent', ax1,...
        'Color', cal(i).col, 'FontSize', 20);
end

totIP_Q = sum(IPdat(:))*6900/20*1e12*qe;
totlanex_C = sum(sum(lanexdat(lanexroi(2):lanexroi(2)+lanexroi(4),...
        lanexroi(1):lanexroi(1)+lanexroi(3))));
calfact = totIP_Q/totlanex_C;

% Now plot FWB of both
ax3 = axes('Parent', 206, 'Position', locs(:,3));

IP_lineout = sum(fliplr(IPdat), 1)*6900/20*1e12*qe;
lanex_lineout = sum(lanexdat,1);

lfac = length(IP_lineout)/length(lanex_lineout);
hfac = max(IP_lineout)/max(lanex_lineout);
hfac = 0.002*6900/20*1e12*qe;

hh = plot(ax3, 1:length(IP_lineout), smooth(IP_lineout), '-k',...
    lfac*(1:length(lanex_lineout)), hfac*smooth(lanex_lineout), '--r');
axis(ax3, 'tight');

ax4 = axes('Parent', 206, 'Position', locs(:,3), 'Box', 'off', 'Color', 'none',...
    'YAxisLocation', 'right');

% Need to interpolate the IP FWB image onto the lanex grid.
IP_lineout_interp = interp1(1:length(IP_lineout), IP_lineout, linspace(1, length(IP_lineout), length(lanex_lineout)));
% Need to ensure integral stays the same!!!!!!
IP_lineout_interp = IP_lineout_interp*sum(IP_lineout)/sum(IP_lineout_interp);
calibdata = smooth(IP_lineout_interp)./smooth(lanex_lineout);
% Let's crop out beginning and end parts of the calibration trace - these
% have poor signal and hence will ruin the fit!
logm = 50:500;
calibdatafit = polyfit(logm+lanexoffset, calibdata(logm)', 1)
hold on;
plot(ax4, 1:length(lanex_lineout), calibdata, '-b');
hh3 = plot(ax4, logm, calibdata(logm)', '-b');
hh4 = plot(ax4, [1 length(lanex_lineout)], calfact*[1 1], '--k');
hh5 = plot(ax4, 1:length(lanex_lineout), polyval(calibdatafit, (1:length(lanex_lineout))+lanexoffset), '.-b');
hold off;

% Legends and pretty things
hleg1 = legend(hh, {'IP, FWB', 'Lanex, FWB'}, 'Location', 'North', 'EdgeColor', 'w');
hleg2 = legend(hh3, 'Calibration', 'Location', 'South', 'EdgeColor', 'w');
set(hleg2, 'Box', 'off')
set([ax1 ax2 ax3], 'XTick', [], 'YTick', []);
ylims = [1.e-7 2.5e-7];
set(ax4, 'XTick', [], 'YLim', ylims, 'XLim', [1 length(lanex_lineout)]);%, ...
   % 'YTick', ylims, 'YTickLabel', num2str(ylims', '%1.1e'));
set([hh; hh3; hh4; hh5], 'LineWidth', 2);
ylabel(ax4, {'Calibration' 'pC/count'}); 


setAllFonts(206,30); make_latex(206);
if saving; 
    export_fig(fullfile(savfol, '20150904_Screen2_calib'), '-pdf', '-nocrop', 206); 
    datt = warpdat.datt;
    datt.Qfit = calibdatafit;
    save('/Users/kristjanpoder/Experiments/2015_Gemini/EspecRefs/Screen3_20150819_espec2lerefCalibData_fit', 'datt');
end;

%% Now try to plot IPs and maybe Lanex on the same graph to get spectra!