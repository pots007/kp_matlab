% Monsterscript to do montages of all length scans in one file!!!!!

load('Gem2015LengthScans');
if ismac
    datfol = '/Volumes/Gemini2015/Data/';
    savfol = '/Users/kristjanpoder/Google Drive/Experiment/2015Gemini/EspecAnalysis/';
elseif ispc
    savfol = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\EspecAnalysis\';
end
load('tracking_onAxis_1mrad_beam.mat');
ff = load(fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecRefs', 'Screen1fit_20150902r002s030CalibData'));

hfig = figure(95); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 1600 700]);
colormap(hfig, jet_white(256));
enlines = [300 500 900 1200 1500 2000];
%%
for ll=1:length(LengthScans)
    clf(hfig);
    % Prepare the length scan axes: find out unique lengths
    Ls = cell2mat({LengthScans(ll).cellL});
    L_unique = unique(Ls); L_numel = zeros(size(L_unique));
    L_inds = cell(size(L_unique));
    for i=1:length(L_unique);
        L_numel(i) = sum(Ls==L_unique(i));
        allshots = 1:length(Ls);
        L_inds{i} = allshots(Ls==L_unique(i));
    end
    locs = GetFigureArray(length(L_unique), 1, [0.02 0.01 0.14 0.1], 0.005, 'down');
    runfol = fullfile(datfol, num2str(LengthScans(ll).date), sprintf('%ir%03i', LengthScans(ll).date, LengthScans(ll).run));
    %flist = dir(fullfile(runfol, '*Espec1.tif'));
    axss = [];
    for i=1:length(L_unique)
        axss(i) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add','LineWidth', 3, 'Box', 'on');
        ttemp = [];
        for k=1:length(L_inds{i})
            shotno = L_inds{i}(k);
            fprintf('Working on scan no %i,  length %1.1f (%i out of %2i)\n', ll, L_unique(i), k, length(L_inds{i}));
            % Now need to open the image and warp and rotate and whatnot
            fnam = fullfile(runfol, sprintf('%ir%03is%03i_Espec1.tif', LengthScans(ll).date,LengthScans(ll).run, shotno));
            if ~exist(fnam, 'file'); continue; end;
            imraw = double(imread(fnam));
            ss = ff.datt.warpdat.cropsize;
            cropim = imraw(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3));
            rotim = imrotate(cropim, ff.datt.warpdat.thet);
            warpim = imwarp(rotim, ff.datt.warpdat.q.tform, 'cubic');
            warpim = warpim./ff.datt.warpdat.q.J2;
            logmx = ff.datt.warpdat.q.x>0 & ff.datt.warpdat.q.x<ff.datt.warpdat.q.screenx(3);
            logmy = ff.datt.warpdat.q.y>0 & ff.datt.warpdat.q.y<ff.datt.warpdat.q.screeny(3);
            imdat = warpim(logmy, logmx);
            bg = createBackground(imdat, [1 150 length(pixax)-1, 150],[],[],4);
            imdat = imdat-bg;
            logmy = 150:300;
            ttemp = [ttemp imdat(logmy,:)'];
        end
        imagesc(ttemp, 'Parent', axss(i));
        xlabel(axss(i), num2str(L_unique(i)));
        axis(axss(i), 'tight');
        drawnow;
    end
    pixax = 1:size(imdat,2);
    lengths = polyval(ff.datt.efit, pixax)*10;
    energies = feval(ff.datt.pfit, lengths);
    inds = zeros(size(enlines));
    for i=1:length(inds)
        [~,inds(i)] = min(abs(energies-enlines(i)));
    end
    climm = cell2mat(get(axss, 'CLim'));
    set(axss, 'XTick', [], 'YTickLabel', [], 'YTick', inds, 'CLim', [0 mean(climm(:,2))]);
    set(axss(1), 'YTickLabel', num2str(enlines'));
    ylabel(axss(1), 'Energy / MeV')
    % Draw energy lines
    for i=1:length(axss)
        plot(axss(i), repmat([get(axss(i), 'XLim') nan],length(inds),1)', repmat(inds',1,3)', '--k', 'LineWidth', 2);
    end
    hann = annotation('textbox', 'Position', [0.2 0.0 0.6 0.06], 'HorizontalAlignment', 'left',...
        'Interpreter', 'latex', 'FontSize', 30, 'LineStyle', 'none');
    set(hann, 'String', ['Cell length (mm) for ' num2str(LengthScans(ll).date) ', run ' num2str(LengthScans(ll).run),...
        '; $n_e = ' num2str(LengthScans(ll).pressure/31, '%2.1f') '\times 10^{18}\mathrm{cm}^{-3}$'])
    % And now save the image and whatnot
    make_latex(hfig); setAllFonts(hfig, 20);
    runsavfol = fullfile(savfol, sprintf('%ir%03i', LengthScans(ll).date, LengthScans(ll).run));
    if ~exist(runsavfol, 'dir'); mkdir(runsavfol); end;
    export_fig(fullfile(runsavfol, 'LengthMontage'), '-pdf', '-nocrop', hfig);
    combfilename = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', 'LengthScanMontages', 'AllScans.pdf');
    if i==1
        if exist(combfilename, 'file'); delete(combfilename); end;
        export_fig(combfilename, '-pdf', '-nocrop', hfig);
    else
        export_fig(combfilename, '-pdf', '-nocrop', '-append', hfig);
    end
    
    % And now for the FWB images as well!
    for i=1:length(axss)
        him = findobj(axss(i), 'Type', 'image');
        cdat = get(him, 'CData');
        set(him, 'CData', sum(cdat,2));
        if isempty(cdat); climm(i,2) = 1e4; continue; end
        climm(i,2) = max(sum(cdat,2));
    end
    set(axss, 'XLim', [0.5 1.5], 'CLim', [0 max(climm(:,2))]);
    export_fig(fullfile(runsavfol, 'LengthMontageFWB'), '-pdf', '-nocrop', hfig);
    combfilename = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', 'LengthScanMontages', 'AllScansFWB.pdf');
    if i==1
        if exist(combfilename, 'file'); delete(combfilename); end;
        export_fig(combfilename, '-pdf', '-nocrop', hfig);
    else
        export_fig(combfilename, '-pdf', '-nocrop', '-append', hfig);
    end
end

%% And pop all of them into the same folder for easy viewing as well!
for ll=1:length(LengthScans)
    destfname = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', 'LengthScanMontages',...
        sprintf('%ir%03i_', LengthScans(ll).date, LengthScans(ll).run));
    runsavfol = fullfile(savfol, sprintf('%ir%03i', LengthScans(ll).date, LengthScans(ll).run));
    copyfile(fullfile(runsavfol, 'LengthMontage.pdf'), [destfname 'LengthMontage.pdf']);
    %copyfile(fullfile(runsavfol, 'LengthMontageFWB.pdf'), [destfname 'FWB_LengthMontage.pdf']);
end
