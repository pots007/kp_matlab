dlist = dir;
for i=3:length(dlist)
    cd(dlist(i).name);
    flist = dir;
    for k=1:length(flist)
        if flist(k).isdir; continue; end;
        nname = flist(k).name;
        nname(8) = '6';
        movefile(flist(k).name, nname);
        disp([flist(k).name '    ' nname]);
    end
    cd('..');
end