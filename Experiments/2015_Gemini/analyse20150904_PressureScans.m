% This script plots all results from 20150904 pressure scans on one graph.

% Load the data first
datfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis');
fdat = load(fullfile(datfol, '20150904r006', 'rundata_analysis'));
dat(1) = fdat.D0904r006;
fdat = load(fullfile(datfol, '20150904r007', 'rundata_analysis'));
dat(2) = fdat.D0904r007;
fdat = load(fullfile(datfol, '20150904r008', 'rundata_analysis'));
dat(3) = fdat.D0904r008;
savfol = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', '0904_PressureScans');

% Firstly, Espec2 peak energies
cellL = cell2mat({dat.cellL});
hfig = figure(94); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [100 100 800 600]);
set(gca, 'NextPlot', 'add');
hh = []; colz = 'krb';
for i=1:3
    hh(i,1) = plot(dat(i).nes, dat(i).peakEns2, ['v' colz(i)]);
    hh(i,2) = plot(dat(i).pp_unique, cellfun(@mean, dat(i).pp_vs_ens2), ['-o' colz(i)]);
end
set(hh, 'MarkerSize', 10, 'LineWidth', 1.5);
set(hh(:,2), 'LineWidth', 2.5);
for i=1:3; leglab{i} = sprintf('$L_{cell}=%i\\mathrm{mm}$', cellL(i)); end;
legend(hh(:,2), leglab, 'Location', 'Northeast');
set(gca, 'XLim', [0.9 6]*1e18, 'Box', 'on', 'LineWidth', 2.25);
xlabel('$n_e / \mathrm{cm}^{-3}$');
ylabel('Espec2 peak energy / MeV');
make_latex(hfig); setAllFonts(hfig, 18);
%export_fig(fullfile(savfol, 'Espec2_summary'), '-nocrop', '-pdf', hfig);

%% Then Espec1s
hfig = figure(95); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [100 100 800 600]);
set(gca, 'NextPlot', 'add');
hh = []; colz = 'krb';
for i=1:3
    hh(i,1) = plot(dat(i).nes, dat(i).peakEns1, ['v' colz(i)]);
    hh(i,2) = plot(dat(i).pp_unique, cellfun(@mean, dat(i).pp_vs_ens1), ['-o' colz(i)]);
end
set(hh, 'MarkerSize', 10, 'LineWidth', 1.5);
set(hh(:,2), 'LineWidth', 2.5);
for i=1:3; leglab{i} = sprintf('$L_{cell}=%i\\mathrm{mm}$', cellL(i)); end;
legend(hh(:,2), leglab, 'Location', 'Northeast');
set(gca, 'XLim', [0.9 6]*1e18, 'Box', 'on', 'LineWidth', 2.25);
xlabel('$n_e / \mathrm{cm}^{-3}$');
ylabel('Espec1 peak energy / MeV');
make_latex(hfig); setAllFonts(hfig, 18);
%export_fig(fullfile(savfol, 'Espec1_summary'), '-nocrop', '-pdf', hfig);

%% Now total charge
hfig = figure(95); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [100 100 800 600]);
set(gca, 'NextPlot', 'add');
hh = []; colz = 'krb';
for i=1:3
    hh(i,1) = plot(dat(i).nes, dat(i).Qtots, ['v' colz(i)]);
    hh(i,2) = plot(dat(i).pp_unique, cellfun(@mean, dat(i).pp_vs_Qtots), ['-o' colz(i)]);
end
set(hh, 'MarkerSize', 10, 'LineWidth', 1.5);
set(hh(:,2), 'LineWidth', 2.5);
for i=1:3; leglab{i} = sprintf('$L_{cell}=%i\\mathrm{mm}$', cellL(i)); end;
legend(hh(:,2), leglab, 'Location', 'Northeast');
set(gca, 'XLim', [0.9 6]*1e18, 'Box', 'on', 'LineWidth', 2.25);
xlabel('$n_e / \mathrm{cm}^{-3}$');
ylabel('$Q$ / pC');
make_latex(hfig); setAllFonts(hfig, 18);
%export_fig(fullfile(savfol, 'Qtotal_summary'), '-nocrop', '-pdf', hfig);

%% And finally high charge
hfig = figure(95); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [100 100 800 600]);
set(gca, 'NextPlot', 'add');
hh = []; colz = 'krb';
for i=1:3
    hh(i,1) = plot(dat(i).nes, dat(i).Qhighs, ['v' colz(i)]);
    hh(i,2) = plot(dat(i).pp_unique, cellfun(@mean, dat(i).pp_vs_Qhighs), ['-o' colz(i)]);
end
set(hh, 'MarkerSize', 10, 'LineWidth', 1.5);
set(hh(:,2), 'LineWidth', 2.5);
for i=1:3; leglab{i} = sprintf('$L_{cell}=%i\\mathrm{mm}$', cellL(i)); end;
legend(hh(:,2), leglab, 'Location', 'Northeast');
set(gca, 'XLim', [0.9 6]*1e18, 'Box', 'on', 'LineWidth', 2.25);
xlabel('$n_e / \mathrm{cm}^{-3}$');
ylabel('$Q_{\mathcal{E}>900}$ / pC');
make_latex(hfig); setAllFonts(hfig, 18);
export_fig(fullfile(savfol, 'Qhigh_summary'), '-nocrop', '-pdf', hfig);

%% Try plotting things against areal density: 
% Firstly, Espec2 peak energies
cellL = cell2mat({dat.cellL});
hfig = figure(94); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [100 100 800 600]);
set(gca, 'NextPlot', 'add');
hh = []; colz = 'krb';
for i=1:3
    hh(i,1) = plot((dat(i).nes-1.4e18)*cellL(i)*0.1, dat(i).peakEns2, ['v' colz(i)]);
    hh(i,2) = plot((dat(i).pp_unique-1.4e18)*cellL(i)*0.1, cellfun(@mean, dat(i).pp_vs_ens2), ['-o' colz(i)]);
end
set(hh, 'MarkerSize', 10, 'LineWidth', 1.5);
set(hh(:,2), 'LineWidth', 2.5);
for i=1:3; leglab{i} = sprintf('$L_{cell}=%i\\mathrm{mm}$', cellL(i)); end;
legend(hh(:,2), leglab, 'Location', 'Northeast');
set(gca, 'XLim', [-1. 8]*1e18, 'Box', 'on', 'LineWidth', 2.25);
xlabel('$n_e / \mathrm{cm}^{-2}$');
ylabel('Espec2 peak energy / MeV');
make_latex(hfig); setAllFonts(hfig, 18);
%export_fig(fullfile(savfol, 'Espec2_summary_areal'), '-nocrop', '-pdf', hfig);

%% Try plotting things against areal density: 
% Then espec1
cellL = cell2mat({dat.cellL});
hfig = figure(94); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [100 100 800 600]);
set(gca, 'NextPlot', 'add');
hh = []; colz = 'krb';
for i=1:3
    hh(i,1) = plot((dat(i).nes-1.4e18)*cellL(i)*0.1, dat(i).peakEns1, ['v' colz(i)]);
    hh(i,2) = plot((dat(i).pp_unique-1.4e18)*cellL(i)*0.1, cellfun(@mean, dat(i).pp_vs_ens1), ['-o' colz(i)]);
end
set(hh, 'MarkerSize', 10, 'LineWidth', 1.5);
set(hh(:,2), 'LineWidth', 2.5);
for i=1:3; leglab{i} = sprintf('$L_{cell}=%i\\mathrm{mm}$', cellL(i)); end;
legend(hh(:,2), leglab, 'Location', 'Northeast');
set(gca, 'XLim', [-0.9 8]*1e18, 'Box', 'on', 'LineWidth', 2.25);
xlabel('$n_e / \mathrm{cm}^{-2}$');
ylabel('Espec2 peak energy / MeV');
make_latex(hfig); setAllFonts(hfig, 18);
%export_fig(fullfile(savfol, 'Espec1_summary_areal'), '-nocrop', '-pdf', hfig);
%% And now for some extravaganza: make a tex presentation for all the stuff we've just seen
% Plus the spectra montages

fnames = {'PeakEn_Espec1_vs_ne' 'PeakEn_Espec2_vs_ne' 'Qhigh_vs_ne' 'Qtot_vs_ne'};
datfol = '/Users/kristjanpoder/Google Drive/Experiment/2015Gemini/EspecAnalysis/';
runs = {'20150904r006', '20150904r007', '20150904r008'};
Ls = [10 15 30];
fname = fullfile(savfol, 'PressureScans_20150904.tex');
fid = fopen(fname, 'w');
fprintf(fid, '\\documentclass{beamer}\n');
fprintf(fid, '\\usepackage{graphicx}\n\n');
fprintf(fid, '\\begin{document}\n\n');

% First put in all the graphs for individual runs
for k=1:3
    fprintf(fid, '\\begin{frame}\n');
    fprintf(fid, '\t\\frametitle{Pressure scan %s: $L=%i$ mm}\n', runs{k}, Ls(k));
    for i=1:4
        fprintf(fid, '\t\\includegraphics[width=0.45\\columnwidth]{"%s"}\n', fullfile(datfol, runs{k}, fnames{i}));
        if i==2; fprintf(fid, '\t\\\\\n'); end;
    end
    fprintf(fid, '\\end{frame}\n\n');
end
% And now the collages, saved earlier here
fnames = {'Espec1_summary' 'Espec2_summary' 'Qhigh_summary' 'Qtotal_summary'};
fprintf(fid, '\\begin{frame}\n');
fprintf(fid, '\t\\frametitle{Summary of different lengths}\n');
for i=1:4
    fprintf(fid, '\t\\includegraphics[width=0.45\\columnwidth]{"%s"}\n', fullfile(savfol, fnames{i}));
    if i==2; fprintf(fid, '\t\\\\\n'); end;
end
fprintf(fid, '\\end{frame}\n\n');
% And now the collages, for areal densities
fnames = {'Espec1_summary_areal' 'Espec2_summary_areal' 'Qhigh_summary' 'Qtotal_summary'};
fprintf(fid, '\\begin{frame}\n');
fprintf(fid, '\t\\frametitle{Summary against areal density}\n');
for i=1:2
    fprintf(fid, '\t\\includegraphics[width=0.45\\columnwidth]{"%s"}\n', fullfile(savfol, fnames{i}));
    %if i==2; fprintf(fid, '\t\\\\\n'); end;
end
fprintf(fid, '\\end{frame}\n\n');

% And now for the montages....
for k=1:3
    fprintf(fid, '\\begin{frame}\n');
    fprintf(fid, '\t\\frametitle{Montage for %s}\n', runs{k});
    for i=1:18
        if k>1 && i>16; continue; end;
        fprintf(fid, '\t\\includegraphics[width=0.24\\columnwidth,page=%i]{"%s"}\n',...
            i, fullfile(datfol, runs{k}, 'Spectra_all'));
        if mod(i,4)==0; fprintf(fid, '\t\\\\\n'); end;
        
    end
    fprintf(fid, '\\end{frame}\n\n');
    
end

fprintf(fid, '\\end{document}');
fclose(fid);

system(['/usr/local/texlive/2015/bin/x86_64-darwin/pdflatex -output-directory="' savfol '" "' fname '"'])