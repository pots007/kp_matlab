nphotons = repmat(1:1:500, 2, 1);
QE = repmat([0.5; 0.9], 1, size(nphotons,2));
RN = 2.9;
SNR = QE.*nphotons./sqrt(RN^2+QE.*nphotons);
figure(9909);
hs = plot(nphotons(1,:), SNR);
set(gca, 'YScale', 'lin')
xlabel('Incident photons');
ylabel('Signal-to-Noise Ratio');
legend(hs, {'Front-illuminated', 'Back-illuminated'}, 'Location', 'Northwest');
setAllFonts(9909, 20);
if ismac
    savefol = '/Users/kristjanpoder/Dropbox/2015Gemini/information/';
end
export_fig([savefol 'FrontvsBackIlluminatedCCD'], '-pdf', '-transparent', '-nocrop', 9099);