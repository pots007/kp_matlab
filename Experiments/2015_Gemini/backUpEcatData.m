% backUpEcatData

% Look in folder Data/Ecat, look for GSNs for particular diagnostics
% Then move these into target folder, organising them nicely into
% directories.

diags = {'S_frog_master', 'S_COMP_SPEC', ...
    'S_comp_ff', 'S_comp_nf', 'S_leg1_green_nf',...
    'S_leg2_green_nf', 'S_uncomp_nf'};
if ispc
    infol = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\EcatRAW\';
    %savfol = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\Ecat\';
    savfol = 'Y:\Ecat\';
elseif ismac
    infol = '/Users/kristjanpoder/Experiments/2015_Gemini/EcatRAW/';
    savfol = '/Volumes/Gemini2015/Ecat/';
end

fols = dir(infol);

for i=3:length(fols)
    % find data in all the folders
    if ~fols(i).isdir; continue; end;
    % Now find all the diagnostics
    disp(['Looking into ' fols(i).name]);
    for k=1:length(diags)
        if ~exist([savfol diags{k}], 'dir')
            mkdir([savfol diags{k}]);
        end
        filen = dir([infol fols(i).name '/' diags{k} '*']);
        for l=1:length(filen)
            ind1 = strfind(filen(l).name, 'GS');
            if ~exist([savfol diags{k} '/' filen(l).name(ind1:end)], 'file')
                disp(['Copying ' filen(l).name]);
                ind1 = strfind(filen(l).name, 'GS');
                copyfile([infol fols(i).name '/' filen(l).name],...
                    [savfol diags{k} '/' filen(l).name(ind1:end)], 'f');
            end
            if mod(l,100)==0; disp(num2str(l)); end;
        end
    end
end
