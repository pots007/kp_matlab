% Take the backtracked spectra from 0908r008s034 and make it look even
% nicer!

% Open the figure
datfol = '/Users/kristjanpoder/Experiments/2015_Gemini/EspecAnalysis/20150908r008/niceFigs';
flist = dir(fullfile(datfol, '*.fig'));

%% This is for total beam energy being under the curve
hfig2 = figure(36); clf(hfig2);
set(hfig2, 'Color', 'w', 'Position', [150 150 800 400]);
ax1 = gca; set(ax1, 'NextPlot', 'add', 'Box', 'on');

% Now try to find the lines!
Edata = cell(length(flist), 4);
hh = -ones(1,length(flist));
for i=1:length(flist)
    hfig = open(fullfile(datfol, flist(i).name));
    hEspec1G = findobj(hfig, 'Type', 'line', 'Color', 'm', 'LineStyle', '-');
    Edata{i,1} = get(hEspec1G, 'XData')*1e-3; 
    Edata{i,2} = get(hEspec1G, 'YData');
    dE = diff(Edata{i,1})*1e3; dE = [dE(1) dE];
    hh(i) = plot(ax1, Edata{i,1}, Edata{i,2}.*Edata{i,1}, '-');
    Edata{i,3} = sum(Edata{i,2}.*dE); % Total charge
    Edata{i,4} = sum(Edata{i,2}.*dE.*Edata{i,1}); % Total energy
    close(hfig);
end
%
set(hh(ishandle(hh)), 'LineWidth', 2);
xlabel(ax1, 'Electron energy [GeV]');
ylabel(ax1, '$\mathrm{d}Q / (\mathrm{d}E/E) \: \mathrm{[nC]}$');
set(ax1, 'XLim', [.2 3.75], 'LineWidth', 2); set(ax1, 'YLim', [-0.015 .55]);
for i=1:length(hh)
    leglabels{i} = sprintf('$Q_{tot}=%3.0f$ pC', Edata{i,3})
end
legend(hh, leglabels, 'Location', 'Northeast');
make_latex(hfig2); setAllFonts(hfig2, 20);

%export_fig(fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', 'HighEnergySpectra_dQ_dE_E'), '-pdf', '-nocrop', hfig2)

%% This is total charge being under the curve
hfig2 = figure(36); clf(hfig2);
set(hfig2, 'Color', 'w', 'Position', [150 150 800 400]);
ax1 = gca; set(ax1, 'NextPlot', 'add', 'Box', 'on');

% Now try to find the lines!
Edata = cell(length(flist), 4);
hh = zeros(1,length(flist));
for i=1:length(flist)
    hfig = open(fullfile(datfol, flist(i).name));
    hEspec1G = findobj(hfig, 'Type', 'line', 'Color', 'm', 'LineStyle', '-');
    Edata{i,1} = get(hEspec1G, 'XData')*1e-3; 
    Edata{i,2} = get(hEspec1G, 'YData');
    dE = diff(Edata{i,1})*1e3; dE = [dE(1) dE];
    hh(i) = plot(ax1, Edata{i,1}, Edata{i,2}, '-');
    Edata{i,3} = sum(Edata{i,2}.*dE); % Total charge
    Edata{i,4} = sum(Edata{i,2}.*dE.*Edata{i,1}); % Total energy
    close(hfig);
end
%
set(hh, 'LineWidth', 2);
xlabel(ax1, 'Electron energy [GeV]');
ylabel(ax1, '$\mathrm{d}Q / \mathrm{d}E \: \mathrm{[nC/GeV]}$');
set(ax1, 'XLim', [.2 3.75], 'LineWidth', 2); set(ax1, 'YLim', [-0.015 .55]);
for i=1:length(hh)
    leglabels{i} = sprintf('$Q_{tot}=%3.0f$ pC', Edata{i,3})
end
legend(hh, leglabels, 'Location', 'Northeast');
make_latex(hfig2); setAllFonts(hfig2, 20);

export_fig(fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', 'HighEnergySpectra_dQ_dE'), '-pdf', '-nocrop', hfig2)