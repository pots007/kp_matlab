
if (ismac)
    rootf = '/Volumes/Gemini2015/Data/';
    savfol = '/Users/kristjanpoder/Google Drive/Experiment/2015Gemini';
elseif (ispc)
    rootf = '';
end

diaglist = {'Burney', 'Espec1', 'Espec2', 'Espec2LE', 'Espec0',...
    'GasPressure', 'IonisationSpec', 'Moire1', 'Moire2', 'PrettyPic',...
    'ExitMode', 'ForwardSpec', 'X-ray_Andor', 'HiMagShadow', 'DDX-ray'};
diaglist = sort(diaglist);
runslist = cell(300,1+length(diaglist));
id = 0;
dates = dir(rootf);
for i=3:length(dates)
    if (~dates(i).isdir || strcmp(dates(i).name, 'ChargeCalibration'))
        continue;
    end
    runs = dir([rootf dates(i).name '/' dates(i).name '*']);
    for j=1:length(runs)
        if (~runs(j).isdir)
            continue;
        end
        fprintf('Looking into %s \n', runs(j).name);
        id = id+1;
        runslist{id,1} = runs(j).name; 
        shots = dir([rootf dates(i).name '/' runs(j).name '/*s001*.*']);
        if (~isempty(shots))
            for k=1:length(shots)
                ind = sum(strcmp(diaglist,shots(k).name(18:end-4)).*(1:length(diaglist)));
                if (ind~=0)
                    runslist{id, ind+1} = 1;
                end
            end
        end
    end
end
runslist = runslist(1:id,:);
%% Dump it all into a text file
fid = fopen(fullfile(savfol, 'RunDiagnostics.txt'), 'w');
fprintf(fid, '   ');
for i=1:length(diaglist); fprintf(fid, ', %s', diaglist{i}); end;
fprintf(fid, '\n');
for i=1:size(runslist,1)
    fprintf(fid, '%s', runslist{i,1});
    for k=1:length(diaglist); fprintf(fid, ', %i', runslist{i,k+1}); end;
    fprintf(fid, '\n');
end
fclose(fid);

%% Write it into the Google sheet
if ~exist('google_tokens.mat', 'file')
    error('Get the Google tokens first!)');
else
    load('google_tokens.mat');
    % to refresh the Docs access token (usually expires after 1 hr) you'd call
    aTokenDocs=refreshAccessToken(client_id,client_secret,rTokenDocs);
    
    % to refresh the Spreadsheet access token (usually expires after 1 hr) you'd call
    aTokenSpreadsheet=refreshAccessToken(client_id,client_secret,rTokenSpreadsheet);
end

if isempty(aTokenDocs) || isempty(aTokenSpreadsheet)
    warndlg('Could not obtain authorization tokens from Google.','');
    return;
end
disp('Logged in.');
sheetIwant = -1;
sheetlist = getSpreadsheetList(aTokenSpreadsheet);
if (~isempty(sheetlist))
    for i=1:length(sheetlist)
        if (strcmp('NajmudinGemini2015_DataSheet', sheetlist(i).spreadsheetTitle))
            sheetIwant = i;
        end
    end
end

worksheetlist = getWorksheetList(sheetlist(sheetIwant).spreadsheetKey, aTokenSpreadsheet);
worksheetIwant = -1;
if (~isempty(worksheetlist))
    for i=1:length(worksheetlist)
        if (strcmp('LiveDiagnostics', worksheetlist(i).worksheetTitle))
            worksheetIwant = i;
        end
    end
end

if (sheetIwant<0 || worksheetIwant <0)
    if (sheetIwant<0)
        error('Incorrect worksheet name!');
    else
        error('Incorrect worksheet name!');
    end
end
disp('Found correct worksheet.')

[~,inds] = sort(diaglist);
editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, worksheetlist(worksheetIwant).worksheetKey, ...
        1, 1, 'Run', aTokenSpreadsheet);
for i=1:length(diaglist)
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, worksheetlist(worksheetIwant).worksheetKey, ...
        1, i+1, diaglist{inds(i)}, aTokenSpreadsheet);
end

%%
for i=2:size(runslist,1)
    % Iterate over rows in shotsheet!
    disp(['Looking into row ' num2str(i)]);
    % Write things out. First, the title row
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, worksheetlist(worksheetIwant).worksheetKey, ...
        i, 1, runslist{i,1}, aTokenSpreadsheet);
    for k=2:size(runslist,2)
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, worksheetlist(worksheetIwant).worksheetKey, ...
        i, k, num2str(~isempty(runslist{i,k})), aTokenSpreadsheet);
    end
end