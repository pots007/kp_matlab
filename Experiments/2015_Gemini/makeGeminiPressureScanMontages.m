% Monsterscript to do montages of all pressure scans in one file!!!!!

load('Gem2015PressureScans');
if ismac
    datfol = '/Volumes/Gemini2015/Data/';
    savfol = '/Users/kristjanpoder/Google Drive/Experiment/2015Gemini/EspecAnalysis/';
elseif ispc
    savfol = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\EspecAnalysis\';
end
load('tracking_onAxis_1mrad_beam.mat');
ff = load(fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecRefs', 'Screen1fit_20150902r002s030CalibData'));

hfig = figure(95); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 1600 700]);
colormap(hfig, jet_white(256));
enlines = [300 500 900 1200 1500 2000];
%%
for ll=1:length(PressureScans)
    clf(hfig);
    if ll==8; continue; end; % Poo superbox run, need to fix the pressures!
    % Prepare the density scan axes: find out unique pressures
    pps = cell2mat({PressureScans(ll).pressure});
    pp_unique = unique(pps); pp_numel = zeros(size(pp_unique));
    pp_inds = cell(size(pp_unique));
    for i=1:length(pp_unique);
        pp_numel(i) = sum(pps==pp_unique(i));
        allshots = 1:length(pps);
        pp_inds{i} = allshots(pps==pp_unique(i));
    end
    locs = GetFigureArray(length(pp_unique), 1, [0.02 0.01 0.14 0.1], 0.005, 'down');
    runfol = fullfile(datfol, num2str(PressureScans(ll).date), sprintf('%ir%03i', PressureScans(ll).date, PressureScans(ll).run));
    %flist = dir(fullfile(runfol, '*Espec1.tif'));
    axss = [];
    for i=1:length(pp_unique)
        axss(i) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add','LineWidth', 3, 'Box', 'on');
        ttemp = [];
        for k=1:length(pp_inds{i})
            shotno = pp_inds{i}(k);
            fprintf('Working on scan no %i,  pressure %1.1f (%i out of %2i)\n', ll, pp_unique(i), k, length(pp_inds{i}));
            % Now need to open the image and warp and rotate and whatnot
            fnam = fullfile(runfol, sprintf('%ir%03is%03i_Espec1.tif', PressureScans(ll).date, PressureScans(ll).run, shotno));
            if ~exist(fnam, 'file'); continue; end;
            imraw = double(imread(fnam));
            ss = ff.datt.warpdat.cropsize;
            cropim = imraw(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3));
            rotim = imrotate(cropim, ff.datt.warpdat.thet);
            warpim = imwarp(rotim, ff.datt.warpdat.q.tform, 'cubic');
            warpim = warpim./ff.datt.warpdat.q.J2;
            logmx = ff.datt.warpdat.q.x>0 & ff.datt.warpdat.q.x<ff.datt.warpdat.q.screenx(3);
            logmy = ff.datt.warpdat.q.y>0 & ff.datt.warpdat.q.y<ff.datt.warpdat.q.screeny(3);
            imdat = warpim(logmy, logmx);
            bg = createBackground(imdat, [1 150 length(pixax)-1, 150],[],[],4);
            imdat = imdat-bg;
            logmy = 150:300;
            ttemp = [ttemp imdat(logmy,:)'];
        end
        imagesc(ttemp, 'Parent', axss(i));
        xlabel(axss(i), num2str(pp_unique(i)/31, '%2.1f'));
        axis(axss(i), 'tight');
        drawnow;
    end
    pixax = 1:size(imdat,2);
    lengths = polyval(ff.datt.efit, pixax)*10;
    energies = feval(ff.datt.pfit, lengths);
    inds = zeros(size(enlines));
    for i=1:length(inds)
        [~,inds(i)] = min(abs(energies-enlines(i)));
    end
    climm = cell2mat(get(axss, 'CLim'));
    set(axss, 'XTick', [], 'YTickLabel', [], 'YTick', inds, 'CLim', [0 mean(climm(:,2))]);
    set(axss(1), 'YTickLabel', num2str(enlines'));
    ylabel(axss(1), 'Energy / MeV')
    % Draw energy lines
    for i=1:length(axss)
        plot(axss(i), repmat([get(axss(i), 'XLim') nan],length(inds),1)', repmat(inds',1,3)', '--k', 'LineWidth', 2);
    end
    hann = annotation('textbox', 'Position', [0.2 0.0 0.6 0.06], 'HorizontalAlignment', 'left',...
        'Interpreter', 'latex', 'FontSize', 30, 'LineStyle', 'none');
    set(hann, 'String', ['$n_e / \times 10^{18}\mathrm{cm}^{-3}$; for cell length ' num2str(PressureScans(ll).cellL) ', '...
        num2str(PressureScans(ll).date) ', run ' num2str(PressureScans(ll).run)])
    % And now save the image and whatnot
    make_latex(hfig); setAllFonts(hfig, 20);
    runsavfol = fullfile(savfol, sprintf('%ir%03i', PressureScans(ll).date, PressureScans(ll).run));
    if ~exist(runsavfol, 'dir'); mkdir(runsavfol); end;
    export_fig(fullfile(runsavfol, 'PressureMontage'), '-pdf', '-nocrop', hfig);
    combfilename = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', 'PressureScanMontages', 'AllScans.pdf');
    if i==1
        if exist(combfilename, 'file'); delete(combfilename); end;
        export_fig(combfilename, '-pdf', '-nocrop', hfig);
    else
        export_fig(combfilename, '-pdf', '-nocrop', '-append', hfig);
    end
    
    % And now for the FWB images as well!
    for i=1:length(axss)
        him = findobj(axss(i), 'Type', 'image');
        cdat = get(him, 'CData');
        set(him, 'CData', mean(cdat,2));
        if isempty(cdat); climm(i,2) = 1e3; continue; end
        climm(i,2) = max(mean(cdat,2));
    end
    set(axss, 'XLim', [0.5 1.5], 'CLim', [0 max(climm(:,2))]);
    export_fig(fullfile(runsavfol, 'PressureMontageFWB'), '-pdf', '-nocrop', hfig);
    combfilename = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', 'PressureScanMontages', 'AllScansFWB.pdf');
    if i==1
        if exist(combfilename, 'file'); delete(combfilename); end;
        export_fig(combfilename, '-pdf', '-nocrop', hfig);
    else
        export_fig(combfilename, '-pdf', '-nocrop', '-append', hfig);
    end
end

%% And pop all of them into the same folder for easy viewing as well!
for ll=1:length(PressureScans)
    destfname = fullfile(getGDrive, 'Experiment', '2015Gemini', 'EspecAnalysis', 'PressureScanMontages',...
        sprintf('%ir%03i_', PressureScans(ll).date, PressureScans(ll).run));
    runsavfol = fullfile(savfol, sprintf('%ir%03i', PressureScans(ll).date, PressureScans(ll).run));
    try
        copyfile(fullfile(runsavfol, 'PressureMontage.pdf'), [destfname 'PressureMontage.pdf']);
    catch
    end
    %copyfile(fullfile(runsavfol, 'LengthMontageFWB.pdf'), [destfname 'FWB_LengthMontage.pdf']);
end
