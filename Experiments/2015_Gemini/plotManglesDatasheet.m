% This will make some nicer looking plots out of Stuart's dataset
if ispc
    xlfile = fullfile(getDropboxPath, 'Writing', 'Gemini2015_paper', 'mono-scalings-data_20160818_KP.xlsx');
    [~,~,datraw] = xlsread(xlfile);
    
    % Now make a MATLAB matrix out of everything :)
    SMdat = struct;
    for i=2:size(datraw,1)
        ind = i-1;
        SMdat(ind).ExpSim = datraw{i,2};
        SMdat(ind).Guiding = datraw{i,3};
        SMdat(ind).InjType = datraw{i,4};
        SMdat(ind).CitaTag = datraw{i,5};
        SMdat(ind).Year = (datraw{i,6});
        SMdat(ind).n_e = (datraw{i,7});
        SMdat(ind).L_int = (datraw{i,8});
        SMdat(ind).E_e = (datraw{i,9});
        SMdat(ind).a0 = (datraw{i,10});
        SMdat(ind).E_laser = (datraw{i,11});
        SMdat(ind).t_laser = (datraw{i,12});
        SMdat(ind).P_laser = (datraw{i,14});
        SMdat(ind).FWHM = (datraw{i,13});
        SMdat(ind).Q_tot = (datraw{i,15});
        SMdat(ind).beamE = datraw{i,16};
    end
    
    save(fullfile(getDropboxPath, 'Writing', 'Gemini2015_paper', 'mono-scalings-data_20150708.mat'), 'SMdat');
elseif ismac
    load(fullfile(getDropboxPath, 'Writing', 'Gemini2015_paper', 'mono-scalings-data_20150708.mat'));
end
%% Try to plot a few things now

saving = 1;

P_laser = cell2mat({SMdat.P_laser});
E_e = cell2mat({SMdat.E_e});
expsim = cellfun(@(x) strcmp(x, 'ex'), {SMdat.ExpSim});
guid_flag = {SMdat.Guiding};
guid_self = cellfun(@(x) strcmp(x, 'self'), guid_flag) & expsim;
guid_guid = cellfun(@(x) strcmp(x, 'guided'), guid_flag) & expsim;
InjType = {SMdat.InjType};
inj_twostage = cellfun(@(x) strcmp(x, 'TS'), InjType);
inj_SI = cellfun(@(x) strcmp(x, 'SI'), InjType) & expsim;
inj_II = cellfun(@(x) strcmp(x, 'II'), InjType) & expsim;
inj_cap = cellfun(@(x) strcmp(x, 'cap'), InjType) & expsim;
inj_DRI = cellfun(@(x) strcmp(x, 'DRI'), InjType) & expsim;

% Our laser powers: 145 TW and 300 TW
% Our peak energies: 1600 MeV, 2000 MeV

hfig = figure(89); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 700 500]);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'XScale', 'log', 'LineWidth', 2);
hguid(1) = plot(P_laser(guid_self), E_e(guid_self), '*k');
hguid(2) = plot(P_laser(guid_guid), E_e(guid_guid), '*r');
hguid(3) = plot([145 300], [1600 2500], 'ob');
legend(hguid, {'Self-guided', 'Externally guided', 'This work'}, 'Location', 'Northwest', 'Box', 'off');
ylabel('Electron energy / MeV'); xlabel('Laser power / TW');
set(hguid, 'MarkerSize', 10, 'LineWidth', 2);
make_latex(hfig); setAllFonts(hfig, 20);
if saving
    export_fig(fullfile(getDropboxPath, 'Writing', '2016_StudentSeminar', 'ManglesScaling1'), '-pdf', '-nocrop', hfig);
end

hfig = figure(88); clf(hfig); 
set(hfig, 'Color', 'w', 'Position', [200 200 700 500]);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'XScale', 'log', 'LineWidth', 2);
hinj = []; 
hinjlab = {'Self-injection', 'Ionization injection', 'Capillary', 'Two-stage', 'Downramp injection', 'This work'};
hinj(1) = plot(P_laser(inj_SI), E_e(inj_SI), '*k');
hinj(2) = plot(P_laser(inj_II), E_e(inj_II), '*r');
hinj(3) = plot(P_laser(inj_cap), E_e(inj_cap), 'dm');
hinj(4) = plot(P_laser(inj_twostage), E_e(inj_twostage), '^g');
hinj(5) = plot(P_laser(inj_DRI), E_e(inj_DRI), 'vc');
hinj(end+1) = plot([145 300], [1600 2500], 'ob');
legend(hinj(ishandle(hinj)), hinjlab(ishandle(hinj)), 'Location', 'Northwest', 'Box', 'off');
ylabel('Electron energy / MeV'); xlabel('Laser power / TW');
set(hinj, 'MarkerSize', 10, 'LineWidth', 2);
make_latex(hfig); setAllFonts(hfig, 20);
if saving
    export_fig(fullfile(getDropboxPath, 'Writing', '2016_StudentSeminar', 'ManglesScaling2'), '-pdf', '-nocrop', hfig);
end

hfig = figure(89); clf(hfig); 
set(hfig, 'Color', 'w', 'Position', [200 200 700 500]);
set(gca, 'NextPlot', 'add', 'Box', 'on', 'XScale', 'log', 'LineWidth', 2, 'YLim', [0 4000]);
hinj = []; 
hinjlab = {'Self-injection', 'This work'};
hinj(1) = plot(P_laser(inj_SI), E_e(inj_SI), '*k');
hinj(end+1) = plot([145 300], [1600 2500], 'ob');
legend(hinj(ishandle(hinj)), hinjlab(ishandle(hinj)), 'Location', 'Northwest', 'Box', 'off');
ylabel('Electron energy / MeV'); xlabel('Laser power / TW');
set(hinj, 'MarkerSize', 10, 'LineWidth', 2);
make_latex(hfig); setAllFonts(hfig, 20);
if saving
    export_fig(fullfile(getDropboxPath, 'Writing', '2016_StudentSeminar', 'ManglesScaling3'), '-pdf', '-nocrop', hfig);
end
