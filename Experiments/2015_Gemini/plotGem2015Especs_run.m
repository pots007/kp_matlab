exits = zeros(1024,1024,79);
hfig = figure(4); clf(hfig);
locs = GetFigureArray(10, 8, 0.01, 0.01, 'across');
ax = nan(1, size(exits,3));
for i=1:size(exits,3)
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add');
    exits(:,:,i) = getGemini2015ExitMode(20150904, 6, i);
    imm = medfilt2(exits(:,:,i));
    imagesc(imm);
    C = contourc(imm, 0.5*max(imm(:))*[1 1]);
    plot(C(1,2:end), C(2,2:end), '.r');
    drawnow;
end
set(ax, 'XTick', [], 'YTick', []);
linkaxes(ax, 'xy');
axis tight