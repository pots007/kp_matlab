function tau = getpulselength(GSN)
% takes in gemini shot number and returns the pulse length in fs recorded
% in eCat. Note that a lot of these are NaN's!

load('2015LaserData.mat');
ind = find(laserdata.GSN==GSN);

tau = laserdata.tau(ind);

end