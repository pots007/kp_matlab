function GSN = getGSN(d,r,s)
% takes in date, run, shot in number format and returns Gemini shot number
% date should be in form YYYYMMDD

load('2015LaserData.mat');
%ind = find(laserdata.date==d & laserdata.run==r & laserdata.shot==s);

logm = laserdata.date==d & laserdata.run==r & laserdata.shot==s;
GSN = laserdata.GSN(logm);

end