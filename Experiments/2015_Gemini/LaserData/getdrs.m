function [d,r,s] = getdrs(GSN)
% takes in Gemini Shot Number and returns the date, run, shot that this
% happened on. Date in format YYYYMMDD

load('2015LaserData.mat');
ind = find(laserdata.GSN==GSN);

d = laserdata.date(ind);
r = laserdata.run(ind);
s = laserdata.shot(ind);

end