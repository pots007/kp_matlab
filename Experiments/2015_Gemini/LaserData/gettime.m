function time = gettime(GSN)
% takes in gemini shot number and outputs the time at which the shot
% happened as recorded in eCat. The output is possibly not in the most
% useful format...

load('2015LaserData.mat');
ind = find(laserdata.GSN==GSN);
time = laserdata.time(ind);

end