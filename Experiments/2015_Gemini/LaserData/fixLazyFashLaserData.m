% fixLazyFashLaserData.m

% This script fixes Fash's LaserData mat file
% The point is that if Ecat recorded NaN, then we can look into the user
% entered column to see if we have something in there.

load('2015LaserData');

logm = isnan(laserdata.energy);
sum(logm);
nanGSNs = laserdata.GSN(logm);

fdat = tdfread(fullfile(getGDrive, 'Experiment', '2015Gemini', 'ShotSheet_20160511.tsv'));
userens = fdat.South_Beam;
GSNs = fdat.Var5;

locdata = zeros(size(GSNs, 1), 2);
for i=1:length(GSNs)
    locdata(i,1) = str2double(GSNs(i,:));
    locdata(i,2) = str2double(userens(i,:));
end

%% And now the actual replacing loop

for i=1:length(nanGSNs)
    ind2 = find(locdata(:,1)==nanGSNs(i));
    ind1 = find(laserdata.GSN==nanGSNs(i));
    laserdata.energy(ind1) = locdata(ind2,2);
end

save(fullfile(getDropboxPath, 'MATLAB', 'Experiments', '2015_Gemini', 'LaserData', '2015LaserData'), 'laserdata');