function E = getenergy(GSN)
% takes Gemini Shot Number as input (can be found with date, run, shot data
% from getGSN.m) and outputs the energy after amplification from eCat. N.B.
% this does not account for compression losses or transport losses.

load('2015LaserData.mat');
ind = find(laserdata.GSN==GSN);

E = laserdata.energy(ind);

end