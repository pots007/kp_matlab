rootf = '/Volumes/Gemini2015/Data/';
outfol = '/Volumes/Sanbdbox/krismoose/2015ExitMode/';

days = dir(rootf); days = {days.name}; days = days(3:end);
% Iterate over days, then runs etc
for i=1:length(days)
    runs = dir([rootf days{i}]);
    runs = {runs.name}; runs = runs(3:end);
    for k=1:length(runs)
        shots = dir([rootf '/' days{i} '/' runs{k} '/*ExitMode*']);
        if isempty(shots); continue; end;
        for l=1:length(shots)
            copyfile(fullfile(rootf,days{i},runs{k},shots(l).name),...
                fullfile(outfol,shots(l).name));
            disp(shots(l).name);
        end
    end
end