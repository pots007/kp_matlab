%% Espec 1;

f1 = '/Volumes/Gemini2015/References/PossiblyIncompleteReferences/20150816_espec1_ref.tif';
ref1 = imread(f1);
f2 = '/Volumes/Gemini2015/References/2015Refs/EspecRefs/Espec1/20150901.tif';
ref2 = imread(f2);
f3 = '/Volumes/Gemini2015/Data/20150902/20150902r003/20150902r003s030_Espec1.tif';
ref3 = imread(f3);


figure(9);
ax1 = subplot(1,2,1);
imagesc(ref1);
ax2 = subplot(1,2,2);
imagesc(ref3);
linkaxes([ax1 ax2], 'xy');

%% Espec 2LE

f1 = '/Volumes/Gemini2015/References/PossiblyIncompleteReferences/20150816_espec2le_01.BMP';
ref1 = imread(f1);
f2 = '/Volumes/Gemini2015/References/2015Refs/EspecRefs/Espec2LE_pike/20150901_Espec2LE.raw';
ref2 = flipud(ReadRAW16bit(f2, 1280, 960));

figure(9);
ax1 = subplot(1,2,1);
imagesc(ref1);
ax2 = subplot(1,2,2);
imagesc(ref2);
linkaxes([ax1 ax2], 'xy');