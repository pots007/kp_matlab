% Think about imaging the ILAO mirror
F40 = 6; %in m everywhere
Dbeam = 0.15;
l_ILAO_F40 = 1:0.01:3;
% image of the ILAO is formed at:
l_im1_F40 = F40*l_ILAO_F40./(l_ILAO_F40-F40);
plot(l_ILAO_F40, l_im1_F40, 'x');
Flens = 0.15;
l_F40_lens = F40 + Flens;
l_im2_lens = Flens*(l_F40_lens-l_im1_F40)./(l_F40_lens-l_im1_F40-Flens);
plot(l_ILAO_F40, l_im2_lens, 'x');

