% Try to come up with a semi reliable backtracking method.

% Run analyse20150903r005_plot1 and _plot2

fstate = load(fullfile(getDropboxPath, 'MATLAB', 'guis', 'MagnetTracker','Gemini2015_20160302update.mat'));
hfig = figure(44); clf(hfig);
set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);

hh1 = plot(1:28, peakEns, 'ok');
hh2 = plot(1:28, peakEns2, 'or');

% Now try to plot the heights along the screens
alongscreen1 = fdat.shotdata.alongscreen(maxind);
alongscreen2 = fdat2.shotdata.alongscreen(maxind2);

cla;
hh1 = plot(1:28, alongscreen1, 'ok');
hh2 = plot(1:28, alongscreen2, 'or');

% Now create the z-y point pairs of all these hit locations

y1 = fstate.dat.y1; z1 = fstate.dat.z1; thet1 = fstate.dat.thet1;
coords1(:,1) = y1 + alongscreen1*sind(thet1); % The y coordinate of hit
coords1(:,2) = z1 + alongscreen1*cosd(thet1);

y2 = fstate.dat.y3; z2 = fstate.dat.z3; thet2 = fstate.dat.thet3;
coords2(:,1) = y2 + alongscreen2*sind(thet2); % The y coordinate of hit
coords2(:,2) = z2 + alongscreen2*cosd(thet2);

cla;
plot(coords1(:,2), coords1(:,1), 'ok');
plot(coords2(:,2), coords2(:,1), 'or');

% Now create the direction vectors
slopes = (coords2(:,1) - coords1(:,1))./(coords2(:,2) - coords1(:,2));
slope_angles = atand(slopes) + 180;
r_initials = [coords2(:,1) coords2(:,2)];
p_initials = [sind(slope_angles) cosd(slope_angles)];

% Plot these for funs and giggles.
cla;
quiver(r_initials(:,2),r_initials(:,1), p_initials(:,2), p_initials(:,1));

% And now try to do some backtracking!
% Use the same brute force method as in MagnetTracker for now.
ftrack = fstate.dat;
ftrack.B1.B = -ftrack.B1.B; % Backwards in time and whatnot
cellL = 20;
btrackdata = zeros(size(r_initials,1),2);
for i=1:size(p_initials,1)
    fprintf('Trying to track electron %i\n', i);
    E0 = mean([peakEns(i) peakEns2(i)]);
    % Can only track if we have sensible positions on both screens
    if isnan(E0); continue; end 
    % Now for tracking
    ftrack.pin = [0 p_initials(i,:)]; ftrack.Rin = [0 r_initials(i,:)]*1e-3;
    ftrack.divoff = 0; ftrack.ediv = [];
    % Set the energy to +-35% and see which energy is closest to our desired value
    ftrack.Emin = E0*0.65; ftrack.Emax = E0*1.35;
    ftrack.noe = 25; ftrack.espace = 2;
    % First, rough tracking
    fret1 = track_electrons2(ftrack);
    ys = zeros(1,length(fret1.div.el));
    for k=1:length(fret1.div.el)
        R = fret1.div.el(k).r;
        [~,ys(k)] = polyxpoly(R(:,3),R(:,2), [cellL cellL], [-1000 1000]);
    end
    [~,ind] = min(abs(ys));
    E1 = fret1.div.el(ind).energy*1.8712e21;
    ftrack.Emin = E1*0.95; ftrack.Emax = E1*1.05;
    ftrack.noe = 25; ftrack.espace = 2;
    % First, rough tracking
    fret2 = track_electrons2(ftrack);
    ys = zeros(1,length(fret2.div.el));
    for k=1:length(fret2.div.el)
        R = fret2.div.el(k).r;
        [~,ys(k)] = polyxpoly(R(:,3),R(:,2), [cellL cellL], [-1000 1000]);
    end
    [~,ind] = min(abs(ys));
    R = fret2.div.el(ind).r; 
    btrackdata(i,1) = 1e3*(R(end-1,2)-R(end,2))/(R(end-1,3)-R(end,3));
    btrackdata(i,2) = fret2.div.el(ind).energy*1.8712e21;
end
