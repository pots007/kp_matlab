% getGem2015ScreenEnergy.m
%
% This function returns the energy for a given screen and a given pointing
% angle. 
%
% getGem2015ScreenEnergy(distToEdge, scrID, divID, pointing)
%   distToEdge - distance (in mm) from high energy edge of screen
%   scrID      - screen, as defined in Geometry setup in MagnetTracker (default 1)
%   divID      - beam divergence hits, only -1, 0 and 1 mrad (default 0)
%   pointing   - beam pointing offset, in range from -3 to 11 mrad (default 0)

function en = getGem2015ScreenEnergy(distToEdge, scrID, divID, pointing)

% To speed things up!
persistent splineLookUpMatrix;
if isempty(splineLookUpMatrix)
    fdat = load('lookupSpline');
    splineLookUpMatrix = fdat.lookupSpline;
end

% Handle lazy users
if nargin==1
    scrID = 1;
    divID = 2;
    pointing = 0;
elseif nargin==2
    divID = 2;
    pointing = 0;
elseif nargin==3
    pointing = 0;
end
% In case we want to do a vector
if isvector(distToEdge)
    pointing = repmat(pointing, size(distToEdge));
end

en = splineLookUpMatrix{scrID, divID}(pointing, distToEdge);