% daq23645_ExtractLineOuts.m
%
% Extract lineout from a daq run, save them into mat file. Also saves the
% transverse lineout within the roi.

function daq23645_ExtractLineOuts

if getComputerName=='flalap1-ubuntu'
    dfol = '/media/kpoder/KRISMOOSE6/28mdata/run23645';
end
flist = dir(fullfile(dfol, 'EspecHighres', '*.png'));
ROI = [140, 1, 670, 2047];

nFiles = length(flist);
savDat = cell(nFiles, 5);

for i=1:nFiles
    if mod(i,100)==0
        fprintf('\tOpening file %4i of %4i\n', i, nFiles);
    end
    imm = double(imread(fullfile(flist(i).folder, flist(i).name)));
    savDat{i,1} = flist(i).name;
    savDat{i,2} = str2double(regexp(flist(i).name, '\d{9}', 'match'));
    subimm = imm(ROI(2):ROI(2)+ROI(4), ROI(1):ROI(1)+ROI(3));
    savDat{i,3} = sum(subimm,1);
    savDat{i,4} = sum(subimm,2);
end
save(fullfile(dfol, 'EspecData'), 'savDat');
