% analyseRun23645.m
%
% Plot the evolution of stuff!!!!

if getComputerName=='flalap1-ubuntu'
    dfol = '/media/kpoder/KRISMOOSE6/28mdata/run23645/';
    datf = load(fullfile(dfol,'EspecData'));
end

exportData = true;

% Calculate some things for the entire dataset:
nFiles = size(datf.savDat,1);
smoothing = true;
% dat is the data! columns: PID, totQ, FWHM_en, FWHM_trans.
dat = zeros(nFiles, 4);
for i=1:nFiles
    dat(i,1) = datf.savDat{i,2};
    if smoothing
        dat(i,2) = sum(smooth(datf.savDat{i,4}));
        dat(i,3) = widthfwhm(smooth(datf.savDat{i,4}));
        dat(i,4) = widthfwhm(smooth(datf.savDat{i,3}));
    end
end

%% Now the filtering to shots! 
% Load the fitted parameters...
ff = load(fullfile(dfol, 'ShotAlignment'));
% I think 95 scan points ought to be enough, in the data there is 100 I
% think?
nScans = 95;

analQ = zeros(nScans, 3);
analE = zeros(size(analQ));
analT = zeros(size(analQ));
for scan=1:nScans
    intStart = ff.startShot + 10*ff.imPerPoint*(scan-1);
    intEnd   = ff.startShot + 10*ff.imPerPoint*(scan  );
    logm = dat(:,1) >= intStart & dat(:,1) < intEnd;
    scanData = dat(logm,:);
    % Throw away the last point of each scan point - this may be dodgy!
    scanData = scanData(1:end-1,:);
    
    % analyse the data!
    % dat is the data! columns: PID, totQ, FWHM_en, FWHM_trans.
    analQ(scan, 2) = mean(scanData(:,2));
    analQ(scan, 3) =  std(scanData(:,2));
    analE(scan, 2) = mean(scanData(:,3));
    analE(scan, 3) =  std(scanData(:,3));
    analT(scan, 2) = mean(scanData(:,4));
    analT(scan, 3) =  std(scanData(:,4));
    % And find the correct delay value as well.
    logm = ff.delays(:,1) >= intStart & ff.delays(:,1) < intEnd;
    delayVals = ff.delays(logm,2);
    if length(unique(delayVals)) > 1
        fprintf('Scan point %2i has multiple delay values!!\n', scan);
        for k=1:length(delayVals); fprintf('\t%i', delayVals(k)); end;
        fprintf('\n');
    end
    analQ(scan,1) = delayVals(end);
    analE(scan,1) = delayVals(end);
    analT(scan,1) = delayVals(end);
    
end

% Scale the charge data! Scan pos 1 should be 300 pC...
analQ(:,2:3) = analQ(:,2:3)*300/analQ(1,2);
%% Now for some plotting!!!!
hfig = figure(412414); clf(hfig);
hfig.Position = [1200, 100, 600, 900];
locs = GetFigureArray(1, 3, [0.03, 0.03, 0.1, 0.2], 0.04, 'down');
ax = []; clear hh;
for i=1:3
    ax(end+1) = makeNiceAxes(hfig, locs(:,i));
end

delayAxis = (analQ(:,1) - analQ(end,1))*9.2e-3 - 2.85;
hh(1) = errorbarxy(ax(1), delayAxis, analQ(:,2), [], analQ(:,3));
hh(2) = errorbarxy(ax(2), delayAxis, analE(:,2), [], analE(:,3));
hh(3) = errorbarxy(ax(3), delayAxis, analT(:,2), [], analT(:,3));

linkaxes(ax, 'x');

% And make it noice!
for k=1:3
    set(hh(k).hMain, 'Marker', 'd', 'LineWidth', 1, 'LineStyle', 'none',...
        'MarkerSize', 3);
    set(hh(k).hErrorbar, 'LineWidth', 1);
    grid(ax(k), 'on'); drawnow;
    
    makeNiceGridHG2(ax(k), 1, '-', 1, '--');
end

% And the labels and ticks
ylabel(ax(1), 'Integrated charge / pC');
ylabel(ax(2), {'FWHM energy' 'spread / pix'});
ylabel(ax(3), {'FWHM transverse' 'beam size / pix'});
xlabel(ax(3), 'Discharge delay / us');
set(ax([1,2]), 'XTickLabel', [], 'XLim', [-11.7, -3]);
%set(ax(1), 'YLim', [4.4, 7.2]*1e6);
set(ax(2), 'YLim', [45, 205]);
set(ax(3), 'YLim', [10, 70]);

setAllFonts(hfig, 16);
%export_fig(fullfile(dfol, 'EspecAnalysis'), '-nocrop', '-pdf', hfig);

% And export stuff to a file
if exportData
    fid = fopen(fullfile(dfol, 'FWHM_data.dat'), 'w');
    for k=1:length(delayAxis)
        fprintf(fid, '%1.6f\t%1.6f\t%1.6f\n', delayAxis(k), analE(k,2), analE(k,3));
    end
    fclose(fid);
end

%% And plot the average spectrum at 4 points
% These are the points where we will plot the spectrum later on.
delayInds = [-1, 22, 38, 52];
delaySpec = cell(size(delayInds));
for i=1:length(delayInds)
    intStart = ff.startShot + 10*ff.imPerPoint*(delayInds(i)-1);
    intEnd   = ff.startShot + 10*ff.imPerPoint*(delayInds(i)  );
    logm = dat(:,1) >= intStart & dat(:,1) < intEnd;
    scanShots = dat(logm,1);
    scanShots = scanShots(1:end-1);
    scanSpec = zeros(length(scanShots), 2048);
    for s = 1:length(scanShots)
        ind = find(scanShots(s)==dat(:,1), 1);
        scanSpec(s,:) = datf.savDat{ind,4};
    end
    delaySpec{i} = scanSpec;
end

% Whether or not to plot the shaded error bar
plotError = true;

hfig = figure(412415); clf(hfig);
hfig.Position = [800, 200, 600, 500];
axx = makeNiceAxes(hfig, [0.1, 0.13 0.85, 0.85]);
cols = brewermap(9, 'Set1'); %cols = flipud(cols);
clear hhh;
for k=1:length(delayInds)
    if plotError
        sh = shadedErrorBar(1:2048,delaySpec{k}*1e-4,{@mean,@std},'-',1); 
        % Edges off!
        set(sh.edge, 'Color', 'none');
        hhh(k) = sh.mainLine;
        sh.mainLine.Color = cols(k,:);
        set(sh.patch, 'FaceColor', cols(k,:), 'FaceAlpha', 0.35);
    else
        hhh(k) = plot(smooth(mean(delaySpec{k}))*1e-4, 'Color', cols(k,:));
    end
end
set(hhh, 'LineWidth',2)

% Legend...
leglabs = {};
for k=1:length(delayInds)
    dd = delayInds(k);
    if dd < 1
        leglabs{k} = 'Unperturbed';
    else
        leglabs{k} = sprintf('Delay = %1.1f ns', delayAxis(dd));
    end
end
  
legend(hhh, leglabs, 'Location', 'Northeast', 'box', 'off');
set(axx, 'XLim', [900, 1600], 'YLim', [-0.05 7]);
xlabel(axx, 'Energy / pixel');
ylabel(axx, 'Spectral density / a.u.');
setAllFonts(hfig, 16);

if plotError 
    %saveas(hfig, fullfile(dfol, 'EspecTraces2.svg'), 'svg');
    %export_fig(fullfile(dfol, 'EspecTraces2'),'-png', '-nocrop', '-m4', hfig);
else
    %export_fig(fullfile(dfol,'EspecTraces1'), '-nocrop', '-pdf', hfig);
end

if exportData
    for i= 1:length(delayInds)
        dd = delayInds(i);
        if dd < 0; fname = 'spec_unperturbed.dat';
        else; fname = sprintf('spec_delay=%1.2fns.dat', delayAxis(dd)); end
        fid = fopen(fullfile(dfol, fname), 'w');
        means = mean(delaySpec{i});
        stds = std(delaySpec{i});
        for k=1:2048
            fprintf(fid, '%i\t%1.6f\t%1.6f\n', k, means(k), stds(k));
        end
        fclose(fid);
    end
end