% daqShotAlignment.m
%
% Try to figure out the delays and shots from a DAQ run

dfol = '/media/kpoder/KRISMOOSE6/28mdata/run23645';

% First the delays
datf = load(fullfile(dfol, 'TunnelDischargeDelay.mat'));
delayIDs = cellfun(@(x) str2double(regexp(x, '\d{9}', 'match')), fieldnames(datf.data));
delays = [delayIDs, struct2array(datf.data)'];

% Now the files ( images )
flist = dir(fullfile(dfol, 'EspecHighres', '*png'));
flistnames = {flist.name};
images = cellfun(@(x) str2double(regexp(x, '\d{9}', 'match')), flistnames);

%% Some filtering of the data
% We can safely ignore a few cases: before the delay started moving, and
% at the end, where the delay is staying constant again. Set up a logical
% matrix for these:
% NB This is dirty as it relies on no other identical delay values.
% NEEEEEEEEED to change this for future.
iend = find(delays(:,2)==delays(end,2), 1, 'first');
ibeg = find(delays(:,2)==delays(1,2), 1, 'last');

% Can now filter the shots based on the PIDs of these.
logm = images >= delays(ibeg,1) & images <= delays(iend,1);
fprintf('Have %i of %i shots remaining\n', sum(logm), length(images));

%% Now need to consider the range of beginning candidates.
% The last acceptable first shot ID is the ID of the first delay point that
% is part of the scan. The first acceptable shot ID is that of the last
% point in the delay scan that has the value of the first scanpoint, minus
% number of images per scan point.

imPerPoint = 51; % 50 + 1 to account for the 'settling time';
startmax = delays(ibeg+1,1);
startmini = ibeg+1;
while delays(startmini,2)==delays(ibeg+1,2); startmini = startmini + 1; end
startmin = delays(startmini-1,1) - imPerPoint*10;
fprintf('First shot of scan must be between %i and %i\n', startmin, startmax);
logm2 = images >= startmin & images <= startmax;
fprintf('There are %i images saved in the interval above\n', sum(logm2));
firstShotCands = images(logm2);

%% Now do the actual thinking and matching...
% Need to check that for an interval of shots starting from any of the
% first shot candiates, the delays saved in that region all have the same
% values.
%firstShotCands = firstShotCands(1):firstShotCands(end);
nScanShots = length(unique(delays(:,2))) - 2;
failCount = zeros(size(firstShotCands));
for sc = 1:length(firstShotCands)
    for it=1:nScanShots
        % The interval starts:
        intStart = firstShotCands(sc) + 10*imPerPoint*(it-1);
        intEnd   = firstShotCands(sc) + 10*imPerPoint*(it  );
        % Shots that lie in this range in the delays:
        lm = delays(:,1) > intStart & delays(:,1) <= intEnd;
        if length(unique(delays(lm,2)))~=1
            fprintf('Candidate %3i: Fail at interval %i\n', sc, it);
            failCount(sc) = it;
            break;
        end
    end
    failCount(sc) = it;
end

%% Check out that image...
[~,maxi] = max(failCount);
indd = find(images==firstShotCands(maxi),1) + 4000;
fname = fullfile(dfol, 'EspecHighres', flistnames{indd});
imm = double(imread(fname));
imagesc(medfilt2(imm));
colorbar;
set(gca, 'Clim', [0 20]);

[Ny,Nx] = size(imm);
ROI = [140, 850, 670, 800];
rectangle('Position', ROI, 'EdgeColor', 'r');
subimm = imm(ROI(2):ROI(2)+ROI(4), ROI(1):ROI(1)+ROI(3));
%plot(1:Ny,sum(imm, 2), 1:Ny, sum(medfilt2(imm),2), '-r')
%plot(1:ROI(4)+1, sum(subimm,2),1:ROI(4)+1, sum(medfilt2(subimm),2), 'r');
sum_simm1 = sum(subimm(:));
sum_simm2 = sum(sum(medfilt2(subimm)));

% Save for future use
startShot = images(find(images==firstShotCands(maxi),1));
save(fullfile(dfol, 'ShotAlignment'), 'imPerPoint', 'startShot', 'delays')
%sum_simm1/sum_simm2