function energy = GetATA2Energy(date, run, shot)

if (ismac)
    savfol = '/Users/kristjanpoder/Dropbox/Writing/ATA2_II_paper/';
    datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
    trackfile = '/Users/kristjanpoder/Dropbox/ICT_release/version1.0/TA2_2013_LundMag8.mat';
elseif (ispc)
    savfol = 'F:\Kristjan\Documents\Uni\Dropbox\Writing\ATA2_II_paper\';
    datafolder = 'F:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Dave\Data\';
    trackfile = 'F:\Kristjan\Documents\Uni\Dropbox\ICT_release\version1.0\TA2_2013_LundMag8.mat';
end
try
    filen = sprintf('%i/%ir%03i/%ir%03is%03i_laser_nf.raw', date,date,run,date,run,shot);
    NFim = ReadRAW16bit([datafolder filen], 640, 480);
catch
    disp('The file doesn''t seem to exist!');
    energy = nan;
    return;
end
bg1 = NFim(1:50,:);
bg2 = NFim(:,1:50);
BG = mean([mean(bg1(:)) mean(bg2(:))]);
NFval = sum(sum(NFim-BG));
% The following values are from NF_camera_calib_diode.m script
% DO NOT change BG regions, else that script needs to update calibration
% values
energy = -0.004261 + 6.4076e-10*NFval;
energy = energy*0.62; % To account for compressor losses as well
end