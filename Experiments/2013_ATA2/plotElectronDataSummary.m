if ispc
    dfol = 'E:\Kristjan\Google Drive\Experiment\2013TA2\analysis\';    
end

load([dfol 'eldata.mat']);
minQ = 5;
SIruns = eldata.D20131004.data;
SIlogm = SIruns(:,3)>minQ;
IIruns = eldata.D20131010.data;
IIlogm = IIruns(:,3)>minQ;
locs = GetFigureArray(2,2,0.07,0.02, 'down');
figure(43); clf;

axs(1) = axes('Parent', 43, 'Position', locs(:,1));
h(1) = plot(SIruns(SIlogm,4), SIruns(SIlogm,1), 'ok');
legend(h(1), 'SI highest energy', 'Location', 'Northeast')
axs(2) = axes('Parent', 43, 'Position', locs(:,2));
h(2) = plot(SIruns(SIlogm,4), SIruns(SIlogm,2), 'or');
legend(h(2), 'SI peak energy', 'Location', 'Northeast')
axs(3) = axes('Parent', 43, 'Position', locs(:,3));
h(3) = plot(IIruns(IIlogm,4), IIruns(IIlogm,1), 'dk');
legend(h(3), 'II highest energy', 'Location', 'Northeast')
axs(4) = axes('Parent', 43, 'Position', locs(:,4));
h(4) = plot(IIruns(IIlogm,4), IIruns(IIlogm,2), 'dr');
legend(h(4), 'II peak energy', 'Location', 'Northeast')
set(axs(3:4), 'YAxisLocation', 'right')
set(axs([1 3]), 'XTickLabel', []);
set(h, 'LineWidth', 2);
xlabel(axs(2), 'Backing pressure / bar');
xlabel(axs(4), 'Backing pressure / bar');
for i=1:4; ylabel(axs(i), 'Energy / MeV'); end;
setAllFonts(43,18);
export_fig([dfol 'peaks_max'], '-pdf', '-transparent', '-nocrop', 43);