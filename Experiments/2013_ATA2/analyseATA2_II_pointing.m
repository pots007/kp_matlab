% And now analyse the II pointing data (aka make nicish graphs)

load IIpointingdata;
IIpointingdata = IIpointdata.pointingdata;

hfig = 812;
figure(812); clf(812);
set(812, 'Color', 'w', 'Position', [100 100 800 600]);
bins = linspace(-10, 20, 30);
[Nx,Xx] = hist(IIpointingdata(:,1), bins);
[Ny,Xy] = hist(IIpointingdata(:,2), bins);

set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
h1 = stairs(Xx, Nx, '--k');
h2 = stairs(Xy, Ny, '--r');
set([h1 h2], 'LineWidth', 2);
legend([h1 h2], {'Horizontal', 'Vertical'}, 'Location', 'NorthEast');
title('Pointing fluctuations')
ylabel('$N$');
xlabel('$\theta \: / \: \mathrm{mrad}$');
% Add the means to the plot
text(10, 8, sprintf('$\\langle\\theta\\rangle = %2.1f $ mrad', mean(IIpointingdata(:,1))));
text(10, 7, sprintf('$\\sigma_{\\theta} = %2.1f $ mrad', std(IIpointingdata(:,1))));
text(-9.8, 9, sprintf('$\\langle\\theta\\rangle = %2.1f $ mrad', mean(IIpointingdata(:,2))), 'Color', 'r');
text(-9.7, 8, sprintf('$\\sigma_{\\theta} = %2.1f $ mrad', std(IIpointingdata(:,2))), 'Color', 'r');
make_latex(handle(hfig));
setAllFonts(812,20);

if ispc
    savfol = 'E:\Kristjan\Google Drive\Experiment\2013TA2\analysis\Eprofile\';
elseif ismac
    savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/analysis/Eprofile/';
end

export_fig(fullfile(savfol, 'II_pointing_offsets'), '-pdf', '-nocrop', 812);
%% And now the beam sizes as well

figure(813); clf(813);
set(813, 'Color', 'w', 'Position', [100 100 800 600]);
set(gca, 'NextPlot', 'add', 'LineWidth', 2, 'Box', 'on');
bins = linspace(0, 6, 20);
[Nx,Xx] = hist(IIpointingdata(:,3), bins);
[Ny,Xy] = hist(IIpointingdata(:,4), bins);

%h1 = plot(1:50, IIpointingdata(:,3), 'ok');
%h2 = plot(1:50, IIpointingdata(:,4), 'dr');
h1 = stairs(Xx, Nx, '--k');
h2 = stairs(Xy, Ny, '--r');
set([h1 h2], 'LineWidth', 2);
legend([h1 h2], {'Horizontal', 'Vertical'});
title('Beam divergence')
ylabel('$N$');
xlabel('Half-angle / mrad');
text(4, 11, sprintf('$\\langle\\theta\\rangle = %2.1f $ mrad', mean(IIpointingdata(:,3))));
text(0.2, 11, sprintf('$\\langle\\theta\\rangle = %2.1f $ mrad', mean(IIpointingdata(:,4))), 'Color', 'r');
make_latex(813);
setAllFonts(813,20);

export_fig(fullfile(savfol, 'II_beam_sizes'), '-pdf', '-nocrop', 813);

%% And the cumulative image, for funsies

figure(813); clf(813);
set(813, 'Color', 'w', 'Position', [100 100 800 600]);

cmap = brewermap(256, 'YlGnBu');
colormap(813, cmap);
imagesc(IIpointdata.x_angle, IIpointdata.y_angle, IIpointdata.imsum);
set(gca, 'LineWidth', 2, 'Box', 'on');
title('Sum of 50 images')
ylabel('Vertical pointing / mrad');
xlabel('Horizontal pointing / mrad');
make_latex(813);
setAllFonts(813,20);

export_fig(fullfile(savfol, 'II_cumulative_image'), '-pdf', '-nocrop', 813);

%export_fig(fullfile(savfol, 'II_beam_sizes'), '-pdf', '-nocrop', 813);