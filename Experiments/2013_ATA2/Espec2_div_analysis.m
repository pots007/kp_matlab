savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Espec2/';
datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
date = 20131010;
figure(121); clf;
%set(120, 'Position', [100 100 900 600]);%, 'PaperUnits', 'centimeters', 'PaperSize', [8.6 6]);
%axs = zeros(4,1);
divlims = [-30 30];
%II part
runn = 1;
load('invfire.mat');
load('II_shots_pressures.mat');
divs1 = zeros(1093,size(press,2));
divsII = zeros(3,size(press,2));
for i=1:size(press,2)   
    disp(['Doing image ' num2str(i)]);
    %ax1 = axes('Position', [0.3 0.1 0.6 0.8]);
    imag = WarpAndRotTA2(date,runn,press(1,i));
    enax = imag.enaxis;
    divax = imag.pixdiv*imag.yaxis;
    co2disp = enax(1:end-1) - enax(2:end); %To correct for dispersion
    co2disp = repmat(co2disp, length(divax),1);
    linen = linspace(min(enax), max(enax), 1000); %Interpolate to linear grid to avoid pcolor
    image = imag.image(:,1:end-1)./co2disp;
    linimag = zeros(size(image,1),1000);
    for l=1:size(image,1)
        linimag(l,:) = interp1(enax(1:end-1), image(l,:), linen, 'pchip');
    end
    divs1(:,i) = -sum(linimag,2);
    divs1(1:182,i) = 0; % To take the weird background off! hopefully is enough
    if (i==18) divs1(1:300) = 0; end; % Needed for that shot
    divsII(1,i) = press(2,i);
    divsII(2,i) = widthfwhm(divs1(:,i)')*imag.pixdiv;
    divsII(3,i) = press(1,i);
    %plot(divs(:,i));
    %pause(0.5);
end
%
%SI part
date = 20131004;
runn = 2;
load('invfire.mat');
load('SI_shots_pressures.mat');
divs = zeros(1137,size(press,2));
divsSI = zeros(3,size(press,2));
for i=1:size(press,2)   
    disp(['Doing image ' num2str(i)]);
    %ax1 = axes('Position', [0.3 0.1 0.6 0.8]);
    imag = WarpAndRotTA2(date,runn,press(1,i));
    enax = imag.enaxis;
    divax = imag.pixdiv*imag.yaxis;
    co2disp = enax(1:end-1) - enax(2:end); %To correct for dispersion
    co2disp = repmat(co2disp, length(divax),1);
    linen = linspace(min(enax), max(enax), 1000); %Interpolate to linear grid to avoid pcolor
    image = imag.image(:,1:end-1)./co2disp;
    linimag = zeros(size(image,1),1000);
    for l=1:size(image,1)
        linimag(l,:) = interp1(enax(1:end-1), image(l,:), linen, 'pchip');
    end
    divs(:,i) = sum(linimag,2);
    divsSI(1,i) = press(2,i);
    divsSI(2,i) = widthfwhm(divs(:,i)')*imag.pixdiv;
    divsSI(3,i) = press(1,i);
    %plot(divs(:,i));
    %pause(0.5);
end
plot(divsII(1,:), divsII(2,:), 'kx', divsSI(1,:), divsSI(2,:), 'ro');