% analyseATA2_eprofile_calib.m

% Unfortunately the best available reference is from 1030, but what can we
% do
%% Set up reference
if ismac
    reff = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/20131016/References/';
end

cmap = brewermap(256, 'YlGnBu');
% Start with reference.

greenieref = imread(fullfile(reff, 'eprofile_center_of_beam_ref.TIF'));

figure(810); clf(810);
set(810, 'Color', 'w', 'Position', [100 100 800 600]);
imagesc(greenieref);
set(gca, 'YDir', 'normal');

% Do FWb and then fit line to that data
hline = sum(greenieref,1);
hline = hline./max(hline)*380;
hfit = fit((1:size(greenieref,2))', hline', 'gauss2');
vline = sum(greenieref, 2);
vline = vline./max(vline)*580;
vfit = fit((1:size(greenieref,1))', vline, 'gauss2');
xc = hfit.b1; yc = vfit.b1;
%xc = 322; yc = 222;

% Plot everything else
hold on; colormap(cmap);
plot(1:size(greenieref,2), hline, 'ok');
plot(feval(vfit, 1:size(greenieref,1)), 1:size(greenieref,1), '-r');
plot(1:size(greenieref,2), feval(hfit, 1:size(greenieref,2)), '-r');
plot(vline, 1:size(greenieref,1), 'ok');
plot(xc*[1 1], [1 size(greenieref,1)], '--k');
plot([1 size(greenieref,2)], yc*[1 1], '--k');
hold off;

P0 = [yc xc];

% And save the plot, if we fancy
if false
    export_fig(fullfile(reff, 'eprofile_center_of_beam'), '-pdf', '-nocrop', 810);
end

% From some code long time ago, seems to be correct for ATA2 profile screen
y_pixel= 1:size(greenieref,1);
x_pixel= 1:size(greenieref,2);

% Below, these are the axes in angular units!
dist0 = 400;
xscale = 1000*0.0519*cos(pi/4)/dist0;
yscale = 1000*0.0526/dist0;
y_angle = 1000*(y_pixel*0.0526-yc*0.0526)/dist0; % in mrad
x_angle = 1000*(x_pixel*0.0519*cos(pi/4)-xc*0.0519*cos(pi/4))/dist0;

%% Set up paths

if ismac
    datafol = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
    savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Eprofile';
    runf1 = '20131016/20131016r002';
end

%% Do run 2. - very simplistic. Do the one below for better analysis

flist = dir(fullfile(datafol, runf1, '*Eprofile.raw'));

figure(811); clf(811);
set(811, 'Color', 'w', 'Position', [100 100 800 600]);
colormap(811, cmap);
imsum = zeros(480, 640);
% Columns will be Intensity centroid x, y Gaussian centroid x, y
IIpointingdata = zeros(length(flist), 4);
for i=1:length(flist)
    
    clf(811);
    im = double(ReadRAW16bit(fullfile(datafol, runf1, flist(i).name), 640, 480));
    im(28, 26:38) = 0;
    imagesc(x_angle, y_angle, im)
    set(gca, 'YDir', 'normal', 'NextPlot', 'add');
    hline = sum(im,1);
    hline = hline./max(hline)*10;
    hfit = fit(x_angle', hline', 'gauss2');
    vline = sum(im, 2);
    vline = vline./max(vline)*10;
    vfit = fit(y_angle', vline, 'gauss2');
    % Plot horizontal lines
    plot(x_angle, hline + y_angle(1), 'ok');
    plot(x_angle, feval(hfit, x_angle)+y_angle(1), '-r');
    plot(vline+x_angle(1), y_angle, 'ok');
    plot(feval(vfit, y_angle)+x_angle(1), y_angle, '-r');
    P0 = centroidlocationsmatrix(im);
    h(1) = plot(x_angle(round(P0(1))), y_angle(round(P0(2))), 'xg', 'MarkerSize', 14, 'LineWidth', 2);
    h(2) = plot(hfit.b1, vfit.b1, 'xr', 'MarkerSize', 14, 'LineWidth', 2);
    legend(h, {'$\mathrm{Intensity}$', '$\mathrm{Gaussian}$'}, 'Location', 'NorthEast',...
        'Color', 'none')
    xlabel('$\theta_{hor}\:/ \:\mathrm{mrad}$')
    ylabel('$\theta_{ver}\:/ \:\mathrm{mrad}$')
    title(['$ \mathrm{Shot} \quad ' num2str(i) '$']);
    make_latex(811);
    setAllFonts(811, 20);
    export_fig(fullfile(savfol, runf1, flist(i).name(1:end-4)), '-pdf', '-nocrop', 811);
    % And collate data
    imsum = imsum + im;
    IIpointingdata(i,1) = x_angle(round(P0(1))); IIpointingdata(i,2) = y_angle(round(P0(2)));
    IIpointingdata(i,3) = hfit.b1; IIpointingdata(i,4) = vfit.b1;
end

%% Now do run2, with backgroudn subtraction and ellipse fitting!

flist = dir(fullfile(datafol, runf1, '*Eprofile.raw'));

figure(811); clf(811);
set(811, 'Color', 'w', 'Position', [100 100 800 600]);
colormap(811, cmap);
imsum = zeros(480, 640);
% Columns will be xc, yc, width_major, width_minor, angle
IIpointingdata = zeros(length(flist), 6);
for i=5%1:length(flist)
    
    clf(811);
    im = double(ReadRAW16bit(fullfile(datafol, runf1, flist(i).name), 640, 480));
    im(28, 26:38) = 0; % A little burn
    % Get rid of noisy pixels fucking up ellipse fit
    im = haircut2D_R1(im);
    % Define signal region; everything else is background!
    sigregion = [50 50 550 400];
    bg = createBackground(im, sigregion,[],[],4);
    im = im-bg;
    imagesc(x_angle, y_angle, im)
    set(gca, 'YDir', 'normal', 'NextPlot', 'add');
    % Find contour of FWHM
    C = contourc(im, max(im(:))*[0.5 0.5]);
    x = C(1,:);
    y = C(2,:);
    lengthx = length(x);
    k = 1;
    %Remove points from your contour that are not actually part of the main
    %contour - this is necessary when looking at noisy data
    while k < lengthx
        if(abs(x(k) - mean(x)) > 2*std(x) || abs(y(k) - mean(y)) > 2*std(y))
            x(k) = [];
            y(k) = [];
            lengthx = lengthx - 1;
        end
        k = k+1;
    end
    plot(x_angle(round(x)), y_angle(round(y)), '-g');
    ell = fit_ellipse(x,y);
    R = [ cos(ell.phi) sin(ell.phi); -sin(ell.phi) cos(ell.phi) ];
    theta_r         = linspace(0,2*pi);
    ellipse_x_r     =  ell.X0 + ell.a*cos( theta_r );
    ellipse_y_r     =  ell.Y0 + ell.b*sin( theta_r );
    rotated_ellipse =  R*[ellipse_x_r;ellipse_y_r];
    %plot( rotated_ellipse(1,:),rotated_ellipse(2,:),'r' );
    plot( x_angle(round(rotated_ellipse(1,:))), y_angle(round(rotated_ellipse(2,:))),'r' );
    
    ell_centre = R*[ell.X0; ell.Y0];
    P0(1) = x_angle(round(ell_centre(1))); P0(2) = y_angle(round(ell_centre(2)));
    
    xlabel('$\theta_{hor}\:/ \:\mathrm{mrad}$')
    ylabel('$\theta_{ver}\:/ \:\mathrm{mrad}$')
    title(['$ \mathrm{Shot} \quad ' num2str(i) '$']);
    make_latex(811);
    setAllFonts(811, 20);
    %export_fig(fullfile(savfol, runf1, flist(i).name(1:end-4)), '-pdf', '-nocrop', 811);
    % And collate data
    imsum = imsum + im;
    IIpointingdata(i,1:2) = P0;
    IIpointingdata(i,3) = ell.a*cos(ell.phi)*xscale;
    IIpointingdata(i,4) = ell.b*cos(ell.phi)*yscale;
    IIpointingdata(i,5) = ell.phi;
    IIpointingdata(i,6) = sum(im(:));
    drawnow;
end
% IMPORTANT! We closed the iris on the lens after shot 6 or 7, from 1.8 to 6:

IIpointdata.pointingdata = IIpointingdata;
IIpointdata.x_angle = x_angle;
IIpointdata.y_angle = y_angle;
IIpointdata.imsum = imsum;
%save(fullfile(getDropboxPath, 'MATLAB', 'Experiments', '2013_ATA2', 'IIpointingdata'), 'IIpointdata');