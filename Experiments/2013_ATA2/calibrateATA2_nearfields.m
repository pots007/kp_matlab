% This script will look at the NF images of a few dates from ATA2 run and
% find the run and day average of the NF images. As there are different
% clips in the beam profile, it is impossible to find absolute energy
% calibration from this; however, assuming that we had the same amoutn of
% energy every day we can at least look at shot to shot variations.

% Load all the data into a massive struct
if ismac
    datfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Data';
end

TA2runs = [1001 2; 1001 3; 1002 1; 1002 2; 1002 3; 1003 1; 1003 3;...
    1004 1; 1004 2; 1010 1; 1016 1; 1016 2];

NFstruct = struct;

for i=1:size(TA2runs,1)
    runfol = fullfile(datfol, ['2013' num2str(TA2runs(i,1))], sprintf('2013%ir%03i', TA2runs(i,:)))
    nfiles = dir(fullfile(runfol, '*nf.raw'));
    tempims = zeros(480, 640, length(nfiles), 'uint16');
    for fil=1:length(nfiles)
        tempims(:,:,fil) = ReadRAW16bit(fullfile(runfol, nfiles(fil).name), 640, 480);
    end
    NFstruct(i).date = 20130000 + TA2runs(i,1);
    NFstruct(i).run = TA2runs(i,2);
    NFstruct(i).NFs = tempims;
end
disp('Now saving the data');
save(fullfile(getGDrive, 'Experiment', '2013_ATA2', 'analysis', 'NFstruct'), 'NFstruct');

%% Now make a mat file that has all the energies in!

% The mean maximum energy from the laser. The 0.625 factor is measured
% throughput from before compressor to TCC. This value will change most
% things.
load('ATA2_energy_calibshots')
allens = [ATA2_energy_shots.D0926r001; ATA2_energy_shots.D1003r003; ATA2_energy_shots.D1016r004];
E_max_av = mean(allens)*0.625
Eval = [];
%%
load(fullfile(getGDrive, 'Experiment', '2013_ATA2', 'analysis_old', 'NFstruct'));
hfig = figure(121); clf(121);
set(hfig, 'Color', 'w', 'Position', [200 500 1500 300]);
set(gca, 'nextPlot', 'add');
for i=1:length(NFstruct)
    tempims = NFstruct(i).NFs;
    Eval_ = []; Eval2_ = [];
    for fil=1:size(tempims,3)
        im = tempims(:,:,fil);
        bg = createBackground(im, [1 75 639 479-75], [], [], 4);
        im_ = im-bg;
        if max(im_(:))<10e3 || sum(sum(im(im==65532)))>3000
            Eval_(fil) = 0; 
        else
            Eval_(fil) = sum(im_(:)); 
        end
        Eval2_(fil) = sum(im_(:));
    end
    NFstruct(i).Eval = Eval_;
    Eval(i) = mean(Eval_(Eval_~=0));
    Eval2(i) = mean(Eval2_);
    imagesc((1:640)/640+(i-0.5), (1:480)*1e6+4.5e8, mean(tempims,3), 'Parent', gca);
    xlab{i} = sprintf('%ir%03i', TA2runs(i,:));
    drawnow;
end
h1 = plot(Eval, '-ok');
h2 = plot(Eval2, '-xr');
set(gca, 'XTick', 1:length(NFstruct), 'XTickLabel', xlab, 'Box', 'on', 'YLim', [4e8 2e9]);
legend([h1 h2], {'Without blank shots', 'With blank shots'}, 'Location', 'northwest');
make_latex(hfig); setAllFonts(hfig, 14);

% And now actually make the struct.
NFcalstruct = struct;
for i=1:length(NFstruct)
    NFcalstruct(i).date = NFstruct(i).date;
    NFcalstruct(i).run = NFstruct(i).run;
    NFcalstruct(i).calval = E_max_av/Eval(i);
    NFcalstruct(i).calibs = NFstruct(i).Eval.*NFcalstruct(i).calval;
end
%save(fullfile(getDropboxPath, 'MATLAB', 'Experiments', '2013_ATA2', 'NFenergycalib'), 'NFcalstruct');