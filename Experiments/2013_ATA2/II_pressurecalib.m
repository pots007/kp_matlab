if ismac
    load('/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Interferometry/mean_densities_initial.mat');
end
figure(809)
load('ATA2_2013');

logm = densities(1,:) == 20131010;
densities = densities(:,logm);
IIprescalib = zeros(2,size(densities,2));

for it=1:size(densities,2)
    IIprescalib(1,it) = ATA2_2013.D20131010.run(densities(2,it)).shot(densities(3,it)).pressure;
    IIprescalib(2,it) = densities(4,it);
end
logm = IIprescalib(2,:)>0.1 & IIprescalib(1,:)>0.1;
IIprescalib = IIprescalib(:,logm);
h1 = plot(IIprescalib(1,:), IIprescalib(2,:), 'xr', 'LineWidth', 2, 'MarkerSize', 15);
calib0 = IIprescalib;
if ismac
    load('/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Interferometry/mean_densities_autom.mat');
end

logm = densities(1,:) == 20131010;
densities = densities(:,logm);
IIprescalib = zeros(2,size(densities,2));

for it=1:size(densities,2)
    IIprescalib(1,it) = ATA2_2013.D20131010.run(densities(2,it)).shot(densities(3,it)).pressure;
    IIprescalib(2,it) = densities(4,it);
end
logm = IIprescalib(2,:)>0.1 & IIprescalib(1,:)>0.1;
IIprescalib = IIprescalib(:,logm);
hold on;
h2 = plot(IIprescalib(1,:), IIprescalib(2,:), 'ok', 'LineWidth', 2, 'MarkerSize', 15);
hold off;
calib1 = IIprescalib;

% The fitting part
% Fit a line to pressures below 12 bar, using both datasets
calib = [calib1 calib0];
logm = calib(1,:) < 12;
calibb = calib(:,logm);
IIfit = polyfit(calibb(1,:), calibb(2,:), 1);
hold on;
h3 = plot(calibb(1,:), polyval(IIfit, calibb(1,:)), 'g', 'LineWidth', 1.5);
hold off;
legend([h1 h2 h3], {'Data, autom y_m', 'Data, fixed y_m', 'Fit to p<12bar'}, 'Location', 'Northwest');
xlabel('Backing pressure / bar');
ylabel('Electron density / 10^{19} cm^{-3}');
setAllFonts(809, 16);
set(809, 'Color', 'w', 'Position', [100 100 800 600]);
%export_fig('/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Interferometry/II_pressurecalib', ...
%    '-nocrop', '-png', 809)
export_fig('/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Interferometry/II_pressurecalib', ...
    '-nocrop', '-pdf', 809)
title(sprintf('Fit: n_e = %2.5f*p + %2.5f\n', IIfit(1), IIfit(2)));