if (ismac)
    savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Espec2/';
    datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
elseif (ispc)
    savfol = './';
    datafolder = 'F:\Kristjan\Documents\Uni\Imperial\2013_ATA2\Dave\Data\';
end
%%
%II part
dlist = dir_names([datafolder '20131010/20131010r001/*Espec2.raw']);
shlist = zeros(size(dlist));
for i=1:length(dlist)
    im = ReadRAW16bit([datafolder '20131010/20131010r001/' dlist{i}], 640, 480);
    figure(89);
    imagesc(im);
    set(gca, 'CLim', [0 10000]);
    title(dlist{i});
    str = input(sprintf('Is %i it OK?',i), 's');
    if isempty(str)
        continue;
    else
        shlist(i) = i;
    end
end

%%
%SI part
dlist = dir_names([datafolder '20131004/20131004r002/*Espec2.raw']);
shlist = zeros(size(dlist));
for i=1:length(dlist)
    im = ReadRAW16bit([datafolder '20131004/20131004r002/' dlist{i}], 640, 480);
    figure(89);
    imagesc(im);
    set(gca, 'CLim', [0 10000]);
    title(dlist{i});
    str = input('Is it OK?', 's');
    if isempty(str)
        continue;
    else
        shlist(i) = i;
    end
end

%% Now look at charge on warped images for those shots
IIdata = zeros(length(IIbeams),2);
for i=1:length(IIbeams)
    shotn = str2double(IIbeams{i}(14:16));
    im = WarpAndRotTA2(20131010,1, shotn);
    IIdata(i,1) = sum(sum(abs(im.image)));
    IIdata(i,2) = ATA2_2013.D20131010.run(1).shot(shotn).pressure;
end
%%
SIdata = zeros(length(SIbeams),2);
for i=1:length(SIbeams)
    shotn = str2double(SIbeams{i}(14:16));
    im = WarpAndRotTA2(20131004, 2, shotn);
    SIdata(i,1) = sum(sum(abs(im.image)));
    SIdata(i,2) = ATA2_2013.D20131004.run(2).shot(shotn).pressure;
end
