savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/ForwardSpectrum/';
datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';

figure(120); clf;
set(120, 'Position', [100 100 900 1000], 'Color', 'w');%, 'PaperUnits', 'centimeters', 'PaperSize', [8.6 6]);

%   Self injection day
date = 20131004;
runn = 2;
nfiles = length(dir(sprintf('%s%i/%ir%03i/*Espec2.raw', datafolder, date, date, runn)));
datam = zeros(nfiles, 1024);
specm = zeros(nfiles, 2);
for i=1:nfiles
    disp(['Doing image ' num2str(i) ' out of ' num2str(nfiles)]);
    spec = fitsread(sprintf('%s/%i/%ir%03i/%ir%03is%03i_forward_spectrum.fit',...
        datafolder,date,date,runn,date,runn,i));
    spec = haircut2D_R1(spec, 1.05);
    cal = Calibrate_forward_spec_ATA2(false);
    BGtop = mean(spec(1:100,:),2); BGbot = mean(spec(200:250,:),2);
    BGfit = polyfit([1:100 200:250], [BGtop' BGbot'], 1);
    BG = polyval(BGfit, 1:size(spec,1));
    BG = repmat(BG', 1, size(spec,2));
    spec1 = spec-BG;
    locspec =  smooth(sum(spec1,1)/(max(sum(spec1,1))));
    datam(i,:) = locspec;
    rawfilen = sprintf('%i/%ir%03i/%ir%03is%03i_Espec2.raw', date,date,runn,date,runn,i);
    rawim = ReadRAW16bit([datafolder rawfilen], 640, 480);
    rawim = haircut2D_R1(rawim, 1.05);
    bg1 = rawim(1:250,:);
    bg2 = rawim(400:430,:);
    BG = mean([mean(bg1(:)) mean(bg2(:))]);
    rawimBG = rawim - BG;
    totQ = sum(sum(rawimBG(250:400,:)))*7.7; %in e-
    specm(i,1) = ATA2_2013.(['D' num2str(date)]).run(runn).shot(i).pressure;
    specm(i,2) = totQ*qe*1e12;
end
%% Plotting of the self injection day
figure(120); clf;
z = [2000 1500 1000 500 0];
shots{1} = 1:24;
shots{2} = 25:54;
shots{3} = 55:86;
shots{4} = 87:113;
shots{5} = 114:143;
locs = GetFigureArray(1,5,0.1,0.005,'down');
for k=1:5
    axs(k) = axes('Parent', 120, 'Position', locs(:,k)+[0.05 0 0 0]');
    temppres = specm(shots{k},:);
    tempdatam = datam(shots{k},:);
    [~,inds] = sort(temppres(:,1),1);
    imagesc(1:1024, temppres(inds,1), tempdatam(inds,:));
    
    set(axs(k), 'XTickLabel', [], 'CLim', [0 1], 'XTick', 50:100:1000,...
        'YAxisLocation', 'right', 'YDir', 'normal');
    qqs(k) = axes('Parent', 120, 'Position', [0.04 locs(2,k) 0.1 locs(4,k)]);
    plot(temppres(inds,2), temppres(inds,1), 'ok');
    set(qqs(k), 'YTickLabel', [], 'XTickLabel', [], 'XTick', 0:200:400,...
        'YLim', get(axs(k), 'YLim'), 'XLim', [-10 400]);
    ylabel(num2str(z(k)));
end
cal = Calibrate_forward_spec_ATA2(false);
set(axs(5),'XTickLabel', num2str(polyval(cal.coeffs, (50:100:1000)'), '%2.0f'));
xlabel(axs(5), 'Wavelength / nm');
set(qqs(5),'XTickLabel', num2str((0:200:400)'));
xlabel(qqs(5), 'Charge / pC');
title(axs(1), 'Self injection - 20131004, run 2');
setAllFonts(120, 20);
%export_fig([savfol 'Spectra_SI'], '-nocrop', '-pdf', 120);
%% Ionisation injection day
date = 20131010;
runn = 1;
nfiles = length(dir(sprintf('%s%i/%ir%03i/*Espec2.raw', datafolder, date, date, runn)));
datam = zeros(nfiles, 1024);
specm = zeros(nfiles, 2);
for i=1:nfiles
    if (i==91) continue; end;
    disp(['Doing image ' num2str(i) ' out of ' num2str(nfiles)]);
    spec = fitsread(sprintf('%s/%i/%ir%03i/%ir%03is%03i_forward_spectrum.fit',...
        datafolder,date,date,runn,date,runn,i));
    spec = haircut2D_R1(spec, 1.05);
    cal = Calibrate_forward_spec_ATA2(false);
    BGtop = mean(spec(1:100,:),2); BGbot = mean(spec(200:250,:),2);
    BGfit = polyfit([1:100 200:250], [BGtop' BGbot'], 1);
    BG = polyval(BGfit, 1:size(spec,1));
    BG = repmat(BG', 1, size(spec,2));
    spec1 = spec-BG;
    locspec =  smooth(sum(spec1(100:200,:),1)/(max(sum(spec1(100:200,:),1))));
    datam(i,:) = locspec;
    rawfilen = sprintf('%i/%ir%03i/%ir%03is%03i_Espec2.raw', date,date,runn,date,runn,i);
    rawim = ReadRAW16bit([datafolder rawfilen], 640, 480);
    rawim = haircut2D_R1(rawim, 1.05);
    bg1 = rawim(1:250,:);
    bg2 = rawim(400:430,:);
    BG = mean([mean(bg1(:)) mean(bg2(:))]);
    rawimBG = rawim - BG;
    totQ = sum(sum(rawimBG(250:400,:)))*7.7; %in e-
    specm(i,1) = ATA2_2013.(['D' num2str(date)]).run(runn).shot(i).pressure;
    specm(i,2) = totQ*qe*1e12;
end
%% Plotting of the ionisation injection day
figure(120); clf;
z = [2000 1500 1000 500 0];
shots{1} = 183:226;
shots{2} = 136:180;
shots{3} = 92:133;
shots{4} = 47:89;
shots{5} = 2:44;
locs = GetFigureArray(1,5,0.1,0.005,'down');
for k=1:5
    axs(k) = axes('Parent', 120, 'Position', locs(:,k)+[0.05 0 0 0]');
    temppres = specm(shots{k},:);
    tempdatam = datam(shots{k},:);
    [~,inds] = sort(temppres(:,1),1);
    imagesc(1:1024, temppres(inds,1), tempdatam(inds,:));
    
    set(axs(k), 'XTickLabel', [], 'CLim', [0 1], 'XTick', 50:100:1000,...
        'YAxisLocation', 'right', 'YDir', 'normal');
    qqs(k) = axes('Parent', 120, 'Position', [0.04 locs(2,k) 0.1 locs(4,k)]);
    plot(temppres(inds,2), temppres(inds,1), 'ok');
    set(qqs(k), 'YTickLabel', [], 'XTickLabel', [], 'XTick', 0:200:400,...
        'YLim', get(axs(k), 'YLim'), 'XLim', [-10 400]);
    ylabel(num2str(z(k)));
end
cal = Calibrate_forward_spec_ATA2(false);
set(axs(5),'XTickLabel', num2str(polyval(cal.coeffs, (50:100:1000)'), '%2.0f'));
xlabel(axs(5), 'Wavelength / nm');
set(qqs(5),'XTickLabel', num2str((0:200:400)'));
xlabel(qqs(5), 'Charge / pC');
title(axs(1), 'Ionisation injection - 20131010, run 1');
setAllFonts(120, 20);
%export_fig([savfol 'Spectra_II'], '-nocrop', '-pdf', 120);