ne = 9e17;%1.2e19;
a0 = 6;% 3.6;
Rbub = 2*sqrt(a0);
xmax = 2*sqrt(log(2*ncrit(800)/(3*ne))-1);
gam = 1+3*ne/(4*ncrit(800));
%in units of c/omega_p!!!!
if (Rbub>xmax); trap = 'trap'; else trap='not trap'; end;
fprintf('Bubble radius = %1.2f, xmax = %1.2f, should %s \n',...
    Rbub, xmax, trap);
w0 = 2*sqrt(a0)*c/omegapcalc(ne)
