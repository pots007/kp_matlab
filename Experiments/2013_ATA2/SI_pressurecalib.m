if ismac
    load('/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Interferometry/mean_densities_initial.mat');
end

load('ATA2_2013');
it = 1;

while densities(1,it)==20131004
    it = it+1;
end
SIprescalib = zeros(2,it);
it = 1;
while densities(1,it)==20131004
    SIprescalib(1,it) = ATA2_2013.D20131004.run(densities(2,it)).shot(densities(3,it)).pressure;
    SIprescalib(2,it) = densities(4,it);
    it = it+1;
end
logm = SIprescalib(2,:)>0.1;
SIprescalib = SIprescalib(:,logm);
figure;
h1 = plot(SIprescalib(1,:), SIprescalib(2,:), 'ok', 'LineWidth', 2, 'MarkerSize', 15);
hold on;
[presfit,errors] = polyfit(SIprescalib(1,:), SIprescalib(2,:), 1);
h2 = plot(SIprescalib(1,:), polyval(presfit, SIprescalib(1,:)), 'r', 'LineWidth', 1.5);
hold off;
xlabel('Backing pressure / bar');
ylabel('n_e / 10^{19} cm^{-3}');
legend([h1 h2], {'Data', 'Fit'}, 'Location', 'Northwest');
title('Fit for He in 3mm gas jet');
setAllFonts(gcf, 16);
set(gcf, 'Color', 'w');
export_fig('/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Interferometry/SI_pressurecalib', '-png',...
    '-nocrop', gcf);
fprintf('Fit: n_e = %2.5f*p + %2.5f\n', presfit(1), presfit(2));