% calibandWarpATA2_proper.m

% Input argument is date, as string, eg '20131004'

% This function will make the transform matrices for ATA2 images, but now
% not assuming the silly shadow is edge of screen. We first open the
% reference and find the edge of the ruler. This is a straight, horizontal
% line. We then FWB all espec shots from that day and say the peab of these
% in the vertical direction is our symmetry axis. We then find where the
% line of the edge of the ruler intersects with the symmetry axis. Then ,
% we find how much symmetrical vertical space is available at the low
% energy side, and construct a trapezoid from these.

function dat = calibandWarpATA2_proper(date_)

% Load data and get initial points
%date = '20131004';

load(fullfile(getGDrive, 'Experiment', '2013_ATA2', 'EspecAnalysis', 'EspecRefs', 'All_espec2_refs'));

logm = strcmp(refdata(:,1), date_);
if sum(logm)==0
    disp('No reference for this date at all! Available options are:');
    disp(refdata(:,1));
    error('Sorry.');
end
dat.warpdat.inputimage = refdata{logm,2};
refim = imrotate(refdata{logm,2}, 180);
dat.warpdat.rotimage = refim;
dat.warpdat.cropsize = [1 1 639 479];
dat.warpdat.cropimage = refdata{logm,2};
dat.path = ['/Users/kristjanpoder/Experiments/2013_ATA2/Data/References/' date_ '/espec2/'];
dat.fname = refdata{logm,3};
dat.ref = refdata{logm,2};
figure(123); clf(123);
imagesc(refim);
set(gca, 'YDir', 'normal', 'NextPlot', 'add');
disp('Please pick two points defining the edge of the ruler:');
xy = ginput(2);
%% Now find all shots from that day and find the peak in those

if ismac
    datfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Espec2';
end
flist = dir(fullfile(datfol, [date_ '*.raw']));
allshots = zeros(480,640, length(flist));
for i=1:length(flist)
    allshots(:,:,i) = ReadRAW16bit(fullfile(datfol, flist(i).name), 640, 480);
end

FWB = sum(sum(allshots,3),2); FWB = flipud(smooth(FWB));
plot(FWB*600/max(FWB), 1:480, '--k', 'LineWidth', 2);
disp('I''m now plotting a FHB image of all espec shots from that day');
disp('Select rectangle around the peak in the espec lienout values')
rr = getrect(gca);
logm = (1:480)>rr(2) & (1:480)<(rr(2)+rr(4));
[~,ind] = findpeaks(FWB(logm), 'MinPeakHeight', 0.9*max(FWB(logm)));
y0 = rr(2) + ind;
plot([1 640], y0*[1 1] , '-k', 'LineWidth', 2);
disp('Is this the correct place for the peak? It ought ot be...');

%% Now make the rectangle we're after!
% This is the crossing point for perspective
x0 = xy(1,1) - (xy(1,2)-y0)*(xy(2,1)-xy(1,1))/(xy(2,2)-xy(1,2));
% This is the maximum extent of our trapezoid in the vertical
ym_2 = floor(min([480-y0 y0-1]));
% This is the slope of the perspective line for maximum extent
k = ym_2/(640-x0);
ym_1 = ym_2 + k*640;
rectpoints = ([0.9 y0+ym_2; 1.1 y0-ym_2; 640 y0-ym_1; 639 y0+ym_1; 1 y0+ym_2]);
plot(rectpoints(:,1), rectpoints(:,2), '-m', 'LineWidth', 2);
dat.warpdat.q = getProjectiveTransformImage2(refim, rectpoints(1:4,1), rectpoints(1:4,2));
dat.warpdat.thet = 180;
% The screenheight is approximate!
dat.warpdat.screenheight = 60;
dat.Qcalib = 3.52e-8;
