% analyseATA2_SI_eprofile.m

% Look at 20131001 run2 eprofiles.

% Get reference first:
analyseATA2_eprofile_calib;
%%
if ismac
    datafol = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
    savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Eprofile';
    runf1 = '20131001/20131001r002';
end
cmap = brewermap(256, 'YlGnBu');
%% Do run 2.

flist = dir(fullfile(datafol, runf1, '*Eprofile.raw'));

figure(811); clf(811);
set(811, 'Color', 'w', 'Position', [100 100 800 600]);
colormap(811, cmap);
imsum = zeros(480, 640);
for i=1:length(flist)
    
    clf(811);
    im = double(ReadRAW16bit(fullfile(datafol, runf1, flist(i).name), 640, 480));
    im(28, 26:38) = 0;
    imsum = imsum + im;
    imagesc(x_angle, y_angle, im)
    set(gca, 'YDir', 'normal', 'NextPlot', 'add');
    hline = sum(im,1);
    hline = hline./max(hline)*10;
    hfit = fit(x_angle', hline', 'gauss2');
    vline = sum(im, 2);
    vline = vline./max(vline)*10;
    vfit = fit(y_angle', vline, 'gauss2');
    % Plot horizontal lines
    plot(x_angle, hline + y_angle(1), 'ok');
    plot(x_angle, feval(hfit, x_angle)+y_angle(1), '-r');
    plot(vline+x_angle(1), y_angle, 'ok');
    plot(feval(vfit, y_angle)+x_angle(1), y_angle, '-r');
    P0 = centroidlocationsmatrix(im);
    h(1) = plot(x_angle(round(P0(1))), y_angle(round(P0(2))), 'xk', 'MarkerSize', 14, 'LineWidth', 2);
    h(2) = plot(hfit.b1, vfit.b1, 'xr', 'MarkerSize', 14, 'LineWidth', 2);
    legend(h, {'$\mathrm{Intensity}$', '$\mathrm{Gaussian}$'}, 'Location', 'NorthEast')
    xlabel('$\theta_{hor}\:/ \:\mathrm{mrad}$')
    ylabel('$\theta_{ver}\:/ \:\mathrm{mrad}$')
    title(['$ \mathrm{Shot} \quad ' num2str(i) '$']);
    make_latex(811);
    setAllFonts(811, 20);
    export_fig(fullfile(savfol, runf1, flist(i).name(1:end-4)), '-pdf', '-nocrop', 811);
end