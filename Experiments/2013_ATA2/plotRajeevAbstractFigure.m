if ismac
    rootf = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/20131106/20131106r002/';
    Hef = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/20131004/20131004r002/';
    anfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/';
    trackfile = '/Users/kristjanpoder/Dropbox/ICT_release/version1.0/TA2_2013_LundMag8.mat';
elseif ispc
    rootf = 'E:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Dave\Data\20131106\20131106r002\';
    Hef = 'E:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Dave\Data\20131004\20131004r002\';
    anfol = 'E:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Dave\Analysis\';
    trackfile = 'E:\Kristjan\Documents\Uni\Dropbox\ICT_release\version1.0\TA2_2013_LundMag8.mat';
end
load(trackfile);
Elinewidth = 1.4;
Efit = fit(l(:,2), E_e_MeV, 'cubicspline');
imD = [0 6 16 26 36 56 76 96 126]; % 5cm line on screen is 6 in imD
scrnD = imD + (12/sin(0.25*pi));
pixels = [5 30 70 108 147 229 316 409 556];
pixfit = fit(pixels', scrnD', 'cubicspline');
pixEfit = Efit(pixfit(1:640));
[~,ind300] = min(abs(pixEfit-300));
[~,ind200] = min(abs(pixEfit-200));
[~,ind150] = min(abs(pixEfit-150));
[~,ind100] = min(abs(pixEfit-100));
filen = dir([rootf '*Espec2.raw']);
calib = ATA2calib('20131106');
%locs = GetFigureArray(1, 2, 0.04, 0.01, 'across');
locs = GetFigureArray(1, 2, [0.01 0.01 0.01 0.07], 0.01, 'across');
figure(100); clf;
ax1 = axes('Parent', 100, 'Position', locs(:,1), 'NextPlot', 'add', 'Box', 'on');
for i=1:length(filen)
    dataf = ReadRAW16bit([rootf filen(i).name], 640, 480);
    sgnal = dataf(200:400,:);
    bg = dataf(1:199,:);
    bgrow = sum(bg,1)/199;
    sgnal = sgnal - repmat(bgrow, size(sgnal,1),1);
    imagesc((i-1)*200:1:i*200, 1:640, (sgnal'));
end
plot([0 80*200], [ind300 ind300], '--k', 'LineWidth', Elinewidth);
plot([0 80*200], [ind200 ind200], '--k', 'LineWidth', Elinewidth);
plot([0 80*200], [ind150 ind150], '--k', 'LineWidth', Elinewidth);
plot([0 80*200], [ind100 ind100], '--k', 'LineWidth', Elinewidth);
set(ax1, 'XLim', [0 80*200], 'YLim', [1 640], 'YDir', 'reverse',...
    'CLim', [0 15000], 'XTick', [], 'YTick', [ind300 ind200 ind150 ind100],...
    'YTickLabel', [300 200 150 100]);
colormap(flipud(gray));
% Calibration: found rough distances for 


% and now He shots
pixels = [55 77 118 156 197 279 366 459 608];
pixfit = fit(pixels', scrnD', 'cubicspline');
pixEfit = Efit(pixfit(1:640));
[~,ind300] = min(abs(pixEfit-300));
[~,ind200] = min(abs(pixEfit-200));
[~,ind150] = min(abs(pixEfit-150));
[~,ind100] = min(abs(pixEfit-100));

ax2 = axes('Parent', 100, 'Position', locs(:,2), 'NextPlot', 'add', 'Box', 'on');
load ATA2_2013
Heshots = 0;
for i=1:144
    pres =  ATA2_2013.D20131004.run(2).shot(i).pressure;
    if pres>12 || pres<7
        continue;
    end
    Heshots = Heshots+1;
    dataf = ReadRAW16bit(sprintf('%s20131004r002s%03i_Espec2.raw', Hef, i), 640, 480);
    sgnal = dataf(200:400,:);
    bg = dataf(1:199,:);
    bgrow = sum(bg,1)/199;
    sgnal = sgnal - repmat(bgrow, size(sgnal,1),1);
    imagesc((Heshots-1)*200:1:Heshots*200, 1:640, (sgnal'));
end
plot([0 80*200], [ind300 ind300], '--k', 'LineWidth', Elinewidth);
plot([0 80*200], [ind200 ind200], '--k', 'LineWidth', Elinewidth);
plot([0 80*200], [ind150 ind150], '--k', 'LineWidth', Elinewidth);
plot([0 80*200], [ind100 ind100], '--k', 'LineWidth', Elinewidth);
set(ax2, 'XLim', [0 80*200], 'YLim', [1 640], 'YDir', 'reverse',...
    'CLim', [0 15000], 'XTick', [], 'YTick', [ind300 ind200 ind150 ind100],...
    'YTickLabel', [300 200 150 100], 'Box', 'off');
ylabel(ax1, 'Energy / MeV');
ylabel(ax2, 'Energy / MeV');

setAllFonts(100, 20);
set(100, 'Color', 'w');
export_fig([anfol '20131106r002_methane_He'], '-nocrop', '-png', 100);
export_fig([anfol '20131106r002_methane_He'], '-nocrop', '-pdf', 100);