% getATA2_2013ForwardSpec.m
%
% Returns the data for ATA2 2013 exit mode image
%
% Accepts either date, run, shot or correctly formatted shot string
% (YYYYMMDDrRRRsSSS)

function im = getATA2_2013Espec2RAW(date,run,shot)
if nargin==1 && ischar(date) && length(date)==16
    shotstr = date;
elseif nargin==3
    shotstr = sprintf('%ir%03is%03i', date, run, shot);
else
    trace = nan;
    return;
end
if ispc
    datapath = 'E:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Dave\Data';
elseif ismac
    datapath = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
end
fname = fullfile(datapath, shotstr(1:8), shotstr(1:12), [shotstr '_Espec2.raw']);
try
    im = double(ReadRAW16bit(fname, 640, 480));
catch err
    disp(err.message);
    im = zeros(640,480);
end
end