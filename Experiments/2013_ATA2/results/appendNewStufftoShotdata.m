fnew = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Espec2/withRAW+energy/20131004/mat/';
fold = '/Users/kristjanpoder/Google Drive/Experiment/2013TA2/analysis/Espec2/20131004/mat/';

flistnew = dir(fullfile(fnew, '*.mat'));
flistold = dir(fullfile(fold, '*.mat'));

for i=1:length(flistnew)
    snew = load(fullfile(fnew, flistnew(i).name));
    sold = load(fullfile(fold, flistnew(i).name));
    shotdata = snew.shotdata;
    if ~sold.shotdata.poo
        disp(flistnew(i).name);
        shotdata.max = sold.shotdata.max;
        shotdata.peak = sold.shotdata.peak;
        shotdata.poo = sold.shotdata.poo;
    end 
    save(fullfile(fold, flistnew(i).name), 'shotdata');
end