% Look thorugh all ATA2 Espec2 references and save it as a stack of some
% nature maybe

if ismac
    datfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/References';
elseif ispc
    datfol = 'E:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Dave\Data\References';
end

figure(122);
espec = 'eprofile';
dates = dir(fullfile(datfol));
refdata = cell(length(dates), 3);
for i=3:length(dates)
    flist = dir(fullfile(datfol, dates(i).name, espec, '*.raw'));
    if isempty(flist)
        flist = dir(fullfile(datfol, dates(i).name, espec, '*.tif'));
        if isempty(flist); continue; end
        refim = flipud(imread(fullfile(datfol, dates(i).name, espec, flist(1).name)));
        refim = 2^8*double(refim);
    else
        refim = ReadRAW16bit(fullfile(datfol, dates(i).name, espec, flist(1).name), 640, 480);
    end
    refdata{i,1} = dates(i).name;
    refdata{i,2} = refim;
    refdata{i,3} = flist(1).name;
    imagesc(refim); drawnow;
    pause(1);
end

logm = cellfun(@isempty, refdata(:,1));
refdata = refdata(~logm,:)
save(fullfile(getGDrive, 'Experiment', '2013_ATA2', 'EspecAnalysis', 'EspecRefs', ['All_' espec '_refs']), 'refdata');