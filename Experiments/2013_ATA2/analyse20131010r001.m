% This ought to look at the file analysed by especPeakFinding.m

% 20131010r001, ATA2 II

% z = -9964 is focused on the edge.

if ismac
    datafol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Espec2/withRAW+energy/20131010/mat';
    savfol = '/Users/kristjanpoder/Google Drive/Experiment/2013TA2/analysis/Espec2/20131010';
elseif ispc
    datafol = 'E:\Kristjan\Google Drive\Experiment\2013TA2\analysis\Espec2\20131010\mat'; 
    savfol = 'E:\Kristjan\Google Drive\Experiment\2013TA2\analysis\Espec2\20131010\';
end

load('20131010r001_zlocations');
load('ATA2_2013');
dat = ATA2_2013.D20131010.run(1);
II_calib = [0.09151 0.11374];

flist = dir(fullfile(datafol, '*.mat'));
fnames = {flist.name};
%
edata = zeros(10,length(fnames));
poo = zeros(size(fnames));
for i=1:length(fnames)
    fprintf('File %i out of %i\n', i, length(fnames));
    ll = load(fullfile(datafol, fnames{i}));
    poo(i) = ll.shotdata.poo;
    if poo(i); continue; end
    shotno = str2double(fnames{i}(5:7));
    edata(1,i) = shotno;
    edata(2,i) = dat.shot(shotno).pressure;
    edata(3,i) = polyval(II_calib, edata(2,i));
    edata(4,i) = zlocs(shotno);
    if ~isfield(ll.shotdata, 'max'); continue; end;
    edata(5,i) = ll.shotdata.max.low;
    edata(6,i) = ll.shotdata.max.val;
    edata(7,i) = ll.shotdata.max.high;
    edata(8,i) = ll.shotdata.peak.low;
    edata(9,i) = ll.shotdata.peak.val;
    edata(10,i) = ll.shotdata.peak.high;
    edata(11,:) = ll.shotdata.totQ;
end
poo = logical(poo);
edata = edata(:, ~poo);

%% And now for plotting all kinds of things

% First just the maximum energy as a function of plasma density
hfig = 77;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
% errorbar for simplistic analysis
h1 = errorbar(edata(3,:), edata(6,:), edata(5,:), edata(7,:), 'o', 'LineWidth', 1.);
% Try rolling average now
[~,sind] = sort(edata(3,:));
npnts = 6;
avg_ens = tsmovavg(edata(6,sind), 's', npnts,2);
hold on;
h2 = plot(edata(3,sind), avg_ens, '-k', 'LineWidth', 3);
% And add the dephasing length!
nes = (0.6:0.01:2)*1e19;
dW = ncrit(800)./(nes)*0.511*2;
h3 = plot(nes*1e-19, dW, '--r', 'LineWidth', 3);
hold off;
xlabel('Plasma density / $10^{19} \: \mathrm{cm}^{-3}$');
ylabel('Maximum energy / MeV');
legend([h1 h2 h3], {'Data', [num2str(npnts) ' point moving average'], '$\Delta W = 2\gamma^2 m c^2$'},...
    'Location', 'Northeast');set(gca, 'Position', [0.12 0.13 0.85 0.85], 'Box', 'on', 'LineWidth', 2);
make_latex(hfig);
setAllFonts(hfig,20);
%export_fig(fullfile(savfol, 'MaxEn_ne'), '-pdf', '-nocrop', hfig);


%% Then, the peak energy as a function of plasma density
hfig = 77;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
errorbar(edata(3,:), edata(6,:), edata(5,:), edata(7,:), 'o', 'LineWidth', 1.5);
xlabel('Plasma density / $10^{19} \: \mathrm{cm}^{-3}$');
ylabel('Maximum energy / MeV');
set(gca, 'Position', [0.12 0.13 0.85 0.85], 'Box', 'on', 'LineWidth', 2);
make_latex(hfig);
setAllFonts(hfig,20);
export_fig(fullfile(savfol, 'MaxEn_ne'), '-pdf', '-nocrop', hfig);


%% Now maximum energy for different z positions

hfig = 77;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
set(gca, 'NextPlot', 'add');
% Fix the odd 1 micron error
edata(4,edata(4,:)==-8967) = -8966;
z_s = unique(edata(4,:));
colz = 'kbrgm';
for i=1:5
    logm = edata(4,:)==z_s(i);
    dens = edata(3:7,logm);
    [~,inds] = sort(dens(1,:));
    hh(i) = plot(dens(1,inds), tsmovavg(dens(4,inds), 's', 3, 2), ['d--' colz(i)]);
    legentry{i} = sprintf('z = %1.0f', abs(round(z_s(i)+7964)));
end
%errorbar((edata(4,:))+9964, edata(6,:), edata(5,:), edata(7,:), 'o');
nes = (0.6:0.01:2)*1e19;
dW = ncrit(800)./(nes)*0.511*2;
hh(6) = plot(nes*1e-19, dW, '--r', 'LineWidth', 3);
legentry{6} = '$\Delta W = 2\gamma^2 mc^2$';
legend(hh, legentry, 'Location', 'northeast');
set(hh(1:5), 'LineWidth', 1.5);
set(gca, 'Position', [0.12 0.13 0.85 0.8], 'Box', 'on', 'LineWidth', 2);
xlabel('Plasma density / $10^{19} \: \mathrm{cm}^{-3}$');
ylabel(gca, 'Maximum energy / MeV');
make_latex(hfig);
setAllFonts(hfig,20);
export_fig(fullfile(savfol, 'MaxEn_zs_ne'), '-pdf', '-nocrop', hfig);