function cal = Calibrate_forward_spec_ATA2(plotflag)
plotflag = false;
hglamp = fitsread('20130820_mercurylamp.fits');
if plotflag
    figure(909);
    imagesc(hglamp);
end
hgline = double(hglamp(100, :));
if plotflag
    cla;
    plot(hgline)
    [pks indcs] = findpeaks(hgline, 'THRESHOLD', 15000);
    hold on;
    plot(indcs, zeros(1,length(indcs)), '*');
    hold off;
end
%pixels = [118 189 195 245 375 444 500 504 835 927];
%lambda = [365.02 404.66 407.78 435.83 507.3 546.07 576.96 579.07 760.09 811.53];
%pixels = [118 189 195 245 375 444 500 504 835 927];
%pixels = [47 118 124 175 305 375 431 435 766 859];
pixels = [11 82 88 138 268 338 394 398 728 820 1005];
lambda = [365.02 404.66 407.78 435.83 507.3 546.07 576.96 579.07 760.09 811.53 912.30];

coef = polyfit(pixels, lambda, 3);
xax = 1:1:length(hgline);
cal.x = xax;
cal.lambda = polyval(coef, xax);
cal.coeffs = coef;
if plotflag
    figure(909)
    plot(polyval(coef, xax), hgline)
end