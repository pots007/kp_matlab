% Line locations from 
% http://www.cs.rochester.edu/~nelson/cesium/cesium_spectrum.html
%top = [703 491];
%bottom = [694 2067];
%thet = atan(top(1) - bottom(1))/(bottom(2) - top(2));
%thet = thet*180/pi;
%Celamp = imread('Cesium_lamp.FIT');
%Celamp = imread('IMG54.FIT');
%Celamp = imrotate(Celamp, thet);
hglamp = fitsread('C:\Data\2013_ATA2_Pattathil\Calibrations\20130820_mercurylamp.fits');
imagesc(hglamp);
hgline = double(hglamp(100, :));
cla;
plot(hgline)
[pks indcs] = findpeaks(hgline, 'THRESHOLD', 15000);
hold on;
plot(indcs, zeros(1,length(indcs)), '*');
hold off;
%pixels = [118 189 195 245 375 444 500 504 835 927];
%lambda = [365.02 404.66 407.78 435.83 507.3 546.07 576.96 579.07 760.09 811.53];
%pixels = [118 189 195 245 375 444 500 504 835 927];
%pixels = [47 118 124 175 305 375 431 435 766 859];
pixels = [11 82 88 138 268 338 394 398 728 820 1005];
lambda = [365.02 404.66 407.78 435.83 507.3 546.07 576.96 579.07 760.09 811.53 912.30];

coef = polyfit(pixels, lambda, 3);
xax = 1:1:length(hgline);
figure(2)
plot(polyval(coef, xax), hgline)