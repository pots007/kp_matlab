%[nums, texts, raws] = xlsread('F:\Kristjan\Documents\Uni\Imperial\2013_ATA2\Dave\Shotsheets\20131108.xlsx');
rawdat = raws(3:end,:); %filter out the top text rows
dates = unique(cell2mat(rawdat(:,1)));
dates = dates(~isnan(dates));
for i=1:length(dates)
    ATA2_2013.(sprintf('D%i',dates(i))) = struct;
end
for i=1:length(dates)
    i
    lmat = cell2mat(rawdat(:,1)) == dates(i);
    datedata = rawdat(lmat,:);
    runs = unique(cell2mat(datedata(:,2)));
    for k=1:length(runs) %preallocate the structures
        ATA2_2013.(sprintf('D%i',dates(i))).run(k) = struct;
    end
    for k=1:length(runs)
        lmat = cell2mat(datedata(:,2)) == runs(k);
        rundata = datedata(lmat,:);
        for l=1:sum(lmat) %preallocate the structure for shots;
            ATA2_2013.(sprintf('D%i',dates(i))).run(k).shot(l) = struct; 
        end
        for l=1:sum(lmat)
            %Actually fill in the data.
            ATA2_2013.(sprintf('D%i',dates(i))).run(k).shot(l).pressure = rundata{l,9};
            ATA2_2013.(sprintf('D%i',dates(i))).run(k).shot(l).gas = rundata{l,7};
            ATA2_2013.(sprintf('D%i',dates(i))).run(k).shot(l).energy = GetATA2Energy(dates(i),k,l);
        end
    end
end

save('F:\Kristjan\Documents\Uni\Dropbox\MATLAB\experiments\2013_ATA2\ATA2_2013.mat', 'ATA2_2013');