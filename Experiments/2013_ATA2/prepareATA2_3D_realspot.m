% This file ought to make a 3D electric field for EPOCH to be able to load!
% It takes spatial data from a measured ATA2 focal spot, and temporal from
% a measured SPIDER trace.

% This script will create a compound image of a few focal spots. The point
% is that this ought to be a nice, spatially smooth intensity pattern,
% readily available to be put into EPOCH.
focalSpotTotalSumATA2;
% spatialaxis is axis
% spotim is E-field profile!

% Seems like I've lost all the SPIDER data for the pulse...
% So we'll have to use a Gaussian temporal profile then :(
taufwhm = 37;
tauomega = taufwhm/sqrt(2*log(2));
I0 = 1.7e18;

%% Now for the resolutions:
xmax = 50; xmin = -15;
ymin = -50; ymax = -ymin;
x0 = 20;
k0 = 2*pi/0.8;
nx = ceil(30*(xmax-xmin)/0.8);
ny = ceil(3*(ymax-ymin)/0.8);
% Checked these against EPOCH values and it seems to be accurate :)
% Now make the transverse grid:
Eamp = sqrt(2*I0*1e4/(epsilon0*c));
x_grid = linspace(xmin, xmax, nx);
y_grid = linspace(ymin, ymax, ny);
[Z,Y] = meshgrid(y_grid, y_grid);
[Zin,Yin] = meshgrid(spatialaxis, spatialaxis);
E_grid = interp2(Zin, Yin, spotim, Z, Y, 'natural');
Ez = zeros(nx, ny, ny);
% And now make the Ez field
for i=1:nx
    tval = exp(-(x_grid(i)-x0)^2/(tauomega/3)^2);
    Ez(i,:,:) = E_grid*tval*Eamp*sin(x_grid(i)*k0);
end

%%
% Write the files straight into the correct place
fol = '/Volumes/work/Fieldtest/data';
fid = fopen(fullfile(fol, 'Ez.dat'), 'w');
fwrite(fid, Ez, 'double');
fclose(fid);

%% Make and write By
for i=1:nx
    tval = exp(-(x_grid(i)-x0)^2/(tauomega/3)^2);
    Ez(i,:,:) = E_grid/c*tval*Eamp*cos(x_grid(i)*k0);
end
fid = fopen(fullfile(fol, 'By.dat'), 'w');
fwrite(fid, Ez, 'double');
fclose(fid);
