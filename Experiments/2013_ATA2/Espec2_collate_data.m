rfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Espec2/withRAW+energy/';
eldataf = importdata([rfol '/20131004/beams/eldataf.txt']);

%% SI data collection
% Col 1 - pressure
% Col 2 - ne (from calibration!)
% Col 3 - zpos
% Col 4 - totQ
% Col 5 - maxen
% Col 6 - peaken

% z-positions from before
zpositions = zeros(size(eldataf,1));
zpositions(1:24) = 2000;
zpositions(25:54) = 1500;
zpositions(55:86) = 1000;
zpositions(87:113) = 500;
zpositions(114:143) = 0;

beams = dir_names([rfol '20131004/beams/*.png']);
SIdata = zeros(length(beams), 5);
for i=1:length(beams)
    shotn = str2double(beams{i}(5:7));
    SIdata(i,1) = eldataf(shotn,4);
    SIdata(i,2) = SIdata(i,1)*0.08726 + 0.15267;
    SIdata(i,3) = zpositions(shotn);
    SIdata(i,4) = eldataf(shotn,3);
    SIdata(i,5) = eldataf(shotn,1);
    SIdata(i,6) = eldataf(shotn,2);
end
save([getDropboxPath '/MATLAB/Experiments/2013_ATA2/SIelectrondata.mat'], 'SIdata');
