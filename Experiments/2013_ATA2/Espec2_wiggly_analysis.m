if (ismac)
    savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Espec2/';
    datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
elseif (ispc)
    savfol = './';
    datafolder = 'F:\Kristjan\Documents\Uni\Imperial\2013_ATA2\Dave\Data\';
end
date = 20131010;
figure(121); clf;
divlims = [-10 10];
enlims = [60 250];
%II part
runn = 1;
load('invfire.mat');
load('II_shots_pressures.mat');
load('mean_densities');
divs1 = zeros(1093,size(press,2));
divsII = zeros(5,size(press,2));
figure(5); clf;
locs = GetFigureArray(11,6,0.01, 0.01, 'down');
for i=1:size(press,2)   
    disp(['Doing image ' num2str(i) ' shot ' num2str(press(1,i))]);


    imag = WarpAndRotTA2(date,runn,press(1,i));
    enax = imag.enaxis;
    divax = imag.pixdiv*imag.yaxis;
    co2disp = enax(1:end-1) - enax(2:end); %To correct for dispersion
    co2disp = repmat(co2disp, length(divax),1);
    linen = linspace(min(enax), max(enax), 1000); %Interpolate to linear grid to avoid pcolor
    image = imag.image(:,1:end-1)./co2disp;
    linimag = zeros(size(image,1),1000);
    for l=1:size(image,1)
        linimag(l,:) = interp1(enax(1:end-1), image(l,:), linen, 'pchip');
    end
    divs1(:,i) = -sum(linimag,2);
    divs1(1:182,i) = 0; % To take the weird background off! hopefully is enough
    if (i==18) divs1(1:300) = 0; end; % Needed for that shot
    divsII(1,i) = press(2,i); %Shot pressure
    divsII(2,i) = widthfwhm(divs1(:,i)')*imag.pixdiv; %Width of horizontal integrated
    lmat = densities(1,:) == date;
    temp = densities(2:5,lmat);
    lmat = temp(1,:) == runn;
    temp = temp(2:4,lmat);
    lmat = temp(1,:) == press(1,i);
    temp = temp(2:3,lmat);
    if (sum(lmat) ~= 0)
        divsII(4,i) = temp(1); %plasma density
        divsII(5,i) = temp(2); %plasma density error
    end
    ax(i) = axes('Parent', 5, 'Position', locs(:,i));
    imagesc(linen, divax, -linimag);
    set(ax(i), 'XTick', [], 'YTick', []);
    text('Units', 'normalized', 'position', [0.4 0.4], 'String', [num2str(divsII(4,i)) ',' num2str(press(1,i))]);
    drawnow;
    [~,tedge] = min(abs(divlims(1)-divax));
    [~,bedge] = min(abs(divlims(2)-divax));
    [~,ledge] = min(abs(enlims(1)-linen));
    [~,redge] = min(abs(enlims(2)-linen));
    subim = -linimag(tedge:bedge,ledge:redge);
    subim = haircut2D_R1(subim, 1.05);
    meds = zeros(1,size(subim,2));
    for k=1:size(subim,2)
        %meds(k) = divlims(1) + imag.pixdiv*(1:size(subim,1))*subim(:,k)/sum(subim(:,k));
        [mama,ind] = max(smooth(subim(:,k)));
        meds(k) = divlims(1) + imag.pixdiv*ind;
        if (mama<0.2*max(subim(:))) meds(k) = nan; end;
    end
    hold on;
    plot(linspace(enlims(1), enlims(2), size(subim,2)),meds, 'k');
    hold off;
    meds = meds(~isnan(meds));
    if (sum(subim(:))>3e7)
        divsII(3,i) = var(meds); %wiggliness
        if (press(1,i)==85 || press(1,i)==124 || press(1,i)==77 || press(1,i)==102 || press(1,i)==34 || press(1,i)==168|| press(1,i)==217 || press(1,i)==193)
            divsII(3,i) = nan; 
        end
    else
        divsII(3,i) = nan;
    end
end
linkaxes(ax, 'xy');
set(ax(1), 'XLim', enlims, 'YLim', divlims);
%figure(121); plot(divsII(2,:),divsII(3,:))
%%
%SI part
date = 20131004;
runn = 2;
load('invfire.mat');
load('SI_shots_pressures.mat');
divs = zeros(1137,size(press,2));
divsSI = zeros(5,size(press,2));
figure(46); clf;
locs = GetFigureArray(8,5,0.01, 0.01, 'down');
for i=1:size(press,2)   
    disp(['Doing image ' num2str(i)]);
    imag = WarpAndRotTA2(date,runn,press(1,i));
    enax = imag.enaxis;
    divax = imag.pixdiv*imag.yaxis;
    co2disp = enax(1:end-1) - enax(2:end); %To correct for dispersion
    co2disp = repmat(co2disp, length(divax),1);
    linen = linspace(min(enax), max(enax), 1000); %Interpolate to linear grid to avoid pcolor
    image = imag.image(:,1:end-1)./co2disp;
    linimag = zeros(size(image,1),1000);
    for l=1:size(image,1)
        linimag(l,:) = interp1(enax(1:end-1), image(l,:), linen, 'pchip');
    end
    divs(:,i) = sum(linimag,2);
    divsSI(1,i) = press(2,i);
    divsSI(2,i) = widthfwhm(divs(:,i)')*imag.pixdiv;
    divsSI(3,i) = press(1,i);
    lmat = densities(1,:) == date;
    temp = densities(2:5,lmat);
    lmat = temp(1,:) == runn;
    temp = temp(2:4,lmat);
    lmat = temp(1,:) == press(1,i);
    temp = temp(2:3,lmat);
    if (sum(lmat) ~= 0)
        divsSI(4,i) = temp(1);
        divsSI(5,i) = temp(2);
    end
        
    ax1(i) = axes('Parent', 46, 'Position', locs(:,i));
    h = imagesc(linen, divax, linimag);
    set(ax1(i), 'XTick', [], 'YTick', []);
    text('Units', 'normalized', 'position', [0.4 0.4], 'String',  [num2str(divsSI(4,i)) ',' num2str(press(1,i))]);
    drawnow;
    [~,tedge] = min(abs(divlims(1)-divax));
    [~,bedge] = min(abs(divlims(2)-divax));
    [~,ledge] = min(abs(enlims(1)-linen));
    [~,redge] = min(abs(enlims(2)-linen));
    subim = linimag(tedge:bedge,ledge:redge);
    subim = haircut2D_R1(subim, 1.05);
    meds = zeros(1,size(subim,2));
    for k=1:size(subim,2)
        %meds(k) = divlims(1) + imag.pixdiv*(1:size(subim,1))*smooth(subim(:,k))/sum(smooth(subim(:,k)));
        [mama,ind] = max(smooth(subim(:,k)));
        meds(k) = divlims(1) + imag.pixdiv*ind;
        if (mama<0.2*max(subim(:))) meds(k) = nan; end;
    end
    hold on;
    plot(linspace(enlims(1), enlims(2), size(subim,2)),meds, 'k');
    hold off;
    %filter out the nans
    meds = meds(~isnan(meds));
    if (sum(subim(:))>5.5e7)
        divsSI(3,i) = var(meds);
    else
        divsSI(3,i) = nan;
    end
end
linkaxes(ax1, 'xy');
set(ax1(1), 'XLim', enlims, 'YLim', divlims);
%%
figure(121); 
hplot = plot(divsII(1,:), divsII(3,:), 'kx', divsSI(1,:), divsSI(3,:), 'ro');
set(hplot, 'MarkerSize', 10, 'LineWidth', 1.5)
xlabel('Backing pressure (bar)');
ylabel('Stdev of slice transverse peak location (a.u.)');
legend(hplot, {'Ionisation injection', 'Self-injection'});
setAllFonts(121,20);

figure(122);
hplot = plot(divsII(4,:), divsII(3,:), 'kx', divsSI(4,:), divsSI(3,:), 'ro');
set(hplot, 'MarkerSize', 10, 'LineWidth', 1.5)
xlabel('n_e (10^{19} cm^{-3})');
%ylabel('Stdev of slice transverse peak location (a.u.)');
ylabel('\sigma_{p_y} (a.u.)');
legend(hplot, {'Ionisation injection', 'Self-injection'});
setAllFonts(122,20);
set(gca, 'XLim', [0.25 2]);

figure(123); cla;
h1 = errorbarxy(divsII(4,:), divsII(3,:), divsII(5,:), [], {'kx', 'k', 'k'});
set(h1.hMain, 'MarkerSize', 10, 'LineWidth', 1.5)
hold on;
h2 = errorbarxy(divsSI(4,:), divsSI(3,:), divsSI(5,:), [], {'ro', 'r', 'r'});
set(h2.hMain, 'MarkerSize', 10, 'LineWidth', 1.5)
set(gca, 'XLim', [0 2], 'YLim', [0 8]);
hold off;
xlabel('n_e (10^{19} cm^{-3})');
ylabel('Stdev of slice transverse peak location (a.u.)');
legend([h1.hMain h2.hMain], {'Ionisation injection', 'Self-injection'});
setAllFonts(123,20);

%export_fig([savfol 'Espec2_wiggly1'], '-pdf', '-transparent', '-nocrop', 121);
%export_fig([savfol 'Espec2_wiggly2'], '-pdf', '-transparent', '-nocrop', 122);
%export_fig([savfol 'Espec2_wiggly3'], '-pdf', '-transparent', '-nocrop', 123);