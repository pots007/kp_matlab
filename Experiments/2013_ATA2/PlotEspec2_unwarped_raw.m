savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Espec2/withRAW/';
datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
date = 20131004;
figure(120); clf;
set(120, 'Position', [100 100 900 1000], 'Color', 'w');%, 'PaperUnits', 'centimeters', 'PaperSize', [8.6 6]);
%axs = zeros(4,1);
divlims = [-30 30];
runn = 2;
load('invfire.mat');
if ~exist([savfol num2str(date)], 'dir')
    mkdir([savfol num2str(date)]);
    mkdir([savfol num2str(date) '/png']);
    mkdir([savfol num2str(date) '/fig']);
end
nfiles = length(dir(sprintf('%s%i/%ir%03i/*Espec2.raw', datafolder, date, date, runn)));
for i=1:nfiles
    if (i==91) continue; end;
    figure(120); clf;
    disp(['Doing image ' num2str(i) ' out of ' num2str(nfiles)]);
    ax1 = axes('Position', [0.3 0.575 0.6 0.4]);
    imag = WarpAndRotTA2(date,runn,i);
    enax = imag.enaxis;
    divax = imag.pixdiv*imag.yaxis;
    co2disp = enax(1:end-1) - enax(2:end); %To correct for dispersion
    co2disp = repmat(co2disp, length(divax),1);
    linen = linspace(min(enax), max(enax), 1000); %Interpolate to linear grid to avoid pcolor
    image = imag.image(:,1:end-1)./co2disp;
    linimag = zeros(size(image,1),1000);
    for l=1:size(image,1)
        linimag(l,:) = interp1(enax(1:end-1), image(l,:), linen, 'pchip');
    end
    imagesc(linen, divax, abs(linimag), 'Parent', ax1);
    set(ax1, 'XLim', [min(linen) 300], 'CLim', [0 2e4], 'YTickLabel', [],...
        'YDir', 'normal', 'YLim', divlims);
    xlabel('Energy / MeV');
    cb = colorbar;
    set(cb, 'Position', [0.91 0.575 0.02 0.4]);
    ax2 = axes('Position', [0.1 0.575 0.19 0.4]);
    plot(sum(linimag,2), divax);
    set(ax2, 'YLim', divlims);
    ylabel('Divergence / mrad');
    % and now the raw image with charge calibration done
    ax3 = axes('Parent', 120, 'Position', [0.3 0.07 0.6 0.4]);
    rawfilen = sprintf('%i/%ir%03i/%ir%03is%03i_Espec2.raw', date,date,runn,date,runn,i);
    rawim = ReadRAW16bit([datafolder rawfilen], 640, 480);
    %rawim = haircut2D_R1(rawim, 1.05);
    % Added Feb 2016
    if date==20131004
        filen = sprintf('%i/%ir%03i/%ir%03is%03i_Espec2.raw', date,date,2,date,2,1);
        dat = ReadRAW16bit([datafolder filen], 640, 480);
        logm = dat>5000;
        rawim(logm) = 0;
        sum(logm(:))
    end
    bgroi = [1 1 639 199];
    BG = createBackground(rawim, [1 200 639 200], bgroi, [], 4);
    % The weird looking slope at the edge:
    %rawim(450:end,:) = mean(mean(BG(1:200,:)));
    rawimBG = rawim - BG;
    % End of new stuff, Feb 2016
    imagesc(rawimBG)
    set(ax3, 'XDir', 'reverse', 'XTick', [], 'YTick', [], 'NextPlot', 'add');
    %bg1 = rawim(1:250,:); 
    %rectangle('Position', [1 1 640 250]);
    %bg2 = rawim(400:430,:);
    %rectangle('Position', [1 400 640 30]);
    %BG = mean([mean(bg1(:)) mean(bg2(:))]);
    %rawimBG = rawim - BG;
    totQ = sum(sum(rawimBG(200:400,:)))*1.55; %WAS 7.7;, edit on 09/04/15
    hbeam = rectangle('Position', [1 200 640 200], 'EdgeColor', 'r');
    title(sprintf('Total charge Q = %1.3f pC', totQ*qe*1e12));
    cb2 = colorbar;
    set(cb2, 'Position', [0.91 0.07 0.02 0.4]);
    setAllFonts(120, 20);
    colormap(iijcm);
    
    export_fig([savfol sprintf('%i/png/%03is%03i', date, runn, i)], '-nocrop', '-png', 120);
    saveas(120, [savfol sprintf('%i/fig/%03is%03i.fig', date, runn, i)]);
end