% gives energy as a function of pixel for ATA2 2013 experiment.
% Input argument is date.
% output: pixels; distance from axis; electron energy

function enpix = ATA2calib(date)
%find the needed trackign output
if (ismac)
    trackfile = '/Users/kristjanpoder/Dropbox/ICT_release/version1.0/TA2_2013_LundMag8.mat';
elseif (ispc)
    trackfile = 'E:\Kristjan\Documents\Uni\Dropbox\ICT_release\version1.0\TA2_2013_LundMag8.mat'; 
end
load(trackfile);
imD = [0 6 16 26 36 56 76 96 126];
scrnD = imD + (12/sin(0.25*pi));
Efit = fit(l(:,2), E_e_MeV, 'cubicspline');
switch date
    case '20131002'
        pixels = [54 75 114 152 192 276 364 458 609];
    case '20131003'
        pixels = [54 77 117 156 196 279 367 460 609];
    case '20131004'
        pixels = [54 77 117 156 196 279 367 460 609];
    case '20131008'
        pixels = [53 77 117 156 196 279 366 459 609];
    case '20131009'
        pixels = [54 77 117 156 196 279 367 459 609];
    case '20131010'
        pixels = [33 60 99 138 179 264 353 445 595];
    case '20131015'
        pixels = [22 46 84 124 165 246 334 428 575];
    case '20131016'
        pixels = [7 32 73 113 154 240 329 426 578];
    case '20131029'
        pixels = [0 12 51 91 132 216 305 400 552];
    case '20131101'
        pixels = [2 24 62 102 142 224 311 404 551];
    case '20131105'
        pixels = [7 30 67 107 146 229 316 408 557];
    case '20131106'
        pixels = [6 30 68 107 146 229 316 408 557];
    otherwise
        disp('No calibration = no interesting data on this day!');
        pixels = [];
end
if (isempty(pixels))
    enpix = [];
    return;
else
    pixaxis = 1:1:640;
    daxis = interp1(pixels, scrnD, pixaxis, 'spline');
    %daxis(isnan(daxis))=0;
    enaxis = Efit(daxis);
    enpix = [pixaxis; daxis; enaxis'];
end

