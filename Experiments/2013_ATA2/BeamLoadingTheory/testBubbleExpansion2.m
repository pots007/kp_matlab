% Try to simulate bubble expansion due to laser a0 increase.

ne = 1e19;
Constants;

global fs;
fs.a0 = 4;
fs.xi0L = 2;
fs.w0 = 1;
fs.tau0 = 1.5;
%% Let's first look at what a sensible offset for initial position is
hfig = figure(77); clf(hfig);
set(gca, 'NextPlot', 'add', 'Box', 'on');
xi = 0:0.01:12;
opts = odeset('RelTol', 1e-9, 'AbsTol', 5e-9, 'NonNegative', 1);
r0s = [1e-4 5e-4 1e-3 5e-3 1e-2 5e-2 1e-1];
for i=1:length(r0s)
    tic
    [t1, rb1] = ode45(@Luetal_Eq46laser, [0 12], [r0s(i) 0], opts);
    toc
    hh(i) = plot(t1, rb1(:,1), '-', 'LineWidth', 2, 'Tag', ['$r_0 = ' num2str(r0s(i)) '$']);
    drawnow;
end
legend(hh, get(hh, 'Tag'));

% so let's use initial offset of r_0=0.01;

%% Now do the bubble expansion test!
hfig = figure(77); clf(hfig);
set(gca, 'NextPlot', 'add', 'Box', 'on');
xi = 0:0.01:12;
opts = odeset('RelTol', 1e-9, 'AbsTol', 5e-9, 'NonNegative', 1);
a0s = 4:0.4:4; hh = [];
tau0 = 1.5; a0 = 4;
for i=1:length(a0s)
    tic
    fs.a0 = a0s(i);
    fs.tau0 = tau0*(a0/a0s(i))^2;
    [t1, rb1] = ode45(@Luetal_Eq46laser, [0 12], [0.01 0], opts);
    toc
    hh(i) = plot(t1, rb1(:,1), '-', 'LineWidth', 2, 'Tag', ['$a_0 = ' num2str(a0s(i)) '$']);
    drawnow;
end
legend(hh, get(hh, 'Tag'));
make_latex(hfig); setAllFonts(hfig, 14)