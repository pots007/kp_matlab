% This is coding up Eq 46 from W Lu PoP, but with ponderomotive term
% analysitcally calculated rather that numerically evaluated.

function yprime = Luetal_Eq46laser2(x,y)
yprime = zeros(2,1);
yprime(1) = y(2);
global fs;
%load('fs.mat');
a0 = fs.a0; xi0 = fs.xi0L; w0 = fs.w0; tau0 = fs.tau0;
a0temp = a0*exp(-(x-xi0)^2/(tau0^2));
%a0 = a0temp*exp(-(y(1)/w0)^2);
a0 = @(rb) a0temp*exp(-(rb^2/w0^2));
%da0 = -2*y(1)/w0^2*a0;
Ds = 0.1;
DL = 0.5;
%alpha = @(rb) (Ds*rb+DL)/rb; % rb = y(1)
alpha = @(rb) (Ds+DL)/rb; % rb = y(1)
beta = @(rb) (1+alpha(rb))^2*log((1+alpha(rb))^2)/((1+alpha(rb))^2-1)-1;
A = 1 + y(1)^2*( 0.25 + 0.5*beta(y(1)) + 0.125*y(1)*fprime(beta,y(1)));
B = 0.5 + 0.75*beta(y(1)) + 0.75*y(1)*fprime(beta,y(1)) + 0.125*y(1)^2*fpprime(beta,y(1));
C = 0.25*(1 + (1+0.5*a0(y(1))^2)/(1 + 0.25*beta(y(1))*y(1)^2)^2);
%C = 0.25*(1+ 1/(1+0.25*beta(y(1))*y(1)^2)^2);
%if (x>10 && x<11.5) lambda=0.; else lambda=0; end
lambda = 0;
da2_dr = -4*y(1)/w0^2*exp(-2*y(1)^2/w0^2)*a0temp^2;
%RHS = lambda/y(1)-0.25*fprime(a02,y(1))/(1+0.25*beta(y(1))*y(1)^2);
RHS = lambda/y(1)-0.25*da2_dr/(1+0.25*beta(y(1))*y(1)^2);
%aprime = a0(y(1))*y(1)/w0;
%RHS = lambda/y(1) - aprime/(1+0.25*beta(y(1))*y(1)^2);
yprime(2) = (RHS - C*y(1)-B*y(1)*y(2)^2)/A;