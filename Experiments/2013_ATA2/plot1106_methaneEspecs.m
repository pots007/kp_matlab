if ismac
    rootf = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/20131106/20131106r002/';
    anfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/';
elseif ispc
    rootf = 'E:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Dave\Data\20131106\20131106r002\';
    anfol = 'E:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Dave\Analysis\';
end

filen = dir([rootf '*Espec2.raw']);
calib = ATA2calib('20131106');
locs = GetFigureArray(20, 4, 0.04, 0.01, 'across');
sgnalddata = zeros(80,640);
figure(100); clf;
for i=1:length(filen)
    dataf = ReadRAW16bit([rootf filen(i).name], 640, 480);
    sgnal = dataf(200:400,:);
    bg = dataf(1:199,:);
    bgrow = sum(bg,1)/199;
    sgnal = sgnal - repmat(bgrow, size(sgnal,1),1);
    sgnaldata(i,:) = sum(sgnal);
    figure(100);
    ax = axes('Position', locs(:,i));
    imagesc(sgnal');
    set(ax, 'XTick', [], 'YTick', [], 'CLim', [0 30000]);
    hold on;
    plot([0 201], [75 75], 'w', 'LineWidth', 1.5);
    plot([0 201], [175 175], 'w', 'LineWidth', 1.5);
    hold off;
    if (mod(i-1,20)==0)
        set(ax, 'YTick', [75 175], 'YTickLabel', round([calib(3,75) calib(3,173)]))
    end
end
%export_fig([anfol '20131106r002_methane'], '-transparent', '-nocrop', '-pdf', 100);
%% 
filen = dir([rootf '*Espec2.raw']);
calib = ATA2calib('20131106');
locs = GetFigureArray(1, 2, 0.04, 0.01, 'across');
figure(100); clf;
ax1 = axes('Parent', 100, 'Position', locs(:,1), 'NextPlot', 'add');
for i=1:length(filen)
    dataf = ReadRAW16bit([rootf filen(i).name], 640, 480);
    sgnal = dataf(200:400,:);
    bg = dataf(1:199,:);
    bgrow = sum(bg,1)/199;
    sgnal = sgnal - repmat(bgrow, size(sgnal,1),1);
    imagesc((i-1)*200:1:i*200, 1:640, (sgnal'));
    %hold on;
    %plot([0 201], [75 75], 'w', 'LineWidth', 1.5);
    %plot([0 201], [175 175], 'w', 'LineWidth', 1.5);
    %hold off;
    %if (mod(i-1,20)==0)
        %set(ax, 'YTick', [75 175], 'YTickLabel', round([calib(3,75) calib(3,173)]))
    %end
end
set(ax1, 'XLim', [0 80*200], 'YLim', [1 640], 'YDir', 'reverse',...
    'CLim', [0 25000], 'XTick', [], 'YTick', []);
colormap(flipud(gray));
%export_fig([anfol '20131106r002_methane'], '-transparent', '-nocrop','-pdf', 100);
%%
figure(103)
sgnalRMS = sqrt(rms(sgnaldata,1));
sgnalmean = mean(sgnaldata,1);
h = area([sgnalmean-sgnalRMS; sgnalmean+sgnalRMS]', 'EdgeColor', [0.73 0.73 0.73]);
set(h(2), 'FaceColor', [0.73 0.73 0.73]);
hold on
plot(sgnalmean, 'k', 'LineWidth', 2);
hold off
ticks = 50:75:640;
ens = zeros(length(ticks),1);
for i=1:length(ticks); ens(i) = round(calib(3,ticks(i)));   end;
set(gca, 'XTick', ticks, 'XTickLabel', ens);
xlabel('Energy /Mev');
setAllFonts(103, 22);
export_fig([anfol '20131106r002_methane_mean'], '-transparent', '-nocrop', '-pdf', 103);