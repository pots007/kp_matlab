savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/ForwardSpectrum/';
datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
date = 20131004;
figure(120); clf;
set(120, 'Position', [100 100 900 1000], 'Color', 'w');%, 'PaperUnits', 'centimeters', 'PaperSize', [8.6 6]);
%axs = zeros(4,1);
divlims = [-30 30];
runn = 2;
load('invfire.mat');
if ~exist([savfol num2str(date)], 'dir')
    mkdir([savfol num2str(date)]);
    %mkdir([savfol num2str(date) '/png']);
    %mkdir([savfol num2str(date) '/fig']);
end
nfiles = length(dir(sprintf('%s%i/%ir%03i/*Espec2.raw', datafolder, date, date, runn)));
for i=1:nfiles
    if (i==91) continue; end;
    figure(120); clf;
    disp(['Doing image ' num2str(i) ' out of ' num2str(nfiles)]);
    ax1 = axes('Parent', 120, 'Position', [0.12 0.55 0.75 0.4]);
    spec = fitsread(sprintf('%s/%i/%ir%03i/%ir%03is%03i_forward_spectrum.fit',...
        datafolder,date,date,runn,date,runn,i));
    spec = haircut2D_R1(spec, 1.05);
    cal = Calibrate_forward_spec_ATA2(false);
    BGtop = mean(spec(1:100,:),2); BGbot = mean(spec(200:250,:),2);
    BGfit = polyfit([1:100 200:250], [BGtop' BGbot'], 1);
    BG = polyval(BGfit, 1:size(spec,1));
    BG = repmat(BG', 1, size(spec,2));
    spec1 = spec-BG;
    imagesc(spec1);
    set(ax1, 'YTick', 1:50:250, 'XTick', 50:100:1000, 'XTickLabel', ...
        num2str(polyval(cal.coeffs, (50:100:1000)'), '%2.0f'), 'YDir', 'normal', 'NextPlot', 'add');
    plot(1:size(spec,2), smooth(sum(spec1,1)/(max(sum(spec1,1)))*200), '--k');
    cb = colorbar;
    set(cb, 'Position', [0.9, 0.55, 0.02 0.4]);
    xlabel('Wavelength / nm');
    
    % and now the raw image with charge calibration done
    ax3 = axes('Parent', 120, 'Position', [0.12 0.03 0.75 0.4]);
    rawfilen = sprintf('%i/%ir%03i/%ir%03is%03i_Espec2.raw', date,date,runn,date,runn,i);
    rawim = ReadRAW16bit([datafolder rawfilen], 640, 480);
    rawim = haircut2D_R1(rawim, 1.05);
    imagesc(rawim)
    set(ax3, 'XDir', 'reverse', 'XTick', [], 'YTick', [], 'NextPlot', 'add');
    bg1 = rawim(1:250,:); 
    rectangle('Position', [1 1 640 250]);
    bg2 = rawim(400:430,:);
    rectangle('Position', [1 400 640 30]);
    BG = mean([mean(bg1(:)) mean(bg2(:))]);
    rawimBG = rawim - BG;
    totQ = sum(sum(rawimBG(250:400,:)))*7.7;
    hbeam = rectangle('Position', [1 250 640 150], 'EdgeColor', 'r');
    title(sprintf('Total charge Q = %1.3f pC', totQ*qe*1e12));
    cb2 = colorbar;
    set(cb2, 'Position', [0.9 0.03 0.02 0.4]);
    setAllFonts(120, 20);
    colormap(iijcm);
    
    export_fig([savfol sprintf('%i/%03is%03i', date, runn, i)], '-nocrop', '-png', 120);

end