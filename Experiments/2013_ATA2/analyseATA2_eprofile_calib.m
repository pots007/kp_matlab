% analyseATA2_eprofile_calib.m

% Unfortunately the best available reference is from 1030, but what can we
% do

if ismac
    reff = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/References/20131031/eprofile/';
end

cmap = brewermap(256, 'YlGnBu');
% Start with reference.

greenieref = imread(fullfile(reff, '20131031_greenie_axis_reference.TIF'));

figure(810); clf(810);
imagesc(greenieref);
set(gca, 'YDir', 'normal');

% Do FWb and then fit line to that data
hline = sum(greenieref,1);
hline = hline./max(hline)*380;
hfit = fit((1:size(greenieref,2))', hline', 'gauss2');
vline = sum(greenieref, 2);
vline = vline./max(vline)*580;
vfit = fit((1:size(greenieref,1))', vline, 'gauss2');
xc = hfit.b1; yc = vfit.b1;
xc = 322; yc = 222;

% Plot everything else
hold on; colormap(cmap);
plot(1:size(greenieref,2), hline, 'ok');
plot(feval(vfit, 1:size(greenieref,1)), 1:size(greenieref,1), '-r');
plot(1:size(greenieref,2), feval(hfit, 1:size(greenieref,2)), '-r');
plot(vline, 1:size(greenieref,1), 'ok');
plot(xc*[1 1], [1 size(greenieref,1)], '--k');
plot([1 size(greenieref,2)], yc*[1 1], '--k');
hold off;

P0 = [yc xc];

% And save the plot, if we fancy
if true
    
end

% From some code long time ago, seems to be correct for ATA2 profile screen
y_pixel= 1:size(greenieref,1);
x_pixel= 1:size(greenieref,2);

% Below, these are the axes in angular units!
dist0 = 400;
y_angle=1000*(y_pixel*0.0526-yc*0.0526)/dist0; % in mrad
x_angle= 1000*(x_pixel*0.0519*cos(pi/4)-xc*0.0519*cos(pi/4))/dist0;
