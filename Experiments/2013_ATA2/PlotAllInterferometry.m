dates = [20131004 20131010 20131016];
if (ismac)
    savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Interferometry/';
    datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
end
densities = zeros(5,1000);
coun = 1;
for i=2
    run = 0;
    while (true)
        run = run + 1;
        runfol = [datafolder sprintf('%i/%ir%03i/', dates(i), dates(i), run)];
        if (~isdir(runfol))
            break;
        end
        filen = dir([runfol '*interferometer.raw']);
        for k=1:length(filen)
            
            if (exist([savfol filen(k).name(1:end-3) 'fig'], 'file'))
                %continue;
            end
            disp(filen(k).name(1:end-4));
            IFdata = ReadRAW16bit([runfol filen(k).name], 1280, 960);
            try
                hfig = plotATA2Interferometry(IFdata, 17, dates(i));
            catch
                continue
            end
            title(filen(k).name(1:16))
            densities(1,coun) = str2double(filen(k).name(1:8));
            densities(2,coun) = str2double(filen(k).name(10:12));
            densities(3,coun) = str2double(filen(k).name(14:16));
            densities(4,coun) = hfig.meanrho;
            densities(5,coun) = hfig.stdevrho;
            coun = coun + 1;
            %saveas(hfig.him, [savfol filen(k).name(1:end-3) 'fig']);
            export_fig([savfol 'png/' filen(k).name(1:end-4)], '-transparent', '-nocrop', '-png', hfig.him);
        end
    end
end