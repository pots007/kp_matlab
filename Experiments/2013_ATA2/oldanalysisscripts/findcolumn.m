% Get Dazzler setting from shotsheet
% Columns for ATA2_2013 Shotsheet:
% 1   date
% 2 run
% 3 shot
% 4 time
% 5 Astra shot number
% 6 target
% 7 gas
% 8 waveplate
% 9 pressure
% 10 pressure autom
% 11 energy
% 12 energy autom
% 13 probe timing
% 14 comments
% 15 gas jet z
% 16 parabola z

function smth = findcolumn(raws, filename, col)
shotdate = str2double(filename(1:8));
shotrun = str2double(filename(10:12));
shotnumber = str2double(filename(14:16));
i=1;
while (shotdate ~= raws{i,1})
    i = i+1;
end
while (shotrun ~= raws{i,2})
    i = i+1;
end
while (shotnumber ~= raws{i,3})
    i = i+1;
end
smth = raws{i,col};
end
