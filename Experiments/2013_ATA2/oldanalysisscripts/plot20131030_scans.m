%% Binned version of above

[nums texts raws] = xlsread('E:\Shotsheets\20131030.xlsx');
fol = 'E:\Data\20131030\';
%filen = dir([fol '*Espec2.raw']);
typ = {'Ar+main', 'Ar+smallpre', 'Ar+fullpre', 'He+pre', 'He'};
runs = [3 4 5 1 1];
shots{1} = 5:64;
shots{2} = 1:35;
shots{3} = 1:39;
shots{4} = 56:69;
shots{5} = 42:55;

for i=1:length(typ)
    temp = shots{i};
    pressures = zeros(1,length(temp));
    %sort shots to decreasing pressure
    for k=1:length(temp)
        temp(k);
        pressures(k) = findcolumn(raws, sprintf('20131030r%03is%03i',...
            runs(i), temp(k)), 9);
    end
    %shotpres = [temp;pressures];
    [shotpres_s, I] = sort(pressures, 2, 'descend');
    temp2 = temp(I);
    spres{i} = [temp2; shotpres_s];
end

figure(3);
set(3, 'WindowStyle', 'docked');
clf;
ax_h = (1-0.07)/length(typ);
ax_w = (1-0.1)/55;
bins = 60:-1:4;

% Electrons
wstart = 0.05;
for i=1:length(bins)
    wcount = zeros(1,length(typ));
    for m=1:length(typ)
        temp = spres{m};
        for k=1:length(temp)
            if (temp(2,k)>bins(i) && temp(2,k)<bins(i)+1)
                filename = sprintf('20131030r%03is%03i_Eprofile.raw', runs(m), temp(1,k));
                %filename = sprintf('20131004r002s%03i_x-ray.tif', temp2(k));
                folextn = sprintf('20131030r%03i',runs(m));
                dat = ReadRAW16bit([fol folextn '\' filename], 640, 480);
                %dat = dat(200:450,:)';
                %dat = imread([fol filename]);
                pressure = findcolumn(raws, filename, 9);
                hax = axes('Position', [wstart+wcount(m)*ax_w, 0.05+(m-1)*ax_h, ax_w, ax_h-0.03],...
                    'XTick', [], 'YTick', []);
                imagesc(dat);
                %hold on;
                %plot([1 400], [192 192], 'r', 'LineWidth', 1.3); %100 MeV line
                %hold off;
                set(gca, 'XTick', [], 'YTick', [], 'CLim', [0 5000]);
                xlabel(sprintf('%2.1f', pressure));
                wcount(m) = wcount(m) + 1;
            end
        end
    end
    wstart = wstart + max(wcount)*ax_w;
end

for i=1:length(typ)
    axes('Position', [0.04, 0.05+(i-1)*ax_h, 0.001, ax_h-0.03], ...
        'YTick', [], 'XTick', [], 'Color', 'none')
    ylabel(typ(i));
%     axes('Position', [0.95, 0.05+(i-1)*ax_h, 0.001, ax_h-0.03], ...
%         'XTick', [], 'Color', 'none', 'YColor', 'r',...
%         'YAxisLocation', 'right', 'YLim', [0 640],...
%         'YDir', 'normal', 'YTick', [0 640-192],...
%         'YTickLabel', [46 140]);
end
%%
figure(4);
set(4, 'WindowStyle', 'docked');
clf;
ax_h = (1-0.07)/length(typ);
ax_w = (1-0.1)/55;
bins = 60:-1:4;

% Electrons
wstart = 0.05;
for i=1:length(bins)
    wcount = zeros(1,length(typ));
    for m=1:length(typ)
        temp = spres{m};
        for k=1:length(temp)
            if (temp(2,k)>bins(i) && temp(2,k)<bins(i)+1)
                filename = sprintf('20131030r%03is%03i_interferometer.raw', runs(m), temp(1,k));
                %filename = sprintf('20131004r002s%03i_x-ray.tif', temp2(k));
                folextn = sprintf('20131030r%03i',runs(m));
                dat = ReadRAW16bit([fol folextn '\' filename], 2*640, 2*480);
                dat = dat(400:650,1:900)';
                %dat = imread([fol filename]);
                pressure = findcolumn(raws, filename, 9);
                hax = axes('Position', [wstart+wcount(m)*ax_w, 0.05+(m-1)*ax_h, ax_w, ax_h-0.03],...
                    'XTick', [], 'YTick', []);
                imagesc(dat);
                %hold on;
                %plot([1 400], [192 192], 'r', 'LineWidth', 1.3); %100 MeV line
                %hold off;
                set(gca, 'XTick', [], 'YTick', [], 'CLim', [10000 65000]);
                xlabel(sprintf('%2.1f', pressure));
                wcount(m) = wcount(m) + 1;
            end
        end
    end
    wstart = wstart + max(wcount)*ax_w;
end
for i=1:length(typ)
    axes('Position', [0.04, 0.05+(i-1)*ax_h, 0.001, ax_h-0.03], ...
        'YTick', [], 'XTick', [], 'Color', 'none')
    ylabel(typ(i));
end
% export_fig('20131010\20131010_r1_espec.pdf', '-nocrop', '-transparent', '-pdf', 3);
% export_fig('20131010\20131010_r1_xray.pdf', '-nocrop', '-transparent', '-pdf', 4);