[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2013_ATA2\20131004.xlsx');
fol = '20131004\20131004r002\';
filen = dir([fol '*Espec2.raw']);
z = [2000 1500 1000 500 0];
shots{1} = 1:24;
shots{2} = 25:54;
shots{3} = 55:86;
shots{4} = 87:113;
shots{5} = 114:143;
figure(1);
set(1, 'WindowStyle', 'docked');
ax_h = (1-0.1)/5;
ax_w = (1-0.1)/31;
for i=1:length(z)
    temp = shots{i};
    pressures = zeros(1,length(temp));
    %sort shots to decreasing pressure
    for k=1:length(temp)
        temp(k);
        pressures(k) = findcolumn(raws, sprintf('20131004r002s%03i_Espec2.raw',...
            temp(k)), 9);
    end
    %shotpres = [temp;pressures];
    [shotpres_s, I] = sort(pressures, 2, 'descend');
    temp2 = temp(I);
    for k=1:length(temp)
        filename = sprintf('20131004r002s%03i_Espec2.raw', temp2(k));
        %filename = sprintf('20131004r002s%03i_x-ray.tif', temp2(k));
        dat = ReadRAW16bit([fol filename],...
            640, 480);
        dat = dat(250:400,:)';
        %dat = imread([fol filename]);
        pressure = findcolumn(raws, filename, 9);
        hax = axes('Position', [0.05+(k-1)*ax_w, 0.05+(i-1)*ax_h, ax_w, ax_h-0.03],...
            'XTick', [], 'YTick', []);
        imagesc(dat);
        set(gca, 'XTick', [], 'YTick', [], 'CLim', [0 25000]);
        if (k==1)
            ylabel(num2str(z(i)));
        end
        xlabel(sprintf('%2.1f', pressure));
    end
end

[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2013_ATA2\20131004.xlsx');
fol = '20131004\20131004r002\';
filen = dir([fol '*Espec2.raw']);
z = [2000 1500 1000 500 0];
shots{1} = 1:24;
shots{2} = 25:54;
shots{3} = 55:86;
shots{4} = 87:113;
shots{5} = 114:143;
figure(2);
set(2, 'WindowStyle', 'docked');
ax_h = (1-0.1)/5;
ax_w = (1-0.1)/31;
for i=1:length(z)
    temp = shots{i};
    pressures = zeros(1,length(temp));
    %sort shots to decreasing pressure
    for k=1:length(temp)
        temp(k);
        pressures(k) = findcolumn(raws, sprintf('20131004r002s%03i_Espec2.raw',...
            temp(k)), 9);
    end
    %shotpres = [temp;pressures];
    [shotpres_s, I] = sort(pressures, 2, 'descend');
    temp2 = temp(I);
    for k=1:length(temp)
        %filename = sprintf('20131004r002s%03i_Espec2.raw', temp2(k));
        filename = sprintf('20131004r002s%03i_x-ray.tif', temp2(k));
        %dat = ReadRAW16bit([fol filename],...
        %    640, 480);
        %dat = dat(250:400,:)';
        dat = imread([fol filename]);
        pressure = findcolumn(raws, filename, 9);
        hax = axes('Position', [0.05+(k-1)*ax_w, 0.05+(i-1)*ax_h, ax_w, ax_h-0.03],...
            'XTick', [], 'YTick', []);
        imagesc(dat);
        set(gca, 'XTick', [], 'YTick', [], 'CLim', [3000 15000]);
        if (k==1)
            ylabel(num2str(z(i)));
        end
        xlabel(sprintf('%2.0f', pressure));
    end
end


%% Binned version of above

[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2013_ATA2\20131004.xlsx');
fol = '20131004\20131004r002\';
filen = dir([fol '*Espec2.raw']);
z = [2000 1500 1000 500 0];
shots{1} = 1:24;
shots{2} = 25:54;
shots{3} = 55:86;
shots{4} = 87:113;
shots{5} = 114:143;

for i=1:length(z)
    temp = shots{i};
    pressures = zeros(1,length(temp));
    %sort shots to decreasing pressure
    for k=1:length(temp)
        temp(k);
        pressures(k) = findcolumn(raws, sprintf('20131004r002s%03i_Espec2.raw',...
            temp(k)), 9);
    end
    %shotpres = [temp;pressures];
    [shotpres_s, I] = sort(pressures, 2, 'descend');
    temp2 = temp(I);
    spres{i} = [temp2; shotpres_s];
end
figure(3);
set(3, 'WindowStyle', 'docked');
ax_h = (1-0.07)/5;
ax_w = (1-0.1)/32;
bins = 21:-1:6;

% Electrons
wstart = 0.05;
for i=1:length(bins)
    wcount = zeros(1,5);
    for m=1:length(z)
        temp = spres{m};
        for k=1:length(temp)
            if (temp(2,k)>bins(i) && temp(2,k)<bins(i)+1)
                filename = sprintf('20131004r002s%03i_Espec2.raw', temp(1,k));
                %filename = sprintf('20131004r002s%03i_x-ray.tif', temp2(k));
                dat = ReadRAW16bit([fol filename], 640, 480);
                dat = dat(250:400,:)';
                %dat = imread([fol filename]);
                pressure = findcolumn(raws, filename, 9);
                hax = axes('Position', [wstart+wcount(m)*ax_w, 0.05+(m-1)*ax_h, ax_w, ax_h-0.03],...
                    'XTick', [], 'YTick', []);
                imagesc(dat);
                hold on;
                plot([1 200], [192 192], 'r', 'LineWidth', 1.3); %100 MeV line
                hold off;
                set(gca, 'XTick', [], 'YTick', [], 'CLim', [0 15000]);
                if (k==1)
                    ylabel(num2str(z(i)));
                end
                xlabel(sprintf('%2.1f', pressure));
                wcount(m) = wcount(m) + 1;
            end
        end
    end
    wstart = wstart + max(wcount)*ax_w;
end

for i=1:length(z)
    axes('Position', [0.04, 0.05+(i-1)*ax_h, 0.001, ax_h-0.03], ...
        'YTick', [], 'XTick', [], 'Color', 'none')
    ylabel(num2str(z(i)));
    axes('Position', [0.95, 0.05+(i-1)*ax_h, 0.001, ax_h-0.03], ...
        'XTick', [], 'Color', 'none', 'YColor', 'r',...
        'YAxisLocation', 'right', 'YLim', [0 640],...
        'YDir', 'normal', 'YTick', [0 640-200],...
        'YTickLabel', [46 140]);
end

% x-rays
figure(4);
set(4, 'WindowStyle', 'docked');
wstart = 0.05;
for i=1:length(bins)
    wcount = zeros(1,5);
    for m=1:length(z)
        temp = spres{m};
        for k=1:length(temp)
            if (temp(2,k)>bins(i) && temp(2,k)<bins(i)+1)
                %filename = sprintf('20131004r002s%03i_Espec2.raw', temp(1,k));
                filename = sprintf('20131004r002s%03i_x-ray.tif', temp(1,k));
                %dat = ReadRAW16bit([fol filename], 640, 480);
                %dat = dat(250:400,:)';
                dat = imread([fol filename]);
                pressure = findcolumn(raws, filename, 9);
                hax = axes('Position', [wstart+wcount(m)*ax_w, 0.05+(m-1)*ax_h, ax_w, ax_h-0.03],...
                    'XTick', [], 'YTick', []);
                imagesc(dat);
                set(gca, 'XTick', [], 'YTick', [], 'CLim', [3000 15000]);
                if (k==1)
                    ylabel(num2str(z(i)));
                end
                xlabel(sprintf('%2.1f', pressure));
                wcount(m) = wcount(m) + 1;
            end
        end
    end
    wstart = wstart + max(wcount)*ax_w;
end

for i=1:length(z)
    axes('Position', [0.04, 0.05+(i-1)*ax_h, 0.001, ax_h-0.03], ...
        'YTick', [], 'XTick', [], 'Color', 'none')
    ylabel(num2str(z(i)));
end
%export_fig('20131004\espec.pdf', '-nocrop', '-transparent', '-pdf', 3);
%export_fig('20131004\xray.pdf', '-nocrop', '-transparent', '-pdf', 4);