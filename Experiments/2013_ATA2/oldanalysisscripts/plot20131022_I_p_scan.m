% Shadowgraphy and interferometry
fol = '../Data/20131022/20131022r002/';
filen = dir([fol '*shadowgraphy.tif']);
nopres = 5;
pres = 60:-10:10;
noI = 6;
I = [210 150 110 70 50 30];
shots = cell(nopres, noI); %cols are pres, rows I
shots{1,1} = 2:3; shots{1,2} = 4:5; shots{1,3} = 7:8;
shots{1,4} = 9:10; shots{1,5} = 16:17; shots{1,6} = 11:12;
shots{2,1} = [20 23]; shots{2,2} = [24 26]; shots{2,3} = [27 29];
shots{2,4} = 30:31; shots{2,5} = 32:33; shots{2,6} = 34:35;
shots{3,1} = 36:37; shots{3,2} = 38:39; shots{3,3} = 40:41;
shots{3,4} = 42:43; shots{3,5} = 44:45; shots{3,6} = 46:47;
shots{4,1} = 48:49; shots{4,2} = 50:51; shots{4,3} = 52:53;
shots{4,4} = 54:55; shots{4,5} = [56 58]; shots{4,6} = 59:60;
shots{5,1} = 62:63; shots{5,2} = [64 66]; shots{5,3} = 67:68;
shots{5,4} = 69:70; shots{5,5} = 71:72; shots{5,6} = 73:74;
shots{6,1} = 75:76; shots{6,2} = 77:78; shots{6,3} = 79:80;
shots{6,4} = 81:82; shots{6,5} = 83:84; shots{6,6} = 85:86;

%Shadowgraphy

figure(3);
set(3, 'WindowStyle', 'docked');
clf;
ax_h = (1-0.07)/nopres;
ax_w = (1-0.1)/noI;
%bins = 21:-1:4;
for i=1:nopres
    for k=1:noI
        temp = shots{i,k};
        dat1 = imread([fol sprintf('20131022r002s%03i_shadowgraphy.tif',temp(1))]);
        dat2 = imread([fol sprintf('20131022r002s%03i_shadowgraphy.tif',temp(2))]);
        dat = [dat1(800:1200,:); dat2(800:1200,:)];
        hax = axes('Position', [0.1+(k-1)*ax_w 0.07+(i-1)*ax_h, ax_w-0.02 ax_h-0.02],...
            'XTick', [], 'YTick', []);
        imagesc(dat);
        set(gca, 'XTick', [], 'YTick', []);
        %colormap('gray');
        if (k==1)
            ylabel([num2str(pres(i)) ' bar']);
        end
        if (i==1)
            xlabel(['Waveplate at ' num2str(I(k))]);
        end
    end
    
end

%Interferometry

figure(4);
set(4, 'WindowStyle', 'docked');
clf;
ax_h = (1-0.07)/nopres;
ax_w = (1-0.1)/noI;
%bins = 21:-1:4;
for i=1:nopres
    for k=1:noI
        temp = shots{i,k};
        dat1 = ReadRAW16bit([fol sprintf('20131022r002s%03i_interferometer.raw',temp(1))],...
            1280, 960);
        dat2 = ReadRAW16bit([fol sprintf('20131022r002s%03i_interferometer.raw',temp(2))],...
            1280, 960);
        dat = [dat1(400:600,:); dat2(400:600,:)];
        hax = axes('Position', [0.1+(k-1)*ax_w 0.07+(i-1)*ax_h, ax_w-0.02 ax_h-0.02],...
            'XTick', [], 'YTick', []);
        imagesc(fliplr(dat));
        set(gca, 'XTick', [], 'YTick', []);%'CLim', [0 10000
        %colormap('gray');
        if (k==1)
            ylabel([num2str(pres(i)) ' bar']);
        end
        if (i==1)
            xlabel(['Waveplate at ' num2str(I(k))]);
        end
    end
    
end


%export_fig('20131010\20131010_r1_espec.pdf', '-nocrop', '-transparent', '-pdf', 3);
%export_fig('20131010\20131010_r1_xray.pdf', '-nocrop', '-transparent', '-pdf', 4);