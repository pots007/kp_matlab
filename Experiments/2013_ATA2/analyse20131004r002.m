% This ought to look at the file analysed by especPeakFinding.m

% 20131010r001, ATA2 II

% z = -9964 is focused on the edge.

if ismac
    %datafol = fullfile(getGDrive, 'Experiment', '2013_ATA2', 'EspecAnalysis', '20131004r002', 'mat_20160704', 'notpoo');
    %savfol = fullfile(getGDrive, 'Experiment', '2013_ATA2', 'EspecAnalysis', '20131004r002');
    datafol = fullfile(getGDrive, 'Experiment', '2013_ATA2', 'analysis_old', 'Espec2', '20131004', 'mat');
    savfol = fullfile(getGDrive, 'Experiment', '2013_ATA2', 'analysis_old', 'Espec2', '20131004');
elseif ispc
    datafol = 'EE:\Kristjan\Google Drive\Experiment\2013TA2\analysis\Espec2\20131004\mat';
    savfol = 'EE:\Kristjan\Google Drive\Experiment\2013TA2\analysis\Espec2\20131004\';
end

load('20131004r002_zlocations');
load('ATA2_2013');
dat = ATA2_2013.D20131004.run(2);
SI_calib = [0.08726 0.15267];
ff = load('ATA2_SI_tracking_axial');
alongs = ff.tracking.screen(1).alongscreen;
logm = ~isnan(alongs);
lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

ax_track = 0;

flist = dir(fullfile(datafol, '*.mat'));
fnames = {flist.name};

edata = zeros(10,length(fnames));
poo = zeros(size(fnames));
for i=1:length(fnames)
    fprintf('File %i out of %i\n', i, length(fnames));
    ll = load(fullfile(datafol, fnames{i}));
    poo(i) = ll.shotdata.poo;
    if poo(i); continue; end
    shotno = str2double(fnames{i}(5:7));%(14:16));
    edata(1,i) = shotno;
    edata(2,i) = dat.shot(shotno).pressure;
    edata(3,i) = polyval(SI_calib, edata(2,i));
    edata(4,i) = zlocs(shotno);
    if ~isfield(ll.shotdata, 'max'); continue; end;
    if ax_track
        edata(5,i) = ll.shotdata.max.low;
        edata(6,i) = ll.shotdata.max.val;
        edata(7,i) = ll.shotdata.max.high;
        edata(8,i) = ll.shotdata.peak.low;
        edata(9,i) = ll.shotdata.peak.val;
        edata(10,i) = ll.shotdata.peak.high;
    else
        edata(6,i) = valfit(ll.shotdata.alongscreen(ll.shotdata.max.pixel));
        edata(5,i) = lowfit(ll.shotdata.alongscreen(ll.shotdata.max.pixel))-edata(6,i);
        edata(7,i) = -highfit(ll.shotdata.alongscreen(ll.shotdata.max.pixel))+edata(6,i);
        edata(9,i) = valfit(ll.shotdata.alongscreen(ll.shotdata.peak.pixel));
        edata(8,i) = lowfit(ll.shotdata.alongscreen(ll.shotdata.peak.pixel))-edata(9,i);
        edata(10,i) = -highfit(ll.shotdata.alongscreen(ll.shotdata.peak.pixel))+edata(9,i);
    end
    edata(11,i) = ll.shotdata.totQ;
end
poo = logical(poo);
edata = edata(:, ~poo);

%% And now for plotting all kinds of things

% First just the maximum energy as a function of plasma density
hfig = 77;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
% errorbar for simplistic analysis
h1 = errorbar(edata(3,:), edata(6,:), edata(5,:), edata(7,:), 'o', 'LineWidth', 1.);
% Try rolling average now
[~,sind] = sort(edata(3,:));
npnts = 5;
avg_ens = tsmovavg(edata(6,sind), 's', npnts,2);
hold on;
h2 = plot(edata(3,sind), avg_ens, '-k', 'LineWidth', 3);
% And add the dephasing length!
nes = (0.8:0.01:2)*1e19;
dW = ncrit(800)./(nes)*0.511*2;
h3 = plot(nes*1e-19, dW, '--r', 'LineWidth', 3);
hold off;
xlabel('Plasma density / $10^{19} \: \mathrm{cm}^{-3}$');
ylabel('Maximum energy / MeV');
legend([h1 h2 h3], {'Data', [num2str(npnts) ' point moving average'], '$\Delta W = 2\gamma^2 m c^2$'},...
    'Location', 'Northeast');
set(gca, 'Position', [0.12 0.13 0.85 0.85], 'Box', 'on', 'LineWidth', 2, 'XLim', [0.8 2]);
make_latex(hfig);
setAllFonts(hfig,20);
%export_fig(fullfile(savfol, 'MaxEn_ne'), '-pdf', '-nocrop', hfig);


%% Then, the peak energy as a function of plasma density
hfig = 77;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
errorbar(edata(3,:), edata(9,:), edata(8,:), edata(10,:), 'o', 'LineWidth', 1.5);
xlabel('Plasma density / $10^{19} \: \mathrm{cm}^{-3}$');
ylabel('Peak energy / MeV');
set(gca, 'Position', [0.12 0.13 0.85 0.85], 'Box', 'on', 'LineWidth', 2);
make_latex(hfig);
setAllFonts(hfig,20);
%export_fig(fullfile(savfol, 'PeakEn_ne'), '-pdf', '-nocrop', hfig);


%% Now peak energy for different z positions

hfig = 77;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
set(gca, 'NextPlot', 'add');
% Fix the odd 1 micron error
z_s = unique(edata(4,:));
colz = 'kbrgm';
for i=1:5
    logm = edata(4,:)==z_s(i) & edata(11,:)>2;
    dens = edata(3:7,logm);
    [~,inds] = sort(dens(1,:));
    %hh(i) = plot(dens(1,inds), tsmovavg(dens(4,inds), 's', 1, 2), ['d--' colz(i)]);
    hh(i) = plot(dens(1,inds), dens(4,inds), ['d' colz(i)]);
    legentry{i} = sprintf('z = %1.0f', abs(round(z_s(i)+7644, 2, 'significant')));
    drawnow
end
%errorbar((edata(4,:))+9964, edata(6,:), edata(5,:), edata(7,:), 'o');
nes = (0.9:0.01:2)*1e19;
dW = ncrit(800)./(nes)*0.511*2;
hh(6) = plot(nes*1e-19, dW, '--r', 'LineWidth', 3);
legentry{6} = '$\Delta W = 2\gamma^2 mc^2$';
legend(hh, legentry, 'Location', 'northeast');
set(hh(1:5), 'LineWidth', 1.5);
set(gca, 'Position', [0.12 0.13 0.85 0.8], 'Box', 'on', 'LineWidth', 2);
xlabel('Plasma density / $10^{19} \: \mathrm{cm}^{-3}$');
ylabel(gca, 'Maximum energy / MeV');
make_latex(hfig);
setAllFonts(hfig,20);
%export_fig(fullfile(savfol, 'MaxEn_zs_ne'), '-pdf', '-nocrop', hfig);

%% Now total charge for different z locations, as fn of ne
hfig = 79;
figure(hfig); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 800 600]);
set(gca, 'NextPlot', 'add');
% Fix the odd 1 micron error
z_s = unique(edata(4,:));
colz = 'kbrgm';
for i=1:5
    logm = edata(4,:)==z_s(i) & edata(11,:)>10;
    dens = edata(3:11,logm);
    [~,inds] = sort(dens(1,:));
    %hh(i) = plot(dens(1,inds), tsmovavg(dens(4,inds), 's', 1, 2), ['d--' colz(i)]);
    hh(i) = plot(dens(1,inds), dens(9,inds), ['d--' colz(i)]);
    legentry{i} = sprintf('z = %1.0f', abs(round(z_s(i)+7644)));
end
%errorbar((edata(4,:))+9964, edata(6,:), edata(5,:), edata(7,:), 'o');
nes = (0.6:0.01:2)*1e19;
dW = 2/3*ncrit(800)./(nes)*0.511*2;
%h3 = plot(nes*1e-19, dW, '--r', 'LineWidth', 3);
legend(hh, legentry, 'Location', 'northeast');
set(hh, 'LineWidth', 1.5);
set(gca, 'Position', [0.12 0.13 0.85 0.8], 'Box', 'on', 'LineWidth', 2);
xlabel('Plasma density / $10^{19} \: \mathrm{cm}^{-3}$');
ylabel(gca, '$Q_{tot}$ / pC');
make_latex(hfig);
setAllFonts(hfig,20);
%export_fig(fullfile(savfol, 'Qtot_zs_ne'), '-pdf', '-nocrop', hfig);