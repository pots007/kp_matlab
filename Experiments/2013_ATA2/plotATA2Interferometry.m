function him = plotATA2Interferometry(dataimage, hfig, date)
if(ismac)
    datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
elseif(ispc)
    datafolder = 'E:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Dave\Data\';%'E:\Data\2013_ATA2_Pattathil\Data\';
end

switch date
    case 20131004
        run = 2;
        shot = 8;
        rotation = 0;
        x = 39;
        y = 477;
        w = 1016;
        h = 228;
        xfft = 538;
        yfft = 105;
        wfft = 17;
        hfft = 20;
        ymid = 110;
    case 20131010
        run = 1;
        shot = 29;
        rotation = 0;
        x = 29;
        y = 476;
        w = 1069;
        h = 250;
        xfft = 566;
        yfft = 115;
        wfft = 20;
        hfft = 20;
        ymid = 127;
    case 20131016
        run = 1;
        shot = 5;
        rotation = 1.5;
        x = 29;
        y = 440;
        w = 971;
        h = 200;
        xfft = 528;
        yfft = 88;
        wfft = 13;
        hfft = 25;
        ymid = 131;
    case 20131106
        run = 1;
        shot = 11;
        rotation = 2;
        x = 111;
        y = 505; %485; %0 degree ones
        w = 840;
        h = 140;
        xfft = 448;
        yfft = 55;
        wfft = 13;
        hfft = 32;
        ymid = 110;
    otherwise
        disp('Do not know what to do with that date....:(');
        return;
end

referencefile = sprintf('%i/%ir%03i/%ir%03is%03i_interferometer.raw',...
    date, date, run, date, run, shot);
reference = double(ReadRAW16bit([datafolder referencefile], 1280, 960));

data = double(dataimage);

phimap = -PhaseRetrieve0(data, reference, rotation, x, y, w, h, xfft, yfft, wfft, hfft);
%him = phimap;
%return
asmooth = 1e-3;
micperpix = 3.2;
if date>20131105
    micperpix = 4.2;
end
lambda = 800;
plotflag = 0;
npoints = 100;

%[rho,ymid] = AbelInversion0(phimap', ymid, asmooth, micperpix, lambda, plotflag, npoints);
% Use the one below to do automatic ymid finding...
[rho,ymid] = AbelInversion0(phimap', 0, asmooth, micperpix, lambda, plotflag, npoints);
%Plotting part
figure(hfig); clf;
set(hfig, 'Position', [100 100 800 1200]);
locs = GetFigureArray(1, 3, 0.08, 0.1, 'down');
xscale = linspace(1, w, size(rho,2))*micperpix;
ax0 = axes('Position', locs(:,1));
%xaxi = (1:1280)*3.16;
yaxi = 1:960;
imagesc(xscale, yaxi, single((dataimage(y:y+h,x:x+w)))); axis xy;
colormap(gray)
set(gca, 'YTick', [], 'XTick', []);
freezeColors;

ax05 = axes('Position', locs(:,2));
imagesc(single(phimap));
hold on
plot([1 size(phimap,2)], [ymid ymid], 'k', 'LineWidth', 2);
hold off;
set(gca, 'XTick', [], 'YTick', []);

axes('Position', locs(:,3));
load ('invfire.mat');
clims = [0 2];
rho = fliplr(rho*1e-25);
imagesc(xscale, ((1:size(rho,1))-0.5*size(rho,1))*micperpix, single(rho));
colormap(iijcm);
colorbar
xlabel('Propagation distance / \mu m');
ylabel('Radial distance / \mu m');
set(gca, 'Position', get(gca, 'Position') + [0 0 -0.02 0], 'CLim', clims, ...
    'Box', 'off','YAxisLocation', 'left');
ax1 = gca;
ax2 = axes('Position', get(ax1, 'Position'));
line(xscale, rho(round(0.5*size(rho,1)),:), 'Parent', ax2, 'LineWidth', 2, 'Color', 'b');
set(ax2, 'XAxisLocation', 'top',...
    'YAxisLocation', 'right', 'Color', 'none', 'XColor', 'k', 'YColor', 'b',...
    'XLim', get(ax1, 'XLim'), 'XTickLabel', [], 'YLim', clims);
set(get(ax2, 'YLabel'), 'String', 'n_e / \times 10^{19} cm^{-3}');
setAllFonts(hfig, 22);
hCB = findobj(hfig, 'Tag', 'Colorbar');
set(hCB, 'Position', get(hCB, 'Position') + [0.05 0 0 0]);
%make upper x-axis match bottom
p1 = get(ax0, 'Position');
p2 = get(ax1, 'Position');
p3 = get(ax05, 'Position');
p1(3) = p2(3);
p3(3) = p2(3);
set(ax0, 'Position', p1);
set(ax05, 'Position', p3);
linkaxes([ax2 ax1 ax0], 'x');
him.him = hfig;
%return the average density along middle, from 600-2700um as well
[~, lind] = min(abs(600-xscale));
[~, rind] = min(abs(2700-xscale));
him.meanrho = mean(rho(round(0.5*size(rho,1)),lind:rind));
him.stdevrho = std(rho(round(0.5*size(rho,1)),lind:rind));

end


function [rho ymid] = AbelInversion0(Phi, ymid, asmooth, micperpix, lambda, plotflag, npoints)
%Phi = the phase from the last function (don't know why I changed the name)
%ymid = pixel of symmetry axis, if zero it can be found automatically or
%manually asmooth = the smoothing parameter for the spline fit micperpix =
%self-explanatory lambda = wavelength in nm (probably not necessary as
%800nm is hard-coded below) plotflag = set to 1 to plot the density at the
%end - I may have messed this bit up npoints = how many points to sample
%across the image - not tested so again may be a bit shit

Phi(isnan(Phi)) = 0;

if (ymid == 0)
    
    % Find symmetry axis automatically
    ymidautoflag = 1;
    Phi = Phi - min(min(Phi));
    if (ymidautoflag == 1)
        % Auto ymid finder
        [zsize, ysize] = size(Phi);
        yaxis = 1:ysize;
        zpoints = 0.1:0.1:0.9;
        zpoints = zpoints*zsize;
        zpoints = round(zpoints);
        Phi0 = Phi;
        Phi0(Phi0<0.4*max(max(Phi0))) = 0;
        for n = 1:length(zpoints)
            yres(n) = trapz(Phi0(zpoints(n),:).*yaxis)./trapz(Phi0(zpoints(n),:));
        end
        yres = yres(~isnan(yres));
        ymid = round(mean(yres));
        if plotflag
            imagesc(Phi)
            line([ymid ymid], [0 zsize], 'color', 'white')
            hold on
            scatter(yres, zpoints)
            hold off
            drawnow
            pause(5)
        end
    else
        % Interactive ymid finder
        imagesc(Phi);%caxis([0 2*mean(mean(Phi))])
        [ymid zmid] = ginput(1);
        ymid = round(ymid);
        line([ymid ymid], [0 1e4], 'color', 'white'); drawnow
    end
    
end

ysize = length(Phi(1,:))-1;
zsize = length(Phi(:,1));
h = micperpix*1e-6;

lambda = lambda*1e-9;

%n=1+k(rho/rho_atm)
%rho_atm=(P/RT)*Na
rho_atm=(101.325e3*6.022e23)/(8.31*273); %units m^-3

%ncrit for 800nm in m^-3
k = rho_atm/(2*1748e24);

zindicies = linspace(1, zsize, npoints); zindicies = round(zindicies);

for z_counter = 1:npoints,
    
    z_index = zindicies(z_counter);
    Philine = Phi(z_index,:);
    Philine = Philine(ymid:ysize);
    Philineold = Philine;
    Philine = [fliplr(Philine) Philine];
    yaxis = 1:length(Philine);
    Philine = csaps(yaxis, Philine, asmooth, yaxis);
    Philine = Philine(end/2 + 1:end);
    yaxis = 1:length(Philine);
    
    % This plots the smoothed and unsmoothed phase for comparison
    %plot(Philineold, '.')
    %hold on
    %plot(Philine, 'red')
    %hold off
    
    dPhidy = -gradient(Philine,1);
    dPhidy = dPhidy/h;
    % Remove singularities at r = 0
    dPhidy(1) = 0.1*dPhidy(2);
    
    for r_index = 1:ysize-ymid,
        for y_index = r_index:ysize - ymid + 1
            y = (double(y_index) - 0.9)*h;
            r = (double(r_index) - 1.0)*h;
            integrand(y_index - r_index + 1) = dPhidy(y_index)*(y^2 - r^2)^-0.5;
        end
        
        rho(z_counter, r_index) = ((lambda*rho_atm)/(2*pi^2*k))*h*trapz(integrand);
        integrand(end) = [];
    end
    
end

rho = [fliplr(rho) rho];
rho = imrotate(rho, -90);

yaxis = length(rho(:,1));
yaxis = 1:yaxis;
yaxis = yaxis*h*1e3;
zaxis = zindicies*h*1e3;

if (plotflag == 1)
    %pcolor(zaxis, yaxis, rho)
    size(yaxis)
    size(zaxis)
    size(rho)
    pcolor(zaxis, yaxis, rho); shading flat
    xlabel('z /mm')
    ylabel('y /mm')
    axis image xy
end

end


function phase = PhaseRetrieve0(datafile, intref, rotation, x, y, w, h, xfft, yfft, wfft, hfft)
%x, y, w, h defines the region of the input image that we will work with.

% data = double(imread(datafile));
data = double(datafile);

% if (intref == 0)
%     intref = zeros(size(data));
% else
%     intref = double(imread(intref));
% end
% %reference image

data = imrotate(data, rotation);
intref = imrotate(intref, rotation);

data = data(y:y+h, x:x+w);
intref = intref(y:y+h, x:x+w);

fftim = fft2(data);
fftref = fft2(intref);

if (max([xfft yfft wfft hfft]) == 0)
    imagesc(log(abs(fftshift(fftim))))
    rect = getrect;
    xfft = round(rect(1))
    yfft = round(rect(2))
    wfft = round(rect(3))
    hfft = round(rect(4))
end
%this if statement gives you the option of manually choosing the area of
%interest in the fft'd image if you input [xfft yfft wfft hfft] = [0 0 0
%0].

fftim = fftshift(fftim);
fftref = fftshift(fftref);
newfftim = zeros(size(data));
newfftref = zeros(size(data));
newfftim(yfft:yfft+hfft, xfft:xfft+wfft) = fftim(yfft:yfft+hfft, xfft:xfft+wfft);
newfftref(yfft:yfft+hfft, xfft:xfft+wfft) = fftref(yfft:yfft+hfft, xfft:xfft+wfft);
newfftim = fftshift(newfftim);
newfftref = fftshift(newfftref);

datafiltered = ifft2(newfftim);
reffiltered = ifft2(newfftref);

if(sum(sum(isnan(reffiltered))) > 0)
    reffiltered = ones(size(datafiltered));
end

phase = angle(datafiltered./reffiltered);
mag = abs(datafiltered./reffiltered);
phase = unwrap(phase, 5.5);

% Here you can spline-smooth the phase, make 'smoothing' smaller for more smoothing
smoothing = 1;
[ysize, xsize] = size(phase);
xsize = 1:xsize;
ysize = 1:ysize;
x = {ysize,xsize};
[phase,p] = csaps(x,phase,smoothing,x);

%imagesc(phase)

end
