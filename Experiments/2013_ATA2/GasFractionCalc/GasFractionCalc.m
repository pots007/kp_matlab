%% This will calculate the fraction of impurity species we'd need for mass
% based fractions of CO2, given a0 sufficient to ionise outer shells

% Fractions
He_f = 0.975;
CO2_f = 1 - He_f;
% Molar masses:
M_He = 4;

M_CO2 = 44; % 12 for C and 16*2 for O

%% 
% Ratio of number densities: These are found from
% 
% $$ n = \frac{m N_A}{V M} $$. 
% 
% Here, the $m/M$ gives the number of moles, which then leads to the total
% number of particles per volume.
% This enables us to find the ratio of number densities as
% 
% $$\frac{n_{He}}{n_{CO2}} = \frac{m_{He} M_{CO2}}{m_{CO2} M_{He}}$$
% 
% But we also have that $m_{He} = m_0 f_{He}$ and $m_{CO2} = m_0 f_{CO2}$,
% so we get for the ratio
nHe_nCO2 = (He_f*M_CO2)/(CO2_f*M_He);

%% 
% Thus, the total amount of electrons liberated without ionising inner
% shells is
%
% $$n_e = 2n_0 + 16n_{CO2} = 2n_0 + 16\frac{n_0}{n_{ratio}} $$ 

n_total = 2 + 16/nHe_nCO2;
%%
% We want a final electron density of 1.1e19. Thus the initial number
% density of He would be

n_He_0 = 1/n_total
%% 
% For simulation inputs, though, the Helium is already assumed to be fully
% ionised and absorbed into the background number density. So in the end,
% the fraction calculated above is for informative reasons only. The
% simulation is initiated with a number density of $n_0$, which is then
% used to calculate the number density of CO2. This is found via:

n_CO2_0 = 1/(n_total*nHe_nCO2)
