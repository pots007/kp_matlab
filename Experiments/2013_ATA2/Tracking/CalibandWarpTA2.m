function CalibandWarpTA2(date)
figure(175); clf;
if (ismac)
    savfol = '/Users/kristjanpoder/Dropbox/Writing/ATA2_II_paper/';
    datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
    trackfile = '/Users/kristjanpoder/Dropbox/ICT_release/version1.0/TA2_2013_LundMag8.mat';
elseif (ispc)
    savfol = 'E:\Kristjan\Documents\Uni\Dropbox\Writing\ATA2_II_paper\';
    datafolder = 'E:\Kristjan\Documents\Uni\Imperial\2013_ATA2\Dave\Data\';
    trackfile = 'E:\Kristjan\Documents\Uni\Dropbox\ICT_release\version1.0\TA2_2013_LundMag8.mat';
end

reffil = dir([datafolder '/References/2013' date '/espec2/*.raw']);
ref = ReadRAW16bit([datafolder '/References/2013' date '/espec2/' reffil.name], 640, 480);
try
    load([datafolder '/References/2013' date '/espec2/refdata.mat']);   
catch
    
    q = GetProjectiveImageTransformTA2strict(ref);
    clf;
    good = true; i=52;
    fils = dir([datafolder '/2013' date '/2013' date 'r003/*Espec2.raw']);
    while (good)
        dataf = ReadRAW16bit([datafolder '/2013' date '/2013' date 'r003/' fils(i).name], 640, 480);
        i = i+1;
        imagesc(dataf);
        res = input('Is this image good for determining rotation?\n', 's');
        if (strcmpi(res,'Y')) break; end;
    end
    crop = ProjectiveTransformImageCropped(dataf,q);
    imagesc(crop);
    while (good)
        angles = input('Tell us an angle to rotate by!\n', 's');
        if (isempty(angles)) break; end;
        angle = str2double(angles);
        rotim = imrotate(crop, angle);
        imagesc(rotim);
    end
    projref = ProjectiveTransformImage(ref,q);
    imagesc(q.x, q.y, projref); hold on;
    set(gca, 'YDir', 'normal', 'CLim', [min(projref(:)) 0.4*max(projref(:))]);
    load(trackfile);
    imD = [0 6 16 26 36 56 76 96 126];
    scrnD = imD + (12/sin(0.25*pi));
    Efit = fit(l(:,2), E_e_MeV, 'cubicspline');
    pixels = zeros(size(imD));
    lines = zeros(size(crop));
    for i=1:length(imD)
        disp(['Find location for ' num2str(imD(i)+44) ' mm on ruler']);
        [pixels(i),y] = ginput(1);
        plot(pixels(i),y, 'xk', 'MarkerSize', 2); drawnow;
        if (pixels(i)<0) pixels(i) = 1; end
        lines(:,round(pixels(i))) = 1e5;
    end
    clf; imagesc(imrotate(crop+lines, angle));
    
    pixels2 = zeros(size(imD));
    disp(['Find location for line ' num2str(1) ' from the left']);
    [pixels2(1),y] = ginput(1);
    hold on; line([0 size(rotim,2)], [y y]); 
    for i=2:length(imD)
        disp(['Find location for line ' num2str(i) ' from the left']);
        [pixels2(i),y] = ginput(1);
        %plot(pixels2(i),y, 'xk', 'MarkerSize', 2); drawnow;     
    end
    %disp('Find place of centre of beam');
    %[offset,~] = ginput(1);
    %rotpix = pixels/cosd(angle)-(size(rotim,2)-offset)/sind(abs(angle));
    rotpix = pixels2;
    pixaxis = 1:1:size(rotim,2);
    daxis = interp1(rotpix, scrnD, pixaxis, 'spline');
    %daxis(isnan(daxis))=0;
    enaxis = Efit(daxis);
    enpix = [pixaxis; daxis; enaxis'];
    clf; imagesc(enaxis, 1:size(rotim,1),rotim);
    calibd.q = q;
    calibd.pixels = pixels;
    calibd.enpix = enpix;
    calibd.ref = projref;
    calibd.refdata = rotim;
    calibd.angle = angle;
    calibd.y0 = y;
    save([datafolder '/References/2013' date '/espec2/refdata.mat'], 'calibd');
end
