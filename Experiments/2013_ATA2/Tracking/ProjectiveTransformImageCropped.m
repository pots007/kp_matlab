function outputim = ProjectiveTransformImageCropped(image, q)

im = ProjectiveTransformImage(image,q);
[~,xmin] = min(abs(q.x-q.screenx(1)));
[~,xmax] = min(abs(q.x-q.screenx(3)));
[~,ymin] = min(abs(q.y-q.screeny(1)));
[~,ymax] = min(abs(q.y-q.screeny(2)));
outputim = im(ymin:ymax,xmin:xmax);
