% plotEspec2_warp_raw_energy2


savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Espec2/withRAW+energy/';
datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
date = 20131004;
%axs = zeros(4,1);

runn = 2;
load('invfire.mat');
load('ATA2_2013.mat');
if ~exist([savfol num2str(date)], 'dir')
    mkdir([savfol num2str(date)]);
    mkdir([savfol num2str(date) '/beams']);
    mkdir([savfol num2str(date) '/craps']);
end
nfiles = length(dir(sprintf('%s%i/%ir%03i/*Espec2.raw', datafolder, date, date, runn)));
eldata.(['D' num2str(date)]).data = zeros(nfiles, 5);
for i=1:nfiles
    if (i==91&&date==20131010) continue; end;
    disp(['Doing image ' num2str(i) ' out of ' num2str(nfiles)]);
    imag = WarpAndRotTA2(date,runn,i);
    enax = imag.enaxis;
    divax = imag.pixdiv*imag.yaxis;
    % and now the raw image with charge calibration done
    
    rawfilen = sprintf('%i/%ir%03i/%ir%03is%03i_Espec2.raw', date,date,runn,date,runn,i);
    rawim = ReadRAW16bit([datafolder rawfilen], 640, 480);
    %rawim = haircut2D_R1(rawim, 1.05);
    % Added Feb 2016
    if date==20131004
        filen = sprintf('%i/%ir%03i/%ir%03is%03i_Espec2.raw', date,date,2,date,2,1);
        dat = ReadRAW16bit([datafolder filen], 640, 480);
        logm = dat>5000;
        rawim(logm) = 0;
        sum(logm(:))
    end
    bgroi = [1 1 639 199];
    BG = createBackground(rawim, [1 200 639 200], bgroi, [], 4);
    % The weird looking slope at the edge:
    %rawim(450:end,:) = mean(mean(BG(1:200,:)));
    rawimBG = rawim - BG;
    % End of new stuff, Feb 2016
    %totQ = sum(sum(rawimBG(200:400,:)))*1.55; %WAS 7.7;, edit on 09/04/15
    %hbeam = rectangle('Position', [1 200 640 200], 'EdgeColor', 'r');
    
    if date ==20131010
        totQ = sum(sum(rawimBG(270:420,:)))*1.55; %WAS 7.7;, edit on 09/04/15
        %hbeam = rectangle('Position', [1 280 640 150], 'EdgeColor', 'r');
    else
        totQ = sum(rawimBG(:))*1.55; %WAS 7.7;, edit on 09/04/15
        %hbeam = rectangle('Position', [1 250 640 150], 'EdgeColor', 'r');
    end
    
    % Create the data structure
    shotdata.alongscreen = fliplr(imag.enpix(2,:) - 12/sind(45));
    shotdata.image = fliplr(imag.image);
    shotdata.divax = divax;
    shotdata.totQ = totQ*qe*1e12;
    shotdata.poo = 1; % Default to all shots being poo
    shotdata.screenID = 1;
    
    save(fullfile(savfol, sprintf('%i/mat/%03is%03i', date, runn, i)), 'shotdata');
end
%save([savfol 'eldata.mat'], 'eldata');