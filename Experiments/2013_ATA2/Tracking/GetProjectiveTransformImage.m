function q = GetProjectiveTransformImage(inputImage)

lwidth  = 3000; 
lheight = 1000;

imageSize = size(inputImage);

if(imageSize(1) == 1)
    if(strcmp(inputImage(end-2:end), 'raw'))
        inputImage = ReadRAW16bit(inputImage, 1280, 960);
    else
        inputImage = imread(inputImage);
    end
end

inputImage = double(inputImage);
imagesc(inputImage)
axis image xy
colormap(jet)
xlabel('u')
ylabel('v')
title('Input Image')
imageSize = size(inputImage);
udata = [1 imageSize(2)];
vdata = [1 imageSize(1)];
disp('Start at the bottom left and go clockwise')
imcontrast
for n = 1:4
    [x(n) y(n)] = ginput(1);
    if n > 1
        line([x(n-1) x(n)], [y(n-1) y(n)], 'Color', 'white')
        drawnow
    end
end
line([x(4) x(1)], [y(4) y(1)], 'Color', 'white')
drawnow

xp = [0 0 lwidth lwidth];
yp = [0 lheight lheight 0];

subplot(2,2,1)
imagesc(inputImage)

tform = maketform('projective',[ x(1) y(1); x(2) y(2); x(3) y(3); x(4) y(4)],...
                               [ xp(1) yp(1); xp(2) yp(2); xp(3) yp(3); xp(4) yp(4)]);
                           
u = 1:1:imageSize(2);
v = 1:1:imageSize(1);
for n = 1:length(u)
    for m = 1:length(v)
        J(m,n) = ProjectiveTransformJacobian(u(n), v(m), tform);
    end
end                           
                           
[outputImage,xdata,ydata] = imtransform(inputImage, tform, 'bicubic', ...
                                        'udata', udata,...
                                        'vdata', vdata,...
                                        'XYScale', 1);
[Jnew,xdata,ydata]        = imtransform(J, tform, 'bicubic', ...
                                        'udata', udata,...
                                        'vdata', vdata,...
                                        'XYScale', 1);
                                    
xaxis = round(xdata(1)):1:round(xdata(1))+length(outputImage(1,:))-1;
yaxis = round(ydata(1)):1:round(ydata(1))+length(outputImage(:,1))-1;

outputImage = outputImage./Jnew;
outputImage(isnan(outputImage)) = 0;
                                    
subplot(2,2,2)
imagesc(xaxis,yaxis,outputImage);
hold on
plot([xp xp], [yp yp], 'w')
hold off
axis image xy
xlabel('x')
ylabel('y')
title('Output Image')

subplot(2,2,3)
imagesc(J); axis image xy
xlabel('u')
ylabel('v')
title('Jacobian (u,v)')
subplot(2,2,4)
imagesc(xaxis, yaxis, Jnew); axis image xy
xlabel('u')
ylabel('v')
title('Jacobian (x,y)')

q.I = inputImage;
q.I2 = outputImage;
q.J = J;
q.J2 = Jnew;
q.tform = tform;
q.x = xaxis;
q.y = yaxis;
q.u = u;
q.v = v;
q.screenu = x;
q.screenv = y;
q.screenx = xp;
q.screeny = yp;

end