function im = WarpAndRotTA2(date, run, shot)

if (ismac)
    savfol = '/Users/kristjanpoder/Dropbox/Writing/ATA2_II_paper/';
    datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
    trackfile = '/Users/kristjanpoder/Dropbox/ICT_release/version1.0/TA2_2013_LundMag8.mat';
elseif (ispc)
    savfol = 'E:\Kristjan\Documents\Uni\Dropbox\Writing\ATA2_II_paper\';
    datafolder = 'E:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Dave\Data\';
    trackfile = 'E:\Kristjan\Documents\Uni\Dropbox\ICT_release\version1.0\TA2_2013_LundMag8.mat';
end

filen = sprintf('%i/%ir%03i/%ir%03is%03i_Espec2.raw', date,date,run,date,run,shot);
dataf = ReadRAW16bit([datafolder filen], 640, 480);
% Take out some hot pixels.
dataf = haircut2D_R1(haircut2D_R1(dataf,1.05),1.05);
% Change haircut to plain old cutting hot pixels, based on an image:
%if date==20131004
%    filen = sprintf('%i/%ir%03i/%ir%03is%03i_Espec2.raw', date,date,2,date,2,1);
%    dat = ReadRAW16bit([datafolder filen], 640, 480);
%    logm = dat>5000;
%    dataf(logm) = 0;
%end
% Old BG
%bg = dataf(1:100,:);
%dataf = dataf - mean(bg(:));
%dataf(400:480,:) = 1;
%dataf(1:200,:) = 1;
% Update Feb 2016 - slightly fancier BG 
bgroi = [1 1 639 199];
BG = createBackground(dataf, [1 201 639 200], bgroi, [], 4);
% The weird looking slope at the edge:
%dataf(450:end,:) = mean(mean(BG(1:200,:)));
dataf = dataf - BG;
load([datafolder sprintf('References/%i/espec2/refdata.mat', date)]);
cropim = ProjectiveTransformImageCropped(dataf, calibd.q);
rotim = imrotate(cropim, calibd.angle, 'bilinear');
pixdist = 61/(calibd.q.screeny(2)-calibd.q.screeny(1));
pixdiv = pixdist/(0.502+0.119+0.430);
im.image = rotim;
im.enaxis = calibd.enpix(3,:);
im.yaxis = (1:size(rotim,1))-calibd.y0;
im.pixdiv = pixdiv;
im.enpix = calibd.enpix;
