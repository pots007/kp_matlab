function q = GetProjectiveImageTransformTA2strict(image)

imagesc(image);
lines = zeros(2,2,3);
disp('Click on bottom left and right corners of lanex');
lines(:,:,1) = ginput(2); %line(:,1) - x; line(:,2) - y
disp('Click on top left and right corners of lanex');
lines(:,:,2) = ginput(2);

for i=1:2
    slope(i) = (lines(2,2,i)-lines(1,2,i))/(lines(2,1,i)-lines(1,1,i));
end

x0 = (lines(1,2,1)-lines(1,2,2)-lines(1,1,1)*slope(1)+lines(1,1,2)*slope(2))/(slope(2)-slope(1));
y0 = lines(1,2,1)+slope(1)*(x0-lines(1,1,1));
hold on;
line([x0 lines(2,1,1)], [y0 lines(2,2,1)])
line([x0 lines(2,1,2)], [y0 lines(2,2,2)])
line([x0 640], [y0 480]);
slope(3) = (480-y0)/(640-x0);
yprime = y0+slope(3)*(lines(1,1,1)-x0);
x(1:2)=lines(1,1,1); x(3:4)=640;
%y(1)=lines(1,2,1); y(2)=yprime;
%y(4)= y0+slope(1)*(640-x0); y(3)=480;
%now adding ones to get full screen area transformed
y(1) = lines(1,2,1);
y(2) = lines(1,2,2);
y(3) = y0+slope(2)*(640-x0);
y(4) = y0+slope(1)*(640-x0);
hold off;
imagesc([image; ones(ceil(y(3)-480), 640)])
hold on;
plot(x,y,'x', 'LineWidth', 2);

q = GetProjectiveTransformImageTA2(image, x, y);