%

% This script is meant to analyse the ATA2 II shots, taking into account
% the pointing offset and beam size measured by analyseATA2_II_pointing.m

% We load the already built reference files, but redo the energy axis. The
% reference files already contain a pixel to distance calibration. We can
% then figure out how big the errors in energy are.

%% Set up paths and load necessary files.

if (ismac)
    savfol = '/Users/kristjanpoder/Google Drive/Experiment/2013TA2/analysis';
    datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
    trackfile = '/Users/kristjanpoder/Dropbox/ICT_release/version1.0/TA2_2013_LundMag8.mat';
elseif (ispc)
    savfol = 'E:\Kristjan\Documents\Uni\Dropbox\Writing\ATA2_II_paper\';
    datafolder = 'E:\Kristjan\Documents\Uni\Imperial\2013_ATA2\Dave\Data\';
    trackfile = 'E:\Kristjan\Documents\Uni\Dropbox\ICT_release\version1.0\TA2_2013_LundMag8.mat';
end

load([datafolder '/References/20131010/espec2/refdata.mat']);
load(trackfile);
track_ax = load('ATA2_II_tracking_axial');
track_off = load('ATA2_II_tracking_offset');

%% Let's now look at the differences between old tracking and new one

% This is the fitted distance for every pixel from axis, along screen
% Have to subtract the offset from the axis!
ds = calibd.enpix(2,:) - (12/sin(0.25*pi));

ax.alongscreen = track_ax.tracking.screen(1).alongscreen(:,3);
ax.energies = track_ax.tracking.screen(1).alongscreen(:,1);
logm = ~isnan(ax.alongscreen);
ax.enfit = interp1(ax.alongscreen(logm), ax.energies(logm), ds, 'spline');

off.alongscreen = track_off.tracking.screen(1).alongscreen(:,3);
off.energies = track_off.tracking.screen(1).alongscreen(:,1);
logm = ~isnan(off.alongscreen);
off.enfit = interp1(off.alongscreen(logm), off.energies(logm), ds, 'spline');

figure(8); clf(8);
set(gca, 'NextPlot', 'add');
hh(1) = plot(calibd.enpix(1,:), ax.enfit, '--');
hh(2) = plot(calibd.enpix(1,:), off.enfit, '--r');
hh(3) = plot(calibd.enpix(1,:), calibd.enpix(3,:), '--k');
set(hh, 'LineWidth', 2);
set(gca, 'LineWidth', 2, 'Box', 'on');
xlabel('Pixel in warped image');
ylabel('Energy / MeV');
legend(hh, {'MagnetTracker, $\theta=0$ mrad', 'MagnetTracker, $\theta=5.7$ mrad', 'ICT, $\theta=0$ mrad'},...
    'Location', 'Northeast');
make_latex(8);
setAllFonts(8,20);
export_fig(fullfile(savfol, 'Tracking_old_new'), '-nocrop', '-transparent', '-pdf', 8)

%% And now let's look at beam divergence and resolution driven errors

figure(8); clf(8);
set(gca, 'NextPlot', 'add');
% The simple way to plot this
hh = zeros(1,5);
scrid = [3 2 4 3 3]; offset = [0 0 0 -1 1]; styles = {'-k', '--r', '.-r', '--g', '.-g'};
for i=1:5
    off.alongscreen = track_off.tracking.screen(1).alongscreen(:,scrid(i));
    off.energies = track_off.tracking.screen(1).alongscreen(:,1);
    logm = ~isnan(off.alongscreen);
    off.enfit = interp1(off.alongscreen(logm), off.energies(logm), ds+offset(i), 'spline');
    hh(i) = plot(calibd.enpix(2,:), off.enfit, styles{i});
end

%%

figure(8); clf(8);
set(gca, 'NextPlot', 'add');
% The simple way to plot this
hh = zeros(1,5);
scrid = [3 2 4 3 3]; offset = [0 0 0 -1 1]; styles = {'-k', '--r', '-r', '--g', '-g'};
off.alongscreen = track_off.tracking.screen(1).alongscreen(:,scrid(1));
off.energies = track_off.tracking.screen(1).alongscreen(:,1);
logm = ~isnan(off.alongscreen);
enfit0  = interp1(off.alongscreen(logm), off.energies(logm), ds+offset(1), 'spline');
for i=2:5
    off.alongscreen = track_off.tracking.screen(1).alongscreen(:,scrid(i));
    off.energies = track_off.tracking.screen(1).alongscreen(:,1);
    logm = ~isnan(off.alongscreen);
    off.enfit = interp1(off.alongscreen(logm), off.energies(logm), ds+offset(i), 'spline');
    %hh(i) = plot(calibd.enpix(2,:), off.enfit-enfit0, styles{i});
    hh(i) = plot(enfit0, off.enfit-enfit0, styles{i});
end

set(hh(2:5), 'LineWidth', 2);
set(gca, 'LineWidth', 2, 'Box', 'on', 'XLim', [50 500]);
grid on;
xlabel('Nominal energy / MeV');
ylabel('Energy error / MeV');
legend(hh(2:5), {'Beam size', 'Beam size', '-1 mm', '+1 mm'},...
    'Location', 'NorthWest');
make_latex(8);
setAllFonts(8,20);
export_fig(fullfile(savfol, 'Tracking_errors'), '-nocrop', '-transparent', '-pdf', 8)
