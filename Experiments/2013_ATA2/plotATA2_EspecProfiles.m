% Get all eprofiles for some day

date_ = '20131001r003';
if ispc
    datfol = 'E:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Eprofile';
elseif ismac
    datfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Eprofile';
end

flist = dir(fullfile(datfol, [date_ '*.raw']));
eprofiles = zeros(480, 640, length(flist));
hfig = figure(9); clf(hfig);
locs = GetFigureArray(4, 5, 0.02, 0.01, 'across');
for i=1:length(flist)
    eprofiles(:,:,i) = ReadRAW16bit(fullfile(datfol, flist(i).name), 640, 480);
    eprofiles(28,26:39,i) = 0;
    eprofiles(:,:,i) = eprofiles(:,:,i) - mean(mean(eprofiles(:,600:end,i)));
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add');
    imagesc(eprofiles(:,:,i), 'Parent', ax(i));
    lineH = sum(eprofiles(:,:,i),1);
    plot(ax(i), 1:640,  lineH*480/max(lineH), '-r');
    [~,edgeL, edgeR] = getCrappyBeamWidth(lineH, 0.25);
    plot(ax(i), edgeL*[1 1], [1 480], '-r', edgeR*[1 1], [1 480], '-r')
    rr = getCentroid(medfilt2(eprofiles(:,:,i)), 0.5);
    plot(rr(1), rr(2), 'xr');
    text(50, 50, num2str(i), 'Parent', ax(i));
    axis tight;
end
set(ax, 'XTick', [], 'YTick', [], 'YDir', 'normal');
