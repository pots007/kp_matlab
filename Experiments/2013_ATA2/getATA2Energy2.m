% This function returns the ATA2 energy, from the NF image, by assuming
% that the range of days it's valid for had the same amount of energy. It
% is basically there to allow for shot to shot fluctuations.

function en =  getATA2Energy2(date, run, shot)

if nargin==1 && ischar(date) && length(date)==16
    shot = str2double(date(14:16));
    run = str2double(date(10:12));
    date = str2double(date(1:8));
elseif nargin~=3    
    en = nan;
    return;
end

load('NFenergycalib');
dates = cell2mat({NFcalstruct.date});
runs = cell2mat({NFcalstruct.run});
logm = sum((1:length(dates)).*(dates==date & runs==run ));
if logm==0
    error('Structure has no data for this date. Valid values are');
end
en = NFcalstruct(logm).calibs(shot);
