load('mean_densities.mat');
load('II_shots_pressures');
[nums, texts, raws] = xlsread('E:\Kristjan\Documents\Uni\Imperial\2013[06]_ATA2\Dave\Shotsheets\20131108.xlsx');
pres = zeros(1,size(densities,2));
for i=1:size(densities,2)
    pres(i) = findcolumn(raws, sprintf('%ir%03is%03i', densities(1:3,i)),9);
end

figure(9); clf;
h = plot(pres(1:152),densities(4,1:152), 'o',...
    pres(153:end),densities(4,153:end), 'rx');
legend(h, {'He', 'He + 5% CO_2'}, 'Location', 'Northwest');
set(h, 'LineWidth', 1.5, 'MarkerSize', 10);
%errorbar(pres(1:152),densities(4,1:152), densities(5,1:152), 'o');
%hold on;
%errorbar(pres(153:end),densities(4,153:end), densities(5,153:end), 'rx');
%hold off;
SI_fit = polyfit(pres(1:152), densities(4,1:152), 1);
II_fit = polyfit(pres(153:end), densities(4,153:end), 1);
x_pres = 1:1:25;
hold on;
plot(x_pres, polyval(SI_fit, x_pres), '--');
plot(x_pres, polyval(II_fit, x_pres), '--r');
hold off;
ylabel('n_e ( 10^{19} cm^{-3})');
xlabel('Backing pressure (bar)');
setAllFonts(9,20);
%export_fig('F:\Kristjan\Documents\Uni\Dropbox\2013TA2\analysis\pressurecalib', ...
 %   '-transparent', '-nocrop', '-pdf', 9);