savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Espec2/withRAW+energy/';
datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
date = 20131010;
figure(120); clf;
set(120, 'Position', [100 100 900 1000], 'Color', 'w');%, 'PaperUnits', 'centimeters', 'PaperSize', [8.6 6]);
%axs = zeros(4,1);
divlims = [-30 30];
runn = 1;
load('invfire.mat');
load('ATA2_2013.mat');
if ~exist([savfol num2str(date)], 'dir')
    mkdir([savfol num2str(date)]);
    mkdir([savfol num2str(date) '/beams']);
    mkdir([savfol num2str(date) '/craps']);
end
nfiles = length(dir(sprintf('%s%i/%ir%03i/*Espec2.raw', datafolder, date, date, runn)));
eldata.(['D' num2str(date)]).data = zeros(nfiles, 5);
for i=91:nfiles
    if (i==91&&date==20131010) continue; end;
    figure(120); clf;
    disp(['Doing image ' num2str(i) ' out of ' num2str(nfiles)]);
    ax1 = axes('Position', [0.3 0.575 0.6 0.4]);
    imag = WarpAndRotTA2(date,runn,i);
    enax = imag.enaxis;
    divax = imag.pixdiv*imag.yaxis;
    co2disp = enax(1:end-1) - enax(2:end); %To correct for dispersion
    co2disp = repmat(co2disp, length(divax),1);
    linen = linspace(min(enax), max(enax), 1000); %Interpolate to linear grid to avoid pcolor
    image = imag.image(:,1:end-1)./co2disp;
    linimag = zeros(size(image,1),1000);
    for l=1:size(image,1)
        linimag(l,:) = interp1(enax(1:end-1), image(l,:), linen, 'pchip');
    end
    imagesc(linen, divax, abs(linimag), 'Parent', ax1);
    set(ax1, 'XLim', [min(linen) 300], 'CLim', [0 2e4], 'YTickLabel', [],...
        'YDir', 'normal', 'YLim', divlims, 'NextPlot', 'add');
    % Now the energy FWB plotting
    %en_fwb_bg = (sum(abs(linimag(divax>10&divax<-10,:)), 1));
    en_fwb = (sum(abs(linimag(divax<10&divax>-10,:)), 1));
    %en_fwb = en_fwb - en_fwb_bg;
    en_fwb1 = en_fwb/max(en_fwb)*0.5*(max(divax)-min(divax))+0.5*min(divax);
    plot(linen, en_fwb1, '--k', 'LineWidth', 1.5);
    % Finding the maximum energy: take the level above which it needs to be
    % smth random:
    %en_fwb(linen<250)
    %thold = mean(en_fwb(linen>500&linen<600));
    %mean(en_fwb(linen>500&linen<600))
    thold = 1.95e5;
    logm = (1:length(en_fwb)).*(en_fwb>thold);
    maxen = linen(max(logm));
    eldata.(['D' num2str(date)]).data(i,1) = maxen;
    plot([maxen maxen], divlims, '--k', 'LineWidth', .95);
    text(maxen+10, 25, ['E = ' num2str(maxen,'%3.1f') ' MeV'], 'FontSize', 20);
    [~,locs] = findpeaks(en_fwb, 'MINPEAKHEIGHT', 5*thold, 'MINPEAKDISTANCE', 25);
    if isempty(locs)
        locs = 1;
    end
    %linen(locs(end))
    plot([linen(locs(end)) linen(locs(end))], divlims, '--k', 'LineWidth', .95);
    text(linen(locs(end)), 20, sprintf('%2.1f MeV', linen(locs(end))),...
        'FontSize', 18);
    eldata.(['D' num2str(date)]).data(i,2) = linen(locs(end));
    eldata.(['D' num2str(date)]).data(i,4) = ATA2_2013.(['D' num2str(date)]).run(runn).shot(i).pressure;
    
    xlabel('Energy / MeV');
    cb = colorbar;
    set(cb, 'Position', [0.91 0.575 0.02 0.4]);
    ax2 = axes('Position', [0.1 0.575 0.19 0.4]);
    plot(sum(linimag,2), divax);
    set(ax2, 'YLim', divlims);
    ylabel('Divergence / mrad');
    % and now the raw image with charge calibration done
    ax3 = axes('Parent', 120, 'Position', [0.3 0.07 0.6 0.4]);
    rawfilen = sprintf('%i/%ir%03i/%ir%03is%03i_Espec2.raw', date,date,runn,date,runn,i);
    rawim = ReadRAW16bit([datafolder rawfilen], 640, 480);
    rawim = haircut2D_R1(rawim, 1.05);
    imagesc(rawim)
    set(ax3, 'XDir', 'reverse', 'XTick', [], 'YTick', [], 'NextPlot', 'add');
    bg1 = rawim(1:250,:); 
    rectangle('Position', [1 1 640 250]);
    bg2 = rawim(420:430,:);
    rectangle('Position', [1 400 640 30]);
    BG = mean([mean(bg1(:)) mean(bg2(:))]);
    %if date==20131010 BG = mean(bg1(:)); end;
    rawimBG = rawim - BG;
    if date ==20131010
        totQ = sum(sum(rawimBG(270:420,:)))*1.55; %WAS 7.7;, edit on 09/04/15
        hbeam = rectangle('Position', [1 280 640 150], 'EdgeColor', 'r');
    else
        totQ = sum(sum(rawimBG(250:400,:)))*1.55; %WAS 7.7;, edit on 09/04/15
        hbeam = rectangle('Position', [1 250 640 150], 'EdgeColor', 'r');
    end
    eldata.(['D' num2str(date)]).data(i,3) = totQ*qe*1e12;
    
    title(sprintf('Total charge Q = %1.3f pC', totQ*qe*1e12));
    cb2 = colorbar;
    set(cb2, 'Position', [0.91 0.07 0.02 0.4]);
    setAllFonts(120, 20);
    colormap(iijcm);
    if (totQ*qe*1e12>4.9)
        export_fig([savfol sprintf('%i/beams/%03is%03i', date, runn, i)], '-nocrop', '-png', 120);
    else
        export_fig([savfol sprintf('%i/craps/%03is%03i', date, runn, i)], '-nocrop', '-png', 120);
    end
    %saveas(120, [savfol sprintf('%i/fig/%03is%03i.fig', date, runn, i)]);
end
%save([savfol 'eldata.mat'], 'eldata');