% If set up properly, this will analyse 20131004r002

date_ = '20131004';
run_ = '001';

nshots = 35;
ttt = struct;
% Save as .mat for peak analysis
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 4);
% Save into the correct path
savepath = fullfile(getGDrive, 'Experiment', '2013_ATA2', 'EspecAnalysis', [date_ 'r' run_]);
%load('ATA2_20131010r001_pres');
load([date_ 'r' run_ '_zlocations']);
MagnetTrackerKP.fig.btrac.dat.outpath = fullfile(savepath, 'mat');
set(MagnetTrackerKP.fig.btrac.outpath_, 'String', fullfile(savepath, 'mat'));
datee = java.util.Date();
datee.setYear(2013-1900);
datee.setMonth(str2double(date_(5:6))-1);
datee.setDate(str2double(date_(7:8)));
MagnetTrackerKP.fig.input.date.setSelectedDate(datee);
MagnetTrackerKP.date_selected;
set(MagnetTrackerKP.fig.input.runbox, 'Value', str2double(run_));
MagnetTrackerKP.run_selected(str2double(run_));

set(MagnetTrackerKP.fig.btrac.Qmin1_, 'String', '50');

%%

for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
    ttt(i).shotID = i;
    ttt(i).laserE = getATA2Energy2(str2double(date_), str2double(run_), i);
    ttt(i).pres = 12;
    if i==9 || i==23; ttt(i).pres = 0; end;
    %ttt(i).ne = 0.09151*pres_(i) + 0.11374;
    ttt(i).ne = 0.08726*ttt(i).pres + 0.15267;
    ttt(i).zloc = zlocs(i);
    ttt(i).nozzleL = 3;
    ttt(i).Q1 = MagnetTrackerKP.fig.btrac.dat.QandE(1,1);
    ttt(i).E1 = MagnetTrackerKP.fig.btrac.dat.QandE(1,2);
    toc
end

save(fullfile(getGDrive, 'Experiment', '2013_ATA2', 'EspecAnalysis', [date_ 'r' run_], 'rundata'), 'ttt');
