% This script looks at data saved by MagnetTracker. It then tries to find
% maximum energy, by looking for first position above a certain amount of
% background. We then set the peak energy to be that of an electron with an
% offset angle of -1 mrad. This is a very rudimentary instrument function
% approach and should be approved upon at a later time.

% For the ATA2 data this process is slightly trickier. We first 

date_ = '20131004'; run_ = '002';

datfol = fullfile(getGDrive, 'Experiment', '2013_ATA2', 'EspecAnalysis', [date_ 'r' run_]);
savfol = datfol;
matfol = 'mat_20160704';

% Load first one, set up data structures
filen = dir(fullfile(datfol, matfol, '*Espec2.mat'));
fdat = load(fullfile(datfol, matfol, filen(1).name));
EspecData = zeros(length(filen), size(fdat.shotdata.image,2));
DivData = zeros(length(filen), size(fdat.shotdata.image,1));
poos = zeros(length(filen), 1);
% Set up tracking
% This is using the offset tracking calculated with II shots' offset
ff = load('ATA2_II_tracking_offset');
tracking = ff.tracking;
scrID = 1;
alongs = ff.tracking.screen(scrID).alongscreen;
logm = ~isnan(alongs);
tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');

%% And now loop through and load data
load(fullfile(datfol, 'rundata.mat'));
for i=1:length(filen)
    fprintf('Looking into file %i\n', i);
    fdat = load(fullfile(datfol, matfol, filen(i).name));
    tempim = medfilt2(fdat.shotdata.image);
    EspecData(i,:) = sum(tempim,1);
    DivData(i,:) = sum(tempim,2)';   
    poos(i) = fdat.shotdata.poo;
end

%% Filter out the poo shots. 
filtmat = repmat(~poos, [1 size(EspecData,2)]);
EspecData = EspecData.*filtmat;
% Filter out not poo shots, with too low peak pixel value
filtmat = repmat(max(EspecData, [], 2)>6e5, [1 size(EspecData,2)]);
%EspecData = EspecData.*filtmat;
filtmat = repmat(~poos, [1 size(DivData,2)]);
DivData = DivData.*filtmat;

%% And interpolate to a linear grid
npnts = 600;
alongscreens = fdat.shotdata.alongscreen;
logm = alongscreens>0;
enaxis0 = tracking.valfit(alongscreens(logm));
dE = diff(enaxis0); dE = [dE(1) dE'];
EspecDataCorr = EspecData(:,logm)./repmat(dE, length(filen), 1);

enaxis = linspace(min(enaxis0), max(enaxis0), npnts);
EspecDatalin = interp1(enaxis0, EspecData(:,logm)', enaxis', 'pchip');

% Now find the peak energy for each shot!
bgregion = 580:size(EspecData,2);
BGmean_ = mean(EspecData(:, bgregion),2);
BGstd_ = std(EspecData(:, bgregion), 0, 2);
BGfact_ = BGmean_ + 5*BGstd_;
maxind = ones(size(BGmean_));
for i=1:length(filen)
    temp = find(EspecData(i,:)>(BGfact_(i)), 1, 'last');
    if isempty(temp); continue; end; 
    maxind(i) = temp;
end
% Whilst this works generally well, it's shit for a few shots. Fixing these
% manually below:

peakEns = tracking.lowfit(fdat.shotdata.alongscreen(maxind));
peakEns_apparent = tracking.valfit(fdat.shotdata.alongscreen(maxind));
peakEns(maxind==1) = nan; %peakEns(155:167) = nan;
Qtots = cell2mat({ttt.Q1});
peakEns(Qtots<5) = nan;

peakEns_apparent(isnan(peakEns)) = nan;
% Try to overlay these on shot data!
figure(3); clf(3);
set(gca, 'NextPlot', 'add', 'YDir', 'normal');
imagesc(enaxis0, 1:length(filen), EspecDatalin');
plot(peakEns_apparent, 1:length(filen), 'dk');
colormap(3, 'parula');
axis tight;
set(gca, 'CLim', [0 1e5]);
%% And making a massive image of spectra
hfig = figure(4); clf(4);
set(4, 'Color', 'w', 'Position', [100 150 2100 1100]);
locs = GetFigureArray(10,7, 0.01, 0.01, 'across');
for i=1:length(filen)
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add', 'Box', 'on');
    plot(enaxis0, EspecData(i,logm), '-k', 'LineWidth', 1.5);
    plot(peakEns_apparent(i)*[1 1], [-3000 40000], '--r', 'LineWidth', 1.5);
    text(400, 0, num2str(i), 'FontSize', 16);
end
set(ax, 'XTickLabel', [], 'YTIckLabel', [], 'XLim', [200 2000], 'YLim', [-3000 40000],...
    'LineWidth', 1.5);
export_fig(fullfile(datfol, 'AllShotsPeaks'), '-nocrop', '-pdf', 4);

%% Now, all the preceding has been fun and sorting data out. 
% Now let's make some calculations and analysis, roughly as per usual

pps = cell2mat({ttt.ne});
Qtots = cell2mat({ttt.Q1}); % The high energy charge
[~,pp_unique] = hist(pps, 40);
pp_vs_ens = cell(size(pp_unique));
pp_vs_Qtots = cell(size(pp_unique));
binwidth = mean(diff(pp_unique));
for i=1:length(pp_unique)
    logm = abs(pps-pp_unique(i))<0.5*binwidth & ~isnan(peakEns') & Qtots>5; sum(logm);
    pp_vs_ens{i} = peakEns(logm);
    pp_vs_Qtots{i} = Qtots(logm);
end
pp_vs_ens_mean  = cellfun(@mean, pp_vs_ens); pp_vs_ens_std  = cellfun(@std, pp_vs_ens);
pp_vs_Qtots_mean  = cellfun(@mean, pp_vs_Qtots); pp_vs_Qtots_std  = cellfun(@std, pp_vs_Qtots);
laserE = cell2mat({ttt.laserE})';
laserEfact = laserE./mean(laserE(~isnan(laserE)));
laserEfact(isnan(laserEfact)) = 1;

% Also look at the zlocations!
zzs = cell2mat({ttt.zloc}); zzs(zzs==-8966) = -8967; % Fix the 1um error
zzs_unique = unique(zzs); zzs_vs_en = cell(size(zzs_unique));
for i=1:length(zzs_unique)
    logm = zzs_unique(i)==zzs;
    zzs_vs_en{i} = peakEns(logm);
    zzs_vs_Qtots{i} = Qtots(logm);
    zzs_nes{i} = pps(logm);
end
%% And the laser energies as well:
hfig = figure(33); clf(33); set(33, 'Color', 'w');
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
rectangle('Position', [0 mean(laserE)-std(laserE) length(filen)+1 2*std(laserE)],...
    'EdgeColor', 0.94*[1 1 1], 'FaceColor', 0.94*[1 1 1]);
plot([0 length(filen)+1], mean(laserE)*[1 1], '--k', 'LineWidth', 2);
plot(1:length(filen), laserE, 'ok', 'MarkerSize', 5, 'LineWidth', 1.5);
ylabel('Laser energy / J'); xlabel('Shot');
title(['$\bar{E} = ' num2str(mean(laserE)*1e3, '%2.0f') '\pm' num2str(std(laserE)*1e3, '%2.0f') '$mJ'])
setAllFonts(hfig, 20); make_latex(hfig);
set(gca, 'XLim', [-1 length(filen)+2]);
export_fig(fullfile(savfol, 'LaserEnergies'), '-nocrop', '-pdf', hfig);

%% Plot peak energy as a function of length

for i=1
    hfig = figure(33+i); clf(hfig); 
    set(hfig, 'Color', 'w', 'Position', [400 100 800 600]);
    set(gca, 'NextPlot', 'add', 'Box', 'on');
    %hens = myplotyy(gca, Ls, peakEns, laserE, 'dk','or');
    logm = Qtots>5;
    switch i
        case 1
            hens = plot(pps(logm), peakEns(logm), 'dk');
            hens_err = errorbar(pp_unique, pp_vs_ens_mean, pp_vs_ens_std, '-ro');
            legend([hens hens_err], {'Shot $\mathcal{E}$', '$\mathcal{E}\pm \sigma_{\mathcal{E}}$'}, 'Location', 'Northeast');
            ylabel('Peak energy / MeV');
            title('Peak electron energy vs $n_e$');
            fname = 'PeakEn_vs_ne';
        case 2
            hens = plot(pps(logm), Qtots(logm), 'dk');
            hens_err = errorbar(pp_unique, pp_vs_Qtots_mean, pp_vs_Qtots_std, '-ro');
            legend([hens hens_err], {'Shot $\mathcal{Q}$', '$\mathcal{Q}\pm \sigma_{\mathcal{Q}}$'}, 'Location', 'Northeast');
            ylabel('$Q_{tot}$ / MeV');
            title('Total beam charge vs $n_e$');
            fname = 'Qtot_vs_cellL';
        case 3
            for k=1:length(zzs_unique)
                %hens(k) = plot(zzs_nes{k}, zzs_vs_Qtots{k}, '-d');
                hens(k) = plot(zzs_nes{k}, zzs_vs_en{k}, '-d');
            end
            for k=1:5; leglabs{k} = ['$z=' num2str(zzs_unique(k)) '$']; end;
            legend(hens, leglabs)
            ylabel('Q total / pC')
        case 4
            
    end
    set(hens, 'MarkerSize', 10, 'LineWidth', 1.5);
    set(hens_err, 'MarkerSize', 10, 'LineWidth', 2);
    xlabel('$n_e$');
    grid on;
    setAllFonts(hfig, 20); make_latex(hfig);
    set(gca, 'Position', [0.15 0.13 0.77, 0.79], 'XLim', [0.5 2.2]);
    %export_fig(fullfile(savfol, fname), '-nocrop', '-pdf', hfig);
end

%% Create a struct for easier analysis later on
D1010r001.EspecData = EspecData;
D1010r001.neData = neData;
D1010r001.peakEns = peakEns;
D1010r001.Ls = Ls;
D1010r001.L_unique = L_unique;
D1010r001.Qhighs = Qhighs;
D1010r001.Qtots = Qtots;
D1010r001.L_vs_ens = L_vs_ens;
D1010r001.L_vs_Qtots = L_vs_Qtots;
D1010r001.L_vs_Qhighs = L_vs_Qhighs;
D1010r001.laserE = laserE;
