savfol = '/Users/kristjanpoder/Experiments/2013_ATA2/Analysis/Espec2/';
datafolder = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
date = 20131010;
figure(120); clf;
set(120, 'Position', [100 100 900 600]);%, 'PaperUnits', 'centimeters', 'PaperSize', [8.6 6]);
%axs = zeros(4,1);
divlims = [-30 30];
runn = 1;
load('invfire.mat');
for i=1:233
    if (i==91) continue; end;
    figure(120); clf;
    disp(['Doing image ' num2str(i)]);
    ax1 = axes('Position', [0.3 0.1 0.6 0.8]);
    imag = WarpAndRotTA2(date,runn,i);
    enax = imag.enaxis;
    divax = imag.pixdiv*imag.yaxis;
    co2disp = enax(1:end-1) - enax(2:end); %To correct for dispersion
    co2disp = repmat(co2disp, length(divax),1);
    linen = linspace(min(enax), max(enax), 1000); %Interpolate to linear grid to avoid pcolor
    image = imag.image(:,1:end-1)./co2disp;
    linimag = zeros(size(image,1),1000);
    for l=1:size(image,1)
        linimag(l,:) = interp1(enax(1:end-1), image(l,:), linen, 'pchip');
    end
    imagesc(linen, divax, -linimag);
    set(gca, 'XLim', [min(linen) 300], 'CLim', [0 2e4], 'YTickLabel', [],...
        'YDir', 'normal', 'YLim', divlims);
    xlabel('Energy / MeV');
    cb = colorbar;
    set(cb, 'Position', [0.91 0.1 0.02 0.8]);
    ax2 = axes('Position', [0.1 0.1 0.19 0.8]);
    plot(sum(linimag,2), divax);
    set(ax2, 'YLim', divlims);
    ylabel('Divergence / mrad');
    setAllFonts(120, 20);
    colormap(iijcm);
    export_fig([savfol sprintf('%i/png/%03is%03i', date, runn, i)], ...
        '-transparent', '-nocrop', '-png', 120);
    saveas(120, [savfol sprintf('%i/fig/%03is%03i.fig', date, runn, i)]);
end