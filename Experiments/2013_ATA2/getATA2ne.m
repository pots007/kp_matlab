% Returns plasma density for 2013ATA2 experiment

function ne = getATA2ne(gas, pres)
if strcmp(gas, 'II')
    ne = 0.09151*pres + 0.11374;
elseif strcmp(gas, 'SI')
    ne = 0.08726*pres + 0.15267;
else
    ne = [];
end
end