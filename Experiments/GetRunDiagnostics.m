
if (ismac)
    rootf = '/Users/kristjanpoder/Experiments/2013_ATA2/Data/';
elseif (ispc)
    rootf = '';
end

diaglist = {'laser_ff', 'laser_nf', 'forward_spectrum', 'x-ray', 'interferometer', 'shadowgraphy', 'exit_mode', 'input_energy', 'trans_energy', 'sidescatterspec', 'Espec1', 'Espec2', 'Eprofile', 'top_view'};
runslist = cell(300,1+length(diaglist));
id = 0;
dates = dir(rootf);
for i=3:length(dates)
    if (~dates(i).isdir || strcmp(dates(i).name, 'References'))
        continue;
    end
    runs = dir([rootf dates(i).name '/' dates(i).name '*']);
    for j=1:length(runs)
        if (~runs(j).isdir)
            continue;
        end
        id = id+1;
        runslist{id,1} = runs(j).name; 
        shots = dir([rootf dates(i).name '/' runs(j).name '/*s001*.*']);
        if (~isempty(shots))
            for k=1:length(shots)
                ind = sum(strcmp(diaglist,shots(k).name(18:end-4)).*(1:length(diaglist)));
                if (ind~=0)
                    runslist{id, ind+1} = 1;
                end
            end
        end
    end
end
runslist = runslist(1:id,:);

fid = fopen([rootf 'RunDiagnostics.txt'], 'w');
fprintf(fid, '   ');
for i=1:length(diaglist); fprintf(fid, ', %s', diaglist{i}); end;
fprintf(fid, '\n');
for i=1:size(runslist,1)
    fprintf(fid, '%s', runslist{i,1});
    for k=1:length(diaglist); fprintf(fid, ', %i', runslist{i,k+1}); end;
    fprintf(fid, '\n');
end
fclose(fid);