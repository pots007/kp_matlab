% calcExpected_a0s.m
%
% Will make a nice 2D map of the expected a0, for a fixed pulse length but
% varying spot size and total pulse energy.

E0 = 0.001:0.001:0.5;
focalL = 0.05:0.001:0.3;
beamD = 0.045;
tau = 30e-15;

lowrangeflag = 1;

% And do all the useful stuff
w0s = 0.823*0.8e-6*focalL/beamD;

[W0, E] = meshgrid(w0s, E0);

[~,a0s] = calcIntensity(E, tau, W0);

hfig = figure(34); clf(hfig); hfig.Color = 'w';
imagesc(focalL, E0, a0s)
set(gca, 'YDir', 'normal', 'Layer', 'top', 'Box', 'on', 'LineWidth', 1);
% We're mainly interested in the lower intensities, I think:
if lowrangeflag; set(gca, 'CLim', [0 10]); end;

grid on; grid minor;
makeNiceGridHG2(gca, 0.5, '-', 0.2, '--');

xlabel('Focal length / m');
ylabel('Pulse energy / J');
colormap(brewermap(256, 'Spectral'))

cb = colorbar;
ylabel(cb, '$a_0$');
make_latex(hfig); setAllFonts(hfig, 13);

savefilename = 'E:\Kristjan\Work\DESY\Experiments\2017_InverseThomsonScattering\a0';
if lowrangeflag; savefilename = [savefilename '_low']; end;
export_fig(savefilename, '-pdf', '-nocrop', hfig);
