% Plot all NIRspecs, batch mode!

datfol = '/Users/kristjanpoder/Experiments/2012GeminiElectron/NIRQuestData/data';
savfol = '/Users/kristjanpoder/Experiments/2012GeminiElectron/NIRQuestData/plots';
flist = dir(fullfile(datfol, '*.txt'));

hfig = figure(998);
set(hfig, 'Position', [100 100 600 400], 'Color', 'w');
for i=1:length(flist)
    fprintf('Plotting file %4i out of %i\n', i, length(flist));
    clf(hfig);
    try
        dataf = importdata(fullfile(datfol, flist(i).name));
        if isstruct(dataf)
            dat = dataf.data;
        else
            dat = dataf; 
        end
        plot(dat(:,1), smooth(dat(:,2)), 'LineWidth', 2);
        set(gca, 'XLim', [850 2100], 'LineWidth', 2);
        xlabel('Wavelength / nm');
        ylabel('Signal / a.u.');
        make_latex(hfig); setAllFonts(hfig, 14);
        drawnow;
        export_fig(fullfile([savfol '_pdf'], [flist(i).name(1:17) 'NIRQuest']), '-pdf', '-nocrop', hfig);
        export_fig(fullfile([savfol '_png'], [flist(i).name(1:17) 'NIRQuest']), '-png', '-nocrop', hfig);
    catch err
        disp(err.message);
    end
end