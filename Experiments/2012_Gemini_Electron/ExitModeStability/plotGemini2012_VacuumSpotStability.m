% plotGemini2012_VacuumSpotStability.m
%
% Plot the stability of the vacuum full power shots.

fdat_v = load('20121025_rn2_s98_s108_FullPowerVacuumShots');
fdat_a = load('20121115_f20StabRun1_focal_spot_images');

exitModeCal = 1.2;
focalSpotCamCal = 0.99;

vacPointing_x = fdat_v.focdata(13, 1:10)*exitModeCal;
vacPointing_y = fdat_v.focdata(14, 1:10)*exitModeCal;

airPointing_x = fdat_a.focdata(13, :)*focalSpotCamCal;
airPointing_y = fdat_a.focdata(14, :)*focalSpotCamCal;


hfig = figure(897); clf(hfig);
locs = GetFigureArray(1, 2, [0.05 0.02 0.3 0.1], 0.14, 'down');

axx(1) = axes('Parent', hfig, 'Position', locs(:,1));
hh = plot(1:10, vacPointing_x, 'd', 1:10, vacPointing_y, 's');
xlabel('Shot number'); ylabel('Centroid position / micron');
title('f/20 full power shots on exit mode diagnostic (20121025r002, 98-108)')
axx(2) = axes('Parent', hfig, 'Position', locs(:,2));
hh = plot(1:100, airPointing_x, 'd', 1:100, airPointing_y, 's');
xlabel('Shot number'); ylabel('Centroid position / micron');
title('f/20 focal spot diagnostic, 20121115/f20stabRun1')
%axx(3) = axes('Parent', hfig, 'Position', locs(:,3));

ddat = [rms(mean(vacPointing_x) - vacPointing_x),...
    rms(mean(vacPointing_y) - vacPointing_y),...
    rms(mean(airPointing_x) - airPointing_x),...
    rms(mean(airPointing_y) - airPointing_y)];

strr = {'Exit mode', 'Exit mode', 'Focal spot', 'Focal spot'};
coord = 'xyxy';
for i=1:4
    tt(i) = text(10, 155 - 6*i, sprintf('%s %s rms deviation from mean pos: %2.2f microns',...
        strr{i}, coord(i), ddat(i)), 'Parent', axx(2)); 
end

set(axx, 'LineWidth', 1.5);
make_latex(hfig); setAllFonts(hfig, 14);

fprintf(' Exit mode x rms deviation from mean pos: %2.2f micron\n', rms(mean(vacPointing_x) - vacPointing_x));
fprintf(' Exit mode y rms deviation from mean pos: %2.2f micron\n', rms(mean(vacPointing_y) - vacPointing_y));
fprintf(' Focal spot x deviation from mean rms pos: %2.2f micron\n', rms(mean(airPointing_x) - airPointing_x));
fprintf(' Focal spot y deviation from mean rms pos: %2.2f micron\n', rms(mean(airPointing_y) - airPointing_y));

export_fig(fullfile(getDropboxPath, 'MATLAB', 'experiments', '2012_Gemini_Electron',...
    'ExitModeStability', 'VacuumSpotStability'), '-pdf', '-nocrop', hfig);