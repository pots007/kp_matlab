% If set up properly, this will analyse 20150902r005


date_ = '20121108';
run_ = '002';

nshots = 42;
ttt = struct;
% Save as .mat for peak analysisa
set(MagnetTrackerKP.fig.btrac.ftype_, 'Value', 4);
% Save into the correct path
savepath = fullfile(getGDrive, 'Experiment', '2012_Gemini', 'EspecAnalysis', '20121108r002');

MagnetTrackerKP.fig.btrac.dat.outpath = fullfile(savepath, 'mat');
set(MagnetTrackerKP.fig.btrac.outpath_, 'String', fullfile(savepath, 'mat'));
datee = java.util.Date();
datee.setYear(2012-1900);
datee.setMonth(str2double(date_(5:6))-1);
datee.setDate(str2double(date_(7:8)));
MagnetTrackerKP.fig.input.date.setSelectedDate(datee);
MagnetTrackerKP.date_selected;
set(MagnetTrackerKP.fig.input.runbox, 'Value', str2double(run_));
MagnetTrackerKP.run_selected(str2double(run_));

%%

for i=1:nshots
    tic;
    disp(i);
    MagnetTrackerKP.shot_selected(i);
    MagnetTrackerKP.saveSpectraPlots;
    ttt(i).shotID = i;
    %ttt(i).GSN = getGSN(20150902, str2double(run_), i);
    %ttt(i).laserE = getenergy(ttt(i).GSN);
    ttt(i).pres = 150;
    ttt(i).cellL = 28;
    ttt(i).Q1 = str2double(get(MagnetTrackerKP.fig.btrac.Q1_, 'String'));
    ttt(i).Q2 = str2double(get(MagnetTrackerKP.fig.btrac.Q2_, 'String'));
    ttt(i).Q3 = str2double(get(MagnetTrackerKP.fig.btrac.Q3_, 'String'));
    ttt(i).E1 = str2double(get(MagnetTrackerKP.fig.btrac.E1_, 'String'));
    ttt(i).E2 = str2double(get(MagnetTrackerKP.fig.btrac.E2_, 'String'));
    ttt(i).E3 = str2double(get(MagnetTrackerKP.fig.btrac.E3_, 'String'));
    toc
    drawnow;
end

save(fullfile(savepath, 'rundata_basic'), 'ttt');