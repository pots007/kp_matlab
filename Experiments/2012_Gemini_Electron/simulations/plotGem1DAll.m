function him = plot1DAll(densit, a0)
Constants;
load divmap.mat;
simtype = 'data';
files = dir([simtype '/*.sdf']);
figure(2)
cla;
set(2, 'WindowStyle', 'docked');
dataf = GetDataSDFchoose([simtype '/' files(7).name], 'ey');
gridl = length(dataf.Electric_Field.Ey.data);
tims = zeros(length(files),1);
ne = zeros(length(files), gridl);
Ey = zeros(length(files), gridl);
omega = zeros(2^(nextpow2(gridl)+1), length(files));

for i=1:length(files)
    dataf = GetDataSDFchoose([simtype '/' files(i).name], 'grid');
    disp(files(i).name);
    tims(i) = dataf.time;
    x = dataf.Grid.Grid.x;
    x = x(1:length(x)-1);
    dataf = GetDataSDFchoose([simtype '/' files(i).name], 'ey');
    Eyl = dataf.Electric_Field.Ey.data;
    Ey(i,:) = abs(hilbert(Eyl));
    numpnts = 2^(nextpow2(length(Eyl))+1);
    omegal = fftX(x./c, Eyl, numpnts);
    omegaax = omegal(:,1);
    omega(:,i) = omegal(:,2).*conj(omegal(:,2));
    omega(:,i) = omega(:,i)./max(omega(:,i));
    dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
        'number_density/electron');
    ne(i,:) = dataf.Derived.Number_Density.electron.data;
end
fsiz = 14;
subplot(2,2,2)
imagesc(tims.*c,x-x(end), ne');
xlabel('Propagation distance (m)', 'FontSize', fsiz);
ylabel('\xi (\mu m)', 'FontSize', fsiz);
title(['n_e, a0=' a0 ' n_e=' densit]);
subplot(2,2,1)
imagesc(tims.*c,x-x(end), Ey');
xlabel('Propagation distance (m)', 'FontSize', fsiz);
ylabel('\xi (\mu m)', 'FontSize', fsiz);
title(['E_y, a0=' a0 ' n_e=' densit]);
subplot(2,2,3)
imagesc(tims.*c, omegaax, omega);
xlabel('Propagation distance (m)', 'FontSize', fsiz);
ylabel('\omega (rad/s)', 'FontSize', fsiz);
omega0 = 2*pi*c/8e-7;
set(gca, 'YLim', [1.8e15 3e15]);
title(['\omega, a0=' a0 ' n_e=' densit]);
saveas(2, 'output.fig');
export_fig('output.pdf', '-pdf', '-nocrop', '-transparent', 2);
him = 2;
end