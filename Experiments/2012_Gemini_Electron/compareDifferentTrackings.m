% Compare the different tracking results from ICT stdin, my code, and
% Jason's email to try to understand what's happening!

fMT0 = load('Gemini2012_tracking_axial_2mrad');
fMT1 = load('Gemini2012_tracking_axial_2mrad+51mmhigh');
fICT0 = load('ICT_tracking_Gemini2012deck');
fICT1 = load('ICT_tracking_Gemini2012deck_mods');


% From Jason's email:
ltrack1 = [342.97;308.02;274.96;244.77;217.26;192.34;169.93;149.83;131.91;115.90;101.71;89.06;77.89;68.07;59.43;51.84;45.191;39.361;34.267;29.809;];
Etrack1 = [200; 230.6373; 265.9678; 306.7105; 353.6944; 407.8756; 470.3566; 542.4089; 625.4986; 721.3165; 831.8124; 959.2348; 1106.2; 1275.6; 1471; 1696.4; 1956.2; 2255.9; 2601.5; 3000;];

ltrack2 = [1347.97701653316;1153.25864812398;1004.40319786117;881.085147480948;775.513788868382;681.806740070422;596.943467001929;515.674076577992;428.512238581140;339.630509650473;263.918805213566;202.940931625696;154.651390972633;117.049876204735;88.0875039892579;66.0774386680169;49.4113809149514;];
Etrack2 = [24.6107250565106;33.2276181949012;44.8615231112037;60.5687787807159;81.7755775677422;110.407461090613;149.064156158081;201.255625585706;271.720766908847;366.857696298730;495.304686736585;668.724508653783;902.863389847290;1218.98074644752;1645.77950210284;2222.01226511208;3000;];

y_1 = 44;
y_2 = 89;
% For screen1, plot the expected energy as a function of distance along the
% screen:
hfig = figure(89); clf(hfig); hfig.Color = 'w';
locs = GetFigureArray(1,2, [0.05 0.02 0.1 0.1], 0.07, 'down');
ax(1) = axes('Parent', hfig, 'Position', locs(:,1), 'NextPlot', 'add'); hh = [];
hh(1,1) = plot(fMT0.tracking.screen(1).alongscreen(:,3), fMT0.tracking.energies, 'o');
hh(1,2) = plot(fICT0.l(:,2)-(y_1+51)/sind(45), fICT0.E_e_MeV, '-x');
hh(1,3) = plot(fICT1.l(:,2)-y_1/sind(45), fICT1.E_e_MeV, '-v');
hh(1,4) = plot(fMT1.tracking.screen(1).alongscreen(:,3), fMT1.tracking.energies, 'o');
hh(1,5) = plot(ltrack1-y_1/sind(45), Etrack1, '-dk');
legend(hh(1,:), {'MagnetTracker', 'stdin Gemini2012.mat', 'stdin Gemini2012.mat,corr'...
    'MagnetTracker, wrong' 'Jason'});

ylabel('Electron energy / MeV');
ax(2) = axes('Parent', hfig, 'Position', locs(:,2), 'NextPlot', 'add');
hh(2,1) = plot(fMT0.tracking.screen(2).alongscreen(:,3), fMT0.tracking.energies, 'o');
hh(2,2) = plot(fICT0.l2(:,2)-(y_2+51)/sind(45), fICT0.E_e_MeV, '-x');
hh(2,3) = plot(fICT1.l2(:,2)-y_2/sind(45), fICT1.E_e_MeV, '-v');
hh(2,4) = plot(fMT1.tracking.screen(2).alongscreen(:,3), fMT1.tracking.energies, 'o');
hh(2,5) = plot(ltrack2-y_2/sind(45), Etrack2, '-dk');
ylabel('Electron energy / MeV');
xlabel('Distance from edge of screen / mm');
set(ax, 'XLim', [0 300], 'Box', 'on', 'LineWidth', 2);
set(hh, 'LineWidth', 1.25);
setAllFonts(hfig, 16);
export_fig(fullfile(getDropboxPath, 'MATLAB', 'experiments', '2012_Gemini_Electron', ...
   'tracking', 'CompareTrackings'), '-pdf', '-nocrop', hfig);