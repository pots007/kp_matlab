% For MagnetTracker, need to remove the F20 from Gemini 2012 data!!!!

if ismac
    datfol = '/Users/kristjanpoder/Experiments/2012GeminiElectron/EspecData';
elseif ispc
    datfol = 'E:\Kristjan\Documents\Uni\Imperial\2012[11]_Gemini\Data_days';    
end

flist1 = dir(fullfile(datfol, '2012*'));

for topin = 1:length(flist1)
    flist2 = dir(fullfile(datfol, flist1(topin).name, '2012*'));
    for midin = 1:length(flist2)
        fprintf('Working in folder %s/%s\n', flist1(topin).name, flist2(midin).name);
        flist3 = dir(fullfile(datfol, flist1(topin).name, flist2(midin).name, '2012*'));
        for botin=1:length(flist3)
            infname = fullfile(datfol, flist1(topin).name, flist2(midin).name, flist3(botin).name);
            outname = flist3(botin).name;
            outname([13 18:21]) = [];
            outname = fullfile(datfol, flist1(topin).name, flist2(midin).name, outname);
            movefile(infname, outname);
        end
    end
end