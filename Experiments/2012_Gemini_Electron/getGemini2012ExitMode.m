% getGemini2012ExitMode.m
%
% Returns the data for Gemini 2012 exit mode image
%
% Accepts either date, run, shot or correctly formatted shot string
% (YYYYMMDDrRRRsSSS)

function im = getGemini2012ExitMode(date,run,shot)
if nargin==1 && ischar(date) && length(date)==16
    shotstr = date;
elseif nargin==3
    shotstr = sprintf('%ir%03i_s%03i', date, run, shot);
else
    trace = nan;
    return;
end
if ispc
    datapath = 'E:\Kristjan\Documents\Uni\Imperial\2012[11]_Gemini\Data_days';
elseif ismac
    datapath = '/Volumes/Gemini2012/ServerData';
end
fname = fullfile(datapath, shotstr(1:8), shotstr(1:12), [shotstr '_F20_ExitMode.tif']);
try
    im = double(imread(fname));
catch err
    disp(err.message);
    im = nan(1024);
end
end