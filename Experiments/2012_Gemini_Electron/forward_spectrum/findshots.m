function shots = findshots(foldername, date, run)
names = dir([foldername '\' date 'r00' num2str(run) '*.tif']);
shots = 1;
k = 1;
for i=1:length(names)
    if (shots(k)~=str2double(names(i).name(15:17)))
        shots = [shots str2double(names(i).name(15:17))];
        k = k+1;
    end
end
shots;
end