Constants;
%% For 55 bar
shots = cell(7,1);
shots{1} = [11:14];      %-2000
shots{2} = [6:9];        %-1000
shots{3} = [1:5];        %-500
shots{4} = [15:19];      %0
shots{5} = [21,21,22,24,25]; %+500
shots{6} = [26:30];         %+1000
shots{7} = [31,33,34,35,36]; %+2000
titl = [-2000,-1000,-500,0,500,1000,2000];
resul = zeros(7,1024);
totcounts55 = zeros(1,7);
%DataPath = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\LaserFROGS\20121029\';
DataPath = 'F:\Kristjan\Documents\Uni\Dropbox\Writing\Goa_presentation\dazzler\datafiles\';

avs = zeros(128,7);
avs_spec = zeros(128,7);
maxs = zeros(1,7);
lengths = zeros(2,7);
errors = zeros(2,7);
for i=1:7
    figure(40+i);
    set(40+i, 'WindowStyle', 'docked');
    cla;
    temps = shots{i};
    maxs(i) = length(temps);
    temp = zeros(size(temps));
    temp2 = zeros(size(temps));
    for m=1:length(temps)
        load([DataPath 'FROGed\20121029r002_s' sprintf('%03i',...
            temps(m)) '_S_FROG.txt-Outputs.mat']);
        hold on;
        %plot(Outputs.lambdaPlot, Outputs.ILam);
        %plot(Outputs.tNew, abs(Outputs.Et).^2);
        plot(Outputs.lambdaPlot, Outputs.PhiLam-Outputs.PhiLam(51));
        avs(:,i) = avs(:,i) + abs(Outputs.Et);
        avs_spec(:,i) = avs_spec(:,i) + Outputs.ILam;
        temp(m) = Outputs.tFWHM;
        temp2(m) = Outputs.retError;
        %clear Outputs;
    end
    lengths(1,i) = mean(temp);
    lengths(2,i) = std(temp);
    errors(1,i) = mean(temp2);
    errors(2,i) = std(temp2);
    hold off;
    title(num2str(titl(i)));
    %xlim([750 850]);
end
figure(48)
for i=1:length(shots)
   avs(:,i) = avs(:,i)./maxs(i);
   avs_spec(:,i) = avs_spec(:,i)./max(avs_spec(:,i));
end
%h0 = plot(Outputs.tNew, avs);
%titls = {'-2000', '-1000', '-500', '0', '500', '1000'  , '2000'};
%legend(h0, titls)
imagesc(Outputs.tNew,1:7, avs')
set(gca, 'YTickLabel', titl);
ylabel('Dazzler setting (fs^2)');
xlabel('Delay (fs)');
xlim([-300 300]);
figure(49)
imagesc(Outputs.lambdaPlot,titl, avs_spec')
ylabel('Dazzler setting (fs^2)');
xlabel('Wavelength (nm)');
xlim([750 875]);

figure(50)
errorbar(titl, lengths(1,:), lengths(2,:))
xlabel('Dazzler setting (fs^2)');
ylabel('Pulse FWHM length (fs)');

figure(52)
errorbar(titl, errors(1,:), errors(2,:))
xlabel('Dazzler setting (fs^2)');
ylabel('Retrieval error');