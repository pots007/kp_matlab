% Take the spectrum and find peak
% Hence get the temperature from Wien's law.
clear;
current = pwd;
%cd('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Forward_spectrum\Forward_spectrometer_calibration');
Calibration;
Constants;
b = 2.8977721e-3; %Wien's displacement, in K^-1
narrow = double(imread('190um_slit_60s_exposure.tif'));
wide = double(imread('590um_slit_60s_exposure.tif'));
narrowbg = narrow(170:256, :);
narrowbg = mean(mean(narrowbg));
widebg = wide(170:256, :);
widebg = mean(mean(widebg));

%plot narrow slit raw data
figure(30);
cla;
imagesc(xaxis*1e9, 1:1:256, narrow);
title('Narrow slit raw data');
colorbar;
set(30, 'WindowStyle', 'docked');

%plot wide slit raw data
figure(31);
cla;
imagesc(xaxis*1e9, 1:1:256, wide);
title('Wide slit raw data');
colorbar;
set(31, 'WindowStyle', 'docked');

widesig = mean(wide(110:120,:))- widebg;
widesig = mean(wide(10:60,:)) - widebg;
temp = max(max(widesig));
widesig = widesig./temp;
widesig = smooth(widesig);
narrowsig = mean(narrow(110:120, :)) - narrowbg;
narrowsig = mean(narrow(10:60,:)) - narrowbg;
temp = max(max(narrowsig));
narrowsig = narrowsig./temp;
narrowsig = smooth(narrowsig);
%Plot the signals and their polynomial fits
figure(32);
cla;
order = 20;
narrowp = polyfit(xaxis*1e9, (narrowsig'), order);
narrowval = polyval(narrowp, xaxis*1e9);
widep = polyfit(xaxis*1e9, (widesig'), order);
wideval = polyval(widep, xaxis*1e9);
plot(xaxis*1e9, narrowsig);
hold on;
plot(xaxis*1e9, narrowval, 'k');
plot(xaxis*1e9, widesig, 'r');
plot(xaxis*1e9, wideval, 'c');
set(32, 'WindowStyle', 'docked');
title('Smoothed spectra from both slits');
axis tight;

%plot the Bentham calibration and a relevant black body curve
figure(35);
cla;
lamp =  importdata('7783_RAL_white_light.txt');
%lambda = lamp(:,1)*1e-9;
%temp = max(lamp(:,2));
h1 = plot(lamp(:,1), lamp(:,2), 'r');
%hold on;
%[temp ind] = max(lamp(:,2));
%T = b/(lamp(ind,1)*1e-9);
T =  3252; %, The correlated color temperature given by calibration
%T = 3800;
%B = 1e-9*2*h*c^2./(lambda.^5.*(exp(h*c./(lambda.*kB.*T)) - 1));
%temp = max(B);
%h2 = plot(lambda*1e9, B/temp);
%hold off;
title('Calibrated black body source');
xlabel('Wavelength (nm)');
ylabel('Irradiance (mW m^{-2} nm^{-1})');
legend(h1, 'Measured calibration lamp', 'Location', 'Southeast');
xlim([250 1000]);
%axis tight;
set(35, 'WindowStyle', 'docked');

figure(40);
set(40, 'WindowStyle', 'docked');
cla;
temp = max(narrowval);
h1 = plot(xaxis*1e9, narrowval/temp);
hold on;
temp = max(wideval);
h2 = plot(xaxis*1e9, wideval/temp, 'r');
lampval = interp1(lamp(:,1), lamp(:,2), xaxis*1e9);
temp = max(lampval);
h3 = plot(xaxis*1e9, lampval./temp, 'k');
legend([h1 h2 h3], 'Narrow slit', 'Wide slit', 'Blackbody calibration', ...
    'Location', 'South');
title('Normalised Shammie signals and calibration spectrum')
xlabel('Wavelength (nm)');
ylabel('Signal (arb un)');

% 
% 
% %plot the smoothed signals and black body from lamp
% figure(33);
% cla;
% h1 = plot(xaxis*1e9, widesig);
% hold on;
% 
% h2 = plot(xaxis*1e9, narrowsig, 'r');
% lambda = xaxis;
% B = 2*h*c^2./(lambda.^5.*(exp(h*c./(lambda.*kB.*T)) - 1));
% temp = max(B);
% B = B./temp;
% h3 = plot(xaxis*1e9, B, 'k');
% legend([h1 h2 h3], 'Narrow slit', 'Wide slit', 'Blackbody');
% %plot blackbody, adjusted by Shammiecalib
% set(33, 'WindowStyle', 'docked');
% figure(34);
% cla;
% widesig = mean(wide(110:120,:)) - widebg;
% widesig = mean(wide(10:60,:)) - widebg;
% temp = max(max(widesig));
% widesig = widesig./temp;
% widesig = smooth(widesig);
% h1 = plot(xaxis*1e9, widesig);
% hold on;
% narrowsig = mean(narrow(110:120, :)) - narrowbg;
% narrowsig = mean(narrow(10:60,:)) - narrowbg;
% temp = max(max(narrowsig));
% narrowsig = narrowsig./temp;
% narrowsig = smooth(narrowsig);
% h2 = plot(xaxis*1e9, narrowsig, 'r');
% 
% lambda = xaxis;
% B = 2*h*c^2./(lambda.^5.*(exp(h*c./(lambda.*kB.*T)) - 1));
% B = B.*calibresponse;
% temp = max(B);
% B = B./temp;
% h3 = plot(xaxis*1e9, B, 'k');
% legend([h1 h2 h3], 'Narrow slit', 'Wide slit', 'Blackbody, adjusted', ...
%     'Location', 'South');
% title('Signals with blackbody curve adjusted for Shammie');
% %Curves showing the signals and polynomials fitted to them.
% set(34, 'WindowStyle', 'docked');
% 
% %plot adjusted blackbody and polynomially fitted signals
% 
% 
% 
%Plot the distribution of light at TCC from Shammie response
figure(36)
cla;
calibresponse = Shammiecalib2(false);
narrowTCC = narrowval./calibresponse;
temp = max(narrowTCC);
h1 = plot(xaxis, narrowTCC./temp);
wideTCC = wideval./calibresponse;
temp = max(wideTCC);
hold on;
h2 = plot(xaxis, wideTCC./temp, 'r');
lambda = xaxis;
B = 2*h*c^2./(lambda.^5.*(exp(h*c./(lambda.*kB.*T)) - 1));
temp = max(B);
h3 = plot(xaxis, B./temp, 'k');
legend([h1 h2 h3], 'Narrow', 'Wide', 'Black body', ...
    'Location', 'Northwest');
title('Spectral distribution at TCC, calculated from spectral response');
set(36, 'WindowStyle', 'docked');


figure(37)
set(37, 'WindowStyle', 'docked');
h1 = plot(xaxis*1e9, calibresponse./max(calibresponse));
hold on;
title('Total calculated response from all optics in beamline vs calibration');
resp = (narrowval./max(narrowval))./(lampval./max(lampval));
h2 = plot(xaxis*1e9, resp./max(resp), 'k');
hold off;
legend([h1 h2], 'Calculated', 'Calibrated');
shamresp = [xaxis', lampval'];
save('../ShamrockResponse.mat', 'shamresp');

