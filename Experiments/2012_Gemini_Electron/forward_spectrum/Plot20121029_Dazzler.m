[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\20121121.xlsx');
fold = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Forward_spectrum\Server\';
exitmodefol = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Exit_mode\Exit_mode_imaging\Server_data\';
especfol = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Data_days\20121029\especcrops\';
xaxis = Calibration('new')*1e9;
hvac = open(['Analysis/20121023r001_s002.fig']);
hunguid = findobj(gca, 'Type', 'Line', 'Color', 'b');
vacshot = get(hunguid, 'YData');
close(hvac);
vacshot = vacshot./max(vacshot);
maxv = exp(-2)*max(vacshot);
i = 1;
while (vacshot(i) < maxv)
    i = i+1;
end
ee_left = xaxis(i);
ee_left_val = vacshot(i);
i = length(vacshot);
while (vacshot(i) < maxv)
    i = i-1;
end
ee_right = xaxis(i);
ee_right_val = vacshot(i);
%% 55 bar Dazzler + espec1s
shots = cell(7,1);
shots{1} = [11:14];      %-2000
shots{2} = [6:9];        %-1000
shots{3} = [1:5];        %-500
shots{4} = [15:19];      %0
shots{5} = [21,21,22,24,25]; %+500
shots{6} = [26:30];         %+1000
shots{7} = [31,33,34,35,36]; %+2000
titl = [-2000,-1000,-500,0,500,1000,2000];
totens55 = zeros(7,2);
spec55 = zeros(7, 1024);
spec55n = zeros(7, 1024);
positi2 = GetFigureArray(1, 7,0.04,0.01, 'down');
encalib = makecalibrationmatrix('20121025');
[la ind8] = min(abs(encalib - 800));
[la ind6] = min(abs(encalib - 600));
[la ind4] = min(abs(encalib - 400));
for i=1:length(shots)
    shotno = shots{i};
    tempspec = zeros(length(shotno),1024);
    focespec = zeros(200*length(shotno), 1093);
    for k=1:length(shotno)
        specnam = [fold '20121029r002_' sprintf('s%03i', ...
            shotno(k)) '_F20_ForwardSpectrometer.tif'];
        spec = imread(specnam);
        tempspec(k,:) = sum(spec(95:105,:));
        exitnam = [exitmodefol '20121029r002_' sprintf('s%03i', ...
            shotno(k)) '_F20_ExitMode.tif'];
        exitmode = imread(exitnam);
        ampen = findcolumn(raws, ['20121029r002_' sprintf('s%03i', ...
            shotno(k))], 22);
        totens55(i,1) = totens55(i,1) + sum(sum(exitmode))/ampen;
        totens55(i,2) = totens55(i,2) + (sum(sum(exitmode))/ampen)^2;
        espec1 = imread([especfol '20121029r002_' sprintf('s%03i',...
            shotno(k)) '_F20_Espec.tif']);%, 1280, 960);
        focespec((k-1)*200+1:k*200,:) = espec1(150:349,:);
    end
    totens55(i,1) = totens55(i,1)/length(shotno);
    temp = totens55(i,2)/length(shotno);
    totens55(i,2) = sqrt(temp - totens55(i,1)^2);
    spec55(i,:) = sum(tempspec);
    spec55n(i,:) = spec55(i,:)./max(spec55(i,:));
    figure(103);
    imagesc(focespec);
    set(gca, 'XTick', [], 'YTick', [], 'CLim', [0 600]);
    ylabel(num2str(titl(i)));
    hold on;
    plot([(ind8) (ind8)], [1 200*k], 'w', 'LineWidth', 1.5);
    plot([(ind6) (ind6)], [1 200*k], 'w', 'LineWidth', 1.5);
    plot([(ind4) (ind4)], [1 200*k], 'w', 'LineWidth', 1.5);
    hold off;
    ax1 = gca;
    hf2 = figure(21);
    ax2 = copyobj(ax1, hf2);
    set(ax2, 'Position', positi2(:,i));
    if (i==7)
        %set(get(ax2, 'Title'), 'String', num2str(focus(i)));
        %else
        set(ax2, 'XTick', [ind8 ind6 ind4], 'XTickLabel', [800 600 400]);
    end
end

figure(31)
imagesc(xaxis, 1:7, spec55n);
set(gca, 'YTickLabel', titl, 'XLim', [600 900])
ylabel('Dazzler setting (fs^2)');
xlabel('Wavelength (nm)');
cb = colorbar;
ylabel(cb, 'Spectral intensity (a.u.)');
hold on;
plot([ee_left ee_left], [0 8], '--k', 'LineWidth', 1.5);
plot([ee_right ee_right], [0 8], '--k', 'LineWidth', 1.5);
hold off;
reportfigure(31, 'halfpage', 'pdf', 'dazzlerscan55norm')
%% 75 bar Dazzler + espec1s
shots = cell(7,1);
shots{1} = [78,79,81,82];      %-2000
shots{2} = [71:76];        %-1000
shots{3} = [65,66,67,69,70];        %-500
shots{4} = [59:64];      %0
shots{5} = [83:86]; %+500
shots{6} = [87:92];         %+1000
shots{7} = [93:100]; %+2000
titl = [-2000,-1000,-500,0,500,1000,2000];
totens75 = zeros(7,2);
spec75 = zeros(7, 1024);
positi2 = GetFigureArray(1, 7,0.04,0.01, 'down');
spec75n = zeros(7, 1024);
encalib = makecalibrationmatrix('20121025');
[la ind8] = min(abs(encalib - 800));
[la ind6] = min(abs(encalib - 600));
[la ind4] = min(abs(encalib - 400));
for i=1:length(shots)
    shotno = shots{i};
    tempspec = zeros(length(shotno),1024);
    focespec = zeros(200*length(shotno), 1093);
    for k=1:length(shotno)
        specnam = [fold '20121029r002_' sprintf('s%03i', ...
            shotno(k)) '_F20_ForwardSpectrometer.tif'];
        spec = imread(specnam);
        tempspec(k,:) = sum(spec(95:105,:));
        exitnam = [exitmodefol '20121029r002_' sprintf('s%03i', ...
            shotno(k)) '_F20_ExitMode.tif'];
        exitmode = imread(exitnam);
        ampen = findcolumn(raws, ['20121029r002_' sprintf('s%03i', ...
            shotno(k))], 22);
        totens75(i,1) = totens75(i,1) + sum(sum(exitmode))/ampen;
        totens75(i,2) = totens75(i,2) + (sum(sum(exitmode))/ampen)^2;
        espec1 = imread([especfol '20121029r002_' sprintf('s%03i',...
            shotno(k)) '_F20_Espec.tif']);%, 1280, 960);
        focespec((k-1)*200+1:k*200,:) = espec1(150:349,:);
    end
    totens75(i,1) = totens75(i,1)/length(shotno);
    temp = totens75(i,2)/length(shotno);
    totens75(i,2) = sqrt(temp - totens75(i,1)^2);
    spec75(i,:) = sum(tempspec);
    spec75n(i,:) = spec75(i,:)./max(spec75(i,:));
    figure(103);
    imagesc(focespec);
    set(gca, 'XTick', [], 'YTick', [], 'CLim', [200 600]);
    ylabel(num2str(titl(i)));
    hold on;
    plot([(ind8) (ind8)], [1 200*k], 'w', 'LineWidth', 1.5);
    plot([(ind6) (ind6)], [1 200*k], 'w', 'LineWidth', 1.5);
    plot([(ind4) (ind4)], [1 200*k], 'w', 'LineWidth', 1.5);
    hold off;
    ax1 = gca;
    hf2 = figure(22);
    ax2 = copyobj(ax1, hf2);
    set(ax2, 'Position', positi2(:,i));
    if (i==7)
        %set(get(ax2, 'Title'), 'String', num2str(focus(i)));
        %else
        set(ax2, 'XTick', [ind8 ind6 ind4], 'XTickLabel', [800 600 400]);
    end
end

figure(32)
imagesc(xaxis, 1:7, spec75n);
set(gca, 'YTickLabel', titl, 'XLim', [600 900])
ylabel('Dazzler setting (fs^2)');
xlabel('Wavelength (nm)');
cb = colorbar;
ylabel(cb, 'Spectral intensity (a.u.)');
hold on;
plot([ee_left ee_left], [0 8], '--k', 'LineWidth', 1.5);
plot([ee_right ee_right], [0 8], '--k', 'LineWidth', 1.5);
hold off;
reportfigure(32, 'halfpage', 'pdf', 'dazzlerscan75norm')

%% Energies
figure(33)
%h55 = plot(titl, totens55, 'x', 'MarkerSize', 10, 'LineWidth', 2);
h55 = errorbar(titl, totens55(:,1), totens55(:,2), 'x', ...
    'MarkerSize', 10, 'LineWidth', 2);
hold on;
h75 = errorbar(titl, totens75(:,1), totens75(:,2), 'xr',...
    'MarkerSize', 10, 'LineWidth', 2);
hold off;
ylabel('Transmitted energy (a.u.)');
xlabel('Chirp \equiv Dazzler setting (fs^2)');
legend([h55 h75], '2.23 \times 10^{18} cm ^{-3}', ...
    '3.03 \times 10^{18} cm ^{-3}', 'Location', 'Southwest');
set(gca, 'XLim', [-2200 2200]);
%reportfigure(33, 'halfpage', 'pdf', 'dazzler_energies')