function shotenergy = findshotenergy(shotname, shotdata)
%shotname = names(183).name;
%shotname = names(184).name;
shotdate = str2double(shotname(1:8));
shotrun = str2double(shotname(10:12));
shotnumber = str2double(shotname(15:17));
use = shotdata(:,1)==shotdate;
temp = shotdata(use,2:4);
use2 = [temp(:,1)]==shotrun;
temp2 = temp(use2,2:3);
use3 = [temp2(:,1)]==shotnumber;
shotenergy = temp2(use3,2);
end