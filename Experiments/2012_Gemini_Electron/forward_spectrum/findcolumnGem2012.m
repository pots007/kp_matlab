% Get Dazzler setting from shotsheet
% Columns for Gemini Shotsheet:
% 1   date
% 2   run
% 3   shot
% 4   time
% 5   GSN
% 6   F20
% 7   waveplate pos
% 8   target
% 9   pres1
% 10  pres1 (mbar)
% 11  pres2
% 12  pres2
% 13  Dazzler
% 14  probe timing
% 15  commnets
% 16  gas jet z
% 17  parabola z
% 18  N amp en
% 19  N comp en
% 20  
% 21  
% 22  S amp en
% 23 comp en

function smth = findcolumnGem2012(raws, filename, col)
shotdate = str2double(filename(1:8));
shotrun = str2double(filename(10:12));
shotnumber = str2double(filename(15:17));
i=1;
while (shotdate ~= raws{i,1})
    i = i+1;
end
while (shotrun ~= raws{i,2})
    i = i+1;
end
while (shotnumber ~= raws{i,3})
    i = i+1;
end
smth = raws{i,col};
end
