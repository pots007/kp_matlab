[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\20121121.xlsx');

%at 75 bar
% nx = 7;
% ny = 6;
% positi = GetFigureArray(nx,ny,0.05,0.03, 'down');
% shots = 59:98;
% hf2 = figure(9);
% set(9, 'WindowStyle', 'docked');
% set(9,'PaperOrientation','landscape');
% clf;
% %continue
% prev = 0;
% prex = 1;
% olddaz = 0;
% for i=1:length(shots)
%     try
%         h0 = open(['Analysis/20121029r002_s0' num2str(shots(i)) '.fig']);
%     catch err
%         continue;
%     end
%     ax1 = gca;
%     titl = get(get(ax1, 'Title'), 'String');
%     if (i==1)
%         dazzler = findDazzler(raws, ['20121029r002_s0' num2str(shots(i))]);
%     else 
%         olddaz = dazzler;
%         dazzler = findDazzler(raws, ['20121029r002_s0' num2str(shots(i))]);
%     end
%     title([titl(9:30) ' ' num2str(dazzler) 'fs/s^2']);xlabel([]); ylabel([]);
%     hf2 = figure(9);
%     ax2 = copyobj(ax1, hf2);
%     if (olddaz == dazzler)
%         prev = prev + 1;
%         %set(ax2, 'XTickLabel', [], 'YTickLabel', []);
%     else
%         %set(ax2, 'YTickLabel', []);
%         prev = prex*ny + 1;% floor(i/ny);
%         prex = prex + 1;
%     end
% 
%     set(ax2, 'Position', positi(:,prev));
%      if (mod(i,ny)~=0)
%          set(ax2, 'XTickLabel', [], 'YTickLabel', []);
%      else
%          set(ax2, 'YTickLabel', []);
%      end
%      close(h0)
% end
% export_fig('Analysis/20121029r002_Dazzler75.pdf', '-transparent', '-nocrop', hf2)

%continue;
%at 55 bar
nx = 7;
ny = 6;
positi = GetFigureArray(nx,ny,0.05,0.03, 'down');
shots = 1:36;
hf2 = figure(9);
set(9, 'WindowStyle', 'docked');
set(9,'PaperOrientation','landscape');
clf;
%continue
prev = 0;
prex = 1;
olddaz = -500;
for i=1:length(shots)
    try
        h0 = open(['Analysis/20121029r002_s' sprintf('%03i',i) '.fig']);
    catch err
        continue;
    end
    ax1 = gca;
    titl = get(get(ax1, 'Title'), 'String');
    if (i==1)
        dazzler = findDazzler(raws, ['20121029r002_s' sprintf('%03i',i)]);
    else 
        olddaz = dazzler;
        dazzler = findDazzler(raws, ['20121029r002_s' sprintf('%03i',i)]);
    end
    title([titl(9:30) ' ' num2str(dazzler) 'fs/s^2']);xlabel([]); ylabel([]);
    hf2 = figure(9);
    ax2 = copyobj(ax1, hf2);
    if (olddaz == dazzler)
        prev = prev + 1;
        %set(ax2, 'XTickLabel', [], 'YTickLabel', []);
    else
        %set(ax2, 'YTickLabel', []);
        prev = prex*ny + 1;% floor(i/ny);
        prex = prex + 1;
    end

    set(ax2, 'Position', positi(:,prev));
     if (mod(i,ny)~=0)
         set(ax2, 'XTickLabel', [], 'YTickLabel', []);
     else
         set(ax2, 'YTickLabel', []);
     end
     close(h0)
end
export_fig('Analysis/20121029r002_Dazzler55.pdf', '-transparent', '-nocrop', hf2)

%close(hf2);
