function runs = findruns(foldername, date)
names = dir([foldername '\' date '*.tif']);
runs = 1;
k = 1;
for i=1:length(names)
    if (runs(k)~=str2double(names(i).name(10:12)))
        runs = [runs str2double(names(i).name(10:12))];
        k = k+1;
    end
end
runs;
end