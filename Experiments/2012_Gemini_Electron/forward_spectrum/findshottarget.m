function shottarget = findshottarget(shotname, shotdata)
%shotname = names(183).name;
%shotname = names(184).name;
%shotdate = str2double(shotname(1:8));
shotrun = str2double(shotname(10:12));
shotnumber = str2double(shotname(15:17));
%use = shotdata(:,1)==shotdate;
use = zeros(length(shotdata),1);
for i=1:length(shotdata)
    use(i) = strcmp(shotname(1:8), num2str(shotdata{i,1}));
end
use = logical(use);
temp = shotdata(use,2:4);
% temp = shotdata(use,2:4);
use2 = zeros(length(temp),1);
for i=1:length(temp)
    use2(i) = shotrun==temp{i,1};
end
use2 = logical(use2);
temp2 = temp(use2,2:3);
% use2 = [temp(:,1)]==shotrun;
% temp2 = temp(use2,2:3);
use3 = zeros(length(temp2),1);
for i=1:length(temp2)
    use3(i) = shotnumber==temp2{i,1};
end
use3 = logical(use3);
% use3 = [temp2(:,1)]==shotnumber;
shottarget = temp2{use3,2};
end