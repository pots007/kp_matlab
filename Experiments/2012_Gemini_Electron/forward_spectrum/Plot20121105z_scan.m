[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\20121121.xlsx');

nx = 7;
ny = 4;
positi = GetFigureArray(nx,ny,0.05,0.03, 'down');
%shots = 34:65;
hf2 = figure(9);
set(9, 'WindowStyle', 'docked');
set(9,'PaperOrientation','landscape');
clf;

focus = 17710:500:20710;
shots = [58:1:61; 34:1:37; 38:1:41; 42:1:45; 46:1:49; 50:1:53; 54:1:57];

for i=1:nx
    for j=1:ny
        try
            h0 = open(['Analysis/20121105r001_s' sprintf('%03i',shots(i,j)) '.fig']);
        catch err
            continue;
        end
        ax1 = gca;
        titl = get(get(ax1, 'Title'), 'String');
        foc = findzposition(raws, ['20121105r001_s' sprintf('%03i',shots(i,j))]);
        title([titl(9:31) ' ' num2str(foc) 'mm']);xlabel([]); ylabel([]);
        hf2 = figure(9);
        ax2 = copyobj(ax1, hf2);
        
        set(ax2, 'Position', positi(:,(4*(i-1)+j)));
        if (mod(j,4)~=0)
            set(ax2, 'XTickLabel', [], 'YTickLabel', []);
        else
            set(ax2, 'YTickLabel', []);
        end
        close(h0)
    end
end
export_fig('Analysis/20121105r001_zscan.pdf', '-transparent', '-nocrop', hf2)

%close(hf2);
