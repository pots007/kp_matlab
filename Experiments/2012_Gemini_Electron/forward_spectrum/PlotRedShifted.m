%% Get Data
[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\20121121.xlsx');

Shots1 = { '20121023r001_s007', 
 '20121023r001_s008', 
 '20121023r001_s015', 
 '20121023r003_s017', 
 '20121023r003_s037', 
 '20121023r003_s038', 
 '20121023r003_s043', 
 '20121023r004_s008', 
 '20121023r004_s019', 
 '20121023r004_s020', 
 '20121024r001_s014', 
 '20121024r001_s027', 
 '20121024r001_s034', 
 '20121024r001_s035', 
 '20121024r001_s038', 
 '20121024r001_s043', 
 '20121024r001_s047', 
 '20121024r001_s049', 
 '20121024r001_s065', 
 '20121024r002_s014', 
 '20121024r002_s017', 
 '20121024r002_s023', 
 '20121024r002_s036', 
 '20121024r002_s037', 
 '20121024r002_s040', 
 '20121024r002_s049', 
 '20121024r002_s054', 
 '20121025r001_s013', 
 '20121025r002_s012', 
 '20121025r002_s013', 
 '20121025r002_s018', 
 '20121025r002_s020', 
 '20121025r002_s021', 
 '20121025r002_s027', 
 '20121025r002_s029', 
 '20121025r002_s034', 
 '20121025r002_s036', 
 '20121025r002_s042', 
 '20121025r002_s043', 
 '20121025r002_s044', 
 '20121025r002_s045', 
 '20121025r002_s049', 
 '20121025r002_s055', 
 '20121025r002_s057', 
 '20121025r002_s059', 
 '20121025r002_s060', 
 '20121025r002_s064', 
 '20121025r002_s070', 
 '20121025r002_s072', 
 '20121025r002_s074', 
 '20121025r002_s076', 
 '20121025r002_s077', 
 '20121025r002_s085', 
 '20121025r002_s086'}



Shots = [18, 20, 21, 27, 29, 34, 36, 42, 43, 44, 45, 49, 55, 57,...
    59, 60, 64, 70, 72, 74, 76, 77, 85, 86];
ydat = zeros(1024, length(Shots));
pres = zeros(1, length(Shots));
for i=1:length(Shots)
    namfig = ['20121025r002_s' sprintf('%03i',Shots(i)) '.fig']; 
    pres(i) = findpressure(raws, namfig);
    h0 = open(['Analysis/' namfig]);
    guidpntr = findobj(gca, 'Type', 'Line', 'Color', 'r');
    xdata = get(guidpntr, 'XData');
    temp = get(guidpntr, 'YData');
    ydat(:,i) = temp./max(temp);
    close(h0);
end
%% Plot data
[sortpres I] = sort(pres);
ydats = ydat;
ydats = ydats(:,I);
imagesc(xdata, sortpres, ydats')
set(gca, 'YTick', linspace(min(pres), max(pres), length(Shots)),...
    'YTickLabel', sortpres);
set(gca, 'XLim', [7e-7 9e-7]);