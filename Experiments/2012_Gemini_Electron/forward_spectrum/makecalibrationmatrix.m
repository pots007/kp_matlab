function encalib = makecalibrationmatrix(date)

%this will turn lanex counts into pC of charge, but you will still need to
%do the background subtraction!

%See excel file "Espec Pixel Size"

matrix_name = 'TRANSdata_calibration';
%Choose an appropriated name then pick one of the following:
switch(date)
    case '20121019'
        % % 20121019- this one's not very accurate. The keen observer will note that
        % % both of them are the same- only one for the top exists but this makes it
        % % easier to code
        polyfittop = [-0.00040084,0.342845477];
        polyfitbot = [-0.00040084,0.342845477];
        w = 1099;
        h = 533;
        
    case '20121025'
        %20121025
        polyfittop = [-0.0003837,0.33790969];
        polyfitbot = [-0.0004115,0.34171639];
        w = 1093;
        h = 540;
        
        
    case '20121031'
        % %20121031
        polyfittop = [-0.00040084,0.342845477];
        polyfitbot = [-0.00040084,0.342845477];
        % %Same as 20121019 because cropped region is most like this date- no good
        % %reference shot available.
        w = 1086;
        h = 540;
        
    case '20121109'
        % %20121109
        polyfittop = [-0.000407506,0.359066734];  %top ruler
        polyfitbot = [-0.000439189,0.364309076];  %bottom ruler
        w = 1014; %width of cropped lanex image
        h = 501;    %height of cropped lanex image
        
        % %stepdatabefore09
        % polyfittop = [-0.00040084,0.342845477];
        % polyfitbot = [-0.00040084,0.342845477];
        % w = 1083; %width of cropped lanex image
        % h = 537;    %height of cropped lanex image
    case 'stepbefore09'
        % %stepdata09
        polyfittop = [-0.000407506,0.359066734];  %top ruler
        polyfitbot = [-0.000439189,0.364309076];  %bottom ruler
        w = 1083; %width of cropped lanex image
        h = 537;    %height of cropped lanex image
        
        % %TRANSdata (for the transformed images using Jason's code)
        % polyfittop = [0,300/3001];  %top ruler
        % polyfitbot = [0,300/3001];  %bottom ruler
        % %constant pixel size in transformed case. 10cm = 1000px
        % w = 3001; %width of cropped lanex image
        % h = 1001;    %height of cropped lanex image
end        
        calmatrix = zeros(h,w); %creates matrix full of ones
        calmatrix = calmatrix + 1;
        d = 0;
        encalib = zeros(1,w);
        lmin = 62.2;    %See JW lab book 2 18/03/13
        
        conversion = 3.3e-5;
        %converts lanex counts into pC
        
        Evsd_coeff = [1501.557059 -23.60017545 0.275417727 -0.002088139 9.24805e-6 -2.14572e-8 2.00306e-11];
        
        for i=1:w
            
            pxsizetop = polyfittop(1)*d + polyfittop(2);
            pxsizebot = polyfitbot(1)*d + polyfitbot(2);
            pxsize = (pxsizetop + pxsizebot)/2;
            %calculates the physical pixel size in mm at this location
            
            d = d+pxsize;   %where we are physically from the bottom of the lanex
            l = d+lmin; %... from the laser axis
            theta = atan(l/(sqrt(2)*(753-(l/sqrt(2)))));  %these are lower case L's not ones
            
            E = Evsd_coeff(7)*d^6 + Evsd_coeff(6)*d^5 + Evsd_coeff(5)*d^4 + Evsd_coeff(4)*d^3 + Evsd_coeff(3)*d^2 + Evsd_coeff(2)*d + Evsd_coeff(1);
            encalib(i) = E;
            %energy of electrons at this position assuming they all come from
            %z = magnet centre
            
            S = 1 - (1e-4)*E;
            %Normalised (to 1MeV level) sensitivity of lanex at this energy. The fit
            %isn't great for low energies but all the energies here are 100's of
            %MeV so that's fine.
            
            calmatrix(:,i) = conversion*calmatrix(:,i)*cos((pi/4)-theta)/S;
            %now multiply (.*) the elements of this matrix with the elements of the actual image
            
        end
        
        %save(strcat(matrix_name, '.mat'), 'calmatrix');
        
end