Constants;
data = SpectrumData(405:835,:);
figure(7)
plot(data(:,1), data(:,2));
omega = 2*pi*c./data(:,1);
counts = 2*pi*c./data(:,1).^2.*data(:,2);
time = ifftX(omega, counts, 2^14);
axis tight
figure(8)
timesig = time(:,2).*conj(time(:,2));
plot(time(:,1), timesig);
fwhm = widthfwhm(timesig');
fwhm = fwhm*1e15*(time(1,1)-time(2,1));
title(['$|\mathcal{F}(\omega)|^2$, transfrom limited width ' num2str(fwhm) ' fs'], ...
    'Interpreter', 'latex');
set(gca, 'XLim', [0 1e-13]);
omegasig = 2*pi*c*data(:,2)./omega.^2;
%clf
%plot(omega, omegasig)
axis tight