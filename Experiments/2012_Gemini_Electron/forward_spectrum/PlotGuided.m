%Modified 30/05/2013 to involve the Shamrock spectral calibration.

figure(10);
cla;
load ShamrockResponse.mat;
h1 = plot(unguided(:,1), unguided(:,2)./shamresp(:,2), 'b');
legend(h1, 'Unguided', 'Location', 'Northwest');
if(~isempty(guided))
    hold on;
    h2 = plot(guided(:,1), guided(:,2)./shamresp(:,2), 'r');
    legend([h1 h2], 'Unguided', 'Guided', 'Location', 'Northwest');
end
xlabel('Wavelength (nm)');
ylabel('Counts (arb un)');
title(specshotname, 'Interpreter', 'none');
axis tight;
saveas(h1, ['Analysis\' specshotname(1:17) '.fig']);