% Take the spectrum and find peak
% Hence get the temperature from Wien's law.
clear;
current = pwd;
cd('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Forward_spectrum\Forward_spectrometer_calibration');
Calibration;
Constants;
b = 2.8977721e-3; %Wien's displacement, in K^-1
narrow = double(imread('190um_slit_60s_exposure.tif'));
wide = double(imread('590um_slit_60s_exposure.tif'));
narrowbg = narrow(170:256, :);
narrowbg = mean(mean(narrowbg));
widebg = wide(170:256, :);
widebg = mean(mean(widebg));
%plot narrow slit raw data
figure(30);
cla;
imagesc(xaxis*1e9, 1:1:256, narrow);
title('Narrow slit');
colorbar;
set(30, 'WindowStyle', 'docked');
%plot wide slit raw data
figure(31);
cla;
imagesc(xaxis*1e9, 1:1:256, wide);
title('Wide slit');
colorbar;
set(31, 'WindowStyle', 'docked');
%plot the Bentham calibration and a relevant black body curve
figure(35);
cla;
white = importdata('optical diagnostics\optical diagnostics\White_light_bentham.txt');
lambda = white(:,1)*1e-9;
temp = max(white(:,2));
hold on;
h1 = plot(white(:,1), white(:,2)./temp, 'r');
[temp ind] = max(white(:,2));
T = b/(white(ind,1)*1e-9);
%T =  3276; %, The correlated color temperature given by calibration
%T = 3800;
B = 2*h*c^2./(lambda.^5.*(exp(h*c./(lambda.*kB.*T)) - 1));
temp = max(B);
h2 = plot(lambda*1e9, B/temp);
title(['Blackbody curve for T=' num2str(T)]);
xlabel('Wavelength (nm)');
ylabel('Irradiance (arb un)');
legend([h1 h2], 'Measured', 'Black body curve');
xlim([250 1000]);
set(35, 'WindowStyle', 'docked');
%plot the smoothed signals and black body from lamp
figure(33);
cla;
widesig = mean(wide(110:120,:))- widebg;
widesig = mean(wide(10:60,:)) - widebg;
temp = max(max(widesig));
widesig = widesig./temp;
widesig = smooth(widesig);
h1 = plot(xaxis*1e9, widesig);
hold on;
narrowsig = mean(narrow(110:120, :)) - narrowbg;
narrowsig = mean(narrow(10:60,:)) - narrowbg;
temp = max(max(narrowsig));
narrowsig = narrowsig./temp;
narrowsig = smooth(narrowsig);
h2 = plot(xaxis*1e9, narrowsig, 'r');
lambda = xaxis;
B = 2*h*c^2./(lambda.^5.*(exp(h*c./(lambda.*kB.*T)) - 1));
temp = max(B);
B = B./temp;
h3 = plot(xaxis*1e9, B, 'k');
legend([h1 h2 h3], 'Narrow slit', 'Wide slit', 'Blackbody');
%plot blackbody, adjusted by Shammiecalib
set(33, 'WindowStyle', 'docked');
figure(34);
cla;
widesig = mean(wide(110:120,:)) - widebg;
widesig = mean(wide(10:60,:)) - widebg;
temp = max(max(widesig));
widesig = widesig./temp;
widesig = smooth(widesig);
h1 = plot(xaxis*1e9, widesig);
hold on;
narrowsig = mean(narrow(110:120, :)) - narrowbg;
narrowsig = mean(narrow(10:60,:)) - narrowbg;
temp = max(max(narrowsig));
narrowsig = narrowsig./temp;
narrowsig = smooth(narrowsig);
h2 = plot(xaxis*1e9, narrowsig, 'r');
calibresponse = Shammiecalib2(false);
lambda = xaxis;
B = 2*h*c^2./(lambda.^5.*(exp(h*c./(lambda.*kB.*T)) - 1));
B = B.*calibresponse;
temp = max(B);
B = B./temp;
h3 = plot(xaxis*1e9, B, 'k');
legend([h1 h2 h3], 'Narrow slit', 'Wide slit', 'Blackbody, adjusted', ...
    'Location', 'South');
title('Signals with blackbody curve adjusted for Shammie');
%Curves showing the signals and polynomials fitted to them.
set(34, 'WindowStyle', 'docked');
figure(32);
cla;
order = 20;
narrowp = polyfit(xaxis*1e9, (narrowsig'), order);
narrowval = polyval(narrowp, xaxis*1e9);
widep = polyfit(xaxis*1e9, (widesig'), order);
wideval = polyval(widep, xaxis*1e9);
plot(xaxis*1e9, narrowsig);
hold on;
plot(xaxis*1e9, narrowval, 'k');
plot(xaxis*1e9, widesig, 'r');
plot(xaxis*1e9, wideval, 'c');
%plot adjusted blackboddy and polynomially fitted signals
set(32, 'WindowStyle', 'docked');
figure(40);
cla;
temp = max(narrowval);
h1 = plot(xaxis*1e9, narrowval/temp);
hold on;
temp = max(wideval);
h2 = plot(xaxis*1e9, wideval/temp, 'r');
h3 = plot(xaxis*1e9, B, 'k');
legend([h1 h2 h3], 'Narrow slit', 'Wide slit', 'Blackbody, adjusted', ...
    'Location', 'South');
title(['Blackbody of T=' num2str(T) ', adjusted for Shammie, with ' ...
    'polynomial fitting for signal']);
%Plot the distribution of light at TCC from Shammie response
set(40, 'WindowStyle', 'docked');
figure(36)
cla;
narrowTCC = narrowval./calibresponse;
temp = max(narrowTCC);
h1 = plot(xaxis, narrowTCC./temp);
wideTCC = wideval./calibresponse;
temp = max(wideTCC);
hold on;
h2 = plot(xaxis, wideTCC./temp, 'r');
lambda = xaxis;
B = 2*h*c^2./(lambda.^5.*(exp(h*c./(lambda.*kB.*T)) - 1));
temp = max(B);
h3 = plot(xaxis, B./temp, 'k');
legend([h1 h2 h3], 'Narrow', 'Wide', 'Black body', ...
    'Location', 'Northwest');
title('Spectral distribution at TCC, calculated from spectral response');
set(36, 'WindowStyle', 'docked');
figure(37)
plot(xaxis*1e9, calibresponse);
title('Total response from all optics in beamline');
cd (current);
set(37, 'WindowStyle', 'docked');