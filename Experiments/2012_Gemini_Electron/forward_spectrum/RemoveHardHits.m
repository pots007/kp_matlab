%Removes hard hits from CCD image
function clearimage = RemoveHardHits(I)
[xsize, ysize] = size(I);
thold = 6;
fsize = 4;
xcur = fsize + 1;
good = true;
ggood = true;
while (good)
    ycur = fsize + 1;
    while (ggood)
        selection = I(xcur-fsize:xcur+fsize,ycur-fsize:ycur+fsize);
        selmed = mean(mean(double(selection)));
        selection(selection>thold*selmed)=selmed;
        I(xcur-fsize:xcur+fsize,ycur-fsize:ycur+fsize) = selection(:,:);
        ycur = ycur + fsize;
        if((ysize - ycur)<fsize)
            ggood = false;
        end
    end
    ggood = true;
    xcur = xcur + fsize;
    if((xsize - xcur)<fsize)
        good = false;
    end
end
clearimage = I;
end