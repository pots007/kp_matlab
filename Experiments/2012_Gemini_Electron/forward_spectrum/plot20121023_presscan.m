[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\20121121.xlsx');

nx = 5;
ny = 4;
positi = GetFigureArray(nx,ny,0.05,0.03, 'down');

pres = 5:5:100;
shots = [1:4; 5:8; 9:12; 13:16; 17:20; 21:24; 25:28; 29,31,32,33; ...
    34:37; 38:41; 42:45; 46:49; 50:53; 54:57; 58:61; 62:65; 66:69; ...
    70:73; 74:77; 78:81];

hf2 = figure(9);
set(9, 'WindowStyle', 'docked');
set(9,'PaperOrientation','landscape');
unguided = zeros(numel(shots), 1024);
m = 0;
for k=1:4
    clf;
    for i=1:nx
        for j=1:ny
            l = (k-1)*5+i;
            m = m+1;
            try
                h0 = open(['Analysis/20121023r003_s' sprintf('%03i',shots(l,j)) '.fig']);
            catch err
                continue;
            end
            ax1 = gca;
            hunguid = findobj(ax1, 'Type', 'Line', 'Color', 'b');
            unguided(m, :) = get(hunguid, 'YData');
            unguided(m,:) = unguided(m,:);%./max(unguided(m,:));
            titl = get(get(ax1, 'Title'), 'String');
            title([titl(9:31)]); xlabel([]); ylabel([]);
            hf2 = figure(9);
            ax2 = copyobj(ax1, hf2);
            set(ax2, 'Position', positi(:,(4*(i-1)+j)));
            if (mod(j,4)~=0)
                set(ax2, 'XTickLabel', [], 'YTickLabel', []);
            else
                set(ax2, 'YTickLabel', []);
            end
            close(h0)
        end
    end
    %     if (k==1)
    %         export_fig('Analysis/20121023r003_presscan.pdf', '-transparent', '-nocrop', hf2)
    %     else
    %         export_fig('Analysis/20121023r003_presscan.pdf', '-transparent', ...
    %             '-append', '-nocrop', hf2)
    %     end
end
%%
hvac = open(['Analysis/20121023r001_s002.fig']);
hunguid = findobj(gca, 'Type', 'Line', 'Color', 'b');
vacshot = get(hunguid, 'YData');
close(hvac);
vacshot = vacshot./max(vacshot);
Constants
load divmap.mat
figure(17)
%set(gcf, 'Colormap', divmap);
unguidaver = zeros(20,1024);
for i=1:20
    unguidaver(i,:) = unguided((i-1)*4+1,:) + unguided((i-1)*4+2,:) + unguided((i-1)*4+3,:) + unguided((i-1)*4+4,:);
end
unguidaver = unguidaver./4;
unguidaver(5,:) = unguidaver(5,:)*2;
for i=1:20
    unguidaver(i,:) = unguidaver(i,:)./max(unguidaver(i,:));
end
xaxis = Calibration('new');
density = (pres .* 4e16 + 3e16);
den = density*1e-18;
%imagesc(density, xaxis, unguidaver');
%set(gca, 'YDir', 'normal');
fsiz = 14;
imagesc([0, den], xaxis*1e9,  [vacshot; unguidaver]');
%set(gca, 'XTick', [0, den], 'XTickLabel', [0, den], 'FontSize', fsiz);
set(gca, 'FontSize', fsiz);
hold on;
omega0 = 2*pi*c/8.0e-7;
a0 = 4;
omegap = omegapcalc(density);%./sqrt(1+ 0.25*a0^2);
lambdap = 2*pi*c./(omega0-omegap);
%plot(lambdap, den, '+k', 'MarkerSize', 15, 'LineWidth', 2);
% plot(2*pi*c./meanomega, [0, den], '+k', 'MarkerSize', 15, 'LineWidth', 2);
% plot(2*pi*c./edgeomega(:,1), [0, den], 'xb', 'MarkerSize', 15, 'LineWidth', 2);
% plot(2*pi*c./edgeomega(:,2), [0, den], 'xr', 'MarkerSize', 15, 'LineWidth', 2);

hold off;
%set(gca, 'XLim', [6e-7 9e-7]);
%set(gca, 'CLim', [0.1 1]);
xlabel('Plasma density/ 10^{18} cm^{-3}', 'FontSize', fsiz);
ylabel('Wavelength / nm', 'FontSize', fsiz);
%export_fig('Analysis/presscan.pdf', '-transparent', '-nocrop', 11);
reportfigure(17, 'halfpage', 'pdf', 'presscan');

%% Do the red edge, blue edge thing
denaxis = [0, den];
unguidaver2 = [vacshot; unguidaver];
for i=1:size(unguidaver2)
    unguidaver2(i,:) = unguidaver2(i,:)./max(unguidaver2(i,:));
end
%Thresholding to remove low counts bias
unguidaver2(unguidaver2<0.1) = 0;
omegaaxis = 2*pi*c./xaxis;
unguidomega = repmat(2*pi*c./xaxis.^2,21,1).*unguidaver2;
meanomega = zeros(21,1);
%plot(omegaaxis, unguidomega(1,:));
rededges = cumsum(unguidomega.*hbar.*repmat(omegaaxis,21,1), 2);
totalsums = sum(unguidomega.*hbar.*repmat(omegaaxis,21,1),2);
bluedges = cumsum(fliplr(unguidomega.*hbar.*repmat(omegaaxis,21,1)),2);
edgeomega = zeros(21,2); %(:,1) are blue edges, (:,2) are red edges
for i=1:21
    %means
    meanomega(i) = trapz(omegaaxis,unguidomega(i,:).*omegaaxis)./trapz(omegaaxis, unguidomega(i,:));
    %blue edges
    [la in] = min(abs(0.135*totalsums(i)-rededges(i,:)));
    edgeomega(i,1) = omegaaxis(in);
    %red edges
    [la in] = min(abs(0.135*totalsums(i)-bluedges(i,:)));
    edgeomega(i,2) = omegaaxis(1024-in);
end
figure(23)
%set(23, 'Position', [100 100 600 600]);
h1 = plot(denaxis, (meanomega-meanomega(1))./meanomega(1), '*-k');
hold on;
h2 = plot(denaxis, (edgeomega(:,1)-meanomega(1))./meanomega(1), '+-b');
h3 = plot(denaxis, (edgeomega(:,2)-meanomega(1))./meanomega(1), 'x-r');
hold off;
legend([h1 h3 h2], 'Mean', 'Red', 'Blue', ...
    'Location', 'Northwest');
ylabel('(\omega - \omega_0)/\omega_0', 'FontSize', fsiz);
xlabel('Plasma density (10^{18} cm^{-3})', 'FontSize', fsiz);
set(gca, 'FontSize', fsiz, 'XLim', [0 4.05], 'YLim', [-0.1 0.45]);
measedges = [denaxis' edgeomega(:,1) meanomega edgeomega(:,2)];