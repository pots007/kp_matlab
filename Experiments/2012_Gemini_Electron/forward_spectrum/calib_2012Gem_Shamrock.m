%Calibration for Shammie. 'old' for pre 23102013, 'new' for later.

function xaxis = calib_2012Gem_Shamrock(time)
if(strcmp(time, 'old'))
    pixels = [107 235 304 572 670 692 779 806 960];   
end
if (strcmp(time, 'new'))
    %The change occured on 23102013
    pixels = [98 226 295 564 661 684 770 797 952];
end
wavelengths = [435.8 507.3 546.1 696.6 750.8 763.5 811.5 826.5 912.3];
wavelengths = wavelengths.*1e-9;
coef = polyfit(pixels', wavelengths', 3);
pix = linspace(1, 1023, 1024);
xaxis = polyval(coef, pix);
end