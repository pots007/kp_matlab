
function refl = PpolR(mat, angle, xaxis)
%mat = 'BK7';
%angle = 45;
thetai = angle /180 * pi;
n1 = 1;
if (strcmp(mat, 'BK7'))
    %For BK7
    B1 = 1.03961212;
    B2 = 0.231792344;
    B3 = 1.01046945;
    C1 = 6.00069867e-3;
    C2 = 2.00179144e-2;
    C3 = 1.03560653e2;
end
if (strcmp(mat, 'FusedSilica'))
    %For fused silica
    B1 = 0.696166300;
    B2 = 0.407942600;
    B3 = 0.897479400;
    C1 = 4.67914826e-3;
    C2 = 1.35120631e-2;
    C3 = 97.9340025;
end
%lambda = 300:1:1000;
%lambda = lambda*1e-3; %lambda needs to be in um
lambda = xaxis*1e6;
n2 = 1 + B1*lambda.^2./(lambda.^2-C1) + ...
    B2*lambda.^2./(lambda.^2-C2) + ...
    B3*lambda.^2./(lambda.^2-C3);
n2 = sqrt(n2);
Rs = (n1.*sqrt(1-(n1./n2.*sin(thetai)).^2) - n2.*cos(thetai)).^2 ./ ...
    (n1.*sqrt(1-(n1./n2.*sin(thetai)).^2) + n2.*cos(thetai)).^2;
%figure(10);
%plot(lambda, Rs);
refl = Rs;
end