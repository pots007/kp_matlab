date = '20121105';
files = dir([date '*.fig']);
nx = 5;
ny = 5;
positi = GetFigureArray(nx,ny,0.05,0.05, 'down');
nfig = ceil(length(files)/(nx*ny));
savef = [date '.pdf'];
for k=1:nfig
    hf2 = figure(k+1);
    set(k+1, 'WindowStyle', 'docked');
    set(k+1,'PaperOrientation','landscape');
    clf;
    for i=1:nx*ny
        try
            h0 = open(files((k-1)*nx*ny+i).name);
        catch err
            break;
        end
        ax1 = gca;
        titl = get(get(ax1, 'Title'), 'String');
        title(titl(9:31));xlabel([]); ylabel([]);
        hf2 = figure(k+1);
        ax2 = copyobj(ax1, hf2);
        set(ax2, 'Position', positi(:,i));
        if (mod(i,ny)~=0)
            set(ax2, 'XTickLabel', [], 'YTickLabel', []);
        else
            set(ax2, 'YTickLabel', []);
        end
        close(h0)
    end
    if (k==1)
        export_fig(savef, '-transparent', '-nocrop', hf2) 
    else
        export_fig(savef, '-transparent', '-append', '-nocrop', hf2)
    end
    close(hf2);
end