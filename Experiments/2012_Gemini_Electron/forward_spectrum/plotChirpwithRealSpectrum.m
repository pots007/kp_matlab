close all;
fold = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Forward_spectrum\Server\';
%exitmodefol = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Exit_mode\Exit_mode_imaging\Server_data\';
%especfol = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Data_days\20121029\especcrops\';
xaxis = Calibration('new');
hvac = open(['Analysis/20121023r001_s002.fig']);
hunguid = findobj(gca, 'Type', 'Line', 'Color', 'b');
vacshot = get(hunguid, 'YData');
close(hvac);
vacshot = vacshot./max(vacshot);
npnts = 2^10;
logm = xaxis>7.65e-7;
xaxis = xaxis(logm);
vacshot = vacshot(logm);
%xaxis(logm) = 0;
%vacshot(xaxis<7.6e-7) = 0;
logm = xaxis<8.4e-7;
xaxis = xaxis(logm);
vacshot = vacshot(logm);
lambda0 = 8e-7;
omega0 = 2*pi*3e8/lambda0;
%omega = linspace(omega0-6e14, omega0+6e14, npnts/4);
omega = 2*pi*c./xaxis;
spec = sqrt(2*pi*c*vacshot./xaxis.^2);
%bandwidthfwhm = 32e-9;
bandwidth = 2*pi*3e8/(8e-7)^2*bandwidthfwhm/(2*sqrt(log(2)));
specphase = zeros(1, length(omega));
%specphase = specphase + (omega-omega0).*100e-15;
ddphi = -1000*1e-30;
zeroph = specphase + 0.5*(omega-omega0).^2.*(200e-30) + ...
    1/6*(omega-omega0).^3*25000e-45;% - 1/24*(omega-omega0).^4*250000e-60;
specphase = specphase + 0.5*(omega-omega0).^2.*ddphi;
specphase = specphase + zeroph;
%specphase = specphase - (omega-omega0).^3.*4000e-45;
flatphase = spec.*exp(1i*zeroph);
spectrum = spec.*exp(1i*specphase);
%spectrum = spectrum.*abs(sin(omega.*7e-14));

figure(1);
set(1, 'WindowStyle', 'docked');
[la minloc] = min(abs(omega - omega0));
zerophase = specphase(minloc);
specphase = specphase - zerophase;
specphase(abs(spectrum)<0.1*max(abs(spectrum))) = nan;
plot(omega, specphase);
[AX H1 H2] = plotyy(omega, (abs(spectrum)).^2, omega, specphase);
set(get(AX(1),'Ylabel'), 'String', 'Spectral intensity (a.u.)');
set(get(AX(2),'Ylabel'), 'String', 'Spectral phase (rad)');
set(AX(1), 'XLim', [2.1e15 2.7e15]);
set(AX(2), 'XLim', [2.1e15 2.7e15]);

figure(2)
cla;
set(2, 'WindowStyle', 'docked');
tranlim = ifftX(omega, flatphase, npnts);
timesig = ifftX(omega, spectrum, npnts);
tim = timesig(:,1)*1e15;
h1 = plot(tim, timesig(:,2).*conj(timesig(:,2)));
hold on;
h2 = plot(tim, tranlim(:,2).*conj(tranlim(:,2)), 'r');
legend([h1 h2], 'Chirped', 'Transform limited');
set(gca, 'XLim', [-1500 1500]);
tlim = widthfwhm(tranlim(:,2)'.*conj(tranlim(:,2))')*(tranlim(2,1)-tranlim(1,1));
tstr = widthfwhm(timesig(:,2)'.*conj(timesig(:,2))')*(timesig(2,1)-timesig(1,1));
title(['tranlim: ' num2str(tlim*1e15) ', stretched ' num2str(tstr*1e15)]);
xlabel('Time (fs)');
ylabel('Intensity (a.u.)');
xlim([-500 500]);

figure(5) 
set(5, 'WindowStyle', 'docked');
plot(xaxis, vacshot);


%figure(5);
%set(5, 'WindowStyle', 'docked');
%plot(omega, sum(wign,2));
%set(gca,'YDir','rev');
% tlim = sqrt(0.5)*widthfwhm(abs(tranlim(:,2))')*(tranlim(2,1)-tranlim(1,1));
% tcalc = sqrt(0.5)*0.44*8e-7^2/(c*bandwidthfwhm)*2; %sqrt(0.5) as looking at intensity fwhm
% tout = sqrt(tlim^4+16*(log(2))^2*(ddphi)^2)/tlim;
% fprintf('Transform limit is %2.2f and tout is %2.2f fs\n', ...
%     tcalc*1e15, tout*1e15);

