%Based on the graphclicked data, interpolates all efficiency values for the
%optics in the Shammie. If variable is set to true, then plots all
%efficiency curves as well. For randomly polarised light all the way down.
function calibrationcurve = Shammiecalib2(plotvar)
Calibration;
xaxis = xaxis*1e9;
cameradata = importdata('camera_QE.csv');
cameradata = cameradata.data;
cameradata(:,2) = cameradata(:,2)*1e-2;
cameracurve = interp1(cameradata(:,1), cameradata(:,2), xaxis, 'linear');
gratingdata = importdata('grating_efficiency2.csv');
gratingdata = gratingdata.data;
gratingdata(:,2) = gratingdata(:,2)*1e-2;
gratingcurve = interp1(gratingdata(:,1), gratingdata(:,2), xaxis, 'linear');
mirrordata = importdata('mirror_reflectivity_Mg2.csv'); %Al mirrors
%mirrordata = importdata('mirror_reflectivity_Ag.csv'); %Ag mirrors
mirrordata = mirrordata.data;
mirrordata(:,2) = mirrordata(:,2)*1e-2;
mirrorcurve = interp1(mirrordata(:,1), mirrordata(:,2), xaxis, 'linear');
AG45data = xlsread('SilverCoating_SP_45AOI.xlsx');
AG45data(:,4) = AG45data(:,4)*1e-2; %Column 4 has the average of S and P R
AG45curve = interp1(AG45data(:,1), AG45data(:,4), xaxis, 'linear');
AG0data = xlsread('P01_0deg_20um_Reflectance.xlsx');
AG0data(:,2) = AG0data(:,2)*1e-2;
AG0curve = interp1(AG0data(:,1)*1e3, AG0data(:,2), xaxis, 'linear');
%Inside the Shammie
totresponse = mirrorcurve.*gratingcurve.*mirrorcurve.*cameracurve;
%And now all the optics the way down
totresponse = totresponse.*0.5.*(SpolR('BK7', 10, xaxis)+PpolR('BK7', 10, xaxis))  ... %holey wedge
    .*0.5.*(SpolR('BK7', 40, xaxis)+PpolR('BK7', 40, xaxis)) ... %First wedge reflection
    .*AG0curve ... %Spherical mirror
    .*(1 - 0.5.*(SpolR('BK7', 40, xaxis)+PpolR('BK7', 40, xaxis))) ... %Transmission through first wedge
    .*AG45curve.^2 ... %Two 235mm mirrors
    .*(1 - 0.5.*(SpolR('BK7', 45, xaxis)+PpolR('BK7', 45, xaxis))) ... %Transmission through second wedge
    .*AG0curve ... %Spherical mirror
    .*0.5.*(SpolR('BK7', 45, xaxis)+PpolR('BK7', 45, xaxis)) ... %Second wedge reflection
    .*AG45curve.^3 ... %A 235mm mirror, 2 4" mirrors
    .*0.5.*(SpolR('BK7', 45, xaxis)+PpolR('BK7', 45, xaxis)) ... %Third wedge reflection
    .*(1 - 0.5.*(SpolR('BK7', 45, xaxis)+PpolR('BK7', 45, xaxis))) ... %Transmission through farfield wedge
    .*AG0curve ... %Odd angle mirror
    .*0.5.*(SpolR('BK7', 10, xaxis)++PpolR('BK7', 10, xaxis)) ... %The wedge for energy spectrum
    .*(1-0.5.*(SpolR('FusedSilica', 3, xaxis)+PpolR('FusedSilica', 3, xaxis))); %transmission through lens

if(plotvar)
figure(9);
cla;
h1 = plot(xaxis, totresponse);
hold on;
h2 = plot(xaxis, gratingcurve, 'k');
h3 = plot(xaxis, mirrorcurve, 'r');
h4 = plot(xaxis, cameracurve, 'c');
h5 = plot(xaxis, AG45curve, 'g');
legend([h1 h2 h3 h4 h5], 'Total', 'Grating', 'Mirror', 'Camera', 'Ag mirror')
xlabel('Wavelength (nm)');
ylabel('Efficiency');
end
calibrationcurve = totresponse;
end