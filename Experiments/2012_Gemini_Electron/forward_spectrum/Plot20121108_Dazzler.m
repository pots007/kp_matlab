[nums texts raws] = xlsread('F:\Kristjan\Documents\Uni\Imperial\2012_OctNov_Gemini\20121121.xlsx');
fold = 'F:\Kristjan\Documents\Uni\Imperial\2012_OctNov_Gemini\Forward_spectrum\Server\';
exitmodefol = 'F:\Kristjan\Documents\Uni\Imperial\2012_OctNov_Gemini\Exit_mode\Exit_mode_imaging\Server_data\';
especfol = 'F:\Kristjan\Documents\Uni\Imperial\2012_OctNov_Gemini\Data_days\20121108\especcrops\';
xaxis = Calibration('new');
hvac = open(['Analysis/20121023r001_s002.fig']);
hunguid = findobj(gca, 'Type', 'Line', 'Color', 'b');
vacshot = get(hunguid, 'YData');
close(hvac);
vacshot = vacshot./max(vacshot);
maxv = exp(-2)*max(vacshot);
i = 1;
while (vacshot(i) < maxv)
    i = i+1;
end
ee_left = xaxis(i);
ee_left_val = vacshot(i);
i = length(vacshot);
while (vacshot(i) < maxv)
    i = i-1;
end
ee_right = xaxis(i);
ee_right_val = vacshot(i);
%% 55 bar Dazzler + espec1s
load invfire.mat
shots = cell(10,1);
shots{1} = [23:27];     %-1500
shots{2} = [16:22];     %-1000
shots{3} = [7:15];      %-500
shots{4} = [1:6];       %0 from run 4
shots{5} = [37:42];     %0 from run 2
shots{6} = [1:5];       %+500
shots{7} = [7:14];      %+1000
shots{8} = [15:22];     %+1500
shots{9} = [23:30];     %+2000
shots{10} = [31:36];     %+2500
specres = cell(10,1);
titl = [-1500,-1000,-500,0,0,500,1000,1500,2000,2500];
runno = [4,4,4,4, 2,2,2,2,2,2];
totens = zeros(10,2);
totQ = zeros(10,1);
positi2 = GetFigureArray(1, 10,0.04,0.01, 'down');
positi3 = GetFigureArray(10, 1, 0.04, 0.01, 'across');
positi2 = positi2 + repmat([0.02 0.035 0 0]', 1, 10);
positi3 = positi3 + repmat([0.03 0.06 0 -0.04]', 1, 10);
encalib = makecalibrationmatrix('20121031');
[la ind8] = min(abs(encalib - 800));
[la ind6] = min(abs(encalib - 600));
[la ind4] = min(abs(encalib - 400));
[la ind10] = min(abs(encalib - 1000));
[la ind12] = min(abs(encalib - 1200));
[la ind15] = min(abs(encalib - 1500));
totespecs = zeros(10, 1086);
for s=1:length(shots)
    i = s+0;
    shotno = shots{i};
    temp = zeros(1024, length(shotno));
    especd = zeros(150*length(shotno), 1086);
    for k=1:length(shotno)
        namfig = ['20121108r' sprintf('%03i_s%03i',runno(i), shotno(k)) '.fig'];
        h0 = open(['Analysis/' namfig]);
        guidpntr = findobj(gca, 'Type', 'Line', 'Color', 'r');
        xdata = get(guidpntr, 'XData');
        templ = get(guidpntr, 'YData');
        temp(:,k) = templ./max(templ);
        close(h0);
        exitnam = [exitmodefol '20121108r' sprintf('%03i_s%03i', ...
            runno(i),shotno(k)) '_F20_ExitMode.tif'];
        exitmode = imread(exitnam);
        ampen = findcolumn(raws, ['20121108r' sprintf('%03i_s%03i', ...
            runno(i),shotno(k))], 22);
        totens(i,1) = totens(i,1) + sum(sum(exitmode))/ampen;
        totens(i,2) = totens(i,2) + (sum(sum(exitmode))/ampen)^2;
        especloc = imread([especfol '20121108r' sprintf('%03i_s%03i',...
            runno(i),shotno(k)) '_F20_Espec.tif']);%, 1280, 960);
        %especloc = RemoveHardHits(especloc);
        especd((k-1)*150+1:k*150,:) = especloc(275:424,:);
    end
    totens(i,1) = totens(i,1)/length(shotno);
    temp2 = totens(i,2)/length(shotno);
    totens(i,2) = sqrt(temp2 - totens(i,1)^2);
    specres{i} = temp';
    %spectrum compilation
    figure(103);
    imagesc(xaxis*1e9, 1:k, temp');
    hold on;
    plot([ee_left ee_left]*1e9, [0 10], '--k', 'LineWidth', 1.5);
    plot([ee_right ee_right]*1e9, [0 10], '--k', 'LineWidth', 1.5);
    hold off;
    set(gca, 'YTick', [], 'XLim', [500 900], 'XTick', [500:100:900], ...
        'XTickLabel', [500:100:900]);
    if (i~=10)
        set(gca, 'XTickLabel', []);
    end
    ylabel(num2str(titl(i)), 'FontSize', 16);
    ax1 = gca;
    hf2 = figure(19);
    ax2 = copyobj(ax1, hf2);
    set(ax2, 'Position', positi2(:,i));
    %espec compilation
    figure(103);
    imagesc(especd');
    set(gca,'XTick', [], 'YTick', [], 'CLim', [100 700], 'YDir', 'reverse', ...
        'YLim', [0 400]);
    if (s==1)
        set(gca, 'YTick', [ind15 ind12 ind10 ind8 ind6],...
            'YTickLabel', [1500 1200 1000 800 600], 'FontSize', 16);
        ylabel('Electron energy /MeV', 'FontSize', 16);
    end
    xlabel(num2str(titl(i)), 'FontSize', 16);
    hold on;
    plot([1 300*k], [(ind10) (ind10)], 'k', 'LineWidth', 1.);
    plot([1 300*k], [(ind8) (ind8)], 'k', 'LineWidth', 1.);
    plot([1 300*k], [(ind6) (ind6)], 'k', 'LineWidth', 1.);
    plot([1 300*k], [(ind12) (ind12)], 'k', 'LineWidth', 1.);
    plot([1 300*k], [(ind15) (ind15)], 'k', 'LineWidth', 1.);
    hold off;
    ax1 = gca;
    hf2 = figure(18);
    ax2 = copyobj(ax1, hf2);
    set(ax2, 'Position', positi3(:,s));
    set(18, 'colormap', iijcm);
    totespecs(i,:) = sum(especd);
    totespecs(i,:) = totespecs(i,:)./max(totespecs(i,:));
    totQ(i) = sum(sum(especd(especd>100)));
end

figure(18)
annotation('textbox', [0.4 0.035 0.2 0.03], 'String', 'Chirp = Dazzler setting /fs^2', ...
    'EdgeColor', 'none', 'FontSize', 16);

figure(19)
set(19, 'colormap', iijcm);
annotation('textbox', [0.45 0.015 0.15 0.03], 'String', 'Wavelength / nm', ...
    'EdgeColor', [1 1 1], 'FontSize', 20, 'EdgeColor', 'none');
annotation('textarrow',[0.0195 0.0195],[0.65 0.65],'string','Chirp = Dazzler setting /fs^2', ...
    'HeadStyle','none','LineStyle', 'none', 'TextRotation',90, 'FontSize', 20);
colormap(flipud(gray(256)))
setAllFonts(19,20)
%%
[la ind12] = min(abs(encalib - 1200));

figure(24)
%totespecs2 = RemoveHardHits(totespecs);
imagesc(1:1086, 1:10, totespecs);
hold on;
plot([(ind10) (ind10)], [-1 12], 'k', 'LineWidth', 2.);
plot([(ind8) (ind8)], [-1 12], 'k', 'LineWidth', 2.);
plot([(ind6) (ind6)], [-1 12], 'k', 'LineWidth', 2.);
plot([(ind12) (ind12)], [-1 18], 'k', 'LineWidth', 2.);
hold off;
set(gca, 'YTickLabel', titl, 'XTick', [ind12 ind10 ind8 ind6],...
    'XTickLabel', [1200 1000 600 600], 'XLim', [1 500]);
%%
figure(22)
% average the two zero ones
if (~isnan(totens(5,1)))
    totens(4,1) = 0.5*(totens(4,1)+totens(5,1));
    totens(4,2) = sqrt(totens(4,2)^2 + totens(5,2)^2);
    totens(5,:) = nan;
end
h1 = errorbar(titl, totens(:,1), totens(:,2), 'x', 'LineWidth', 2, 'MarkerSize', 15);
ylabel('Transmitted energy (a.u.)');
xlabel('Chirp = Dazzler setting /fs^2')
set(gca, 'XTick', [-1000:1000:2500]);
legend(h1, '2.0  \times 10^{18} cm ^{-3}', 'Location', 'North');
%reportfigure(22, 'halfpage', 'pdf', 'gascelldazzlerenergies');