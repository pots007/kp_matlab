load White_light_bentham

lambda = lambda*1e-9;
data = irradiance;
norm = max(data);
data = data./norm;

figure(1)
plot(lambda/1e-9, data*norm, '.')

h = 6.63e-34;
c = 2.998e8;
kb = 1.38e-23;

%%
amp_guess = max(data);
T_guess = 3000;

[estimates, model] = fitplanck(lambda, data, T_guess, amp_guess);
amp_fit = estimates(2);
T_fit = estimates(1);

fit = amp_fit ./lambda.^5 * 1 ./ ( exp( h*c ./lambda /kb/T_fit) - 1);
fit = fit./max(fit);

figure(1)
hold on
plot(lambda/1e-9, fit*norm, 'r')
hold off
title(strcat('T = ', num2str(T_fit, '%2.0f'), ' K'));
xlabel('wavelength /nm')
ylabel('Spectral Irradiance, /mW m^{-2} nm^{-1}');
