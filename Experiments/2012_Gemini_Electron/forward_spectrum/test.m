function varargout = test(varargin)
% TEST MATLAB code for test.fig
%      TEST, by itself, creates a new TEST or raises the existing
%      singleton*.

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @test_OpeningFcn, ...
    'gui_OutputFcn',  @test_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end


function test_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;

handles.current = pwd;
[nums texts raws] = xlsread('F:\Kristjan\Documents\Uni\Imperial\2012_OctNov_Gemini\20121121.xlsx');
clear nums;
clear texts;
handles.raws = raws;
% handles.shotampenergydata = [shots(:,1) shots(:,2) shots(:,3) shots(:,22)];
% handles.shotcompenergydata = [shots(:,1) shots(:,2) shots(:,3) shots(:,23)];
% handles.shotGeminiNumberdata = [shots(:,1) shots(:,2) shots(:,3) shots(:,5)];
% handles.shottargetdata = [raws(:,1) raws(:,2) raws(:,3) raws(:,8)];
% handles.shotcommentdata = [raws(:,1) raws(:,2) raws(:,3) raws(:,15)];
handles.shotfol = 'F:\Kristjan\Documents\Uni\Imperial\2012_OctNov_Gemini\Forward_spectrum\Server';
shotdates = findshotdates(handles.shotfol);
set(handles.listbox3, 'String', shotdates);
% Update handles structure
guidata(hObject, handles);
initial_dir = pwd;
load_listbox(initial_dir,handles);


function varargout = test_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;


function pushbutton1_Callback(hObject, eventdata, handles)
%Get file name from the listbox
get(handles.figure1,'SelectionType');
index_selected = get(handles.listbox1,'Value');
file_list = get(handles.listbox1,'String');
filename = file_list{index_selected};
writeshotdata(handles, filename);
%image = imread(filename);
plotthings(handles,filename);


function writeshotdata(handles, filename)
raws = handles.raws;
if (length(filename)>45)
    filename = filename(length(filename)-44:length(filename));
end
i = 1;
shotdate = str2double(filename(1:8));
shotrun = str2double(filename(10:12));
shotnumber = str2double(filename(15:17));
while (shotdate ~= raws{i,1})
    i = i+1;
end
while (shotrun ~= raws{i,2})
    i = i+1;
end
while (shotnumber ~= raws{i,3})
    i = i+1;
end
ampenergy = raws{i,22};
compenergy = raws{i, 23};
GemNum = raws{i,5};
set(handles.text2, 'String', ['AmpEn: ' num2str(ampenergy) ' J']);
set(handles.text3, 'String', ['CompEn: ' num2str(compenergy) ' J']);
set(handles.text4, 'String', ['Gemini shot #: ' num2str(GemNum)]);
shottarget = raws{i,8};
shotcomment = raws{i, 15};
set(handles.text5, 'String', shottarget);
set(handles.text6, 'String', shotcomment);
%Pressure finding.
k15 = strfind(shottarget, '15mm nozzle');
k10 = strfind(shottarget, '10mm nozzle');
cellflag = strfind(shottarget, 'Gas cell');
if (~isempty(k15))
    pres = raws{i,9};
    density = (pres * 4e16 + 3e16)*1e-18;
    set(handles.text10, 'String', [num2str(pres) 'bar, n_e = ' num2str(density) ' x10^18 cm^-3']);
elseif (~isempty(k10))
    pres = raws{i,9};
    density = (pres * 1.3e17 + 1.9e17)*1e-18;
    set(handles.text10, 'String', [num2str(pres) 'bar, n_e = ' num2str(density) ' x10^18 cm^-3']);
elseif(~isempty(cellflag))
    pres1 = raws{i,10};
    pres2 = raws{i, 12};
    set(handles.text10, 'String', ...
        [num2str(pres1) '/' num2str(pres2) ' mbar']);
else
    pres = raws{i,9};
    set(handles.text10, 'String', ['Gas pressure: ' num2str(pres) ' bar']);
end

%Avoid NaNs
if (isnan(shottarget))
    set(handles.text5, 'String', 'Target not in shotsheet');
end
if (isnan(shotcomment))
    set(handles.text6, 'String', 'Comments not in shotsheet');
end


function plotthings(handles,filename)
%Do the plotting
image = imread(filename);
if (get(handles.checkbox1, 'Value'))
    image = RemoveHardHits(image);
end
xaxis = Calibration('new');
xaxis = xaxis.*1e9;
axes(handles.axes2);
cla;
plot(xaxis, (sum(image)./max(sum(image))));
xlabel('Wavelength (nm)');
title('Full vertical binning');
axis tight;
axes(handles.axes3);
cla;
axes(handles.axes4);
cla;
try
    exitmodedir = 'F:\Kristjan\Documents\Uni\Imperial\2012_OctNov_Gemini\Exit_mode\Exit_mode_imaging\Server_data\';
    if (length(filename)>45)
        filename = filename(length(filename)-44:length(filename));
    end
    exitmodefile = [exitmodedir filename(1:22) 'ExitMode.tif'];
    exitmode = imread(exitmodefile);
    axes(handles.axes5);
    mesh = 1:1:1024;
    imaxis = mesh*1.274;
    cla;
    exitmode = imrotate(exitmode, 90);
    imagesc(imaxis, imaxis, exitmode);
catch err
end
axes(handles.axes1);
cla;
scale = 1:1:256;
yaxis = scale*26;
hImage = imagesc(xaxis, yaxis, image);
xlabel('Wavelength(nm)');
axis tight;
zoom yon;
zoom(2);
zoom off;
set(gca, 'ButtonDownFcn', {@testcallbackthing, image, handles});
set(hImage,'HitTest','Off');
makeunguided(handles.figure1, handles);
checkdone(handles);


function testcallbackthing(hObject,~,image, handles)
pos=get(hObject,'CurrentPoint');
X = pos(1,1);
Y = pos(1,2);
axes(handles.axes1);
hold on;
h = findobj(gca, 'Type', 'Line');
if (~isempty(h))
    delete(h)
end
xaxis = Calibration('new');
xaxis = xaxis.*1e9;
scale = 26*[1:1:256];
[~, Yloc] = min(abs(scale - Y));
[~, Xloc] = min(abs(xaxis - X));
%disp(['You clicked X:',num2str(X),', Y:',num2str(Y)]);
l = get(handles.edit1, 'String');
l = floor(str2num(l));
if (l==1)
    lineout = image(Yloc-floor(0.5*(l-1)):Yloc+floor(0.5*l),:);
else
    lineout = sum(image(Yloc-floor(0.5*(l-1)):Yloc+floor(0.5*l),:))/l;
end
plot(X,26*(Yloc-floor(0.5*(l-1))), ...
    '+','MarkerSize',40,'LineWidth', 1.6, 'Color', 'k');
plot(X,26*(Yloc+floor(0.5*l)), ...
    '+','MarkerSize',40,'LineWidth', 1.6, 'Color', 'k');
makeunguided(hObject, handles);
axes(handles.axes3);
plot(xaxis, lineout);
axis tight;
xlabel('Wavelength (nm)');
axes(handles.axes4);
imagesize = size(image);
plot(image(:,Xloc));
view(-90,90)
set(gca,'xdir','reverse')
set(gca, 'xLim', [0 imagesize(1)]);
ylabel('Counts');


function redrawlineout(hObject, handles)
axes(handles.axes1);
h = findobj(gca, 'Type', 'Line', 'Color', 'k');
if (isempty(h))
    return;
end
image = getimage(gca);
Ydat = get(h, 'YData');
Xdat = get(h, 'XData');
X = Xdat{1};
delete(h);
Y = abs(0.5*(Ydat{1}+Ydat{2}));
scale = 26*[1:1:256];
[~, Yloc] = min(abs(scale - Y));
l = get(handles.edit1, 'String');
l = floor(str2num(l));
if (l==1)
    lineout = image(Yloc-floor(0.5*(l-1)):Yloc+floor(0.5*l),:);
else
    lineout = sum(image(Yloc-floor(0.5*(l-1)):Yloc+floor(0.5*l),:))/l;
end
hold on;
plot(X,26*(Yloc-floor(0.5*(l-1))), ...
    '+','MarkerSize',40,'LineWidth', 1.6, 'Color', 'k');
plot(X,26*(Yloc+floor(0.5*l)), ...
    '+','MarkerSize',40,'LineWidth', 1.6, 'Color', 'k');
axes(handles.axes3);
xaxis = Calibration('new');
xaxis = xaxis.*1e9;
plot(xaxis, lineout);
axis tight;
xlabel('Wavelength (nm)');


function makeunguided(hObject, handles)
top = str2double(get(handles.edit2, 'String'));
bottom = str2double(get(handles.edit3, 'String'));
axes(handles.axes1);
h = findobj(gca, 'Type', 'Line', 'Color', 'w');
delete(h(:));
hold on;
plot(4e2:9e2, top, 'w', 'Linewidth', 2);
plot(4e2:9e2, bottom, 'w');
hold off;


function checkdone(handles)
shotname = handles.filenam(79:95);
existflag = dir(['Analysis\' shotname '.fig']);
if (isempty(existflag))
    set(handles.text14, 'BackGroundColor', 'r');
else
    set(handles.text14, 'BackGroundColor', 'g');
end


function FileMenu_Callback(hObject, eventdata, handles)


function OpenMenuItem_Callback(hObject, eventdata, handles)
[filename, pathname] = uigetfile('*.tif', 'Select a data file');
if isequal(filename,0)
    disp('User selected Cancel')
    return;
end
handles.filename = filename;
handles.path = pathname;
guidata(hObject, handles);
image = imread([pathname filename]);
plotthings(handles, image);


function PrintMenuItem_Callback(hObject, eventdata, handles)
printdlg(handles.figure1)


function CloseMenuItem_Callback(hObject, eventdata, handles)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
    ['Close ' get(handles.figure1,'Name') '...'],...
    'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end
cd (handles.current);
delete(handles.figure1)


function listbox1_Callback(hObject, eventdata, handles)
get(handles.figure1,'SelectionType');
% If double click
if strcmp(get(handles.figure1,'SelectionType'),'open')
    index_selected = get(handles.listbox1,'Value');
    file_list = get(handles.listbox1,'String');
    % Item selected in list box
    filename = file_list{index_selected};
    % If folder
    if  handles.is_dir(handles.sorted_index(index_selected))
        cd (filename)
        % Load list box with new folder.
        load_listbox(pwd,handles)
    else
        [path,name,ext] = fileparts(filename);
        switch ext
            case '.tif'
                %image = imread(filename);
                writeshotdata(handles, filename);
                plotthings(handles,filename);
        end
    end
end


function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function load_listbox(dir_path, handles)
cd (dir_path);
dir_struct = dir(dir_path);
[sorted_names,sorted_index] = sortrows({dir_struct.name}');
handles.file_names = sorted_names;
handles.is_dir = [dir_struct.isdir];
handles.sorted_index = sorted_index;
guidata(handles.figure1,handles);
set(handles.listbox1,'String',handles.file_names,'Value',1)
currentDirectory = pwd;
[upperPath, deepestFolder] = fileparts(currentDirectory);
set(handles.text1,'String',deepestFolder);


function listbox3_Callback(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
get(handles.figure1,'SelectionType');
% If double click
if strcmp(get(handles.figure1,'SelectionType'),'open')
    index = get(handles.listbox3,'Value');
    date_list = get(handles.listbox3,'String');
    handles.date = date_list{index};
    shotruns = findruns(handles.shotfol, date_list{index});
    guidata(hObject, handles);
    set(handles.listbox4, 'String', num2cell(shotruns));
end


function listbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function listbox4_Callback(hObject, eventdata, handles)
% hObject    handle to listbox4 (see GCBO)
get(handles.figure1,'SelectionType');
% If double click
if strcmp(get(handles.figure1,'SelectionType'),'open')
    index = get(handles.listbox4,'Value');
    run_list = get(handles.listbox4,'String');
    handles.run = run_list{index};
    %handles.run = sprintf('%0*d',3,run)
    shots = findshots(handles.shotfol, handles.date, run_list{index});
    guidata(hObject, handles);
    set(handles.listbox5, 'String', num2cell(shots));
end


function listbox4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox4 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function listbox5_Callback(hObject, eventdata, handles)
get(handles.figure1,'SelectionType');
% If double click
if strcmp(get(handles.figure1,'SelectionType'),'open')
    index = get(handles.listbox5,'Value');
    shot_list = get(handles.listbox5,'String');
    shot = shot_list{index};
    shot = str2double(shot);
    run = (handles.run);
    %sprintf('%0*d',3,run)
    num2str(run);
    run = str2double(run);
    filename = [handles.shotfol '\' handles.date 'r' ...
        sprintf('%0*d',3,run) '_s' sprintf('%0*d',3,shot) ...
        '_F20_ForwardSpectrometer.tif'];
    handles.filenam = filename;
    guidata(hObject, handles);
    writeshotdata(handles, filename);
    plotthings(handles,filename);
end


function listbox5_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton5_Callback(hObject, eventdata, handles)
axes(handles.axes3);
h = findobj(gca, 'Type', 'Line');
temp1 = get(h, 'xdata');
temp2 = get(h, 'ydata');
guided = [temp1*1e-9; temp2];
assignin('base', 'SpectrumData', guided');
assignin('base', 'guided', guided');
axes(handles.axes1);
h = findobj(gca, 'Type', 'Line', 'Color', 'w');
y1 = get(h(1), 'YData');
y2 = get(h(length(h)), 'YData');
scale = 26*[1:1:256];
[~, Y1] = min(abs(scale - y1));
[~, Y2] = min(abs(scale - y2));
if (Y1<Y2)
    temp = Y1;
    Y1 = Y2;
    Y2 = temp;
end
image = getimage(gca);
h = findobj(gca, 'Type', 'Line', 'Color', 'k');
if (~isempty(h))
    y3 = get(h(1), 'YData');
    y4 = get(h(2), 'YData');

    [~, Yloc1] = min(abs(scale - y1));
    [~, Yloc2] = min(abs(scale - y2));
    
    [~, ybot] = min(abs(scale - y3));
    [~, ytop] = min(abs(scale - y4));


    all = [Y1 Y2 ytop ybot];
    if(min(all)==ytop && max(all)==Y1 && ybot<Y2)
        disp('1');
        temp2 = sum(image(Y2:Y1,:))./abs(Y2-Y1);
    end
    if(min(all)==ytop && max(all)==Y1 && ybot>=Y2)
        disp('2');
        temp2 = sum(image(ybot+1:Y1,:))./abs(Y1-ybot-1);
    end
    if(min(all)==Y2 && max(all)==Y1)
        disp('3');
        if (~(abs(ytop-Y2-1)==0))
            temp3 = sum(image(Y2:ytop-1,:))./abs(ytop-Y2-1);
            %disp('la');
        else
            temp3 = zeros(1,length(temp1));
        end
        if (~(abs(Y1-ybot-1)==0))
            temp2 = sum(image(ybot+1:Y1,:))./abs(Y1-ybot-1);
            %disp('lala')
        else
            temp2 = zeros(1, length(temp1));
        end
        temp2 = temp2 + temp3;
    end
    if(min(all)==Y2 && max(all)==ybot && ytop<Y1)
        disp('4');
        temp2 = sum(image(Y2:ytop-1,:))/abs(ytop-Y2-1);
    end
    if(min(all)==Y2 && max(all)==ybot && ytop>=Y1)
        disp('5');
        temp2 = sum(image(Y2:Y1,:))./abs(Y1-Y2);
    end
end
%if (Yloc2>Yloc1)
    %temp2 = sum(image(Yloc1:Yloc2,:))./(abs(Yloc2-Yloc1));
%else
temp2 = sum(image(Y2:Y1,:))./(abs(Y2-Y1));
%end
axes(handles.axes2);
h = findobj(gca, 'Type', 'Line');
temp1 = get(h, 'xdata');
unguided = [temp1*1e-9; temp2];
assignin('base', 'unguided', unguided');
plottitle = [handles.filenam(83:99) ' pres: ' get(handles.text10, 'String')];
assignin('base', 'specshotname', plottitle);
evalin('base', 'PlotGuided');
set(handles.text14, 'BackGroundColor', 'g');


function pushbutton6_Callback(hObject, eventdata, handles)
export_fig(handles.figure1, ...
    ['Screenshots\' handles.filenam(length(handles.filenam)-44:length(handles.filenam)-3) 'png']);


function edit1_Callback(hObject, eventdata, handles)
redrawlineout(hObject, handles);


function edit1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton7_Callback(hObject, eventdata, handles)
l0 = get(handles.edit1, 'String');
l = (str2num(l0));
l = l+1;
set(handles.edit1, 'String', num2str(l));
redrawlineout(hObject, handles);


function pushbutton8_Callback(hObject, eventdata, handles)
l0 = get(handles.edit1, 'String');
l = (str2num(l0));
if (l>1)
    l = l-1;
end
set(handles.edit1, 'String', num2str(l));
redrawlineout(hObject, handles);


function edit2_Callback(hObject, eventdata, handles)
makeunguided(hObject, handles);


function edit2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton9_Callback(hObject, eventdata, handles)
num = str2double(get(handles.edit2, 'String'));
num = num+26;
set(handles.edit2, 'String', num2str(num));
makeunguided(hObject, handles);


function pushbutton10_Callback(hObject, eventdata, handles)
num = str2double(get(handles.edit2, 'String'));
if (num<26)
    return;
end
num = num-26;
set(handles.edit2, 'String', num2str(num));
makeunguided(hObject, handles);


function edit3_Callback(hObject, eventdata, handles)
makeunguided(hObject, handles);


function edit3_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pushbutton11_Callback(hObject, eventdata, handles)
num = str2double(get(handles.edit3, 'String'));
num = num+26;
set(handles.edit3, 'String', num2str(num));
makeunguided(hObject, handles);


function pushbutton12_Callback(hObject, eventdata, handles)
num = str2double(get(handles.edit3, 'String'));
if (num<26)
    return;
end
num = num-26;
set(handles.edit3, 'String', num2str(num));
makeunguided(hObject, handles);


% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
filename = handles.filenam;
writeshotdata(handles, filename);
plotthings(handles,filename);
