[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\20121121.xlsx');

%continue;
%at 55 bar
nx = 7;
ny = 7;
positi = GetFigureArray(nx,ny,0.05,0.03, 'down');
shots = 2:38;
hf2 = figure(9);
set(9, 'WindowStyle', 'docked');
set(9,'PaperOrientation','landscape');
clf;
%continue
prev = 0;
prex = 0;
olddaz = 275;
tit = false;
for i=1:length(shots)
    try
        h0 = open(['Analysis/20121105r002_s' sprintf('%03i',shots(i)) '.fig']);
    catch err
        continue;
    end
    ax1 = gca;
    titl = get(get(ax1, 'Title'), 'String');
    if (i==1)
        dazzler = titl(29:31);
    else 
        olddaz = dazzler;
        dazzler = titl(29:31);
    end
    title(titl(9:31));xlabel([]); ylabel([]);
    hf2 = figure(9);
    ax2 = copyobj(ax1, hf2);
    if (olddaz == dazzler)
        prev = prev + 1;
        tit = false;
    else
        prev = prex*ny + 1;
        prex = prex+1;
        tit = true;
    end

    set(ax2, 'Position', positi(:,prev));
    if (~tit)
         set(ax2, 'XTickLabel', [], 'YTickLabel', []);
         set(get(ax2,'Title'),'String',[])
         %title([]);
     else
         set(ax2, 'YTickLabel', []);
     end
     close(h0)
end
export_fig('Analysis/20121105r002_presscan1.pdf', '-transparent', '-nocrop', hf2)

%close(hf2);
