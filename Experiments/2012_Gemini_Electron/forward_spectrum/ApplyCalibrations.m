%Run on 30/05/2013 @15:40-15:55. Hence, everything after this will be good.

filen = dir('Analysis/Uncalibrated/*.fig');
load ShamrockResponse.mat;
return; %to avoid accidents :p
for i=1:length(filen)
    hin = open(['Analysis/Uncalibrated/' filen(i).name]);
    specshotname = get(get(gca, 'Title'), 'String');
    unguidpntr = findobj(gca, 'Type', 'Line', 'Color', 'b');
    xdata = get(unguidpntr, 'XData');
    unguiddata = get(unguidpntr, 'YData');
    unguided = [xdata', unguiddata'./shamresp(:,2)];
    guidpntr = findobj(gca, 'Type', 'Line', 'Color', 'r');
    if (isempty(guidpntr))
        guided = [];
    else
        guiddata = get(guidpntr, 'YData');
        guided = [xdata', guiddata'./shamresp(:,2)];
    end
    close(hin);
    PlotGuided
end