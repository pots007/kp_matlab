fold = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Forward_spectrum\Server\';
%exitmodefol = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Exit_mode\Exit_mode_imaging\Server_data\';
%especfol = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Data_days\20121029\especcrops\';
xaxis = Calibration('new')*1e9;
hvac = open(['Analysis/20121023r001_s002.fig']);
hunguid = findobj(gca, 'Type', 'Line', 'Color', 'b');
vacshot = get(hunguid, 'YData');
close(hvac);
vacshot = vacshot./max(vacshot);
maxv = exp(-2*sqrt(1))*max(vacshot);
i = 1;
while (vacshot(i) < maxv)
    i = i+1;
end
ee_left = xaxis(i);
ee_left_val = vacshot(i);
i = length(vacshot);
while (vacshot(i) < maxv)
    i = i-1;
end
ee_right = xaxis(i);
ee_right_val = vacshot(i);
%%
chirps = -1000:500:500;
tFWHM = [50.8, 42.7, 43.6, 52.4];
pfit = fit(chirps', tFWHM', 'poly2');
%chirps2 = -2000:500:2000;
chirps2 = [-2000,-1000,-500,0,500,1000,2000];
tFWHM2 = pfit.p1.*chirps2.^2 + pfit.p2.*chirps2 + pfit.p3;
%zerodazz = -0.5*pfit.p2/pfit.p1;
zerodazz = -0.5*0.0054/1.06e-5;
tzerodazz = 1.06e-5.*zerodazz.^2 + 0.0054.*zerodazz + 41.12
figure(99);
plot(chirps2, tFWHM2);
lengths = tFWHM2;

%%
titl = [-2000,-1000,-500,0,500,1000,2000];
tauE00 = 1e-15*lengths(1,:)/sqrt(log(2)); %1/e^2 of the field
tauE0 = tauE00/sqrt(2);
tauI00 = tauE0;
tauI0 = tauI00/sqrt(2); % Just so it makes sense later on....
E = 14; %To account for a fraction of energy being in the spot
P_0 = 1/sqrt(pi)*E./tauI0;
ne = 3.03e18;
Pcrit = 17.4e9*ncrit(800)/ne;
Constants;
nc = ncrit(800);
omega0 = 2*pi*c/8e-7;
taup = 0.015.*ne./(c.*nc)*1e15;
t = -300:0.1:300;
pul = @(w) exp(-(t/w).^2);
pulses = zeros(length(tauE00), length(t));
for i=1:length(tauE00)
    pulses(i,:) = P_0(i).*pul(tauI0(i)*1e15);
end
figure(51);
imagesc(t,1:7, pulses)
xlabel('Delay /fs');
ylabel('Chirp /fs^{2}');
cb = colorbar;
ylabel(cb, 'Power /W');
set(gca, 'YTickLabel', titl)
fracs = zeros(2,7);
hold on;
for k=1:7
    temp = pulses(k,:);
    i = 1;
    while (temp(i) < Pcrit) %to account for energy outside the centroid
        i = i+1;
    end
    ee_left_t = t(i);
    ee_left_val = temp(i);
    i = length(temp);
    while (temp(i) < Pcrit)
        i = i-1;
    end
    ee_right_t = t(i);
    ee_right_val = temp(i);  
    plot(ee_right_t, k, '*k', 'MarkerSize', 15);
    plot(tauI00(k)*1e15, k, 'xr')
    plot(ee_left_t, k, '*k', 'MarkerSize', 15);
    plot(ee_right_t-taup, k, 'xk', 'MarkerSize', 15);
    fracs(2,k) = ee_right_t/(tauI00(k)*1e15);
    midlambda = 0.5*(ee_left+ee_right);
    omega0 = 2*pi*c/8e-7;
    if (sign(titl(k))>0)
        omega0 = midlambda + (ee_right - midlambda)*fracs(2,k);
        omega0 = 2*pi*c/(omega0*1e-9);
    end
    if (sign(titl(k))<0)
        omega0 = midlambda + (ee_left - midlambda)*fracs(2,k);
        omega0 = 2*pi*c/(omega0*1e-9);
    end
    taup1 = 0.015.*omegapcalc(ne)^2./(c.*omega0^2)*1e15
    
    fracs(1,k) = 1- (taup)/(ee_right_t - ee_left_t);
end
hold off;

remspec = zeros(7,1024);
for i=1:7
    edge = ee_left + (ee_right-ee_left)*fracs(1,i);
    temp = vacshot;
    temp(xaxis>edge) = 0;
    remspec(i,:) = temp;
end

figure(90);
imagesc(xaxis, 1:7, remspec);
xlim([750 900]);
hold on;
plot([ee_left ee_left], [0 8], '--k', 'LineWidth', 1.5);
plot([ee_right ee_right], [0 8], '--k', 'LineWidth', 1.5);
hold off;