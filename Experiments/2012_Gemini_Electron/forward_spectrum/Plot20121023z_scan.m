[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\20121121.xlsx');

nx = 6;
ny = 4;
positi = GetFigureArray(nx,ny,0.05,0.03, 'down');
%shots = 34:65;
hf2 = figure(9);
set(9, 'WindowStyle', 'docked');
set(9,'PaperOrientation','landscape');
clf;

focus = 13010:1000:18010;
shots = [18:1:21; 14:1:17; 9:1:12; 5:1:8; 1:1:4; 24:1:27];

for i=1:nx
    for j=1:ny
        try
            h0 = open(['Analysis/20121023r002_s' sprintf('%03i',shots(i,j)) '.fig']);
        catch err
            continue;
        end
        ax1 = gca;
        titl = get(get(ax1, 'Title'), 'String');
        foc = findzposition(raws, ['20121023r002_s' sprintf('%03i',shots(i,j))]);
        title([titl(9:31) ' ' num2str(foc) 'mm']);xlabel([]); ylabel([]);
        hf2 = figure(9);
        ax2 = copyobj(ax1, hf2);
        
        set(ax2, 'Position', positi(:,(4*(i-1)+j)));
        if (mod(j,4)~=0)
            set(ax2, 'XTickLabel', [], 'YTickLabel', []);
        else
            set(ax2, 'YTickLabel', []);
        end
        close(h0)
    end
end
export_fig('Analysis/20121023r002_zscan.pdf', '-transparent', '-nocrop', hf2)

%close(hf2);
