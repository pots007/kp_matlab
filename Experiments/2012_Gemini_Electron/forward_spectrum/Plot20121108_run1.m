[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\20121121.xlsx');
fold = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Forward_spectrum\Analysis\';
exitmodefol = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Exit_mode\Exit_mode_imaging\Server_data\';
especfol = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Data_days\20121029\especcrops\';
xaxis = Calibration('new');

%% p2=200, 10mm
shots = cell(17,1);
shots{2} = [7:12];      %p1 = 20
shots{3} = [13:16];        %30
shots{4} = [17:21];        %40
shots{5} = [22,23,25];      %50
shots{6} = [26:28]; %60
shots{9} = [29:32];         %120
shots{11} = [33:35]; %200
shots{13} = [36:39]; %400
shots{16} = [40:44]; %800
shots{17} = [45:46]; %1200
titl = [0,20,30,40,50,60,75,100,120,150,200,300,400,500,600,800,1200];
totens10_200 = zeros(17,1);
spec10_200 = zeros(17, 1024);
positi2 = GetFigureArray(1, 17,0.04,0.01, 'down');
% encalib = makecalibrationmatrix;
% [la ind8] = min(abs(encalib - 800));
% [la ind6] = min(abs(encalib - 600));
% [la ind4] = min(abs(encalib - 400));
for i=1:length(shots)
    shotno = shots{i};
    tempspec = zeros(length(shotno),1024);
    %    focespec = zeros(200*length(shotno), 1093);
    for k=1:length(shotno)
        specnam = [fold '20121108r001_' sprintf('s%03i', ...
            shotno(k)) '.fig'];
        h0 = open(specnam);
        guidpntr = findobj(gca, 'Type', 'Line', 'Color', 'r');
        %xdata = get(guidpntr, 'XData');
        temp = get(guidpntr, 'YData');
        tempspec(k,:) = temp;
        close(h0);
        
        exitnam = [exitmodefol '20121108r001_' sprintf('s%03i', ...
            shotno(k)) '_F20_ExitMode.tif'];
        exitmode = imread(exitnam);
        ampen = findcolumn(raws, ['20121108r001_' sprintf('s%03i', ...
            shotno(k))], 22);
        %         totens55(i) = totens55(i) + sum(sum(exitmode))/ampen;
        %         espec1 = imread([especfol '20121108r001_' sprintf('s%03i',...
        %             shotno(k)) '_F20_Espec.tif']);%, 1280, 960);
        %         focespec((k-1)*200+1:k*200,:) = espec1(150:349,:);
    end
    totens10_200(i) = totens10_200(i)/length(shotno);
    spec10_200(i,:) = sum(tempspec);
%     figure(103);
%     imagesc(focespec);
%     set(gca, 'XTick', [], 'YTick', [], 'CLim', [0 600]);
%     ylabel(num2str(titl(i)));
%     hold on;
%     plot([(ind8) (ind8)], [1 200*k], 'w', 'LineWidth', 1.5);
%     plot([(ind6) (ind6)], [1 200*k], 'w', 'LineWidth', 1.5);
%     plot([(ind4) (ind4)], [1 200*k], 'w', 'LineWidth', 1.5);
%     hold off;
%     ax1 = gca;
%     hf2 = figure(21);
%     ax2 = copyobj(ax1, hf2);
%     set(ax2, 'Position', positi2(:,i));
%     if (i==7)
%         %set(get(ax2, 'Title'), 'String', num2str(focus(i)));
%         %else
%         set(ax2, 'XTick', [ind8 ind6 ind4], 'XTickLabel', [800 600 400]);
%     end
end

figure(31)
imagesc(xaxis, 1:17, spec10_200);
set(gca, 'YTickLabel', titl, 'XLim', [6e-7 9e-7])
ylabel('Dazzler setting (fs^2)');
xlabel('Wavelength (m)');
cb = colorbar;
ylabel(cb, 'Spectral intensity (a.u.)');
% hold on;
% plot([ee_left ee_left], [0 8], '--k', 'LineWidth', 1.5);
% plot([ee_right ee_right], [0 8], '--k', 'LineWidth', 1.5);
% hold off;
