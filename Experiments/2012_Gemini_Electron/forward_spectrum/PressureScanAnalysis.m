filen = dir('Server/20121023r003*.tif');
xaxis = Calibration('new');
spectra = zeros(length(filen), 1024);
npnts = 2^13;
pulselengths = zeros(length(filen), npnts);
for i=1:length(filen)
   data = imread(['Server/' filen(i).name]);
   temp = sum(data(75:105, :));
   tempmax = max(temp);
   temp = temp./tempmax;
   spectra(i,:) = temp;
   omega = 2*pi*3e8./(xaxis);
   counts = temp.*xaxis.^2/(2*pi*3e8);
   temp2 = fftX(omega, counts, npnts);
   tempmax = max(temp2(:,2));
   temp2(:,2) = temp2(:,2)./tempmax;
   pulselengths(i,:) = abs(temp2(:,2));
end
figure(1)
imagesc(xaxis, 1:79, spectra)
figure(2)
imagesc(temp2(:,1),1:79, pulselengths);
set(gca, 'XLim', [-1e-13 1e-13]);