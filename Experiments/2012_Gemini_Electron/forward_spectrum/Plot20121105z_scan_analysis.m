%% Get Data
[nums texts raws] = xlsread('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\20121121.xlsx');
especdata = xlsread('D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Jons_especs.xlsx');
exitmodefol = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Exit_mode\Exit_mode_imaging\Server_data\';
%especfol = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Data_days\20121105\20121105r001\';
especfol = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Data_days\20121105\especcrops\';
nx = 7;
ny = 4;
positi = GetFigureArray(nx,ny,0.05,0.03, 'down');
%shots = 34:65;
%%

focus = 17710:500:20710;
shots = [58:1:61; 34:1:37; 38:1:41; 42:1:45; 46:1:49; 50:1:53; 54:1:57];
guided = zeros(1024, nx*ny);
unguided = guided;
Especs = zeros(2,nx*ny);
energies = zeros(1, nx*ny);
ampens = zeros(1, nx*ny);
for i=1:nx
    for j=1:ny
        try
            h0 = open(['Analysis/20121105r001_s' sprintf('%03i',shots(i,j)) '.fig']);
        catch err
            continue;
        end
        %ax1 = gca;
        %titl = get(get(ax1, 'Title'), 'String');
        foc = findzposition(raws, ['20121105r001_s' sprintf('%03i',shots(i,j))]);
        unguidpntr = findobj(gca, 'Type', 'Line', 'Color', 'b');
        xdata = get(unguidpntr, 'XData');
        uguided = get(unguidpntr, 'YData');
        unguided(:,ny*(i-1)+j) = uguided./max(uguided);
        %unguided = [xdata', unguiddata'./shamresp(:,2)];
        guidpntr = findobj(gca, 'Type', 'Line', 'Color', 'r');
        if (~isempty(guidpntr))
            guid = get(guidpntr, 'YData');
            guided(:,ny*(i-1)+j) = guid./max(guid);
        end
        cd ('..');
        espectemp = findEspecs(['20121105r001_s' sprintf('%03i',...
            shots(i,j))], especdata);
        if (~isempty(espectemp))
            Especs(:,ny*(i-1)+j) = espectemp;
        end
        cd('Forward_spectrum');
        exitm = imread([exitmodefol '20121105r001_s' sprintf('%03i',...
            shots(i,j)) '_F20_ExitMode.tif']);
        energies(:,ny*(i-1)+j) = sum(sum(exitm));
        ampens(ny*(i-1)+j) = findcolumn(raws, ['20121105r001_s' sprintf('%03i',...
            shots(i,j))], 22);
        close(h0)
    end
end

%% Plot all corresponding exit modes

for i=1:nx
    for j=1:ny
        exitm = imread([exitmodefol '20121105r001_s' sprintf('%03i',...
            shots(i,j)) '_F20_ExitMode.tif']);
        h0 = figure(17);
        imagesc(exitm');
        ax1 = gca;
        hf2 = figure(10);
        ax2 = copyobj(ax1, hf2);
        
        set(ax2, 'Position', positi(:,(4*(i-1)+j)));
        set(ax2, 'XTickLabel', [], 'YTickLabel', []);
        if (mod(j,4)==1)
            set(get(ax2, 'Title'), 'String', num2str(focus(i)));
        %else
            %set(ax2, 'YTickLabel', []);
        end
        close(h0)
    end
end
%export_fig('Analysis/20121105r001_zscan.pdf', '-transparent', '-nocrop', hf2)

% %% Plot all espec1s - those are the ones in Jon's sheet
% repmat(focus, 4,1)
% for i=1:nx
%     for j=1:ny
%         exitm = ReadRAW16bit([especfol '20121105r001_s' sprintf('%03i',...
%             shots(i,j)) '_F20_Espec1.raw'], 1280, 960);
%         h0 = figure(17);
%         imagesc(exitm');
%         ax1 = gca;
%         hf2 = figure(11);
%         ax2 = copyobj(ax1, hf2);
%         set(get(ax2, 'Title'), 'String', num2str(shots(i,j)));
%         set(ax2, 'Position', positi(:,(4*(i-1)+j)));
%         set(ax2, 'XTickLabel', [], 'YTickLabel', [], 'CLim', [0 1500]);
%         if (mod(j,4)==1)
%             %set(get(ax2, 'Title'), 'String', num2str(focus(i)));
%         %else
%             %set(ax2, 'YTickLabel', []);
%         end
%         close(h0)
%     end
% end
% 
%% Plot all Especs in one image
repmat(focus, 4,1)
positi2 = GetFigureArray(1, nx,0.04,0.01, 'down');
encalib = makecalibrationmatrix('stepbefore09');
[la ind8] = min(abs(encalib - 800));
[la ind6] = min(abs(encalib - 600));
[la ind4] = min(abs(encalib - 400));
for i=1:nx
    focespec = zeros(1083, 804);
    for j=1:ny
        exitm = imread([especfol '20121105r001_s' sprintf('%03i',...
            shots(i,j)) '_F20_Espec.tif']);%, 1280, 960);
        focespec(:,(j-1)*201+1:j*201) = exitm(150:350,:)';
        if (shots(i,j) ==37)
           la = exitm; 
        end
    end
    h0 = figure(17);
    imagesc(focespec');
    hold on;
    plot([(ind8) (ind8)], [1 804], 'w', 'LineWidth', 1.5);
    plot([(ind6) (ind6)], [1 804], 'w', 'LineWidth', 1.5);
    plot([(ind4) (ind4)], [1 804], 'w', 'LineWidth', 1.5);
    hold off;
    ax1 = gca;
    hf2 = figure(11);
    ax2 = copyobj(ax1, hf2);
    set(get(ax2, 'YLabel'), 'String', num2str(focus(i)));
    %set(ax2, 'Position', positi2(:,(4*(i-1)+j)));
    set(ax2, 'Position', positi2(:,i));
    set(ax2, 'XTickLabel', [], 'YTickLabel', [], 'CLim', [0 1500]);
    if (i==7)
        %set(get(ax2, 'Title'), 'String', num2str(focus(i)));
        %else
        set(ax2, 'XTick', [ind8 ind6 ind4], 'XTickLabel', [800 600 400]);
    end
    close(h0)
    %end
end

%% Plot the data
load divmap.mat
foc2 = reshape(repmat(focus, 4,1), 28,1);
hf2 = figure(9);
set(gcf, 'Colormap', divmap);
set(9, 'WindowStyle', 'docked');
set(9,'PaperOrientation','landscape');
clf;
subplot(2,2,1)
imagesc(xdata, 1:28, unguided');
set(gca, 'YTick', 2:4:26, 'YTickLabel', focus);
title ('Unguided spectrum')
subplot(2,2,2)
imagesc(xdata, 1:28, guided');
set(gca, 'YTick', 2:4:26, 'YTickLabel', focus);
title('Guided spectrum');
subplot(2,2,3);
plot(foc2, Especs(1,:), 'r*', foc2, Especs(2,:), '*k');
set(gca, 'XTick', focus);
title('Maximum electron energy');
subplot(2,2,4)
plot(foc2, energies./ampens, '*');
set(gca, 'XTick', focus);
title('Exiting energy');
%export_fig('Analysis/20121105r001_zscan.pdf', '-transparent', '-nocrop', hf2)

%close(hf2);
