load divmap.mat;
Constants;
den = 0.1:0.05:4;
omegap = omegapcalc(den*1e18);
lambdap = 2*pi*c./omegap;
omega0 = 2*pi*c./8e-7;
for i=1:length(den)
   resul(i) = nlwakene(2.65, 50e-15, den(i)*1e18);
end
%figure(1)
%plot(den, lambdap*1e6)
figure(2)
dns = zeros(length(resul), length(resul(1).xi));
%shortest length scale, interpolate all others to this for plotting
%purposes
xi = resul(end).xi;
for i=1:length(resul)
   dns(i,:) = interp1(resul(i).xi,resul(i).n, xi);
   dns(i,:) = dns(i,:)./(den(i)*1e18);
end
xi  = -xi;
imagesc(xi, den, dns)
xlabel('\xi (\mu m)');
ylabel('Plasma density (10^{18} cm^{-3})');
cb = colorbar;
ylabel(cb, 'n/n_0');
title('Plasma wave');
hold on;
%plot(-lambdap*1e6, den, 'Color', 'k', 'LineWidth', 1.5);
hold off;
%set(gcf, 'Colormap', divmap);
%reportfigure(2, 'halfpage', 'pdf', 'denscan_dn')

%plot the refractive index
figure(3);
refrin = zeros(length(resul), length(resul(1).xi));
for i=1:length(den)
    gamm = sqrt(1+0.5.*resul(i).a.^2);
    omegap2 = qe^2.*dns(i,:)*den(i)*1e24./(epsilon0*me); %ne in m-3 here
    refrin(i,:) = sqrt((1-omegap2./(gamm*omega0.^2)));
end

imagesc(xi, den, refrin-1)
xlabel('\xi (\mu m)');
ylabel('Plasma density (10^{18} cm^{-3})');
cb = colorbar;
ylabel(cb, '\eta-1');
title('Refractive index');
%set(gca, 'YDir', 'normal');
hold on;
%xi is from the last result, so get a0 from there as well
plot(xi, 4-resul(end).a./(max(resul(1).a)*0.33), 'Color', 'k', 'LineWidth', 2);
hold off;
set(gca, 'CLim', [-2.5e-3 0]);
%set(gcf, 'Colormap', divmap);
%%
figure(4)
%dndxi = -diff(refrin,1);
[GX GY] = gradient(refrin, xi*1e-6, den);
dndxi = GX;
%dndxi(dndxi<0) = nan;
imagesc(xi, den, dndxi);
colorbar
set(gcf, 'Colormap', divmap);
set(gca, 'CLim', [-5e1 5e1], 'XLim', [-50 20]);
hold on;
plot(xi, 4-resul(end).a./(max(resul(1).a)*0.33), 'Color', 'k', 'LineWidth', 2.5);
hold off;
xlabel('\xi (\mu m)');
ylabel('Plasma density (10^{18} cm^{-3})');
cb = colorbar;
ylabel(cb, 'd\eta/d\xi');
title('Refractive index gradient');

reportfigure(4, 'halfpage', 'pdf', 'denscan_deta')