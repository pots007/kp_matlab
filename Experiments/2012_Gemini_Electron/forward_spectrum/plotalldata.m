%Does the plotting and file opening of other data
function out = plotalldata(filename, xaxis)
exitdata = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Exit_mode\Exit_mode_imaging\Server_data\';
energydata = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Energy_spectrum\Server\';
spectrumdata = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Forward_spectrum\Server\';
saveloc = 'D:\Kristjan\Documents\�likool\Imperial\2012_OctNov_Gemini\Forward_spectrum\Analysis\';
saveloc = [saveloc filename(1:8) '\'];
exitfilename = [exitdata filename(1:22) 'ExitMode.tif'];
spectrumfilename = [spectrumdata filename];
spectrum = imread(spectrumfilename);
hf = figure('units','normalized','outerposition',[0 0 1 1]);
subplot(2,2,1)
try
exitmode = imread(exitfilename);
exitmode = imrotate(exitmode, 90);
imagesc(exitmode);
catch err
end
colorbar;
title('Exit mode');
subplot(2,2,2)
try
    energyfilename = [energydata filename(1:22) 'Energy_spectrum.txt'];
    data = importdata(energyfilename);
    plot(data(:,1), data(:,2));
catch err
    
end
xlabel('Wavelength (nm)');
ylabel('Counts');
title('Energy spectrum');
subplot(2,2,[3 4]);
yaxis = 1:1:256;
yaxis = yaxis * 26;
imagesc(xaxis*1e9, yaxis, spectrum)
hcol = colorbar;
xlabel('Wavelength (nm)');
ylabel('Vertical distance');
ylabel(hcol, 'Counts');
title(filename(1:17), 'Interpreter', 'none');
saveas(hf, [saveloc filename(1:17) '.png'], 'png');
close(hf);
out = 0;
end