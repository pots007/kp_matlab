% Get Dazzler setting from shotsheet

function Dazzler = findDazzler(raws, filename)
shotdate = str2double(filename(1:8));
shotrun = str2double(filename(10:12));
shotnumber = str2double(filename(15:17));
i=1;
while (shotdate ~= raws{i,1})
    i = i+1;
end
while (shotrun ~= raws{i,2})
    i = i+1;
end
while (shotnumber ~= raws{i,3})
    i = i+1;
end
Dazzler = raws{i,13};
end
