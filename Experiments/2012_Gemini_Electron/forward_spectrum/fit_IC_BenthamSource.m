dd = importdata(fullfile(getGDrive, 'Experiment', '2012Gemini', 'ForwardSpec',...
    'IC_BenthamSource', 'White_light_bentham.txt'));

lambda = dd(:,1)*1e-9;
data = dd(:,2);
norm = max(data);
%data = data./norm;

figure(1111)
plot(lambda*1e9, data, '.')

h = 6.63e-34;
c = 2.998e8;
kb = 1.38e-23;

%
amp_guess = max(data);
T_guess = 3000;
logm = lambda < 800e-9;

%[estimates, model] = fitplanck(lambda, data, T_guess, amp_guess);
fitfun = @(A,T,x) PlanckFunction(A,T,x);
fitopt = fitoptions('Method', 'NonlinearLeastSquares', 'MaxIter', 1000,...
    'algorithm', 'Levenberg-Marquardt', 'StartPoint', [1 3000], 'TolX', 1e-50,...
    'TolFun', 1e-50);
[planckfit1,~,~] = fit(lambda, data, fitfun, fitopt);
[planckfit2,~,~] = fit(lambda(logm), data(logm), fitfun, fitopt);
%

figure(1111)
hold on
hf1 = plot(lambda/1e-9, planckfit1(lambda), 'r');
hf2 = plot(lambda/1e-9, planckfit2(lambda), 'k');
hold off
%title(strcat('T = ', num2str(T_fit, '%2.0f'), ' K'));
title('Bentham lightsource calibration')
xlabel('Wavelength /nm')
ylabel('Spectral Irradiance, /mW m^{-2} nm^{-1}');
legend([hf1 hf2], {['Fit to all data, T = ' num2str(planckfit1.T, '%2.0f') ' K'],...
    ['Fit to measured data, T = ' num2str(planckfit2.T, '%2.0f') ' K']}, 'Location', 'best')