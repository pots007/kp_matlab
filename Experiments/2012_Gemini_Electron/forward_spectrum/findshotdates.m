function dates = findshotdates(foldername)
filenames = dir([foldername '\*.tif']);
dates = {'dummy'};
dates = [dates filenames(1).name(1:8)];
k = 2;
for i=2:length(filenames)
    temp = filenames(i).name(1:8);
    if (~strcmp(dates{k}, temp))
        dates = [dates temp];
        k = k + 1;
    end
end
dates = dates(2:length(dates));
end