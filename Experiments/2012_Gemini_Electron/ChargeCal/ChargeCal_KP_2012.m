% Do the charge calibrations for different days. Load all the IPs, then do
% fancy-pancy background fitting and find total charge in the beam. Then,
% so same for Lanex images for those shots and produce a number for
% calibration. Look over saturation issues for now; just give a very rough
% estimate as to how much of a lower estimate our number would be.

%% First, let's load the data
% Imageplates first
if ispc
    IP(1).IP_QE = ReadRAW16bit('E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\2015ChargeCalibration\20150828\Cali_1_1\Cali_1_1.img', 2750, 500)';
    IP(2).IP_QE = ReadRAW16bit('E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\2015ChargeCalibration\20150904_chargecal\20150904_cc\20150904_cc.img', 1250, 3250);
    IP(3).IP_QE = ReadRAW16bit('E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\2015ChargeCalibration\20150926_cc_1\20150926_cc_1.img', 2750, 1250)';
    lanexfol = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\Data';
elseif ismac
    %IP(1).IP_QE = ReadRAW16bit('/Users/kristjanpoder/Experiments/2015_Gemini/2015ChargeCalibration/26082015_Gemini/26082015_Gemini.img', 2125, 1375)';
    IP(1).IP_QE = ReadRAW16bit('/Volumes/Gemini2015/Data/ChargeCalibration/Cali_1_1/Cali_1_1.img',...
        2750, 500)';
    IP(2).IP_QE = ReadRAW16bit('/Volumes/Gemini2015/2015ChargeCalibration/20150904_chargecal/20150904_cc/20150904_cc.img',...
        1250, 3250);
    IP(3).IP_QE = ReadRAW16bit('/Volumes/Gemini2015/2015ChargeCalibration/20150926_cc_1/20150916_cc_1.img',...
        2750, 1250)';
    lanexfol = '/Volumes/Gemini2015/Data/';
end
lanex(1).scr(1).raw = double(imread(fullfile(lanexfol, '20150828',...
    '20150828r002', '20150828r002s001_Espec1.tif')));
lanex(1).scr(2).raw = ReadRAW16bit(fullfile(lanexfol, '20150828',...
    '20150828r002', '20150828r002s001_Espec2LE.raw'), 1280, 960)';
lanex(2).scr(1).raw = double(imread(fullfile(lanexfol, '20150904',...
    '20150904r001', '20150904r001s005_Espec1.tif')));
lanex(2).scr(2).raw = flipud(ReadRAW16bit(fullfile(lanexfol, '20150904',...
    '20150904r001', '20150904r001s005_Espec2LE.raw'), 1280, 960)');
lanex(3).scr(1).raw = double(imread(fullfile(lanexfol, '20150926',...
    '20150926r006', '20150926r006s006_Espec1.tif')));
lanex(3).scr(2).raw = double(imread(fullfile(lanexfol, '20150926',...
    '20150926r006', '20150926r006s006_Espec2LE.tif')))';

titls = {'20150828', '20150904', '20150926'};
savfol = fullfile(getDropboxPath, 'Writing', 'ChargeCal_KP_2015');
load('owncolourmaps');

PSL = @(im,R,S,G,L) (R/100)^2*(4000/S)*10.^(L*(im./G-0.5));
R = 100; S = 5000; G=2^16-1; L = 5;

for i=1:3
    IP(i).IPraw = PSL(IP(i).IP_QE, R, S, G, L);
end

saving = false;
%% Now, let's plot the RAW IP images if we want - in QEs
figure(201); clf;
set(201, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(3,1, [0.1 0.05 0.05 0.05], 0.05, 'across');
axs = zeros(1,3);
for i=1:3
    axs(i) = axes('Parent', 201, 'Position', locs(:,i));
    imagesc(IP(i).IP_QE);
    title(axs(i), titls{i});
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^16]);
setAllFonts(201,26); make_latex(201);
colormap(201, parula);
if saving; export_fig(fullfile(savfol, 'IP_rawsQL'), '-pdf', '-nocrop', 201); end


%% Now, let's plot the RAW IP images if we want - PSLs here
figure(201); clf;
set(201, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(3,1, [0.1 0.05 0.05 0.05], 0.05, 'across');
axs = zeros(1,3);
for i=1:3
    axs(i) = axes('Parent', 201, 'Position', locs(:,i));
    imagesc(IP(i).IPraw);
    title(axs(i), titls{i});
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^8]);
setAllFonts(201,20); make_latex(201);
colormap(201, parula);
if saving; export_fig(fullfile(savfol, 'IP_rawsPSL'), '-pdf', '-nocrop', 201); end

%% Let's get to business: crop out regions of IP in all images
% Define the regions
%IP(1).scr(1).roi = [390 25 960 1820];
%IP(1).scr(2).roi = [22 10 325 690];
IP(1).scr(1).roi = [25 38 320 2460];
IP(1).scr(2).roi = [25 38 320 2460];
IP(2).scr(1).roi = [605 77 480 2050];
IP(2).scr(2).roi = [10 1520 475 1000];
IP(3).scr(1).roi = [520 30 460 2330];
IP(3).scr(2).roi = [10 10 480 1615];
% And create subimages of these
for i=1:3
    for k=1:2
        rr = IP(i).scr(k).roi;
        IP(i).scr(k).dat = IP(i).IPraw(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3));
    end
end
% And try to plot as many as we have.
figure(201); clf;
set(201, 'Color', 'w', 'Position', [100 100 1000 700]);
colormap(201, parula);
locs = GetFigureArray(3,1, [0.1 0.05 0.05 0.05], 0.05, 'across');
axs = zeros(1,3);
for i=1:3
    axs(i) = axes('Parent', 201, 'Position', locs(:,i));
    imagesc(IP(i).IP_QE);
    title(axs(i), titls{i});
    rectangle('Position', IP(i).scr(1).roi, 'EdgeColor', 'm', 'LineWidth', 2);
    rectangle('Position', IP(i).scr(2).roi, 'EdgeColor', 'm', 'LineWidth', 2);
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^16]);
setAllFonts(201,20); make_latex(201);
if saving; export_fig(fullfile(savfol, 'IP_rois_QL'), '-pdf', '-nocrop', 201); end

%% Now define the signal and background regions on all IP images
% Define signals; assume everything else is background to fit surface over
%IP(1).scr(1).sigroi = [535 265 370 510];
%IP(1).scr(2).sigroi = [160 75 94 78];
IP(1).scr(1).sigroi = [80 85 135 1660];
IP(1).scr(2).sigroi = [80 85 135 1660];
IP(2).scr(1).sigroi = [255 60 85 1875];
IP(2).scr(2).sigroi = [190 91 200 910];
IP(3).scr(1).sigroi = [85 45 260 2240];
IP(3).scr(2).sigroi = [160 1 308 1575];
% And plot them again
figure(202); clf;
set(202, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.05 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 202, 'Position', locs(:,i));
    dat = IP(ii(i)).scr(kk(i)).dat; 
    dat(dat==PSL(2^16-1,R,S,G,L)) = nan; 
    imagesc(dat);
    title(axs(i), [titls{ii(i)} ' IP ' num2str(kk(i))]);
    rectangle('Position', IP(ii(i)).scr(kk(i)).sigroi, 'EdgeColor', 'k', 'LineWidth', 2);
end
colormap(colmap.jet_white);
%set(axs, 'CLim', [0 2^8]);
setAllFonts(202,20); make_latex(202);
if saving; export_fig(fullfile(savfol, 'IP_signalrois'), '-pdf', '-nocrop', 202); end

%% Now do the background subtraction: fit a surface to everything apart from signal

method = [2 2 2; 2 2 2];
for i=1:3
    for k=1:2
        fprintf('Fitting IP %i, screen %i\n', i, k);
        dat = IP(i).scr(k).dat;
        rr = IP(i).scr(k).sigroi;
%         dat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3)) = nan;
%         [Xq,Yq] = meshgrid(1:size(dat,2), 1:size(dat,1));
%         X = Xq(~isnan(dat)); Y = Yq(~isnan(dat));
%         dat1 = dat(~isnan(dat));
%         IP(i).scr(k).bg = griddata(X,Y,dat1,Xq,Yq, 'linear');
        IP(i).scr(k).bg = createBackground(dat, rr, [], [], method(k,i));
    end
end

%% And plot the backgrounds, so we can estimate how good they are
figure(203); clf;
set(203, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.05 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 203, 'Position', locs(:,i));
    imagesc(IP(ii(i)).scr(kk(i)).bg);
    title(axs(i), [titls{ii(i)} ' IP ' num2str(kk(i))]);
    %rectangle('Position', IP(ii(i)).scr(kk(i)).sigroi, 'EdgeColor', 'k', 'LineWidth', 2);
end
%set(axs, 'CLim', [0 2^16]);
setAllFonts(203,20); make_latex(203);
if saving; export_fig(fullfile(savfol, 'IP_signalBGs'), '-pdf', '-nocrop', 203); end


%% Plot the IPs with the background subtracted

figure(203); clf;
set(203, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.11 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 203, 'Position', locs(:,i));
    dat = IP(ii(i)).scr(kk(i)).dat-IP(ii(i)).scr(kk(i)).bg;
    rr = IP(ii(i)).scr(kk(i)).sigroi; 
    fprintf('IP %i, scr %i: total signal counts = %2.2e ; Total IP Q = %2.2f pC\n',...
        ii(i), kk(i), sum(dat(:)), sum(IP(ii(i)).scr(kk(i)).dat(:))*6900/20*qe*1e12);
    totQ = sum(dat(:))*6900/20*qe*1e12;
    IP(ii(i)).scr(kk(i)).Qtot = totQ;
    ttemp = dat;
    ttemp(IP(ii(i)).scr(kk(i)).dat==PSL(2^16-1,R,S,G,L)) = nan;
    IP(ii(i)).scr(kk(i)).Nsatpixels = sum(isnan(ttemp(:)));
    IP(ii(i)).scr(kk(i)).Qsatpixels = sum(sum(dat(isnan(ttemp))))*6900/20*qe*1e12;
    imagesc(dat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3)));
    title(axs(i), {[titls{ii(i)} ' IP ' num2str(kk(i))] sprintf('$Q_{tot}=%2.0f pC$', totQ)});
end
set(axs, 'XTick', [], 'YTick', [], 'CLim', [0 2^8]); 
colormap(colmap.jet_white);
cb = colorbar('Peer', axs(1), 'Location', 'North');
set(cb, 'Position', [0.05 0.08 0.9 0.02])
xlabel(cb, 'Charge / PSL')
setAllFonts(203,20); make_latex(203);
if saving; export_fig(fullfile(savfol, 'IP_signalrois'), '-pdf', '-nocrop', 203); end

%% Now choose signal regions for lanex screens
% Define the regions
lanex(1).scr(1).roi = [840 470 960 1515];
lanex(1).scr(2).roi = [270 915 360 190];
lanex(2).scr(1).roi = [985 360 620 1740];
lanex(2).scr(2).roi = [135 620 605 580];
lanex(3).scr(1).roi = [845 195 810 1560];
lanex(3).scr(2).roi = [320 705 1075 1280];
% And create subimages of these
for i=1:3
    for k=1:2
        rr = lanex(i).scr(k).roi;
        lanex(i).scr(k).dat = lanex(i).scr(k).raw(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3));
    end
end
% And again plot the lot
figure(204); clf;
set(204, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.05 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 204, 'Position', locs(:,i));
    imagesc(lanex(ii(i)).scr(kk(i)).raw)
    title(axs(i), {titls{ii(i)} [' Lanex ' num2str(kk(i))]});
    rectangle('Position', lanex(ii(i)).scr(kk(i)).roi, 'EdgeColor', 'k', 'LineWidth', 2);
end
set(axs, 'XTick', [], 'YTick', []);
colormap(parula);
setAllFonts(204,20); make_latex(204);
if saving; export_fig(fullfile(savfol, 'Lanex_rois'), '-pdf', '-nocrop', 204); end

%% Now define the lanex signal regions as well

lanex(1).scr(1).sigroi = [405 80 120 1040];
lanex(1).scr(2).sigroi = [10 10 20 20];
lanex(2).scr(1).sigroi = [265 15 85 1210];
lanex(2).scr(2).sigroi = [230 50 125 510];
lanex(3).scr(1).sigroi = [350 60 120 1330];
lanex(3).scr(2).sigroi = [445 45 235 1235];
figure(204); clf;
set(204, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.05 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 204, 'Position', locs(:,i));
    imagesc(lanex(ii(i)).scr(kk(i)).dat)
    title(axs(i), {titls{ii(i)} [' Lanex ' num2str(kk(i))]});
    rectangle('Position', lanex(ii(i)).scr(kk(i)).sigroi, 'EdgeColor', 'k', 'LineWidth', 2);
end
set(axs, 'XTick', [], 'YTick', []);
set(axs(5), 'CLim', [0 500]);
colormap(colmap.jet_white);
setAllFonts(204,20); make_latex(204);
if saving; export_fig(fullfile(savfol, 'Lanex_sigrois'), '-pdf', '-nocrop', 204); end

%% Now do the background subtraction: fit a surface to everything apart from signal

method = [2 2 2; 2 2 2];
for i=1:3
    for k=1:2
        fprintf('Fitting Lanex %i, screen %i\n', i, k);
        dat = lanex(i).scr(k).dat;
        rr = lanex(i).scr(k).sigroi;
%         The below is basically the method 2 used in createBackground.m
%         dat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3)) = nan;
%         [Xq,Yq] = meshgrid(1:size(dat,2), 1:size(dat,1));
%         X = Xq(~isnan(dat)); Y = Yq(~isnan(dat));
%         dat1 = dat(~isnan(dat));
%         IP(i).scr(k).bg = griddata(X,Y,dat1,Xq,Yq, 'linear');
        lanex(i).scr(k).bg = createBackground(dat, rr, [], [], method(k,i));
    end
end

%% And plot the backgrounds, so we can estimate how good they are
figure(205); clf;
set(205, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.05 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 205, 'Position', locs(:,i));
    imagesc(lanex(ii(i)).scr(kk(i)).bg);
    title(axs(i), {titls{ii(i)} [' Lanex ' num2str(kk(i))]});
end
set(axs, 'XTick', [], 'YTick', []);
setAllFonts(205,20); make_latex(205);
colormap(colmap.jet_white);
if saving; export_fig(fullfile(savfol, 'Lanex_signalBGs'), '-pdf', '-nocrop', 205); end

%% Plot the screens with the background subtracted

figure(205); clf;
set(205, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(6, 1, [0.1 0.05 0.1 0.05], 0.05, 'across');
ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
for i=1:6
    axs(i) = axes('Parent', 205, 'Position', locs(:,i));
    dat = lanex(ii(i)).scr(kk(i)).dat -lanex(ii(i)).scr(kk(i)).bg;
    rr = lanex(ii(i)).scr(kk(i)).sigroi; 
    ttemp = dat; ttemp(ttemp<0.05*max(ttemp(:))) = 0;
    Ctot = sum(ttemp(:));
    %Ctot = sum(dat(:));
    lanex(ii(i)).scr(kk(i)).Ctot = Ctot;
    sum(isnan(dat(:)));
    imagesc(dat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3)));
    title(axs(i), {[titls{ii(i)} ' Lnx ' num2str(kk(i))] sprintf('$C_{tot}=%2.1e$', Ctot)});
    cb = colorbar('Peer', axs(i), 'Location', 'North');
    set(cb, 'Position', [locs(1,i) 0.08 locs(3,i), 0.015],...
        'XTick', [0 max(dat(:))], 'XTickLabel', {'0' num2str(max(dat(:)), '%1.0f')}); 
end
set(axs, 'XTick', [], 'YTick', []);%, 'CLim', [0 2^8]);
colormap(colmap.jet_white);
setAllFonts(205,20); make_latex(205);
if saving; export_fig(fullfile(savfol, 'Lanex_signalrois'), '-pdf', '-nocrop', 205); end

%% Let's do it properly now, for images where we warp the screen back
%  I won't delete the code for earlier, but it doesn't serve much use, as
%  when using it with MagnetTracker, one needs to take into account that
%  the warping doesn't exactly conserve integral ( I think ). In any case,
%  spatial extent etc changes, so take this into account here.

% The following should be valid for 0902r004 up to 20150911. On the latter
% date the screen has moved, so should do a new warp.

% For CC for pre 0902r003, can use the same warp calibration. This will be
% done below.

warpdat = load('/Users/kristjanpoder/Experiments/2015_Gemini/EspecRefs/Screen1_20150902r002s030CalibData');

ss = warpdat.datt.warpdat.cropsize;
cropim = lanex(2).scr(1).raw(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3));
rotim = imrotate(cropim, warpdat.datt.warpdat.thet);
warpim = imwarp(rotim, warpdat.datt.warpdat.q.tform, 'cubic');
warpim = warpim./warpdat.datt.warpdat.q.J2;
logmx = warpdat.datt.warpdat.q.x>0 & warpdat.datt.warpdat.q.x<warpdat.datt.warpdat.q.screenx(3);
logmy = warpdat.datt.warpdat.q.y>0 & warpdat.datt.warpdat.q.y<warpdat.datt.warpdat.q.screeny(3);
lanexim = warpim(logmy,logmx);

IProi = [1 30 720 45]; lanexroi = [1 20 500 35];
IProi = [600 30 600 20]; lanexroi = [400 19 400 25];
%IProi = [1 1 1875 85]; lanexroi = [1 1 1235 65];

figure(206); clf(206);
set(206, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(1, 2, [0.05 0.15 0.05 0.05], 0.05, 'across');
ax1 = axes('Parent', 206, 'Position', locs(:,1));
IPdat = IP(2).scr(1).dat-IP(2).scr(1).bg;
rr = IP(2).scr(1).sigroi; 
IPdat = IPdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3))';
imagesc(IPdat, 'Parent', ax1);
rectangle('Parent', ax1, 'Position', IProi, 'EdgeColor', 'k', 'LineWidth', 2);
cb = colorbar('Peer', ax1, 'Location', 'West');
set(cb, 'Position', [0.86 locs(2,1) 0.02 locs(4,1)]);
ylabel(cb, 'Charge / PSL');
ylabel(ax1, 'Image plate');

ax2 = axes('Parent', 206, 'Position', locs(:,2));
rr = [430 205 1235 65]; 
lanexBG = createBackground(lanexim, rr, [], [], 2);
lanexdat = lanexim - lanexBG; 
lanexdat = lanexdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3));

imagesc(lanexdat, 'Parent', ax2);
rectangle('Parent', ax2, 'Position', lanexroi, 'EdgeColor', 'k', 'LineWidth', 2);
cb = colorbar('Peer', ax2, 'Location', 'West');
set(cb, 'Position', [0.86 locs(2,2) 0.02 locs(4,2)]);
ylabel(cb, 'Charge / Counts');
ylabel(ax2, 'Espec1 camera, warped');

colormap(206, colmap.jet_white);
setAllFonts(206,30); make_latex(206);

set([ax1 ax2], 'XTick', [], 'YTick', []);
set(ax2, 'CLim', [min(lanexdat(:)) 4000])

totIP_Q = sum(sum(IPdat(IProi(2):IProi(2)+IProi(4),...
    IProi(1):IProi(1)+IProi(3))))*6900/20*1e12*qe;
totlanex_C = sum(sum(lanexdat(lanexroi(2):lanexroi(2)+lanexroi(4),...
    lanexroi(1):lanexroi(1)+lanexroi(3))));
calfact = totIP_Q/totlanex_C;

%% After all this, let's revisit the 20150904 espec 1 image to use only
%  non-saturated parts of the images

IProi = [1 30 720 45]; lanexroi = [1 20 500 50];
IProi = [600 30 600 20]; lanexroi = [400 17 400 25];


figure(206); clf(206);
set(206, 'Color', 'w', 'Position', [100 100 1000 700]);
locs = GetFigureArray(1, 2, [0.05 0.15 0.05 0.05], 0.05, 'across');
ax1 = axes('Parent', 206, 'Position', locs(:,1));
IPdat = IP(2).scr(1).dat-IP(2).scr(1).bg;
rr = IP(2).scr(1).sigroi; 
IPdat = IPdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3))';
imagesc(IPdat, 'Parent', ax1);
rectangle('Parent', ax1, 'Position', IProi, 'EdgeColor', 'k', 'LineWidth', 2);
cb = colorbar('Peer', ax1, 'Location', 'West');
set(cb, 'Position', [0.86 locs(2,1) 0.02 locs(4,1)]);
ylabel(cb, 'Charge / PSL');
ylabel(ax1, 'Image plate');

ax2 = axes('Parent', 206, 'Position', locs(:,2));
rr = lanex(2).scr(1).sigroi; 
lanexdat = lanex(2).scr(1).dat-lanex(2).scr(1).bg; 
lanexdat = fliplr(lanexdat(rr(2):rr(2)+rr(4),rr(1):rr(1)+rr(3))');
imagesc(lanexdat, 'Parent', ax2);
rectangle('Parent', ax2, 'Position', lanexroi, 'EdgeColor', 'k', 'LineWidth', 2);
cb = colorbar('Peer', ax2, 'Location', 'West');
set(cb, 'Position', [0.86 locs(2,2) 0.02 locs(4,2)]);
ylabel(cb, 'Charge / Counts');
ylabel(ax2, 'Espec1 camera');

colormap(206, colmap.jet_white);
setAllFonts(206,30); make_latex(206);

set([ax1 ax2], 'XTick', [], 'YTick', []);
set(ax2, 'CLim', [min(lanexdat(:)) 4000])
export_fig(fullfile(savfol, '20150904_Espec1_partial'), '-nocrop', '-pdf', 206);

totIP_Q = sum(sum(IPdat(IProi(2):IProi(2)+IProi(4),...
    IProi(1):IProi(1)+IProi(3))))*6900/20*1e12*qe;
totlanex_C = sum(sum(lanexdat(lanexroi(2):lanexroi(2)+lanexroi(4),...
    lanexroi(1):lanexroi(1)+lanexroi(3))));
calfact = totIP_Q/totlanex_C;

%% And finally, pop out a table of the total charges and calibration numbers

% Estimate the error by saying at most, the saturated IP pixels could have
% held twice the PSL they scanned out as... very dodgy but will do it
% properly later on. In fact, this only really affects the 20150904 espec1
% calibration.
oversaturationfactor = 2;

ii = [1 1 2 2 3 3]; kk = [1 2 1 2 1 2];
fprintf('\n%10s\t%5s\t%5s\t%9s\t%9s\t%6s\n', 'Date', 'Scr', 'Q/pC', 'C_tot',...
    'Cal', 'Error');
for i=1:6
    Qtot = IP(ii(i)).scr(kk(i)).Qtot;
    Ctot = lanex(ii(i)).scr(kk(i)).Ctot;
    err_est = (oversaturationfactor-1)*IP(ii(i)).scr(kk(i)).Qsatpixels/Qtot*100;
    fprintf('%10s\t%5i\t%5.1f\t%9.2e\t%9.2e\t%6.0f\n', titls{ii(i)}, kk(i),...
        Qtot, Ctot, Qtot/Ctot, err_est); 
end
fprintf('%10s\t%5i\t%5.1f\t%9.2e\t%9.2e\t%6.0f\n', titls{2}, 1,...
        totIP_Q, totlanex_C, calfact, 0); 

% And a latex formatted one if we so please
if 0
    fprintf('\n%10s & %5s & %5s & %9s & %9s & %6s\n', 'Date', 'Scr',...
        'Q/pC', 'C_tot', 'Cal', 'Error');
    for i=1:6
        Qtot = IP(ii(i)).scr(kk(i)).Qtot;
        Ctot = lanex(ii(i)).scr(kk(i)).Ctot;
        fprintf('%10s & %5i & %5.1f & %9.2e & %9.2e & %6.0f\n', ...
            titls{ii(i)}, kk(i), Qtot, Ctot, Qtot/Ctot, 0.05);
    end
end
   