load('expfocus.mat');
exper = 'Gem2013';
ycommon = true;
expdays = fieldnames(focus.(exper));
nodays = length(expdays);
shotm = zeros(size(expdays));
for i=1:nodays
    shotm(i) = size(focus.(exper).(expdays{i}).data, 2);
end

%main plotting loop
noplots = 4; figure(44); clf;
set(44, 'PaperType', 'A4');
if ismac
    savp = '/Users/kristjanpoder/Experiments/';
elseif ispc
    savp = 'F:\Kristjan\Documents\Uni\Imperial\2014_ManglesGemini\';
end
locs = GetFigureArray(1, noplots, 0.08, 0.05, 'down');
syms = {'xr', 'dk', 'ob', 's'};
it = 1;
for i=1:ceil(nodays/noplots)
    figure(44); clf;
    axs = zeros(noplots,1);
    for k=1:noplots
        axs(k) = axes('Parent', 44, 'Position', locs(:,k));
        hs = zeros(3,1);
        hs(1) = plot(axs(k),1:shotm(it), focus.(exper).(expdays{it}).data(11,:), syms{1});
        hold on;
        hs(2) = plot(axs(k),1:shotm(it), focus.(exper).(expdays{it}).data(9,:), syms{2});
        hs(3) = plot(axs(k),1:shotm(it), focus.(exper).(expdays{it}).data(10,:), syms{3});
        hold off;
        title(expdays{it}(2:end));
        set(hs, 'MarkerSize', 7, 'LineWidth', 1.2);
        ylabel('Spot size / \mu m'); 
        leg = legend(hs, {'FWHM r', 'FWHM x', 'FWHM y'}, 'Location', 'EastOutside');   
        it = it+1;
        if it>nodays break; end;
    end
    set(axs(axs~=0), 'XLim', [1 max(shotm)]);
    if (ycommon) set(axs(axs~=0), 'YLim', [10 60]); end
    xlabel('Shot number');
    setAllFonts(44, 16);
    if i==1
        export_fig([savp exper '_spots'], '-transparent', '-pdf', '-nocrop', 44);
    else
        export_fig([savp exper '_spots'], '-transparent', '-pdf', '-nocrop', '-append', 44);
    end
end

    