% plotDESY2017_Pardis_tracking.m
%
% Plots the magnetic electron deflections.

ppath = 'E:\Kristjan\Work\DESY\DESYcloud\Experiments\2017_WakeProbing';
Tfile = load(fullfile(ppath, 'DESY_2017_Pardis_Tracking'));

hfig = figure(2017); clf(hfig); hfig.Position = [520 370 600 400];
ax1 = gca;
set(ax1, 'NextPlot', 'add');

ens = Tfile.tracking.energies;
scr1_low = Tfile.tracking.screen(1).alongscreen(:,4);
scr1_val = Tfile.tracking.screen(1).alongscreen(:,3);
scr1_high= Tfile.tracking.screen(1).alongscreen(:,2);

%plotyy(ens, scr1_val, ens, scr1_val-scr1_low);
hh = plot(ens, Tfile.tracking.screen(1).alongscreen(:,2:4), 'LineWidth', 1);

legend(hh, {'$\theta = -3$ mrad', 'Beam center', '$\theta = 3$ mrad'},...
    'Location', 'Southwest');
xlabel('Electron energy / MeV');
ylabel('Deflection from beam axis / mm');
set(ax1, 'Box', 'on', 'Layer', 'top', 'LineWidth', 1.5, 'XLim', [20 150]);
grid on; grid minor;
makeNiceGridHG2(gca);
% And now the inset
ax2 = axes('Parent', hfig, 'Units', 'Normalized', 'Position', [0.47 0.5 0.4 0.4]);
hh = plot(ax2, ens, Tfile.tracking.screen(1).alongscreen(:,2:4), 'LineWidth', 1);
set(ax2, 'XLim', [45 55], 'LineWidth', 0.75, 'YTick', 16:22);
xlabel('$\mathcal{E}$ / MeV'); ylabel('Deflection / mm');
grid on;

make_latex(hfig); setAllFonts(hfig, 15);
ax2.FontSize = 12;

export_fig(fullfile(ppath, 'DESY_2017_Pardis_Tracking'), '-pdf', '-nocrop', hfig);