% This will make a nice plot to send to Rob C regarding the radiation
% safety

hfig = figure(321); clf(hfig);
set(hfig, 'Color', 'w');
ax = gca;
set(ax, 'NextPlot', 'add');
% Highlight TCC location
hTCC = plot(ax, 0, 0, '*k', 'MarkerSize', 10, 'LineWidth', 2);
% Draw the breadboard
gray = 0.64*[1 1 1];
hBrd = rectangle('Position', [-100 -440 2598 103], 'LineWidth', 1.5, 'EdgeColor', 'none',...
    'FaceColor', gray);
text(0, -440, 'Breadboard: 103 mm Al', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'left')
% Plot the B-field
fset = load('Zepf2016_Settings.mat');
imagesc(fset.dat.B1.z, fset.dat.B1.y, -fset.dat.B1.B)
% Plot the magnet yoke
hYoke = rectangle('Position', [662 -148 328 43], 'LineWidth', 1.5, 'EdgeColor', 'none',...
    'FaceColor', 0.4*[1 1 1]);
colormap(jet_white(256));
text(650, -148, 'Yoke: 43 mm Fe', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'right')
% The intended lead shielding location
hPb1 = rectangle('Position', [1400 -337 150 425], 'EdgeColor', 'none', 'FaceColor', 'g');

% And plot the bunker outline
beige = [210 180 140]/255;
hBun1 = rectangle('Position', [-100 -1500 5800 200]);
hBun2 = rectangle('Position', [4700 -1300 1000 950]);
set([hBun1 hBun2], 'EdgeColor', 'none', 'FaceColor', beige, 'LineWidth', 1.5);
text(100, -1450, 'Bunker', 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'left')
% And the lead wall in there
hPb = rectangle('Position', [4900 -350 300 1000], 'FaceColor', 0.2*[1 1 1]);

% And finally plot some tracks
ftrack = load('Zepf2016_tracking_tracks.mat');
trr = ftrack.tracking.div(2).el;
enfact = 1e6*1.60217687e-19/299792458;
ids = [1 3 5 7 10 12 15 20 22]; ens = [];
cols = brewermap(9, 'Set1'); hh = []; titl = {};
for i=1:length(ids)
    ens(i) = trr(ids(i)).energy/enfact;
    titl{i} = sprintf('%2.0f MeV', ens(i));
    rr = trr(ids(i)).r;
    hh(i) = plot(ax, rr(:,3), rr(:,2), '-', 'Color', cols(i,:), 'LineWidth', 1);
end
legend(hh, titl);
axis equal, axis off
set(ax, 'XLim', [-100 5800], 'YLim', [-1500 200]);
drawnow;
export_fig(fullfile(getDropboxPath, 'MATLAB', 'experiments', '2016_Zepf', 'ICLRadSafety'), '-nocrop', '-pdf', hfig);