% Make a calibration file for server for Zepf2016;

fset = load('161116_lanex_final_CalibData');

fcalib.tracking = fset.datt.tracking;
fcalib.pixels = fset.datt.pixels;
fcalib.lengths = fset.datt.lengths;
fcalib.pfit = fset.datt.efit;
fcalib.efit = fset.datt.pfit;
save('161116_lanex_final_calib', 'fcalib');