% Check for correlations between main F40 focus diagnostic and the leakage
% behind the burney mirror.

dataf = 'E:\Kristjan\Documents\Uni\Imperial\2016[10]_Zepf\Focus\CorrelationTest\20161121';
files_f = dir(fullfile(dataf, 'Focuscam', '*.tiff'));
files_l = dir(fullfile(dataf, 'Leakage', '*.raw'));
% Calibrations: 1 um/pix for spot cam, 1.35 um/pix for inputFF
CC = [1 1.35];

% Figure out synchronisation later - we need to get the original
% timestamps, not the ones on the current machine!

% Grab the initial timestamps and then find an array of differences
times_f = cell2mat({files_f.datenum}); 
times_f = times_f - times_f(1); times_f = round(times_f*24*3600);
times_l = cell2mat({files_l.datenum}); 
times_l = times_l - times_l(1); times_l = round(times_l*24*3600);

nshots = min([length(files_f) length(files_l)]);
spotdata = zeros(nshots, 12);

hfig = figure(800); clf(hfig);
set(hfig, 'Position', [100 100 800 300], 'Color', 'w');
locs = GetFigureArray(2, 1, [0.1 0.05 0.05 0.05], 0.05, 'across');
for k=1:nshots
    fprintf('Working on shot %i of %i\n', k, nshots);
    clf(hfig);
    % Load images
    im_f = double(imread(fullfile(dataf, 'Focuscam', files_f(k).name)));
    % This was the slow one, so we now find the time offset from this image
    % and load the corresponding leakage image
    ind = times_l==times_f(k);
    if sum(ind)==0; continue; end;
	% Open the file
    im_l = ReadRAW16bit(fullfile(dataf, 'Leakage', files_l(ind).name), 640, 480);
    % Subtract BG
    im_f = im_f - mean(mean(im_f(:, 1:100)));
    im_l = im_l - mean(mean(im_l(:, 540:end)));
    im_f = medfilt2(im_f);
    im_l = medfilt2(im_l);
    ax1 = axes('Parent', hfig, 'Position', locs(:,1), 'NextPlot', 'add', 'Box', 'on', 'Layer', 'top');
    imagesc(im_f, 'Parent', ax1);
    ax2 = axes('Parent', hfig, 'Position', locs(:,2), 'NextPlot', 'add', 'Box', 'on', 'Layer', 'top');
    imagesc(im_l, 'Parent', ax2);
    % Get centroid location - intensity weighted centroid, threshold of 0.5
    im_t = im_f; im_t(im_t<0.5*max(im_f(:)))=0;
    locs_f = centroidlocationsmatrix(im_t);
    im_t = im_l; im_t(im_t<0.5*max(im_f(:)))=0;
    locs_l = centroidlocationsmatrix(im_t);
    % Also fit an ellipse and use the centre of that as the offset
    % Binarise image, and find the FWHM contour
    bw_f = im_f; bw_f(bw_f<0.5*max(bw_f(:)))=0; bw_f(bw_f~=0)=1;
    cc = bwboundaries(bw_f); cc = cc{1};
    ell_f = fit_ellipse(cc(:,2), cc(:,1), ax1);
    bw_l = im_l; bw_l(bw_l<0.5*max(bw_l(:)))=0; bw_l(bw_l~=0)=1;
    cc = bwboundaries(bw_l); cc = cc{1};
    ell_l = fit_ellipse(cc(:,2), cc(:,1), ax2);
    set([findall(ax1, 'Type', 'line') findall(ax2, 'Type', 'line')], 'Color', 'k');
    set(ax1, 'CLim', [0 2^12], 'XLim', locs_f(1)+[-150 150], 'YLim', locs_f(2)+[-150 150], 'XTick', [], 'YTick', []);
    set(ax2, 'CLim', [0 2^16], 'XLim', locs_l(1)+[-150 150], 'YLim', locs_l(2)+[-150 150], 'XTick', [], 'YTick', []);
    title(ax1, 'Focal spot camera');
    title(ax2, 'Leakage beam diagnostic');
    colormap(jet_white(256));
    drawnow;
    if isempty(ell_f) || isempty(ell_l); continue; end;
    export_fig(fullfile(dataf, 'shots', sprintf('shot%03i', k)), '-png', '-nocrop', hfig);
    % And collate the data
    % Simple centroid locations
    spotdata(k,1) = locs_f(1)*CC(1);
    spotdata(k,2) = locs_f(2)*CC(1);
    spotdata(k,3) = locs_l(1)*CC(2);
    spotdata(k,4) = locs_l(2)*CC(2);
    % Ellipse centroids    
    spotdata(k,5) = ell_f.X0_in*CC(1);
    spotdata(k,6) = ell_f.Y0_in*CC(1);
    spotdata(k,7) = ell_l.X0_in*CC(2);
    spotdata(k,8) = ell_l.Y0_in*CC(2);
    % Ellipse angles!
    spotdata(k,9) = ell_f.phi;
    spotdata(k,10) = ell_l.phi;
end

%% And plot something
% The correlations between spot movement
hfig = figure(87); clf(hfig); set(hfig, 'Color', 'w');
logmx = spotdata(:,5)>100 & spotdata(:,7)>100;
plot(spotdata(logmx,5), spotdata(logmx,7), 'ok');
set(gca, 'NExtPlot', 'add', 'Box', 'on', 'LineWidth', 2);
corrc = corrcoef(spotdata(logmx,5), spotdata(logmx,7));
[fit_x, err_x] = fit(spotdata(logmx,5), spotdata(logmx,7), 'poly1');
xlabel('Focal spot camera x position / micron');
ylabel('Leakage spot camera x position / micron');
xx_ = get(gca, 'XLim');
plot(xx_, fit_x(xx_), '-r');
text(425, 200, sprintf('$$slope = %2.2f; R^2 = %2.3f$$', fit_x.p1, err_x.rsquare), 'Color', 'r');
text(425, 190, sprintf('$$p= %2.2f$$', corrc(1,2)));
make_latex(hfig);
setAllFonts(hfig, 14);
export_fig(fullfile(getDropboxPath, 'MATLAB', 'experiments', '2016_Zepf', 'PointingCorr_x'), '-png', '-nocrop', hfig);
%%
hfig = figure(88); clf(hfig); set(hfig, 'Color', 'w');
logmy = spotdata(:,6)>100 & spotdata(:,8)>100;
plot(spotdata(logmy,6), spotdata(logmy,8), 'x');
set(gca, 'NExtPlot', 'add', 'Box', 'on', 'LineWidth', 2);
corrc = corrcoef(spotdata(logmy,6), spotdata(logmy,8));
[fit_y, err_y] = fit(spotdata(logmy,6), spotdata(logmy,8), 'poly1');
xlabel('Focal spot camera y position / micron');
ylabel('Leakage spot camera y position / micron');
xx_ = get(gca, 'XLim');
plot(xx_, fit_y(xx_), '-r');
text(245, 252, sprintf('$$slope = %2.2f; R^2 = %2.3f$$', fit_y.p1, err_y.rsquare), 'Color', 'r');
text(245, 245, sprintf('$$p= %2.2f$$', corrc(1,2)));
make_latex(hfig);
setAllFonts(hfig, 14);
export_fig(fullfile(getDropboxPath, 'MATLAB', 'experiments', '2016_Zepf', 'PointingCorr_y'), '-png', '-nocrop', hfig);