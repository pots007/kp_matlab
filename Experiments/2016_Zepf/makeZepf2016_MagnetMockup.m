% This will make a rough mockup of the Zepf 2016 magnet setup - two ICL150,
% separated by a 28mm Al spacer plate, but at the same height.

load('Nelson_gemini_one_magnet')
ICL150x = x2-73; ICL150y = y2+60; ICL150map = data*1e-3;

%load('ICL120magnet2mm.mat');
%load('E:\Kristjan\Documents\Uni\Dropbox\MATLAB\guis\+MagnetMapping\ICL120magnet2mm.mat');
%ICL120x = y-236; ICL120y = x-44; ICL120map = map';
totgridx = -200:1:200;
totgridy = -70:100;
%offset120 = [-163 30];
%[X0,Y0] = meshgrid(ICL120x-offset120(1), ICL120y-offset120(2));
offset150_1 = [-89 10];
[X1,Y1] = meshgrid(ICL150x-offset150_1(1), ICL150y-offset150_1(2));
offset150_2 = [89 10];
[X2,Y2] = meshgrid(ICL150x-offset150_2(1), ICL150y-offset150_2(2));
[Xout,Yout] = meshgrid(totgridx, totgridy);
%mapint0 = interp2(X0,Y0,ICL120map,Xout,Yout, 'nearest', 0);
mapint1 = interp2(X1,Y1,ICL150map,Xout,Yout, 'nearest', 0);
mapint2 = interp2(X2,Y2,ICL150map,Xout,Yout, 'nearest', 0); 
mapint = mapint1+mapint2;
totgridx = fliplr(totgridx);
imagesc(fliplr(totgridx), totgridy, mapint); colorbar;
save('Zepf2016Mockup.mat', 'totgridx', 'totgridy', 'mapint')

