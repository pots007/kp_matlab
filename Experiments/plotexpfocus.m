load('expfocus.mat');
exper = 'Gem2013';
expdays = fieldnames(focus.(exper));
fwhms = zeros(length(expdays), 6);
for i=1:length(expdays)
    %radius
    fwhms(i,1) = mean(focus.(exper).(expdays{i}).data(11,:));
    fwhms(i,2) = sqrt(var(focus.(exper).(expdays{i}).data(11,:)));
    %x
    fwhms(i,3) = mean(focus.(exper).(expdays{i}).data(9,:));
    fwhms(i,4) = sqrt(var(focus.(exper).(expdays{i}).data(9,:)));
    %y
    fwhms(i,5) = mean(focus.(exper).(expdays{i}).data(10,:));
    fwhms(i,6) = sqrt(var(focus.(exper).(expdays{i}).data(10,:)));
end
figure(1); clf;
if strcmp(exper, 'Gem2014a')
    fwhms = fwhms*0.72; %Calibration for as Gem2014a 
end
hr = errorbar((1:length(expdays))+0.1, fwhms(:,1), fwhms(:,2), 'xr');
hold on;
hx = errorbar(1:length(expdays), fwhms(:,3), fwhms(:,4), 'ok');
hy = errorbar((1:length(expdays))-0.1, fwhms(:,5), fwhms(:,6), 'db');
hold off;
legend([hr hx hy], {'FWHM radius', 'FWHM x', 'FWHM y'}, 'Location', 'EastOutSide');
if strcmp(exper, 'Gem2014b')
    ylabel('Spot size / pixel')
else
    ylabel('Spot size / \mu m')
end
set(gca, 'XTick', 1:length(expdays), 'XTickLabel', expdays, 'YLim', [10 35]);
rotateXLabels(gca, 89);
set(gca, 'Position', [0.1 0.2 0.7 0.7])
set([hr hx hy], 'MarkerSize', 12, 'LineWidth', 1.5);
setAllFonts(1, 18)
export_fig(['/Users/kristjanpoder/Dropbox/MATLAB/Experiments/' exper],...
    '-nocrop', '-transparent', '-pdf', 1);