% measurePlasmaLensJitter.m
%
% Plot and analyse the stability of the plasma lens discharge, as
% experienced by Martin during one of his runs.

APL = Analysis.Diagnostic(20180531, 1, 'Lens_Current');

% Set analysis fcn to just show the trace
APL.setAnalysisFunction(@(x) x(:,2))
jitterData = APL.getAllAnalysed;
ss = APL.getRAWdiag(10);
%% Plot and find jitter or 90% current level
[~,ind] = min(abs(jitterData - 29000));

hfig = figure(23452345); clf(hfig);
hfig.Position = [300 400 600 500];
ax1 = makeNiceAxes(hfig, [0.1 .55 0.85 .4]);
ax2 = makeNiceAxes(hfig, [0.1 .1 0.85 .4]);
imagesc(ss(:,1), 1:size(jitterData,2), jitterData', 'Parent', ax1);
set(ax1, 'XLim', [671.1 672.5], 'YLim', [0 size(jitterData,2)], 'Layer', 'top',...
    'XTickLabel', []);
hh = shadedErrorBar(ss(:,1), jitterData', {@mean,@std});
set(ax2, 'XLim', ax1.XLim)
xlabel(ax2, 'Time / us');
ylabel(ax1, 'Shot number');
ylabel(ax2, 'Current / ADC counts');
setAllFonts(hfig, 12);
text(ax2, 671.8, 26e3, sprintf('$\\sigma_\\mathrm{I=29000}$ = %1.2f us', std(ind)*mean(diff(ss(:,1)))),...
    'FontSize', 22);
make_latex(hfig); 
%export_fig('/home/fflab28m/DATA/GDDmeasurement/Martin_stab_20180531', '-png', '-m4', '-nocrop', hfig);