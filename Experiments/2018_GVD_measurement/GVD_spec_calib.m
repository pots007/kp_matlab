% gddSpectrometerCalibration.m

datfol = '/home/kpoder/DESYcloud/Experiments/2018_GVDmeasurement/CalibData';
fname = 'Chamber_screen1 (22403046)_20180730_160135453_0001.tiff';
fnamebg = 'Chamber_screen1 (22403046)_20180730_160205289_0001.tiff';
imm = imread(fullfile(datfol, fname));
bg = imread(fullfile(datfol, fnamebg));

imm = double(imm);

roi = [1, 515, 1919, 450];
immROI = imm(roi(2):roi(2)+roi(4),roi(1):roi(1)+roi(3));

immline = sum(immROI);
bgroi = [1:800, 1000:1150, 1300:1400, 1800:1900];
bgfit = fit(bgroi', immline(bgroi)', 'poly2');

pixax = 1:1920;
plot(pixax, immline, pixax, bgfit(pixax), 'r');
immlinebg = immline - bgfit(pixax)';
plot(pixax, immlinebg)

lines = [546, 435.833, 407, 365.015, 313, 253.652];

% First, rough try
[~,cc] = findpeaks(immlinebg, 'MinPeakHeight', 1e6)

lfit = polyfit(cc([1,2,4]), lines([3,2,1]), 1)
plot(polyval(lfit, pixax), immlinebg)
%plot(immlinebg)
lambdaax = polyval(lfit, pixax);

plot(lambdaax, sum(imread(fullfile(datfol, 'BBO1.tiff'))));