% plotFringeStability20180823.m

dfol = '/home/kpoder/DATA/GVD_measurement/20180823';
fname_ = '0mbar_stability_fixedROI2.h5';
fname = fullfile(dfol, fname_);

finfo = h5info(fname);

nShots = length(finfo.Groups);
lineoutraw = h5read(fname, '/shot0004/lineout');

lineouts = zeros(nShots, size(lineoutraw,1));
% The edges is: [left index, max index, right index, COM of fringe];
edges = zeros(nShots, 4);
gdds = zeros(nShots,1);
for shotID=1:nShots
    gdds(shotID) = h5read(fname, sprintf('/shot%04i/delay', shotID-1));
    try
        lineoutraw = h5read(fname, sprintf('/shot%04i/lineout', shotID-1));
    catch
        gdds(shotID) = 0;
        continue
    end
    if mean(lineoutraw(:,2)) < 4
        gdds(shotID) = 0;
        continue
    end
    
    % Filtering!
    lineout = sgolayfilt(lineoutraw(:,2), 3, 15);
    
    % Check if we lose any information...
    %plot(lineoutraw(:,1), lineout, lineoutraw(:,1), lineoutraw(:,2), '-');
    
    % Now look at the gradient
    dlineout = sgolayfilt(gradient(lineout), 3, 15);
    %plot(lineoutraw(:,1), dlineout)%, lineoutraw(:,1), gradient(lineout), '-');
    
    %plot(lineoutraw(:,1), lineout,lineoutraw(:,1), dlineout*100);
    
    % Find the width of the first fringe: find the peak and then the first
    % zerocrossing of the gradient on either side of it.
    [~,pind] = max(lineout);
    extrema = findZeroCrossings(dlineout);
    % Find the locations
    rind = extrema(extrema>pind+30); rind = ceil(rind(1));
    lind = extrema(extrema<pind-30); lind = floor(lind(end));
    %plot(lineoutraw(:,1), lineout, [lineoutraw(lind,1), nan, lineoutraw(rind,1)],...
    %    [0,0,0], 'x')
    
    % Calculate the CoM of this fringe now
    cc = centroidlocationsmatrix(lineout(lind:rind));
    comind = lind+round(cc(2));
    
    % Keep data for later!
    lineouts(shotID,:) = lineoutraw(:,2)';
    edges(shotID,:) = [lind, pind, rind, comind];
    
end

rhocal = 4*c*epsilon0*me*omega0/(qe^2*0.015)*1e-6;
rhocalT = 8*epsilon0*me*c*omega0^2/(3*qe^2*0.015)*1e-6;

% Do some filtering as some shots are only noise!
lineouts = lineouts(gdds~=0,:);
edges = edges(gdds~=0,:);
gdds = gdds(gdds~=0);
nShots = length(gdds);
%% Now do some plotting

% The lineouts and the found edges and peaks.
hfig = figure(35114); clf(hfig);
ax1 = makeNiceAxes(hfig,[0.1, 0.4, 0.7, 0.52]);
ax2 = makeNiceAxes(hfig,[0.82, 0.4, 0.15, 0.52]);
ax3 = makeNiceAxes(hfig,[0.1 0.07 0.4 0.25]);
ax4 = makeNiceAxes(hfig,[0.57 0.07 0.4 0.25]);
imagesc(lineoutraw(:,1)*1e9, 1:nShots, lineouts, 'Parent', ax1);
cols = 'rkgw';
cols0 = brewermap(9,'set1'); cols = cols0([7,5,3,1],:);
for k=1:4
    %hh(k) = plot(lineoutraw(edges(:,k),1)*1e9, 1:nShots, ['-' cols(k)]);
    hh(k) = plot(ax1,lineoutraw(edges(:,k),1)*1e9, 1:nShots, '-','Color', cols(k,:));
end
axis(ax1, 'tight');
set(hh, 'LineWidth', 2);


% Now the GDD variation
plot(ax2, gdds*1e15, 1:nShots, '.k');
set(ax2, 'YTickLabel', [], 'XLim', [157 171], 'YLim', ax1.YLim);

% And histograms...
rhovar = edges(:,4)/mean(edges(:,3)-edges(:,1));
% There are some HUGE outliers in the rhovar here: exclude them
rhovar( abs(rhovar-mean(rhovar)) > 10*std(rhovar) ) = [];

h1 = histogram(ax3, rhovar-mean(rhovar), 20, 'FaceColor', cols(3,:));

h2 = histogram(ax4, gdds*1e15, 20, 'FaceColor', cols(3,:));

set(ax1, 'Layer', 'top')
xlabel(ax1, 'Wavelength / nm');
ylabel(ax1, 'Shot number');
xlabel(ax2, 'GDD / fs');
xlabel(ax3, 'Phase / rad');
ylabel(ax3, 'Counts');
xlabel(ax4, 'GVD value / fs');
ylabel(ax4, 'Counts');
title(ax1, 'Single shot spectra + fringes edges');
title(ax2, 'Measured GVD');
title(ax3, 'Histogram of phases');
title(ax4, 'Histogram of measured GDD');
text(-0.085, 750, sprintf('\\sigma_{\\phi}=%2.3f rad',std(rhovar)), 'Parent', ax3);
text(-0.085, 650, sprintf('\\sigma_{n_e}=%2.2e cm^{-3}', std(rhovar)*rhocal), 'Parent', ax3);
text(166, 650, sprintf('\\sigma_{GDD}=%2.2f fs',std(gdds)*1e15), 'Parent', ax4);
text(166, 550, sprintf('\\sigma_{n_e}=%2.2e cm^{-3}', std(gdds)*rhocalT), 'Parent', ax4);

setAllFonts(hfig, 12);

% And Uebertitle: the run this is
ha = annotation('textbox', [0.2, 0.95, 0.6 0.04], 'String', '20180823, stability run at 0mbar',...
    'HorizontalAlignment', 'center', 'EdgeColor', 'none', 'FontSize', 16);

savfols = {'/home/kpoder/DATA/GVD_measurement/', '/home/kpoder/DESYcloud/Experiments/2018_GVDmeasurement'};
for s=savfols
    saveas(hfig, fullfile(s{1}, '20180823_stability_0mbar.svg'));
end
   

