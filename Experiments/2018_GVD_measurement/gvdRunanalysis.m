% gvdRunanalysis.m

dfol = '/home/fflab28m/DATA/Jimmy/52.1mbar';
flist = dir(fullfile(dfol, '*.tiff'));
imMatrix = zeros(400,1920, length(flist), 'uint16');
for f=1:length(flist)
    fprintf('File %i\n', f)
    im = imread(fullfile(dfol, flist(f).name));
    imMatrix(:,:,f) = im(400:799,:);
end

%%

% reduce to lineouts
cropShit = false;

imLines = squeeze(sum(imMatrix(150:180,:,:),1));
if cropShit
    imLines(1:800,:) = 0;
end
imLinesN = imLines./repmat(max(imLines,[],1), [size(imLines,1),1]);

imFFT = fftshift(fft(imLines,2^12,1),1)';
imFFTabs = abs(imFFT);
imFFTabs = imFFTabs./repmat(max(imFFTabs(:,2060:end),[],2), [1,size(imFFTabs,2)]);

% Plot things to understand

hfig = figure(35235); clf(hfig);
hfig.Position = [300, 500, 1200, 700];
locs = GetFigureArray(2, 1, [0.05 0.02 0.07 0.08], 0.02, 'across');
ax1 = makeNiceAxes(hfig, locs(:,1));
imagesc(imLinesN', 'Parent', ax1);
ax2 = makeNiceAxes(hfig, locs(:,2));
imagesc(abs(imFFTabs));
set([ax1, ax2], 'YLim', [0 size(imLines,2)], 'Layer', 'top', 'XTick', []);
set(ax2, 'XLim', [2000, 2100], 'YTickLabel', [], 'CLim', [0,1]);
set(ax1, 'XLim', [200, 1600]);
colormap(jet_white(256))
title(ax1, 'Lineouts of images');
title(ax2, 'FFTs of the images');
ylabel(ax1, 'Shot number');
xlabel(ax1, 'Wavelength / pixel');
xlabel(ax2, 'Time / pixel');
setAllFonts(hfig, 16); make_tex(hfig);
if cropShit; fname='20180820_DensityScan_crop'; else; fname = '20180820_DensityScan'; end
export_fig(fullfile('/home/fflab28m/DATA/GDDmeasurement', fname), '-png', '-nocrop', '-m4', hfig)