function [outdata headings] = GetGeminiShotsData(exp)
%exp = 'Nov2013';
switch exp
    case 'Nov2013'
        if(ispc)
            filen = 'E:\Kristjan\Documents\Uni\Imperial\2013_Nov_Gemini\Laserdata\SHOT_90182_93382.csv';
        elseif(ismac)
            filen = '/Volumes/Drobo/Experimental Data/2013GeminiNajmudin_backup/LaserData/SHOT_90182_93382_USER_SELECTION.csv';
        end
    case 'Aug2014'
        if (ispc)
            filen = 'E:\Kristjan\Documents\Uni\Imperial\2014_ManglesGemini\ecat\INV_13210021_USER_SELECTION2.csv';
        elseif ismac
            filen = '/Users/kristjanpoder/Dropbox/MATLAB/Experiments/2014_Gemini/INV_13210021_USER_SELECTION2.csv';    
        end
    case 'Aug2015'
        if ispc
            filen = 'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\EcatRAW\DATES_2015-08-17_2015-09-27_USER_SELECTION.csv';
        end
    otherwise
        disp('Not a valid experiment name! Options are:');
        disp('Nov2013');
        disp('Aug2013');
        outdata = [];
        return;
end
data = csvimport(filen);
numdiag = 8;
Ind = zeros(1,numdiag);
Ind(1) = sum(strcmp('"Id"', data(1,:)).*(1:size(data,2))); %GSN
Ind(2) = sum(strcmp('"Time"', data(1,:)).*(1:size(data,2))); %Time
Ind(3) = sum(strcmp('"S_uncomp_nf_e"', data(1,:)).*(1:size(data,2))); 
Ind(4) = sum(strcmp('"Xduc_8_reg_1_chamber_pressure_value"', data(1,:)).*(1:size(data,2)));
Ind(5) = sum(strcmp('"Xduc_9_reg_2_chamber_pressure_value"', data(1,:)).*(1:size(data,2)));
Ind(6) = sum(strcmp('"Xduc_10_reg_3_chamber_pressure_value"', data(1,:)).*(1:size(data,2)));
Ind(7) = sum(strcmp('"Xduc_11_reg_4_chamber_pressure_value"', data(1,:)).*(1:size(data,2)));
Ind(8) = sum(strcmp('"S_frog_pl_value"', data(1,:)).*(1:size(data,2)));
outdata = cell(size(data,1)-1, numdiag);
for i=2:size(data,1)
    for k=1:numdiag
        if k==1
            outdata{i-1,k} = str2double(data{i,1}(2:end-1));
            continue;
        end
        if k==2
            da = datevec(data{i,Ind(k)});
            outdata{i-1,k} = sprintf('%02i:%02i:%02i', da(4), da(5),round(da(6)));
            continue;
        end
        if Ind(k)~=0
            outdata{i-1,k} = data{i,Ind(k)};
        end
    end
end

headings = {'GSN', 'Time', 'S_uncomp_E', 'Regulator_1', 'Regulator_2', 'Regulator_3',...
    'Regulator_4', 'S_FROG_pulselength'};
% for i=1:size(data,1)-1
%     odata(i).GSN = str2double(data{i+1,1}(2:end-1));
%     da = datevec(data{i+1,Ind(2)});
%     odata(i).Time = sprintf('%02i:%02i', da(4), da(5));
%     
%     
% end
%odata = struct('GSN', cellfun(@(x) str2double(x(2:end-1)), data(2:end,1)))
end