function him = playGloriousMontage

hObject = figure('units','pixels',...
    'position',[50 50 150 100],... %[left bottom width height]
    'menubar','none',...
    'numbertitle','off',...
    'name','Montage',...
    'Tag', 'montage',...
    'resize','off');

handles.but = uicontrol('style', 'pushbutton', 'units', 'normalized',...
    'Position', [0.1 0.1 0.8 0.8], 'String', 'Got the point!', ...
    'Callback', @butcallback);


drawnow;
handles.fig = hObject;
guidata(hObject, handles);
playGloriousSong(hObject, handles);

function playGloriousSong(hObject, handles)
[Y, FS] = audioread('montage.wav');
size(Y);
handles.player = audioplayer(Y, FS);
guidata(hObject, handles);
play(handles.player);

function butcallback(hObject, handles)
disp('Good you got the message');
handles = guidata(hObject);
stop(handles.player);
close(handles.fig);