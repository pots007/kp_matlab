dataf = '/Users/kristjanpoder/TL/';
dates = dir(dataf);
for i=13:length(dates)
    if ~dates(i).isdir; continue; end
    if sum(strcmp(dates(i).name, {'.' '..'})); continue; end
    files = dir(fullfile(dataf, dates(i).name, '*.avi'));
    if isempty(files); continue; end
    mkdir(fullfile(dataf, dates(i).name), 'temp');
    totframes = 1;
    for k=1:length(files)
        disp(['Working on file ' files(k).name]);
        vidobj = VideoReader(fullfile(dataf, dates(i).name, files(k).name));
        timebase = datenum(files(k).name(11:end-4), 'YYYY-mm-DD-HH-MM-SS');
        it = 0;
        while vidobj.hasFrame
            fram = readFrame(vidobj);
            timestring = datestr(timebase+it/(3600*24), 'HH:MM');
            fram = insertText(fram, [20 20], timestring, 'FontSize', 36, 'BoxColor', 'black', 'TextColor', 'w');
            imwrite(fram, fullfile(dataf, dates(i).name, ['temp/frame' num2str(totframes, '%05i') '.png']), 'png');
            totframes = totframes + 1;
            it = it+10;
        end
        delete(vidobj)
    end
    % And now make the video itself
    system(['~/ffmpeg/ffmpeg -framerate 30 -i ~/TL/' dates(i).name '/temp/frame%05d.png -c:v libx264 -r 30 -pix_fmt yuv420p ~/TL/' dates(i).name '/' dates(i).name '.mp4']); 
end