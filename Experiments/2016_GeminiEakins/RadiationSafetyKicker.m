% Plot some figures for Rob C to think about Radiation Safety


dfol = 'E:\Kristjan\Documents\Uni\Imperial\2016[02]_Gemini\RadiationSafety';
load(fullfile(dfol, 'KickerMagnetTracking'));

%% For the magnet gap
tracks = tracking.screen(1).heights;
hfig = figure(8); clf(8);
set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add');
plot(tracks(:,1), tracks(:,3), 'LineWidth', 2);
set(gca, 'XLim', [40 500], 'Box', 'on', 'YLim', [0 60]);
plot([0 5000], [15 15], '--k', 'LineWidth', 2);
ha = area([0 500],[0 15 100; 0 15 100]);
ha(1).Visible = 'off'; ha(2).Visible = 'off';
set(ha(3), 'FaceAlpha', 0.1, 'FaceColor', 'g');
xlabel('Electron energy / MeV');
ylabel('Distance from axis / mm');
text(70, 10, 'Magnet gap');
text(310, 19, 'Magnet pole');
make_latex(hfig); setAllFonts(hfig, 20);
export_fig(fullfile(dfol, 'KickerEnergiesMagnetGap'), '-png', '-nocrop', hfig);

%% For the lead wall
tracks = tracking.screen(2).heights;
hfig = figure(10); clf(hfig);
set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add');
plot(tracks(:,1), tracks(:,3), 'LineWidth', 2);
set(gca, 'XLim', [40 200], 'Box', 'on');
%plot([0 5000], [10 10], '--k', 'LineWidth', 2);
xlabel('Electron energy / MeV');
ylabel('Distance from axis / mm');
make_latex(hfig); setAllFonts(hfig, 20);
export_fig(fullfile(dfol, 'KickerEnergiesLeadWall'), '-pdf', '-nocrop', hfig);

%% Plot the kicker field as well

load(fullfile(getDropboxPath, 'ICT_release', 'version1.0', 'ICT_B_field_maps', 'IC_25mmKicker_1mm'));

hfig = figure(9); clf(hfig);
set(hfig, 'Color', 'w');
imagesc(x,y,map);
rectangle('Position', [-12.5 -12.5 25 25], 'LineWidth', 2);
cb = colorbar;
ylabel(cb, 'B / T');
xlabel('x / mm'); ylabel('y / mm');
make_latex(hfig); setAllFonts(hfig, 16);
export_fig(fullfile(dfol, 'KickerFieldMap'), '-pdf', '-nocrop', hfig);