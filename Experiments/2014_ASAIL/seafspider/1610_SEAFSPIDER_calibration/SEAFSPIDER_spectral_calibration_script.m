%%
clear
% pathz=tobiPathz;

%%
%[oo.I, oo.lambda, oo.omega] = read_oo_sample('../IR_spectrum_OOIBase.Master.Scope');
in(1).name = 'HGlamp_10frames.mat';
% in(2).name = '../405_10_frames.mat';
% in(3).name = '../532_10_frames.mat';
in(1).fftbgsubtract = 0;
%in(2).fftbgsubtract = 0;
%in(3).fftbgsubtract = 0;

Nf = length(in)

for nf = 1:Nf
    %%
    load(in(nf).name);
    
    tmp = squeeze(mean(processed.image,4));
    x_axis = processed.x;
    y_axis = processed.y;
    
    figure_docked('data2d');clf
    imagesc(x_axis, y_axis, tmp)
    size(tmp)
    
    cropaxis=[  0        1028         272         643 ];
    [ xc, yc, tmpc] = crop_array_to_axis(x_axis, y_axis, tmp, cropaxis);
    figure_docked('data2dcropped');clf
    imagesc(xc, yc, tmpc)
    
    data_packed_m_blue = scaleMax1(substract_bg_1d( makecol(mean(tmpc,1)) ));
    x = xc(:);%makecol(1:length(data_packed_m));
    [x,data_packed_mc] = crop_vector_to_axis(x, data_packed_m_blue, [1 1000]);
    
    if ~in(nf).fftbgsubtract
        tmp1 = substract_bg_1d(data_packed_mc);
        lo(:,nf) = tmp1;
    else
        bg=abs(fftfilter_1d(x, tmp1, [0 0.4 4], 1, [-1 1 1e-3 1]));
        tmp = tmp1-bg;
        figure_docked('bgsubtract');clf
        plot(x, tmp1, x, bg, x, tmp)
        lo(:,nf) = tmp;
        drawnow
    end
    
    figure_docked('data1d');clf
    plot(x, lo(:,nf))
    drawnow
end



figure_docked('data1d_all');clf
plot(x, lo)
hl=legend(in(:).name);
set(hl, 'interpreter', 'none')




%% find peaks:
lo_Peaks=[];
for nf=1:1
    lo_Peaks = [lo_Peaks;fpeak_top(x, lo(:,nf), 6 ,[1,length(x), 0.02, inf] )];
end
% lo_Peaks = [lo_Peaks; [11,0.04]];
% lo_Peaks = [lo_Peaks; [465,0.0771]];

[A,ix] = sort(lo_Peaks,1);
lo_Peaks=  lo_Peaks(ix(:,1),:);

figure_docked('data1dwithPeaks'); clf;
plot(x, lo);
title( sprintf('spectral lineout, %i peaks found', size(lo_Peaks,1)) )
hold on
for n=1:length(lo_Peaks)
    plot(lo_Peaks(n,1), lo_Peaks(n,2),'k+','MarkerSize',20)
    text(lo_Peaks(n,1), lo_Peaks(n,2),['\leftarrow',sprintf('%i) %i pix',n,lo_Peaks(n,1))],...
        'Rotation',90,'HorizontalAlignment','left')
end
hold off
xlabel('pixels')



%% define wavelengths:

%lamp_lines_l =  calibrationlamps('Oriel_Kr', [600 900])
%lamp_lines_l = [ lamp_lines_l ; 2*calibrationlamps('Oriel_Kr', [960 990]/2)]

% lamp_lines_l =  calibrationlamps('OcdeanOptics_HG1_HgPeaks', [600 1000])
% lamp_lines_l =  [ lamp_lines_l ; ...
range =  ([-150,200]+400)
% lamp_lines_l1 = calibrationlamps('OcdeanOptics_HG1_ArPeaks',range)
lamp_lines_l1 = makecol(calibrationlamps('OcdeanOptics_HG1_HgPeaks',range))
%calibrationlamps('OcdeanOptics_HG1_ArPeaks',range/2)
%calibrationlamps('OcdeanOptics_HG1_HgPeaks',range/2)

range =  ([-150,50]+507)
lamp_lines_l2 = makecol(2*calibrationlamps('OcdeanOptics_HG1_HgPeaks',range/2)) % second order mercury lines


% lamp_lines_l = [ 405; 545; 532 ; 253.65; 435.8330;546.0740];% 302.15; 365.01; 435.84; 546.08];
% lamp_lines_l = [ 405; 532 ; 253.65; 435.8330;546.0740];% 302.15; 365.01; 435.84; 546.08];
% lamp_lines_l = [ 406; 532; 253.65  ];% 302.15; 365.01; 435.84; 546.08];
% usepeaks = [ 3 6 9];

%lamp_lines_l1 =[2*253.6520 253.65 313.1550 334.1480 435.8330  407.7830 365.0150]; % mercury
%lamp_lines_l = lamp_lines_l1 + 12; % strange!!!!!!!!!!!!
%lamp_lines_l = [lamp_lines_l 405 532]
lamp_lines_l = [lamp_lines_l1;lamp_lines_l2]
usepeaks = [1 2 3 4 5];

Nlines = length(lamp_lines_l)

%% fitting:
lamp_lines_w = chng_rng(lamp_lines_l);
%x_arr=[];
ix=[];
for n=1:size(lo_Peaks,1)
    %if min(abs(reject-n)) ~= 0
    if min(abs(usepeaks-n)) == 0
        ix = [ix;n];
        %x_arr = [x_arr ; lo_Peaks(n,1)];
    end
end
x_arr = lo_Peaks(ix, 1);
weights =  lo_Peaks(ix, 2);
fitOrder = 1;
%[omega_arr best_red_chisqd] = assign_fit_peaks(x_arr, weights, lamp_lines_w, 3, 'backwards');
%lambda_arr = chng_rng(omega_arr);

[lambda_arr best_red_chisqd] = assign_fit_peaks(x_arr, weights, lamp_lines_l, fitOrder, 'forwards');
%[lambda_arr best_red_chisqd] = assign_fit_peaks(x_arr, weights, lamp_lines_l, fitOrder, 'backwards');
omega_arr = chng_rng(lambda_arr);

figure_docked('peak fitting result');clf; hold on
plot(x_arr, lambda_arr, '.-')
for n=1:length(x_arr)
    text(x_arr(n), lambda_arr(n)+2, sprintf('%i)', ix(n)))
end


%
%fitc = polyfit( x_arr, omega_arr , 2);
[fitc, fitc_cov] = polyfit_weighted_unnorm( x_arr, lambda_arr, 1./weights, fitOrder);
%fit = polyval(fitc, x_arr);
fitcurve = polyval(fitc, x);

h=figure_docked('calib_fit'); clf;
ax = axes('Parent', h);
plot(ax, x_arr, lambda_arr, '.');
hold on;
plot(x, fitcurve);
xlabel('x (pixels)');
ylabel('\lambda (nm)');
ticklabel_withformat(ax,'y','%.0f')
ax2 = yaxis_on_right(ax);
ylim(ax2, ylim(ax));
set(ax2, 'YTickLabel', num2str_cell(makecol(chng_rng(get(ax, 'YTick'))), '%.3f'));
set([ax,ax2], 'YMinorTick', 'on')
set(ax, 'YMinorGrid', 'on')
set([ax,ax2], 'XMinorTick', 'on')
set(ax, 'XMinorGrid', 'on')
ylabel('\omega (rad/fs)')
grid on
ind = 600;
% title(sprintf('Max. fit error = %.3f nm, slope at %.1f nm = %.3f nm/pix', max(abs(calib_array(:, 2) - fit)), ...
%     polyval(fitc, ind), polyval(polyder(fitc), ind)) )%, gca, 'tl');
polystrText=[];
for n=1:length(fitc)
    polystrText =  [polystrText,'  ',sprintf('%.2e \\pm %.3e x^%i', fitc(n), fitc_cov(n,n), length(fitc)-n ) ];
end
%polystrText
title( sprintf('slope at %.1f nm = %.3f nm/pix, poly = %s', polyval(fitc, ind), polyval(polyder(fitc), ind), polystrText)  )


% pixels = x;
omega = chng_rng(polyval(fitc, x))*1e15;

h=figure_docked('calib_spectrum');clf
plot( chng_rng(omega/1e15), lo);
title( sprintf('spectral lineout, %i peaks found, %i identified', size(lo_Peaks,1), size(lambda_arr,1)) )
hold on
for n=1:length(x_arr)
    plot( lambda_arr(n), weights(n), '.')
    text( lambda_arr(n), weights(n), ['\leftarrow',sprintf('%i), %.1f nm',...
        ix(n),lambda_arr(n))],'Rotation',90,'HorizontalAlignment','left')
end
hold off
xlabel('\lambda  (nm)')
axis tight
ylim([0 2])
% range(fitcurve)






%% saving
% pixels = x;
pixels = 0:1029;
omega = chng_rng(polyval(fitc, pixels))*1e15;
basename = datestr(now, 'yyyy_mm_dd_HHMM');
save([basename,'_spec_calib'], 'omega', 'pixels');

sav_figs = {'calib_fit', 'calib_spectrum'};

for n=1:length(sav_figs)
    h=figure_docked(sav_figs{n});
    ffn = [basename, '_', get(h,'name')];
    add_figure_timestamp(h, ffn);
    %plotsaver(h, ffn, 'pdf');
    plotsaver(h, ffn, 'png');
    %plotsaver(h, ffn, 'fig');
end

%%
% pixels = x;
% omega = chng_rng(polyval(fitc, x))*1e15;
% save([datestr(now, 'yyyy_mm_dd_HHMM'),'_spec_calib'], 'omega', 'pixels');
%


