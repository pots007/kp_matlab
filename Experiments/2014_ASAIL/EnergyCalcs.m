E0 = 8; %J main pulse energy after PMs
tau = 40e-15; %Optimistic, but hey!
R = 0.5; %No idea...
T = 1;
D_refl = 0.013; %pickoff size for reflected
D_refl_beam = 0.005; %Beam size for reflected
D_trans.D = 0.05;
D_trans.F = 0.5;
D_trans.beam = 0.008;

E_R.lost = E0*R*(D_refl/0.15)^2;
E_R.pickoff = E0*R*(D_refl/0.15)^2;
E_R.beam = E0*R*(D_refl_beam/0.15)^2
E_R.bint = 1e-4*E_R.beam/(tau*pi*0.25*D_refl_beam) %in W/m^2

E_T.spherical = E0*T*(D_trans.D/(0.15*D_trans.F/0.3))^2;
E_T.beam = E_T.spherical*(D_trans.beam/D_trans.D)^2;
E_T.spectrobeamI = 1e-4*E_T.spherical/(tau*0.25*pi*D_trans.D^2)