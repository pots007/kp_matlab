figure(987)
lambda = 0.3:0.001:1; %lambda in microns;
n_FS = Sellmeier(lambda, 'FS');
n_BK7 = Sellmeier(lambda, 'BK7');
%Assuming plano-convex, f=3.5 at lambda
lambda0 = 0.632;
f = 3.;
R_BK7 = (Sellmeier(lambda0, 'BK7')-1)*f;
R_FS = (Sellmeier(lambda0, 'FS')-1)*f;
D = 0.15;
w0 = 0.9*lambda0*1e-6*f/D; %Gaussian with volume equal to Airy pattern
zR = pi*w0^2/(lambda0*1e-6);
f_FS = R_FS./(n_FS-1);
f_BK7 = R_BK7./(n_BK7-1);
h1 = plot(lambda, f_FS);
hold on;
h2 = plot(lambda, f_BK7, 'r');
plot([0.3 1], (f+zR)*[1 1], '--k');
plot([0.3 1], (f-zR)*[1 1], '--k');
hold off
xlabel('\lambda / \mu m');
ylabel('Focal length/ m');
legend([h1 h2], {'Fused Silica', 'BK7'}, 'Location', 'Southeast')
setAllFonts(987, 20);
%export_fig('FocalDepth', '-pdf', '-transparent', '-nocrop', 987);