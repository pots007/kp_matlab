Constants
E = 5; %J
tau = 45e-15; % in s
D = 100; % in mm
F = 3000; % in mm
aberfac = 1.0;
lambda = 0.8e-6;
omega0 = 2*pi*c/lambda;
FWHM = aberfac*1.03*lambda*F/D;
Iw0 = aberfac*0.61*lambda*F/D;
Iw00 = aberfac*0.82*lambda*F/D %1/e^2 radius of intensity = 1/e radius of E-field
Ew0 = Iw00;
zR = pi*Ew0^2/lambda;
wz = Ew0*sqrt(1+(500e-6/zR)^2);
I0 = 0.299*E/(tau*Iw0^2);
IEn = I0*Ew0/wz;
Eamp = sqrt(2*IEn/(epsilon0*c));
offsets = [-500:100:500]*1e-15;
%offsets = offsets + ((1:11).^2*0.75)*1e-15;
%pulsetrain envelope width
tau_pt = 0.8e-12;
wt = tau_pt/sqrt(2*log(2));
pulseamps = exp(-offsets.^2/wt^2);
a0 = qe*Eamp.*pulseamps./(me*c*2*pi*c/8e-7);
gamma = sqrt(1+a0.^2*0.5);
re = qe^2/(me*c^2*4*pi*epsilon0);
dne = 1/(Ew0^2*pi*re)

oneD = false;
if (oneD)
    tempinput = getFileText('input1.deck');
else
    tempinput = getFileText('input0.deck');
end
fid = fopen('input.deck', 'w');
for i=1:52
    fprintf(fid, '%s\n', tempinput{i});
end

for i=1:length(offsets)
    fprintf(fid, '\n\n%s\n%s\n', tempinput{55}, tempinput{56});
    fprintf(fid, '  amp = %2.2e\n', Eamp*pulseamps(i));
    fprintf(fid, '%s\n%s\n%s\n', tempinput{58}, tempinput{59}, tempinput{60});
    fprintf(fid, '  t_profile = gauss(time, %i*femto+timezeropoint,timeomega)\n',...
        round(1.*(offsets(i)*1e15+500)));
    fprintf(fid, '%s\n', tempinput{62});
end

particles = true;
for i=63:length(tempinput)
    if (~particles)
        if (i==80 || i==82 || (i>85 && i<110))
            fprintf(fid, '# %s\n', tempinput{i});
        else
            fprintf(fid, '%s\n', tempinput{i});
        end
     else
        fprintf(fid, '%s\n', tempinput{i});
    end
end

fclose(fid);

