%compare beatwave generated multipulses to individual ones

omega1 = 2*pi*c/8e-7;
tau = 1e-12;
wt = tau/sqrt(4*log(2));
t = -1e-11:1e-15:1e-11;
domega = 2*pi/1e-13;
omega2 = omega1+domega;
w1 = exp(-t.^2./wt^2).*exp(1i*omega1.*t);
w2 = exp(-t.^2./wt^2).*exp(1i.*omega2.*t);
figure(2)
plot(t, abs(w1), 'r');
hold on;
plot(t, abs(w2), '--k');
hold off
plot(t, abs(w1+w2))
xlim([-6e-13 6e-13])

tau2 = 45e-15;
wt2 = tau2/sqrt(4*log(2));
offsets = [-500:100:500]*1e-15;
figure(3)
clf
pulses = zeros(length(offsets), length(t));
hold on;
for i=1:length(offsets)
    pulses(i,:) = exp(-(t-offsets(i)).^2./wt2^2).*exp(1i*omega1.*t);
    pulses(i,:) = pulses(i,:)*exp(-offsets(i)^2/wt^2);

end
hold off;
%plot(t, (sum(pulses,1)));
plot(t, abs(w1+w2)/2)
xlim([-6e-13 6e-13])
hold on;
plot(t, abs(hilbert(sum(pulses,1))), '--r');
hold off;

