
ne = 9*1.53e18;
Npulses = 11;
dtau = 90e-15;%2*pi/omegapcalc(ne);
%dtau = 56e-15;
tau = dtau*0.5;
I0 = GaussI0(3.33, 19.68e-6, (Npulses-1)*dtau)
offsets = -5*dtau:dtau:5*dtau;
uniform = true;
if (uniform)
    I0s = I0*exp(-offsets.^2/wt^2);
    E0s = sqrt(2*I0s/(epsilon0*c));
    a0s = qe*E0s./(me*c*2*pi*c/8e-7);
else
for i=1:2
    wt = (Npulses-1)*dtau/(2*sqrt(log(2)));
    I0s = I0*exp(-offsets.^2/wt^2);
    E0s = sqrt(2*I0s/(epsilon0*c));
    a0s = qe*E0s./(me*c*2*pi*c/8e-7);
    offsets = findBestMPspacing2(a0s, ne);
    offsets = offsets - 5*dtau;
end
end
oneD = false;
if (oneD)
    tempinput = getFileText('input1.deck');
else
    tempinput = getFileText('input0.deck');
end
fid = fopen('input.deck', 'w');
for i=1:52
    fprintf(fid, '%s\n', tempinput{i});
end

for i=1:length(offsets)
    fprintf(fid, '\n\n%s\n%s\n', tempinput{55}, tempinput{56});
    fprintf(fid, '  amp = %2.2e\n', E0s(i));
    fprintf(fid, '%s\n%s\n%s\n', tempinput{58}, tempinput{59}, tempinput{60});
    fprintf(fid, '  t_profile = gauss(time, %i*femto+timezeropoint,timeomega)\n',...
        round(1.*(offsets(i)*1e15 + 5*dtau*1e15)));
    fprintf(fid, '%s\n', tempinput{62});
end

particles = true;
for i=63:length(tempinput)
    if (~particles)
        if (i==80 || i==82 || (i>85 && i<110))
            fprintf(fid, '# %s\n', tempinput{i});
        else
            fprintf(fid, '%s\n', tempinput{i});
        end
    else
        fprintf(fid, '%s\n', tempinput{i});
    end
end

fclose(fid);