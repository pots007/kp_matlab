%Simon Hooker's Mathematica notebook in good old MATLAB

Constants;
lambda0 = 8e-7; omega0 = 2*pi*c/lambda0;
E0 = 15;
D = 100;
F = 3000;
E = (D/150)^2*E0;
tau = 35e-15;
Ew0 = 0.82*lambda0*F/D;
Np = 6;
dtau = 100e-15;
I0 = GaussI0(0.5*E, Ew0, Np*dtau)*1e-4;
L = 0.0005*10;
t = (-2500:0.5:2500)*1e-15;
lambda = (400:0.5:900)*1e-9;
locs = GetFigureArray(1,4,0.07, 0.05, 'down');
figure(43); clf;
ax1 = axes('Position', locs(:,1));
plot(lambda*1e9, RefIndex(2*pi*c./lambda, 'calcite', 'O'), 'r', ...
    lambda*1e9, RefIndex(2*pi*c./lambda, 'calcite', 'E'), 'k');
%title('Refractive index');
xlabel('Wavelength /nm');
n_E = @(w) RefIndex(w, 'Calcite', 'E')*w*L/c;
n_O = @(w) RefIndex(w, 'Calcite', 'O')*w*L/c;
phi_E = n_E(omega0);
phi_O = n_O(omega0);
dphi_E = fprime(n_E, omega0);
dphi_O = fprime(n_O, omega0);
ddphi_E = fpprime(n_E, omega0);
ddphi_O = fpprime(n_O, omega0);
a = 2*log(2)/(Np*dtau)^2;
b = pi/(dtau*abs(dphi_E-dphi_O));
gamma0 = a + 1i*b;
gamma1 = @(dphi) 1/(1/gamma0 - 2*1i*dphi);
t0 = 0.5*(dphi_E + dphi_O);

E_E = sqrt(0.5*gamma1(ddphi_E)/gamma0)*exp(1i*phi_E).*exp(1i*omega0.*(t+t0)).*exp(-gamma1(ddphi_E)*(t+t0-dphi_E).^2);
E_O = sqrt(0.5*gamma1(ddphi_O)/gamma0)*exp(1i*phi_O).*exp(1i*omega0.*(t+t0)).*exp(-gamma1(ddphi_O)*(t+t0-dphi_O).^2);

ax2 = axes('Position', locs(:,2));
plot(t*1e15, abs(E_O).^2, 'r', t*1e15, abs(E_E).^2, 'k');

E_tot = sqrt(0.5)*(E_O + E_E);

ax3 = axes('Position', locs(:,3));
plot(t*1e15, I0*abs(E_tot).^2, 'LineWidth', 1.5);
xlabel(' Delay / fs');
ylabel('Intensity / Wcm^{-2}');
%h1 = area(t*1e15, I0*abs(E_tot).^2, 'EdgeColor', 'k', 'FaceColor', 'm');
linkaxes([ax2 ax3], 'x');
setAllFonts(43,10)

ax4 = axes('Position', locs(:,4));
[autocor, lags] = xcorr(E_tot, 100);
plot(t(lags+round(length(t)*0.5))*1e15, abs(autocor).^2);

E_E1 = sqrt(0.5*gamma1(ddphi_E)/gamma0)*exp(1i*phi_E).*exp(1i*omega0.*(t+t0)).*exp(-gamma1(ddphi_E)*(t+t0-dphi_E).^2);
E_O1 = sqrt(0.5*gamma1(ddphi_O)/gamma0)*exp(1i*phi_O).*exp(1i*omega0.*(t+t0)).*exp(-gamma1(ddphi_O)*(t+t0-dphi_O).^2);
E_tot1 = sqrt(0.5)*(E_O1 - E_E1);
omegal = fftX((t), E_tot1, 2^14);
omega = omegal(:,1);
spectrum = omegal(:,2).*exp(-1i*omega.*t0);
hW = plotWignerFancy(omega, spectrum);
setAllFonts(hW, 10);
try
    dataf = GetDataSDF('MPsim1i/data/0005.sdf');
    x = dataf.Grid.Grid.x;
    tsim= x(1:end-1)./c;
    Ey = dataf.Electric_Field.Ey.data';
    Eyl = Ey(ceil(0.5*size(Ey,1)),:);
    a0l = a0calcE(abs(hilbert(Eyl)), 8e-7);
    t0 = -480;
    figure(789)
    h1 = plot(tsim*1e15-t0, 1*a0l, '--k', 'LineWidth', 2);
    hold on;
    h2 = plot(t*1e15, a0calcI(I0*abs(E_tot).^2, 0.8), '--r', 'LineWidth', 1.5);
    hold off;
    xlabel('Delay /fs');
    ylabel('a0')
    ylim([0 0.5])
    setAllFonts(789,22);
    saveas(789, 'Pulsetrain_overlay.fig');
catch

end
