function him = plotMP1DAll(densit)
n0 = densit*1e6;
densit = sprintf('%1.2fe18', densit*1e-18);
Constants;
load divmap.mat;
simtype = 'data';
files = dir([simtype '/*.sdf']);
if (isempty(files))
    him = [];
    return;
end
figure(2)
cla;
set(2, 'WindowStyle', 'docked');
dataf = GetDataSDFchoose([simtype '/' files(7).name], 'ey');
gridl = length(dataf.Electric_Field.Ey.data);
tims = zeros(length(files),1);
ne = zeros(length(files), gridl);
Ey = zeros(length(files), gridl);
omega = zeros(2^(nextpow2(gridl)+1), length(files));

for i=1:length(files)
    dataf = GetDataSDFchoose([simtype '/' files(i).name], 'grid');
    %disp(files(i).name);
    tims(i) = dataf.time;
    x = dataf.Grid.Grid.x;
    x = x(1:length(x)-1);
    dataf = GetDataSDFchoose([simtype '/' files(i).name], 'ey');
    Eyl = dataf.Electric_Field.Ey.data;
    Ey(i,:) = abs(hilbert(Eyl));
    numpnts = 2^(nextpow2(length(Eyl))+1);
    omegal = fftX(x./c, Eyl, numpnts);
    omegaax = omegal(:,1);
    omega(:,i) = omegal(:,2).*conj(omegal(:,2));
    omega(:,i) = omega(:,i)./max(omega(:,i));
    dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
        'number_density/electron');
    ne(i,:) = dataf.Derived.Number_Density.electron.data;
end
fsiz = 16;
subplot(2,2,[2 4])
imagesc(tims.*c,x-x(end), ne'./n0);
xlabel('Propagation distance (m)', 'FontSize', fsiz);
ylabel('\xi (\mu m)', 'FontSize', fsiz);
title(['n_e, n_e=' densit]);
set(gca, 'CLim', [0 8]);
cb = colorbar;
set(get(cb, 'YLabel'), 'String', '\delta n/n_0', 'FontSize', fsiz);
subplot(2,2,1)
imagesc(tims.*c,x-x(end), Ey');
xlabel('Propagation distance (m)', 'FontSize', fsiz);
ylabel('\xi (\mu m)', 'FontSize', fsiz);
title(['E_y, n_e=' densit]);
subplot(2,2,3)
imagesc(tims.*c, omegaax, omega);
xlabel('Propagation distance (m)', 'FontSize', fsiz);
ylabel('\omega (rad/s)', 'FontSize', fsiz);
omega0 = 2*pi*c/8e-7;
set(gca, 'YLim', [1.8e15 3e15]);
title(['\omega, n_e=' densit]);
%saveas(2, 'output.fig');
setAllFonts(2, 20);
%export_fig(['output_' densit], '-pdf', '-nocrop', '-transparent', 2);

him = 2;
end