figure(3);
Constants;
ne = 4e18;
ximax = 15;
clf;
a0s = [0.42214, 0.51484, 0.60122, 0.67094, 0.71404, 0.72318, 0.69557, 0.63398, 0.54640, 0.44431, 0.34012];
locs = GetFigureArray(2, 6, 0.05, 0.05, 'down');
xi0 = zeros(size(a0s));
a0 = a0s;
xi0(1) = 0.25;
a0(2:end) = 0;
[xi, a, dn] = multipulseNLwake(a0, xi0, ne, ximax);
ax = zeros(size(a0s));
ax(1) = axes('Position', locs(:,1));
plotyy(xi,a, xi, dn);
[AX,~,~] = plotyy(xi,a, xi, dn); set(AX(1), 'YLim', [0 1]);
title('1 pulse');

optimisation loop
for iter=2:11
    a0 = a0s(1:iter);
    a0(iter+1:end) = 0;
    xi1 = linspace(0.75, 1.5, 40);
    adder = zeros(40,length(a0s));
    adder(:,iter) = xi1;
    
    maxi = 0;
    maxv = 0;
    ax(iter) = axes('Position', locs(:,iter));
    for k=1:25
        xi0 = [0.25 0.25+xi1(i)];
        xi0it = xi0;
        xi0it(iter) = xi0(iter-1);
        xi0it = xi0it + adder(k,:);
        [xi, a, dn] = multipulseNLwake(a0, xi0it, ne, ximax);
        if (max(dn)>maxv)
            maxi = k;
            maxv = max(dn);
        end
        [AX,~,~] = plotyy(xi,a, xi, dn); set(AX(1), 'YLim', [0 1]);
        drawnow;
        pause(0.01)
    end
    xi0it = xi0;
    xi0it(iter) = xi0(iter-1);
    xi0it = xi0it + adder(maxi,:);
    xi0 = xi0it;
    [xi, a, dn] = multipulseNLwake(a0, xi0, ne, ximax);
    plot(xi,a, xi, dn);
    [AX,~,~] = plotyy(xi,a, xi, dn); set(AX(1), 'YLim', [0 1], 'YTick', 0:0.2:1);
    if (max(dn)>1); set(AX(2), 'YLim', [-1 ceil(max(dn))]); end;
    title([num2str(iter) ' pulses, \delta n /n_0 = ' num2str(max(dn))]);    
end




tau = 2*pi/omegapcalc(ne)*1e15;

fprintf('%6.i\t', 1:length(a0s));
fprintf('\n');
fprintf('%6.0f\t', tau*(xi0-0.25));
fprintf('\n');