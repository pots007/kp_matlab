% Trying out SRSI generation. The XPW frequency is from Canova2008.
Constants;
sigmax = -1.08; %From Desalvo1992
xixxxx = 1.59e-22; % In m2/V2, from Desalvo
n = 1.4704; %From refractiveindex.info
beta = pi/8;
gammap = -3/8*omega0/(c*n)*xixxxx*0.25*sigmax*sin(4*beta);
L = 1e-3; % in m
% Get spectral intensity from fft
t = linspace(-100,100,2^10)*1e-15;
I_0 = 3e12*1e4;
E_0 = sqrt(2*I_0/(epsilon0*c)); %I in W/m^2
fwhm = 30e-15; %intensity duration
sig = fwhm/sqrt(2*log(2));
E = E_0*exp(-t.^2/sig^2).*exp(1i*omega0*t);
omeg = fftX(t, E, 2^12);
%plot(omeg(:,1), omeg(:,2).*conj(omeg(:,2)))
%plot(t, 0.5*epsilon0*c*E.*conj(E))
%A0 = max(abs(omeg(:,2)));
%BW = 0.5e14;
BW = abs(0.5*width1e(abs(omeg(:,2))')*(omeg(1,1)-omeg(2,1)));
En = pi*50e-6^2*sqrt(pi)*sig*I_0;
A_0 = sqrt(En/sqrt(pi*BW^2))
%%
A = @(w,phi2,phi3) A_0*exp(-(w-omega0).^2/(2*BW^2)).*exp(1i*0.5*phi2*(w-omega0).^2+1i/6*phi3*(w-omega0).^3);
omega = [2.0:0.005:2.7]*1e15;
B = zeros(size(omega));
phi2 = 500e-30;
phi3 = 0;
for i=1:length(omega)
    disp(num2str(i));
    for k=1:length(omega)
        for l=1:length(omega)
            B(i) = B(i) -1i*gammap*L*A(omega(l),phi2,phi3)*A(omega(k),phi2,phi3)*conj(A(omega(l)+omega(k)-omega(i),phi2,phi3))*(0.005^2);
        end
    end
end

figure(1)
myplotyy(gca, omega, abs(A(omega,phi2,phi3)), abs(B));

%% birefringence for replica generation, along with polarisation angle
L = 0.002;
n_E = @(w) RefIndex(w, 'Calcite', 'E')*w*L/c;
n_O = @(w) RefIndex(w, 'Calcite', 'O')*w*L/c;
tg_E = fprime(n_E, omega0);
tg_O = fprime(n_O, omega0);
dt = -tg_E+tg_O;
Ap = A(omega,phi2,phi3)*cosd(0.5);

% The intereference
figure(2)
plot(omega, abs(Ap+B.*exp(1i*omega*dt)), 'x-')