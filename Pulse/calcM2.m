% Calculates the M2 of a nongaussian spot
%
% Inputs: w0,R - the measured spot size
%         fno - fnumber
%         lambda - defaults to 800 nm if omitted
%
% All inputs in SI!

function [M2, zRR, zR] = calcM2(w0R, Fno, lambda0)

if nargin<3
    lambda0 = 800e-9;
end

M2 = pi*w0R./(Fno*lambda0);
zRR = pi*w0R.^2./(lambda0.*M2);
zR = pi*w0R.^2./(lambda0);