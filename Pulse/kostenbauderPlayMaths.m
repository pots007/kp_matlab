syms A1 B1 C1 D1 E1 F1 G1 H1 I1
syms A2 B2 C2 D2 E2 F2 G2 H2 I2
syms A3 B3 C3 D3 E3 F3 G3 H3 I3
syms A4 B4 C4 D4 E4 F4 G4 H4 I4
K1 = [A1 0 0 E1; 0 D1 0 F1; G1 0 1 0; 0 0 0 1];
K2 = [A2 0 0 E2; 0 D2 0 F2; G2 0 1 0; 0 0 0 1];
K3 = [A3 0 0 E3; 0 D3 0 F3; G3 0 1 0; 0 0 0 1];
K4 = [A4 0 0 E4; 0 D4 0 F4; G4 0 1 0; 0 0 0 1];

syms L1 L2 L3
KL1 = sym(eye(4)); KL1(1,2) = L1;
KL2 = sym(eye(4)); KL2(1,2) = L2;
KL3 = sym(eye(4)); KL3(1,2) = L3;

kk = K4*KL3*K3*KL2*K2*KL1*K1