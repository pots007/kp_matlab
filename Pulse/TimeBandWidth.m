function TBPs = TimeBandWidth(shape)
TBPs = zeros(1,4);
t = -550:0.01:550;
t = t*1e-15;
pl = 7e-15;
shapes = {'Gaussian', 'Sech', 'Sech2', 'Lorentz'};
for i=1:4
    switch shapes{i}
        case 'Gaussian'
            tsig = exp(-(t/pl).^2).*exp(1i*(2*pi*3e8/8e-7.*t));
        case 'Sech'
            tsig = sech(t/pl).*exp(1i*(2*pi*3e8/8e-7.*t));
        case 'Sech2'
            tsig = sech(t/pl).^2.*exp(1i*(2*pi*3e8/8e-7.*t));
        case 'Lorentz'
            tsig = 1./((1 + (t/pl).^2)).*exp(1i*(2*pi*3e8/8e-7.*t));
    end
    I = (tsig .* conj(tsig));
    len = width2(I)*0.01e-15; %temporal FWHM length of pulse
    omega = fftX(t, tsig, 2^17);
    omega_w = omega(:,2).*conj(omega(:,2));
    omegaomega = width2(abs((omega_w))')*(omega(2,1)-omega(1,1));
    TBPs(i) = omegaomega*len/(2*pi);
    
    if (strcmp(shapes{i}, shape)==1)
        figure(1)
        set(1, 'WindowStyle', 'docked');
        cla;
        h1 = plot(t/7*1e15, real(tsig));
        hold on;
        h2 = plot(t/7*1e15, I, 'r');
        legend([h1 h2], 'Field', 'Intensity');
        hold off;
        title (['Electric field and intensity of ' shape ' shape pulse']);
        xlabel('Time (fs)');
        set(gca, 'XLim', [-150 150]);
        figure(2)
        set(2, 'WindowStyle', 'docked');
        cla;
        plot(omega(:,1), omega_w);
        set(gca, 'XLim', [2e15 2.7e15]);
        title (['Spectrum of ' shape ' shape pulse']);
        xlabel('Angular frequency (rad/s)');
    end
end

tp = 10e-15;
t0G = tp/1.177;
t0s = tp/1.763;
t0L = tp/1.287;
tsG = exp(-(t/t0G).^2).^2;
tss1 = sech(t/t0s).^2;
%tss2 = sech(t/pl).^2.*exp(1i*(2*pi*3e8/8e-7.*t));
tsL = 1./((1 + (t/t0L).^2)).^2;
figure(3)
set(3, 'WindowStyle', 'docked');
hG = plot(t*1e14, tsG);
hold on;
plot(t*1e14, tss1, ':');
plot(t*1e14, tsL, '--');
hold off;
set(gca, 'XLim', [-2 2]);



end