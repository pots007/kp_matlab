function t_g = GroupDelay(length, material)
%L = 0.001;
Constants;
L = length;
n = @(w) RefIndex(w, material, 'E')*w*L/c;
dphi = fprime(n, omega0);
t_g = dphi;
end

