% kostenbauderPlay.m
%
% Some fun with Kostenbauder matrices to understand pulse compressors (and
% eventualaly stretchers) a bit more.

function kostenbauderPlay

% Define some things, like grating period, distances etc
N = 1480; % lines per mm
L1 = 0.75; % Distance between gratings
L2 = 1.5; % Distance between bounces off second grating
L3 = L1;
thet1 = 35; % Incidence angle on first grating (between grating normal and beam)
thet2 = 35; % Incidence angle on second grating

% We assume the gratings are infinitely wide for now!

% Spectrum of the pulse:
lambda0 = 800;
dlambda = 30;
spec = linspace(lambda0-dlambda, lambda0+dlambda, 200)*1e-9;

% Define the incoming beam:
beam = 1;
switch beam
    case 1 % Collimated, nice beam.
        xx = linspace(-25,25,200)*1e-3;
        rays = cell(length(xx), length(spec));
        for x=1:length(xx)
            for s = 1:length(spec)
                rays{k} = [xx(x) 0., 0., spec(s)]';
            end
        end
end

% Calculate the full matrix:


function K = K_space(L)
    K = eye(4);
    K(1,2) = L;
end

function K = K_grating(lambda, thetI, n)
    c = 299792458;
    K = eye(4);
    thetO = asind(sind(thetI) - lambda*n);
    K(1,1) = -cosd(thetO)./sind(thetI);
    K(2,2) = 1/K(1,1);
    K(2,4) = lambda*(sind(thetO) - sind(thetI))/(c*cosd(thetO));
    K(3,1) = (sind(thetI) - sind(thetO))/(c*cosd(thetI));
end