E = 14; %J
tau = 35e-15;
D = 150;
F = 6000;
lambda = 0.8e-6;
FWHM = 1.03*lambda*F/D;
fprintf('Spot FWHM size is %2.3e\n', FWHM);
w0 = FWHM/(2*sqrt(log(2)));
w00 = w0*sqrt(2);
fprintf('Spot 1/e size is %1.3e\n', w0);
fprintf('Spot 1/e^2 size is %1.3e\n', w00);
%P = E/tau;
I = 0.299*E/(tau*w0^2);
I_Wcm = I*1e-4;
fprintf('Peak intensity is %1.3e W/cm^2\n', I_Wcm);
a0 = 0.856*sqrt(I_Wcm*1e-18*lambda*1e6);
fprintf('a0 is %f\n', a0);
Constants
%From k_p w_0 = 2*sqrt(a_0)
ne = 4*a0*epsilon0*me*c^2/(qe^2*w0^2);
%From Stuarts paper
%ne = (2*c*sqrt(2*epsilon0*me)/(w00*qe))^3*sqrt(E/(tau*17.4e9*1.75e27));
fprintf('Density for matched spot size is %1.3e cm^-3\n', ne*1e-6);
ymin = 65e-6;
y = linspace(-ymin, ymin, 2000);
plot(y, exp(-(y/w0).^2));

