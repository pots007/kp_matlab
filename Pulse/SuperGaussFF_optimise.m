
%Define NF grid
figure(135); clf;
F = 0.3;
npnts = 2^9;
locs = GetFigureArray(3, 2, 0.04, 0.06, 'across');
axs = zeros(6,1);
NFsize = 0.15;
[NFx, NFy] = meshgrid(linspace(-NFsize,NFsize,npnts), linspace(-NFsize,NFsize,npnts));
zsiz = 100;
r = sqrt(NFx.^2 + NFy.^2);
NFint = NFx.^2 + NFy.^2 < 0.075^2;
NF = double(NFint);

axs(1) = axes('Position', locs(:,1));
imagesc(NFx(1,:), NFy(:,1),NF);
%set(gca, 'XTick', [], 'YTick', []);
title('Initial near field intensity');

axs(4) = axes('Position', locs(:,4));
FFinitial = fftshift(fft2(NF, npnts, npnts));
FFinitial = FFinitial.*conj(FFinitial);
deltax = 2*pi/(x(1,2)-x(1,1));
xaxis = linspace(-0.5*deltax, 0.5*deltax, npnts);
xaxis = xaxis*0.8*0.15/(pi*F);
%imagesc(xaxis(0.5*npnts-zsiz:0.5*npnts+zsiz), xaxis(0.5*npnts-zsiz:0.5*npnts+zsiz),...
%    FFinitial(0.5*npnts-zsiz:0.5*npnts+zsiz, 0.5*npnts-zsiz:0.5*npnts+zsiz));
imagesc(xaxis, xaxis,FFinitial);
title('Initial FF distribution');
FFlims = [-50 50];
set(axs(4), 'XLim', FFlims, 'YLim', FFlims);
axs(3) = axes('Position', locs(:,3));
N = 6;
[x_ff,y_ff] = meshgrid(xaxis, xaxis);
R_ff = sqrt(x_ff.^2+y_ff.^2);
%wid = 2.*widthfwhm(FFinitial(0.5*npnts+1,:))*(xaxis(2)-xaxis(1));
widthFWHM = 60;
wid = widthFWHM/(log(2)^(1/(2*N)));
FFtarg = exp(-(R_ff./wid).^(2*N));
imagesc(xaxis, xaxis, FFtarg); colorbar;
%set(axs(3), 'XLim', FFlims, 'YLim', FFlims);
title('Target FF distribution');

%initialise first random phase distribution
load('InitialRanPhase.mat');
if (~exist('ranphase', 'var'))
    rFWHM = 0.018; %To keep same ratio no nearfield size
    rw0 = rFWHM/(2*log(2));
    mask2 = exp(-(NFx./rw0).^2 - (NFy./rw0).^2);
    ranphase = conv2(initialrandomphase, mask2);
    ranlen = length(ranphase);
    ranphase = ranphase(floor(0.5*ranlen)-size(NFx,1)*0.5:floor(0.5*ranlen)+size(NFx,1)*0.5-1,...
        floor(0.5*ranlen)-size(NFx,1)*0.5:floor(0.5*ranlen)+size(NFx,1)*0.5-1);
    fact = pi/(rms(ranphase(:)));
    ranphase = ranphase.*(fact);
end
axs(2) = axes('Position', locs(:,2));
imagesc(NFx(1,:), NFy(:,1),ranphase); colorbar;
title('Phase initialisation');
axs(5) = axes('Position', locs(:,5));
axs(6) = axes('Position', locs(:,6));

curphi = ranphase;
FFcal = xaxis(2)-xaxis(1);
[FFx, FFy] = meshgrid(xaxis, xaxis);

%looooooooooooooooop
for i=1:30000
    axes(axs(5));% = axes('Position', locs(:,5)); cla;
    imagesc(NFx(1,:), NFy(:,1),curphi); colorbar;
    title(['NF phase after it ' num2str(i)]);
    axes(axs(6));% = axes('Position', locs(:,6)); cla;
    FF_it = fftshift(fft2(NF.*exp(1i*curphi), npnts, npnts));
    FF_it_I = FF_it.*conj(FF_it);
    imagesc(xaxis, xaxis, FF_it_I); colorbar;
    %set(axs(6), 'XLim', FFlims, 'YLim', FFlims);
    title(['Far field after it ' num2str(i)]);
    hold on;
    maxI = 0.01*max(FF_it_I(:));
    la = centroidlocationsmatrix(FF_it_I);
    lax = la*(xaxis(2)-xaxis(1));
    %plot(lax(1)+min(xaxis),lax(2)+min(xaxis), 'xw', 'MarkerSize', 20);
    Nel = sum(sum((FF_it_I>maxI))); sqrt(Nel);
    %enratio = sum(sum(FF_it_I(FF_it_I<maxI)))/sum(sum(FF_it_I(FF_it_I>maxI)));
    enratio = sum(sum(FF_it_I(FFx.^2+FFy.^2<widthFWHM^2)))/sum(sum(FF_it_I));
    drawcircle(sqrt(Nel)*FFcal, min(xaxis)+lax(1), min(xaxis)+lax(2));
    hold off;
    drawnow;
    setAllFonts(135, 16);
    %pause(1);
    FF_old = (FF_it_I);
    if(sqrt(Nel)*FFcal>wid)
        FF_old(FF_old<maxI)=0;
        phiref = curphi;
        Iref = FF_it_I;
        FF_old = FF_old * 0.85*max(FF_it_I(:));
    else
        FF_old(FFx.^2+FFy.^2>wid^2)=0;
        FF_old = FF_old * 0.5*max(FF_it_I(:));
    end;
    
    FF_old = sqrt(FF_old).*exp(1i*angle(FF_it));
    NF_it = (ifft2(fftshift(FF_old), npnts, npnts));
    curphi = (angle(NF_it));
    disp(['Iteration ' num2str(i) ' with enratio = ' num2str(enratio)]);
    if (mod(i,1000)==0)
        export_fig(sprintf('SGspot/%05i', i), '-transparent', '-nocrop', '-pdf', 135);
    end
end


