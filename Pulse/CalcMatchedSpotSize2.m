Constants
E = 8; %J
tau = 40e-15; % in s
D = 150; % in mm
F = 6000; % in mm
aberfac = 1.2;
lambda = 0.8e-6;
omega0 = 2*pi*c/lambda;
FWHM = aberfac*1.03*lambda*F/D;
Iw0 = aberfac*0.61*lambda*F/D;
Iw00 = aberfac*0.82*lambda*F/D; %1/e^2 radius of intensity = 1/e radius of E-field
Ew0 = Iw00;
fprintf('\n\nSpot FWHM intensity is %2.3e\n', FWHM);
%w0 = FWHM/(2*sqrt(log(2)));
%w00 = w0*sqrt(2);
fprintf('Spot 1/e intensity is %1.3e\n', Iw0);
fprintf('Spot 1/e^2 intensity = 1/e field is %1.3e\n', Iw00);
%P = E/tau;
I = 0.299*E/(tau*Iw0^2);
I_Wcm = I*1e-4;
fprintf('Peak intensity is %1.3e W/cm^2\n', I_Wcm);
%a0 = 0.856*sqrt(I_Wcm*1e-18*lambda*1e6);
a0 = 0.94*E*qe^2*lambda^2/(pi^3*epsilon0*me^2*c^5*Ew0^2*tau);
a0 = sqrt(a0);
fprintf('a0 is %f\n', a0);
P = 0.94*E/tau;
%From k_p w_0 = 2*sqrt(a_0)
ne = 4*a0*epsilon0*me*c^2/(qe^2*Ew0^2);
%From Stuarts paper
ne = (2*c*sqrt(2*epsilon0*me)/(Ew0*qe))^3*sqrt(P/(17.4e9*1.75e27));
fprintf('Density for matched spot size is %1.3e cm^-3\n', ne*1e-6);
ymin = 95e-6;
y = linspace(-ymin, ymin, 2000);
Ld = 4*omega0^2*c*sqrt(a0)/(3*omegapcalc(ne*1e-6)^3);
fprintf('Dephasing length for this density is %1.3f mm\n', Ld*1e3);
Lpd = c*tau*(omega0/omegapcalc(ne*1e-6))^2;
fprintf('Pump depletion length for this density is %1.3f mm\n', Lpd*1e3);
dW = 2/3*a0*(omega0/omegapcalc(ne*1e-6))^2*0.511;
fprintf('Maximum energy gain for this density is %1.3f GeV\n', dW*1e-3);
%plot(y, exp(-(y/Ew0).^2));

