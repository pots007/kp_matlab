% Pulse length in fs, GDD in fs^2
% Pulse length intensity FWHM width.

function tout = ChirpedPulseLength(tin,GDD)
phidd = GDD;
beta = tin^2/(8*log(2));
tout = tin*sqrt(1+phidd.^2./(4*beta.^2)); %chirped pulse length