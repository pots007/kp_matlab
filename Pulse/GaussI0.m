% peak intensity of Gaussian spot
%Units in SI please!

function I0 = GaussI0(energy, Iw0, tau_fwhm)
Constants;
Itau0 = tau_fwhm/2*sqrt(log(2));
I0 = energy./(pi^1.5.*Iw0^2.*Itau0);