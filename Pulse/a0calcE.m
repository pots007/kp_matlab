% returns a0
% Everything in SI (E-field in V/m, lambda0 in m)

function a0 = a0calcE(Efield, lambda0)
Constants;
a0 = qe*Efield./(me*c*2*pi*c/lambda0);