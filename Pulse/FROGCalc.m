function FROG_E1 = FROGCalc(E1_t)
gridN = length(E1_t);
y = ifftshift(((0:gridN-1)'-gridN/2)*2*pi/((gridN)));
x = ((0:gridN-1)-gridN/2);
shiftMat = exp( 1i*y*x ); % needed for genFROG
E1Mat_w = repmat(fft(E1_t),[1 gridN]);
Esig1_t = ifft( E1Mat_w) .* ifft( E1Mat_w.*shiftMat); %E(t,tau) (tau is horizontal)
Esig1_w = fftshift(fft(Esig1_t,[],1),1); %E(w,tau)

FROG_E1 = abs(Esig1_w).^2; % Frog trace for E1_t
end