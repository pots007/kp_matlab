%returns total spectral phase coefficients nin fs and fs^2
%out(1) = dphi/domega
%out(2) = d2phi/domega2


function out = SpectralPhaseDelay(material, L, lambda0)
%L = 0.001; 
%lambda0 = 8e-7;
%material = 'FUSED SILICA';
c = 299792458;
omega = 2*pi*c/lambda0;
n = @(w) RefIndex(w, material);
dn = fprime(n, omega)*2*pi*c/(lambda0*1e3)^2;
dphi = n(omega)/c*(1 + fprime(n, omega))*L*1e15;
ddphi = 1/c*(2*fprime(n, omega) + omega*fpprime(n, omega))*L*1e30;
out(1) = dphi;
out(2) = ddphi;
end