%intensity in W/cm2, lambda in microns

function a0 = a0calcI(I, lambda)
    a0 = 0.856.*sqrt(I.*1e-18 .* lambda.^2);
end