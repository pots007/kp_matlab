function him = plotWignerFancy(varargin)
if nargin==2
    omega = varargin{1};
    spectrum = varargin{2};
elseif nargin==4
    omega = varargin{1};
    spectrum = varargin{2};
    a0 = varargin{3};
    ne = varargin{4};
end

numpnts = 2^12;
newom = linspace(min(omega), max(omega), numpnts)';
newr = interp1(omega, real(spectrum), newom);
newi = interp1(omega, imag(spectrum), newom);
newspec = complex(newr, newi);
W = WignerCalc(newspec);
timesig = ifftX(newom, newspec, numpnts);

speclim = [2 3]*1e15;
tlim = [-1000 1000];
fsiz = 14;
%Plot the wigner itself
figure(66); clf;
ax1 = axes('Units', 'normalized','Position', [0.33 0.33 0.65 0.65]);
imagesc(timesig(:,1)*1e15, newom*1e-15, W);
set(gca, 'YLim', speclim*1e-15, 'YDir', 'normal', 'FontSize', fsiz, ...
    'XLim', tlim, 'YTick', [], 'XTick', []);

%Plot the spectrum to the left of Wigner
normsp = newspec.*conj(newspec);
normsp = normsp./max(normsp);
ax2 = axes('Units', 'normalized','Position', [0.12 0.33 0.2 0.65]);
plot(normsp, newom*1e-15);
set(gca, 'YLim', speclim*1e-15, 'XDir', 'reverse', 'FontSize', fsiz, ...
    'XTick', []);
ylabel('Spectrum /10^{15} rads^{-1}', 'FontSize', fsiz);

%Plot the time axis below the Wigner
normt = timesig(:,2).*conj(timesig(:,2));
normt = normt./max(normt);
ax3 = axes('Units', 'normalized','Position', [0.33 0.12 0.65 0.2]);
plot(timesig(:,1)*1e15, normt);
set(gca, 'FontSize', fsiz, 'YDir', 'normal', 'YTick', [], 'XLim', tlim);
xlabel('Delay /fs', 'FontSize', fsiz);
ax3 = gca;
linkaxes([ax1 ax2], 'y');
linkaxes([ax1 ax3], 'x');

%Annotate the graph
if (exist('ne', 'var'))
    nef = ['n_e = ' ne(4:end)];
    a0f = ['a_0 = ' a0(4:end)];
    pulselen = widthfwhm(normt')*(timesig(2,1) - timesig(1,1))*1e15;
    plen = sprintf('t_{FWHM} = %2.1f fs', pulselen);
    annotation('textbox', [.1 .1 .1 .1], ...
        'FontSize', fsiz, ...
        'String', {nef, a0f, plen});   
end

him = 66;