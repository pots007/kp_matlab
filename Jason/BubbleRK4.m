function [T,state,gammab] = BubbleRK4(N)

me = 9.11e-31;
c = 3e8;
epsilon0 = 8.85e-12;
ne = 5e24;
nc = 1750e24;
gammab = nc/(sqrt(3)*ne);
q = 1.6e-19;
omegap = sqrt(ne*q^2/(me*epsilon0));
lambdap = 2*pi*c/omegap;
rb = lambdap;

x = -0.85*gammab*rb*ones(N,1);
disp(x)
x = x + gammab*rb*linspace(-0.05, 0.05, N)';
disp(x)
y = 0.01*linspace(-rb,rb,N)';
z = zeros(size(x));

px = -me*c*sqrt(gammab^2 - 1)*ones(N,1);
py = zeros(size(x));
pz = zeros(size(x));

state0 = [x; y; z; px; py; pz];

params{1} = [gammab omegap me c rb];
options = odeset('MaxStep',1e-15);
[T,state] = ode45(@BubbleODE,[0 10e-12],state0,options,params);

%Plotting

bubblex = -gammab*rb:2*gammab*rb/100:gammab*rb;
bubbley = sqrt(rb^2 - bubblex.^2/gammab^2);
bubblex = [bubblex bubblex];
bubbley = [bubbley -bubbley];
fprintf('Total of %i timesteps\n', length(T));

for n = 1:100:5000%length(T)
   
    x = state(n,1:N);
    y = state(n,N+1:2*N);
    px = state(3*N+1:4*N);
    py = state(4*N+1:5*N);
    pz = state(5*N+1:6*N);
    gammap = sqrt(1 + (px.^2 + py.^2 + pz.^2)/(me*c)^2);
    scatter(x/1e-6, y/1e-6, 100*ones(size(y)), gammap, '.')
    xlabel('Propagation Direction /\mum')
    ylabel('Transverse Direction /\mum')
    xlim(gca, [-1.2*gammab*rb/1e-6 1.2*gammab*rb/1e-6])
    ylim(gca, [-1.2*rb/1e-6 1.2*rb/1e-6])
    hold on
    plot(bubblex/1e-6,bubbley/1e-6) 
    hold off
    %fs14
    drawnow
    
end

end

function dstatedt = BubbleODE(t, state, params)

N = length(state)/6;

gammab = params{1}(1);
omegap = params{1}(2);
me     = params{1}(3);
c      = params{1}(4);
rb     = params{1}(5);

x  = state(1:N);
y  = state(N+1:2*N);
z  = state(2*N+1:3*N);
px = state(3*N+1:4*N);
py = state(4*N+1:5*N);
pz = state(5*N+1:6*N);

gammap = sqrt(1 + (px.^2 + py.^2 + pz.^2)/(me*c)^2);
bx = px./(gammap*me*c);
by = py./(gammap*me*c);
bz = pz./(gammap*me*c);
   
r2 = (x/gammab).^2 + y.^2 + z.^2;

Fx = -(omegap^2*me/(2*gammab))*(log(gammab^2)*x - gammab^2*(by.*y + bz.*z));
Fy = -0.5*omegap^2*me*gammab*y.*(1+bx);
Fz = -0.5*omegap^2*me*gammab*z.*(1+bx);

Fx(r2 > rb^2) = 0;
Fy(r2 > rb^2) = 0;
Fz(r2 > rb^2) = 0;

dpxdt = Fx;
dpydt = Fy;
dpzdt = Fz;
   
dxdt = px./(gammap*me);
dydt = py./(gammap*me);
dzdt = pz./(gammap*me);

dstatedt = [dxdt; dydt; dzdt; dpxdt; dpydt; dpzdt];

end
