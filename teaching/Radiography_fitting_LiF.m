if ispc
    datpath = 'F:\Kristjan\Documents\Uni\Dropbox\teaching\2014\';
elseif ismac
    datpath = '/Users/kristjanpoder/Dropbox/teaching/2014/';
end
fine1raw = importdata([datpath 'LiF_Fine1.dat']);
fine1 = fine1raw.data;
figure(191); clf;
h1 = plot(fine1(:,1), fine1(:,2), 'xr', fine1(:,1), fine1(:,3), 'xk');
%legend(h1, fine1raw.colheaders(2:3), 'Location', 'Northwest');
xlabel(' 2\theta (degrees)');
ylabel('Signal (Counts)');
% Try to fit gaussians to peaks to see how well that works
x1 = fine1(1:25,1);
y1 = fine1(1:25,2); y1(isnan(y1)) = 10;
[fit1, gof1, ~] = fit(x1, y1, 'gauss1');
gauss = @(x,a,b,c) a.*exp(-((x-b)/c).^2);
hold on;
hf1 = plot(x1, gauss(x1, fit1.a1, fit1.b1, fit1.c1), '-b');
y2 = fine1(1:25,3);
[fit2, gof2, ~] = fit(x1, y2, 'gauss1');
hf2 = plot(x1, gauss(x1, fit2.a1, fit2.b1, fit2.c1), '-g');
x2 = fine1(26:end,1);
y3 = fine1(26:end,2);
[fit3, gof3, ~] = fit(x2, y3, 'gauss1');
hf3 = plot(linspace(x2(1),x2(end),100), gauss(linspace(x2(1),x2(end),100), fit3.a1, fit3.b1, fit3.c1), '-r');
y4 = fine1(26:end,3);
[fit4, gof4, ~] = fit(x2, y4, 'gauss1');
hf4 = plot(linspace(x2(1),x2(end),100), gauss(linspace(x2(1),x2(end),100), fit4.a1, fit4.b1, fit4.c1), '-k');
legend([h1(1), hf1, hf2, h1(2), hf3, hf4],...
    {fine1raw.colheaders{2},...
    sprintf('%s, \\mu=%1.2f,\\sigma=%1.2f, r^2=%1.3f',fine1raw.colheaders{2}, fit1.b1, fit1.c1, gof1.adjrsquare),...
    sprintf('%s, \\mu=%1.2f,\\sigma=%1.2f, r^2=%1.3f',fine1raw.colheaders{3}, fit2.b1, fit2.c1, gof2.adjrsquare),...
    fine1raw.colheaders{3},...
    sprintf('%s, \\mu=%1.2f,\\sigma=%1.2f, r^2=%1.3f',fine1raw.colheaders{2}, fit3.b1, fit3.c1, gof3.adjrsquare),...
    sprintf('%s, \\mu=%1.2f,\\sigma=%1.2f, r^2=%1.3f',fine1raw.colheaders{3}, fit4.b1, fit4.c1, gof4.adjrsquare)},...
    'Location', 'Northwest');
set(h1, 'MarkerSize', 10);
set([h1(1),h1(2),hf1,hf2,hf3,hf4], 'LineWidth', 1.5);
setAllFonts(191, 16);
export_fig([datpath 'LiF_Fine1'], '-pdf', '-transparent', '-nocrop', 191);