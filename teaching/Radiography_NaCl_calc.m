lambdaa = 154; %In pm
err = 1;
thet_alpha = [29.1 59.7 95.8];
thet_beta = [32.3 66.7 110.6];
dthet_alpha = 0.5*pi/180; % Error in radians
m = [1 2 3];
twod = m.*lambdaa./sind(0.5*thet_alpha);
dtwod = twod./tand(0.5*thet_alpha)*dthet_alpha;
twodav = sum(twod./dtwod.^2)/sum(1./dtwod.^2);
dtwodav = 1/sum(1./dtwod.^2);
fprintf('\n%20s\t%10s\t%10s\t%10s\t%10s\n', ' ', '1st set', '2nd set', '3rd set', 'Total');
fprintf('%20s\t%10.1f\t%10.1f\t%10.1f\t\n','Theta_alpha', thet_alpha);
fprintf('%20s\t%10.0f\t%10.0f\t%10.0f\t%10.0f\n','a = 2d', twod, twodav);
fprintf('%20s\t%10.0f\t%10.0f\t%10.0f\t%10.0f\n','error', dtwod, dtwodav);
% Now find the other wavelength
lambdab = twod.*sind(0.5*thet_beta)./m;
lambdabav = twodav.*sind(0.5*thet_beta)./m;
dlambdabav = lambdab.*sqrt((dtwodav./twodav).^2+(dthet_alpha./tand(0.5*thet_beta)).^2);
lambdabav = sum(lambdabav./dlambdabav.^2)/sum(1./dlambdabav.^2);
dlambdabav = 1/sum(1./dlambdabav.^2);
fprintf('%20s\t%10.0f\t%10.0f\t%10.0f\t%10.0f\n','lambda_beta', lambdab, lambdabav);
dlambdab = lambdab.*sqrt((dtwod./twod).^2+(dthet_alpha./tand(0.5*thet_beta)).^2);
fprintf('%20s\t%10.0f\t%10.0f\t%10.0f\t%10.0f\n','dlambda_beta', dlambdab, dlambdabav);
twodav = sum(twod./dtwod.^2)/sum(1./dtwod.^2);
dtwodav = 1/sum(1./dtwod.^2);
%fprintf('\n Consolidated measurement a = 2d = (%4.0f +- %2.0f) pm\n', twodav, dtwodav);

