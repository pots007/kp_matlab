%% Change material here
material = 'NaCl';
if strcmp(material, 'LiF')
    m = 1.11;
    dm = 0.01;
    a = 19;
    da = 0.5;
    b = 7;
    c = 3.5;
elseif strcmp(material, 'NaCl')
    m = 1.01;
    dm = 0.01;
    a = 19;
    da = 0.5;
    b = 7;
    c = 4;    
end
rho = m/(a*b*c)*1e3;
drho = rho*sqrt((dm/m)^2+(da/a)^2+(da/b)^2+(da/c)^2);
fprintf('%s rho = (%1.1f +- %1.1f) g/cm3\n',material, rho, drho);
Constants;
kalpha = 8037;
dkalpha = 2;
kbeta = 8904;
lalpha = h*c/(kalpha*qe);
dlalpha = lalpha*(dkalpha/kalpha);
fprintf('lambda_alpha = (%1.2f +- %1.2f) pm\n', 1e12*lalpha, 1e12*dlalpha);
lbeta = h*c/(kbeta*qe);
dlbeta = lalpha*(dkalpha/kbeta);
fprintf('lambda_alpha = (%1.2f +- %1.2f) pm\n', 1e12*lbeta, 1e12*dlbeta);

%% Now for exercise (a)

lambdaa = 154; %In pm
if strcmp(material, 'LiF')
    thet_alpha = (42*6+1)/6;
    thet_beta = (48*6+5)/6;
    dthet_alpha = 1/6*pi/180; % Error in radians
elseif strcmp(material, 'NaCl')
    thet_alpha = (29*6+0.5)/6;
    thet_beta = (32*6+2)/6;
    m = [1 2 3];
    dthet_alpha = 1/6*pi/180; % Error in radians
end
twod = lambdaa/sind(0.5*thet_alpha);
dtwod = twod/tand(0.5*thet_alpha)*dthet_alpha;
fprintf('Lattice constant for %s a = 2d = (%1.0f +- %1.0f) pm\n',material, twod, dtwod);
% Now find the other wavelength

lambdab = twod*sind(0.5*thet_beta);
dlambdab = lambdab*sqrt((dtwod/twod)^2+(dthet_alpha/tand(0.5*thet_beta))^2);
fprintf('The calculated wavelength for %s for kbeta is (%1.0f +- %1.0f) pm\n',material, lambdab, dlambdab);
