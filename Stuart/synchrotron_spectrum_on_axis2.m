function d2N_dEdOmega = synchrotron_spectrum_on_axis2(E, Ec)
%evaluates the synchrotron spectrum on axis using critical energy defined
%as per Jackson 3rd edition 
%
%d2N_dEdOmega = synchrotron_spectrum_on_axis(E, Ec) returns the photon distribution on axis for a given array of energies, E and
%given critical energy Ec
xi = E./(2*Ec);
d2N_dEdOmega = E.*besselk(2/3, xi).^2;


end