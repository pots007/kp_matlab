function [x, image_data] = diffraction_image(geometry, object, detector, source)
%returns the 1D image of object, taking into account source size, spectrum
%and detector sensititivity
%inputs are
%  geometry.Zso  source to object distance
%  geometry.Zod  object to detector distance
%
%  object.xi  % coordinates of object plane
%  object.beta  
%  object.delta  % real and imaginganry compenents of refractive index of
%  object (currently one material only)
%  object.thickness %thickness of object at each xi
%
%  detector.N_pixels  number of pixels in detector  shoud be an even number
%  detector.pixel    size of one pixel in m
%  detector.k
%  detector.T     tranmission of detector as a function of photon energy k
%
%  source.width  1/e^2 radius of souce (gaussian)
%  source.k
%  source.S  %spectrum (number of photons per energy bin, dN/dk)

Z_so = geometry.Zso;
Z_od = geometry.Zod;

xi = object.xi;
beta =  object.beta ; 
delta = object.delta;
thickness = object.thickness;

N_d = detector.N_pixels ; 
pixel =  detector.pixel  ;  
kd =  detector.k;
T =  detector.T;     

source_width =  source.width ;
ks =  source.k;
S =  source.S; 

M =  (Z_so + Z_od)/ Z_so; %magnification factor
h = 4.135667516e-15;  %Planck constant in eV.s
c = 2.998e8;  %c in ms-1
x = ([1:N_d] - N_d/2)*pixel;  

%create PSF
PSF = exp(- 2*x.^2/ (M*source_width).^2) ;
int_val = sum(PSF(:));
PSF = PSF./int_val;

%I am assuming the user will make ks and kd the same !  this will produce
%an error if not
if length(ks) ~= length(kd)
    disp('please make source.k and detector.k the same before calling this fuinction')
end
if length(ks) == length(kd)
    if sum(ks == kd) < length(ks)
        disp('please make source.k and detector.k the same before calling this fuinction')
    end
end
k = ks;
Nk = length(ks);
spectral_factor = S.*T.*k;
spectral_factor = (spectral_factor./sum(spectral_factor))';

image_data = zeros(size(x));
N_o = length(xi);
dxi = xi(2) - xi(1);

for this_k = 1:Nk;
    %disp(strcat('calculating image for spectral component', num2str(this_k), ' of ', num2str(Nk)));
    lambda =  h*c/(k(this_k)*1000);  % in metres
    for this_x = 1:N_d  %loop over detector
        this_x
        integral = 0;
        for this_xi = 1:N_o
            phi = -2*pi/lambda .* (delta - 1i*beta) * thickness(this_xi); 

            term1 = exp( pi*1i/lambda * ( (xi(this_xi))^2/Z_so + (( xi(this_xi) - x(this_x))^2/Z_od) ) );
            term2 = exp( 1i * phi) - 1 ;
            integral = integral +  dxi*term1 * term2 ;
         end
        A(this_x) = sqrt( (Z_so + Z_od)/(1i * lambda*Z_so*Z_od) ) * exp( - pi*1i/lambda * (x(this_x))^2/(Z_so+Z_od) ) * integral;
        E(this_x) = 1 + A(this_x);
    end
    I = abs(E).^2;
    I_conv = conv(I, PSF, 'same');
    %disp(strcat('this image amplitude factor is ', num2str(spectral_factor(this_k)) ))
    image_data = image_data + I_conv*spectral_factor(this_k);
end
    
end

