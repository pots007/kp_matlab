/*==========================================================
 * The worker function for diffraction_image
 * Implements the integration loop for one particular wavelength
 * Returns image_data as an array.
 *
 * Uses Euler's formula to do exp functions.
 *
 *========================================================*/

#include "mex.h"
#include <math.h>
#include <complex.h>

void diffraction_image_worker(double *outMatrix, double *thickness, double *xi,
        double *x, double beta, double delta, double Zso, double Zod, double lambda,
        mwSize Nx,mwSize Nxi){
    mwSize ixi, ix;
    double complex phi1, phi2, term1, term2, phi3;
    double complex integral;
    double complex A[Nx];
    double complex b = -((Zso+Zod) / (lambda*Zso*Zod)) * I; // b is actually -ve.
    b = csqrt(b);
    //Do the actual integral
    double dxi = xi[1]-xi[0];
    for (ix=Nx; ix--; ){
    //for (ix=0; ix<Nx; ix++){
        integral = 0. + 0. * I;
        for (ixi=Nxi; ixi--; ){
        //for (ixi=0; ixi<Nxi; ixi++){
            phi2 = (M_PI/lambda*(xi[ixi]*xi[ixi]/Zso + (xi[ixi]-x[ix])*(xi[ixi]-x[ix])/Zod)) * I;
            term2 = cexp(phi2);
            phi1 = -2*M_PI*thickness[ixi]/lambda*(delta - beta * I);
            term1 = cexp( phi1 * I) - 1;
            integral = integral + dxi*term1*term2;
        }
        phi3 = - M_PI*x[ix]*x[ix] / (lambda*(Zso+Zod));
        A[ix] = b * cexp(phi3 * I) * integral + 1;
        outMatrix[ix] = cabs(A[ix])*cabs(A[ix]);
    }
    
//     double exp2, t1r, t1i, t2r, t2i, dxi, phi1, phi2, phi3;
//     double intr, inti;
//     double Ai[Nx];
//     double Ar[Nx];
//     double b = (Zso+Zod) / (2*lambda*Zso*Zod); // b is actually -ve.
//     double Cr = sqrt(0.5*b);
//     double Ci = -sqrt(0.5*b); //using the sign of b here.
//     double Cl = sqrt(Ci*Ci+Cr*Cr);
//     double Cphi = atan2(Ci, Cr);
//     double ei, er;
//     //Do the actual integral
//     dxi = xi[1]-xi[0];
//     for (ix=0; ix<Nx; ix++){
//         intr = 0;
//         inti = 0;
//         for (ixi=0; ixi<Nxi; ixi++){
//             phi1 = M_PI/lambda*(xi[ixi]*xi[ixi]/Zso + (xi[ixi]-x[ix])*(xi[ixi]-x[ix])/Zod);
//             t1r = cos(phi1);
//             t2r = sin(phi1);
//             exp2 = exp(-2*M_PI*thickness[ixi]*beta/lambda);
//             phi2 = -2*M_PI*thickness[ixi]*delta/lambda;
//             t2r = exp2*cos(phi2)-1;
//             t2i = exp2*sin(phi2);
//             intr = intr + dxi*(t1r*t2r - t1i*t2i);
//             inti = inti + dxi*(t1r*t2i + t1i*t2r);
//         }
//         phi2 = atan2(inti, intr);
//         exp2 = sqrt(intr*intr+inti*inti);
//         phi3 = -M_PI*x[ix]*x[ix] / (lambda*(Zso+Zod));
//         er = cos(phi3);
//         ei = sin(phi3);
//         Ar[ix] = Cr*er*intr - intr*Ci*ei - Cr*ei*inti - Ci*er*inti + 1;
//         Ai[ix] = Cr*ei*intr + Ci*er*intr + Cr*er*inti - Ci*ei*inti;
//         Ar[ix] = exp2*Cl*cos(Cphi+phi2+phi3) + 1;
//         Ai[ix] = exp2*Cl*sin(Cphi+phi2+phi3);
//         outMatrix[ix] = Ar[ix]*Ar[ix] + Ai[ix]*Ai[ix];
//     }
}

/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double beta, delta, Zso, Zod, lambda; //Input values
    double *thickness;
    double *xi;
    double *x;
    size_t Nx, Nxi;
    double *outMatrix;              /* output matrix */

    /* check for proper number of arguments */
    if(nrhs!=8) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:nrhs","Eight inputs required.");
    }
    if(nlhs!=1) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:nlhs","One output required.");
    }
   
    // make sure the first three input argument is type double
    if( !mxIsDouble(prhs[0]) || mxIsComplex(prhs[0])) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:notDouble","thickness matrix must be type double.");
    }
    if( !mxIsDouble(prhs[1]) || mxIsComplex(prhs[1])) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:notDouble","xi matrix must be type double.");
    }
    if( !mxIsDouble(prhs[2]) || mxIsComplex(prhs[2])) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:notDouble","x matrix must be type double.");
    }
    // check these are row vectors as well.
    if(mxGetM(prhs[0])!=1) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:notRowVector","thickness must be a row vector.");
    }
    if(mxGetM(prhs[1])!=1) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:notRowVector","xi must be a row vector.");
    }
    if(mxGetM(prhs[2])!=1) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:notRowVector","x must be a row vector.");
    }
    
    // make sure the other arguments are scalar 
    if( !mxIsDouble(prhs[3]) || mxIsComplex(prhs[3]) || mxGetNumberOfElements(prhs[3])!=1 ) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:notScalar","beta must be a scalar.");
    }
    if( !mxIsDouble(prhs[4]) || mxIsComplex(prhs[4]) || mxGetNumberOfElements(prhs[4])!=1 ) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:notScalar","delta must be a scalar.");
    }
    if( !mxIsDouble(prhs[5]) || mxIsComplex(prhs[5]) || mxGetNumberOfElements(prhs[5])!=1 ) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:notScalar","Zso must be a scalar.");
    }
    if( !mxIsDouble(prhs[6]) || mxIsComplex(prhs[6]) || mxGetNumberOfElements(prhs[6])!=1 ) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:notScalar","Zod must be a scalar.");
    }
    if( !mxIsDouble(prhs[7]) || mxIsComplex(prhs[7]) || mxGetNumberOfElements(prhs[7])!=1 ) {
        mexErrMsgIdAndTxt("MyToolbox:diffimage:notScalar","lambda must be a scalar.");
    }
    
    //create pointers to the data vectors
    thickness = mxGetPr(prhs[0]);
    xi = mxGetPr(prhs[1]);
    x = mxGetPr(prhs[2]);
    
    // get the values of the scalar inputs  
    beta = mxGetScalar(prhs[3]);
    delta = mxGetScalar(prhs[4]);
    Zso = mxGetScalar(prhs[5]);
    Zod = mxGetScalar(prhs[6]);
    lambda = mxGetScalar(prhs[7]);

    /* create a pointer to the real data in the input matrix  */
    //inMatrix = mxGetPr(prhs[1]);

    /* get dimensions of the input matrix */
    Nx = mxGetN(prhs[2]);
    Nxi = mxGetN(prhs[1]);

    /* create the output matrix */
    plhs[0] = mxCreateDoubleMatrix(1,(mwSize)Nx,mxREAL);

    /* get a pointer to the real data in the output matrix */
    outMatrix = mxGetPr(plhs[0]);

    /* call the computational routine */
    //arrayProduct(multiplier,inMatrix,outMatrix,(mwSize)ncols);
    diffraction_image_worker(outMatrix,thickness,xi,x,beta,delta,Zso,Zod,lambda,(mwSize)Nx,(mwSize)Nxi);
}
