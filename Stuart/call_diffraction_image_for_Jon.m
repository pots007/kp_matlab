tic
clear all

tt(1) = toc;
%set up geometry
geometry.Zso = 0.1;
geometry.Zod  = 3.4;

M =  (geometry.Zso + geometry.Zod)/ geometry.Zso; %magnification factor

%set up source
k = linspace(30,30, 1);


Ec = 30; %critical energy in keV
S = synchrotron_spectrum_on_axis2(k, Ec);
source.width  = 1e-6;
source.k = k; 
source.S  = S; 

%set up detector
detector.N_pixels  = 256;  
detector.pixel   = 13.5e-6; 
load('Princeton_CsI');
T = interp1(Princeton_CsI(:,1), Princeton_CsI(:, 2)/100, k);
detector.k = k;
detector.T    = T;
max_x = 0.5 * detector.N_pixels * detector.pixel;

%%
% set up object
%type = 'wire';
type = 'half_plane_angle'
%type = 'wire'
smooth_step= 1;
%material = 'plastic';
material = 'GaAs';

if strcmp(type, 'wire') width_factor = 2; end
if strcmp(type, 'half_plane') width_factor = 5; end
if strcmp(type, 'half_plane_angle') width_factor = 5; end
if strcmp(type, 'two_thickness_plane') width_factor = 20; end
if strcmp(type, 'bar') width_factor = 2; end    

dxi = 0.002e-6;
xi_max = width_factor*max_x/M;
xi = [-xi_max:dxi:xi_max];
N_o = length(xi);
fraction = 0.1;
N_points = round(fraction*N_o);
thickness = zeros(size(xi));

if strcmp(type, 'two_thickness_plane')
    R1 = 40e-6;
    R2 = 40.4e-6;
    region1 = find(xi < 0);
    region2 = find(xi >= 0);
    thickness(region1) = R1;
    thickness(region2) = R2;
    %thicness(abs(xi) > 50e-6) = 0;
    if smooth_step
        step_width = 1e-6;
        a = exp(-2*xi.^2./step_width^2);
        a = a./sum(a(:));
        thickness = conv(thickness, a, 'same');
    end
end
    
if strcmp(type, 'wire')
%    cylindrical wire
    R = 10e-6;
    wire = find(abs(xi) < R);
    thickness(wire) = R * sqrt(1 - (xi(wire)/R).^2);
end

if strcmp(type, 'bar')
%rectangular cross section wire
    width = 50e-6;
    wire = find(abs(xi) < width/2);
    thickness(wire) = 20e-6;
end
if strcmp(type, 'half_plane')
    % half plane
    R = 1000e-6;
    half_plane = find(xi < 0);
    thickness(half_plane) = R;
end
if strcmp(type, 'half_plane_angle')
    R = 1000e-6;  
    theta = 1*pi/180;  
    thickness(xi <=0) = R*cos(theta);
    thickness(xi > R*sin(theta) ) = 0;
    edge_region =  find(xi > 0 & xi <= R*sin(theta));
    thickness( edge_region) = R*cos(theta) - cot(theta).* xi(edge_region);
end
if strcmp(material, 'plastic')
    delta = 1.05401E-06;
    beta = 5.45076817E-10;
end
if strcmp(material, 'Fe')
     delta = 5.8339E-05;
     beta = 2.0839E-06;
end
if strcmp(material, 'GaAs')
     delta =1.095e-6;
     beta = 3.415e-8;
end 


object.xi = xi; % coordinates of object 
object.beta  = beta;
object.delta = delta; % real and imaginganry compenents of refractive index of
object.thickness = thickness;
tt(2) = toc
%[x, image_data] = diffraction_image_original(geometry, object, detector, source);
%[x, image_data] = diffraction_image(geometry, object, detector, source);
[x, image_data] = diffraction_image_Kris(geometry, object, detector, source);
%%
figure(1)
plot(x/1e-3, image_data, 'o-k');
xlabel('x (detector) [mm]')
title('intensity')
xlim([-1.5, 1.5])
hold off
%myplotstyle(10, 10, 14)

% %%
figure(2)
plot(xi/1e-6, thickness/1e-6, '-k')
xlabel('\xi (object) [\mum]')
ylabel('thickness [\mum]')
title('thickness')
%xlim([-5, 5])
%ylim([39, 41])
hold off
%myplotstyle(10, 10, 14)
tt(3) = toc;
figure(300)
plot(diff(tt))
toc

%% do error function fit on image data


 