function [x, image_data] = diffraction_image(geometry, object, detector, source)
%returns the 1D image of object, taking into account source size, spectrum
%and detector sensititivity
%inputs are
%  geometry.Zso  source to object distance
%  geometry.Zod  object to detector distance
%
%  object.xi  % coordinates of object plane
%  object.beta  
%  object.delta  % real and imaginganry compenents of refractive index of
%  object (currently one material only)
%  object.thickness %thickness of object at each xi
%
%  detector.N_pixels  number of pixels in detector  shoud be an even number
%  detector.pixel    size of one pixel in m
%  detector.k
%  detector.T     tranmission of detector as a function of photon energy k
%
%  source.width  1/e^2 radius of souce (gaussian)
%  source.k
%  source.S  %spectrum (number of photons per energy bin, dN/dk)
%
%
% %% upadated Nov 2014 to vectorise integral (thanks to Kristjan Poder)

%unpackage the data structures for easier code reading

tic

Z_so = geometry.Zso;
Z_od = geometry.Zod;


xi = object.xi;
beta =  object.beta ; 
delta = object.delta;
thickness = object.thickness;

N_d = detector.N_pixels ; 
pixel =  detector.pixel  ;  
kd =  detector.k;
T =  detector.T;     

source_width =  source.width ;
ks =  source.k;
S = source.S;


Intensity_spectrum =  ks.*S; 
spectral_amplitude = (sqrt( (Intensity_spectrum)./max(Intensity_spectrum) ));

M =  (Z_so + Z_od)/ Z_so; %magnification factor
h = 4.135667516e-15;  %Planck constant in eV.s
c = 2.998e8;  %c in ms-1
x = ([1:N_d] - N_d/2)*pixel;  

%create PSF
PSF = exp(- 2*x.^2/ (M*source_width).^2) ;
int_val = sum(PSF(:));
PSF = PSF./int_val;

%I am assuming the user will make ks and kd the same !  this will produce
%an error if not
if length(ks) ~= length(kd)
    disp('please make source.k and detector.k the same before calling this fuinction')
end
if length(ks) == length(kd)
    if sum(ks == kd) < length(ks)
        disp('please make source.k and detector.k the same before calling this fuinction')
    end
end
k = ks;
Nk = length(ks);


image_data = zeros(size(x));
N_o = length(xi);
dxi = xi(2) - xi(1);

%    disp(strcat('calculating image for spectral component', num2str(this_k), ' of ', num2str(Nk)));
lambda =  h*c./(k*1000);  % in metres

tt(1) = toc;
    
THICKNESS = repmat(thickness', [1, N_d, Nk]);
[X,XI,LAMBDA] = meshgrid(x', xi', lambda');
size(spectral_amplitude)
sa2 = repmat(spectral_amplitude, N_d,1);
size(sa2);
SPECTRAL_AMPLITUDE = reshape(sa2, [1 size(sa2,1) size(sa2,2)]);%repmat(spectral_amplitude', [1,N_d, 1]);
sizexi = size(XI)
sizex = size(X)

tt(2) = toc;

PHI = -2*pi./LAMBDA.*(delta-1i*beta).*THICKNESS;
TERM1 = exp(pi*1i./LAMBDA.*(XI.^2/Z_so + (XI - X.^2/Z_od)));
TERM2 = exp(1i*PHI)-1;
INTEG = sum(TERM1.*TERM2.*dxi,1);
A = sqrt( (Z_so+Z_od)./(1i*LAMBDA(1,:,:)*Z_so*Z_od)) .* exp(-pi*1i./LAMBDA(1,:,:).*X(1,:,:).^2/(Z_od+Z_so)).*INTEG;

tt(3) = toc;

%A = squeeze(A);    
size(A)
size(SPECTRAL_AMPLITUDE)

E = SPECTRAL_AMPLITUDE .* (1+A);
I = E.*conj(E);
%I = sum(I,1);


size_PSF = size(PSF)
size_E = size(I)
image_data = zeros(size(PSF));
for i=1:size(E,3)
    image_data = image_data + conv(I(:,:,i), PSF, 'same');
end
tt(4) = toc;

%figure(200)
%plot( diff(tt) )

 
end

