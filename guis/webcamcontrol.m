% Kristjan Poder, IC, 2013

function hobj = webcamcontrol()

hObject = figure('units','pixels',...
    'position',[150 150 800 600],... %[left bottom width height]
    'menubar','none',...
    'numbertitle','off',...
    'name','Webcam control',...
    'Tag', 'webcams',...
    'resize','on');

imaqreset;
cams = imaqhwinfo('winvideo');
caminfos = cams.DeviceInfo;
nocams = length(caminfos);
camnames = cell(nocams+1,1);
camnames{1} = 'Camera';
for i=1:nocams
    camnames{i+1} = cams.DeviceInfo(i).DeviceName;
end
%camnames = cams.DeviceInfo.DeviceName;
%camnames = {'Camera' camnames};
S.cameras = cams;

S.startbutton = uicontrol('style','pushbutton',...
    'units', 'normalized', ...
    'position',[0.4 0.02 0.2 0.05],...
    'string','START',...
    'callback',{@startstop_call});

posix = [0.1 0.1 0.55 0.55];
posiy = [0.1 0.55 0.1 0.55];
for i=1:4
    cambox = ['cambox' num2str(i)];
    S.(cambox) = uicontrol('style', 'popup', ...
        'units', 'normalized', ...
        'String', camnames, ...
        'Position', [posix(i) posiy(i)+0.3925 0.19 0.05]);
    set(S.(cambox), 'Callback', {@populateResolution, S.(cambox)});    
    resolution = ['resolution' num2str(i)];
    S.(resolution) = uicontrol('style', 'popup', ...
        'units', 'normalized', ...
        'String', 'Resolution', ...
        'Position', [posix(i)+0.21 posiy(i)+0.3925 0.19 0.05]);
    camaxes = ['axes' num2str(i)];
    S.(camaxes) = axes('Position', ...
        [posix(i) posiy(i) 0.4 0.4], ...
        'XTick', [], 'YTick', []);
end

S.run = 0;
guidata(hObject,S);
hobj = hObject;


function [] = populateResolution(hObject, ~, hcambox)
S = guidata(hObject);
assignin('base', 'S', S);
cams = S.cameras;
camlist = get(hcambox, 'String');
camID = get(hcambox, 'Value');
camname = camlist{camID};
Sfields = fieldnames(S);
i = 2;
while (S.(Sfields{i}) ~= hcambox)
    i = i + 1;
end
hcamboxname = Sfields{i};
if (~strcmp(camname, 'Camera'))
    resolutions = cams.DeviceInfo(camID-1).SupportedFormats;
    hreso = ['resolution' hcamboxname(end:end)];
    set(S.(hreso), 'String', resolutions);
end


function [] = startstop_call(hObject, ~)
S = guidata(hObject);
curr = get(S.startbutton, 'String');
if (strcmp(curr, 'START') == 1)
    set(S.startbutton, 'String', 'STOP');
    set(S.startbutton, 'BackgroundColor', [0. 1. 0.]);
    S.run = 1;
    guidata(hObject, S);
    runcameras(hObject);
end
if (strcmp(curr, 'STOP') == 1)
    set(S.startbutton, 'String', 'START');
    set(S.startbutton, 'BackgroundColor', [0.941 0.941 0.941]);
    S.run = 0;
    guidata(hObject, S);
end


function runcameras(hObject)
imaqreset
S = guidata(hObject);
nocams = 0;
camEnabled = zeros(4,1);
for i=1:4
    camID = get(S.(['cambox' num2str(i)]), 'Value') - 1;
    if (camID == 0)
        continue;
    end
    camEnabled(i) = 1;
    nocams = nocams + 1;
    vid = ['vid' num2str(i)];
    reslist = get(S.(['resolution' num2str(i)]), 'String');
    resolution = reslist{get(S.(['resolution' num2str(i)]), 'Value')};
    S.(vid) = videoinput('winvideo', camID, resolution);
    set(S.(vid), 'TriggerRepeat',Inf, ...
        'ReturnedColorSpace','rgb', ...
        'FramesPerTrigger', 500);
    S.(vid).FrameGrabInterval = 1;
%     resx = strfind(resolution, '_');
%     resy = strfind(resolution, 'x');
%     x = resolution(resx+1:resy-1);
%     y = resolution(resy+1:end);
%     frames.(['f' num2str(1)]) = zeros(str2num(y), str2num(x),3);
    start(S.(vid));
end
guidata(hObject, S);
while (S.run)
    S = guidata(hObject);
    for i=1:4
        if (~camEnabled(i))
            continue;
        end
        data = getdata(S.(['vid' num2str(i)]),1);
        axes(S.(['axes' num2str(i)]));
        imshow(data);
        flushdata(S.(['vid' num2str(i)]));
    end
    pause(0.2)
end

for i=1:nocams
    if(~camEnabled(i))
        continue;
    end
    stop(S.(['vid' num2str(i)]));
    delete(S.(['vid' num2str(i)]));
end