classdef ListedHandle < handle
    methods
        function o=ListedHandle
            global ListedHandle_list;
            if isempty(ListedHandle_list)
                ListedHandle_list={o};
            else
                ListedHandle_list{end+1}=o;
            end
        end
        function delete(o)
            global ListedHandle_list;
            inds=find(cellfun(@(x) x==o,ListedHandle_list));
            if isempty(inds)
                warning('couldn''t find it');
            elseif length(inds)>1
                warning('more than one');
            else
                ListedHandle_list=[ListedHandle_list(1:inds-1),ListedHandle_list(inds+1:end)];
            end
        end
    end
    methods (Static)
        function r=list
            global ListedHandle_list;
            r=ListedHandle_list;
        end
    end
end
                
            