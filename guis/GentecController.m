% COM port based controller for Gentec INTEGRA calorimeter

% Enables the user to set trigger level, energy scale and autosave

% K Poder, IC 2015

classdef GentecController < uiextrasX.VBox
    
    properties
        serial_obj
        serial_port
        serial_open_close
        autosave
        energy
    end
    
    methods
        function o = GentecController(varargin)
            Parent = figure(101001);
            set(Parent, 'Resize', 'off', 'Toolbar', 'none', 'MenuBar', 'none', 'Color', 'w',...
                'Position', [100 300 600 225], 'Name', 'Gentec controller', 'NumberTitle', 'off');%, 'CloseRequestFcn',@(s,e) disp('don''t close me!'));
            o@uiextrasX.VBox('Parent', Parent);
            defer(o);
            hbox = uiextrasX.HBox('Parent', o);
            uicontrol('Style', 'text', 'String', 'Serial port:', 'Parent', hbox);
            o.serial_port = uicontrol('Style', 'edit', 'String', 'COM7', 'Parent', hbox);
            o.serial_open_close = uicontrol('Style', 'togglebutton', 'String', 'OPEN', 'Parent', hbox,...
                'Callback', @(obj,val)connect(o, get(o.serial_open_close, 'Value')));
            hbox = uiextrasX.HBox('Parent', o);
            % Autosave bits and bobs
            hpan = uiextrasX.Panel('Parent',hbox,'Title','Autosave','Padding',2);
            vbox = uiextrasX.VBox('Parent', hpan);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'pushbutton', 'String', 'Path:', 'Parent', hbox, 'Callback', @(s,e)set_path(o,1));
            o.autosave.path = uicontrol('style', 'edit', 'String', 'C:/', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'Filename:', 'Parent', hbox);
            o.autosave.filename = uicontrol('Style', 'edit', 'String', 'shot', 'Parent', hbox);
            o.autosave.enable = uicontrol('Style', 'checkbox', 'String', 'ENABLE', 'Parent', hbox);
            hbox.Sizes = [50 -1 50 100 75];
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'offset', 'Parent', hbox);
            o.autosave.incr = javaNumSpinner2(1,1,Inf,1,hbox);
            o.autosave.date = uicontrol('Style', 'checkbox', 'String', 'Date in filename', 'Parent', hbox);
            o.autosave.time = uicontrol('Style', 'checkbox', 'String', 'Time in filename', 'Parent', hbox);
            % Control box
            hbox = uiextrasX.HBox('Parent', o);
            hpan = uiextrasX.Panel('Parent',hbox,'Title','Control','Padding',2);
            hbox = uiextrasX.HBox('Parent', hpan);
            uicontrol('Style', 'text', 'String', 'Trigger level', 'Parent', hbox);
            o.energy.trigger = javaNumSpinner2(2,0.01,50,0.01,hbox,...
                'StateChangedCallback', @(s,e)set_triggerlevel(o,get(o.energy.trigger, 'Value')));
            uicontrol('Style', 'text', 'String', 'Energy range:', 'Parent', hbox);
            o.energy.range = uicontrol('Style', 'popupmenu', 'String',{'300 uJ','1 mJ','3 mJ', '10 mJ', '30 mJ', '100 mJ', '300 mJ', '1 J', '3 J', '10 J'},...
                'Parent', hbox, 'Callback', @(s,e)set_energyrange(o,get(o.energy.range, 'Value')));
            o.energy.runb = ContinuousButton(hbox);
            addlistener(o.energy.runb, 'trigger', @(s,e)get_energy(o,1));
            hbox.Sizes = [100 100 100 -1 150];
            hbox = uiextrasX.HBox('Parent', o);
            o.energy.energy = uicontrol('Style', 'text', 'String', '0 J', 'Parent', hbox, 'FontSize', 50);
            o.energy.value = 0;
            o.Sizes = [25 60 40 -1];
            resume(o);
            load_(o,1);
        end
        
        function load_(o,v)
            if v %load data
                filename = [getuserdir '/Gentec_state.txt'];
                if ~exist(filename, 'file')
                    return;
                end
                dd = importdata(filename);
                set(o.autosave.path, 'String', dd{1});
                set(o.autosave.filename, 'String', dd{2});
                set(o.serial_port, 'String', dd{3});
            else %save data
                path = getuserdir;
                fid = fopen([path '/Gentec_state.txt'], 'w');
                fprintf(fid, '%s\n', get(o.autosave.path, 'String'));
                fprintf(fid, '%s\n', get(o.autosave.filename, 'String'));
                fprintf(fid, '%s\n', get(o.serial_port, 'String'));
                fclose(fid);
            end
        end
        
        function set_path(o,~)
            dirname = uigetdir(get(o.autosave.path, 'String'), 'Select autosave folder');
            if dirname==0
                return;
            end
            set(o.autosave.path, 'String', dirname);
        end
        
        function get_energy(o,~)
            %if ~isfield(o, 'serial_obj'); return; end;
            if ~strcmpi(get(o.serial_obj, 'Status'), 'Open'); return; end;
            % Query whether we have a new value available or not - this
            % didn't seem to work as a command though
            fprintf(o.serial_obj, ['*NVU' char(13)]);
            pause(0.05);
            ret = fread(o.serial_obj, o.serial_obj.BytesAvailable, 'char')';
            disp(char(ret));
            %if ~isempty(strfind(ret, 'not')); return; end;
            % Read the new value
            fprintf(o.serial_obj, '*CVU');
            pause(0.05);
            ret = fread(o.serial_obj, o.serial_obj.BytesAvailable, 'char');
            val = sscanf(char(ret)', '%f');
            set(o.energy.energy, 'String', sprintf('%5g J', val));
            autosave_(o,val);
        end
        
        function autosave_(o,val)
            time_ext = [];
            if get(o.autosave.date, 'Value') || get(o.autosave.time, 'Value')
                cc = clock;
                if get(o.autosave.date, 'Value')
                    time_ext = sprintf('%i%02i%02i', cc(1),cc(2),cc(3));
                end
                if get(o.autosave.time, 'Value')
                    time_ext = sprintf('%s_%i_%i_%2.2f', time_ext, cc(4),cc(5),cc(6));
                end
            end
            path = get(o.autosave.path, 'String');
            fname = get(o.autosave.filename, 'String');
            offset = num2str(get(o.autosave.incr, 'Value'));
            set(o.autosave.incr, 'Value', get(o.autosave.incr, 'Value')+1);
            filename = [path '/' fname time_ext '_' offset '.txt'];
            fid = fopen(filename, 'W');
            fprintf(fid, '%f\n', val);
            fclose(fid);
            %disp(filename);
        end
        
        function set_energyrange(o,val)
            if ~isfield(o, 'serial_obj'); return; end;
            if ~strcmpi(get(o.serial_obj, 'Status'), 'Open'); return; end;
            fprintf(o.serial_obj, '*SCS%02i', 16+val);
        end
        
        function set_triggerlevel(o,val)
            if ~isfield(o, 'serial_obj'); return; end;
            if ~strcmpi(get(o.serial_obj, 'Status'), 'Open'); return; end;
            % set the trigger level
            fprintf(o.serial_obj, '*STL%04g', val);
        end
        
        function connect(o,val)
            serp = get(o.serial_port, 'String');
            if val
                try
                    o.serial_obj = serial(serp);
                    fopen(o.serial_obj);
                    set(o.serial_open_close, 'String', 'CLOSE');
                    fprintf('Serial port %s is now %s \n', serp, o.serial_obj.status);
                catch
                    fprintf('Opening port %s failed\n', serp);
                    instrreset;
                    fprintf('Running instrreset. Try again.\n');
                    set(o.serial_open_close, 'Value', 0);
                end
            else
                try
                    fprintf('Trying to close serial %s\n', serp);
                    fclose(o.serial_obj);
                    delete(o.serial_obj);
                catch
                    fprintf('Not able to close serial %s\n', serp);
                end
                set(o.serial_open_close, 'String', 'OPEN');
            end
        end
        
        function delete(o)
            if get(o.serial_open_close, 'Value')
                fprintf('Automatically closing COM port!\n')
                connect(o,0);
            end
            load_(o,0);
        end
        
    end
end
