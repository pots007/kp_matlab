function h = editwithlabel(labeltext, edittext, parent, varargin)
hbox = uiextrasX.HBox('Parent', parent);
uicontrol('style', 'text', 'Parent', hbox, 'String', labeltext);
h = uicontrol('style', 'edit', 'Parent', hbox, 'String', edittext);
