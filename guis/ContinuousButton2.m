% (semi) shamelessly borrowed from Tobi's Mercurial store.

% Not sure who wrote this, but am forever indebt to Tobi.

% This is with fixed rate!

classdef ContinuousButton2<ListedHandle
    properties
        button
        running
        timer
        FPS_h
        timestamp_buffer
        timestamp_buffer_ind
        paused=false
    end
    properties (Dependent)
        enabled
    end
    events
        trigger
    end
    methods
        function o=ContinuousButton2(Parent)
            hbox=uiextrasX.HBox('Parent',Parent);
            o.button=uicontrol('Parent',hbox,'Style','checkbox','String','Continuous', 'Callback',@(s,e) set_running(o,get(o.button,'Value')));
            o.FPS_h=uicontrol('Parent',hbox,'Style','text');%setappdata(o.FPS_h,'SmartWidth',80);
            o.timer=timer('TimerFcn', @(s,e)timer_fun(o), 'Period', 0.04,...
                'ExecutionMode', 'fixedRate', 'StartFcn', @(s,e)running_set(o,true),...
                'StopFcn',@(s,e)running_set(o,false));        
        end
        function timer_fun(o)
            if ~o.paused
                notify(o,'trigger');
            end
        end                
        function set_running(o,v)
            if v==o.running
                return;
            end
            if v
                o.timestamp_buffer=nan(10,1);
                o.timestamp_buffer_ind=1;
                start(o.timer);
            else
                stop(o.timer);
            end
        end      
        function running_set(o,v)
            o.running=v;
            set(o.button,'Value',v);
            if v
                set(o.button,'BackgroundColor', [1 0 0], 'String', '   STOP')
            else
                set(o.button,'BackgroundColor', [0 1 0], 'String', 'Continuous')
            end
        end
        function delete(o)
            delete(o.timer);
        end
        function count_frame(o)
             if o.running
                 buf=o.timestamp_buffer(o.timestamp_buffer_ind);
                 if ~isnan(buf)
                     set(o.FPS_h,'String',sprintf('%.2f FPS',length(o.timestamp_buffer)/((now-buf)*24*3600)));
                 end
                 o.timestamp_buffer(o.timestamp_buffer_ind)=now;
                 o.timestamp_buffer_ind=mod1(o.timestamp_buffer_ind+1,length(o.timestamp_buffer));  
             end
        end
        function r=get.enabled(o)
            r=onoff2bool(get(o.button,'Enable'));
        end
        function set.enabled(o,v)
            set(o.button,'Enable',bool2onoff(v));
        end
    end
    methods (Static)
        function set_pause_all(v)
            l=ListedHandle.list;
            for li=1:length(l)
                tl=l{li};
                if isa(tl,'ContinuousButton')
                    tl.paused=v;
                end
            end
        end
    end
end