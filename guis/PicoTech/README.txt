PicoScope 4000 Series MATLAB Generic Instrument Driver Release Notes
====================================================================

Driver Version: 1.1.1 2nd December 2014

License
-------

The driver and supporting files in this zip file are subject to the License terms in the PicoScope 4000 Series Programmer's Guide which can be found in the User Manuals section of our Support pages and in this zip file:

http://www.picotech.com/document/pdf/ps4000pg.en-8.pdf

Installation
------------

For the underlying dlls required please download the PicoScope 4000 Series Software Development Kit from 

http://www.picotech.com/software.html

or contact support@picotech.com stating whether you have a 32 or 64-bit version of MATLAB.

A User Guide is also included with this release.

Please add the location of the dlls to your MATLAB path (use the 'addpath' command if required).  

The mex -setup command may need to be run on your PC in order to load the dll files.

Ensure that the ps4000.dll is named in lower case or rename it.

If PicoScope 6 has not been installed on your PC, please ensure it is for the USB driver.

The root folder of this zip file contains the following folders and files:

Functions 	- helper functions to convert data
PS4000    	- Containing the instrument control driver (picotech_ps4000_generic.mdd), information extracted from the header files, PicoScope 4000 specific constants, a configuration file & scripts.

PicoConstants.m - Constant values used for Pico devices - DO NOT EDIT
PicoStatus.m    - Status code definitions - DO NOT EDIT

The driver was created using MATLAB R2014b (32-bit for Windows) with Instrument Control Box v3.2.

The PS4000Config.m will setup relative paths to the Functions and root directory when run - ensure that this script is called before creating an icdevice object.


64-bit drivers
--------------

64-bit dlls are provided but have not been tested.

Microsoft Windows SDK 7.1 will need to be installed on your PC. Please see the following web page for further information:

http://www.mathworks.com/matlabcentral/answers/101105

Please contact support@picotech.com for addtional files


Supported Devices
-----------------

PicoScope 4226/7
PicoScope 4423/4
PicoScope 4223/4 
PicoScope 4262


Functions & Properties
----------------------

The Instrument Control driver contains a number of functions and properties which can be used to configure the device and collect data. 

For functions beginning with 'p46000' e.g. ps4000SetChannel, please refer to the corresponding underlying function in the main Programmer's guide for further information on parameters and function descriptions. 

New functions that simplify calls to the underlying driver e.g. setSigGenBuiltInSimple have also been included in the driver.

The resetDevice() function can be used to set the driver back to it's original state which includes Channel A and B (as well as C and D for 4-channel devices) being set to a range of 5V.

Help text for some of the functions and properties can be found by loading the driver by using the 'instrhelp' command or in the Test and Measurement tool (tmtool) using the .


Examples Provided
-----------------

PS4000_IC_Generic_Driver_Block.m 		- Captures a block of data using the driver's default settings and a simple trigger.

PS4000_IC_Generic_Driver_Block_FFT.m 		- Captures a block of data using the driver's default settings and a simple trigger, then plots an FFT of the signal using an example from the MathWorks website.

PS4000_IC_Generic_Driver_Rapid_Block.m  	- Captures a rapid block of data using a simple trigger.

PS4000_IC_Generic_Driver_Rapid_Block_Plot3D.m  - Captures a rapid block of data using a simple trigger and plots the captures in 3D.

PS4000_IC_Generic_Driver_Streaming.m  		- Captures a streaming data using a simple trigger and plots the captures for 2 channels.

PS4000_Generic_Driver_Sig_Gen.m 		- Shows different examples of using the in-built function generator with specific waveforms.

These scripts can be run from the MATLAB command window or editor.


Test and Measurement Tool
-------------------------

The driver can be used with the Test and Measurement Tool by typing 'tmtool' at the MATLAB command prompt. 

Ensure that the current directory is set to PS4000 and that the path to the 'Functions' and root directory is set as described above. The PS5000Config.m file MUST be run PRIOR to loading the driver.

Create a new device object, and load in the driver file. Click 'Connect' to make the connection to the scope and it should then be ready for data collection.

Variables from the MATLAB workspace can be passed as arguments using the following command:

evalin('base', 'variable_name')

where the variable name must be in quotes.

Ensure that the device is disconnected when finished to avoid a driver lockup.


Unsupported Features
--------------------

The following features are not tested fully in this release of the driver:

- Advanced Triggering

Known Issues
------------

* Depending on the PC, plotting data while collecting streaming data may slow down data collection.
* Setting up data buffers for rapid block data may take some time depending on the PC.
* AWG is not funcational with the 4226/7

Contact
-------

Please send any questions, comments and feedback to support@picotech.com