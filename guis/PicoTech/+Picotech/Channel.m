% Object to hold all channel data
% works as a wrapper to include all enumerals and settings, also implements
% settings dialogues.

classdef Channel < uiextrasX.VBox
    
    properties
        chan
        device
        enabled
        scope_running
        range
        offset
        coupling
        color
        
        pBuffer
        data_mV
        data_ms
    end
    
    methods
        function o = Channel(Parent, Device, Channel, advancedFlag)
            o@uiextrasX.VBox('Parent', Parent);
            defer(o)
            o.device = Device;
            o.color = getcolor(o,Channel);
            o.chan = Channel-1;
            hpanel = uipanel('Parent', o, 'Title', ['Channel ' char(64+Channel)]);
            vbox = uiextrasX.VBox('Parent', hpanel);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Parent', hbox, 'Style', 'text', 'String', 'Enable:', 'ForegroundColor', o.color);
            o.enabled = uicontrol('Parent', hbox, 'Style', 'checkbox', 'String', '', 'Value',0, 'Callback', @(s,e)channel_enable(o,get(o.enabled, 'Value')));
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Parent', hbox, 'Style', 'text', 'String', 'Range:');
            %[~, ~, ps4000aEnuminfo,~] = ps4000aMFile;
            voltages = {'10 mV','20 mV','50 mV','100 mV','200 mV','500 mV',...
                '1 V','2 V','10 V','20 V','50 V'};
            o.range = uicontrol('Parent', hbox, 'Style', 'popupmenu', 'String', voltages, 'Value', 1, 'Enable', 'off', 'Callback', @(s,e)channel_changed(o,1));
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Parent', hbox, 'Style', 'text', 'String', 'Offset:', 'ForegroundColor', o.color);
            o.offset = uicontrol('Parent', hbox, 'Style', 'edit', 'String', '0.0', 'Callback', @(s,e)channel_changed(o,1));
            o.color = getcolor(o,Channel);
            if (advancedFlag)
                hbox = uiextrasX.HBox('Parent', vbox);
                uicontrol('Parent', hbox, 'style', 'text', 'String', 'Coupling:');
                o.coupling = uicontrol('Parent', hbox, 'Style', 'popupmenu', 'String', {'AC','DC'}, 'Value', 2);
            else
                o.coupling = uicontrol('Parent', vbox, 'Style', 'popupmenu','String', {'AC','DC'}, 'Value',2,'Visible', 'off');
            end
            set(o, 'BackgroundColor', 'w');
            
            % Set up buffer to put data in and pass the pointer to driver
            %overviewBufferSize  = 250000; % Size of the buffer to collect data from buffer.        
            %ratioMode = ps4000aEnuminfo.enPS4000ARatioMode.PS4000A_RATIO_MODE_NONE;
            % Buffers to be passed to the driver
            %o.pBuffer = libpointer('int16Ptr', zeros(overviewBufferSize, 1));
            %errcode = invoke(o.device, 'ps4000aSetDataBuffer', o.chan, o.pBuffer, overviewBufferSize, 0, 0);
            resume(o)
        end
        
        function channel_enable(o,v)
            if v
                set(o.range, 'Enable', 'on');
                set(o.offset, 'Enable', 'off');
                set(o.coupling, 'Enable', 'off');
            else
                set(o.range, 'Enable', 'off');
                set(o.offset, 'Enable', 'off');
                set(o.coupling, 'Enable', 'off');
            end
            channel_changed(o,1);
        end
        
        function channel_changed(o,v)
            if ~strcmp(get(o.device, 'Status'), 'open')
                return;
            end
            if o.scope_running
                return;
            else
                la = invoke(o.device, 'ps4000aSetChannel', ...
                    o.chan, get(o.enabled, 'Value'), ...
                    get(o.coupling, 'Value')-1, get(o.range, 'Value'), ...
                    str2double(get(o.offset, 'String')))
            end
        end
        
        function col = getcolor(o,channel)
            colors = [0,0,1;...
                1,0,0;...
                0,1,0;...
                1,1,0;...
                0.5,0,0.5;...
                0.5,0.5,0.5;...
                0.6902,0.8794,0.902;...
                1,0.0784,0.5765];
            col = colors(channel,:);
        end
    end
end