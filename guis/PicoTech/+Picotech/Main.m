% Parent for the PicoTech 4824 scope
% Deals with triggering setup, running the scope and collecting the data
% Only works for block mode for now, ie the scope captures the data and
% once that is done it passes it back to the PC. May have alow rep rate,
% but should suffice for what we want to do.
% Also deals with autosave, creates folder channelA, channelB etc in the
% save folder and save the data as [t(s) U(V)] .txt
% can set first file number and file stem.
% All settings are saved upon successful exit into a file in a good
% location

classdef Main < uiextrasX.VBox
    properties
        device
        set_connect
        channels
        ax
        trigger
        startstop
        save
    end
    
    methods
        function obj = Main(varargin)
            if nargin==0
                Parent = figure;
                serialnum = '';
            elseif nargin==1
                Parent = varargin{1};
                serialnum = '';
            elseif nargin==2
                serialnum = varargin{2};
            end
            figure(Parent);
            set(Parent, 'Units', 'pixel', 'Position', [100 100 960 700], 'Menubar', 'none', 'HandleVisibility', 'callback', 'NumberTitle', 'off', 'Name', 'PicoTech scope');
            obj@uiextrasX.VBox('Parent', Parent);
            defer(obj);
            
            PS4000aConfig;
            evalin('base', 'PS4000aConfig');
            obj.device = icdevice('picotech_ps4000a_generic', serialnum); 
            set(Parent, 'Name', ['PicoScope 4000: ' obj.device.unitSerial]);
            % box for axes and triggering etc
            hbox0 = uiextrasX.HBox('Parent', obj);
            hpan = uipanel('Parent', hbox0);
            obj.ax = axes('Parent', hpan);
            vbox = uiextrasX.VBox('Parent', hbox0);
            hbox0.Sizes = [-1 300];
            % Firstly, Connecting to the device
            obj.set_connect = uicontrol('Parent', vbox, 'Style', 'togglebutton', 'String', 'Connect to scope', 'Callback', @(s,e)set_connected(obj,get(obj.set_connect, 'Value')));
            % Now the trigger
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Parent', hbox, 'Style', 'text', 'String', 'Trigger source');
            obj.trigger.source = uicontrol('Parent', hbox, 'Style', 'popupmenu', 'String', {'A','B','C','D','E','F','G','H'}, 'Value', 1);
            hbox  = uiextrasX.HBox('Parent', vbox);
            uicontrol('Parent', hbox, 'Style', 'text', 'String', 'Trigger type');
            obj.trigger.type = uicontrol('Parent', hbox, 'Style', 'popupmenu', 'String', {'Rising','Falling'}, 'Value', 1);
            % Now the autosave folders and filename
            
            % box for all the channel
            hbox = uiextrasX.HBox('Parent', obj);
            for i=1:8
                obj.channels(i) = feval('Picotech.Channel', hbox, obj.device, i,0);
            end
            hbox.Sizes = 95*ones(1,8);
            obj.Sizes = [-1 150];
            resume(obj);
        end
        
        function set_connected(o,v)
            if v
                connect(o.device);
                set(o.set_connect, 'String', 'Disconnect', 'BackgroundColor', 'g');
            else
                disconnect(o.device);
                set(o.set_connect, 'String', 'Connect', 'BackgroundColor', 'r');
            end
            
        end
    end
end

