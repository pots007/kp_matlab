% Generic container for holding an ActiveX control. Handles some
% housekeeping. See APT.MGMotorContor for an example.
% (c) Dane R. Austin 2012
classdef ActiveXContainer < uiextras.Container
    properties
        control
    end
    methods
        function obj = ActiveXContainer(progid, varargin)
            obj@uiextras.Container( varargin{:} );
            obj.control = actxcontrol(progid, 'Parent', ancestor(obj, 'figure'));
            size = getpixelposition( obj );
            obj.control.move(size);
        end
        
        function delete(obj)
            try
                % If the container is closing because the user closed the
                % figure, then this raises a warning because the control is
                % already deleted.
                delete(obj.control);
            end
        end
    end
    methods (Access = protected)
        function redraw(obj)
        end
        
        function onResized( obj, source, eventData )
            % This returns the position with respect to the parent figure
            % or panel
            % new_size = getpixelposition( obj );
            % We want it w.r.t. the parent figure
            new_size = getpixelposition(double(obj), true);
            if ~isempty(obj.control)
                obj.control.move(new_size);
            end
        end
    end
end