classdef main2 < uiextras.VBox
    % Analyse focal spots and whatnot
    % K Poder, IC, 2016
    
    properties
        fig
        dat
        params
        focim
        specs
        allfiles
        focalScanner
    end
    
    methods
        function o = main2(varargin)
            Parent = figure(7073); clf(Parent);
            o@uiextras.VBox('Parent', Parent);
            if (nargin==0); o.dat.fol = pwd;
            elseif (nargin==1); o.dat.fol = varargin{1}; end
            o.fig.Parent = Parent;
            mainbox = o;
            set(o.fig.Parent, 'NumberTitle', 'off', 'Name', 'Focal spot profiler',...
                'Toolbar', 'figure', 'MenuBar', 'none', 'Color', 'w',...
                'HandleVisibility', 'Callback');
            if (ismac)
                set(o.Parent, 'DefaultUIControlFontSize', 13, 'DefaultAxesFontSize', 13);
            elseif ispc
                set(o.Parent, 'DefaultUIControlFontSize', 10, 'DefaultAxesFontSize', 10);
            elseif isunix
                set(o.Parent, 'DefaultUIControlFontSize', 8, 'DefaultAxesFontSize', 10);
            end
            set(o.Parent, 'Units', 'Pixels', 'Position', [200 200 600 500]);
            o.fig.hbox1 = uiextras.HBoxFlex('Parent', mainbox, 'Spacing', 7);
            %The column to have all controls etc
            vboxL = uiextras.VBox('Parent', o.fig.hbox1);
            
            % Pulse settings and stuff
            sb = uiextras.Panel('Parent',vboxL,'Title','Settings','Padding',4);
            sbox = uiextras.VBox('Parent', sb);
            hbox = uiextras.HBox('Parent', sbox);
            uicontrol('Style', 'text', 'String', 'Energy (J)', 'Parent', hbox);
            o.dat.E = 10;
            o.fig.E_ = uicontrol('Style', 'edit', 'String', '10', 'Parent', hbox, 'Callback', @o.changeVal, 'Tag', 'E');
            hbox.Sizes = [-1 50];
            hbox = uiextras.HBox('Parent', sbox);
            uicontrol('Style', 'text', 'String', 'Tau (fwhm)', 'Parent', hbox);
            o.dat.t = 40;
            o.fig.t_ = uicontrol('Style', 'edit', 'String', '40', 'Parent', hbox, 'Callback', @o.changeVal, 'Tag', 't');
            o.dat.tshape = 1;
            o.fig.tshape_ = uicontrol('Style', 'popupmenu', 'String', {'Gauss', 'sech2', 'sin2'}, 'Parent', hbox, 'Callback', @o.changeVal, 'Tag', 'tshape');
            hbox.Sizes = [80 30 -1];
            hbox = uiextras.HBox('Parent', sbox);
            uicontrol('Style', 'text', 'String', 'Cal (um/pix)', 'Parent', hbox);
            o.dat.calib = 1;
            o.fig.calib_ = uicontrol('Style', 'edit', 'String', '1', 'Parent', hbox, 'Callback', @o.changeVal, 'Tag', 'calib');
            hbox.Sizes = [-1 50];
            hbox = uiextras.HBox('Parent', sbox);
            uicontrol('Style', 'text', 'String', 'Focal length (m)', 'Parent', hbox);
            o.dat.F = 3;
            o.fig.F_ = uicontrol('Style', 'edit', 'String', '3', 'Parent', hbox, 'Callback', @o.changeVal, 'Tag', 'F');
            hbox.Sizes = [-1 50];
            hbox = uiextras.HBox('Parent', sbox);
            uicontrol('Style', 'text', 'String', 'Beam D (m)', 'Parent', hbox);
            o.dat.D = 0.15;
            o.fig.D_ = uicontrol('Style', 'edit', 'String', '0.15', 'Parent', hbox, 'Callback', @o.changeVal, 'Tag', 'D');
            o.dat.Dshape = 1;
            o.fig.Dshape_ = uicontrol('Style', 'popupmenu', 'String', {'Top hat', 'Gaussian'}, 'Parent', hbox, 'Callback', @o.changeVal, 'Tag', 'Dshape');
            hbox.Sizes = [75 -1 65];
            hbox = uiextras.HBox('Parent', sbox);
            uicontrol('Style', 'text', 'String', 'Wavelength (nm)', 'Parent', hbox);
            o.dat.lambda = 800;
            o.fig.lambda_ = uicontrol('Style', 'edit', 'String', '800', 'Parent', hbox, 'Callback', @o.changeVal, 'Tag', 'lambda');
            hbox.Sizes = [-1 50];
            hbox = uiextras.HBox('Parent', sbox);
            uicontrol('Style', 'text', 'String', 'CAxis:', 'Parent', hbox);
            o.fig.caxstyle = uicontrol('Style', 'popupmenu', 'String', {'Counts' 'Intensity' 'a0'}, 'Parent', hbox, 'Callback', @o.plotim);
            hbox.Sizes = [ 75 -1];
            
            % File structure panel
            hpan = uiextras.Panel('Parent',vboxL,'Title','Folder:','Padding',1);
            vbox = uiextras.VBox('Parent', hpan);  hbox = uiextras.HBox('Parent', vbox);
            o.fig.curfol = uicontrol('style', 'text', 'String', '.', 'Parent', hbox);
            o.fig.fol = uicontrol('style', 'listbox', 'String', ' ', 'Parent', vbox, 'Callback', @o.setFile, 'Tag', 'ff');
            o.setFile(1);
            vbox.Sizes = [30 -1];
            savestate(o,0);
            
            % --------------------- Final utilities panel
            sb = uiextras.Panel('Parent', vboxL, 'Title', 'Utilities', 'Padding', 4);
            vbox = uiextras.VBox('Parent', sb); hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'BG method:', 'Parent', hbox);
            o.fig.BGmethod_ = uicontrol('Style', 'popupmenu', 'String', {'Below threshold','Top', 'Left', 'Bottom', 'Right',...
                'Left and right', 'Top and bottom', '4 edges'}, 'Parent', hbox, 'Callback', @o.plotim);
            hbox.Sizes = [75 -1];
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Threshold (%):', 'Parent', hbox);
            o.fig.BGthreshold_ = uicontrol('Style', 'edit', 'String', '0.1', 'Parent', hbox, 'Callback', @o.plotim);
            uicontrol('Style', 'text', 'String', 'Width:', 'Parent', hbox);
            o.fig.BGwidth_ = uicontrol('Style', 'edit', 'String', '25', 'Parent', hbox, 'Callback', @o.plotim);
            hbox.Sizes = [-1 30 45 25];
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'pushbutton', 'Parent', hbox, 'String', 'Set burn area', 'Callback', @o.setBurn, 'Tag', '1');
            uicontrol('Style', 'pushbutton', 'Parent', hbox, 'String', 'Clear burn', 'Callback', @o.setBurn, 'Tag', '0');
            hbox = uiextras.HBox('Parent', vbox);
            o.fig.medfilter = uicontrol('Style', 'checkbox', 'String', 'Median filter', 'Parent', hbox, 'Callback', @o.plotim);
            o.fig.imsmooth = uicontrol('Style', 'checkbox', 'String', 'Smooth image', 'Parent', hbox, 'Callback', @o.plotim);
            uicontrol('Style', 'pushbutton', 'Parent', vbox, 'String', 'All in folder', 'Callback', @o.openallfiles);
            uicontrol('Style', 'pushbutton', 'Parent', vbox, 'String', 'Focal scanner', 'Callback', @o.openFocalScanner);
            
            %---------------- The axes column
            vboxR = uiextras.VBox('Parent', o.fig.hbox1);
            %sb = uiextrasX.Panel('Parent',vboxR,'Title','Spot image:','Padding',5);
            sb = uipanel('Parent', vboxR, 'Title', 'Spot image:');
            axes_menu = AxisScaleContextMenu(o.fig.Parent);
            o.fig.ax = axes('Parent', sb,'UIContextMenu',axes_menu.menu);
            o.dat.raw = rand(100);
            if ~isfield(o.dat, 'burncount')
                o.dat.burncount = 0;
                o.dat.burns = zeros(4,10);
            end
            o.fig.him = imagesc(o.dat.raw, 'Parent', o.fig.ax);
            o.fig.cb = colorbar('Peer', o.fig.ax, 'Location', 'Southoutside');
            set(get(o.fig.cb, 'YLabel'), 'String', 'Counts', 'Interpreter', 'latex');
            axis(o.fig.ax, 'image');
            % Listener to trigger things
            addlistener(o.fig.him, 'CData', 'PostSet', @o.spotAnalysis);
            
            % Spot parameters panel
            sb = uiextras.Panel('Parent', vboxR, 'Title', 'Spot parameters:', 'Padding', 4);
            grid = uiextras.Grid('Parent', sb);
            o.fig.cols = 'rgk';
            uicontrol('Style', 'text', 'String', ' ', 'Parent', grid);
            uicontrol('Style', 'text', 'String', '1/e^2 radius', 'Parent', grid, 'ForegroundColor', o.fig.cols(1));
            uicontrol('Style', 'text', 'String', '1/e radius', 'Parent', grid, 'ForegroundColor', o.fig.cols(2));
            uicontrol('Style', 'text', 'String', 'FWHM diameter', 'Parent', grid, 'ForegroundColor', o.fig.cols(3));
            uicontrol('Style', 'text', 'String', 'Plot', 'Parent', grid);
            for i=1:3
                ii = num2str(i);
                o.specs.control(i) = uicontrol('Style', 'checkbox', 'String', '', 'Parent', grid, 'Callback', @o.plotEllipse, 'Tag', ['C' ii]);
            end
                uicontrol('Style', 'text', 'String', 'Major', 'Parent', grid);
            for i=1:3
                o.specs.major(i) = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            end
                uicontrol('Style', 'text', 'String', 'Minor', 'Parent', grid);
            for i=1:3
                o.specs.minor(i) = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            end
                uicontrol('Style', 'text', 'String', 'r', 'Parent', grid);
            for i=1:3
                o.specs.r(i) = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            end
                uicontrol('Style', 'text', 'String', 'r_t', 'Parent', grid);
            for i=1:3
                o.specs.idealR(i) = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            end
                uicontrol('Style', 'text', 'String', 'E/E0', 'Parent', grid);
            for i=1:3
                o.specs.energy(i) = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            end
            o.specs.ell = cell(1,3);
            grid.ColumnSizes = [100 40 40 40 40 40 40];
            o.recalc;
            o.fig.hbox1.Sizes = [200 400];
            vboxL.Sizes = [160 -1 170];
            vboxR.Sizes = [-1 100];
            set(o.Parent, 'ResizeFcn', @(obj,val)resizefunc(o,1));
            assignin('base', 'FocusProfile', o);
        end
        
        %% Utility functions 
        
        function changeVal(o,e,~)
            id = get(e, 'Tag');
            if strcmp(get(e, 'Style'), 'popupmenu')
                v = get(e, 'Value');
                o.dat.(id) = v;
            elseif strcmp(get(e, 'Style'), 'edit')
                v = str2double(get(e, 'String'));
                if isnan(v); return; end;
                o.dat.(id) = v;
            end
            if strcmp(id, 'calib'); o.changeCal; end;
            o.recalc;
        end
        
        function recalc(o, ~, ~)
            % recalculate intensity/theoretical spot size
            if o.dat.Dshape==1
                % This is tophat, so normal thing apply
                coeffs = [1.029 0.61 0.823];
            else
                % Gaussian spot
                coeffs = 2/pi*[1 sqrt(0.5) sqrt(0.25)];
            end
            tlim = coeffs*o.dat.lambda*o.dat.F/o.dat.D*1e-3;
            for i=1:3
                set(o.specs.idealR(i), 'String', sprintf('%2.1f',tlim(i)));
            end
        end
        
        function changeCal(o)
            set(o.fig.him, 'XData', o.dat.calib*(1:length(get(o.fig.him, 'XData'))),...
                'YData', o.dat.calib*(1:length(get(o.fig.him, 'YData'))));
            axis(o.fig.ax, 'image');
            sf = [0.5 0.5 1];
            for k=1:3
                if isempty(o.specs.ell{k}); continue; end;
                longax = o.specs.ell{k}.long_axis*o.dat.calib;
                shortax = o.specs.ell{k}.short_axis*o.dat.calib;
                set(o.specs.major(k), 'String', sprintf('%3.1f',longax*sf(k)));
                set(o.specs.minor(k), 'String', sprintf('%3.1f',shortax*sf(k)));
                set(o.specs.r(k), 'String', sprintf('%3.1f',sqrt(longax*shortax)*sf(k)));
            end
            o.plotEllipse;
        end
        
        function imread(o,~,~)
            [~,~,ext] = fileparts(o.dat.filen);
            switch lower(ext)
                case {'.tif', '.tiff', '.png', '.bmp', '.pgm'}
                    imdata = imread(o.dat.filen);
                %case '.bmp'
                 %   imdata = imread(o.dat.filen);
                case '.raw'
                    % Have to figure out the size!
                    % Trying this with usual sizes, or 640x480, 1280x1024,
                    % 1088x2048
                    ff = dir(o.dat.filen); ff.bytes;
                    switch ff.bytes
                        case 614400 % 640x480, AVT
                            imdata = ReadRAW16bit(o.dat.filen, 640, 480);
                        case 2621440 % 1280x1024
                            imdata = ReadRAW16bit(o.dat.filen, 1280, 1024, 1);
                        case 2457600
                            imdata = ReadRAW16bit(o.dat.filen, 1280, 960);
                        case 4456448
                            imdata = ReadRAW16bit(o.dat.filen, 2048, 1088, 1);
                        case 2457615 % For DESY 1280 by 960 files
                            imdata = ReadRAW16bit(o.dat.filen, 1280, 960, 'DESY');
                        otherwise
                            imdata = ReadRAW16bit(o.dat.filen, 1, 1, 'DESY');
                    end
                otherwise
                    error('Unknown format....');
            end
            o.dat.raw = imdata;
        end
        
        function setFile(o,e,~)
            if ~ishandle(e)
                dirlist = dir(o.dat.fol);
                dlist = {dirlist.name};
                % Try to filter out hidden files on Macs
                logm = ~cellfun(@(x) length(x)>2 && strcmp(x(1), '.'), dlist);
                set(o.fig.fol, 'String', dlist(logm), 'Value', 1, 'ListboxTop', 1);
                return;
            end
            % If double click
            if strcmp(get(o.fig.Parent, 'SelectionType'), 'open')
                index = get(e, 'Value');
                dlist = get(e, 'String');
                cursel = fullfile(o.dat.fol, dlist{index});
                cursel = GetFullPath(cursel);
                if isdir(cursel)
                    o.dat.fol = cursel;
                    dirlist = dir(o.dat.fol);
                    dlist = {dirlist.name};
                    % Try to filter out hidden files on Macs
                    logm = ~cellfun(@(x) length(x)>2 && strcmp(x(1), '.'), dlist);
                    set(e, 'String', dlist(logm), 'Value', 1, 'ListboxTop', 1);
                    [~,k,~] = fileparts(o.dat.fol);
                    set(o.fig.curfol, 'String', k);
                else
                    try
                        o.dat.filen = cursel;
                        o.imread;
                    catch err
                        disp('Error reading image.');
                        disp(err.message);
                        return;
                        %p = focreaderror(o, cursel);
                        %if p.success
                        %    o.dat.raw = p.data;
                        %else
                        %    return;
                        %end
                    end
                    o.plotim; % Triggered by listener now.
                end
            end
        end
        
        function resizefunc(o,~)
            siz = (get(o.Parent, 'Position'));
            o.fig.hbox1.Sizes = [200 siz(3)-200];
        end
        
        function centroidlocs = centroidloc(o)
            I = o.dat.raw;
            [rows, cols] = size(I);
            IMAGE = double(I);
            %Define BG level, under which pixels will not count towards centroid.
            BGfac = 0.5;
            BG = BGfac*max(IMAGE(:));
            IMAGE(IMAGE<BG)=0;
            %Find centroid by weighting pixels by intensity
            x = ones(rows,1)*(1:cols);
            y = (1:rows)'*ones(1,cols);
            totcounts = sum(IMAGE(:));
            xbar = sum(sum(IMAGE.*x))/totcounts;
            ybar = sum(sum(IMAGE.*y))/totcounts;
            centroidlocs = [xbar ybar]*o.dat.calib;
        end
        
        function BG = getBackground(o)
            bgopt = get(o.fig.BGmethod_, 'Value');
            bgW = str2double(get(o.fig.BGwidth_, 'String'));
            if isnan(bgW) || bgW<0; bgW = 25; end;
            bgThres = str2double(get(o.fig.BGthreshold_, 'String'))*0.01;
            if isnan(bgThres) || bgThres<0; bgThres = 1e-3; end;
            BG.bg = 0; BG.rect = 1;
            [ym, xm] = size(o.dat.im);
            switch bgopt
                case 1 % Below threshold
                    logm = o.dat.im<bgThres*max(o.dat.im(:)) & o.dat.im~=0;
                    if sum(logm(:))==0
                        disp('No pixels below threshold!');
                        bgfact = 0;
                    else
                        bgfact = mean(mean(o.dat.im(logm)));
                    end
                    bg = zeros(size(o.dat.im));
                    bg(o.dat.im~=0) = bgfact;
                case 2 % Top
                    sigrect = [1 1 xm-1 ym-bgW];
                case 3 % Left
                    sigrect = [bgW 1 xm-bgW ym-1];
                case 4 % Bottom
                    sigrect = [1 bgW xm-1 ym-bgW];
                case 5 % Right
                    sigrect = [1 1 xm-bgW ym-1];
                case 6 % Left and right
                    sigrect = [bgW 1 xm-2*bgW ym-1];
                case 7 % Top and bottom
                    sigrect = [1 bgW xm-1 ym-2*bgW];
                case 8 % All edges
                    sigrect = [bgW bgW xm-2*bgW ym-2*bgW];
            end
            if bgopt==1
                BG.bg = bg;
                BG.signalarea = contourc(o.dat.im, bgThres*max(o.dat.im(:))*[1 1]);
                BG.rect = 0; % Flag to plot it as a contour instead of rectangle
            else
                BG.bg = createBackground(o.dat.im, sigrect, [], [], 4);
                BG.signalarea = sigrect;
            end
        end
        
        %% Plotting and analysis
        function plotim(o, ~, ~)
            o.dat.im = double(o.dat.raw);
            % Apply burns to the raw image
            if (o.dat.burncount ~= 0)
                for i=1:o.dat.burncount
                    coor = o.dat.burns(:,i);
                    o.dat.im(coor(2):coor(2)+coor(4),coor(1):coor(1)+coor(3)) = 0;
                end
            end
            % Subtract BG, depending on what the user wants to do!
            %[ym, xm] = size(o.dat.im); BGl = 25;
            %bg1 = mean(o.dat.im(1:BGl,:));
            %bg2 = mean(o.dat.im(ym-BGl:ym,:));
            %bg3 = mean(o.dat.im(:,1:BGl));
            %bg4 = mean(o.dat.im(:,xm-BGl:xm));
            %bg = mean([bg1 bg2 bg3 bg4]);
            %bg = createBackground(o.dat.im, [25 25 xm-50 ym-50], [], [], 4);
            bg = o.getBackground;
            o.dat.im = o.dat.im-bg.bg;
            % Apply filter to image
            if get(o.fig.medfilter, 'Value'); o.dat.im = medfilt2(o.dat.im); end
            if get(o.fig.imsmooth, 'Value')
                hfilter = ones(5)./5;
                o.dat.im = imfilter(o.dat.im, hfilter);
            end
            % Convert to intensity, if we want to
            clab = 'Counts';
            if get(o.fig.caxstyle, 'Value')~=1 % Need conversion
                % Display intensity!
                %tau = o.dat.t*1e-15/(sqrt(2*log(2)));
                %taufac = sqrt(pi/2*tau^2);
                % Making it more clear:
                tFWHM = o.dat.t*1e-15;
                taufac = sqrt(pi*tFWHM^2/(2*2*log(2)));
                spatialfac = sum(o.dat.im(:))*(o.dat.calib*1e-6)^2;
                totE = taufac*spatialfac;
                scalefac = o.dat.E/totE;
                if get(o.fig.caxstyle, 'Value')==2
                    o.dat.im = o.dat.im*scalefac*1e-4;
                    clab = 'Intensity / $\mathrm{Wcm}^{-2}$'; 
                elseif get(o.fig.caxstyle, 'Value')==3
                    o.dat.im = 0.856*o.dat.lambda*1e-3*sqrt(abs(o.dat.im*scalefac*1e-22));
                    clab = '$a_0$';
                end
            end
            % And now plot everything
            yax = o.dat.calib*(1:size(o.dat.im,1));
            xax = o.dat.calib*(1:size(o.dat.im,2));
            set(o.fig.him, 'XData', xax, 'YData', yax, 'CData', o.dat.im);
            delete(findobj(o.fig.ax, 'Tag', 'signalarea'));
            if bg.rect
                rectangle('Position', bg.signalarea*o.dat.calib, 'EdgeColor', 'r',...
                    'tag', 'signalarea', 'Parent', o.fig.ax);
            else
                %hold on;
                %plot(bg.signalarea(1,:), bg.signalarea(2,:), '-r', 'tag', 'signalarea');
                %hold off;
            end
            set(get(o.fig.cb, 'YLabel'), 'String', clab);
        end
        
        function spotAnalysis(o,~,~)
            imc = o.dat.im;
            % find contours for different radii
            immax = max(imc(:));
            contvals = [exp(-2) exp(-2); exp(-1) exp(-1); 0.5 0.5];
            for k=1:3
                C = contourc(imc, contvals(k,:)*immax);
                x = C(1,2:end); y = C(2,2:end);
                %Remove points from your contour that are not actually part of the main
                %contour - this is necessary when looking at noisy data
                logm = abs(x-mean(x)) < 2*std(x) & abs(y-mean(y)) < 2*std(y);
                x = x(logm); y = y(logm);
                % NEW 2018 June
                cc = focusContourTrace(imc, contvals(k,1));
                x = cc{1}(:,1); y = cc{1}(:,2);
                % Fit the ellipse
                ell = fit_ellipse(x,y);
                if ~isempty(ell.status)
                    disp('No ellipse! Try setting burn areas.');
                    continue;
                end
                o.specs.ell{k} = ell;
                % Find the energy content - idea stolen form Juffalo's script
                o.changeCal;
                [X,Y] = meshgrid(1:size(imc,2),1:size(imc,1));
                xr = (X - ell.X0_in)*cos(ell.phi) - (Y - ell.Y0_in)*sin(ell.phi);
                yr = (X - ell.X0_in)*sin(ell.phi) + (Y - ell.Y0_in)*cos(ell.phi);
                logm = xr.^2/ell.a^2 + yr.^2/ell.b^2 < 1;
                encont = sum(sum(imc(logm)))/sum(imc(:));
                set(o.specs.energy(k), 'String', sprintf('%2.1f', 100*encont));
            end
            o.plotEllipse;
        end
        
        function plotEllipse(o, ~, ~)
            calval = o.dat.calib;
            for k=1:3
                % Initially delete the stuff
                circs = findobj(o.fig.ax, 'Type', 'Line', 'Color', o.fig.cols(k));
                for i=1:length(circs); delete(circs(i)); end
                % And plot new, if we want to
                if ~get(o.specs.control(k), 'Value'); continue; end;
                ell = o.specs.ell{k};
                if isempty(ell); continue; end
                hold(o.fig.ax, 'on');
                R = [cos(ell.phi) sin(ell.phi); -sin(ell.phi) cos(ell.phi)];
                % the axes
                ver_line        = [ [ell.X0 ell.X0]; ell.Y0+ell.b*[-1 1] ];
                horz_line       = [ ell.X0+ell.a*[-1 1]; [ell.Y0 ell.Y0] ];
                new_ver_line    = calval * R * ver_line;
                new_horz_line   = calval * R * horz_line;
                % the ellipse
                theta_r         = linspace(0,2*pi);
                ellipse_x_r     = ell.X0 + ell.a*cos( theta_r );
                ellipse_y_r     = ell.Y0 + ell.b*sin( theta_r );
                rotated_ellipse = calval * R * [ellipse_x_r;ellipse_y_r];
                % draw
                plot(o.fig.ax, new_ver_line(1,:),new_ver_line(2,:), o.fig.cols(k));
                plot(o.fig.ax, new_horz_line(1,:),new_horz_line(2,:), o.fig.cols(k));
                plot(o.fig.ax, rotated_ellipse(1,:),rotated_ellipse(2,:), o.fig.cols(k));
                hold(o.fig.ax, 'off');
            end
        end
          
        function savestate(o,val)
            fol = fileparts(which('FocusProfiler.main2'));
            compname = getComputerName;
            if val %save state
                fstate = o.dat; %#ok<NASGU>
                save(fullfile(fol, ['state_' compname '.mat']), 'fstate');
            else % recall
                if (exist([fol '/state_' compname '.mat'], 'file'))
                    f = load(fullfile(fol, ['state_' compname '.mat']));
                    fstate = f.fstate;
                    o.dat = fstate;
                    % And update GUI stuff
                    texts = {'E', 't', 'calib', 'F', 'D', 'lambda'};
                    for i=1:length(texts)
                        set(o.fig.([texts{i} '_']), 'String', num2str(fstate.(texts{i})));
                    end
                    popups = {'tshape', 'Dshape'};
                    for i=1:length(popups)
                        set(o.fig.([popups{i} '_']), 'Value', fstate.(popups{i}));
                    end
                    setFile(o,-2);
                end
            end
        end
        
        function setBurn(o,e,~)
            val = str2double(get(e, 'Tag'));
            if val
                figure(7172);
                if isfield(o.dat, 'im')
                    imagesc(o.dat.im);
                else
                    imagesc(o.dat.raw);
                end
                burn = getrect(7172);
                o.dat.burncount = o.dat.burncount+1;
                % Make sure burns are limited to image size.
                if round(burn(1))<1; burn(1) = 1; end;
                if round(burn(2))<1; burn(2) = 1; end;
                if round(burn(3))+round(burn(1))>size(o.dat.raw,2); burn(3) = size(o.dat.raw,2)-round(burn(1)); end;
                if round(burn(4))+round(burn(2))>size(o.dat.raw,1); burn(4) = size(o.dat.raw,1)-round(burn(2)); end;
                o.dat.burns(:,o.dat.burncount) = round(burn);
                %o.dat.raw(round(burn(2)):round(burn(2)+burn(4)), round(burn(1)):round(burn(1)+burn(3))) = 0;
                close(7172);
            else
                o.dat.burncount = 0;
                o.dat.burns = zeros(4,10);
            end
            o.plotim;
        end
                
        function p = focreaderror(o, filename)
            answ = questdlg('Failed to open image! Are you opening an AVT .raw image?', 'Error');
            if strcmpi(answ, 'Yes')
                answer = inputdlg({'Width', 'Height'}, 'Enter .raw file size', 1, {'640', '480'});
                try
                    p.data = ReadRAW16bit(filename,str2double(answer{1}), str2double(answer{2}));
                    p.success = 1;
                catch
                    disp('No luck. Try another file');
                    p.success = 0;
                    p.data = [];
                end
            else
                p.success = 0;
                p.data = [];
            end
        end
            
        %% Batch processing part
        function openallfiles(o,~,~)
            o.allfiles = o.build_allfiles;
        end
        
        function p = build_allfiles(o)
            p.fig.parent = figure(7171); clf(p.fig.parent);
            set(7171, 'NumberTitle', 'off', 'Name', 'Focal spot profiler', 'HandleVisibility', 'callback');
            if ismac
                set(7171, 'DefaultUIControlFontSize', 14, 'DefaultAxesFontSize', 14);
            elseif ispc
                set(7171, 'DefaultUIControlFontSize', 10, 'DefaultAxesFontSize', 10);
            end
            set(p.fig.parent, 'Units', 'Pixels', 'Position', [200 200 800 600]);
            % some filtering for filenames, then table with calculated
            % values, also averaged values, export button, cancel button,
            % and progress bar, also a plot of the results somehow
            hbox0 = uiextras.HBox('Parent', p.fig.parent);
            vboxL = uiextras.VBox('Parent', hbox0);
            vboxR = uiextras.VBox('Parent', hbox0);
            hbox0.Sizes = [250 -1];
            % Left hand panel, File filtering first
            p0 = uiextras.Panel('Parent',vboxL,'Title','Settings','Padding',4);
            vbox = uiextras.VBox('Parent', p0);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('style', 'text', 'Parent', hbox, 'String', 'File filter');
            p.fig.filefilter = uicontrol('style', 'edit', 'Parent', hbox, 'String', '*.png', 'Callback', @o.filefilter);
            hbox = uiextras.VBox('Parent', vbox);
            p.fig.filefilterres = uicontrol('style', 'text', 'Parent', hbox, 'String', '0 files');
            uicontrol('style', 'text', 'String', 'Image size for .raw files', 'Parent', vbox);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('style', 'text', 'String', 'width', 'Parent', hbox);
            p.fig.width = uicontrol('style', 'edit', 'String', '640', 'Parent', hbox);
            uicontrol('style', 'text', 'String', 'height', 'Parent', hbox);
            p.fig.height = uicontrol('style', 'edit', 'String', '480', 'Parent', hbox);
            p.fig.startbut = uicontrol('Parent', vbox, 'String', 'START', 'style', 'togglebutton', 'Callback', @o.doallfiles);
            %hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Parent', vbox, 'Style', 'pushbutton', 'String', 'Export figure', 'Callback', @o.exportfig);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Parent', hbox, 'Style', 'pushbutton', 'String', 'Export to base', 'Callback', @o.exportdata, 'Tag', 'ws');
            uicontrol('Parent', hbox, 'Style', 'pushbutton', 'String', 'Export to file', 'Callback', @o.exportdata, 'Tag', 'file');
            vbox.Sizes = 25*ones(1,7);%[30 30 30 30 30 30];
            % Now plot controls
            p3 = uiextras.Panel('Parent', vboxL, 'Title', 'Plot controls', 'Padding', 5);
            vbox = uiextras.VBox('Parent', p3);
            p.fig.fwhm(1) = uicontrol('style', 'checkbox', 'Parent', vbox, 'String', 'Plot FWHM', 'Callback', @o.enableplot, 'Tag', 'fwhm1');
            hbox = uiextras.HBox('Parent', vbox);
            % The tag for each checkbox is the index in the plot handles matrix
            p.fig.fwhm(2) = uicontrol('style', 'checkbox', 'Parent', hbox, 'Tag', '11',...
                'String', '<html> <i> x <font color="red"> x</font></html>', 'Callback', @o.replotone);
            p.fig.fwhm(3) = uicontrol('style', 'checkbox', 'Parent', hbox, 'Tag', '12',...
                'String', '<html> <i> y <font color="red"> &#9633</font></html>', 'Callback', @o.replotone);
            p.fig.fwhm(4) = uicontrol('style', 'checkbox', 'Parent', hbox, 'Tag', '13',...
                'String', '<html> <i> r <font color="red"> &#9675</font></html>', 'Callback', @o.replotone);
            p.fig.fwhm(5) = uicontrol('style', 'checkbox', 'Parent', hbox, 'Tag', '14',...
                'String', '<html> <i> E/E0 <font color="red"> &#9671</font></html>', 'Callback', @o.replotone);
            hbox.Sizes = [-1 -1 -1 80];
            p.fig.e(1) = uicontrol('style', 'checkbox', 'Parent', vbox, 'String', 'Plot 1/e', 'Callback', @o.enableplot, 'Tag', 'e2');
            hbox = uiextras.HBox('Parent', vbox);
            p.fig.e(2) = uicontrol('style', 'checkbox', 'Parent', hbox, 'Tag', '21',...
                'String', '<html> <i> x <font color="green"> x</font></html>', 'Callback', @o.replotone);
            p.fig.e(3) = uicontrol('style', 'checkbox', 'Parent', hbox, 'Tag', '22',...
                'String', '<html> <i> y <font color="green"> &#9633</font></html>', 'Callback', @o.replotone);
            p.fig.e(4) = uicontrol('style', 'checkbox', 'Parent', hbox, 'Tag', '23',...
                'String', '<html> <i> r <font color="green"> &#9675</font></html>', 'Callback', @o.replotone);
            p.fig.e(5) = uicontrol('style', 'checkbox', 'Parent', hbox, 'Tag', '24',...
                'String', '<html> <i> E/E0 <font color="green"> &#9671</font></html>', 'Callback', @o.replotone);
            hbox.Sizes = [-1 -1 -1 80];
            p.fig.ee(1) = uicontrol('style', 'checkbox', 'Parent', vbox, 'String', 'Plot 1/ee', 'Callback', @o.enableplot, 'Tag', 'ee3');
            hbox = uiextras.HBox('Parent', vbox);
            p.fig.ee(2) = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'x', 'Callback', @o.replotone, 'Tag', '31');
            p.fig.ee(3) = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'y', 'Callback', @o.replotone, 'Tag', '32');
            p.fig.ee(4) = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'r', 'Callback', @o.replotone, 'Tag', '33');
            p.fig.ee(5) = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'E/E0', 'Callback', @o.replotone, 'Tag', '34');
            % Create the necessary handle and visibility matrices
            p.fig.hh.hline = -ones(3,4);
            p.fig.hh.visible = zeros(3,4);
            rs = {'fwhm', 'e', 'ee'}; ls = {'x', 'y', 'r', 'E'};
            for i=1:3
                for k=1:4
                    p.fig.hh.leglabels{i,k} = [rs{i} ', ' ls{k}];
                end
            end
            % Right hand panel!
            uiextras.HBox('Parent', vboxL);
            vboxL.Sizes = [190 130 -1];
            % Results panel and axes
            p1 = uipanel('Parent', vboxR, 'Title', 'Results plot');
            hbox = uiextras.HBox('Parent', p1);
            p.fig.ax = axes('Parent', hbox, 'Box', 'on', 'NextPlot', 'add');
            xlabel(p.fig.ax, 'Shot number');
            ylabel(p.fig.ax, {'Spot size / \mu m', 'Energy within parameter / %'});
            p2 = uipanel('Parent', vboxR, 'Title', 'Results data');
            hbox = uiextras.VBox('Parent', p2);
            grid = uiextras.Grid('Parent', hbox);
            uicontrol('Style', 'text', 'String', '', 'Parent', grid);
            uicontrol('Style', 'text', 'String', '1/e^2', 'Parent', grid);
            uicontrol('Style', 'text', 'String', '1/e', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'FWHM', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'x', 'Parent', grid);
            p.specs.ee.x = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.x = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.x = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'var(x)', 'Parent', grid);
            p.specs.ee.xv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.xv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.xv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'y', 'Parent', grid);
            p.specs.ee.y = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.y = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.y = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'var(y)', 'Parent', grid);
            p.specs.ee.yv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.yv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.yv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'r', 'Parent', grid);
            p.specs.ee.r = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.r = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.r = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'var(r)', 'Parent', grid);
            p.specs.ee.rv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.rv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.rv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'r_t', 'Parent', grid);
            p.specs.ee.idealR = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.idealR = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.idealR = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'E/E0', 'Parent', grid);
            p.specs.ee.E = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.E = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.E = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'var(E/E0)', 'Parent', grid);
            p.specs.ee.Ev = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.Ev = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.Ev = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            
            grid.ColumnSizes = [50 40 40 40 40 40 40 40 40 60];
            hbox2 = uiextras.HBox('Parent', hbox);
            uicontrol('Style', 'text', 'Parent', hbox2, 'String', 'Stability');
            hbox2 = uiextras.HBox('Parent', hbox);
            p.fig.stabinfo = uicontrol('Style', 'text', 'Parent', hbox2, 'String', 'x: y:');
            hbox.Sizes = [-1 20 20];
            vboxR.Sizes = [-1 150];
            % Plot symbols
            p.fig.sym = 'xsod';
        end
        
        function doallfiles(o,e,~)
            flist = o.filefilter(o.allfiles.fig.filefilter); nfiles = length(flist);
            val = get(e, 'Value');
            if val
                cla(o.allfiles.fig.ax);
                set(o.allfiles.fig.startbut, 'String', 'STOP');
                o.allfiles.fig.hh.hline = -ones(3,4);
                o.allfiles.dat.params = cell(3,4); % The spot parameters live here
                o.allfiles.dat.locs = zeros(nfiles,2); %Spot centroid locations live here
                o.allfiles.dat.peakI = zeros(nfiles,1); % Peak intensity lives here
                for i=1:3
                    for k=1:4
                        o.allfiles.dat.params{i,k} = NaN(1, nfiles);
                        o.allfiles.fig.hh.hline(i,k) = plot(o.allfiles.fig.ax, 1:nfiles, NaN(1,nfiles), [o.fig.cols(i) o.allfiles.fig.sym(k)]);
                        % Show the plot, if we asked it to be shown
                        set(o.allfiles.fig.hh.hline(i,k), 'Visible', bool2onoff(o.allfiles.fig.hh.visible(i,k)));
                    end
                end
                logm = logical(o.allfiles.fig.hh.visible);
                %o.allfiles.fig.hh.leg = legend(o.allfiles.fig.ax, o.allfiles.fig.hh.hline(logm),...
                 %   o.allfiles.fig.hh.leglabels(logm), 'Location', 'northeast');
            else
                set(o.allfiles.fig.startbut, 'String', 'START');
                return;
            end
            % And now actually analyse all files!
            for l=1:nfiles;
                if ~get(o.allfiles.fig.startbut, 'Value')
                    break;
                end
                fprintf('Analysing file %3i: %s\n', l, flist(l).name);
                try
                    o.dat.filen = fullfile(o.dat.fol, flist(l).name);
                    o.imread;
                catch err
                    disp(err.message);
                    continue;
                end
                o.plotim;
                replotsummary(o, l);
                recalcsummary(o, l);
            end
            % And if we're done
            set(o.allfiles.fig.startbut, 'String', 'START', 'Value', 0);
        end
        
        function exportfig(o,~,~)
            figure(7173); clf(7173); set(7173, 'Color', 'w');
            destax = axes('Parent', 7173);
            copyobj(findobj(o.allfiles.fig.ax, 'Type', 'line'), destax);
            xlabel(destax, 'Shot number');
            ylabel(destax, {'Spot size / \mu m', 'Energy within parameter / %'});
            logm = logical(o.allfiles.fig.hh.visible);
            legend(destax, o.allfiles.fig.hh.hline(logm),...
                    o.allfiles.fig.hh.leglabels(logm), 'Location', 'eastoutside');
            set(destax, 'Box', 'on');
            setAllFonts(7173,16);
            files = {'pdf'; 'png'};
            [fname,paths,fext] = uiputfile(files,'Save plot as', [o.dat.fol '/']);
            if isequal(fname,0) || isequal(paths,0) || isequal(fext,0)
                return;
            end
            export_fig(fullfile(paths, fname), '-nocrop', ['-' files{fext}], 7173);
        end
        
        function exportdata(o,e,~)
            if isfield(o.allfiles, 'dat')
                l = length(o.allfiles.dat.params{1,1});
                dat_ = NaN(15,l);
                in = 1;
                for i=1:3
                    for k=1:4
                        dat_(in,:) = o.allfiles.dat.params{i,k};
                        in = in+1;
                    end
                end
                dat_(13:14,:) = o.allfiles.dat.locs';
                dat_(15,:) = o.allfiles.dat.peakI;
                if strcmp(get(e, 'Tag'), 'ws')
                    assignin('base', 'focdata', dat_);
                else
                    [fname,paths,fext] = uiputfile('*.mat','Export data as', [o.dat.fol '/']);
                    if isequal(fname,0) || isequal(paths,0) || isequal(fext,0)
                        return;
                    end
                    focdata = dat_; %#ok<NASGU>
                    rowHeaders = {'1/e^2 major radius', '1/e^2 minor radius',...
                        '1/e^2 radius', '1/e^2 encircled energy',...
                        '1/e major radius', '1/e minor radius',...
                        '1/e radius', '1/e encircled energy',...
                        'FWHM major radius', 'FWHM minor radius',...
                        'FWHM radius', 'FWHM encircled energy',...
                        'x centroid location', 'y centroid location',...
                        'Intensity (W/cm^2)'};%#ok<NASGU>
                    fcal = o.dat;
                    fcal.filen = get(o.allfiles.fig.filefilter, 'String');
                    save(fullfile(paths, fname), 'focdata', 'rowHeaders', 'fcal');
                end
            else
                return
            end
        end
        
        function replotsummary(o, nshot)
            % Update the YData of all the plot handles.
            for i=1:3
                ell = o.specs.ell{i};
                if isempty(ell); continue; end;
                o.allfiles.dat.params{i,1}(nshot) = ell.long_axis*o.dat.calib*0.5;
                o.allfiles.dat.params{i,2}(nshot) = ell.short_axis*o.dat.calib*0.5;
                o.allfiles.dat.params{i,3}(nshot) = 0.5*sqrt(ell.long_axis*ell.short_axis)*o.dat.calib;
                o.allfiles.dat.params{i,4}(nshot) = str2double(get(o.specs.energy(i), 'String'));
                for k=1:4
                    set(o.allfiles.fig.hh.hline(4-i,k), 'YData', o.allfiles.dat.params{i,k});
                    drawnow;
                end
            end
            o.allfiles.dat.locs(nshot,:) = o.dat.calib*centroidloc(o);
            
            % Display intensity!
            tau = o.dat.t*1e-15/(sqrt(2*log(2)));
            taufac = sqrt(pi/2*tau^2);
            spatialfac = sum(o.dat.im(:))*(o.dat.calib*1e-6)^2;
            totE = taufac*spatialfac;
            scalefac = o.dat.E/totE;
            %if get(o.fig.caxstyle, 'Value')==1
                imm = o.dat.im*scalefac*1e-4;
            %elseif get(o.fig.caxstyle, 'Value')==2
            %    imm = o.dat.im;
            %elseif get(o.fig.caxstyle, 'Value')==3
            %    imm = (o.dat.im/(0.856*o.dat.lambda*1e-3)).^2*1e18;
            %end
            
            o.allfiles.dat.peakI(nshot) = max(imm(:));
            set(o.allfiles.fig.ax, 'XLim', [0.5 nshot+0.5]);
        end
        
        function recalcsummary(o, nshot)
            rs = {'ee', 'e', 'fwhm'};
            ls = {'x', 'y', 'r', 'E'};
            for i=1:3
                for k=1:4
                    set(o.allfiles.specs.(rs{i}).(ls{k}), 'String', num2str(mean(o.allfiles.dat.params{i,k}(1:nshot)), '%2.1f'));
                    set(o.allfiles.specs.(rs{i}).([ls{k} 'v']), 'String', num2str(sqrt(var(o.allfiles.dat.params{i,k}(1:nshot))), '%2.1f'));
                end
            end
            set(o.allfiles.fig.stabinfo, 'String', ...
                sprintf('var(x_m) = %2.1f; var(y_m) = %2.1f', sqrt(var(o.allfiles.dat.locs(1:nshot,:)))));    
        end
        
        function replotone(o,e,~)
            if ishandle(e) % If executing callback from checkbox!
                v = get(e, 'Tag'); i = str2double(v(1)); k = str2double(v(2));
                o.allfiles.fig.hh.visible(i, k) = get(e, 'Value');
                if ishandle(o.allfiles.fig.hh.hline(i,k)) % If the plot handle exists
                    set(o.allfiles.fig.hh.hline(i, k), 'Visible', bool2onoff(get(e, 'Value')));
                end
                
            else
                % Otherwise, just apply all visible logicals to plots
                if ishandle(o.allfiles.fig.hh.hline(1,1))
                    % If at least one of them is a handle, they all are
                    % Need this workaround...
                    hh = o.allfiles.fig.hh.hline(:);
                    for i=1:length(hh)
                        set(hh(i), 'Visible', bool2onoff(o.allfiles.fig.hh.visible(i)));
                    end
                end
            end
            % In any case, sort out legend in the end!
            if isfield(o.allfiles.fig.hh, 'leg')
                if ishandle(o.allfiles.fig.hh.leg); delete(o.allfiles.fig.hh.leg); end;
            end
            logm = logical(o.allfiles.fig.hh.visible);
            %o.allfiles.fig.hh.leg = legend(o.allfiles.fig.ax, o.allfiles.fig.hh.hline(logm),...
             %    o.allfiles.fig.hh.leglabels(logm), 'Location', 'northeast');
        end
          
        function filelist = filefilter(o,e,~)
            filt = get(e, 'String');
            flist = dir(fullfile(o.dat.fol, filt));
            set(o.allfiles.fig.filefilterres, 'String', [num2str(length(flist)) ' files']);
            filelist = flist;
        end
        
        function enableplot(o,e,~)
            vv = get(e, 'Tag'); val = get(e, 'Value');
            hh = o.allfiles.fig.(vv(1:end-1));
            set(hh(2:5), 'Value', val);
            o.allfiles.fig.hh.visible(str2double(vv(end)),:) = val;
            o.replotone(-1);
        end
        
        %% This is motorised focal scan part
        function openFocalScanner(o,~,~)
            o.focalScanner = o.build_focalScanner;
        end
        
        function p = build_focalScanner(o)
            addpath('/local/lib');
            p.fig.parent = figure(7172); clf(p.fig.parent);
            set(7172, 'NumberTitle', 'off', 'Name', 'Focal spot profiler',...
                'HandleVisibility', 'callback');
            if ismac
                set(7172, 'DefaultUIControlFontSize', 14, 'DefaultAxesFontSize', 14);
            elseif ispc || isunix
                set(7172, 'DefaultUIControlFontSize', 9, 'DefaultAxesFontSize', 9);
            end
            set(p.fig.parent, 'Units', 'Pixels', 'Position', [200 200 800 600]);
            % some filtering for filenames, then table with calculated
            % values, also averaged values, export button, cancel button,
            % and progress bar, also a plot of the results somehow
            hbox0 = uiextras.HBox('Parent', p.fig.parent);
            vboxL = uiextras.VBox('Parent', hbox0);
            vboxR = uiextras.VBox('Parent', hbox0);
            hbox0.Sizes = [250 -1];
            p0 = uiextras.Panel('Parent',vboxL,'Title','Settings','Padding',4);
            vbox = uiextras.VBox('Parent', p0);
            
            %  ------######### Motor settings
            uicontrol('style', 'text', 'Parent', vbox, 'String', 'Motor address:');
            p.fig.motorAddress = uicontrol('style', 'edit', 'Parent', vbox,...
                    'String', 'FLASH.DIAG/FFW.BOND.MOTOR/MOTOR12');
                    %'String', 'TTF2.EXP/FLAMOBIL2/MOTOR2');
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Start position');
            p.fig.startPos = uicontrol('style', 'edit', 'parent', hbox, 'String', '-205000');
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'End position');
            p.fig.endPos = uicontrol('style', 'edit', 'parent', hbox, 'String', '-235000');
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Steps/mm:');
            p.fig.stepsPermm = uicontrol('style', 'edit', 'parent', hbox, 'String', '12800');
            
            uicontrol('style', 'text', 'Parent', vbox, 'String', 'Camera address:');
            p.fig.cameraAddress = uicontrol('style', 'edit', 'Parent', vbox,...
                    'String', 'FLASH.DIAG/FLASHFWDCAM8.CAM/TargetChamberFocalSpot');
                    %'String', 'FLASH.DIAG/FLASHFWDCAM8.CAM/CompTestFF');
            
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Number of scan steps:');
            p.fig.nSteps = javaNumSpinner2(10, 1, 50, 1, hbox);
            hbox.Sizes = [-1 45];
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Number of images at step:');
            p.fig.nImages = javaNumSpinner2(10, 1, 50, 1, hbox);
            hbox.Sizes = [-1 45];
            
            % And options for autosave:
            hbox = uiextras.HBox('Parent', vbox);
            p.fig.autoSave = uicontrol('Style', 'checkbox', 'String', 'Auto-save images', 'Parent', hbox);
            p.fig.autoSavePath = uicontrol('Style', 'edit', 'Parent', vbox,...
                'String', '/home/fflab28m/newdata/Datasets/X-1/FFLaserBeamline/2019/20190808_TransverseFocusScan');
                %'String', '/home/fflab28m-data/FocalSpot/TargetChamber/');
            
            p0 = uiextras.Panel('Parent',vboxL,'Title','Controls','Padding',4);
            vbox = uiextras.VBox('Parent', p0);
            p.fig.startButton = uicontrol('Style', 'togglebutton', 'Parent', vbox, ...
                'Value', 0, 'String', 'Start scan', 'callback', @o.performScan);
            p.fig.motorStatus = uicontrol('style', 'text', 'String', 'Motor idle', 'parent', vbox);
            p.fig.fitButton = uicontrol('Style', 'pushbutton', 'Parent', vbox,...
                'String', 'Fit focus parameters', 'callback', @o.fitFocalScan);
            uiextras.HBox('Parent', vboxL);
            vboxL.Sizes = [280 100 -1];
            
            
            % -----####### Results panel and axes
            p1 = uipanel('Parent', vboxR, 'Title', 'Results plot');
            hbox = uiextras.HBox('Parent', p1);
            p.fig.ax = axes('Parent', hbox, 'Box', 'on', 'NextPlot', 'add');
            xlabel(p.fig.ax, 'Motor position');
            ylabel(p.fig.ax, 'Spot size');
            % Useful data:
            p.fig.hData = [];
            p.dat.images = [];
        end
        
        function performScan(o,~,~)
            if ~o.focalScanner.fig.startButton.Value; return; end;
            %if strcmp(o.focalScanner.fig.startButton.String, 'Stop scan')
            %    flag = 0;
            %    o.focalScanner.fig.startButton.String = 'Start scan';
            %else
                o.focalScanner.fig.startButton.String = 'Stop scan';
            %    flag = 1;
            %end
            % This will perform the focal scan!!!!
            startPos = str2double(o.focalScanner.fig.startPos.String);
            endPos = str2double(o.focalScanner.fig.endPos.String);
            nSteps = o.focalScanner.fig.nSteps.Value;
            if isnan(startPos) || isnan(endPos)
                o.focalScanner.fig.startButton.String = 'Start scan';
                return; 
            end
            scanPositions = round(linspace(startPos, endPos, nSteps));
            nImages = o.focalScanner.fig.nImages.Value;
            motorAddress = o.focalScanner.fig.motorAddress.String;
            cameraAddress = o.focalScanner.fig.cameraAddress.String;
            cal = str2double(o.focalScanner.fig.stepsPermm.String);
            
            % Set up the place to save data to:
            o.focalScanner.dat.images = cell(size(scanPositions));
            % And set up the axes
            cla(o.focalScanner.fig.ax);
            o.focalScanner.fig.hData = [];
            % And the autosaving...
            autoSave = o.focalScanner.fig.autoSave.Value;
            autoSaveFol = o.focalScanner.fig.autoSavePath.String;
            if autoSave
                % Try to make the folder
                if ~exist(autoSaveFol, 'dir')
                    try
                        mkdir(autoSaveFol);
                    catch err
                        autoSave = 0;
                    end
                end
            end
            
            % Now start the scan. First move the motor to the start. If it
            % does not like it, abort!
            vv = doocsread([motorAddress '/POS']);
            if ~isempty(vv.error)
                o.focalScanner.fig.startButton.String = 'Start scan';
                return; 
            end
            
            for i=1:length(scanPositions)
                if ~o.focalScanner.fig.startButton.Value; break; end
                scanPos = scanPositions(i);
                % Set the stage 'soll' position:
                ss = doocswrite([motorAddress '/POS.SET'], scanPos);
                pause(0.2);
                for k=1:2000
                    vv = doocsread([motorAddress '/POS.SET']);
                    if vv.data == scanPos || ~o.focalScanner.fig.startButton.Value 
                        break; 
                    end
                    pause(0.1);
                end
                % Now move the motor!
                ss = doocswrite([motorAddress '/CMD'], 1);
                o.focalScanner.fig.motorStatus.String = 'Motor moving';
                for k=1:2000
                    vv = doocsread([motorAddress '/POS']);
                    if vv.data == scanPos || ~o.focalScanner.fig.startButton.Value 
                        break; 
                    end
                    pause(0.1);
                end
                % We are in the correct position: take images!
                this_im = [];
                o.focalScanner.fig.motorStatus.String = 'Motor idle';
                for im=1:nImages
                    camdat = doocsread([cameraAddress '/IMAGE_EXT_ZMQ']);
                    if ~isempty(camdat.error) || ~o.focalScanner.fig.startButton.Value 
                        %o.focalScanner.fig.startButton.String = 'Start scan';
                        break; 
                    end;
                    fprintf('Got image %i\n', im);
                    o.dat.raw = camdat.data.val_val;
                    this_im(:,:,im) = o.dat.raw;
                    o.plotim;
                    o.plotScanStep(scanPos/cal);
                    if autoSave
                        fname = sprintf('%s_%i.png', datestr(now, 'YYYYmmDDThhMMss.FFF'), scanPos);
                        imwrite(o.dat.raw, fullfile(autoSaveFol, fname), 'PNG');
                    end
                    pause(0.02);
                end
                o.focalScanner.dat.images{i} = this_im;
            end
            
            % Change the name back :)
            set(o.focalScanner.fig.startButton, 'String', 'Start scan', 'Value', 0);
        end
        
        function fitFocalScan(o,~,~)
            
        end
        
        function plotScanStep(o,pos,~)
            ell = o.specs.ell{2};
            ydat = [ell.long_axis ell.short_axis]*o.dat.calib*0.5;
            if isempty(o.focalScanner.fig.hData)
                o.focalScanner.fig.hData(1) = plot(o.focalScanner.fig.ax, pos, ydat(1), 'or');
                o.focalScanner.fig.hData(2) = plot(o.focalScanner.fig.ax, pos, ydat(2), 'xk');
            else
                for i=1:2
                    yd = get(o.focalScanner.fig.hData(i), 'YData');
                    xd = get(o.focalScanner.fig.hData(i), 'XData');
                    set(o.focalScanner.fig.hData(i), 'XData', [xd, pos], 'YData', [yd, ydat(i)]);
                end
            end
        end
        %% Final stuff
        function delete(o)
            savestate(o,1);
            try
                close(7173);
            catch
            end
        end
    end
end
