classdef main < uiextrasX.VBox
    % Analyse focal spots and whatnot
    % K Poder, IC, 2014
    
    properties
        params
        focim
        paths
        specs
        allfiles
        disp
    end
    
    methods
        function o = main(varargin)
            if (nargin==0) Parent = figure(7070);
            elseif (nargin==1) Parent = varargin{1}; end;
            o@uiextrasX.VBox('Parent', Parent);
            defer(o)
            mainbox = o;
            if strcmp(get(Parent, 'Type'), 'figure')
                set(o.Parent, 'NumberTitle', 'off', 'Name', 'Focal spot profiler');
                if (ismac)
                    set(o.Parent, 'DefaultUIControlFontSize', 14, 'DefaultAxesFontSize', 14);
                elseif ispc
                    set(o.Parent, 'DefaultUIControlFontSize', 11, 'DefaultAxesFontSize', 10);
                end
                set(o.Parent, 'Units', 'Pixels', 'Position', [500 200 600 500]);
            end
            o.disp.hbox1 = uiextrasX.HBox('Parent', mainbox);
            %The column to have all controls etc
            o.disp.vboxL = uiextrasX.VBox('Parent', o.disp.hbox1);
            sb = uiextrasX.Panel('Parent',o.disp.vboxL,'Title','Settings','Padding',5);
            sbox = uiextrasX.VBox('Parent', sb);
            o.params.Ebox = editwithlabel('Energy', '10', sbox);
            set(o.params.Ebox, 'Callback', @(obj,val)recalc(o,1));
            o.params.taubox = editwithlabel('Tau', '37', sbox);
            set(o.params.taubox, 'Callback', @(obj,v)recalc(o,1));
            %hbox = uiextrasX.HBox('Parent', sbox);
            %uicontrol('Style', 'text', 'String', 'Cal (um/pix)', 'Parent', hbox);
            %o.params.cal = uicontrol('Style', 'edit', 'String', '1', 'Parent', hbox,'Callback', @(obj,v)changecal(o,1));
            o.params.cal = editwithlabel('Cal (um/pix)', '1', sbox);
            set(o.params.cal, 'Callback', @(obj,v)changecal(o,1));
            o.params.fno = editwithlabel('F/#', '20', sbox);
            set(o.params.fno, 'Callback', @(obj,val)recalc(o,0));
            o.params.lambda = editwithlabel('lambda', '800', sbox);
            set(o.params.lambda, 'Callback', @(obj,val)recalc(o,0));
            
            hpan = uiextrasX.Panel('Parent',o.disp.vboxL,'Title','Folder:','Padding',1);
            vbox = uiextrasX.VBox('Parent', hpan);  hbox = uiextrasX.HBox('Parent', vbox);
            o.paths.curfolfield = uicontrol('style', 'text', 'String', '.', 'Parent', hbox);
            o.paths.files = uicontrol('style', 'listbox', 'String', dir_names('.'), 'Parent', vbox, 'Callback', @(obj,v)set_file(o,get(o.paths.files, 'Value')));
            o.paths.fol = pwd;
            vbox.Sizes = [30 -1];
            savestate(o,0);
            
            sb = uiextrasX.Panel('Parent',o.disp.vboxL,'Title','Utilities','Padding',5);
            vbox = uiextrasX.VBox('Parent', sb);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'pushbutton', 'Parent', hbox, 'String', 'Set burn area', ...
                'Callback', @(obj,val)setburn(o,1));
            uicontrol('Style', 'pushbutton', 'Parent', hbox, 'String', 'Clear burn', ...
                'Callback', @(obj,val)setburn(o,0));
            uicontrol('Style', 'pushbutton', 'Parent', vbox, 'String', 'All in folder', ...
                'Callback', @(obj,e)openallfiles(o));
            o.paths.filter = uicontrol('Style', 'checkbox', 'String', 'Median filter', ...
                'Parent', vbox);
            %The axes column
            o.disp.vboxR = uiextrasX.VBox('Parent', o.disp.hbox1);
            %sb = uiextrasX.Panel('Parent',vboxR,'Title','Spot image:','Padding',5);
            sb = uipanel('Parent', o.disp.vboxR, 'Title', 'Spot image:');
            axes_menu=AxisScaleContextMenu(o.Parent);
            o.focim.raw = axes('Parent', sb,'UIContextMenu',axes_menu.menu);
            o.focim.data = rand(100);
            o.focim.burncount = 0;
            o.focim.burns = zeros(4,10);
            imagesc(o.focim.data, 'Parent', o.focim.raw);
            o.focim.cb = colorbar('Peer', o.focim.raw, 'Location', 'Southoutside');
            %set_axes_grid([o.focim.raw o.focim.cb], [nan, -1, nan], [nan, -1, nan, 10,nan], sb);
            
            sb = uiextrasX.Panel('Parent',o.disp.vboxR,'Title','Spot parameters:','Padding',5);
            grid = uiextrasX.Grid('Parent', sb);
            uicontrol('Style', 'text', 'String', 'Plot', 'Parent', grid);
            o.specs.ee.col = 'r'; o.specs.ee.ell = []; o.specs.ee.circ = [0 0 0];
            o.specs.e.col = 'g';  o.specs.e.ell = []; o.specs.e.circ = [0 0 0];
            o.specs.fwhm.col = 'k'; o.specs.fwhm.ell = [];
            o.specs.ee.control = uicontrol('Style', 'togglebutton', 'String', '1/e^2', ...
                'Parent', grid, 'Callback', @(obj,v)replot(o,get(o.specs.ee.control, 'Value')));
            o.specs.e.control = uicontrol('Style', 'togglebutton', 'String', '1/e', ...
                'Parent', grid, 'Callback', @(obj,v)replot(o,get(o.specs.e.control, 'Value')));
            o.specs.fwhm.control = uicontrol('Style', 'togglebutton', 'String', 'fwhm', ...
                'Parent', grid, 'Callback', @(obj,v)replot(o,get(o.specs.fwhm.control, 'Value')));
            uicontrol('Style', 'text', 'String', 'Major', 'Parent', grid);
            o.specs.ee.majoraxis = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            o.specs.e.majoraxis = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            o.specs.fwhm.majoraxis = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'Minor', 'Parent', grid);
            o.specs.ee.minoraxis = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            o.specs.e.minoraxis = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            o.specs.fwhm.minoraxis = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'r', 'Parent', grid);
            o.specs.ee.radius = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            o.specs.e.radius = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            o.specs.fwhm.radius = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'r_t', 'Parent', grid);
            o.specs.ee.idealR = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            o.specs.e.idealR = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            o.specs.fwhm.idealR = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'E/E0', 'Parent', grid);
            o.specs.ee.energy = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            o.specs.e.energy = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            o.specs.fwhm.energy = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            grid.ColumnSizes = [40 40 40 40 40 40];
            recalc(o,0);
            o.disp.hbox1.Sizes = [200 400];
            o.disp.vboxL.Sizes = [130 250 100];
            o.disp.vboxR.Sizes = [380 100];
            resume(o)
            %setSmartUISize(o);
            set(o.Parent, 'ResizeFcn', @(obj,val)resizefunc(o,1));
            assignin('base', 'FocusProfile', o);
        end
        
        
        function set_file(o,val)
            if (val == -1)
                dirlist = dir(pwd);
                dlist = cell(1,length(dirlist));
                for i=1:length(dirlist)
                    dlist{i} = dirlist(i).name;
                end
                set(o.paths.files, 'String', dlist, 'Value', 1, 'ListboxTop', 1);
                %set(o.path.curfolfield, 'String', textwrap(o.paths.fol));
                return;
            elseif (val == -2)
                dirlist = dir(o.paths.fol);
                dlist = cell(1,length(dirlist));
                for i=1:length(dirlist)
                    dlist{i} = dirlist(i).name;
                end
                set(o.paths.files, 'String', dlist, 'Value', 1, 'ListboxTop', 1);
                %set(o.path.curfolfield, 'String', textwrap(o.paths.fol));
                return;
            end
            % If double click
            if strcmp(get(o.Parent,'SelectionType'),'open')
                index = val;
                dlist = get(o.paths.files,'String');
                cursel = [o.paths.fol '/' dlist{index}];
                cursel = GetFullPath(cursel);
                if (isdir(cursel))
                    o.paths.fol = cursel;%[o.paths.fol '/' dlist{index}]
                    dirlist = dir(o.paths.fol);
                    dlist = cell(1,length(dirlist));
                    for i=1:length(dlist)
                        dlist{i} = dirlist(i).name;
                    end
                    set(o.paths.files, 'String', dlist, 'Value', 1, 'ListboxTop', 1);
                    [~,k,~] = fileparts(o.paths.fol);
                    set(o.paths.curfolfield, 'String', k);
                else
                    try
                        o.focim.data = o.imread(cursel);
                    catch
                        disp('Error reading image.');
                        p = focreaderror(o, cursel);
                        if p.success
                            o.focim.data = p.data;
                        else
                            return;
                        end
                    end
                    if (o.focim.burncount ~= 0)
                        for i=1:o.focim.burncount
                            coor = o.focim.burns(:,i);
                            o.focim.data(coor(2):coor(2)+coor(4),coor(1):coor(1)+coor(3)) = 0;
                        end
                    end
                    plotim(o, o.focim.data);
                    replot(o,1);
                end
            end
        end
        
        function plotim(o, im)
            
            %axes(o.focim.raw);
            calval = str2double(get(o.params.cal, 'String'));
            yax = calval*(1:size(im,1));
            xax = calval*(1:size(im,2));
            imagesc(xax,yax,im, 'Parent', o.focim.raw);
            axis(o.focim.raw, 'image');
            o.focim.cb = colorbar('Peer', o.focim.raw, 'Location', 'Southoutside');
            drawnow;
            % For anaysis, do bg subtraction. Take edges by default.
            [ym, xm] = size(im); BGl = 25;
            bg1 = mean(im(1:BGl,:));
            bg2 = mean(im(ym-BGl:ym,:));
            bg3 = mean(im(:,1:BGl));
            bg4 = mean(im(:,xm-BGl:xm));
            bg = mean([bg1 bg2 bg3 bg4]);
            imc = double(im) - bg;
            % find contours for different radii
            immax = max(imc(:));
            contvals = immax*[exp(-2) exp(-2); exp(-1) exp(-1); 0.5 0.5];
            rs = {'ee', 'e', 'fwhm'};
            for k=1:3
                C = contourc(imc, contvals(k,:));
                x = C(1,:);
                y = C(2,:);
                lengthx = length(x);
                i = 1;
                %Remove points from your contour that are not actually part of the main
                %contour - this is necessary when looking at noisy data
                while i < lengthx
                    if(abs(x(i) - mean(x)) > 2*std(x) || abs(y(i) - mean(y)) > 2*std(y))
                        x(i) = [];
                        y(i) = [];
                        lengthx = lengthx - 1;
                    end
                    i = i+1;
                end
                % Fit the ellipse
                ell = fit_ellipse(x,y);
                if ~isempty(ell.status)
                    disp('No ellipse! Try setting burn areas.');
                    return;
                end
                o.specs.(rs{k}).ell = ell;
                set(o.specs.(rs{k}).majoraxis, 'String', sprintf('%3.1f',o.specs.(rs{k}).ell.long_axis*calval*0.5));
                set(o.specs.(rs{k}).minoraxis, 'String', sprintf('%3.1f',o.specs.(rs{k}).ell.short_axis*calval*0.5));
                set(o.specs.(rs{k}).radius, 'String', sprintf('%3.1f',0.5*sqrt(o.specs.(rs{k}).ell.long_axis*o.specs.(rs{k}).ell.short_axis)*calval));
                % Find the energy content - stolen form Juffalo's script
                insidefwhm = 0;
                for x = 1:length(imc(1,:))
                    for y = 1:length(imc(:,1))
                        xr = (x - ell.X0_in)*cos(ell.phi) - (y - ell.Y0_in)*sin(ell.phi);
                        yr = (x - ell.X0_in)*sin(ell.phi) + (y - ell.Y0_in)*cos(ell.phi);
                        testinside = xr^2/(ell.a^2) + yr^2/(ell.b^2);
                        if testinside < 1
                            insidefwhm = insidefwhm + double(imc(y,x));
                        end
                    end
                end
                set(o.specs.(rs{k}).energy, 'String', sprintf('%2.1f', 100*insidefwhm/sum(imc(:))));
            end
        end
        
        function replot(o,val)
            rs = {'ee', 'e', 'fwhm'};
            calval = str2double(get(o.params.cal, 'String'));
            for k=1:3
                if get(o.specs.(rs{k}).control, 'Value')
                    ell = o.specs.(rs{k}).ell;
                    hold(o.focim.raw, 'on');
                    R = [cos(ell.phi) sin(ell.phi); -sin(ell.phi) cos(ell.phi)];
                    % the axes
                    ver_line        = [ [ell.X0 ell.X0]; ell.Y0+ell.b*[-1 1] ];
                    horz_line       = [ ell.X0+ell.a*[-1 1]; [ell.Y0 ell.Y0] ];
                    new_ver_line    = calval*R*ver_line;
                    new_horz_line   = calval*R*horz_line;
                    % the ellipse
                    theta_r         = linspace(0,2*pi);
                    ellipse_x_r     = ell.X0 + ell.a*cos( theta_r );
                    ellipse_y_r     = ell.Y0 + ell.b*sin( theta_r );
                    rotated_ellipse = calval*R * [ellipse_x_r;ellipse_y_r];
                    
                    % draw
                    plot(o.focim.raw, new_ver_line(1,:),new_ver_line(2,:), o.specs.(rs{k}).col);
                    plot(o.focim.raw, new_horz_line(1,:),new_horz_line(2,:), o.specs.(rs{k}).col);
                    plot(o.focim.raw, rotated_ellipse(1,:),rotated_ellipse(2,:), o.specs.(rs{k}).col);
                    hold(o.focim.raw, 'off');
                else
                    circs = findobj(o.focim.raw, 'Type', 'Line', 'Color', o.specs.(rs{k}).col);
                    for i=1:length(circs)
                        delete(circs(i));
                    end
                end
            end
        end
        
        function changecal(o,val)
            calval = str2double(get(o.params.cal, 'String'));
            im = o.focim.data;
            xax = calval*(1:size(im,2));
            yax = calval*(1:size(im,1));
            axes(o.focim.raw);
            imagesc(xax,yax,im);
            axis image;
            rs = {'ee', 'e', 'fwhm'};
            for k=1:3
                set(o.specs.(rs{k}).control, 'Value', 0);
                set(o.specs.(rs{k}).majoraxis, 'String', sprintf('%3.1f',o.specs.(rs{k}).ell.long_axis*calval*0.5));
                set(o.specs.(rs{k}).minoraxis, 'String', sprintf('%3.1f',o.specs.(rs{k}).ell.short_axis*calval*0.5));
                set(o.specs.(rs{k}).radius, 'String', sprintf('%3.1f',0.5*sqrt(o.specs.(rs{k}).ell.long_axis*o.specs.(rs{k}).ell.short_axis)*calval));
            end
        end
        
        function savestate(o,val)
            fol = fileparts(which('FocusProfiler.main'));
            compname = getComputerName;
            if val %save state
                en = get(o.params.Ebox, 'String');
                tau = get(o.params.taubox, 'String');
                calval = get(o.params.cal, 'String');
                fno = get(o.params.fno, 'String');
                lambda = get(o.params.lambda, 'String');
                fid = fopen([fol '/state_' compname '.txt'], 'w');
                fprintf(fid, '%s,%s\n\r', 'Energy', en);
                fprintf(fid, '%s,%s\n\r', 'Tau', tau);
                fprintf(fid, '%s,%s\n\r', 'Calibration', calval);
                fprintf(fid, '%s,%s\n\r', 'Folder', o.paths.fol);
                fprintf(fid, '%s,%s\n\r', 'Fnumber', fno);
                fprintf(fid, '%s,%s\n\r', 'lambda', lambda);
                fclose(fid);
            else % recall
                if (exist([fol '/state_' compname '.txt'], 'file'))
                    fil = getFileText([fol '/state_' compname '.txt']);
                    ind = strfind(fil{1}, ',') + 1;
                    set(o.params.Ebox, 'String', fil{1}(ind:end));
                    ind = strfind(fil{2}, ',') + 1;
                    set(o.params.taubox, 'String', fil{2}(ind:end));
                    ind = strfind(fil{3}, ',') + 1;
                    set(o.params.cal, 'String', fil{3}(ind:end));
                    ind = strfind(fil{4}, ',') + 1;
                    o.paths.fol = fil{4}(ind:end);
                    ind = strfind(fil{5}, ',') + 1;
                    set(o.params.fno, 'String', fil{5}(ind:end));
                    ind = strfind(fil{6}, ',') + 1;
                    set(o.params.lambda, 'String', fil{6}(ind:end));
                    set_file(o,-2);
                else
                    set_file(o,-1);
                end
            end
        end
        
        function recalc(o, val)
            %recalculate intensity/theoretical spot size
            if val %intensity
                
            else %spot size
                fno = str2double(get(o.params.fno, 'String'));
                lambda = str2double(get(o.params.lambda, 'String'))*1e-3;
                tlim = [2.1 1.5 1.03]*lambda*fno;
                rs = {'ee', 'e', 'fwhm'};
                for i=1:3
                    set(o.specs.(rs{i}).idealR, 'String', sprintf('%2.1f',tlim(i)));
                end
            end
        end
        
        function setburn(o,val)
            if val
                figure(7172);
                imagesc(o.focim.data);
                burn = getrect(7172);
                i = o.focim.burncount+1;
                % Make sure burns are limited to image size.
                if round(burn(1))<1 burn(1) = 1; end;
                if round(burn(2))<1 burn(2) = 1; end;
                if round(burn(3))+round(burn(1))>size(o.focim.data,2) burn(3) = size(o.focim.data,2)-round(burn(1)); end;
                if round(burn(4))+round(burn(2))>size(o.focim.data,1) burn(4) = size(o.focim.data,1)-round(burn(2)); end;
                o.focim.burns(:,i) = round(burn);
                o.focim.data(round(burn(2)):round(burn(2)+burn(4)), round(burn(1)):round(burn(1)+burn(3))) = 0;
                close(7172);
                plotim(o, o.focim.data);
                o.focim.burncount = i;
            else
                o.focim.burncount = 0;
                o.focim.burns = zeros(4,10);
            end
        end
        
        function imdata = imread(o, filen)
            [~,~,ext] = fileparts(filen);
            switch ext
                case '.tif'
                    imdata = imread(filen);
                case '.raw'
                    % Have to figure out the size!
                    % Trying this with usual sizes, or 640x480, 1280x1024,
                    % 1088x2048
                    ff = dir(filen); ff.bytes;
                    switch ff.bytes
                        case 614400 % 640x480, AVT
                            imdata = ReadRAW16bit(filen, 640, 480);
                        case 2621440 % 1280x1024
                            imdata = ReadRAW16bit(filen, 1280, 1024, 1);
                        case 2457600
                            imdata = ReadRAW16bit(filen, 1280, 960);
                        case 4456448
                            imdata = ReadRAW16bit(filen, 2048, 1088, 1);
                    end
                otherwise
                    error('Unknown format....');
            end
            if get(o.paths.filter, 'Value')
                imdata = medfilt2(imdata);
            end
        end
        
        function p = focreaderror(o, filename)
            answ = questdlg('Failed to open image! Are you opening an AVT .raw image?', 'Error');
            if strcmpi(answ, 'Yes')
                answer = inputdlg({'Width', 'Height'}, 'Enter .raw file size', 1, {'640', '480'});
                try
                    p.data = ReadRAW16bit(filename,str2double(answer{1}), str2double(answer{2}));
                    p.success = 1;
                catch
                    disp('No luck. Try another file');
                    p.success = 0;
                    p.data = [];
                end
            else
                p.success = 0;
                p.data = [];
            end
        end
        
        function resizefunc(o,val)
            siz = (get(o.Parent, 'Position'));
            o.disp.hbox1.Sizes = [200 siz(3)-200];
            o.disp.vboxL.Sizes = [130 siz(4)-230 100];
            o.disp.vboxR.Sizes = [siz(4)-100 100];
        end
        
        function openallfiles(o)
            o.allfiles = make_allfiles(o);
        end
        
        function p = make_allfiles(o)
            p.figure = figure(7171); clf;
            set(7171, 'NumberTitle', 'off', 'Name', 'Focal spot profiler');
            if (ismac)
                set(7171, 'DefaultUIControlFontSize', 14, 'DefaultAxesFontSize', 14);
            elseif ispc
                set(7171, 'DefaultUIControlFontSize', 11, 'DefaultAxesFontSize', 10);
            end
            set(7171, 'Units', 'Pixels', 'Position', [200 200 800 600]);
            %some filtering for filenames, then table with calculated
            %values, also averaged values, export button, cancel button,
            %and progress bar, also a plot of the results somehow
            hbox0 = uiextrasX.HBox('Parent', p.figure);
            %vbox = uiextrasX.VBox('Parent', hbox0);
            vboxL = uiextrasX.VBox('Parent', hbox0);
            vboxR = uiextrasX.VBox('Parent', hbox0);
            hbox0.Sizes = [250 -1];
            p0 = uiextrasX.Panel('Parent',vboxL,'Title','Settings','Padding',2);
            vbox = uiextrasX.VBox('Parent', p0);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('style', 'text', 'Parent', hbox, 'String', 'File filter');
            p.filefilter = uicontrol('style', 'edit', 'Parent', hbox, 'String', '*.png', 'Callback', @(obj,e)filefilter(o));
            hbox = uiextrasX.VBox('Parent', vbox);
            p.filefilterres = uicontrol('style', 'text', 'Parent', hbox, 'String', '0 files');
            p3 = uiextrasX.Panel('Parent', vbox, 'Title', 'Plot controls', 'Padding', 1);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('style', 'text', 'String', 'Image size for .raw files', 'Parent', hbox);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('style', 'text', 'String', 'width', 'Parent', hbox);
            p.width = uicontrol('style', 'edit', 'String', '640', 'Parent', hbox);
            uicontrol('style', 'text', 'String', 'height', 'Parent', hbox);
            p.height = uicontrol('style', 'edit', 'String', '480', 'Parent', hbox);
            hbox = uiextrasX.HBox('Parent', vbox);
            p.startbut = uicontrol('Parent', hbox, 'String', 'START', 'style', 'togglebutton', 'Callback', @(obj,val)doallfiles(o,get(o.allfiles.startbut, 'Value')));
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Parent', hbox, 'Style', 'pushbutton', 'String', 'Export figure', 'Callback', @(obj,e)exportfig(o));
            uicontrol('Parent', hbox, 'Style', 'pushbutton', 'String', 'Export data', 'Callback', @(obj,e)exportdata(o));
            vbox.Sizes = [30 30 -1 20 30 30 30];
            vbox = uiextrasX.VBox('Parent', p3); hbox = uiextrasX.HBox('Parent', vbox);
            p.cons.fwhm.plotany = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'Plot FWHM', 'Callback', @(obj,e)enableplot(o,1));
            hbox = uiextrasX.HBox('Parent', vbox);
            p.cons.fwhm.x = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'x', 'Callback', @(obj,s)replotone(o, 'xfwhm'));
            p.cons.fwhm.y = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'y', 'Callback', @(obj,s)replotone(o, 'yfwhm'));
            p.cons.fwhm.r = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'r', 'Callback', @(obj,s)replotone(o, 'rfwhm'));
            p.cons.fwhm.E = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'E/E0', 'Callback', @(obj,s)replotone(o, 'Efwhm'));
            hbox = uiextrasX.HBox('Parent', vbox); hbox = uiextrasX.HBox('Parent', vbox);
            p.cons.e.plotany = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'Plot 1/e', 'Callback', @(obj,e)enableplot(o,2));
            hbox = uiextrasX.HBox('Parent', vbox);
            p.cons.e.x = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'x', 'Callback', @(obj,s)replotone(o, 'xe'));
            p.cons.e.y = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'y', 'Callback', @(obj,s)replotone(o, 'ye'));
            p.cons.e.r = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'r', 'Callback', @(obj,s)replotone(o, 're'));
            p.cons.e.E = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'E/E0', 'Callback', @(obj,s)replotone(o, 'Ee'));
            hbox = uiextrasX.HBox('Parent', vbox); hbox = uiextrasX.HBox('Parent', vbox);
            p.cons.ee.plotany = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'Plot 1/ee', 'Callback', @(obj,e)enableplot(o,3));
            hbox = uiextrasX.HBox('Parent', vbox);
            p.cons.ee.x = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'x', 'Callback', @(obj,s)replotone(o, 'xee'));
            p.cons.ee.y = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'y', 'Callback', @(obj,s)replotone(o, 'yee'));
            p.cons.ee.r = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'r', 'Callback', @(obj,s)replotone(o, 'ree'));
            p.cons.ee.E = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'E/E0', 'Callback', @(obj,s)replotone(o, 'Eee'));
            p1 = uipanel('Parent', vboxR, 'Title', 'Results plot');
            hbox = uiextrasX.HBox('Parent', p1);
            p.plot = axes('Parent', hbox);
            p2 = uipanel('Parent', vboxR, 'Title', 'Results data');
            hbox = uiextrasX.VBox('Parent', p2);
            grid = uiextrasX.Grid('Parent', hbox);
            uicontrol('Style', 'text', 'String', '', 'Parent', grid);
            uicontrol('Style', 'text', 'String', '1/e^2', 'Parent', grid);
            uicontrol('Style', 'text', 'String', '1/e', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'FWHM', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'x', 'Parent', grid);
            p.specs.ee.x = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.x = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.x = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'var(x)', 'Parent', grid);
            p.specs.ee.xv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.xv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.xv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'y', 'Parent', grid);
            p.specs.ee.y = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.y = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.y = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'var(y)', 'Parent', grid);
            p.specs.ee.yv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.yv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.yv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'r', 'Parent', grid);
            p.specs.ee.r = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.r = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.r = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'var(r)', 'Parent', grid);
            p.specs.ee.rv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.rv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.rv = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'r_t', 'Parent', grid);
            p.specs.ee.idealR = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.idealR = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.idealR = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'E/E0', 'Parent', grid);
            p.specs.ee.E = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.E = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.E = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            uicontrol('Style', 'text', 'String', 'var(E/E0)', 'Parent', grid);
            p.specs.ee.Ev = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.e.Ev = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            p.specs.fwhm.Ev = uicontrol('Style', 'text', 'String', '0', 'Parent', grid);
            
            grid.ColumnSizes = [50 40 40 40 40 40 40 40 40 60];
            hbox2 = uiextrasX.HBox('Parent', hbox);
            uicontrol('Style', 'text', 'Parent', hbox2, 'String', 'Stability');
            hbox2 = uiextrasX.HBox('Parent', hbox);
            p.stabinfo = uicontrol('Style', 'text', 'Parent', hbox2, 'String', 'x: y:');
            recalc(o,0);
            hbox.Sizes = [-1 20 20];
            vboxR.Sizes = [-1 150];
            % Plot symbols
            p.cons.x.sym = 'x';
            p.cons.y.sym = 's';
            p.cons.r.sym = 'o';
            p.cons.E.sym = 'd';
        end
        
        function doallfiles(o,val)
            flist = filefilter(o);
            
            if val
                set(o.allfiles.startbut, 'String', 'STOP');
                rs = {'fwhm', 'e', 'ee'};
                ls = {'x', 'y', 'r', 'E'};
                temp = plot(o.allfiles.plot,0,0,'.'); hold on;
                o.allfiles.legdata.hs = zeros(1,12); o.allfiles.legdata.labs = cell(1,12);
                for i=1:3
                    for k=1:4
                        o.allfiles.data.(rs{i}).(ls{k}) = NaN(1,length(flist));
                        
                        o.allfiles.plots.(rs{i}).(ls{k}) = plot(o.allfiles.plot, 1:length(flist),NaN(1,length(flist)),...
                            [o.specs.(rs{i}).col o.allfiles.cons.(ls{k}).sym]);
                        addlistener(o.allfiles.plots.(rs{i}).(ls{k}), 'YData', 'PostSet', @(src, evnt) set(evnt.AffectedObject.Parent, 'DataAspectRatioMode', 'auto'));
                        if get(o.allfiles.cons.(rs{i}).(ls{k}), 'Value')
                            o.allfiles.legdata.hs((i-1)*4+k) = o.allfiles.plots.(rs{i}).(ls{k});
                        end
                        o.allfiles.legdata.labs{(i-1)*4+k} = [ls{k} ', ' rs{i}];
                    end
                end
                hold off; delete(temp);
                xlabel('Shot number');
                ylabel({'Spot size / \mu m', 'Energy within parameter / %'});
                o.allfiles.data.centroids = zeros(length(flist),2);
                o.allfiles.leg = legend(o.allfiles.plot, o.allfiles.legdata.hs(o.allfiles.legdata.hs~=0),...
                    o.allfiles.legdata.labs(o.allfiles.legdata.hs~=0), 'Location', 'eastoutside');
            else
                set(o.allfiles.startbut, 'String', 'START');
                return;
            end
            
            for l=1:length(flist);
                if ~get(o.allfiles.startbut, 'Value')
                    break;
                end
                fprintf('Analysing file %3i: %s\n', l, flist(l).name);
                try
                    o.focim.data = o.imread([o.paths.fol '/' flist(l).name]);
                catch
                    fprintf('imread failed. Trying ReadRAW16bit.\n');
                    width = str2double(get(o.allfiles.width, 'String'));
                    height = str2double(get(o.allfiles.height, 'String'));
                    try
                        o.focim.data = ReadRAW16bit([o.paths.fol '/' flist(l).name], width, height);
                    catch
                        fprintf('ReadRAW16bit failed as well. Not sure we can do this file.\n');
                    end
                end
                if (o.focim.burncount ~= 0)
                    for i=1:o.focim.burncount
                        coor = o.focim.burns(:,i);
                        o.focim.data(coor(2):coor(2)+coor(4),coor(1):coor(1)+coor(3)) = 0;
                    end
                end
                plotim(o, o.focim.data);
                replot(o,1);
                replotsummary(o, l);
                recalcsummary(o, l);
            end
            if l==length(flist)
                set(o.allfiles.startbut, 'String', 'START', 'Value', 0);
            end
        end
        
        function exportfig(o)
            figure(7173); clf;
            destax = axes('Parent', 7173);
            copyobj(findobj(o.allfiles.plot, 'Type', 'line'), destax);
            hs = findobj(destax, 'Type', 'line');
            xlabel(destax, 'Shot number');
            ylabel(destax, {'Spot size / \mu m', 'Energy within parameter / %'});
            l = 1;
            while sum(~isnan(get(hs(l), 'YData')))==0 && l<13
                l = l+1;
            end
            set(destax, 'XLim', [0.5 sum(~isnan(get(hs(l), 'YData')))], 'Box', 'on');
            legend(destax, o.allfiles.legdata.hs(o.allfiles.legdata.hs~=0),...
                o.allfiles.legdata.labs(o.allfiles.legdata.hs~=0), 'Location', 'eastoutside');
            setAllFonts(7173,16);
            files = {'pdf'; 'png'};
            [fname,paths,fext] = uiputfile(files,'Save plot as', [o.paths.fol '/']);
            if isequal(fname,0) || isequal(paths,0) || isequal(fext,0)
                return;
            end
            export_fig(fullfile(paths, fname), '-nocrop', '-transparent', ['-' files{fext}], 7173);
        end
        
        function exportdata(o)
            if isfield(o.allfiles, 'data')
                l = length(o.allfiles.data.e.x);
                dat = NaN(14,l);
                rs = {'ee', 'e', 'fwhm'};
                ls = {'x', 'y', 'r', 'E'};
                in = 1;
                for i=1:3
                    for k=1:4
                        dat(in,:) = o.allfiles.data.(rs{i}).(ls{k});
                        in = in+1;
                    end
                end
                dat(13:14,:) = o.allfiles.data.centroids';
                assignin('base', 'focdata', dat);
            else
                return
            end
        end
        
        function replotsummary(o, nshot)
            calval = str2double(get(o.params.cal, 'String'));
            rs = {'ee', 'e', 'fwhm'};
            ls = {'x', 'y', 'r', 'E'};
            for i=1:3
                o.allfiles.data.(rs{i}).x(nshot) = o.specs.(rs{i}).ell.long_axis*calval*0.5;
                o.allfiles.data.(rs{i}).y(nshot) = o.specs.(rs{i}).ell.short_axis*calval*0.5;
                o.allfiles.data.(rs{i}).r(nshot) = 0.5*sqrt(o.specs.(rs{i}).ell.long_axis*o.specs.(rs{i}).ell.short_axis)*calval;
                o.allfiles.data.(rs{i}).E(nshot) = str2double(get(o.specs.(rs{i}).energy, 'String'));
                for k=1:4
                    if ~get(o.allfiles.cons.(rs{i}).(ls{k}), 'Value')
                        continue;
                    end
                    set(o.allfiles.plots.(rs{i}).(ls{k}), 'YData', o.allfiles.data.(rs{i}).(ls{k}));%,...
                    % 'XData', [1:nshot]);
                    drawnow;
                end
            end
            o.allfiles.data.centroids(nshot,:) = calval*centroidloc(o);
            set(o.allfiles.plot, 'XLim', [0.5 nshot+0.5]);
        end
        
        function recalcsummary(o, nshots)
            rs = {'ee', 'e', 'fwhm'};
            ls = {'x', 'y', 'r', 'E'};
            for i=1:3
                for k=1:4
                    set(o.allfiles.specs.(rs{i}).(ls{k}), 'String', num2str(mean(o.allfiles.data.(rs{i}).(ls{k})(1:nshots)), '%2.1f'));
                    set(o.allfiles.specs.(rs{i}).([ls{k} 'v']), 'String', num2str(sqrt(var(o.allfiles.data.(rs{i}).(ls{k})(1:nshots))), '%2.1f'));
                end
            end
            set(o.allfiles.stabinfo, 'String', ...
                sprintf('var(x_m) = %2.1f; var(y_m) = %2.1f', sqrt(var(o.allfiles.data.centroids(1:nshots,:)))));
            
        end
        
        function replotone(o,st)
            if (isfield(o.allfiles, 'plots'))
                if isfield(o.allfiles.plots, st(2:end))
                    hp = o.allfiles.plots.(st(2:end)).(st(1));
                else
                    return;
                end
                if get(o.allfiles.cons.(st(2:end)).(st(1)), 'Value')
                    set(hp, 'YData', o.allfiles.data.(st(2:end)).(st(1)));
                else
                    set(hp, 'YData', NaN(size(get(hp, 'XData'))));
                end
                rs = {'fwhm', 'e', 'ee'};
                ls = {'x', 'y', 'r', 'E'};
                lims = zeros(2,12);
                for i=1:3
                    for k=1:4
                        lims(1,(i-1)*4+k) = min(get(o.allfiles.plots.(rs{i}).(ls{k}), 'Ydata'));
                        lims(2,(i-1)*4+k) = max(get(o.allfiles.plots.(rs{i}).(ls{k}), 'Ydata'));
                        o.allfiles.legdata.hs((i-1)*4+k) = o.allfiles.plots.(rs{i}).(ls{k})*get(o.allfiles.cons.(rs{i}).(ls{k}), 'Value');
                    end
                end
                o.allfiles.leg = legend(o.allfiles.plot, o.allfiles.legdata.hs(o.allfiles.legdata.hs~=0),...
                    o.allfiles.legdata.labs(o.allfiles.legdata.hs~=0), 'Location', 'eastoutside');
                set(o.allfiles.plot, 'YLim', [min(lims(1,:)) max(lims(2,:))]);
            else
                return
            end
        end
        
        function centroidlocs = centroidloc(o)
            I = o.focim.data;
            [rows cols] = size(I);
            IMAGE = double(I);
            %Define BG level, under which pixels will  not count towards centroid.
            BGfac = 0.5;
            maxval = max(max(IMAGE));
            BG = maxval*BGfac;
            IMAGE(IMAGE<BG)=0;
            %Find centroid by weighting pixels by intensity
            x = ones(rows,1)*[1:cols];
            y = [1:rows]'*ones(1,cols);
            totcounts = sum(sum(IMAGE));
            xbar = sum(sum(IMAGE.*x))/totcounts;
            ybar = sum(sum(IMAGE.*y))/totcounts;
            centroidlocs(1) = xbar;
            centroidlocs(2) = ybar;
            calval = str2double(get(o.params.cal, 'String'));
            centroidlocs = centroidlocs*calval;
        end
        
        function filelist = filefilter(o)
            filt = get(o.allfiles.filefilter, 'String');
            flist = dir([o.paths.fol '/' filt]);
            set(o.allfiles.filefilterres, 'String', [num2str(length(flist)) ' files']);
            filelist = flist;
        end
        
        function enableplot(o,val)
            % val: 1 = FWHM; 2=1/e; 3=1/ee;
            rs = {'fwhm', 'e', 'ee'};
            vals = get(o.allfiles.cons.(rs{val}).plotany, 'Value');
            ls = {'x', 'y', 'r', 'E'};
            for i=1:4
                set(o.allfiles.cons.(rs{val}).(ls{i}), 'Value', vals);
                replotone(o, [ls{i} rs{val}]);
            end
        end
        
        function delete(o)
            savestate(o,1);
            try
                close(7171);
            catch
            end
        end
    end
end
