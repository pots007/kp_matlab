% test_focusprofiler_a0calc.m
%
% Check if the a0 value that my tool calculates is what I think it is...

a0 = 4;
lambda0 = 800e-9;
omega0 = 2 * pi * c / lambda0;
tau = 30e-15;  % FWHM intensity duration
sigma_E = tau / sqrt(2*log(2));  % The electric field 1/e duration
w0 = 10e-6;  % Laser beam waist
wI = w0 / sqrt(2);  % Laser intensity 1/e width

E0 = a0 * me * c * omega0 / qe;
fprintf('\nPeak a0: %1.1f \n', a0)
fprintf('Peak electric field value: %1.2e V/m \n', E0)

% The camera chip will measure time averaged intensity
I0 = epsilon0 * c * E0^2 / 2;
fprintf('Time averaged peak intensity: %1.2e W/cm^2 \n', I0*1e-4)
fprintf('Laser waist size: %1.2f um\n', w0 * 1e6)
% Now make an image with this peak

dx = 0.3e-6;  % The spatial grid size
dt = 1e-15;    % The temporal grid size
x_ax = linspace(-150, 150, 301) * dx;
t_ax = linspace(-100, 100, 201) * dt;

% Make the 3D grid
[X, Y, T] = meshgrid(x_ax, x_ax, t_ax);

% Calculate the Electric field of the laser
E = exp(-X.^2 / w0 ^ 2) .* exp(-Y.^2 / w0 ^ 2) .* exp(-T.^2 / sigma_E ^ 2);
E = E0 .* E .* cos(T * omega0);

% Calculate total energy - integrate E^2
total_energy_int = epsilon0 * c * sum(E(:).^2) * dx * dx * dt;
total_energy_calc = 3.34 * I0 * wI^2 * tau;
fprintf('Total laser energy:\n')
fprintf('\t\tintegrated: %1.2f J\n', total_energy_int)
fprintf('\t\tcalculated: %1.2f J\n', total_energy_calc)

% And make a focal spot out of this: normalise to just shy of 16 bits...
focal_spot = sum(E .^ 2, 3);
focal_spot = uint16(focal_spot / max(focal_spot(:)) * 2^15);
imwrite(focal_spot, '~/test_focus.png')