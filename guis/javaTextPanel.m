% From undocumented Matlab, date chooser panel from JIDEsoft
% Ref: http://undocumentedmatlab.com/blog/date-selection-components
function h = javaTextPanel(Parent, varargin)
if ~exist('Parent','var')
    Parent=gcf;
end

% Display a DateChooserPanel
jPanel = javax.swing.JTextPane;

if feature('HGUsingMATLABClasses')
    [h,c] = javacomponent(jPanel, [10,10,200,200], (Parent));
else
    [h,c] = javacomponent(jPanel, [10,10,200,200], double(Parent));
end
set(h,varargin{:});
set(c,'Tag','javax.swing.JTextPane;','UserData',h);