% Class to monitor Gentecs output file and to write the last saved value
% into a file.

classdef GentecMonitor < uiextrasX.HBox
    
    properties
        par
        file
        monitor
    end
    
    methods
        function o = GentecMonitor()
            figure(555);
            o@uiextrasX.HBox('Parent', 555);
            set(555, 'Resize', 'off', 'Toolbar', 'none', 'MenuBar', 'none', 'Color', 'w',...
                'Position', [100 300 600 50], 'Name', 'Gentec file monitor', 'NumberTitle', 'off');
            defer(o);
            uicontrol('Parent', o, 'Style', 'text', 'String', 'File to monitor');
            o.file.monitor = uicontrol('Parent', o, 'Style', 'edit', 'String', 'C:\Data\2015Najmudin\ExitEnergy\energy.acq');
            o.file.size = [];
            o.file.time = [];
            o.monitor = ContinuousButton2(o);
            set(o.monitor.timer, 'Period', 1);
            addlistener(o.monitor, 'trigger', @(s,e)check_(o,1));
            o.Sizes = [100 -1 150];
            o.load(1);
            resume(o);
        end
        
        function check_(o,~)
            disp('lala');
            monf = dir(get(o.file.monitor, 'String'));
            if isempty(o.file.size)
                % If haven't looked at anything yet
                o.file.size = monf.bytes;
                o.file.time = monf.datenum;
                dataf = importdata(get(o.file.monitor, 'String'));
                o.write_(dataf.data(end));
            else
                if o.file.size == monf.size && o.file.time == monf.datenum
                    % If the file time and size are the same, do nothing
                    return;
                else
                    % But if they're not the same, read value and update
                    dataf = importdata(get(o.file.monitor, 'String'));
                    o.write_(dataf.data(end));
                end
            end
        end
        
        function write_(o,v)
            path_ = fileparts(get(o.file.monitor, 'String'));
            cc = clock;
            time_ext = sprintf('%i%02i%02i', cc(1),cc(2),cc(3)); 
            time_ext = sprintf('%s_%i_%i_%2.0f', time_ext, cc(4),cc(5),cc(6));
            fid = fopen([path_ '\shot_' time_ext '.txt'], 'W');
            fprintf(fid, '%f\n', v);
            fclose(fid);
        end
        
        function load(o,v)
            settfile = [getuserdir '\GentecMonitorState.txt'];
            if v
                % If 1, then try to load the path
                if exist(settfile, 'file')
                    fid = fopen(settfile, 'r');
                    str = fgetl(fid);
                    set(o.file.monitor, 'String', str);
                    fclose(fid);
                end
            else
                % Else write the path to file
                fid = fopen(settfile, 'W');
                fprintf(fid, '%s\n', get(o.file.monitor, 'String'));
                fclose(fid);
            end
        end
        
        function delete(o)
            o.load(0);
        end
    end
    
end