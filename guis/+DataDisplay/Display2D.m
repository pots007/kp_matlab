% Display2D.m
%
% Class to plot 2D data, such as spectra
%
% KP, DESY, 2017

classdef Display2D < DataDisplay.DisplayBase
    properties
        
    end
    
    methods
        function self = Display2D(ID)
            % This has already run the base class constructor.
            self = self@DataDisplay.DisplayBase(ID);
            hh = findobj(self.fig.parent, 'Title', 'Plot controls');
            vbox = get(hh, 'Children'); % This should return the vbox that will
            % contain the rest of the gui controls for plotting controls
            % The stuff for the x-axis:
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Colormap');
            self.fig.colormap = uicontrol('Style', 'popupmenu', 'Parent', hbox,...
                'String', {'none', '-', '--', '-.', ':'}, 'Callback', @self.setColormap);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'c min');
            self.fig.cmin = uicontrol('Style', 'edit', 'Parent', hbox,...
                'String', '0', 'Callback', @self.updateClims, 'Tag', 'min');
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'c max');
            self.fig.markerStyle = uicontrol('Style', 'edit', 'Parent', hbox,...
                'String', '100', 'Callback', @self.updateClims, 'Tag', 'max');
            self.fig.colorbar = uicontrol('Style', 'checkbox', 'Parent', hbox,...
                'String', 'Colorbar', 'Callback', @self.setColorbar);
            self.fig.highlightLastShot = uicontrol('Style', 'checkbox', ...
                'String', 'Highlight last shot', 'Parent', hbox);
            hbox.Sizes = [65 100 50 60 50 60 70 -1];
            
            % hbox for padding the stuff out
            uiextras.HBox('Parent', vbox);
            
            % Now set the heights, depending on what system we're on!
            nLines = length(get(vbox, 'Children'));
            vbox.Sizes = [30*ones(nLines-1,1); -1];
            % And the timestamps!
            self.xAxis.timestamp = 0;
            self.yAxis.timestamp = 0;
        end
        
        %% Actual data plotting
        function shotAnalysis(self)
            hPlot = findobj(self.fig.mainAxes, 'Tag', 'mainPlot');
            xdat = get(hPlot, 'XData');
            %ydat = get(hPlot, 'YData');
            try
                cdat = get(hPlot, 'CData');
            catch 
                cdat = [];
            end
            % -------------- And now figure out what we are asked to plot
            xv = get(self.xAxis.source, 'Value');
            if xv == 1 % x is shot number!
                xData = self.shotCount;
                xStat = 1;
            else
                [xData, xStat] = self.getDoocsData('x');
            end
            yv = get(self.yAxis.source, 'Value');
            if yv == 1 % y is the randi number
                yData = rand(1)*10;
                yStat = 1;
            else % We're getting data from DOOCS
                [yData, yStat] = self.getDoocsData('y');
                %self.yAxis.rawdata(end+1) = randi(1)*4;
            end
            % But if we didn't get data, stop now...
            if yStat < 0 || xStat < 0
                self.shotCount = self.shotCount - 1;
                return
            end
            
            % -----------------------  Function applying
            % We have the new for everything data, now let's try to apply the analysis
            % function!
            % Y-axis first
            [yVal, yStat, yAxis] = self.applyFunction('y', yData);
            % And now go through the x-function!
            [xVal, xStat] = self.applyFunction('x', xData);
            if xStat < 0 || yStat < 0
                return
            end
            
            % ---------------     Finally do the plotting
            % All functions need to return a vector
            xdat(:, end+1) = xVal;
            ydat = yAxis;
            cdat(:,end+1) = yVal;
            % And update the plot, and check whether we limit the number of
            % shots!!!
            if xv==1 && ~get(self.xAxis.autoRange, 'Value')
                ll = length(xdat);
                nShots = str2double(get(self.xAxis.l, 'String'));
                lim0 = ll-nShots;
                if lim0<0; lim0 = 0; end;
                set(self.fig.mainAxes, 'XLim', [lim0 ll]);
            end
            % If we're doing highlighting...
            hHighlight = findobj(self.fig.mainAxes, 'Type', 'line', 'Color', 'r');
            if get(self.fig.highlightLastShot, 'Value')
                set(hHighlight, 'Visible', 'on');
            else
                set(hHighlight, 'Visible', 'off');
            end
            % Check whether we are plotting the first shot, and if so,
            % remove the single datapoint and replace with suitable thing
            if strcmp(get(hPlot, 'Type'), 'Line')
                delete(hPlot);
                imagesc(xdat, ydat, cdat, 'Parent', self.fig.mainAxes, 'Tag', 'mainPlot');
            else
                set(hPlot, 'XData', xdat, 'YData', ydat, 'CData', cdat);
            end
            %set(hHighlight, 'XData', xVal, 'YData', yVal);
        end
        
        %% Plotting related functions
        
        function setColormap(self, hSource, ~)
            
        end
        
        function updateClims(self, hSource, ~)
            
        end
        
        %% 
        
        function [datVal, stat] = getDoocsData(self, axis)
            thisaxis = [axis 'Axis'];
            stat = -1; % Means unsuccessful return :(
            datVal = nan;
            ystr = get(self.(thisaxis).source, 'String');
            yv = get(self.(thisaxis).source, 'Value');
            diagAddr = self.getDoocsAddress(ystr{yv});
            doocsData = doocsread(diagAddr);
            %self.(thisaxis).timestamp == doocsData.timestamp;
            if isempty(doocsData)
                % If no data, return
                %self.xAxis.rawdata(end) = [];
                return
            end
            % doocsData.data is a number ONLY for float returns
            if ~isstruct(doocsData.data) % ie we have one number
                % For an incorrect address, doocsRead returns one NaN in data!
                if ~isnan(doocsData.data)
                    datVal = doocsData.data;
                else
                    return
                end
            else % MultiD data
                % Things get trickier...
                % First check whether it is the same shot
                if self.(thisaxis).timestamp ~= doocsData.data.tm
                    datVal = doocsData.data.d_spect_array_val;
                    self.(thisaxis).timestamp = doocsData.data.tm;
                else
                    return
                end
            end
            stat = 1;
        end
        
        function [xVal, stat, axis] = applyFunction(self, axis, xData)
            stat = -1;
            xFunc = get(self.([axis 'Axis']).func, 'String');
            if xFunc == '-'
                % If the data is 1D, we're good. Otherwise, get max value
                if numel(xData) == 1
                    xVal = xData;
                else
                    xVal = max(xData(:)); % Default to plotting max of data
                end
            elseif xFunc(1) == '@' % anonymous function!
                try
                    xVal = feval(str2func(xFunc), xData);
                catch err
                    set(self.statbar, 'String', ...
                        ['Error in ' axis '- function: ' err.message]);
                    return % For function errors, return
                end
            else % If it's not a '-' or anonymous fcn, must be a function name!
                try
                    xVal = feval(xFunc, xData);
                catch err
                    set(self.statbar, 'String', ...
                        ['Error in ' axis '- function: ' err.message]);
                    return % For function errors, return
                end
            end
            stat = 1;
        end
        
        %% Final parts
        function loadSettings(self, hSource, ~)
            % We will save the settings we chose for poll the sources and
            % functions of both axes, along with axes limits and line
            % settings.
            v = str2double(get(hSource, 'Tag'));
            fpath = fileparts(which('DataDisplay.DisplayBase'));
            if v == -1 % This means we are saving settings...
                fset = self.getMainSettings;
                fset.colormap = get(self.fig.colormap, 'Value');
                fset.highlightLastShot = get(self.fig.highlightLastShot, 'Value');
                for k=1:5 % I don't while loops, so give 5 tries to get the number right
                    settNo = inputdlg('Save into settings number (0-9):', 'Save settings', 1, {'1'});
                    if isempty(settNo) || isnan(str2double(settNo)) ; return; end;
                    settNo = str2double(settNo);
                    if settNo>=0 && settNo < 10
                        save(fullfile(fpath, ['Settings_' num2str(settNo)]), 'fset');
                        set(self.statbar, 'String', ['Saved current setup into settings no ' num2str(settNo)]);
                        break;
                    end
                end
            else
                try
                    load(fullfile(fpath, ['Settings_' num2str(v)]))
                    set(self.statbar, 'String', ['Loading setup from settings ' num2str(v)]);
                catch err
                    set(self.statbar, 'String', ['Error loading settings:' err.message]);
                    return;
                end
                % And now apply all the settings...
                self.setMainSettings(fset);
                try set(self.fig.colormap, 'Value', fset.colormap); catch; end
                self.setColormap(self.fig.colormap);
                try set(self.fig.highlightLastShot, 'Value', fset.highlightLastShot); catch; end;
                
            end
        end
    
        function displayData = getSaveData(self, ~, ~)
            hPlot = findobj(self.fig.mainAxes, 'Type', 'line', 'Tag', 'mainPlot');
            displayData.xData = get(hPlot, 'XData');
            displayData.yData = get(hPlot, 'YData');
            displayData.cData = get(hPlot, 'CData');
        end
    end
    
end