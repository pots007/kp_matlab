% DisplayBase.m
%
% Base class to derive all display classes from
%
% KP, DESY, 2017

classdef DisplayBase < handle
    properties
        fig
        doocsList
        timer
        sett
        statbar
        shotCount
        xAxis
        yAxis
        logbookPrinter
    end
    
    methods
        function self = DisplayBase(ID)
            
            self.fig.parent = figure(22607+ID); clf(self.fig.parent);
            set(self.fig.parent, 'Position', [1300 100 600 800],...
                'Toolbar', 'figure', 'MenuBar', 'none', 'Color', 'w',...
                'Name', 'Live Data Display: ', 'NumberTitle', 'off',...
                'HandleVisibility', 'callback', 'CloseRequestFcn', @(s,e)delete(self));
            if ismac
                set(self.fig.parent, 'DefaultUIControlFontSize', 12, 'DefaultAxesFontSize', 12);
            elseif ispc
                set(self.fig.parent, 'DefaultUIControlFontSize', 9, 'DefaultAxesFontSize', 8);
            end
            % -------------     Start setting up graphics:
            vbox_ = uiextras.VBox('Parent', self.fig.parent);
            self.fig.vboxF = uiextras.VBoxFlex('Parent', vbox_, 'Spacing', 7);
            hpan = uiextras.Panel('Parent', self.fig.vboxF, 'Padding', 2, 'Title', 'Controls');
            vboxU = uiextras.VBox('Parent', hpan);
            % First line is all about controls
            hbox = uiextras.HBox('Parent', vboxU);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Data poll frequency:');
            self.fig.pollFrequency = uicontrol('Style', 'popupmenu', 'Parent', hbox,...
                'String', {'0.1 Hz', '1 Hz', '10 Hz'}, 'Value', 2, 'Callback', @self.updateFrequency);
            self.fig.startButton = uicontrol('style', 'togglebutton', 'Parent', hbox,...
                'String', 'Start display', 'Value', 0, ...
                'BackGroundColor', 'r', 'Callback', @self.startButtonFunc);
            uicontrol('Style', 'pushbutton', 'String', 'Clear plot', 'Parent', hbox,...
                'Callback', @self.clearPlot)
            uicontrol('Style', 'pushbutton', 'String', 'Print', 'Parent', hbox, ...
                'Callback', @self.printToElog);
            hbox.Sizes = [150 100 150 100 -1];
            hbox = uiextras.HBox('Parent', vboxU);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Source for X axis:');
            self.xAxis.source = uicontrol('Style', 'popupmenu', 'Parent', hbox,...
                'String', {'Shot number',...
                'FLASH.DIAG/FFWDCONSTANTS/GAS_FLOW_PRESSURE/FLOAT_CONSTANT'});
            hbox.Sizes = [150 -1];
            % Stuff for y-axis
            hbox = uiextras.HBox('Parent', vboxU);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Source for Y axis:');
            self.yAxis.source = uicontrol('Style', 'popupmenu', 'Parent', hbox,...
                'String', {'randi number', 'FLASH.DIAG/FFW.BOND.SHUTTER/ANALOGIN/AIN.2',...
                'FLASH.DIAG/SIS8300DMA/FWDDIAG2.0/CH01.TD',...
                'FLASH.DIAG/FFWD.ADC/DAMON_SIGNAL/CH00.TD'});
            hbox.Sizes = [150 -1];
            % Now the functions for analysis
            hbox = uiextras.HBox('Parent', vboxU);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Function for X axis analysis:');
            self.xAxis.func = uicontrol('Style', 'edit', 'Parent', hbox,...
                'String', '-', 'HorizontalAlignment', 'left');
            hbox.Sizes = [175 -1];
            hbox = uiextras.HBox('Parent', vboxU);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Function for Y axis analysis:');
            self.yAxis.func = uicontrol('Style', 'edit', 'Parent', hbox,...
                'String', '-', 'HorizontalAlignment', 'left');
            hbox.Sizes = [175 -1];
            
            % ------------ Panel containing plotting controls
            hpan = uiextras.Panel('Parent', vboxU, 'Padding', 2, 'Title', 'Plot controls');
            vbox = uiextras.VBox('Parent', hpan);
            % Stuff for the x axis range
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'X-axis:');
            self.xAxis.autoRange = uicontrol('Style', 'checkbox', 'Parent', hbox,...
                'String', 'Auto-range', 'Callback', @self.autoRange,...
                'Value', 1, 'Tag', 'X');
            self.xAxis.log = uicontrol('Style', 'checkbox', 'Parent', hbox, ...
                'String', 'Log', 'Tag', 'X', 'Value', 0, 'Callback', @self.setLogAxis);
            uicontrol('Style', 'text', 'String', 'X min:', 'Parent', hbox);
            self.xAxis.min = uicontrol('Style', 'edit', 'String', '0',...
                'Parent', hbox, 'Tag', 'Xmin', 'Callback', @self.updateLimits);
            uicontrol('Style', 'text', 'String', 'X max:', 'Parent', hbox);
            self.xAxis.max = uicontrol('Style', 'edit', 'String', '100',...
                'Parent', hbox, 'Tag', 'Xmax', 'Callback', @self.updateLimits);
            uicontrol('Style', 'text', 'String', 'OR this many shots:', 'Parent', hbox);
            self.xAxis.l = uicontrol('Style', 'edit', 'String', '100', 'Parent', hbox);
            hbox.Sizes = [55 90 45 45 -1 45 -1 -1 50];
            hbox_sizes = hbox.Sizes;
            % Stuff for the y axis range
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Y-axis:');
            self.yAxis.autoRange = uicontrol('Style', 'checkbox', 'Parent', hbox,...
                'String', 'Auto-range', 'Callback', @self.autoRange,...
                'Value', 1, 'Tag', 'Y');
            self.yAxis.log = uicontrol('Style', 'checkbox', 'Parent', hbox, ...
                'String', 'Log', 'Tag', 'Y', 'Value', 0, 'Callback', @self.setLogAxis);
            uicontrol('Style', 'text', 'String', 'Y min:', 'Parent', hbox);
            self.yAxis.min = uicontrol('Style', 'edit', 'String', '0',...
                'Parent', hbox, 'Tag', 'Ymin', 'Callback', @self.updateLimits);
            uicontrol('Style', 'text', 'String', 'Y max:', 'Parent', hbox);
            self.yAxis.max = uicontrol('Style', 'edit', 'String', '100',...
                'Parent', hbox, 'Tag', 'Ymax', 'Callback', @self.updateLimits);
            uiextras.HBox('Parent', hbox); uiextras.HBox('Parent', hbox);
            hbox.Sizes = hbox_sizes;
            % Finally set the sizes to look nice
            vboxU.Sizes = [30*ones(1,5) -1];
            % From here on the subclass can define the rest of the things,
            % dependent on whether it's oneD or 2D plotting...
            
            % -------------- Panel for the axes
            hpan = uiextras.Panel('Parent', self.fig.vboxF, 'Padding', 2, 'Title', 'Plot');
            hhpan = uipanel('Parent', hpan);
            self.fig.mainAxes = axes('Parent', hhpan, 'NextPlot', 'add', 'Box', 'on');
            plot(self.fig.mainAxes, nan, nan, 'xk', 'Tag', 'mainPlot');
            plot(self.fig.mainAxes, nan, nan, 'xr');
            statbox = uiextras.HBox('Parent', vbox_);
            vbox_.Sizes = [-1 20];
            
            % ------------ General things left to do!
            fmenu = uimenu(self.fig.parent, 'Label', 'Saving options');
            uimenu(fmenu, 'Label', 'Save into setting', 'Callback', @self.loadSettings,...
                'Accelerator', 'S', 'Tag', '-1');
            for k=1:10
                uimenu(fmenu, 'Label', ['Load settings ' num2str(k-1)],...
                    'Callback', @self.loadSettings, 'Tag', num2str(k-1),...
                    'Accelerator', num2str(k-1));
            end
            uimenu(fmenu, 'Label', 'Save plot data', 'Callback', @self.savePlotData);
                
            self.shotCount = 0;
            self.timer = timer('Period', 1, 'TimerFcn', @self.timerFunc,...
                'ExecutionMode', 'fixedRate');
            %self.statbar = statusbar(self.fig.parent, 'Select axes to start.');
            self.statbar = uicontrol('Style', 'text', 'Parent', statbox,...
                'String', 'Select axes to start', 'HorizontalAlignment', 'left');
            % And do loading of necessary data.
            fpath = fileparts(which('DataDisplay.DisplayBase'));
            self.setDoocsAddresses(fullfile(fpath,'ListofKnownAddresses.txt'))
            
            % Set suitable minimum heights:
            self.fig.vboxF.MinimumHeights = [50 100];
            
            % Set up the printer
            try
                self.logbookPrinter.printer = PrintToELog;
                self.logbookPrinter.status = 1;
            catch err
                fprintf('No printer found!')
                self.logbookPrinter.status = -1;
            end
            
        end
                
        %% Timer related functions
        function startButtonFunc(self, hSource, ~)
            if get(hSource, 'Value')
                % We were off before, so stop the timer
                start(self.timer);
                set(hSource, 'String', 'Stop display', 'BackGroundColor', 'g');
            else
                % We were on, so need to stop it now
                stop(self.timer)
                set(hSource, 'String', 'Start display', 'BackGroundColor', 'r');
            end
        end
        
        function updateFrequency(self, hSource, ~)
            newFrequency = 10^(2 - get(hSource, 'Value'));
            % If the timer is running, we need to sstop it to update it
            if strcmp(self.timer.Running, 'on')
                stop(self.timer);
                set(self.timer, 'Period', newFrequency);
                start(self.timer);
            else
                set(self.timer, 'Period', newFrequency);
            end
            set(self.statbar, 'String', ['New polling frequency is ' num2str(1/newFrequency) ' Hz']);
        end
        
        function timerFunc(self, ~, ~)
            % This is the function that does the actual data retrieval.
            %disp(['Now plotting shot ' num2str(self.shotCount)]);
            self.shotCount = self.shotCount + 1;
            self.shotAnalysis;
        end
        
        function shotAnalysis(self)
            hPlot = get(self.fig.mainAxes, 'Children'); 
            xdat = get(hPlot, 'XData');
            ydat = get(hPlot, 'YData');
            xdat(end+1) = self.shotCount;
            ydat(end+1) = rand(1)*25;
            set(hPlot, 'XData', xdat, 'YData', ydat);
        end
        
        %% Commands for axis limits etc
        
        function autoRange(self, hSource, ~)
            axx = get(hSource, 'Tag');
            if get(hSource, 'Value') % We're autoranging!
                set(self.fig.mainAxes, [axx 'LimMode'], 'auto');
            else
                xmin = str2double(get(self.([lower(axx) 'Axis']).min, 'String'));
                xmax = str2double(get(self.([lower(axx) 'Axis']).max, 'String'));
                if ~isnan(xmin) && ~isnan(xmax)
                    if axx=='Y'
                        set(self.fig.mainAxes, [axx 'Lim'], [xmin xmax]);
                    elseif get(self.xAxis.source, 'Value')~=1
                        set(self.fig.mainAxes, [axx 'Lim'], [xmin xmax]);
                    end
                end
            end
        end
        
        function updateLimits(self, hSource, ~)
            IDaxx = get(hSource, 'Tag'); ax = IDaxx(1); %ID = IDaxx(2:end);
            min_ = str2double(get(self.([lower(ax) 'Axis']).min, 'String'));
            max_ = str2double(get(self.([lower(ax) 'Axis']).max, 'String'));
            if ~strcmp(get(self.fig.mainAxes, [ax 'LimMode']), 'auto')
                set(self.fig.mainAxes, [ax 'Lim'], [min_ max_]);
            end
        end
        
        function setLogAxis(self, hSource, ~)
            IDaxx = get(hSource, 'Tag'); ax = IDaxx(1); %ID = IDaxx(2:end);
            if get(hSource, 'Value')
                set(self.fig.mainAxes, [ax 'Scale'], 'log');
            else
                set(self.fig.mainAxes, [ax 'Scale'], 'lin');
            end
        end
 
        %% Functions for DOOCS address import and stuff
        function addr = getDoocsAddress(self, displayName)
            % Gets the actual doocs address, from the displayName
            inds = strcmp(displayName, self.doocsList(:,1));
            addr = self.doocsList{inds,2};
        end
        
        function setDoocsAddresses(self, fileName)
            flines = getFileText(fileName);
            doocsList0 = cell(length(flines), 1);
            displayList = cell(size(doocsList0));
            for k=1:length(flines)
                ind = strfind(flines{k}, '=');
                displayList{k} = strtrim(flines{k}(1:ind-1));
                doocsList0{k} = strtrim(flines{k}(ind+1:end));
            end
            self.doocsList = [displayList, doocsList0];
            set(self.xAxis.source, 'String', ['Shot number'; displayList]);
            set(self.yAxis.source, 'String', ['randi number'; displayList]);
        end
       
        %% Functions for save/load - need to be updated by child
        
        function loadSettings(self, hSource, ~)
            % Needs to be implemented by child classes.
        end
        
        function fset = getMainSettings(self)
            % This will be called by children, speeds up setting saving
            fset.pollFrequency = get(self.fig.pollFrequency, 'Value');
            fset.xSource = get(self.xAxis.source, 'Value');
            fset.ySource = get(self.yAxis.source, 'Value');
            fset.xFun = get(self.xAxis.func, 'String');
            fset.yFun = get(self.yAxis.func, 'String');
            fset.xAutoRange = get(self.xAxis.autoRange, 'Value');
            fset.xLog = get(self.xAxis.log, 'Value');
            fset.xMin = get(self.xAxis.min, 'String');
            fset.xMax = get(self.xAxis.max, 'String');
            fset.xl = get(self.xAxis.l, 'String');
            fset.yAutoRange = get(self.yAxis.autoRange, 'Value');
            fset.yLog = get(self.yAxis.log, 'Value');
            fset.yMin = get(self.yAxis.min, 'String');
            fset.yMax = get(self.yAxis.max, 'String');
        end
        
        function setMainSettings(self, fset)
            try set(self.fig.pollFrequency, 'Value', fset.pollFrequency); catch; end;
            self.updateFrequency(self.fig.pollFrequency);
            try set(self.xAxis.source, 'Value', fset.xSource); catch; end;
            try set(self.yAxis.source, 'Value', fset.ySource); catch; end;
            try set(self.xAxis.func, 'String', fset.xFun); catch; end;
            try set(self.yAxis.func, 'String', fset.yFun); catch; end;
            try set(self.xAxis.autoRange, 'Value', fset.xAutoRange); catch; end;
            self.autoRange(self.xAxis.autoRange);
            try set(self.xAxis.log, 'Value', fset.xLog); catch; end;
            self.setLogAxis(self.xAxis.log);
            try set(self.xAxis.min, 'String', fset.xMin); catch; end;
            self.updateLimits(self.xAxis.min);
            try set(self.xAxis.max, 'String', fset.xMax); catch; end;
            self.updateLimits(self.xAxis.max);
            try set(self.xAxis.l, 'String', fset.xl); catch; end;
            try set(self.yAxis.autoRange, 'Value', fset.yAutoRange); catch; end;
            self.autoRange(self.yAxis.autoRange);
            try set(self.yAxis.log, 'Value', fset.yLog); catch; end;
            self.setLogAxis(self.yAxis.log);
            try set(self.yAxis.min, 'String', fset.yMin); catch; end;
            self.updateLimits(self.yAxis.min);
            try set(self.yAxis.max, 'String', fset.yMax); catch; end;
            self.updateLimits(self.yAxis.min);
        end
        
        function savePlotData(self, ~, ~)
           % Needs to be implemented by child, depending on type.
            displayData = self.getSaveData;
            [filename, pathname] = uiputfile('*.mat', 'Save plot data');
            if isequal(filename,0) || isequal(pathname,0)
                return
            else
                save(fullfile(pathname, filename), 'displayData');
            end
        end
        
        function ll = getSaveData(self, ~, ~)
            % Thiss method should be implemented by child class to provide
            % the data from the plot!
            ll = 'foo';
        end
        
        %%
        function printToElog(self, ~, ~)
            if self.logbookPrinter.status==1
                % If the printer successfully exists...
                self.logbookPrinter.printer.printToFFBondLabLog(self.fig.parent,...
                    {'Live Data Display', 'LiveData', ''})
            end
        end
        
        function clearPlot(self, ~, ~)
            % This will clear the data from the plot!
            hh = get(self.fig.mainAxes, 'Children');
            for k=1:length(hh)
                set(hh(k), 'XData', nan, 'YData', nan);
            end
            self.shotCount = 0;
        end
        
        function delete(self)
            % Stop the timer to clean up!
            stop(self.timer);
            delete(self.fig.parent);
        end
    end
end