% Display1D.m
%
% Class to plot 1D data.
%
% KP, DESY, 2017

classdef Display1D < DataDisplay.DisplayBase
    properties
        
    end
    
    methods
        function self = Display1D(ID)
            % This has already run the base class constructor.
            self = self@DataDisplay.DisplayBase(ID);
            hh = findobj(self.fig.parent, 'Title', 'Plot controls');
            vbox = get(hh, 'Children'); % This should return the vbox that will
            % contain the rest of the gui controls for plotting controls
            % The stuff for the x-axis:
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Linestyle');
            self.fig.lineStyle = uicontrol('Style', 'popupmenu', 'Parent', hbox,...
                'String', {'none', '-', '--', '-.', ':'}, 'Callback', @self.setLineStyle);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Line width');
            self.fig.lineWidth = uicontrol('Style', 'popupmenu', 'Parent', hbox, 'Value', 3,...
                'String', {'0.5', '1', '1.5', '2', '3', '4'}, 'Callback', @self.setLineWidth);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Marker');
            self.fig.markerStyle = uicontrol('Style', 'popupmenu', 'Parent', hbox,...
                'String', {'x', '.', 'o', 'd', 's'}, 'Callback', @self.setMarkerStyle);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Marker size');
            self.fig.markerSize = uicontrol('Style', 'popupmenu', 'Parent', hbox, 'Value', 3,...
                'String', {'5' '7' '10' '15' '20' '30'}, 'Callback', @self.setMarkerSize);
            self.fig.highlightLastShot = uicontrol('Style', 'checkbox', 'Value', 1, ...
                'String', 'Highlight last shot', 'Parent', hbox);
            hbox.Sizes = [65 70 70 50 50 40 70 40 -1];
            % hbox for padding the stuff out
            uiextras.HBox('Parent', vbox);
            
            % Now set the heights, depending on what system we're on!
            nLines = length(get(vbox, 'Children'));
            vbox.Sizes = [30*ones(nLines-1,1); -1];
            % And the timestamps!
            self.xAxis.timestamp = 0;
            self.yAxis.timestamp = 0;
        end
        
        %% Actual data plotting
        function shotAnalysis(self)
            hPlot = findobj(self.fig.mainAxes, 'Type', 'line', 'Color', 'k');
            xdat = get(hPlot, 'XData');
            ydat = get(hPlot, 'YData');
            % -------------- And now figure out what we are asked to plot
            xv = get(self.xAxis.source, 'Value');
            if xv == 1 % x is shot number!
                xData = self.shotCount;
                xStat = 1;
            else
                [xData, xStat] = self.getDoocsData('x');
            end
            yv = get(self.yAxis.source, 'Value');
            if yv == 1 % y is the randi number
                yData = rand(1)*10;
                yStat = 1;
            else % We're getting data from DOOCS
                [yData, yStat] = self.getDoocsData('y');
                %self.yAxis.rawdata(end+1) = randi(1)*4;
            end
            % But if we didn't get data, stop now...
            if yStat < 0 || xStat < 0
                self.shotCount = self.shotCount - 1;
                return
            end
            
            % -----------------------  Function applying
            % We have the new for everything data, now let's try to apply the analysis
            % function!
            % Y-axis first
            [yVal, yStat] = self.applyFunction('y', yData);
            % And now go through the x-function!
            [xVal, xStat] = self.applyFunction('x', xData);
            if xStat < 0 || yStat < 0
                return
            end
            
            % ---------------     Finally do the plotting
            xdat(end+1) = xVal;
            ydat(end+1) = yVal;
            % And update the plot, and check whether we limit the number of
            % shots!!!
            if xv==1 && ~get(self.xAxis.autoRange, 'Value')
                ll = length(xdat);
                nShots = str2double(get(self.xAxis.l, 'String'));
                lim0 = ll-nShots;
                if lim0<0; lim0 = 0; end;
                set(self.fig.mainAxes, 'XLim', [lim0 ll]);
            end
            % If we're doing highlighting...
            hHighlight = findobj(self.fig.mainAxes, 'Type', 'line', 'Color', 'r');
            if get(self.fig.highlightLastShot, 'Value')
                set(hHighlight, 'Visible', 'on');
            else
                set(hHighlight, 'Visible', 'off');
            end
            set(hPlot, 'XData', xdat, 'YData', ydat);
            set(hHighlight, 'XData', xVal, 'YData', yVal);
            % And set/update axis labels
            xstr = get(self.xAxis.source, 'String');
            xv = get(self.xAxis.source, 'Value');
            xlabel(self.fig.mainAxes, xstr(xv));
            ystr = get(self.yAxis.source, 'String');
            yv = get(self.yAxis.source, 'Value');
            ylabel(self.fig.mainAxes, ystr(yv));
            % And update the status bar with the last value...
            set(self.statbar, 'String', sprintf('Latest x=%2.3e, y=%2.3e', xVal, yVal));
        end
        
        %% Plotting related functions
        
        function setLineStyle(self, hSource, ~)
            ind = get(hSource, 'Value');
            str = get(hSource, 'String');
            set(get(self.fig.mainAxes, 'Children'), 'LineStyle', str{ind});
        end
        
        function setLineWidth(self, hSource, ~)
            ind = get(hSource, 'Value');
            str = get(hSource, 'String');
            set(get(self.fig.mainAxes, 'Children'), 'LineWidth', str2double(str{ind}));
        end
        
        function setMarkerStyle(self, hSource, ~)
            ind = get(hSource, 'Value');
            str = get(hSource, 'String');
            set(get(self.fig.mainAxes, 'Children'), 'Marker', str{ind});
        end
        
        function setMarkerSize(self, hSource, ~)
            ind = get(hSource, 'Value');
            str = get(hSource, 'String');
            set(get(self.fig.mainAxes, 'Children'), 'MarkerSize', str2double(str{ind}));
        end
        
        %% 
        
        function [datVal, stat] = getDoocsData(self, axis)
            thisaxis = [axis 'Axis'];
            stat = -1; % Means unsuccessful return :(
            datVal = nan;
            ystr = get(self.(thisaxis).source, 'String');
            yv = get(self.(thisaxis).source, 'Value');
            diagAddr = self.getDoocsAddress(ystr{yv});
            doocsData = doocsread(diagAddr);
            %self.(thisaxis).timestamp == doocsData.timestamp;
            if isempty(doocsData)
                % If no data, return
                %self.xAxis.rawdata(end) = [];
                return
            end
            % doocsData.data is a number ONLY for float returns
            if ~isstruct(doocsData.data) % ie we have one number
                % For an incorrect address, doocsRead returns one NaN in data!
                if ~isnan(doocsData.data)
                    datVal = doocsData.data;
                else
                    return
                end
            else % MultiD data
                % Things get trickier...
                % First check whether it is the same shot
                if isfield(doocsData.data, 'tm')
                    doocsTimeStamp = doocsData.data.tm;
                else
                    doocsTimeStamp = doocsData.data.sec;
                end
                if self.(thisaxis).timestamp ~= doocsTimeStamp %doocsData.data.tm
                    if isfield(doocsData.data, 'd_spect_array_val')
                        datVal = doocsData.data.d_spect_array_val;
                    elseif isfield(doocsData.data, 'val_val')
                        datVal = doocsData.data.val_val;
                    end
                    self.(thisaxis).timestamp = doocsTimeStamp;
                else
                    return
                end
            end
            stat = 1;
        end
        
        function [xVal, stat] = applyFunction(self, axis, xData)
            stat = -1;
            %disp(size(xData))
            xFunc = get(self.([axis 'Axis']).func, 'String');
            if xFunc == '-'
                % If the data is 1D, we're good. Otherwise, get max value
                if numel(xData) == 1
                    xVal = xData;
                else
                    xVal = max(xData(:)); % Default to plotting max of data
                end
            elseif xFunc(1) == '@' % anonymous function!
                try
                    xVal = feval(str2func(xFunc), xData);
                catch err
                    set(self.statbar, 'String', ...
                        ['Error in ' axis '- function: ' err.message]);
                    return % For function errors, return
                end
            else % If it's not a '-' or anonymous fcn, must be a function name!
                try
                    xVal = feval(xFunc, xData);
                catch err
                    set(self.statbar, 'String', ...
                        ['Error in ' axis '- function: ' err.message]);
                    return % For function errors, return
                end
            end
            stat = 1;
        end
        
        %% Final parts
        function loadSettings(self, hSource, ~)
            % We will save the settings we chose for poll the sources and
            % functions of both axes, along with axes limits and line
            % settings.
            v = str2double(get(hSource, 'Tag'));
            fpath = fileparts(which('DataDisplay.DisplayBase'));
            if v == -1 % This means we are saving settings...
                fset = self.getMainSettings;
                fset.lineStyle = get(self.fig.lineStyle, 'Value');
                fset.lineWidth = get(self.fig.lineWidth, 'Value');
                fset.markerStyle = get(self.fig.markerStyle, 'Value');
                fset.markerSize = get(self.fig.markerSize, 'Value');
                fset.highlightLastShot = get(self.fig.highlightLastShot, 'Value');
                for k=1:5 % I don't while loops, so give 5 tries to get the number right
                    settNo = inputdlg('Save into settings number (0-9):', 'Save settings', 1, {'1'});
                    if isempty(settNo) || isnan(str2double(settNo)) ; return; end;
                    settNo = str2double(settNo);
                    if settNo>=0 && settNo < 10
                        save(fullfile(fpath, ['Settings_' num2str(settNo)]), 'fset');
                        set(self.statbar, 'String', ['Saved current setup into settings no ' num2str(settNo)]);
                        break;
                    end
                end
            else
                try
                    load(fullfile(fpath, ['Settings_' num2str(v)]))
                    set(self.statbar, 'String', ['Loading setup from settings ' num2str(v)]);
                catch err
                    set(self.statbar, 'String', ['Error loading settings:' err.message]);
                    return;
                end
                % And now apply all the settings...
                self.setMainSettings(fset);
                try set(self.fig.lineStyle, 'Value', fset.lineStyle); catch; end;
                self.setLineStyle(self.fig.lineStyle);
                try set(self.fig.lineWidth, 'Value', fset.lineWidth); catch; end;
                self.setLineWidth(self.fig.lineWidth);
                try set(self.fig.markerStyle, 'Value', fset.markerStyle); catch; end;
                self.setMarkerStyle(self.fig.markerStyle);
                try set(self.fig.markerSize, 'Value', fset.markerSize); catch; end
                self.setMarkerSize(self.fig.markerSize);
                try set(self.fig.highlightLastShot, 'Value', fset.highlightLastShot); catch; end;
                
            end
        end
    
        function displayData = getSaveData(self, ~, ~)
            hPlot = findobj(self.fig.mainAxes, 'Type', 'line', 'Color', 'k');
            displayData.xData = get(hPlot, 'XData');
            displayData.yData = get(hPlot, 'YData');
        end
    end
    
end