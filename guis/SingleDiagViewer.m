% Basically, redoing Java stack viewer for Matlab
% BUT, has builtin support for our run structures so can easily view data

classdef SingleDiagViewer < uiextrasX.HBox
    
    properties
        ax
        imdata
        runn
        colmaps
        statbar
        controls
        listeners = {}
    end
    
    methods
        function o = SingleDiagViewer(parent, varargin)
            if ~ishandle(parent); Parent = figure(parent); end
            o@uiextrasX.HBox('Parent', Parent);
            defer(o);
            vboxL = uiextrasX.VBox('Parent', o);
            vboxR = uiextrasX.VBox('Parent', o);
            hbox = uiextrasX.HBox('Parent', vboxL);
            hpan = uipanel('Parent', hbox, 'Title', 'Data selection');
            vbox = uiextrasX.VBox('Parent', hpan);
            % First, data location
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Data root:', 'Parent', hbox);
            o.runn.path = uicontrol('Style', 'edit', 'String',...
                'E:\Kristjan\Documents\Uni\Imperial\2015[07]_Gemini\Data\Data', 'Parent', hbox);
            hbox.Sizes = [60 -1];
            % Day selection
            hbox = uiextrasX.HBox('Parent', vbox);
            o.runn.date = javaDateChooserPanel(hbox, 'ItemStateChangedCallback', @(s,e)date_selected(o,1));
            % List of runs - updated by date selection
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Select run:', 'Parent', hbox);
            o.runn.runbox = uicontrol('Style', 'popupmenu', 'String', '-',...
                'Parent', hbox, 'Callback', @(s,e)run_selected(o,get(o.runn.runbox, 'Value')));
            % Now choose diagnostics, based on unique diagnostic names
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Select diagnostic:');
            o.runn.diagbox = uicontrol('Style', 'popupmenu', 'String', ' ',...
                'Parent', hbox, 'Callback', @(s,e)diag_selected(o,1));
            % And display some info about them...
            hbox = uiextrasX.HBox('Parent', vbox);
            o.runn.info = uicontrol('Style', 'text', 'String', '', 'Parent', hbox);
            % And finally choose shot, or all of them!
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Select shot:');
            o.runn.shotbox = javaNumSpinner2(1,1,100,1, hbox,...
                'StateChangedCallback', @(s,e)shot_selected(o,-1));
            o.runn.allshots = uicontrol('Style', 'checkbox', 'String', 'All',...
                'Parent', hbox, 'Callback', @(s,e)shot_selected(o,1));
            hbox.Sizes = [90 -1 50];
            vbox.Sizes = [30 200 25 30 30 30];
            
            % Now for the image controls panel
            hbox = uiextrasX.HBox('Parent', vboxL);
            hpan = uipanel('Parent', hbox, 'Title', 'Image control');
            vbox = uiextrasX.VBox('Parent', hpan);
            % Setting colormaps
            o.colmaps = make_colormaps;
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Colormap', 'Parent', hbox);
            o.controls.colmap = uicontrol('Style', 'popupmenu', 'String', o.colmaps(:,2),...
                'Parent', hbox, 'Callback', @(s,e)colormap_changed(o,0));
            % Setting CLimits
            hbox = uiextrasX.HBox('Parent', vbox);
            o.controls.climLow = uicontrol('Style', 'edit', 'String', '0',...
                'Parent', hbox, 'Callback', @(s,e)climbox_changed(o,-1));
            uicontrol('Style', 'text', 'String', 'Intensity limits:', 'Parent', hbox,...
                'HorizontalAlignment', 'center');
            o.controls.climHigh = uicontrol('Style', 'edit', 'String', '1',...
                'Parent', hbox, 'Callback', @(s,e)climbox_changed(o,1));
            % CLims selections
            hbox = uiextrasX.HBox('Parent', vbox);
            o.controls.bg = uibuttongroup('Parent', hbox,'SelectionChangeFcn', @(s,e)clim_selected(o,e));
            uicontrol('Style', 'radiobutton', 'String', 'Min-Max', 'Parent', o.controls.bg,...
                'Units', 'normalized', 'Position', [0 0 0.33 1]);
            uicontrol('Style', 'radiobutton', 'String', '5%/95%', 'Parent', o.controls.bg,...
                'Units', 'normalized', 'Position', [0.33 0 0.33 1]);
            uicontrol('Style', 'radiobutton', 'String', 'Manual', 'Parent', o.controls.bg,...
                'Units', 'normalized', 'Position', [0.66 0 0.33 1]);
            % Slider for manual CLims
            hbox = uiextrasX.HBox('Parent', vbox);
            o.controls.climSlider = javaRangeSlider(0,100,30,80, hbox);
            set(o.controls.climSlider, 'MajorTickSpacing',50, 'MinorTickSpacing',10, 'PaintTicks',true,...
                'PaintLabels',true, 'StateChangedCallback',@(s,e)clim_changed(o,e));
            vbox.Sizes = [30 30 30 -1];
            % Now for the lines controls
            hbox = uiextrasX.HBox('Parent', vboxL);
            hpan = uipanel('Parent', hbox, 'Title', 'Lineouts control');
            vbox = uiextrasX.VBox('Parent', hpan);
            % Checkbox to enable/disable lineouts (gains more space if off)
            hbox = uiextrasX.HBox('Parent', vbox);
            o.ax.lineoutbox = uicontrol('Style', 'checkbox', 'String', 'Show lineplots', 'Value', 0,...
                'Parent', hbox, 'Callback', @(s,e)lineout_enable(o, get(o.ax.lineoutbox, 'Value')));
            % Checkbox to choose between lineout and FWBs
            hbox = uiextrasX.HBox('Parent', vbox);
            o.ax.lineouts = uicontrol('Style', 'checkbox', 'String', 'Lineout',...
                'Parent', hbox, 'Value', 1, 'Callback', @(s,e)line_changed(o,0));
            o.ax.FWBs = uicontrol('Style', 'checkbox', 'String', 'Sum along dim',...
                'Parent', hbox, 'Value', 0, 'Callback', @(s,e)line_changed(o,1));
            % Button to select lineout location
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'pushbutton', 'String', 'Pick', 'Parent', hbox,...
                'Callback', @(s,e)lineout_changed(o,0));
            uicontrol('Style', 'text', 'String', 'x', 'Parent', hbox);
            o.ax.lineout_xbox = javaNumSpinner2(1,1,2^16, 1, hbox, 'StateChangedCallback', ...
                @(s,e)lineout_changed(o,1));
            uicontrol('Style', 'text', 'String', 'y', 'Parent', hbox);
            o.ax.lineout_ybox = javaNumSpinner2(1,1,2^16, 1, hbox, 'StateChangedCallback', ...
                @(s,e)lineout_changed(o,2));
            vbox.Sizes = [30 30 40];
            hbb = uiextrasX.HBox('Parent', vboxL); % Dummy to go below statusbar
            vboxL.Sizes = [350 175 -1 20];
            
            % Now the panel with axes and stack slider
            hbox = uiextrasX.HBox('Parent', vboxR);
            sb = uipanel('Parent', hbox, 'Title', '');
            axes_menu=AxisScaleContextMenu(Parent);
            o.ax.axmain = axes('Parent', sb, 'Units', 'normalized', 'YDir', 'normal',...
                'Position', [0.1 0.1 0.85 0.85],'UIContextMenu',axes_menu.menu);
            o.ax.axvert = axes('Parent', sb, 'Units', 'normalized', 'Box', 'on',...
                'Position', [0.72 0.3 0.25 0.6],'Visible', 'off', 'YAxisLocation','right');
            o.ax.axhorz = axes('Parent', sb, 'Units', 'normalized', 'Box', 'on',...
                'Position', [0.1 0.03 0.6 0.25],'Visible', 'off');
            o.listeners{end+1} = addlistener(o.ax.axmain, 'YLim', 'PostSet',...
                @(s,e) mainaxlim_changed(o,0));
            o.listeners{end+1} = addlistener(o.ax.axmain, 'XLim', 'PostSet',...
                @(s,e) mainaxlim_changed(o,1));
            o.listeners{end+1} = addlistener(o.ax.axmain, 'CLim', 'PostSet',...
                @(s,e) mainaxlim_changed(o,2));
            % Now initial image and listeners to update lineouts
            o.ax.himage = imagesc(rand(100), 'Parent', o.ax.axmain);
            o.listeners{end+1} = addlistener(o.ax.himage, 'CData', 'PostSet',...
                @(s,e) lineout_changed(o,1));
            o.listeners{end+1} = addlistener(o.ax.himage, 'CData', 'PostSet',...
                @(s,e) lineout_changed(o,2));
            o.ax.hvertline = line(1:100, nan(1,100), 'Parent', o.ax.axvert, 'Color', 'k', 'Visible', 'off');
            o.ax.hhorzline = line(1:100, nan(1,100), 'Parent', o.ax.axhorz, 'Color', 'k', 'Visible', 'off');
            o.ax.cb = colorbar('Peer', o.ax.axmain, 'Location', 'Westoutside');
            % The stack slider
            hbox = uiextrasX.HBox('Parent', vboxR);
            o.ax.stackSlider = javaSlider(0,100,1, hbox,...
                'StateChangedCallback', @(s,e)stackslider_changed(o,1));
            hbbb = uiextrasX.HBox('Parent', vboxR);
            vboxR.Sizes = [-1 40 20];
            o.Sizes = [250 -1];
            o.statbar = statusbar(Parent, 'Select date and run.');
            o.colormap_changed(2);
            resume(o);
            assignin('base', 'SingleDiagnosticViewer', o);
        end
        
        function date_selected(o,~)
            % Get the selected date
            selectedDate = o.runn.date.getSelectedDate();
            % Get Date Values
            DD = get(selectedDate, 'Date');
            MM = get(selectedDate, 'Month')+1; % a Java quirk
            YYYY = get(selectedDate, 'Year')+1900; % Another Java quirk!
            spath = get(o.runn.path, 'String');
            dates = sprintf('%i%02i%02i', YYYY, MM, DD);
            % Sprintf isn't platform independent!
            o.runn.datefol = fullfile(spath, dates);%sprintf('%s\\%i%02i%02i\\',spath, YYYY, MM, DD);
            follist = dir(o.runn.datefol);
            if isempty(follist)
                set(o.runn.runbox, 'Value', 1, 'String', 'No data this day', 'Enable', 'off');
                return;
            end
            runlist = cell(100,1); it = 1;
            for i=3:length(follist)
                if ~follist(i).isdir; continue; end;
                runlist{it} = follist(i).name(end-2:end);
                it = it+1;
            end
            runlist = runlist(~cellfun('isempty',runlist));
            inval = get(o.runn.runbox, 'Value');
            if inval>length(runlist); inval = 1; end;
            set(o.runn.runbox, 'Value', inval, 'String', runlist, 'Enable', 'on');
            run_selected(o, get(o.runn.runbox, 'Value'));
        end
        
        function run_selected(o,r)
            if ~isfield(o.runn, 'datefol'); return; end;
            runlist = get(o.runn.runbox, 'String');
            % A bit shabby with hardcoded lengths, but let's hope
            % Fullfile should work on macs as well!
            o.runn.runfol = fullfile(o.runn.datefol, [o.runn.datefol(end-8:end) 'r' runlist{r}]);
            fillist = dir(o.runn.runfol);
            % Filter out all folders, if there are any
            fnames = {fillist.name}; isdirs = cell2mat({fillist.isdir});
            fnames = fnames(~isdirs);
            %fnamm = cellfun(@(x) x(strfind(x, '_')+1:strfind(x, '.')-1), fnames, 'UniformOutput', false);
            fnamm = cellfun(@(x) x(strfind(x, '_')+1:end), fnames, 'UniformOutput', false);
            diagnames = unique(fnamm);
            diagnames = diagnames(~cellfun('isempty', diagnames));
            inval = get(o.runn.diagbox, 'Value');
            if inval>length(diagnames); inval = 1; end;
            set(o.runn.diagbox, 'Value', inval, 'String', diagnames);
            o.diag_selected;
            %if get(o.runn.allshots, 'Value') %If 'All' is ticked, load stack right away
             %   shot_selected(o,1);
            %end
        end
        
        function diag_selected(o,~)
            % Set the maximum number in the shot spinner
            diaglist = get(o.runn.diagbox, 'String');
            diag = diaglist{get(o.runn.diagbox, 'Value')};
            shotfol = fullfile(o.runn.runfol, o.runn.runfol(end-12:end));
            fillist = dir([shotfol '*' diag]);
            inval = get(o.runn.shotbox, 'Value');
            set(o.runn.info, 'String', [num2str(length(fillist)) ' files for ' diag]);
            if inval>length(fillist); inval = 1; end;
            set(o.runn.shotbox, 'Value', inval);
            if get(o.runn.allshots, 'Value') % If ticked, do stack!
                shot_selected(o,1);
            else
                shot_selected(o,-1);
            end
        end
        
        function shot_selected(o,v)
            diaglist = get(o.runn.diagbox, 'String');
            diag = diaglist{get(o.runn.diagbox, 'Value')};
            shotfol = fullfile(o.runn.runfol, o.runn.runfol(end-12:end));
            shotno = get(o.runn.shotbox, 'Value');
            if v==1 % All is selected - shot selector will change frame in stack
                if get(o.runn.allshots, 'Value'); % Load stack!
                    % Get list of files, then load all
                    fillist = dir([shotfol '*' diag]);
                    imtemp = o.imread(fullfile(o.runn.runfol, fillist(1).name));
                    set(o.statbar, 'text', 'Loading stack...');
                    set(o.statbar.ProgressBar, 'Minimum',1, 'Maximum',length(fillist), 'Value',1);
                    o.statbar.ProgressBar.setVisible(true);
                    o.imdata.data = zeros([size(imtemp) length(fillist)]);
                    o.imdata.data(:,:,1) = imtemp;
                    for i=2:length(fillist)
                        set(o.statbar.ProgressBar, 'Value', i);
                        o.imdata.data(:,:,i) = o.imread(fullfile(o.runn.runfol, fillist(i).name));
                    end
                    set(o.ax.himage, 'CData', o.imdata.data(:,:,1));
                    set(o.ax.stackSlider, 'Maximum', length(fillist), 'Minimum', 1);
                    o.statbar.ProgressBar.setVisible(false);
                    set(o.statbar, 'text', ['Loaded stack with ' num2str(length(fillist)) ' frames']);
                    o.imdata.max = max(o.imdata.data(:));
                    o.imdata.min = min(o.imdata.data(:));
                else % Reload single image and disable the scroll bar
                    filen = sprintf('%ss%03i_%s', shotfol, shotno, diag);
                    if exist(filen, 'file')
                        o.imdata.data = o.imread(filen);
                        set(o.ax.himage, 'CData', o.imdata.data);
                        o.imdata.max = max(o.imdata.data(:));
                        o.imdata.min = min(o.imdata.data(:));
                    end
                end
            elseif v==-1 % If 'All' is not checked, load image; else, next slide in stack
                if get(o.runn.allshots, 'Value') % next slide in stack
                    if numel(size(o.imdata.data))==3 %Only do this if have a stack!
                        set(o.ax.himage, 'CData', o.imdata.data(:,:,shotno));
                        set(o.ax.stackSlider, 'Value', shotno);
                    end
                    return;
                else % Simply load image 
                    filen = sprintf('%ss%03i_%s', shotfol, shotno, diag);
                    if exist(filen, 'file')
                        o.imdata.data = o.imread(filen);
                        set(o.ax.himage, 'CData', o.imdata.data);
                        o.imdata.max = max(o.imdata.data(:));
                        o.imdata.min = min(o.imdata.data(:));
                    end
                end
            end
            set(o.ax.axmain, 'XLim', [1 size(o.imdata.data,2)], 'YLim', [1 size(o.imdata.data,1)]);            
            majorticks = 10*(round(0.1*(o.imdata.max-o.imdata.min)/4));
            minorticks = majorticks/2;
            set(o.controls.climSlider, 'MajorTickSpacing', majorticks, ...
                'MinorTickSpacing', minorticks, 'Maximum', o.imdata.max,...
                'Minimum', o.imdata.min);
           %strcmp(get(get(o.controls.bg, 'SelectedObject'), 'String'), 'Min-Max')
            if strcmp(get(get(o.controls.bg, 'SelectedObject'), 'String'), 'Min-Max')
                
                set(o.controls.climHigh, 'String', num2str(o.imdata.max, '%i'));
                set(o.controls.climLow, 'String', num2str(o.imdata.min, '%i'));
                set(o.ax.axmain, 'CLim', [o.imdata.min o.imdata.max]);
            end
            
        end
        
        function clim_selected(o,v)
            selstring = get(v.NewValue, 'String');
            if ~isfield(o.imdata, 'data'); return; end;
            minv = o.imdata.min; maxv = o.imdata.max;
            switch selstring
                case 'Min-Max' % Scale to min and max of stack
                    clims = [minv maxv];
                    set(o.controls.climSlider, 'Enable', 0);
                case '5%/95%' % Scale to 5% and 95% of stack
                    diff = double(maxv-minv);
                    clims = double(minv) + diff*[0.05 0.95];
                    set(o.controls.climSlider, 'Enable', 0, 'LowValue', clims(1),...
                        'HighValue', clims(2));
                case 'Manual' % Enable slider for manual control
                    clims = get(o.ax.axmain, 'CLim');
                    set(o.controls.climSlider, 'Enable', 1);
            end
            set(o.ax.axmain, 'CLim', clims);
            set(o.controls.climHigh, 'String', num2str(clims(2), '%i'));
            set(o.controls.climLow, 'String', num2str(clims(1), '%i'));
        end
        
        function clim_changed(o,~)
            clims = [o.controls.climSlider.getLowValue() o.controls.climSlider.getHighValue()];
            set(o.ax.axmain, 'CLim', clims);
            set(o.controls.climHigh, 'String', num2str(clims(2), '%i'));
            set(o.controls.climLow, 'String', num2str(clims(1), '%i'));
        end
        
        function climbox_changed(o,v)
            clims = get(o.ax.axmain, 'CLim');
            if v>0
                clims(2) = str2double(get(o.controls.climHigh, 'String'));
                set(o.controls.climSlider, 'HighValue', clims(2));
            else
                clims(1) = str2double(get(o.controls.climLow, 'String'));
                set(o.controls.climSlider, 'LowValue', clims(1));
            end
            set(o.ax.axmain, 'CLim', clims);
        end
        
        function stackslider_changed(o,~)
            if numel(size(o.imdata.data))==3 % If we have a stack
                %set(o.ax.himage, 'CData', o.imdata.data(:,:, round(o.ax.stackSlider.Value)));
                % The above is done in the callback to below ;)
                set(o.runn.shotbox, 'Value', round(o.ax.stackSlider.Value));
            end
        end
        
        function imdata = imread(o, filen)
            [~,~,ext] = fileparts(filen);
            switch ext
                case '.tif'
                    imdata = imread(filen);
                case '.raw'
                    % Have to figure out the size!
                    % Trying this with usual sizes, or 640x480, 1280x1024,
                    % 1088x2048
                    ff = dir(filen); ff.bytes;
                    switch ff.bytes
                        case 614400 % 640x480, AVT
                            imdata = ReadRAW16bit(filen, 640, 480);
                        case 2621440 % 1280x1024
                            imdata = ReadRAW16bit(filen, 1280, 1024, 1);
                        case 2457600
                            imdata = ReadRAW16bit(filen, 1280, 960);
                        case 4456448
                            imdata = ReadRAW16bit(filen, 2048, 1088, 1);
                    end
                case '.txt'
                    imdata = rand(100);
                    set(o.statusbar, 'Text', 'Not implemented txt files yet...... :(');
            end
        end
        
        function lineout_enable(o,v)
            if v % Enabled, let downsize the main axes
                set(o.ax.axmain, 'Position', [0.1 0.3 0.6 0.6], 'XTickLabel', [],...
                    'YTickLabel', []);
                set([o.ax.axvert o.ax.axhorz o.ax.hvertline o.ax.hhorzline], 'Visible', 'on');
            else % Disabled, as big as we want
                set(o.ax.axmain, 'Position', [0.1 0.1 0.85 0.85]);
                set([o.ax.axvert o.ax.axhorz o.ax.hvertline o.ax.hhorzline], 'Visible', 'off');
            end
        end
        
        function mainaxlim_changed(o,v)
            switch v
                case 0 % Change XLim
                    set(o.ax.axvert, 'YLim', get(o.ax.axmain, 'YLim'));
                case 1 % Changed YLim
                    set(o.ax.axhorz, 'XLim', get(o.ax.axmain, 'XLim'));
                case 2 % Changed CLim
                    if get(o.ax.lineouts, 'Value') % Only set limits if using lineouts!
                        set(o.ax.axvert, 'XLim', get(o.ax.axmain, 'CLim'));
                        set(o.ax.axhorz, 'YLim', get(o.ax.axmain, 'CLim'));
                    end
            end
        end
        
        function lineout_changed(o,v)
            if v==0 % Means we need to pick a lineout position
                [x,y] = ginput(1);
                % The rest is done here, recursively 
                set(o.ax.lineout_xbox, 'Value', round(x));
                set(o.ax.lineout_ybox, 'Value', round(y));
            elseif v==1 % Changed x lineout position
                xval = get(o.ax.lineout_xbox, 'Value');
                cdata = get(o.ax.himage, 'CData');
                if get(o.ax.lineouts, 'Value')
                    set(o.ax.hvertline, 'XData', cdata(:,xval), 'YData', 1:size(cdata,1));
                else
                    set(o.ax.hvertline, 'XData', sum(cdata,2), 'YData', 1:size(cdata,1));
                    set(o.ax.axvert, 'XLim', [min(sum(cdata,2)) max(sum(cdata,2))]);
                end
            elseif v==2
                yval = get(o.ax.lineout_ybox, 'Value');
                cdata = get(o.ax.himage, 'CData');
                if get(o.ax.lineouts, 'Value')
                    set(o.ax.hhorzline, 'YData', cdata(yval,:), 'XData', 1:size(cdata,2));
                else
                    set(o.ax.hhorzline, 'YData', sum(cdata,1), 'XData', 1:size(cdata,2));
                    set(o.ax.axhorz, 'YLim', [min(sum(cdata,1)) max(sum(cdata,1))]);
                end
            end
        end
        
        function line_changed(o,v)
            if v % We selected FWBs
                set(o.ax.lineouts, 'Value', 0);
                set(o.ax.FWBs, 'Value', 1);
            else
                set(o.ax.FWBs, 'Value', 0);
                set(o.ax.lineouts, 'Value', 1);
            end
        end
        
        function colormap_changed(o,v)
            if v==0
                val = get(o.controls.colmap, 'Value');
                colormap(o.ax.axmain, o.colmaps{val,1});
            else
                colormap(o.ax.axmain, o.colmaps{v,1});
            end
        end
        
        function load_settings(o, v)
            if v % load
                
            else % save
                
            end
        end
    end
end