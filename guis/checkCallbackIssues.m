% checkCallbackIssues.m
%
% try to understand the callback issue between HG1 and HG2.

hfig = figure(24242); clf(hfig);
ax = gca;
him = imagesc(rand(480, 640));
addlistener(him, 'CData', 'PostSet', @(s,e) disp('here!!!'));

pause(0.5);
set(him, 'CData', randi(480, 640));