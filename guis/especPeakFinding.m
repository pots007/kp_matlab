%plotEspec2Warp_raw.m

% Simple gui to go through images of ATA2 data
% The thing plots it, and if it's been analysed before, loads that
% Uses peakfind and some above BG fraction to help find peak and maximum
% energy position.

% This ought to be very simple :p

classdef especPeakFinding < uiextras.VBox
    
    properties
        imfig
        dat
        fig
        ax
    end
    
    methods
        %% Creation
        function o = especPeakFinding
            par = figure(99); clf(99);
            o@uiextras.VBox('Parent', par);
            set(par, 'Toolbar', 'none', 'MenuBar', 'none', 'Color', 'w',...
                'Name', 'Espec Peak Finding helper', 'NumberTitle', 'off', 'Position', [200 200 200 500],...
                'HandleVisibility', 'callback', 'CloseRequestFcn', @(s,e)delete(o));
            
            hpan = uiextras.Panel('Parent', o, 'Title', 'File controls', 'Padding', 5);
            vbox = uiextras.VBox('Parent', hpan);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', {'Data' 'folder'}, 'Parent', hbox);
            o.dat.datafol = '/Users/kristjanpoder/';
            o.fig.datafol = uicontrol('Style', 'edit', 'String', 'lala', 'Parent', hbox,...
                'Callback', @o.setPath, 'Tag', 'datafol');
            uicontrol('Style', 'pushbutton', 'String', '...', 'Parent', hbox,...
                'Callback', @o.setPath, 'Tag', 'datafol');
            hbox.Sizes = [35 -1 20];
            o.fig.filebox = uicontrol('Style', 'listbox', 'String', '', 'Parent', vbox, ...
                'Callback', @o.fileSelected);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', {'Track' 'file'}, 'Parent', hbox);
            o.dat.trackfile = '/Users/kristjanpoder/';
            o.fig.trackfile = uicontrol('Style', 'edit', 'String', 'lala', ...
                'Parent', hbox, 'Callback', @o.loadTrackFile);
            uicontrol('Style', 'pushbutton', 'String', '...', 'Parent', hbox,...
                'Callback', @o.setFile, 'Tag', 'trackfile');
            hbox.Sizes = [35 -1 20];
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Screen id', 'Parent', hbox);
            o.fig.scrID = uicontrol('Style', 'popupmenu', 'String', {'1' '2' '3'}, 'Value', 1, ...
                'Parent', hbox, 'Callback', @o.loadTrackFile);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', {'Save' 'folder'}, 'Parent', hbox);
            o.dat.savefol = '/Users/kristjanpoder/';
            o.fig.savefol = uicontrol('Style', 'edit', 'String', 'lala', ...
                'Parent', hbox, 'Callback', @o.setPath, 'Tag', 'savefol');
            uicontrol('Style', 'pushbutton', 'String', '...', 'Parent', hbox,...
                'Callback', @o.setPath, 'Tag', 'savefol');
            hbox.Sizes = [35 -1 20];
            vbox.Sizes = [-1 180 -1 -1 -1];
            % Spectra controls and whatnot
            hpan = uiextras.Panel('Parent', o, 'Title', 'Spectra controls', 'Padding', 5);
            vbox = uiextras.VBox('Parent', hpan);  
            uicontrol('Style', 'pushbutton', 'String', 'Set HE BG region', 'Parent', vbox, 'Callback', @o.moveBG);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'pushbutton', 'String', '<', 'Parent', hbox, 'Tag', '0maxe',...
                'Callback', @o.moveFeature);
            o.fig.max = uicontrol('Style', 'edit', 'String', '0', 'Parent', hbox,...
                'Tag', '2maxe', 'Callback', @o.moveFeature);
            uicontrol('Style', 'pushbutton', 'String', '>', 'Parent', hbox, 'tag', '1maxe',...
                'Callback', @o.moveFeature);
            o.fig.maxetext = uicontrol('Style', 'text', 'String', 'Max:  MeV + MeV - MeV', 'Parent', vbox,...
                'ForeGroundColor', 'r');
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'pushbutton', 'String', '<', 'Parent', hbox, 'Tag', '0peake',...
                'Callback', @o.moveFeature);
            o.fig.peak = uicontrol('Style', 'edit', 'String', '0', 'Parent', hbox,...
                'Tag', '2peake', 'Callback', @o.moveFeature);
            uicontrol('Style', 'pushbutton', 'String', '>', 'Parent', hbox, 'Tag', '1peake',...
                'Callback', @o.moveFeature);
            o.fig.peaketext = uicontrol('Style', 'text', 'String', 'Peak:  MeV + MeV - MeV', 'Parent', vbox);
            uicontrol('Style', 'pushbutton', 'String', 'Find max and peak', 'Parent', vbox, ...
                'Callback', @o.findMaxPeak);
            o.fig.poo = uicontrol('Style', 'checkbox', 'String', 'Is it poo shot?', 'Parent', vbox, ...
                'Callback', @o.togglePoo);
            
            hpan = uiextras.Panel('Parent', o, 'Title', 'Save controls', 'Padding', 5);
            vbox = uiextras.VBox('Parent', hpan);  %hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'pushbutton', 'String', 'Save results', 'Parent', vbox,...
                'Callback', @o.saveShotdata);
            uicontrol('Style', 'pushbutton', 'string', 'Save image', 'Parent', vbox,...
                'Callback', @o.saveShotplot);
            o.imfig.fig = figure(98);
            set(o.imfig.fig, 'Color', 'w', 'Position', [405 200 900 600]);
            o.imfig.ax = axes('Parent', o.imfig.fig);
            o.Sizes = [-1 130 100];
            load('owncolourmaps');
            colormap(98, colmap.jet_white);
            o.loadSave(0);
            fmenu = uimenu(99, 'Label', 'Longcuts');
            uimenu(fmenu, 'Label', 'Save results', 'Callback', @o.saveShotdata, 'Accelerator', 'S');
            uimenu(fmenu, 'Label', 'Save plot', 'Callback', @o.saveShotplot, 'Accelerator', 'D');
            uimenu(fmenu, 'Label', 'Find max and peak', 'Callback', @o.findMaxPeak, 'Accelerator', 'F');
            uimenu(fmenu, 'Label', 'Set as not poo', 'Callback', @o.togglePooGood, 'Accelerator', 'P');
            assignin('base', 'ATA2Especs', o);
        end
        
        %% Stuff
        
        function fileSelected(o,e,~)
            if strcmp(get(99,'SelectionType'), 'open')
                ind = get(e, 'Value');
                o.openMatFile(ind);
            end
        end
        
        function findMaxPeak(o,~,~)
            % Again, the really useful function with loads of GUI around it
            hh = findobj(o.imfig.ax, 'Tag', 'lineout');
            sig = get(hh, 'YData'); xdat = get(hh, 'XData');
            % Starting BG from rigthhand side, hopefully works
            bg = mean(sig(1:100));
            bgfact = .02;
            [~,mind] = max(sig);
            subsig = sig(mind:end);
            maxind = find(subsig<(bg + abs(bgfact*bg)), 1, 'first');            
            set(o.fig.max, 'String', num2str(mind+maxind));
            o.dat.shotdata.max.pixel = mind+maxind;
            % New and hopefully better method 
            bgrect = get(findobj(o.imfig.ax, 'Tag', 'BGrectangle'), 'Position');
            [~,ind1] = min(abs(xdat-bgrect(1)));
            [~,ind2] = min(abs(xdat-(bgrect(1)+bgrect(3))));
            bg = mean(sig(ind1:ind2));
            bgfact = 30*std(sig(ind1:ind2));
            maxind = find(sig>(bg+bgfact), 1, 'last');
            set(o.fig.max, 'String', num2str(maxind));
            o.dat.shotdata.max.pixel = maxind;            
            
            % And for peak as well
            ylims = get(o.imfig.ax, 'YLim');
            range = ylims(1) + 0.6*(ylims(2)-ylims(1));
            [~,locs] = findpeaks(sig, 'MinPeakDistance', 100, 'MinPeakHeight', range);
            if isempty(locs); return; end;
            peakind = locs(end);
            set(o.fig.peak, 'String', num2str(peakind));
            o.dat.shotdata.peak.pixel = peakind;
            o.updateFeaturePlot;
        end
        
        function folderSelected(o,~,~)
            ttemp = dir(fullfile(o.dat.datafol, '*.mat'));
            if isempty(ttemp);
                set(o.fig.filebox, 'String', '', 'Value', 1);
                return;
            end
            flist = {ttemp.name};
            set(o.fig.filebox, 'String', flist, 'Value', 1, 'ListboxTop', 1);
        end
        
        function loadSave(o,v)
            if v % Saving
                stat.datafol = o.dat.datafol;
                stat.trackfile = o.dat.trackfile;
                stat.savefol = o.dat.savefol;
                stat.BGrect = o.dat.BGrect;
                save(fullfile(getDropboxPath, 'MATLAB', 'guis', 'especPeakFindState.mat'), 'stat');
            else % Loading
                if exist(fullfile(getDropboxPath, 'MATLAB', 'guis', 'especPeakFindState.mat'), 'file')
                    load(fullfile(getDropboxPath, 'MATLAB', 'guis', 'especPeakFindState.mat'));
                    o.dat.datafol = stat.datafol;
                    set(o.fig.datafol, 'String', stat.datafol);
                    o.folderSelected;
                    o.dat.trackfile = stat.trackfile;
                    set(o.fig.trackfile, 'String', stat.trackfile);
                    o.dat.savefol = stat.savefol;
                    set(o.fig.savefol, 'String', stat.savefol);
                    o.dat.BGrect = stat.BGrect;
                    o.loadTrackFile;
                end
            end
        end
        
        function loadTrackFile(o,~,~)
            fname = o.dat.trackfile;
            try
                ff = load(fname);
                o.dat.trackfile = fname;
                o.dat.tracking = ff.tracking;
                scrID = get(o.fig.scrID, 'Value');
                alongs = ff.tracking.screen(scrID).alongscreen;
                logm = ~isnan(alongs);
                o.dat.tracking.lowfit = fit(alongs(logm(:,2),2), alongs(logm(:,2),1), 'spline');
                o.dat.tracking.valfit = fit(alongs(logm(:,3),3), alongs(logm(:,3),1), 'spline');
                o.dat.tracking.highfit = fit(alongs(logm(:,4),4), alongs(logm(:,4),1), 'spline');
            catch err
                disp(err.message)
                %set(o.status, 'String', 'Not a valid tracking file');
                o.dat.tracking = [];
            end
        end
        
        function moveFeature(o,e,~)
            tag = get(e, 'Tag');
            dir = str2double(tag(1)); id = tag(2:end-1);
            if dir==2
                newval = str2double(get(o.fig.(id), 'String'));
            else
                oldval = o.dat.shotdata.(id).pixel;
                dval = 5;
                newval = oldval - (-1)^dir*dval;
            end
            o.dat.shotdata.(id).pixel = newval;
            set(o.fig.(id), 'String', num2str(newval));
            o.updateFeaturePlot;
        end
        
        function moveBG(o,e,~)
            if ~isfield(o.dat.shotdata, 'image'); return; end;
            set(e, 'String', 'Double click on rectangle when done');
            ylim = get(o.imfig.ax, 'YLim');
            xdat = get(findobj(o.imfig.ax, 'Tag', 'lineout'), 'XData');
            if isfield(o.dat, 'BGrect')
                rect = o.dat.BGrect;
                delete(findobj(o.imfig.ax, 'Tag', 'BGrectangle'));
            else
                rect = [xdat(end-30) ylim(1) xdat(end)-xdat(end-30) ylim(2)-ylim(1)];
            end
            temprect = imrect(o.imfig.ax, rect);
            fcn = makeConstrainToRectFcn('imrect',[min(xdat) max(xdat)],ylim);
            setPositionConstraintFcn(temprect,fcn);
            setColor(temprect, 'k');
            final_pos = wait(temprect); % Wait until user finishes positioning it
            delete(temprect);
            rectangle('Position', final_pos, 'Parent', o.imfig.ax, 'tag', 'BGrectangle', 'EdgeColor', 'r');
            o.dat.BGrect = round(final_pos);
            set(e, 'String', 'Set HE BG region');
        end
        
        function openMatFile(o,v)
            filen = get(o.fig.filebox, 'String');
            if v>length(filen); return; end;
            set(o.fig.filebox, 'Value', v);
            filen = filen{v};
            if ~exist(fullfile(o.dat.datafol, filen), 'file')
                disp('File does not seem to exist');
                return;
            end
            ff = load(fullfile(o.dat.datafol, filen));
            if ~isfield(ff, 'shotdata'); disp('Not valid file'); return; end;
            o.dat.filen = filen;
            f = ff.shotdata;
            o.dat.shotdata.image = f.image;
            o.dat.shotdata.alongscreen = f.alongscreen;
            if isfield(f, 'totQ')
                o.dat.shotdata.totQ = f.totQ; disp([num2str(f.totQ) ' pC']);
            end
            o.dat.shotdata.poo = f.poo;
            set(o.fig.poo, 'Value', f.poo);
            if isfield(f, 'divax'); o.dat.shotdata.divax = f.divax; end;
            if isfield(f, 'max');
                o.dat.shotdata.max = f.max;
                set(o.fig.max, 'String', num2str(f.max.pixel));
            else
                o.dat.shotdata.max.pixel = [];
            end;
            if isfield(f, 'peak');
                o.dat.shotdata.peak = f.peak;
                set(o.fig.peak, 'String', num2str(f.peak.pixel));
            else
                o.dat.shotdata.peak.pixel = [];
            end;
            o.plotEspec;
        end
        
        function plotEspec(o)
            if ~ishandle(o.imfig.fig); o.imfig.fig = figure(o.imfig.fig); end;
            if ishandle(o.imfig.ax)
                cla(o.imfig.ax);
            else
                o.imfig.ax = axes('Parent', o.imfig.fig);
            end
            set(o.imfig.ax, 'NextPlot', 'add', 'YDir', 'normal', 'Box', 'on');
            if isfield(o.dat.tracking, 'valfit')
                xaxis = o.dat.tracking.valfit(o.dat.shotdata.alongscreen);
            else
                xaxis = 1:size(o.dat.shotdata.image,2);
            end
            if isfield(o.dat.shotdata, 'divax');
                yaxis = o.dat.shotdata.divax;
                ylab = 'Divergence / mrad';
            else
                yaxis = 1:size(o.dat.shotdata.image,1);
                ylab = 'Divergence / a.u.';
            end
            co2disp = -xaxis(1:end-1) + xaxis(2:end); %To correct for dispersion
            co2disp = repmat(co2disp, 1, length(yaxis))';
            linen = linspace(min(xaxis), max(xaxis), 1000); %Interpolate to linear grid to avoid pcolor
            image = o.dat.shotdata.image(:,1:end-1);%./co2disp;
            linimag = zeros(size(image,1),1000);
            for l=1:size(image,1)
                linimag(l,:) = interp1(xaxis(1:end-1), image(l,:), linen, 'pchip');
            end
            imagesc(xaxis, yaxis, (abs(linimag)), 'Parent', o.imfig.ax);
            lineout = sum(abs(o.dat.shotdata.image),1);
            ylims = get(o.imfig.ax, 'YLim');
            lineout = lineout/max(lineout)*(ylims(2)-ylims(1))*0.8 + ylims(1);
            plot(o.imfig.ax, xaxis, smooth(lineout), '--k', 'Tag', 'lineout', 'LineWidth', 2);
            if ~isempty(o.dat.shotdata.peak.pixel)
                peakD = o.dat.shotdata.alongscreen(o.dat.shotdata.peak.pixel);
                line('XData', o.dat.tracking.valfit(peakD)*[1 1], 'Color', 'k',...
                    'YData', ylims, 'Parent', o.imfig.ax, 'Tag', 'peak', 'LineStyle', '--');
            end
            if ~isempty(o.dat.shotdata.max.pixel)
                maxD = o.dat.shotdata.alongscreen(o.dat.shotdata.max.pixel);
                line('XData', o.dat.tracking.valfit(maxD)*[1 1], 'Color', 'r',...
                    'YData', ylims, 'Parent', o.imfig.ax, 'Tag', 'max', 'LineStyle', '--');
            end
            xlabel(o.imfig.ax, 'Energy / MeV');
            ylabel(o.imfig.ax, ylab);
            set(o.imfig.ax, 'Box', 'on', 'LineWidth', 2);
            rectangle('Position', o.dat.BGrect, 'Parent', o.imfig.ax, 'tag', 'BGrectangle', 'EdgeColor', 'r');
            setAllFonts(o.imfig.fig, 20);
            o.updateFeaturePlot;
        end
        
        %%
        function saveShotdata(o,~,~)
            fname = o.dat.filen;
            shotdata = o.dat.shotdata;
            if ~exist(o.dat.savefol, 'dir'); return; end
            save(fullfile(o.dat.savefol, fname), 'shotdata');
        end
        
        function saveShotplot(o,~,~)
            disp('saving')
            export_fig(fullfile(o.dat.savefol, o.dat.filen(1:end-4)), '-pdf', '-nocrop', o.imfig.fig);
        end
        
        function setFile(o,e,~)
            typ = get(e, 'Tag');
            filen = uigetfile(o.dat.trackfile, 'Select tracking file');
            if filen==0; return; end
            o.dat.(typ) = filen;
            set(o.fig.(typ), 'String', filen);
            o.loadTrackFile;
        end
        
        function setPath(o,e,~)
            typ = get(e, 'Tag');
            if strcmp(get(e, 'Style'), 'edit')
                filen = get(e, 'String');
            else
                filen = uigetdir(o.dat.(typ), 'Select folder');
                if filen==0; return; end
                set(o.fig.(typ), 'String', filen);
            end
            o.dat.(typ) = filen;
            if strcmp(typ, 'datafol'); o.folderSelected; end;
        end
        
        function togglePoo(o,e,~)
            o.dat.shotdata.poo = get(e, 'Value');
        end
        
        function togglePooGood(o,~,~)
            v = get(o.fig.poo, 'Value');
            set(o.fig.poo, 'Value', ~v);
            o.togglePoo(o.fig.poo);
        end
        
        function updateFeaturePlot(o)
            % Update max plot
            if ~isempty(o.dat.shotdata.max.pixel)
                maxD = o.dat.shotdata.alongscreen(o.dat.shotdata.max.pixel);
                o.dat.shotdata.max.val = o.dat.tracking.valfit(maxD);
                o.dat.shotdata.max.low = o.dat.tracking.lowfit(maxD) - o.dat.shotdata.max.val;
                o.dat.shotdata.max.high = o.dat.tracking.highfit(maxD) - o.dat.shotdata.max.val;
                set(o.fig.maxetext, 'String', sprintf('Max: %2.0f MeV %+2.0f MeV %+2.0f MeV',...
                    o.dat.shotdata.max.val, o.dat.shotdata.max.high, o.dat.shotdata.max.low));
                hmax = findobj(o.imfig.ax, 'Tag', 'max');
                ylims = get(o.imfig.ax, 'YLim');
                if isempty(hmax)
                    line('Parent', o.imfig.ax, 'XData', o.dat.tracking.valfit(maxD)*[1 1], ...
                        'Tag', 'max', 'LineStyle', '--', 'Color', 'r', 'YData', ylims);
                else
                    set(hmax, 'XData', o.dat.tracking.valfit(maxD)*[1 1])
                end
                htext = findobj(o.imfig.ax, 'Tag', 'maxtext');
                if ~isempty(htext); delete(htext); end;
                text(o.dat.tracking.valfit(maxD), 0.9*(ylims(2)-ylims(1))+ylims(1), ...
                    ['E = ' num2str(o.dat.tracking.valfit(maxD),'%3.1f') ' MeV'], 'FontSize', 20, ...
                    'Parent', o.imfig.ax, 'Color', 'r', 'tag', 'maxtext');
            else
                set(o.fig.maxetext, 'String', sprintf('Max: MeV + MeV - MeV'));
                set(o.fig.max, 'String', '0');
            end
            % Update peak plot
            if ~isempty(o.dat.shotdata.peak.pixel)
                peakD = o.dat.shotdata.alongscreen(o.dat.shotdata.peak.pixel);
                o.dat.shotdata.peak.val = o.dat.tracking.valfit(peakD);
                o.dat.shotdata.peak.low = o.dat.tracking.lowfit(peakD)-o.dat.shotdata.peak.val;
                o.dat.shotdata.peak.high = o.dat.tracking.highfit(peakD)-o.dat.shotdata.peak.val;
                set(o.fig.peaketext, 'String', sprintf('Peak: %2.0f MeV  %+2.0f MeV  %+2.0f MeV',...
                    o.dat.shotdata.peak.val, o.dat.shotdata.peak.high, o.dat.shotdata.peak.low));
                hmax = findobj(o.imfig.ax, 'Tag', 'peak');
                ylims = get(o.imfig.ax, 'YLim');
                if isempty(hmax)
                    line('Parent', o.imfig.ax, 'XData', o.dat.tracking.valfit(peakD)*[1 1], ...
                        'Tag', 'peak', 'LineStyle', '--', 'Color', 'k', 'YData', ylims);
                else
                    set(hmax, 'XData', o.dat.tracking.valfit(peakD)*[1 1])
                end
                htext = findobj(o.imfig.ax, 'Tag', 'peaktext');
                if ~isempty(htext); delete(htext); end;
                text(o.dat.tracking.valfit(peakD), 0.85*(ylims(2)-ylims(1))+ylims(1), ...
                    ['E = ' num2str(o.dat.tracking.valfit(peakD),'%3.1f') ' MeV'], 'FontSize', 20, ...
                    'Parent', o.imfig.ax, 'Color', 'k', 'tag', 'peaktext');
            else
                set(o.fig.peaketext, 'String', sprintf('Max: MeV + MeV - MeV'));
                set(o.fig.peak, 'String', '0');
            end
        end
        
        function delete(o)
            o.loadSave(1);
            delete(98);
            delete(99)
        end
    end
end