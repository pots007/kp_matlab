% daqTestGUI.m
%
% GUI tool to help testing the DAQ data recording etc
%
% Allows the user to select a list of channels, then set the senders for
% these. If some cameras are offline, online these. Also sets the
% triggers required to Burst mode. Then fires a number of shots and keeps
% track of how many images each server sends etc. Finally, allows the user
% to extract the data for that particular run and check what was saved and
% what was not.

% TODO: Camera picker, ADC channel and spectrometer DAQ send updater

classdef daqTestGUI < handle
    properties
        fig
        daq_status
        channels
        dat
        tim
        debug = 0
    end
    
    methods
        function self = daqTestGUI
            addpath('/local/lib');
            % Class and GUI constructor
            self.fig.parent = figure(98789); clf(self.fig.parent);
            % Set consistent font sizes and make it look nice
            if (ismac)
                self.dat.fsize = 13;
            else
                self.dat.fsize = 10;
            end
            set(self.fig.parent, 'Toolbar', 'none', 'MenuBar', 'none',...
                'Name', 'DAQ test tool', 'NumberTitle', 'off',...
                'Units', 'Pixels', 'Position', [200 200 1400 850],...
                'DefaultUIControlFontSize', self.dat.fsize, 'Color', 'w',...
                'HandleVisibility', 'callback', 'CloseRequestFcn', @self.delete);
            
            % Start  of GUI code
            vboxMain = uiextras.HBox('Parent', self.fig.parent);
            hboxControl = uiextras.VBox('Parent', vboxMain);
            
            % ------------ Run controls
            hpanCamera = uiextras.Panel('Parent', hboxControl, 'Title', 'Camera channels control', 'Padding', 3);
            vboxCamera = uiextras.VBox('Parent', hpanCamera);
            hbox = uiextras.HBox('Parent', vboxCamera);
            uicontrol('Style', 'text', 'String', 'Camera list:', 'Parent', hbox);
            uicontrol('Style', 'pushbutton', 'String', 'Load file',...
                'Parent', hbox, 'Tag', 'LoadCameras', 'Callback', @self.loadCameraList);
            uicontrol('Style', 'pushbutton', 'String', 'Pick',...
                'Parent', hbox, 'Tag', 'PickCameras', 'Callback', @self.pickCamera);
            uicontrol('Style', 'pushbutton', 'String', 'Save to file',...
                'Parent', hbox, 'Tag', 'SaveCameras', 'Callback', @self.saveCameraList);
            uicontrol('Style', 'text', 'String', 'Set all:', 'Parent', hbox);
            uicontrol('Style', 'pushbutton', 'String', 'Online',...
                'Parent', hbox, 'Tag', 'OnlineCameras', 'Callback', @self.setCamerasOnline);
            uicontrol('Style', 'pushbutton', 'String', 'Started',...
                'Parent', hbox, 'Tag', 'StartCameras', 'Callback', @self.setCamerasStarted);
            uicontrol('Style', 'pushbutton', 'String', 'Image senders',...
                'Parent', hbox, 'Tag', 'SendersCamerasOn', 'Callback', @self.setSendersCameras);
            uicontrol('Style', 'pushbutton', 'String', 'Senders clear',...
                'Parent', hbox, 'Tag', 'SendersCamerasOff', 'Callback', @self.clearSendersCameras);
            
            % ----------- Ocean and ADC controls
            hpanADC = uiextras.Panel('Parent', hboxControl, 'Title', 'ADC+Ocean channels control', 'Padding', 3);
            vboxADC = uiextras.VBox('Parent', hpanADC);
            hbox = uiextras.HBox('Parent', vboxADC);
            uicontrol('Style', 'text', 'String', 'Save:', 'Parent', hbox);
            self.fig.uiSaveADC = uicontrol('Style', 'checkbox', 'Parent', hbox,...
                'String', 'ADC channels', 'Tag', 'SaveADC');
            self.fig.uiSavePreInt = uicontrol('Style', 'checkbox', 'Parent', hbox,...
                'String', 'PreInt (PI7)', 'Tag', 'SavePreInt');
            self.fig.uiSavePostInt = uicontrol('Style', 'checkbox', 'Parent', hbox,...
                'String', 'PostInt (PI11)', 'Tag', 'SavePostInt');
            self.fig.uiSaveMPA2 = uicontrol('Style', 'checkbox', 'Parent', hbox,...
                'String', 'MPA2 (PI14)', 'Tag', 'SaveMPA');
            uicontrol('Style', 'text', 'String', 'Set all:', 'Parent', hbox);
            uicontrol('Style', 'pushbutton', 'String', 'Senders',...
                'Parent', hbox, 'Tag', 'SendersADC', 'Callback', @self.setSendersADCOcean);
            
            % ---------- Triggering and shooting
            hpanTrig = uiextras.Panel('Parent', hboxControl, 'Title', 'Triggers and shots', 'Padding', 3);
            vboxTrig = uiextras.VBox('Parent', hpanTrig);
            hbox = uiextras.HBox('Parent', vboxTrig);
            uicontrol('Parent', hbox, 'String', 'Set triggers to:', 'Style', 'text');
            uicontrol('Parent', hbox, 'String', 'BURST', ...
                'Style', 'Pushbutton', 'Tag', '1', 'Callback', @self.setTriggers);
            uicontrol('Parent', hbox, 'String', 'SYNC', ...
                'Style', 'Pushbutton', 'Tag', '0', 'Callback', @self.setTriggers);
            % And shooting
            hbox = uiextras.HBox('Parent', vboxTrig);
            self.fig.nShots = javaNumSpinner2(10, 1, 50, 1, hbox);
            uicontrol('Style', 'text', 'String', ' shots at ', 'Parent', hbox);
            self.fig.freq = javaNumSpinner2(0.5, 0.1, 1, 0.1, hbox);
            uicontrol('Style', 'text', 'String', 'Hz: ', 'Parent', hbox);
            self.fig.fireButton = uicontrol('Style', 'pushbutton', 'String', 'FIRE', ...
                'Parent', hbox, 'Callback', @self.startStopLaser);
            hbox.Sizes = [60 110 50 60 120];
            
            % ---------- DAQ access etc
            hpanDAQ = uiextras.Panel('Parent', hboxControl, 'Title', 'Channels', 'Padding', 2);
            vbox = uiextras.VBox('Parent', hpanDAQ);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'DAQ status:', 'Parent', hbox);
            self.fig.daqStatus = uicontrol('Style', 'text', 'String', 'IDLE', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'DAQ run:', 'Parent', hbox);
            self.fig.daqRun = uicontrol('Style', 'text', 'String', '#####', 'Parent', hbox);
            uicontrol('style', 'pushbutton', 'String', 'Get DAQ data for this run',...
                'Parent', hbox, 'Callback', @self.getDataInDAQ);
            
            % ---------- Channels table
            hpanTable = uiextras.Panel('Parent', hboxControl, 'Title', 'Channels', 'Padding', 2);
            hbox = uiextras.VBox('Parent', hpanTable);
            self.fig.uiTable = uitable('Parent', hbox,'ColumnName',...
                {'Channel Name', 'Online', 'Started', 'Last Frame #', 'Sender # Sent', '# Received', '# in DAQ'},...
                'ColumnWidth', {400, 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'},...
                'ColumnFormat', {'char', 'logical', 'logical', 'numeric', 'numeric', 'numeric', 'numeric'},...
                'Data', {' ', [],[], [], [], [], []});
            
            hboxControl.Sizes = [50, 50, 70, 50, -1];
            
            % ------------ The log
            hpanLog = uiextras.Panel('Parent', vboxMain, 'Title', 'Log', 'Padding', 2);
            hbox = uiextras.HBox('Parent', hpanLog);
            %self.fig.logWin = uicontrol('Parent', hbox, 'Style', 'listbox', ...
            %    'String', {['Started up at ' datestr(now)]});
            self.fig.logWin = uicontrol('Style', 'edit', 'String', '', 'Parent', hbox,...
                'HorizontalAlignment', 'left', 'Max', 1000, 'FontName', 'Courier',...
                'FontSize', 0.9*get(self.fig.parent, 'DefaultUIControlFontSize')); 
            % Make the text container not editable
            jEdit = findjobj(self.fig.logWin,'nomenu'); %get the UIScrollPane container
            jEdit = jEdit.getComponent(0).getComponent(0);
            set(jEdit,'Editable',0);
            self.fig.jlogWin = jEdit;
            
            vboxMain.Sizes = [-1 500];
           
            % And default data structures
            self.dat.allCamList = {};
            self.dat.camList = {};
            self.dat.OceanList = {};
            self.dat.ADCList = {};
            self.dat.OceanConf = {'PreInt', 7, 'PRE.INTERACTION';...
                      'PostInt', 11, 'POST.INTERACTION';...
                      'MPA2', 14, 'MPA2'};
            self.dat.daq = struct('prevStat', '', 'runs', []);
            self.dat.sPulse = 'FLASH.DIAG/FFWD.AMPLITUDE.LASER.CONTROL/SELECTPULSE/';
            self.dat.camStat = {};
            self.tim.chans = timer.empty;
            self.tim.laser = timer('Period', 2, 'TimerFcn', @self.fireLaser,...
                'ExecutionMode', 'fixedRate');
            self.tim.daq = timer('Period', 0.25, 'TimerFcn', @self.checkDaq,...
                'ExecutionMode', 'fixedRate');
            start(self.tim.daq); % Start the DAQ follower...
            
            
            warning('off', 'doocsread:processChannels:unavailable_property')
            assignin('base', 'DAQTestGUI', self);
            self.disp_('Started up')
        end
        
%% -----------     Sender functions
        function setADCSenders(self, v)
            if isempty(self.dat.ADCList)
                self.dat.ADCList = self.getADCChannels;
            end
            % Each channel
            for i  = 1:length(self.dat.ADCList)
                self.disp_([self.dat.ADCList{i} 'DAQ_ENABLE ' num2str(v)]);
                s = doocswrite([self.dat.ADCList{i} 'DAQ_ENABLE'], v);
            end
            % And then the Sender for that machine
            s = doocswrite('FLASH.DIAG/FFWD.ADC/FLASHFWDCPUDIAG2.SND/DAQ.SND_ON', v);
            self.updateMainTable;
        end
        
        function setSendersADCOcean(self, ~, ~)
            % Sets all senders for ADC and Oceans to be ON
            if self.fig.uiSaveADC.Value
                self.setADCSenders(1);
                self.dat.ADCList = self.getADCChannels;
            else
                self.setADCSenders(0);
                self.dat.ADCList = {};
            end
            
            Oceans = {'PreInt', 'PostInt', 'MPA2'};
            OceansNos = [7, 11, 14];
            oConf = self.dat.OceanConf;
            for i=1:length(Oceans)
                %ind = find(cellfun(@(x) x==o, oConf(:,2)))
                ss = sprintf('FLASH.DIAG/FFWD.OCEAN.DAQ/FLAFFWDPI%i.SND', oConf{i,2});%OceansNos(i));
                if self.fig.(['uiSave' oConf{i,1}]).Value
                    self.disp_([ss 'DAQ.SND_ON 1'])
                    s = doocswrite([ss 'DAQ.SND_ON'], 1);
                    if isempty(self.dat.OceanList)
                        self.dat.OceanList{end+1} = ss;
                    elseif ~contains(self.dat.OceanList, ss)
                        self.dat.OceanList{end+1} = ss;
                    end
                else
                    self.disp_([ss 'DAQ.SND_ON 0'])
                    s = doocswrite([ss 'DAQ.SND_ON'], 0);
                    ind = find(strcmp(self.dat.OceanList, ss));
                    self.dat.OceanList(ind) = [];
                end
            end
%             if self.fig.uiSavePreInt.Value
%                 ss = 'FLASH.DIAG/FFWD.OCEAN.DAQ/FLAFFWDPI7.SND/';
%                 self.disp_([ss 'DAQ.SND_ON 1'])
%                 s = doocswrite([ss 'DAQ.SND_ON'], 1);
%                 self.dat.OceanList{end+1} = ss;
%             else
%                 %s = doocswrite('FLASH.DIAG/FFWD.OCEAN.DAQ/FLAFFWDPI7.SND/DAQ.SND_ON', 0);
%                 ind = strfind(self.dat.OceanList, ss); 
%                 self.dat.OceanList{ind}==[];
%             end
%             if self.fig.uiSavePostInt.Value
%                 self.disp_('FLASH.DIAG/FFWD.OCEAN.DAQ/FLAFFWDPI11.SND/DAQ.SND_ON 1')
%                 s = doocswrite('FLASH.DIAG/FFWD.OCEAN.DAQ/FLAFFWDPI11.SND/DAQ.SND_ON', 1);
%             end
%             if self.fig.uiSaveMPA2.Value
%                 self.disp_('FLASH.DIAG/FFWD.OCEAN.DAQ/FLAFFWDPI14.SND/DAQ.SND_ON 1')
%                 s = doocswrite('FLASH.DIAG/FFWD.OCEAN.DAQ/FLAFFWDPI14.SND/DAQ.SND_ON', 1);
%             end
            self.updateMainTable;
        end
        
        function setSendersCameras(self, ~, ~)
            for i=1:length(self.dat.camList)
                s = doocswrite([self.dat.camList{i} 'TO_DAQ'], 1);
                self.disp_([self.dat.camList{i} 'TO_DAQ 1']);
            end
        end
        
        function clearSendersCameras(self, ~, ~)
            for i=1:length(self.dat.camList)
                s = doocswrite([self.dat.camList{i} 'TO_DAQ'], 0);
                self.disp_([self.dat.camList{i} 'TO_DAQ 0']);
            end
        end
        
        %% DOOCS access functions
        
        function channels = getADCChannels(self)
            chans = doocsread('FLASH.DIAG/FFWD.ADC/*');
            chanNames = {chans.channel}';
            logm = ~contains(chanNames, {'SVR', 'SND'});
            channels = chanNames(logm);
        end
        
        function setCamerasOnline(self, ~, ~)
            for i=1:length(self.dat.camList)
                s = doocswrite([self.dat.camList{i} 'SET.ONLINE'], 1);
                self.disp_([self.dat.camList{i} 'SET.ONLINE 1']);
            end
        end
        
        function setCamerasStarted(self, ~, ~)
            for i=1:length(self.dat.camList)
                s = doocswrite([self.dat.camList{i} 'START'], 1);
                self.disp_([self.dat.camList{i} 'START 1']);
            end
        end
        
        function setTriggers(self, hSource, ~)
            tag = str2double(hSource.Tag);
            modeStr = {' set to SYNC', ' set to BURST'};
            trigChans = {'Trig cam MP2', 'Bond Lab 1', 'X2 timer',...
                'Bond Lab 2', 'Bond Lab 4'};
            for i=1:20
                chNameDat = doocsread([self.dat.sPulse 'CHANNEL_NAME_' num2str(i)]);
                chName = strip(chNameDat.data);
                if sum(contains(trigChans, chName))
                    self.disp_([chName, modeStr{tag+1}]);
                    s = doocswrite([self.dat.sPulse 'CHANNEL_MODES_' num2str(i)], 2*tag);
                end
            end
        end
        
        function checkDaq(self, ~, ~)
            % Get the DAQ status, update the panel, keep track of start and
            % end times for us for later.
            % TODO: Finish this guy!
            g = doocsread('TTF2.DAQ/FLASHFWD.RUN.CTR/DB.MGR/RC.STATE');
            self.fig.daqStatus.String = g.data.str_data_val;
            if strcmp(g.data.str_data_val, 'IDLE')
                self.fig.daqStatus.BackgroundColor = [1 0 0];
            elseif strcmp(g.data.str_data_val, 'RUN')
                self.fig.daqStatus.BackgroundColor = [0 1 0];
            end
            daq_run = doocsread('TTF2.DAQ/FLASHFWD.RUN.CTR/DB.MGR/RC.RUN.NUMBER');
            if ~isempty(daq_run.data)
                self.fig.daqRun.String = num2str(daq_run.data);
                if ~ismember(daq_run.data, self.dat.daq.runs)
                    self.dat.daq.runs(end+1) = daq_run.data;
                end
            end
            % If status is run and wasn't before, save the timestamp and
            % write it out. Also save the run number. If status was run and
            % is no more, save time as well!
            if isempty(self.dat.daq.prevStat)
                self.dat.daq.prevStat = g.data.str_data_val;
            end
            if ~strcmp(self.dat.daq.prevStat, g.data.str_data_val)
                if strcmp(self.dat.daq.prevStat, 'IDLE')
                    self.dat.daq.startTime = now;
                elseif strcmp(self.dat.daq.prevStat, 'RUN')
                    self.dat.daq.stopTime = now;
                end
                self.dat.daq.prevStat = g.data.str_data_val;
            end
        end
        
        function channelTimerFcn(self, hTimer, ~)
            % Fired to check the status of a camera or a channel
            % Get the SET.ONLINE and START status for cameras, FRAME for
            % everything.
            doocsChan = hTimer.Tag;
            try
                if contains(doocsChan, '.CAM') % Do this for cameras
                    % Get the data from DOOCS
                    o = doocsread([doocsChan 'SET.ONLINE']);
                    s = doocsread([doocsChan 'START']);
                    f = doocsread([doocsChan 'FRAME']);
                    sentNo = self.getSenderSentCount(doocsChan);
                    collNo = self.getCollectorCount(doocsChan);
                    % And add it to the table
                    d = self.fig.uiTable.Data;
                    ind = find(strcmp(d(:,1), doocsChan));
                    self.fig.uiTable.Data(ind, 2:6) = {logical(o.data), logical(s.data),...
                        f.data, sentNo, collNo};
                elseif contains(doocsChan, 'FFWD.ADC')
                    f = doocsread([doocsChan 'MACRO_PULSE_NUMBER']);
                    sentNo = self.getSenderSentCount(doocsChan);
                    collNo = self.getCollectorCount(doocsChan);
                    d = self.fig.uiTable.Data;
                    ind = find(strcmp(d(:,1), doocsChan));
                    self.fig.uiTable.Data(ind, 4:6) = {f.data, sentNo, collNo};%.data, sentNo, collNo};
                elseif contains(doocsChan, 'OCEAN')
                    sentNo = self.getSenderSentCount(doocsChan);
                    collNo = self.getCollectorCount(doocsChan);
                    d = self.fig.uiTable.Data;
                    ind = find(strcmp(d(:,1), doocsChan));
                    self.fig.uiTable.Data(ind, 5:6) = {sentNo, collNo};
                end
            catch err
                disp(err.stack)
            end
        end
        
        function frameNo = getSenderSentCount(self, camName)
            g = doocsread([self.getSenderName(camName) '/DAQ.SND_BLCNT']);
            if isempty(g.error); frameNo = g.data; else; disp([self.getSenderName(camName) '/DAQ.SND_BLCNT']); frameNo = -1; end
        end
        
        function frameNo = getCollectorCount(self, camName)
            frameNo = -1;
            % For this camera, first get the sender name
            senderName = self.getSenderName(camName);
            coll2snd = self.makeCollectorSenderTable;
            if isempty(coll2snd); return; end;
            ind = find(strcmp(coll2snd(:,1), senderName));
            if isempty(ind); return; end
            collName = ['TTF2.DAQ/FLASHFWD.COLLECT.FAST/' coll2snd{ind,2} '_PASSED'];
            g = doocsread(collName);
            if isempty(g.error); frameNo = g.data; end
        end
        
        function coll2snd = makeCollectorSenderTable(self)
            collBase = 'TTF2.DAQ/FLASHFWD.COLLECT.FAST/';
            coll2snd = {};
            for c=1:4
                for cc = 0:9
                    g = doocsread(sprintf('%sIMAGE%i/DAQ.CON%i_CONF', collBase, c, cc));
                    if isempty(g.error)
                        coll2snd(end+1,1:2) = {g.data.str_data_val, ...
                            sprintf('IMAGE%i/DAQ.CON%i',c,cc)}; %#ok<AGROW>
                    else
                        break
                    end
                end
            end
            for cc = 0:9
                g = doocsread(sprintf('%sNORMAL/DAQ.CON%i_CONF', collBase, cc));
                if isempty(g.error)
                    coll2snd(end+1,1:2) = {g.data.str_data_val, ...
                        sprintf('NORMAL/DAQ.CON%i', cc)}; %#ok<AGROW>
                else
                    break
                end
            end
            self.dat.coll2snd = coll2snd;
        end
        %% Laser firing and timers
        
        function fireLaser(self, ~, ~)
            self.disp_(['Fired shot no ' num2str(self.tim.laser.TasksExecuted)]);
            if ~self.debug
                s = doocsread([self.dat.sPulse 'START_STOP_BURST']);
                if s.data==0
                    %s = doocswrite([self.dat.sPulse 'START_STOP_BURST'], 1); %#ok<*NASGU>
                else
                    %s = doocswrite([self.dat.sPulse 'START_STOP_BURST'], 0);
                end
            end
            start(self.tim.chans)
        end
        
        function startStopLaser(self, ~, ~)
            if self.fig.fireButton.String=='FIRE'
                set(self.tim.laser, 'Period', 1/self.fig.freq.Value,...
                    'TasksToExecute', self.fig.nShots.Value,...
                    'StopFcn', @(s,e) set(self.fig.fireButton, 'String', 'FIRE'))
                start(self.tim.laser);
                self.fig.fireButton.String = 'STOP';
            else
                stop(self.tim.laser);
                self.fig.fireButton.String = 'FIRE';
            end
        end     
        
        function addTimer(self, doocsName)
            % Called to add a timer to monitor a particular channel.
            t = timer('ExecutionMode', 'singleShot', ... % %'Period', 0.25, ...
                'Tag', doocsName, 'TimerFcn', @self.channelTimerFcn);
            self.tim.chans(end+1) = t;
            %start(t);
        end
        
        %% DAQ access at the end functions
        
        function getDataInDAQ(self, ~, ~)
            daq = self.dat.daq;
            % Sanity checks
            if ~isfield(daq, 'startTime') || ~isfield(daq, 'stopTime')
                fprintf('No start or end time: have you stopped the run?')
                return
            end
            if isempty(daq.runs)
                fprintf('No runs detected: Did you run DAQ?');
                return
            end
            if daq.startTime > daq.stopTime
                fprintf('Run start time is earlier than stop: is a run going?');
                return
            end
            % Now start getting the data from DAQ!
            diags = self.fig.uiTable.Data(:,1);
            inp{1} = ['-TStart ' datestr(daq.startTime, 'yyyy-mm-ddTHH:MM:SS')];
            inp{2} = ['-TStop ' datestr(daq.stopTime, 'yyyy-mm-ddTHH:MM:SS')];
            inp{3} = '-Exp flashfwd';
            inp{4} = '-DDir /daq_data/flashfwd/EXP/';
            for i=1:length(diags)
                fprintf('Extracting info for diag %s\n', diags{i});
                if contains(diags{i}, 'OCEAN')
                    piNo = str2double(regexp(diags{i}, '\d+', 'match'));
                    ind = find(cellfun(@(x) x==piNo, self.dat.OceanConf(:,2)));
                    inp{5} = sprintf('-Chan FLASH.DIAG/FFWD.OCEAN.DAQ/%s', self.dat.OceanConf{ind,3});
                else
                    inp{5} = ['-Chan ' strip(diags{i}, '/')]; 
                end
                [data,err] = daq_read(inp);
                if ~isempty(err)
                    fprintf('Error getting diagnostic %s\n', diags{i});
                    disp(err)
                    continue
                end
                fieldnames(data);
                self.fig.uiTable.Data{i,end} = length(fieldnames(data));
            end
        end
        
        %% Utilities....
        
        function sender = getSenderName(self, camName)
            if contains(camName, '.CAM')
                ss = strsplit(camName, '/');
                if ~isempty(strfind(camName, 'FFWDDIAG'))
                    sender = ['FLASHFWDCPU' ss{2}(end-8:end-4) '.SND'];
                else
                    sender = [ss{2}(1:end-4) '.SND'];
                end
                sender = [ss{1} '/' ss{2} '/' sender];
            elseif contains(camName, 'FFWD.ADC')
                sender = 'FLASH.DIAG/FFWD.ADC/FLASHFWDCPUDIAG2.SND';
            elseif contains(camName, 'OCEAN')
                sender = camName;
            end
        end
        
        function updateMainTable(self, ~, ~)
            % Update cameras
            data = self.fig.uiTable.Data;
            chans = data(:,1);
            % Cameras
            if ~isempty(self.dat.camList)
                for i=1:length(self.dat.camList)
                    if ~contains(chans, self.dat.camList{i})
                        data(end+1,:) = {self.dat.camList{i}, 0, 0, 0, 0, 0,0};
                        self.addTimer(self.dat.camList{i});
                    end
                end
            end
            % ADC channels
            if ~isempty(self.dat.ADCList)
                for i=1:length(self.dat.ADCList)
                    if ~contains(chans, self.dat.ADCList{i})
                        data(end+1,:) = {self.dat.ADCList{i}, 0, 0, 0, 0, 0, 0};
                        self.addTimer(self.dat.ADCList{i});
                    end
                end
            end
            % Oceans
            if ~isempty(self.dat.OceanList)
                for i=1:length(self.dat.OceanList)
                    if ~contains(chans, self.dat.OceanList{i})
                        data(end+1,:) = {self.dat.OceanList{i}, 0, 0, 0, 0, 0, 0};
                        self.addTimer(self.dat.OceanList{i});
                    end
                end
            end
            % Remove the placeholder!
            if size(data,1)>1
                if data{1,1} == ' '; data(1,:) = []; end
            end
            self.fig.uiTable.Data = data;
        end
        
        function updateMainTableCam(self)
            if ~isempty(self.dat.camStat)
                for i=1:length(self.dat.camStat)
                    ind = find(strcmp(self.fig.uiTable.Data(:,1), self.dat.camStat{i,1}));
                    self.fig.uiTable.Data(ind,2:end) = self.dat.camStat(i, 2:end);
                end
            end
        end
        
        function loadCameraList(self, ~, ~)
            [filename,pathname] = uigetfile('*.*', 'Load chanlist', '/home/fflab28m/ChanLists');
            if isequal(filename,0) || isequal(pathname,0)
                return
            end
            chanList = getFileText(fullfile(pathname, filename));
            % Add to the camera list
            if isempty(self.dat.camList)
                self.dat.camList = chanList;
            else
                % Add cameras that are not in our list yet!
                [~,~,IB] = intersect(self.dat.camList, chanList);
                if ~isempty(IB)
                    chanList(IB) = [];
                    self.dat.camList = [self.dat.camList; chanList];
                end
            end
            % And update the main table...
            self.updateMainTable;
        end
        
        function pickCamera(self, ~, ~)
            if isempty(self.dat.allCamList)
                g = doocsread('FLASH.DIAG/*'); chans = {g.channel};
                fullDoocs = {};
                weWant = {'FLASHFWDCAM', 'FFWDDIAG'};
                weHate = {'TEST', 'UEYE'};
                logmY = contains(chans, weWant);
                logmN = ~contains(chans, weHate);
                fullDoocsServers = chans(logmY & logmN)';
                fullDoocs = {};
                for i=1:length(fullDoocsServers)
                    try
                        chans = doocsread([fullDoocsServers{i}(1:end-1) '*']);
                        for k=1:length(chans)
                            if ~contains(chans(k).channel, {'SND', '_SVR'})
                                fullDoocs{end+1} = chans(k).channel;
                            end
                        end
                    catch
                        continue
                    end
                    
                end
                self.dat.allCamList = fullDoocs;
            else
                fullDoocs = self.dat.allCamList;
            end
            maxStrLen = max(cellfun(@length, fullDoocs));
            [indx,tf] = listdlg('PromptString','Select a channel to add alias for:',...
                           'SelectionMode','single', 'ListSize', [maxStrLen*7,250],...
                           'ListString', fullDoocs);
            if tf
                newCam = strip(fullDoocs{indx}, '`');
                if ~contains(self.dat.camList, newCam)
                    self.dat.camList{end+1} = newCam;
                    self.updateMainTable;
                end
            end
        end
        
        function saveCameraList(self, ~, ~)
            if isempty(self.dat.camList); return; end
            [filename,pathname] = uiputfile('*.*', 'Save chanlist', '/home/fflab28m/ChanLists/');
            if isequal(filename,0) || isequal(pathname,0)
                return
            end
            fname = fullfile(pathname, filename);
            if ~endsWith(fname, '.chanlist'); fname = [fname '.chanlist']; end
            fid = fopen(fname,'w');
            fprintf(fid,'%s\n',self.dat.camList);
            fclose(fid);
        end
        
        function clearCameraList(self, ~, ~)
            self.dat.camList = {};
            self.updateMainTable;
        end
        
        function disp_(self, string)
            % Displays and adds to the log window...
            tt = datestr(now, 'HH:MM:SS');
            string = erase(string, 'FLASH.DIAG/');
            fprintf('%s: %s\n', tt, string);
            prev = get(self.fig.logWin, 'String');
            prev(2:end+1) = prev;
            prev{1} = [tt ': ' string];
            set(self.fig.logWin, 'String', prev);
        end
        
        function delete(self,~,~)
            for i=1:length(self.tim.chans)
                stop(self.tim.chans(i));
            end
            stop(self.tim.daq);
            stop(self.tim.laser);
            delete(self.fig.parent)
        end
    end
end