function varargout = SpotSim(varargin)
% SPOTSIM MATLAB code for SpotSim.fig
%      SPOTSIM, by itself, creates a new SPOTSIM or raises the existing
%      singleton*.
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SpotSim_OpeningFcn, ...
                   'gui_OutputFcn',  @SpotSim_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before SpotSim is made visible.
function SpotSim_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% Choose default command line output for SpotSim
handles.output = hObject;
%Beam defaults: 150mm beam, F20, 800nm
handles.beam.D = 0.15; 
handles.beam.F = 3;
handles.beam.lambda0 = 800e-9;
handles.beam.FFcalib = setFFcalib(hObject, eventdata, handles);
% Update handles structure
guidata(hObject, handles);

set(handles.text01, 'String', ['PtV: ' num2str(0) ' lambda']);
set(handles.text02, 'String', ['RMS: ' num2str(0) ' lambda']);
calculate(hObject, eventdata, handles);
%fftstuff(hObject, eventdata, handles);

function calib = setFFcalib(hObject, eventdata, handles)
diameter = 2; %Beam of unit radius
popup_sel= get(handles.popupmenu1, 'Value');
switch popup_sel
    case 1
        FFTspace = 64;
    case 2
        FFTspace = 128;
    case 3
        FFTspace = 256;
    case 4
        FFTspace = 512;
    case 5
        FFTspace = 1024;
end
axes(handles.axes1);
cla;
%Setting up coordinates in polar form
ax = linspace(-diameter*0.5,diameter*0.5,FFTspace);
x = repmat(ax, FFTspace, 1);
y = x';
mask = (x.^2 + y.^2 <= (diameter*0.5*0.95)^2);
FFTspace = 2^12;
spot = fftshift(fft2(mask, FFTspace, FFTspace));
cdat = abs(spot).^2;
%calculate(hObject, eventdata, handles);
%cdat = getimage(handles.axes2);
lineout = diff(cdat(round(0.5*size(cdat,1)),:));
[~,ind] = max(lineout);
q_0 = 1.22*handles.beam.lambda0*handles.beam.F/handles.beam.D;
calib = 0.5*q_0/(round(0.5*size(cdat,1))-ind);

% --- Outputs from this function are returned to the command line.
function varargout = SpotSim_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)

calculate(hObject, eventdata, handles);
%fftstuff(hObject, eventdata, handles);

function calculate(hObject, eventdata, handles)
%does all the heavy lifting.
diameter = 2; %Beam of unit radius
popup_sel= get(handles.popupmenu1, 'Value');
switch popup_sel
    case 1
        FFTspace = 64;
    case 2
        FFTspace = 128;
    case 3
        FFTspace = 256;
    case 4
        FFTspace = 512;
    case 5
        FFTspace = 1024;
end
axes(handles.axes1);
cla;
%Setting up coordinates in polar form
ax = linspace(-diameter*0.5,diameter*0.5,FFTspace);
x = repmat(ax, FFTspace, 1);
y = x';
mask = (x.^2 + y.^2 <= (diameter*0.5*0.95)^2);
radius = sqrt((x.^2 + y.^2));
%radius = (radius);
theta = atan2(y,x);
coef = zeros(20,1);
coef(1) = str2num(get(handles.edit1, 'String'));
coef(2) = str2num(get(handles.edit2, 'String'));
coef(3) = str2num(get(handles.edit3, 'String'));
coef(4) = str2num(get(handles.edit4, 'String'));
coef(5) = str2num(get(handles.edit5, 'String'));
coef(6) = str2num(get(handles.edit6, 'String'));
coef(7) = str2num(get(handles.edit7, 'String'));
coef(8) = str2num(get(handles.edit8, 'String'));
coef(9) = str2num(get(handles.edit9, 'String'));
coef(10) = str2num(get(handles.edit10, 'String'));
coef(11) = str2num(get(handles.edit12, 'String'));
coef(12) = str2num(get(handles.edit13, 'String'));
coef(13) = str2num(get(handles.edit14, 'String'));
coef(14) = str2num(get(handles.edit15, 'String'));
coef(15) = str2num(get(handles.edit16, 'String'));
coef(16) = str2num(get(handles.edit17, 'String'));
coef(17) = str2num(get(handles.edit18, 'String'));
coef(18) = str2num(get(handles.edit19, 'String'));
%coef(19) = str2num(get(handles.edit20, 'String'));
%coef(20) = str2num(get(handles.edit21, 'String'));
aberration = zeros(FFTspace, FFTspace, 20);
totwav = zeros(FFTspace);

aberration(:,:,1) = 2*radius.^2 - ones(FFTspace); %defocus
aberration(:,:,2) = radius.^2.*cos(2.*theta); %0 degree astig
aberration(:,:,3) = radius.^2.*sin(2.*theta); %45 astig
aberration(:,:,4) = (3.*radius.^2-2).*radius.*cos(theta); %0 coma
aberration(:,:,5) = (3.*radius.^2-2).*radius.*sin(theta); %90 coma
aberration(:,:,6) = 6*radius.^4 - 6*radius.^1 + 1; %spherical
aberration(:,:,7) = radius.^3.*cos(3.*theta); %30 degree trefoil
aberration(:,:,8) = radius.^3.*sin(3.*theta); %0 degree trefoil
aberration(:,:,9) = (4*radius.^2-3).*radius.^2.*cos(2*theta); %0 degree secondary astigmatism
aberration(:,:,10) = (4*radius.^2-3).*radius.^2.*sin(2*theta); %45 degree secondary astigmatism
aberration(:,:,11) = radius.^4.*cos(4*theta); %0 degree tetrafoil
aberration(:,:,12) = radius.^4.*sin(4*theta); %22.5 degree tetrafoil
aberration(:,:,13) = (10*radius.^5-12*radius.^3+3*radius).*cos(theta); %Secondary x coma
aberration(:,:,14) = (10*radius.^5-12*radius.^3+3*radius).*sin(theta); %Secondary y coma
%aberration(:,:,15) = (5*radius.^5-4*radius.^3).*cos(3.*theta); %Secondary 0 degree trefoil
aberration(:,:,15) = 2*radius*cos(theta); % tip
%aberration(:,:,16) = (5*radius.^5-4*radius.^3).*sin(3.*theta); %Secondary 30 degree trefoil
aberration(:,:,16) = 2*radius*sin(theta); % tilt
aberration(:,:,17) = radius.*cos(theta);
for i=1:20
   totwav = totwav + aberration(:,:,i).*coef(i); 
end
imagesc(totwav.*mask);
set(gca, 'XTickMode', 'manual', 'YTickMode', 'manual',...
    'XTick', [], 'YTick', []);
%colorbar;
PtV = max(max(totwav)) - min(min(totwav));
set(handles.text01, 'String', ['PtV: ' num2str(PtV) ' lambda']);
RMS = rms(rms(totwav));
set(handles.text02, 'String', ['RMS: ' num2str(RMS) ' lambda']);
%offset = 5*ones(FFTspace);
handles.wavefront = totwav.*mask;
handles.mask = mask;
guidata(hObject, handles);
fftstuff(hObject, eventdata,handles);

function fftstuff(hObject, eventdata, handles)
popup_sel= get(handles.popupmenu1, 'Value');
switch popup_sel
    case 1
        FFTspace = 64;
    case 2
        FFTspace = 128;
    case 3
        FFTspace = 256;
    case 4
        FFTspace = 512;
    case 5
        FFTspace = 1024;
end
phasefront = handles.wavefront;
axes(handles.axes2);
cla;
wavefront = double(handles.mask).*exp(1i*phasefront);
FFTspace = 2^12;
spot = fft2(wavefront, FFTspace, FFTspace);
spot = fftshift(spot);
assignin('base', 'spot', spot);
spot = abs(spot).^2;
minedge = floor(FFTspace*0.45);
maxedge = ceil(FFTspace*0.55);
spotzoom = spot(minedge:maxedge,minedge:maxedge);
imagesc(spotzoom);
set(gca, 'XTickMode', 'manual', 'YTickMode', 'manual',...
    'XTick', [], 'YTick', []);
spotE = spotparams(abs(spot));
set(handles.text27, 'String', ['E(FWHM): ' num2str(spotE(1)*handles.beam.FFcalib,3)]);
set(handles.text29, 'String', ['E(1/e): ' num2str(spotE(2)*handles.beam.FFcalib,3)]);
set(handles.text28, 'String', ['Hor width: ' num2str(spotE(3)*handles.beam.FFcalib,3)]);
set(handles.text30, 'String', ['Ver width: ' num2str(spotE(4)*handles.beam.FFcalib,3)]);

%find the spot parameters
function energies = spotparams(IMAGE)
[rows cols] = size(IMAGE);
[Maxs, Maxind] = max(IMAGE);
[MaxVal, xMax] = max(max(IMAGE));
%yMax =  Maxind(xMax);
%Find centroid by weighting pixels by intensity
x = ones(rows,1)*[1:cols];
y = [1:rows]'*ones(1,cols);
totcounts = sum(sum(IMAGE));
xbar = sum(sum(IMAGE.*x))/totcounts;
ybar = sum(sum(IMAGE.*y))/totcounts;
xbar = round(xbar);
ybar = round(ybar);
horlineout = IMAGE(ybar,:);
horfwhm = width2(horlineout);
verlineout = IMAGE(:,xbar)';
verfwhm = width2(verlineout);
totcounts = sum(sum(IMAGE));
Efwhm = sum(sum(IMAGE(IMAGE>MaxVal*0.5)))/totcounts;
%Efwhm = sum(sum(IMAGE(IMAGE>MaxVal*0.5)))/totcounts;
area = numel(IMAGE(IMAGE>MaxVal*0.5));
fwhmradius = 1*2*sqrt(area/pi);
E1e= sum(sum(IMAGE(IMAGE>MaxVal*exp(-1))))/totcounts;
%E1ee= sum(sum(IMAGE(IMAGE>MaxVal*exp(-2))))/totcounts;
energies(1) = Efwhm;
energies(2) = E1e;
energies(3) = horfwhm;
energies(4) = verfwhm;
energies(5) = fwhmradius;

%Find the fwhm of the image given in
%Extrapolate exact FWHM locations between the two pixels with linear
%extrapolation
function fwhm = width2(image)
[totY totX] = size(image);
x = linspace(1,totX,totX);
y = image;
[HM maxloc] = max(y);
HM = HM*0.5;
%Find first HM location.
i = 2;
while sign(y(i)-HM) == sign(y(i-1)-HM)
    i = i+1;
end  
slope = (HM-y(i-1)) / (y(i)-y(i-1));
x1 = x(i-1) + slope*(x(i)-x(i-1));
%Find second HM location. Assume it's after first one and after max value.
i = maxloc+1;
while ((sign(y(i)-HM) == sign(y(i-1)-HM)) & (i <= totX-1))
    i = i+1;
end
%Avoid NaN returns.
if (i ~= totX)
    slope = (HM-y(i-1)) / (y(i)-y(i-1));
    x2 = x(i-1) + slope*(x(i)-x(i-1));
    fwhm = abs(x2 - x1);
else
    disp('No second edge.')
    return
end

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)

% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.wavefront)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
handles.beam.FFcalib = setFFcalib(hObject, eventdata, handles);
guidata(hObject, handles);
calculate(hObject, eventdata, handles);


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end
set(hObject, 'String', {'64', '128', '256', '512', '1024'});

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
calculate(hObject, eventdata, handles);

% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
calculate(hObject, eventdata, handles);


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
prompt={'Beam diameter (m):',...
        'Focal distance (m):',...
        'Laser wavelength (m):'};
name='Set beam parameters:';
numlines=1;
defaultanswer={num2str(handles.beam.D), num2str(handles.beam.F), num2str(handles.beam.lambda0)};
answer=inputdlg(prompt,name,numlines,defaultanswer);
if (isempty(answer))
    return;
else
    handles.beam.D = str2double(answer{1});
    handles.beam.F = str2double(answer{2});
    handles.beam.lambda0 = str2double(answer{3});
    handles.beam.FFcalib = setFFcalib(hObject, eventdata, handles);
    guidata(hObject, handles);
end
