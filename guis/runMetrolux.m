%Simple Matlab to view data from SEA-SPIDER Metrolux camera

function him = runMetrolux()

hObject = figure('units', 'pixels', ...
    'position', [100 100 800 600], ...
    'menubar', 'none', ...
    'name', 'Metrolux camera viewing', ...
    'Tag', 'Metrolux', ...
    'resize', 'on');

S.startbutton = uicontrol('style', 'pushbutton', 'units', 'normalized', ...
    'position', [0.02, 0.85 0.16, 0.1], ...
    'string', 'RUN CAMERA', 'FontSize', 16, ...
    'callback', {@startbutton_callback});

S.imaxis = axes('units', 'normalized', ...
    'position', [0.2 0.02 0.78, 0.96], ...
    'YTick', [], 'XTick', []);
imshow(zeros(1000));
colorbar;
colormap(jet(256));

S.gaintext = uicontrol('style', 'pushbutton', 'units', 'normalized', ...
    'position', [0.02 0.75 0.08 0.1], 'FontSize', 16, ...
    'string', 'Gain');
S.gainbox = uicontrol('style', 'edit', 'units', 'normalized', ...
    'position', [0.12 0.75 0.06 0.1], ...
    'string', '0', 'FontSize', 16, 'callback', @checkgain);
S.shuttertext = uicontrol('style', 'pushbutton', 'units', 'normalized', ...
    'position', [0.02 0.65 0.08 0.1], 'FontSize', 16, ...
    'string', 'Shutter');
S.shutterbox = uicontrol('style', 'edit', 'units', 'normalized', ...
    'position', [0.12 0.65 0.06 0.1], ...
    'string', '10', 'FontSize', 16, 'callback', @checkshutter);
S.triggerbox = uicontrol('style', 'popupmenu', 'units', 'normalized', ...
    'position', [0.02 0.55 0.16 0.1], ...
    'string', {'Internal', 'External'}, 'FontSize', 16);
S.ratebox = uicontrol('style', 'text', 'units', 'normalized', ...
    'position', [0.02 0.45 0.16 0.05], ...
    'FontSize', 14);
S.autosave = uicontrol('style', 'checkbox', 'units', 'normalized', ...
    'position', [0.02 0.16 0.16 0.04], ...
    'string', 'Enable', 'FontSize', 12);
S.savebox = uicontrol('style', 'pushbutton', 'units', 'normalized', ...
    'position', [0.02 0.05 0.16 0.1], ...
    'string', 'SAVE IMAGE', 'FontSize', 16, 'callback', @savefile);
S.autosavebox = uicontrol('style', 'pushbutton', 'units', 'normalized', ...
    'position', [0.02 0.2 0.16 0.1], ...
    'string', 'AUTOSAVE', 'FontSize', 16, 'callback', @setautosave);

S.run = 0;
S.autosavepath = '';
S.shot = 1;
imaqreset;
vid = videoinput('dcam', 1, 'F7_Y16_1028x1008');
S.vid = vid;
guidata(hObject, S);
him = hObject;

function [] = setautosave(hObject,~)
S = guidata(hObject);
savpath = uigetdir('C:\','Path for autosave');
if isempty(savpath)
    return
end
S.autosavepath = savpath;
guidata(hObject,S);

function [] = startbutton_callback(hObject, ~)
S = guidata(hObject);
curr = get(S.startbutton, 'String');
if (strcmp(curr, 'RUN CAMERA'))
    set(S.startbutton, 'String', 'STOP');
    set(S.startbutton, 'BackgroundColor', [0. 1. 0.]);
    S.run = 1;
    set(S.gainbox, 'Enable', 'off');
    set(S.shutterbox, 'Enable', 'off');
    set(S.triggerbox, 'Enable', 'off');
    guidata(hObject, S);
    runcamera(hObject);
end
if (strcmp(curr, 'STOP'))
    set(S.startbutton, 'String', 'RUN CAMERA');
    set(S.startbutton, 'BackgroundColor', [0.941 0.941 0.941]);
    S.run = 0;
    set(S.gainbox, 'Enable', 'on');
    set(S.shutterbox, 'Enable', 'on');
    set(S.triggerbox, 'Enable', 'on');
    stop(S.vid);
    guidata(hObject, S);
end


function [] = checkgain(hObject, ~)
S = guidata(hObject);
val = get(S.gainbox, 'String');
val = str2double(val);
if (val < 0 || val > 511)
    warndlg('Gain needs to be between 0 and 511! Defaulting to 0.', ...
        'Incompatible gain setting!');
    set(S.gainbox, 'String', '0');
end


function [] = checkshutter(hObject, ~)
S = guidata(hObject);
val = get(S.shutterbox, 'String');
val = str2double(val);
if (val < 1 || val > 999)
    warndlg('Shutter value needs to be between 1 and 999! Defaulting to 10.', ...
        'Incompatible shutter setting!');
    set(S.shutterbox, 'String', '10');
end


function [] = savefile(hObject, ~)
S = guidata(hObject);
[filename, pathname, filterin] = uiputfile({'*.tif', 'Tif file'},...
    'Save image as', '/new.tif');
if (filename == 0)
    return
end
imdata = uint16(getimage(S.imaxis));
imwrite(imdata, [pathname filename]);


function [] = doautosave(hObject)
S = guidata(hObject);
savpath = S.autosavepath;
filname = ['im' num2str(S.shot) '.tif'];
imdata = uint16(getimage(S.imaxis));
imwrite(imdata, [savpath '\' filname]);
S.shot = S.shot + 1;
guidata(hObject,S)


function [] = runcamera(hObject)
%imaqreset;
S = guidata(hObject);
vid = S.vid;
src = getselectedsource(vid);
src.GainMode = 'manual';
src.Gain = str2double(get(S.gainbox, 'String'));
src.ShutterMode = 'manual';
src.Shutter = str2double(get(S.shutterbox, 'String'));
triggerp = get(S.triggerbox, 'Value');
if (triggerp == 1)
    triggerconfig(vid, 'immediate', 'none', 'none');
elseif (triggerp == 2)
    triggerconfig(vid, 'hardware', 'fallingEdge', 'externalTrigger');
end
%triggerconfig(vid)
set(vid, 'TriggerRepeat', Inf);
%Next parameter crucial to ensure camera runs, must be small enough
src.NormalizedBytesPerPacket = 350;
%src.FrameTimeout = 10000;%
vid.FramesPerTrigger = 1;
vid.FrameGrabInterval = 1;
set(vid, 'ROIPosition', [10 1 1008 1000])
%vid.Running
start(vid)
while (S.run)
    S = guidata(hObject);
    tic
    if (vid.FramesAcquired == 0)
        pause(0.05);
        continue;
    end
    fram = getdata(vid,1);
    axes(S.imaxis);
    imshow(fram);
    colorbar;
    colormap(jet(256));
    set(gca, 'CLim', [0 2^12]);
    flushdata(vid)
    set(S.ratebox, 'String', [num2str(1/toc) ' Hz']);
    if get(S.autosave, 'Value')
        doautosave(hObject);
    end
end
stop(vid);
disp('stopped');
%delete(vid);

