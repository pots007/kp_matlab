classdef VBox < uiextras.VBox & uiextrasX.Container
     methods (Access = protected)
        function [widths, heights] = redraw(obj)
            if request(obj)
                [widths, heights] = redraw@uiextras.VBox(obj);
            else
                widths = nan;
                heights = nan;
            end
        end
     end
     methods
         function obj = VBox(varargin)
            obj@uiextras.VBox(varargin{:});
         end
         function redraw_(obj)
             redraw(obj);
         end
     end     
end