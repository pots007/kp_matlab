classdef Grid < uiextras.Grid & uiextrasX.Container
     methods (Access = protected)
        function [widths, heights] = redraw(obj)
            if request(obj)
                [widths, heights] = redraw@uiextras.Grid(obj);
            else
                widths = nan;
                heights = nan;
            end
        end
     end
     methods
         function obj = Grid(varargin)
            obj@uiextras.Grid(varargin{:});
         end
         function redraw_(obj)
             redraw(obj);
         end
     end     
end