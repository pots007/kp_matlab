classdef HButtonBox < uiextras.HButtonBox & uiextrasX.Container
     methods (Access = protected)
        function redraw(obj)
            if request(obj)
                redraw@uiextras.HButtonBox(obj);
            end
        end
     end
     methods
         function obj = HButtonBox(varargin)
            obj@uiextras.HButtonBox(varargin{:});
         end
         function redraw_(obj)
             redraw(obj);
         end
     end     
end