% Augment uiextras.Container with extra functions
% *** Redraw deferral ***
% Complicated applications take a long time to initialize because the
% Containers redraw every time something is added. This class, which is
% intended to be subclassed alongside a concrete container, adds the
% ability to defer redrawing until later. The subclass must override the
% redraw function. See e.g. uiextrasX.VBox for an example of how to do this.
% A uiextrasX.Container defers
% drawing if its Deferring property is true OR its parent is a
% uiextrasX.Container which is deferring. When deferring, redraw commands
% set the Pending property to true. When an object stops deferring, if
% Pending is true it redraws itself and informs its children. In this way,
% when a parent object resumes drawing, its children are redrawn in a top
% down fashion, which suits the way that uiextras calculates sizes.
classdef Container < handle
    properties (SetAccess = protected)
        Deferring = false
        Pending = false
    end 
    methods (Access = protected)
        function redraw = request(obj)
            redraw = ~isDeferring(obj);
            obj.Pending = ~redraw;
        end
        function deferring = isDeferring(obj)
            deferring = obj.Deferring;
            Parent = getappdata(get(obj, 'Parent'), 'Container');
            if isa(Parent, 'uiextrasX.Container')
                deferring = deferring | isDeferring(Parent);
            end
        end
        function deferStateChange(obj)
            if ~isDeferring(obj)
                if obj.Pending
                    redraw_(obj);
                end
                children = get(obj, 'Children');
                for ci = 1:numel(children)
                    child = getappdata(children(ci), 'Container');
                    if isa(child, 'uiextrasX.Container')
                        deferStateChange(child);
                    end
                end
            end
        end
    end
    methods
        function defer(obj)
             obj.Deferring = true;
        end
        function resume(obj)
            if ~obj.Deferring
                error('Wasn''t deferred');
            end
            obj.Deferring = false;
            deferStateChange(obj);
        end
    end
end
