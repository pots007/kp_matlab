classdef VBoxFlex < uiextras.VBoxFlex & uiextrasX.Container
     methods (Access = protected)
        function redraw(obj)
            if request(obj)
                redraw@uiextras.VBoxFlex(obj);
            end
        end
     end
     methods
         function obj = VBoxFlex(varargin)
            obj@uiextras.VBoxFlex(varargin{:});
         end
         function redraw_(obj)
             redraw(obj);
         end
     end     
end