classdef TabPanel < uiextras.TabPanel & uiextrasX.Container
     methods (Access = protected)
        function redraw(obj)
            if request(obj)
                redraw@uiextras.TabPanel(obj);
            end
        end
     end
     methods
         function obj = TabPanel(varargin)
            obj@uiextras.TabPanel(varargin{:});
         end
         function redraw_(obj)
             redraw(obj);
         end
     end     
end