classdef HBox < uiextras.HBox & uiextrasX.Container
     methods (Access = protected)
        function [widths, heights] = redraw(obj)
            if request(obj)
                [widths, heights] = redraw@uiextras.HBox(obj);
            else
                widths = nan;
                heights = nan;
            end
        end
     end
     methods
         function obj = HBox(varargin)
            obj@uiextras.HBox(varargin{:});
         end
         function redraw_(obj)
             redraw(obj);
         end
     end     
end