classdef HBoxFlex < uiextras.HBoxFlex & uiextrasX.Container
     methods (Access = protected)
        function [widths, heights] = redraw(obj)
            if request(obj)
                [widths, heights] = redraw@uiextras.HBoxFlex(obj);
            else
                widths = nan;
                heights = nan;
            end
        end
     end
     methods
         function obj = HBoxFlex(varargin)
            obj@uiextras.HBoxFlex(varargin{:});
         end
         function redraw_(obj)
             redraw(obj);
         end
     end     
end