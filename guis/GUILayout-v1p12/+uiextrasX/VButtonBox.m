classdef VButtonBox < uiextras.VButtonBox & uiextrasX.Container
     methods (Access = protected)
        function redraw(obj)
            if request(obj)
                redraw@uiextras.VButtonBox(obj);
            end
        end
     end
     methods
         function obj = VButtonBox(varargin)
            obj@uiextras.VButtonBox(varargin{:});
         end
         function redraw_(obj)
             redraw(obj);
         end
     end     
end