classdef Panel < uiextras.Panel & uiextrasX.Container
    methods (Access = protected)
        function redraw(obj)
            if request(obj)
                redraw@uiextras.Panel(obj);
            end
        end
     end
     methods
         function obj = Panel(varargin)
            obj@uiextras.Panel(varargin{:});
         end
         function redraw_(obj)
             redraw(obj);
         end
     end     
end