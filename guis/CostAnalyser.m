% CostAnalyser.m
%
% A GUI to look through my bank statements and to figure out where my money
% goes/comes from. Allows to categorise things as:
%                     Bills_Electricity
%                     Bills_Gas
%                     Bills_Internet
%                     Bills_Phone
%                     Bills_Water
%                     Clothes
%                     Food
%                     Other
%                     Prizes
%                     Rent
%                     Salary
%                     Travel_Flights
%                     Travel_TFL
%                     Travel_Trains
%
% It then allows one to look at fractions of money spent per
% week/month/year on any of these things and plot these fractions as a
% function of time.
%
% Written in Jan 2017, London
%
%

classdef CostAnalyser < handle
    properties
        fig
        dat
        statbar
    end
    
    methods
        function o = CostAnalyser
        % Set up graphics and all the tabs
        o.fig.parent = figure(888); clf(888); set(888, 'Visible', 'off');
        set(o.fig.parent, 'Toolbar', 'figure', 'MenuBar', 'none', 'Color', 'w',...
            'Name', 'Financial analysis', 'NumberTitle', 'off',...
            'HandleVisibility', 'callback', 'CloseRequestFcn', @(s,e)delete(o));
        vbox = uiextras.VBox('Parent', o.fig.parent); hbox = uipanel('Parent', vbox);
        o.fig.htabs = uitabgroup('Parent', hbox);
        o.fig.data.par = uitab('Parent', o.fig.htabs, 'Title', 'Databases');
        o.fig.anal.par = uitab('Parent', o.fig.htabs, 'Title', 'Cost assignment');
        o.fig.plot.par = uitab('Parent', o.fig.htabs, 'Title', 'Plotting and anaysis');
        %o.fig.input.par = uitab('Parent', o.fig.htabs, 'Title', 'Data input');
        %o.fig.btrac.par = uitab('Parent', o.fig.htabs, 'Title', 'Back tracking');
        uiextras.HBox('Parent', vbox);
        vbox.Sizes = [-1 20];
        
        set(o.fig.parent, 'Visible', 'on');
        end
        
        function makeTable(o,v)
        costCategories = {'Bills_Electricity',...
            'Bills_Gas',...
            'Bills_Internet',...
            'Bills_Phone',...
            'Bills_Water',...
            'Clothes',...
            'Food',...
            'Other',...
            'Prizes',...
            'Rent',...
            'Salary',...
            'Travel_Flights',...
            'Travel_TFL',...
            'Travel_Trains'};
        headers = {'Date', ...
            '<html><center>Trans.<br />ID</center></html>', ...
            '<html><center>Trans.<br />type</center></html>', ...
            '<html><center>Trans.<br />amount</center></html>', ...
            '<html><center>Trans.<br />name</center></html>', ...
            '<html><center>Trans.<br />memo</center></html>', ...
            '<html><center>Cost<br />type</center></html>'};
        editable = false(size(headers)); editable(end) = true;
        columnformat = {'char', 'numeric', 'char', 'numeric', 'char', 'char', 'popup'};
        data0 = {'01-Jan-1970', 1, 'lala', 'shortG', 'Something', 'something', {'one', 'two'}}; 
        hTable = uitable('Data', data0, 'ColumnEditable',editable, ...
            'ColumnName',headers, 'RowName',[], ...
            'ColumnFormat',columnformat, ...
            'ColumnWidth','auto', ...
            'Units','norm');
        end
        
        function delete(o)
        % Try to save a few thing here...
        disp('Closing and saving current status.')
        %o.loadSaveWork(1);
        delete(888);
        end
        
    end
    
end