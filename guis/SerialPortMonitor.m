% SerialPortMonitor.m
%
% Lightweight, portable MATLAB program to communicate with serial port
% Allows the user to choose a port, set its baud and terminator
% Returns all replies at any time.
%
% KP, London Jan 2017

classdef SerialPortMonitor < handle
    
    properties
        fig
        port
        baud = 9600
        term = 'LF'
        statbar
    end

    methods
        function o = SerialPortMonitor
            o.fig.parent = figure(1002);
            set(o.fig.parent, 'MenuBar', 'none', 'NumberTitle', 'off', ...
                'Name', 'Serial Port Monitor', 'DefaultUIControlFontSize', 13,...
                'Units', 'Pixels', 'Position', [500 300 500 400]);
            row4 = 0.9; H4 = 0.1; 
            row3 = 0.8; H3 = 0.1;
            row2 = 0.7; H2 = 0.1;
            uicontrol('Style', 'pushbutton', 'String', 'Scan ports', 'Parent', o.fig.parent,...
                'Units', 'normalized', 'Position', [0.02 row4 0.2 H4], 'Callback', @o.scan);
            ht = uicontrol('Style', 'text', 'String', 'Port:', 'Parent', o.fig.parent,...
                'Units', 'normalized', 'Position', [0.23 row4 0.1 H4]);
            jh = findjobj(ht);
            jh.setVerticalAlignment(javax.swing.JLabel.CENTER)
            o.fig.port = uicontrol('Style', 'popupmenu', 'String', {'No ports'},...
                'Parent', o.fig.parent, 'Units', 'normalized',...
                'Position', [0.33 row4 0.5 H4], 'Enable', 'off', 'Callback', @o.connect);
            uicontrol('Style', 'text', 'String', 'Baud:', 'Parent', o.fig.parent,...
                'Units', 'normalized', 'Position', [0.02 row3 0.1 H3]);
            o.fig.bauds = {'9600', '19200', '38400', '57600', '115200'};
            o.fig.baud = uicontrol('Style', 'popupmenu', 'String', o.fig.bauds,'Parent', o.fig.parent,...
                'Units', 'normalized', 'Position', [0.15 row3 0.25 H3], 'Callback', @o.setBaud);
            uicontrol('Style', 'text', 'String', 'Terminator', 'Parent', o.fig.parent,...
                'Units', 'normalized', 'Position', [0.5 row3 0.2 H3]);
            o.fig.terms = {'CR', 'LF', 'LF/CR', 'CR/LF'};
            o.fig.term = uicontrol('Style', 'popupmenu', 'String', o.fig.terms,'Parent', o.fig.parent,...
                'Units', 'normalized', 'Position', [0.7 row3 0.3 H3], 'Callback', @o.setTerminator);
            % The command prompt
            ht = uicontrol('Style', 'text', 'String', 'Command:', 'Parent', o.fig.parent,...
                'Units', 'normalized', 'Position', [0.01 row2 0.18 H2]);
            o.fig.cmd = uicontrol('Style', 'edit', 'String', '', 'Parent', o.fig.parent,...
                'Units', 'normalized', 'Position', [0.19 row2 0.75 H2], 'HorizontalAlignment', 'left',...
                'FontName', 'Courier', 'Callback', @o.writePort);
            jh = findjobj(ht);
            jh.setVerticalAlignment(javax.swing.JLabel.CENTER)
            % And the main box for text
            o.fig.display = uicontrol('Style', 'edit', 'String', '', 'Parent', o.fig.parent,...
                'Units', 'normalized', 'Position', [0.01 0.07 0.98 0.6],...
                'HorizontalAlignment', 'left', 'Max', 1000, 'FontName', 'Courier',...
                'FontSize', 0.6*get(o.fig.parent, 'DefaultUIControlFontSize')); 
            % Make the text container not editable
            jEdit=findjobj(o.fig.display,'nomenu'); %get the UIScrollPane container
            jEdit=jEdit.getComponent(0).getComponent(0);
            set(jEdit,'Editable',0);
            o.fig.jdisplay = jEdit;
            % Simple-simle status bar for single mode operation
            o.statbar = uicontrol('Style', 'text', 'String', 'Select port to open.',...
                'Parent', o.fig.parent, 'FontSize', 0.7*get(o.fig.parent, 'DefaultUIControlFontSize'),...
                'Units', 'normalized', 'Position', [0.01 0.01 0.9 0.05], 'HorizontalAlignment', 'left');
        end
        
        function scan(o,~,~)
            % Perform reset or not?
            %portlist = instrfindall;
            ll = instrhwinfo('serial');
            portlist = ll.SerialPorts;
            if isempty(portlist);
                o.disp_('No serial ports found! Running instrreset.');
                instrreset;
                %portlist = instrfindall;
                ll = instrhwinfo('serial');
                portlist = ll.SerialPorts;
                if isempty(portlist)
                    o.disp_('No serial ports found. Connect some devices.');
                end
                set(o.fig.port, 'Enable', 'off');
            else 
                set(o.fig.port, 'String', portlist);
                set(o.fig.port, 'Enable', 'on');
            end
        end
       
        function v = isopen(o,~,~)
            % Quick one to do all necessary sanity check to serial port
            if isempty(o.port); v = false; return; end;
            if strcmp(o.port.Status, 'open')
                v = true;
            else
                v = false;
            end
        end
        
        function connect(o,~,~)
            % First check whether some ports are open. If so, close them
            % and delete the objects.
            if o.isopen
                % Stop async read first!
                %stopasync(o.port);
                fclose(o.port);
                delete(o.port);
            end
            ind = get(o.fig.port, 'String'); indv = ind{get(o.fig.port, 'Value')};
            o.port = serial(indv);
            set(o.port, 'Baud', o.baud, 'Terminator', o.term,...
                'ReadAsyncMode', 'continuous', 'InputBufferSize', 2048,...
                'BytesAvailableFcnMode', 'terminator',...
                'BytesAvailableFcn', @o.readPort, 'Timeout', 0.5);
            % Now try to open the port and start the asyncronous read.
            try
                fopen(o.port);
                o.disp_(['Port ' indv ' is now open']);
                %readasync(o.port);
            catch err
                o.disp_(['Error opening port ' indv ': ' err.message '. Running instrreset.']);
                instrreset;
                o.scan;
            end
        end
        
        function setBaud(o, ~, ~)
            ind = get(o.fig.baud, 'Value');
            indv = o.fig.bauds{ind};        
            if o.isopen
                o.baud = str2double(indv);
                set(o.port, 'BaudRate', o.baud);
                o.disp_(['Baud rate set to ' indv]);
            else
                o.disp_(['Error setting baud rate to ' indv '. Serial port not open']);
            end
        end
        
        function setTerminator(o, ~, ~)
            ind = get(o.fig.term, 'Value');
            indv = o.fig.terms{ind};        
            if o.isopen
                o.term = indv;
                set(o.port, 'Terminator', o.term);
                o.disp_(['Terminator set to ' o.term]);
            else
                o.disp_(['Error setting terminator to ' indv '. Serial port not open']);
            end
        end
        
        function writePort(o,~,~)
            % Check if there's anywhere to write...
            if ~o.isopen
                o.disp_('No ports are open!');
                return;
            end
            cmd = get(o.fig.cmd, 'String');
            switch o.term
                case 'LF'
                    terminator = char(10);
                case 'CR'
                    terminator = char(13);
                case 'LF/CR'
                    terminator = [char(10) char(13)];
                case 'CR/LF'
                    terminator = [char(13) char(10)];
            end
            fprintf(o.port, '%c', [cmd terminator]);
            int8([cmd terminator]);
            instr = get(o.fig.display, 'String');
            % Ensure we don't overrun...
            if length(instr)>90; instr(1) = []; end;
            instr{end+1} = ['<' cmd];
            set(o.fig.display, 'String', instr);
            set(o.fig.cmd, 'String', '');
            drawnow;
            % Idea for the below from
            % https://uk.mathworks.com/matlabcentral/answers/154589-how-to-automatically-scroll-a-textbox-to-end-of-line
            o.fig.jdisplay.setCaretPosition(o.fig.jdisplay.getDocument.getLength);
        end
        
        function readPort(o,~,~)
            % Port should be open now, so no point in checking for that!
            %disp('Got something!');
            resp = fgetl(o.port);
            instr = get(o.fig.display, 'String');
            % Ensure we don't overrun...
            if length(instr)>90; instr(1) = []; end;
            instr{end+1} = ['>' resp];
            set(o.fig.display, 'String', instr);
            set(o.fig.cmd, 'String', '');
            pause(0.005);
            drawnow;
            %o.fig.jdisplay.getDocument.getLength
            o.fig.jdisplay.setCaretPosition(10);
            o.fig.jdisplay.setCaretPosition(o.fig.jdisplay.getDocument.getLength)
            drawnow;
        end
        
        function disp_(o,str)
            set(o.statbar, 'String', str);
            disp(str);
        end
        
        function delete(o)
            if o.isopen
                fclose(o.port);
                delete(o.port);
            end
            delete(o.fig.parent);
        end
    end
end