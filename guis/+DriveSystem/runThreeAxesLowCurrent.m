% This will open a GUI to control the three axes of ThreeAxesLowCurrent
% driver

fpath = fileparts(which('DriveSystem.sbox_driver'));
calibFile = fullfile(fpath, 'stages', 'portascan_20DAM.mat');
axDriver_x = DriveSystem.sbox_driver([], calibFile);
axDriver_x.axis = 'X';
axDriver_y = DriveSystem.sbox_driver([], calibFile);
axDriver_x.axis = 'Y';
axDriver_z = DriveSystem.sbox_driver([], calibFile);
axDriver_x.axis = 'Z';
axDriver_x.setSerialPort('COM16', 115200, 'LF');

% Set up the graphics, the container
hfig = figure(701); clf(hfig);
hp = uix.ScrollingPanel('Parent', hfig);
hbox = uix.HBox('Parent', hp);
ax_x = DriveSystem.axisController(axDriver_x, hbox);
ax_x.fig.axisName.String = 'X-axis'; % axDriver_x.stage_dat.name;
ax_y = DriveSystem.axisController(axDriver_y, hbox);
ax_y.fig.axisName.String = 'Y-axis'; %axDriver_x.stage_dat.name;
ax_z = DriveSystem.axisController(axDriver_z, hbox);
ax_z.fig.axisName.String = 'Z-axis'; %axDriver_x.stage_dat.name;
hbox.Widths = [200 200 200];
hp.Widths = 800; hp.Heights = 700;

ax_x.fig.barcode.String =  num2str(axDriver_x.stage_dat.barcode, '%06i');
ax_y.fig.barcode.String =  num2str(axDriver_y.stage_dat.barcode, '%06i');
ax_z.fig.barcode.String =  num2str(axDriver_z.stage_dat.barcode, '%06i');
% Finally, open the port.
axDriver_x.openPort(1);
axDriver_y.interface.port = axDriver_x.interface.port;
axDriver_y.axis = 'Y';
axDriver_z.interface.port = axDriver_x.interface.port;
axDriver_z.axis = 'Z';