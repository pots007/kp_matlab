% This will open a GUI to control the grating mount driver

fpath = fileparts(which('DriveSystem.sbox_driver'));
calibFile = fullfile(fpath, 'stages', 'gratingMount.mat');
axDriver = DriveSystem.sbox_driver([], calibFile);
axDriver.setSerialPort('COM15', 115200, 'LF');
axController = DriveSystem.axisController(axDriver, figure(700));
set(axController.fig.axisName, 'String', axDriver.stage_dat.name);
set(axController.fig.barcode, 'String', num2str(axDriver.stage_dat.barcode, '%06i'));
axDriver.openPort(1);
