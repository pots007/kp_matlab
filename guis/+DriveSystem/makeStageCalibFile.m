% makeStageCalibFile.m
%
% This file saves a calibration file for a particular stage, in a format
% that is acceptacle to the axisDriver class. 
%
% Inputs:
%               name
%               barcode
%               stepspermm                
%               maxTravel
%               maxSpeed
%               maxAcceleration
%               limMinInstalled
%               limMaxInstalled
%
% KP, Feb 2017

function makeStageCalibFile(filename, name, barcode, stepspermm, maxTravel,...
    maxSpeed, maxAcceleration, limMinInstalled, limMaxInstalled)

fset = struct;
fset.name = name;
fset.barcode = barcode;
fset.stepspermm = stepspermm;
fset.maxTravel = maxTravel;
fset.maxSpeed = maxSpeed;
if nargin < 7 || isempty(maxAcceleration)
    fset.maxAcceleration = [];
else
    fset.maxAcceleration = maxAcceleration;
end
if nargin < 8 || isempty(limMinInstalled)
    fset.limMinInstalled = 0;
else
    fset.limMinInstalled = limMinInstalled;
end
if nargin < 9 || isempty(limMaxInstalled)
    fset.limMaxInstalled = 0;
else
    fset.limMaxInstalled = limMaxInstalled; 
end

save(filename, 'fset');
end