% SBoX_Driver.m
%
% Driver to control the stepper driver based on G-code.
%
% KP Jan 2017

classdef sbox_driver < DriveSystem.axisDriver
    properties
        axis = 'X';
        speed
        stage_dat
    end
    
    methods
        function o = sbox_driver(varargin)
            if nargin == 0 
                o.interface.port = [];
            end
            % First input argument can be an interface object
            o.setDefaultValues;
            if nargin < 2
                if nargin == 1
                    o.interface.port = varargin{1};
                end
                % But also, it is imperative to get a calibration. If the
                % user refuses to give us one, we pick sensible default
                % values.
                fpath = fileparts(which('DriveSystem.sbox_driver'));
                [fname, pathname] = uigetfile(fullfile(fpath, '*.mat'), 'Select calibration file');
                if isequal(fname,0) || isequal(pathname,0)
                    o.setDefaultValues;
                else
                    % Try to load the file, if it doesn't contain all we
                    % need, revert to defaults
                    ff = load(fullfile(pathname, fname));
                    if ~isfield(ff, 'fset'); return; end;
                    fieldnams = fieldnames(o.stage_dat);
                    for i=1:length(fieldnams)
                        if isfield(ff.fset, fieldnams{i})
                            o.stage_dat.(fieldnams{i}) = ff.fset.(fieldnams{i});
                        end
                    end
                end
            elseif nargin == 2
                % Try to load the file, if it doesn't contain all we
                % need, revert to defaults
                ff = load(varargin{2});
                if ~isfield(ff, 'fset'); return; end;
                fieldnams = fieldnames(o.stage_dat);
                for i=1:length(fieldnams)
                    if isfield(ff.fset, fieldnams{i})
                        o.stage_dat.(fieldnams{i}) = ff.fset.(fieldnams{i});
                    end
                end
            end
            
            % Not much left to do in here, apart from setting up default
            % status
            o.status.inMotion = 0;
            o.status.homeLimit = 0;
            o.status.maxLimit = 0;
            o.status.currentPos = 0;
            o.speed = o.stage_dat.maxSpeed*0.5*o.stage_dat.stepspermm;
        end
        
        function setSerialPort(o, COM, baud, term)
            o.interface.port = serial(COM, 'BaudRate', baud, 'Terminator', term,...
                'InputBufferSize', 2048, 'Timeout', 0.5,...
                'ReadAsyncMode', 'continuous',...
                'BytesAvailableFcnMode', 'terminator',...
                'BytesAvailableFcn', @o.readPort);
        end
        
        function stat = openPort(o, v)
            if v % Try to open the port
                if o.isPortOpen;
                    % Port is already open, nothing to be done
                    stat = 'Port already open';
                    return;
                else
                    try
                        fopen(o.interface.port);
                        stat = 'Port successfully open.';
                    catch err
                        stat = ['Error opening port: ' err.message];
                    end
                end
            else
                if ~o.isPortOpen
                    %Port not open, nothing to do
                    stat = 'Port already closed.';
                    return;
                else
                    try
                        fclose(o.interface.port);
                        stat = 'Port successfully closed.';
                    catch err
                        stat = ['Error closing port: ' err.message];
                    end
                end
            end
        end
        
        function v = isPortOpen(o)
            % Quick one to do all necessary sanity check to serial port
            if isempty(o.interface.port); v = false; return; end;
            if strcmp(o.interface.port.Status, 'open')
                v = true;
            else
                v = false;
            end
        end
        
        %% ---------- Utility  and motion functions
        
        function moveAbs(o, val)
            % val is in mm, need to convert it to steps.
            val_ = round(val*o.stage_dat.stepspermm);
            if o.isPortOpen
                cmd = sprintf('G01 F%f %s%f', o.speed, o.axis, val_);
                fprintf(o.interface.port, cmd);
            end
            end
            
        
        function home(o)
            if o.isPortOpen
                % Check if homeLimit is installed. If so, move to min
                % limit, else to logical zero.
                if o.stage_dat.limMinInstalled
                    homespeed = o.stage_dat.maxSpeed/3*o.stage_dat.stepspermm;
                    cmd = sprintf('G161 %s F%1.3f', o.axis, homespeed);
                    fprintf(o.interface.port, cmd);
                else
                    fprintf(o.interface.port, 'G28');
                end
            end
        end
        
        function stop(o)
            if o.isPortOpen
                fprintf(o.interface.port, 'M0;');
            end
        end
        
        function stat = getStatus(o)
            if o.isPortOpen
                o.interface.port.ReadAsyncMode = 'manual';
                fprintf(o.interface.port, 'M119');
                fgetl(o.interface.port); % The command is repeated back
                tt = fgetl(o.interface.port);
                % But there's a retarded bug, try to get around it...
                if strcmp(tt, 'M119'); tt = fgetl(o.interface.port); end
                o.interface.port.ReadAsyncMode = 'continuous';
                if isempty(tt);
                    o.status.inMotion = 0;
                    o.status.homeLimit = 0;
                    o.status.maxLimit = 0;
                    return;
                end;
                ind = strfind(lower(tt), 'stat');
                if ~isempty(ind)
                    % Bear in mind that tt is a char vector, so need to
                    % make it into a number first.
                    % EDIT 20170315: add options to check how many axes we
                    % have... Also, cast it to a 24 bit word...
                    bits = dec2bin(str2double(tt(ind+4:end)), 24);
                    if strcmp(o.axis, 'X')
                        o.status.maxLimit = str2double(bits(17));
                        o.status.homeLimit = str2double(bits(18));
                        o.status.inMotion = str2double(bits(19));
                    elseif strcmp(o.axis, 'Y')
                        o.status.maxLimit = str2double(bits(9));
                        o.status.homeLimit = str2double(bits(10));
                        o.status.inMotion = str2double(bits(11));
                    elseif strcmp(o.axis, 'Z')
                        o.status.maxLimit = str2double(bits(1));
                        o.status.homeLimit = str2double(bits(2));
                        o.status.inMotion = str2double(bits(3));
                    end
                end
            end
            % As status involves the position as well, get an update on
            % that
            o.getPosn;
            stat = o.status;
        end
        
        function posn =  getPosn(o)
            if o.isPortOpen
                o.interface.port.ReadAsyncMode = 'manual';
                fprintf(o.interface.port, 'M114');
                fgetl(o.interface.port); % This is the repeated command
                tt = fgetl(o.interface.port);
                % But there's a retarded bug, try to get around it...
                if strcmp(tt, 'M119'); tt = fgetl(o.interface.port); end
                o.interface.port.ReadAsyncMode = 'continuous';
                if isempty(tt); return; end
                ind1 = strfind(upper(tt), o.axis);
                ind2 = strfind(upper(tt), 'F');
                if ~isempty(ind1) && ~isempty(ind2)
                    o.status.currentPos = str2double(tt(ind1+1:ind2-1))/o.stage_dat.stepspermm;
                end
            end
            posn = o.status.currentPos;
        end
        %% --------- Setting various motion properties
        function setSpeed(o, val)
            % Sets a relative speed, in percent.
            o.speed = val/100*o.stage_dat.maxSpeed*o.stage_dat.stepspermm;
        end
        
        function setAbsoluteMode(o)
            if o.isPortOpen
                fprintf(o.interface.port, 'G90;');
            end
        end
        
        function setRelativeMode(o)
            if o.isPortOpen
                fprintf(o.interface.port, 'G91;');
            end
        end
        
        function setIdleHold(o, val)
            if o.isPortOpen
                cmd = sprintf('M84 S%f', val);
                fprintf(o.interface.port, cmd);
            end
        end
        
        function setCurrentPosition(o, val)
            % Forces the controller to a particular step count.
            if o.isPortOpen
                val_ = round(val*o.stage_dat.stepspermm);
                cmd = sprintf('G92 %s%i', o.axis, val_);
                fprintf(o.interface.port, cmd);
            end
        end
        
        function readPort(o,~,~)
            % This function will read the serial buffer and place the data
            % into the Userdata field of the object available to other
            % instances trying to access it (in the long run). For the
            % sbox, there is always exactly one driver per serial port.
            fgetl(o.interface.port);
            %end
        end
        
        function delete(o)
            if o.isPortOpen
                try
                    fclose(o.interface.port);
                    delete(o.interface.port);
                catch
                    
                end
            end
        end
    end
end