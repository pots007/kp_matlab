% axisDriver.m
%
% General abstract class to derive individual stage drivers from. Defines
% the basic methods that are required and data structures.
%

classdef axisDriver < handle
    properties
        interface
        dat
        status 
    end
    
    methods
        function o = axisDriver
            % The definition of status field
            o.status.inMotion = 0;      % Whether the stage is currently moving
            o.status.homeLimit = 0;     % Whether the home limit is active
            o.status.maxLimit = 0;      % Whether end of travel limit is active
            o.status.currentPos = 3.4;  % Where we are now.
        end
        
        function moveAbs(o,pos)
            % Implement motion to an absolute position
            fprintf('Generic absolute movement function\n');
        end
        
        function stop(o)
            % Implement method to stop the stage moving.
        end
        
        function home(o)
            % Implement a method to home the stage
            fprintf('Generic homing function\n');
        end
        
        function v = isMoving(o)
            o.getStatus;
            v = o.status.inMotion;
        end
        
        function posn = getPosn(o)
            % Implement return of position
            posn = 0;
        end
        
        function stat = getStatus(o)
            % Implement method to query the stage its status
            stat = o.status;
        end
        
        function setSpeed(o, val)
            % Implement method to set the speed of the stage. 
        end
        
        function setAcceleration(o, val)
            % Implement method to set the speed of the stage. 
        end
        
        function setDefaultValues(o)
            % Set a range of sensible default values for stage properties.
            o.stage_dat.name = 'Default';
            o.stage_dat.barcode = 1;
            o.stage_dat.stepspermm = 400;
            o.stage_dat.maxTravel = 10;
            o.stage_dat.minTravel =  0;
            o.stage_dat.maxSpeed = 10;
            o.stage_dat.maxAcceleration = [];
            o.stage_dat.limMinInstalled = 0;
            o.stage_dat.limMaxInstalled = 0;
        end
    end
    
end