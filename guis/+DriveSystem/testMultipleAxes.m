% testMultipleAxes.m
%
% Highlights how many axes can be controlled from one window, by employing
% the uix.ScrollingPanel graphics holder.
%
hfig = figure(11); clf(hfig);

hp = uix.ScrollingPanel('Parent', hfig);
hbox = uix.HBox('Parent', hp);
ax1 = DriveSystem.axisController([], hbox);
ax1.fig.axisName.String = 'Test stage, X';
ax2 = DriveSystem.axisController([], hbox);
ax2.fig.axisName.String = 'Test stage, Y';
ax3 = DriveSystem.axisController([], hbox);
ax3.fig.axisName.String = 'Test stage, Z';
ax4 = DriveSystem.axisController([], hbox);
ax4.fig.axisName.String = 'Test stage, Rotation';
hbox.Widths = [200 200 200 200];
hp.Widths = 800; hp.Heights = 700;