% axisContoller.m
%
% General, high level GUI to control an underlying motion control axis
% Requires a low level driver that will relay all the commands onto the 
% actual stage.
% Written using uiextras figure placement, namely the uix class. Thus only
% compatible with HG2, ie Matlab 2014b+

classdef axisController < uix.Panel
    properties
        axisDriver
        fig
        dat
    end
    
    methods
        function o = axisController(axisDrv, parent)
            
            o@uix.Panel('Parent', parent);
            o.setPanelStyle(o);
            ov = uix.VBox('Parent', o);
            o.fig.parent = parent;
            if o.isParentFig
                set(o.fig.parent, 'MenuBar', 'none', 'NumberTitle', 'off', ...
                'Name', 'Single axis controller',...
                'Units', 'Pixels', 'Position', [500 100 200 700], 'Color', 'w', ...
                'CloseRequestFcn', @o.delete);
            % 'DefaultUIControlFontSize', 12,
            end
            if ispc
                dFont = 12;
            elseif ismac
                dFont = 15;
            end
            %set(o.fig.parent, 'DefaultUIControlBackgroundColor', 'w');
            o.fig.darkgreen = [0 0.4 0]; o.fig.darkred = [0.7 0 0];
            % ---------- Get the controller to know the driver
            if isempty(axisDrv)
                % Use the base class as default
                %o.axisDriver = feval('DriveSystem.axisDriver');
            else
                if ischar(axisDrv)
                    o.axisDriver = feval(axisDrv);
                else
                    o.axisDriver = axisDrv;
                end
            end
            
            % --------- Try to get up to date values for the stage
            try
                o.dat.minTravel = o.axisDriver.stage_dat.minTravel;
                o.dat.maxTravel = o.axisDriver.stage_dat.maxTravel;
            catch  % Use default values!    
                o.dat.minTravel = 0;
                o.dat.maxTravel = 10;
            end
            o.dat.stepSize = 1000;
            o.dat.targetPos = 0.5;
            o.dat.currentPos = 0.5;
            
            % --------- Axis name and barcode
            hp = uix.Panel('Parent', ov, 'Title', 'Axis');
            o.setPanelStyle(hp);
            vbox = uix.VBox('Parent', hp);
            o.fig.axisName = uicontrol('Parent', vbox, 'Style', 'text',...
                'String', 'Test axis x', 'FontSize', dFont);
            o.fig.barcode = uicontrol('Parent', vbox, 'Style', 'text',...
                'String', 'Barcode: 100123',  'FontSize', dFont);
            % --------- Axis speed controls
            hp = uix.Panel('Parent', ov, 'Title', 'Speed');
            o.setPanelStyle(hp);
            vbox = uix.VBox('Parent', hp);
            speedval = 50;% 0.5*(o.dat.maxSpeed+o.dat.minSpeed);
            o.fig.speedVal = uicontrol('Parent', vbox, 'Style', 'edit', 'FontSize', dFont,...
                'String', num2str(speedval, '%i'), 'Callback', @o.setSpeed);
            %jSlider = javax.swing.JSlider(o.dat.minSpeed,o.dat.maxSpeed);
            jSlider = javax.swing.JSlider(1, 100);
            o.fig.speedSlider = javacomponent(jSlider,[0,0,200,40], vbox);
            set(o.fig.speedSlider, 'StateChangedCallback', @o.setSpeed);  
            set(jSlider, 'Value', speedval, 'MajorTickSpacing',20, 'PaintLabels',true,...
                'Background', java.awt.Color(1,1,1));  % with labels, no ticks
            vbox.Heights = [-1 40];
            % --------  Axis acceleration controls
            hp = uix.Panel('Parent', ov, 'Title', 'Acceleration');
            o.setPanelStyle(hp);
            vbox = uix.VBox('Parent', hp);
            o.fig.accVal = uicontrol('Parent', vbox, 'Style', 'edit', 'FontSize', dFont,...
                'String', '50', 'Callback', @o.setAcceleration);
            jSlider = javax.swing.JSlider(1,100);
            o.fig.accSlider = javacomponent(jSlider,[0,0,200,40], vbox);
            set(o.fig.accSlider, 'StateChangedCallback', @o.setAcceleration);  
            set(jSlider, 'Value',50, 'MajorTickSpacing',20, 'PaintLabels',true,...
                'Background', java.awt.Color(1,1,1));  % with labels, no ticks
            vbox.Heights = [-1 40];
            % ---------- Position and steps 
            hp = uix.Panel('Parent', ov, 'Title', 'Position');
            o.setPanelStyle(hp);
            vbox = uix.VBox('Parent', hp);
            hbox = uix.HBox('Parent', vbox);
            o.fig.LLim = uicontrol('Style', 'pushbutton', 'Parent', hbox, ...
                'String', '', 'Enable', 'off', 'BackgroundColor', o.fig.darkgreen);
            o.fig.posVal = uicontrol('Style', 'edit', 'String', '500', 'Parent', hbox,...
                'FontSize', dFont, 'Callback', @o.editboxMove);
            o.fig.RLim = uicontrol('Style', 'pushbutton', 'Parent', hbox, ...
                'String', '', 'Enable', 'off', 'BackgroundColor', o.fig.darkgreen);
            jButton = findjobj(o.fig.LLim); jButton.setBorderPainted(false);
            jButton = findjobj(o.fig.RLim); jButton.setBorderPainted(false);
            hbox.Widths = [30 -1 30];
            hbox = uix.HBox('Parent', vbox);
            o.dat.stepSize = 250;
            uicontrol('Style', 'pushbutton', 'Parent', hbox, 'String', '<html><big>&larr;</big></html>',...
                'Tag', 'L', 'Callback', @o.moveRelative, 'TooltipString',...
                sprintf('Do step of the size specified \n in the box to the right,\n in the negative direction'));
            o.fig.stepVal = uicontrol('Style', 'edit', 'Parent', hbox, 'String', '1000',...
                'Tag', 'S', 'FontSize', dFont, 'Callback', @o.setStepSize);
            ht = uicontrol('Style', 'pushbutton', 'Parent', hbox, 'String', '<html><big>&rarr;</big></html>',...
                'Tag', 'R', 'Callback', @o.moveRelative, 'TooltipString',...
                sprintf('Do step of the size specified \n in the box to the left,\n in the positive direction'));
            %jButton = findjobj(ht); jButton.setBorderPainted(false);
            hbox.Widths = [40 -1 40];
            % -------   This is the box with position slider and more controls
            hbox = uix.HBox('Parent', ov);
            vbox = uix.VBox('Parent', hbox);
            % First off, a large STOP button for when we get into trouble
            o.fig.stop = uicontrol('Style', 'Pushbutton', 'Parent', vbox, 'FontSize', dFont, ...
                'String', 'STOP', 'BackgroundColor', 'r', 'Callback', @o.stopMotion);
            o.fig.mode = 1; % 1 for absolute, 0 for relative
            o.fig.absRelMode = uicontrol('Style', 'Pushbutton', 'Parent', vbox, 'FontSize', dFont,...
                'String', 'Abs', 'BackgroundColor', 'r', 'Callback', @o.toggleAbsRel);
            o.fig.homeButton = uicontrol('Style', 'pushbutton', 'String', 'Home', 'Parent', vbox,...
                'FontSize', dFont, 'Callback', @o.home);
            o.fig.setZero = uicontrol('Style', 'Pushbutton', 'Parent', vbox, ...
                'String', 'Set 0', 'FontSize', dFont, 'Callback', @o.setZero);
            o.fig.changeSettings = uicontrol('Style', 'pushbutton', 'Parent', vbox,...
                'String', {'Change settings'}, 'FontSize',  0.7*dFont);%0.7*get(o.fig.parent, 'DefaultUIControlFontSize'));
            vbox.Heights = [70 40 40 40 40];
            % The position slider now. Two modes of operation: micron level
            % relative steps or absolute positions
            jSlider = javax.swing.JSlider(0,1000*o.dat.maxTravel);
            set(jSlider, 'Value', 500, 'MajorTickSpacing', 1000,...
                'PaintLabels',true, 'PaintTicks', true, 'Orientation',jSlider.VERTICAL,...
                'Background', java.awt.Color(1,1,1));  % with labels, no ticks
            o.fig.posSlider = javacomponent(jSlider,[0,0,100,300], hbox);
            set(o.fig.posSlider, 'StateChangedCallback', @o.sliderMove);
            % The position slider is in units of microns, for any case.
            % Make a java hashtable to convert this to mm for convenience; ABS mode to start with.
            o.setLabels(0, o.dat.maxTravel, 11, 1);
            o.makeTooltips;
            
            % Finish off the graphics
            ov.Heights = [60 80 80 80 -1];
            
            
            % ----------- Timer to keep track of movements
            o.fig.motionTimer = timer('TimerFcn', @o.trackMotion, 'Period', 0.1, ...
                'ExecutionMode', 'fixedRate');
            o.dat.motionFlag = 2;
            assignin('base', 'AxisController', o)
        end
%% Functions to actually drive the stage
        function setSpeed(o,h,~)
            
            if ~isprop(h, 'Tag')
                % Source is the slider: 
                % Only set the speed if we're in place!
                if get(h, 'ValueIsAdjusting')
                    return;
                else
                    %disp('in place');
                    val = h.Value;
                    o.fig.speedVal.String = num2str(val);
                end
            else
                val = str2double(h.String);
                o.fig.speedSlider.Value = val;
            end
            % And tell the driver we've set a new speed value.
            o.axisDriver.setSpeed(val);
        end
        
        function setAcceleration(o,h,~)
            if ~isprop(h, 'Tag')
                % Source is the slider: 
                % Only set the speed if we're in place!
                if get(h, 'ValueIsAdjusting')
                    return;
                else
                    %disp('in place');
                    val = h.Value;
                    o.fig.accVal.String = num2str(val);
                end
            else
                val = str2double(h.String);
                o.fig.accSlider.Value = val;
            end
            % And tell the driver we've set a new speed value.
            o.axisDriver.setAcceleration(val);
        end
        
        function setStepSize(o,h,~)
            val = get(h, 'String'); v = str2double(val);
            if ~isnan(v) || v>0
                o.dat.stepSize = v;
            end
            % And set the slider as well, if we're in relative mode
            if ~o.fig.mode && v<=500
                o.fig.posSlider.Value = v;
            end
        end
        
        function sliderMove(o,h,~)
            % This function deals with the position slider movements
            if get(h, 'ValueIsAdjusting')
                return;
            else
                %disp('in place');
                val = h.Value;
            end
            if o.fig.mode % In absolute mode
                %o.dat.targetPos = 0.001*val;
                if 0.001*val == o.dat.currentPos; return; end
                o.moveAbsolute(0.001*val);
            else % In relative mode, just set the step value.
                o.dat.stepSize = val;
                o.fig.stepVal.String = num2str(val);
            end
        end
        
        function editboxMove(o,h,~)
            % We need to make sure we don't call the motion function if
            % we're in motion and simply updating the position value:
            if o.dat.motionFlag==2
                val = get(h, 'String'); v = str2double(val);
                if isnan(v) || v<0
                    return;
                end
                o.moveAbsolute(0.001*v);
                return
            end
        end
        
        function moveRelative(o,h,~)
            % This is to do relative steps. Only called by the arrow buttons.
            dir = (-1)^(strcmp(get(h, 'Tag'), 'L'));
            fprintf('Relative step of %i from %2.3f\n', 1e-3*o.dat.stepSize, o.dat.currentPos);
            o.axisDriver.moveAbs(o.dat.currentPos + dir*1e-3*o.dat.stepSize);
            start(o.fig.motionTimer);
        end
        
        function moveAbsolute(o,val)
            fprintf('Absolute move to %i\n', val);
            % So we need to tell the driver to do the move, and then
            % monitor it's progress. 
            o.axisDriver.moveAbs(val);
            start(o.fig.motionTimer);
        end
        
        function home(o,~,~)
            % Simply call the driver
            o.axisDriver.home;
            start(o.fig.motionTimer);
        end
        
        function stopMotion(o,~,~)
            % Simply call the driver
            o.axisDriver.stop;
        end
        
%% 
        function toggleAbsRel(o,~,~)
            if o.fig.mode  % If in absolute travel mode before, means we're switching to rel!
                o.fig.posSlider.Minimum = 0;
                o.fig.posSlider.Maximum = 500;
                o.fig.posSlider.Value = o.dat.stepSize;
                o.setLabels(0, 500, 11, 0);
                o.fig.mode = 0;
                set(o.fig.absRelMode, 'BackgroundColor', 'g', 'String', 'Rel');
                set(o.fig.stepVal, 'String', num2str(o.dat.stepSize));
            else
                o.fig.posSlider.Maximum = o.dat.maxTravel*1000;
                o.setLabels(0, o.dat.maxTravel, 11, 1);
                o.fig.mode = 1;
                o.fig.posSlider.Value = o.dat.currentPos*1000;
                set(o.fig.absRelMode, 'BackgroundColor', 'r', 'String', 'Abs');
                %set(o.fig.stepVal, 'String', '1000');
                %o.dat.stepSize = 1000;
            end
        end       
        
        function trackMotion(o,~,~)
            % This function polls the driver to understand where the stage
            % is. Also takes care of highlighting the limits.
            stat = o.axisDriver.getStatus;
            o.dat.currentPos = stat.currentPos;
            if ~stat.inMotion; 
                o.dat.motionFlag = o.dat.motionFlag - 1;
            end
            if o.dat.motionFlag<1
                stop(o.fig.motionTimer); 
                % If we're in absolute mode, then set the slider and position
                % box values to where we're at.
                if o.fig.mode
                    o.fig.posSlider.Value = stat.currentPos*1000;
                end
                o.dat.motionFlag = 2;
            end;
            stat.currentPos
            set(o.fig.posVal, 'String', num2str(stat.currentPos*1000)); drawnow;
            set(o.fig.LLim, 'BackgroundColor', o.getLimitColor(stat.homeLimit));
            set(o.fig.RLim, 'BackgroundColor', o.getLimitColor(stat.maxLimit));
        end
        
%% Various helper methods
        function htable = setLabels(o, minv, maxv, nticks, v)
            % v is the ABS/REL toggle: v=1 is ABS
            % Input to this is still in mm
            % See: https://kodejava.org/how-do-i-create-a-jslider-with-custom-labels/
            
            htable = java.util.Hashtable;
            ticks = linspace(minv, maxv, nticks);
            for i=1:nticks
                if v
                    htable.put(int32(ticks(i)*1000), javax.swing.JLabel(num2str(ticks(i), '%1.3f'))); 
                else
                    htable.put(int32(ticks(i)), javax.swing.JLabel(num2str(ticks(i), '%i')));
                end
            end
            o.fig.posSlider.setLabelTable(htable);
        end

        function makeTooltips(o)
            % This will print tooltips for many buttons.
            set(o.fig.posVal, 'TooltipString', 'Absolute position, in microns.\n To go to a precise location, \n type a number here.');
            set(o.fig.LLim, 'TooltipString', 'Home limit indicator.\n Green for limit not active,\n red for limit currently engaged');
            set(o.fig.RLim, 'TooltipString', 'Maximum travel limit indicator.\n Green for limit not active,\n red for limit currently engaged');
            set(o.fig.absRelMode, 'TooltipString', sprintf('Press this button to change\n between slider modes'));
            set(o.fig.changeSettings, 'TooltipString', 'Use this button to \n change the fundamental control settings, \n such as max travel or steps per mm.');
        end
        
        function col = getLimitColor(o, vv)
            if vv % if limit active, set to red
                col = o.fig.darkred;
            else % Else green;
                col = o.fig.darkgreen;
            end
        end
        
        function v = isParentFig(o)
            v =  strcmp(get(o.fig.parent, 'Type'), 'figure');
        end
        
        function setPanelStyle(o, p)
            % Small helper to make all panels pretty and the same :)
            set(p, 'BorderType', 'line', 'BorderWidth', 1, 'Padding', 3,...
                'TitlePosition', 'centertop', 'ForegroundColor', 'k',...
                'BackgroundColor', 'w', 'HighlightColor', 'k');
        end
  %%      
        function delete(o,~,~)
            if o.isParentFig
                delete(o.fig.parent);
            end
        end
    end
end