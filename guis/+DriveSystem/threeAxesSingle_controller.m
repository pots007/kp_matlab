% threeAxesSingleController.m
%
% This is a class used with the three axes stepper controller, built by
% KP in March 2017. It inherits everything from sbox_driver.m class, but
% overwrites the posn method and implements a method to set microsteps.
%
% KP March 2017, ICL

classdef threeAxesSingle_controller < DriveSystem.sbox_driver
    
    methods
        function posn = getPosn(o)
        if o.isPortOpen
            o.interface.port.ReadAsyncMode = 'manual';
            fprintf(o.interface.port, 'M114');
            fgetl(o.interface.port); % This is the repeated command
            % For three axis controller, it sends back three lines, one or
            % each axis.
            tt1 = fgetl(o.interface.port);
            % But there's a retarded bug, try to get around it...
            if strcmp(tt1, 'M119'); tt1 = fgetl(o.interface.port); end
            % ANd now just to ensure we had something, check 
            if isempty(tt1); return; end
            tt2 = fgetl(o.interface.port);
            if isempty(tt2); return; end
            tt3 = fgetl(o.interface.port);
            if isempty(tt3); return; end
            o.interface.port.ReadAsyncMode = 'continuous';
            tt = [tt1 tt2 tt3];
            ind1 = strfind(upper(tt), o.axis);
            ind2 = strfind(upper(tt), 'F');
            if ~isempty(ind1) && ~isempty(ind2)
                o.status.currentPos = str2double(tt(ind1+1:ind2-1))/o.stage_dat.stepspermm;
            end
        end
        posn = o.status.currentPos;
        end
    end
    
end