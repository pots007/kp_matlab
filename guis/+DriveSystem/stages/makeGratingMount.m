% This file will make a calibration file for the grating mount driver,
% built by KP in 2016-2017.

stepspermm = 400/2; % 400 steps per rev, 2mm pitch
maxTravel = 60;     % in mm
% Motor stalls for more than 400 steps per second...
maxSpeed = 2;       % in mm/s

fold = fileparts(which('DriveSystem.makeStageCalibFile'));
fname = fullfile(fold, 'stages', 'gratingMount');

DriveSystem.makeStageCalibFile(fname, 'GratingMount', 1, stepspermm,...
    maxTravel, maxSpeed, [], 1, 1);