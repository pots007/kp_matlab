% This file will make a calibration file for the grating mount driver,
% built by KP in 2016-2017.

% Assume 16 microsteps...
stepspermm = 16/0.0254; % 16 microsteps per 25.4 micron single step?
maxTravel = 12;     % in mm
% Motor stalls for more than 400 steps per second...
maxSpeed = 2;       % in mm/s

fold = fileparts(which('DriveSystem.makeStageCalibFile'));
fname = fullfile(fold, 'stages', 'portascan_20DAM');

DriveSystem.makeStageCalibFile(fname, 'Portascan_20DAM', 2, stepspermm,...
    maxTravel, maxSpeed, [], 1, 1);