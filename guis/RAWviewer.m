% Sequential etc AVT RAW viewer.
% Kristjan Poder, IC, 2014

% Updated 06/2016: guesses RAW file size from filesize; option for little
% endian files

function varargout = RAWviewer(varargin)

if (isunix)
    S.slash = '/';
elseif (ispc)
    S.slash = '\';
end

if (nargin==0)
    infol = pwd;
end
if (nargin==1)
    infol = varargin{1};
end
S.infol = infol;
flist = getfilenames(infol);

hObject = figure('IntegerHandle', 'off', 'units','pixels',...
    'position',[150 50 900 500],... %[left bottom width height]
    'menubar','none',...
    'numbertitle','off',...
    'name',['RAWviewer: ' infol],...
    'Tag', 'RAWviewer',...
    'resize','on', ...
    'WindowKeyPressFcn', @keymove,...
    'IntegerHandle', 'off');
S.fig1 = hObject;

% Top row 
S.filter = uicontrol('style', 'edit', 'units', 'normalized', ...
    'position', [0.14 0.9 0.15 0.03], 'String', [], 'Callback', @filter);
uicontrol('style', 'text', 'units', 'normalized', ...
    'position', [0.04 0.9 0.1 0.03], 'String', 'Filter filenames:');
% Filelist and axes
S.fbox = uicontrol('style', 'listbox', 'units', 'normalized',...
    'position', [0.04 0.45 0.25 0.45], 'string', flist, 'Callback', @populatelist);
S.ax = axes('units', 'normalized', 'position', [0.35 0.05 0.63 0.9],...
    'YDir', 'normal', 'buttonDownFcn', @getrect, 'HandleVisibility', 'callback');
% Below box - help 
S.help = uicontrol('style', 'text', 'units', 'normalized', ...
    'position', [0.04 0.38 0.25 0.03], 'String', []);
% File size controls
uicontrol('style', 'text', 'units', 'normalized', ...
    'position', [0.04 0.35 0.06 0.03], 'String', 'Width');
S.wbox = uicontrol('style', 'edit', 'units', 'normalized',...
    'position', [0.1 0.35 0.05 0.03], 'String', '640');
uicontrol('style', 'text', 'units', 'normalized', ...
    'position', [0.18 0.35 0.06 0.03], 'String', 'Height');
S.hbox = uicontrol('style', 'edit', 'units', 'normalized',...
    'position', [0.24 0.35 0.05 0.03], 'String', '480');
% Fourth row from bottom
S.endian = uicontrol('style', 'checkbox', 'units', 'normalized', 'value', 1, ...
    'position', [0.04 0.3 0.2 0.03], 'String', 'Big endian (little endian)');
% Third row from bottom
S.clims = uicontrol('style', 'checkbox', 'units', 'normalized', ...
    'position', [0.04 0.25 0.1 0.03], 'String', 'CLims:', 'Callback', @clim);
S.cmin = uicontrol('style', 'edit', 'units', 'normalized', ...
    'position', [0.14 0.25 0.07 0.03], 'String', '0', 'Callback', @clim);
S.cmax = uicontrol('style', 'edit', 'units', 'normalized', ...
    'position', [0.22 0.25 0.07 0.03], 'String', '1', 'Callback', @clim);
% Second to bottom row
S.cmap = uicontrol('Style', 'popup', 'String', {'jet' 'gray' 'hsv' 'hot' 'cool'},...
    'units', 'normalized', 'Position', [0.04 0.2 0.1 0.03],...
    'Callback', @setmap);
S.colorbar = uicontrol('style', 'checkbox', 'units', 'normalized', ...
    'position', [0.18 0.2 0.1 0.03], 'String', 'Colorbar', 'Callback', @cbar);
% Bottom row items
S.zoom = uicontrol('style', 'checkbox', 'units', 'normalized', ...
    'position', [0.17 0.1 0.11 0.03], 'String', 'ZOOM', 'Value', false, ...
    'Callback', @togglezoom);
S.assign = uicontrol('style', 'pushbutton', 'units', 'normalized', ...
    'position', [0.04 0.1 0.1 0.03], 'String', 'To base!', 'Callback', @tobase);
%S.getrect = uicontrol('style', 'pushbutton', 'units', 'normalized', ...
%    'position', [0.04 0.1 0.11 0.03], 'String', 'GetRect', 'Callback', @getrect);

S.imdata = [0 1];
guidata(hObject, S);


function tobase(hObject,~)
S = guidata(hObject);
assignin('base', 'rawimage', S.imdata);


function keymove(hObject, eventdata)
S = guidata(hObject);
if(strcmp(eventdata.Key, 'leftarrow'))
    movearrow(S,'left');
elseif(strcmp(eventdata.Key, 'rightarrow'))
    movearrow(S,'right');
elseif(strcmp(eventdata.Key, 'uparrow'))
    movearrow(S,'up');
elseif(strcmp(eventdata.Key, 'downarrow'))
    movearrow(S,'down');
end


function movearrow(S, direction)
axes(S.ax);
hold on;
h = findobj(gca, 'Type', 'Line');
if(~isempty(h))
    X = get(h, 'XData');
    Y = get(h, 'YData');
    delete(h)
else
    return;
end
switch direction
    case 'up'
        Y = Y+1;
    case 'down'
        Y = Y-1;
    case 'right'
        X = X+1;
    case 'left'
        X = X-1;
end
plot(X,Y, '+k', 'MarkerSize', 40, 'LineWidth', 1.5);
hold off;
set(S.help, 'String', sprintf('x=%3.0f y=%3.0f ', X, Y));



function getrect(hObject,~)
S = guidata(hObject);
if (get(S.zoom, 'Value'))
    return;
end
pos = get(hObject, 'CurrentPoint');
X = pos(1,1);
Y = pos(1,2);
axes(S.ax);
hold on;
h = findobj(gca, 'Type', 'Line');
if(~isempty(h))
    delete(h)
end
plot(X,Y, '+k', 'MarkerSize', 40, 'LineWidth', 1.5);
hold off;
set(S.help, 'String', sprintf('x=%3.0f y=%3.0f ', X,Y));


function cbar(hObject, ~)
S = guidata(hObject);
if (get(S.colorbar, 'Value'))
    colorbar;
else
    colorbar('delete');
end


function clim(hObject, ~)
S = guidata(hObject);
val = get(S.clims, 'Value');
if (~val)
    set(S.cmin, 'String', num2str(min((S.imdata(:)))));
    set(S.cmax, 'String', num2str(max((S.imdata(:)))));
end
set(S.ax, 'CLim', [str2double(get(S.cmin, 'String')) str2double(get(S.cmax, 'String'))]);


function togglezoom(hObject, ~)
S = guidata(hObject);
val = get(S.zoom, 'Value');
if (val)
    zoom on;
else
    zoom off;
end


function [] = filter(hObject, ~)
S = guidata(hObject);
filtert = get(S.filter, 'String');
dirlist = dir(S.infol);
if (isempty(filtert))
    return;
end
nfil = 0;
for i=1:length(dirlist)
    if (strfind(dirlist(i).name, filtert)~=0)
        nfil = nfil+1;
    end
    %nfil = nfil + dirlist(i).isdir;
end
flist = cell(1,nfil+2);
flist{1} = '.';
flist{2} = '..';
k = 3;
for i=1:length(dirlist)
    if (~dirlist(i).isdir &&(length(dirlist(i).name)>length(filtert)))
        if (sum(strfind(dirlist(i).name, filtert))~=0)
            flist{k} = dirlist(i).name;
            k = k+1;
        end
    end
end
set(S.fbox, 'String', flist);


function [] = setmap(hObject, ~)
S = guidata(hObject);
val = get(S.cmap, 'Value');
clist = get(S.cmap, 'String');
cmap = clist{val};
colormap(cmap);


function [] = populatelist(hObject, ~)
S = guidata(hObject);
if (strcmp(get(S.fig1, 'SelectionType'), 'open'))
    w = str2double(get(S.wbox, 'String'));
    h = str2double(get(S.hbox, 'String'));
    fID = get(S.fbox, 'Value');
    fLIST = get(S.fbox, 'String');
    fnam = fLIST{fID};
    %dlist = dir(S.infol);
    if (isdir([S.infol S.slash fnam]))
        if (strcmp(fnam, '..'))
            [S.infol,~] = fileparts([S.infol]);
        else
            S.infol = [S.infol S.slash fnam];
        end
        flist = getfilenames(S.infol);
        set(S.fbox, 'String', flist, 'Value', 1, 'ListboxTop', 1);
        set(S.fig1, 'name', ['RAWviewer: ' S.infol]);
    else
        [~,~,ext] = fileparts(fnam);
        filename = [S.infol S.slash fnam];
        if (strcmpi(ext, '.RAW'))
            fdata = dir(filename);
            set(S.help, 'String', ['Filesize is ' num2str(fdata.bytes) ' bytes']);
            endian = get(S.endian, 'Value');
            if (640*480*2 == fdata.bytes)
                S.imdata = ReadRAW16bit0(filename, 640, 480, endian);
                set(S.hbox, 'String', num2str(480));
                set(S.wbox, 'String', num2str(640));
            elseif(1280*960*2 == fdata.bytes)
                S.imdata = ReadRAW16bit0(filename, 1280, 960, endian);
                set(S.hbox, 'String', num2str(960));
                set(S.wbox, 'String', num2str(1280));
            elseif(2048*1088*2 == fdata.bytes)
                S.imdata = ReadRAW16bit0(filename, 2048, 1088, endian);
                set(S.hbox, 'String', num2str(1088));
                set(S.wbox, 'String', num2str(2048));
            else
                %warndlg('File size does not match known image sizes! | Using given values.', 'Warning', 'modal');
                S.imdata = ReadRAW16bit0(filename, w, h);
            end
        elseif (strcmpi(ext, '.TIF') || strcmpi(ext, '.TIFF'))
            S.imdata = imread(filename);
            set(S.help, 'String', ['width=' num2str(size(S.imdata,2)) ', height=' num2str(size(S.imdata,1))]);
        elseif (strcmpi(ext, '.FIT') || strcmpi(ext, '.FITS'))
            S.imdata = fitsread(filename);
            set(S.help, 'String', ['width=' num2str(size(S.imdata,2)) ', height=' num2str(size(S.imdata,1))]);
        else
            return
        end
        %axes(S.ax);
        hImage = imagesc(S.imdata, 'Parent', S.ax);
        if (~get(S.clims, 'Value'))
            set(S.cmin, 'String', num2str(min((S.imdata(:)))));
            set(S.cmax, 'String', num2str(max((S.imdata(:)))));
        end
        set(gca, 'YDir', 'normal', 'buttondownfcn', @getrect, 'HandleVisibility', 'on');
        set(hImage, 'HitTest', 'off');
        guidata(hObject, S);
        clim(hObject, w);
        cbar(hObject, w);
    end
    guidata(hObject, S);
end


function flist = getfilenames(fol)
dirlist = dir(fol);
flist = cell(1,length(dirlist));
for i=1:length(dirlist)
    flist{i} = dirlist(i).name;
end


%Reads AVT camera raw files, from Juffalo
function I = ReadRAW16bit0(filename, width, height, endian)

bands = 1;
precision = 'uint16';
offset = 0;
interleave = 'bip'; %smth
if endian
    byteorder = 'ieee-be'; %Little Endian
else
    byteorder = 'ieee-le'; % For XIMEA cams 
end
I = multibandread(filename, [height width bands], precision, offset, interleave, byteorder);
I = flipud(I);
