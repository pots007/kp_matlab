% From undocumented Matlab, date chooser panel from JIDEsoft
% Ref: http://undocumentedmatlab.com/blog/date-selection-components
function h = javaDateChooserPanel(Parent, varargin)
if ~exist('Parent','var')
    Parent=gcf;
end

% Initialize JIDE's usage within Matlab
com.mathworks.mwswing.MJUtilities.initJIDE;
 
% Display a DateChooserPanel
jPanel = com.jidesoft.combobox.DateChooserPanel;
%[hPanel,hContainer] = javacomponent(jPanel,[10,10,200,200],gcf)

if ~verLessThan('matlab','8.4.0')
    [h,c] = javacomponent(jPanel, [10,10,200,200], (Parent));
else
    [h,c] = javacomponent(jPanel, [10,10,200,200], double(Parent));
end
set(h,varargin{:});
set(c,'Tag','jidesoft.combobox.DateChooserPanel','UserData',h);