% very simple script to autosave AVT camera images
% saves into tiffs in a given folder incrementing filenames

function him = runAVT(setfile)
if nargin==0
    [fname pname] = uigetfile('*.mat', 'Find file with acquisition settings');
    if fname == 0
        disp('No settings file. End.');
        return;
    end
    setfile = [pname '/' fname];
end
tmp = load(setfile);
fset = tmp.fset;
% This file should have format, exposure, savepath, gain
hw=imaqhwinfo;
for n=1:length(hw.InstalledAdaptors)
    tmp=hw.InstalledAdaptors{n};
    if strfind(tmp, 'avtmatlabadaptor')
        adaptor_name = tmp;
    end
end
if isempty(adaptor_name)
    error('Unable to find AVT MATLAB IMAQ adaptor. Make sure you have installed according to http://www.alliedvisiontec.com/us/products/software/third-party-software.html. You may have to register the adapter with imaqtool.');
end
vid = videoinput(adaptor_name, 1, fset.format); %is it always cam 1?
triggerconfig(vid,'manual')
triggerconfig(vid, 'hardware', 'risingEdge', 'externalTrigger');
vid.FramesPerTrigger=1;
set(vid, 'ExtededShutter', fset.exposure);
set(vid, 'TriggerRepeat', Inf);
set(vid, 'Gain', fset.Gain);


o.cont = true;
%figure part
fig = figure(456);
o.ax = axes('Parent', fig, 'Position', [0.1 0.1 0.8 0.8]);
o.but = uicontrol('Style', 'Pushbutton', 'Parent', fig, 'Position', [0.1 0.02 0.8 0.06], ...
    'String', 'STOP', 'Callback', @(obj,e) setcont(o,1));
shot = 1;
start(vid);
while cont
    if (vid.FramesAcquired == 0)
        pause(0.05);
        continue;
    end
    fram = getdata(vid,1);
    imagesc(o.ax, fram);
    colorbar;
    imwrite(fram, [fset.savepath '/' num2str(shot) '.tiff', 'tiff');
    shot = shot + 1;
end
stop(vid);
delete(vid);

function setcont(o,e)
o.cont = false;
