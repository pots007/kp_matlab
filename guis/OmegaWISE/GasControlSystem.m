% This is the integrated gas pressure control system.
% User sets the desired pressure and everything else works like magic!
% (hopefully)
% Upon entering the desired pressure, program tells what line pressure it
% needs (assuming a fluctuation of 25% in that)

% For two gas system, align the valves up so they are on consecutive
% registers - then can flush gas from both with one command cycle. With
% this in mind, the valve layout needs to be:
% Fill valves - ser2, D0 and D1
% First release valve - ser2, D2 and D3
% Second release valve - ser2, D4 and D5

% Modbus notes - messag = int16([0 0 6 (int16(256*2)+int16(5)) (21) 65280])
% For single coil. the multiplier of 256 is modbus ID!
% messag = int16([0 0 6 (int16(256*2)+int16(15)) 20 2 (int16(256*1)+int16(3))])
% For multiple coils; again the modbus ID; 20 is forst coil address, 2 is
% number of coils to force, 256*n wher en is the number of byts to follow
% and last number is coils to turn on 3 = 00000011
% K. Poder, IC 2015

classdef GasControlSystem < uiextrasX.VBox
    
    properties
        IP
        ser1
        ser2
        connect
        modbus
        poll
        pres
        temp
        tempmon
        masterpoll
        closebutton
        fillbutton
    end
    
    methods
        function o = GasControlSystem
            Parent = figure(1001);
            set(Parent, 'Resize', 'off', 'Toolbar', 'none', 'MenuBar', 'none', 'Color', 'w', 'Position', [100 300 600 475], 'Name', 'Gas Control System', 'NumberTitle', 'off', 'CloseRequestFcn',@(s,e) disp('don''t close me!'));
            o@uiextrasX.VBox('Parent', Parent);
            defer(o);
            % Set up the IP panels
            hbox0 = uiextrasX.HBox('Parent', o);
            vbox0 = uiextrasX.VBox('Parent', hbox0);
            hbox = uiextrasX.HBox('Parent', vbox0);
            uicontrol('Style', 'text', 'String', '7118Z ', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'IP:', 'Parent', hbox);
            o.IP.ip1 = uicontrol('Style', 'edit', 'String', '192.168.255.1', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'Port', 'Parent', hbox);
            uicontrol('Style', 'edit', 'String', '502', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'Modbus ID', 'Parent', hbox);
            o.modbus.val1 = 1;
            o.modbus.ui1 = uicontrol('Style', 'edit', 'String', num2str(o.modbus.val1), 'Parent', hbox, 'Callback', @(s,e)modbus_update(o,1));
            hbox = uiextrasX.HBox('Parent', vbox0);
            uicontrol('Style', 'text', 'String', '7144 ', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'IP:', 'Parent', hbox);
            o.IP.ip2 = uicontrol('Style', 'edit', 'String', '192.168.255.2', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'Port', 'Parent', hbox);
            uicontrol('Style', 'edit', 'String', '502', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'Modbus ID', 'Parent', hbox);
            o.modbus.val2 = 2;
            o.modbus.ui2 = uicontrol('Style', 'edit', 'String', num2str(o.modbus.val2), 'Parent', hbox, 'Callback', @(s,e)modbus_update(o,2));
            o.connect = uicontrol('Style', 'togglebutton', 'String', 'OPEN TCP', 'Parent', hbox0, 'Callback', @(s,e)open_close(o, get(o.connect, 'Value')));
            hbox0.Sizes = [-1 85];
            % Now set up poll timer for pressure updating
            hbox = uiextrasX.HBox('Parent', o);
            o.poll = PollTimer2(hbox);
            set(o.poll.polltime, 'String', '250', 'Enable', 'off');
            addlistener(o.poll, 'Poll', @(s,e)get_pressure(o,1));
            hbox = uiextrasX.HBox('Parent', o);
            o.tempmon = uicontrol('Style', 'checkbox', 'String', 'Monitor temperatures', 'Value', 0, 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'Shot interval', 'Parent', hbox);
            o.masterpoll.period = javaNumSpinner2(40, 20, 800, 10, hbox);
            o.masterpoll.button = ContinuousButton2(hbox);
            set(o.masterpoll.period, 'StateChangedCallback', @(s,e)set(o.masterpoll.button.timer, 'Period', get(o.masterpoll.period, 'Value')));
            addlistener(o.masterpoll.button, 'trigger', @(s,e)regulate_(o,1));
            % Add pressure set box, the user guidance box and dual gas
            % option
            hbox0 = uiextrasX.HBox('Parent', o);
            vbox0 = uiextrasX.VBox('Parent', hbox0);
            hbox = uiextrasX.HBox('Parent', vbox0);
            o.pres.target1 = 150; o.pres.target2 = 150;
            uicontrol('Style', 'text', 'String', 'Target pressure:', 'Parent', hbox);
            o.pres.targetbox1 = uicontrol('Style', 'edit', 'String', num2str(o.pres.target1), 'Parent', hbox, 'Callback', @(s,e)set_target(o,1));
            uicontrol('Style', 'text', 'String', 'Current:', 'Parent', hbox);
            o.pres.curr1 = uicontrol('Style', 'text', 'String', num2str(o.pres.target1), 'Parent', hbox, 'BackgroundColor', 'r');
            o.pres.linebox1 = uicontrol('Style', 'text', 'String', sprintf('Set line to %3.1f mbar', o.pres.target1*1.25), 'Parent', hbox);
            %o.pres.regbutton = uicontrol('Style', 'pushbutton', 'String', 'GO', 'Parent', hbox, 'Callback', @(s,e)regulate_(o, get(o.pres.regbutton, 'Value')));
            hbox.Sizes = [-1 -1 -1 -1 150];
            hbox = uiextrasX.HBox('Parent', vbox0);
            uicontrol('Style', 'text', 'String', 'Target pressure:', 'Parent', hbox);
            o.pres.targetbox2 = uicontrol('Style', 'edit', 'String', num2str(o.pres.target2), 'Parent', hbox, 'Callback', @(s,e)set_target(o,2));
            uicontrol('Style', 'text', 'String', 'Current:', 'Parent', hbox);
            o.pres.curr2 = uicontrol('Style', 'text', 'String', num2str(o.pres.target2), 'Parent', hbox, 'BackgroundColor', 'r');
            o.pres.linebox2 = uicontrol('Style', 'text', 'String', sprintf('Set line to %3.1f mbar', o.pres.target2*1.25), 'Parent', hbox);
            hbox.Sizes = [-1 -1 -1 -1 150];
            vbox = uiextrasX.VBox('Parent', hbox0);
            o.pres.regbutton = uicontrol('Style', 'pushbutton', 'String', 'GO', 'Parent', vbox, 'Callback', @(s,e)regulate_(o, get(o.pres.regbutton, 'Value')));
            o.pres.gasnum = uicontrol('Style', 'checkbox', 'String', 'Two gases', 'Parent', vbox, 'Callback', @(s,e)set_twogases(o,get(o.pres.gasnum, 'Value')));
            hbox0.Sizes = [-1 100];
            set_twogases(o,0);
            % Load calibration 
            ff = load('Gemini2015Gas');
            fset = ff.fset;
            o.pres.calib(2).y0 = fset.mingas2;
            o.pres.calib(2).k = 500/(fset.maxgas2-fset.mingas2);
            o.pres.calib(1).y0 = fset.mingas1;
            o.pres.calib(1).k = 500/(fset.maxgas1-fset.mingas2);
            
            % The pressure graph
            hbox = uiextrasX.HBox('Parent', o);
            hpan = uiextrasX.Panel('Parent',hbox,'Title','Pressure history','Padding',2);
            o.pres.ax = axes('Parent', hpan);
            % Finally exit button
            hbox = uiextrasX.HBox('Parent', o);
            uicontrol('Style', 'text', 'String', 'Pressure history length', 'Parent', hbox);
            o.pres.hist = javaNumSpinner2(200, 50, 1e4, 10, hbox);
            o.pres.cla = uicontrol('Style', 'pushbutton', 'Parent', hbox, 'String', 'Clear graph', 'Callback', @(s,e)clear_pres(o,1));
            o.closebutton = uicontrol('Style', 'pushbutton', 'Parent', hbox, 'String', 'EXIT', 'Callback', @(s,e)closefcn(o));
            % And finally finally, temperature set points and current
            % temperatures
            labels = {'H1', 'H2', 'H3', 'H4', 'P1', 'P2', 'CSC', ''};
            hbox = uiextrasX.HBox('Parent', o);
            hpan = uiextrasX.Panel('Parent',hbox,'Title','Temperatures','Padding',2);
            hbox = uiextrasX.HBox('Parent', hpan);
            for i=1:8
                vbox = uiextrasX.VBox('Parent', hbox);
                uicontrol('Style', 'text', 'String', labels{i}, 'Parent', vbox);
                o.temp(i).curr = uicontrol('Style', 'text', 'String', '0', 'Parent', vbox);
                o.temp(i).target = uicontrol('Style', 'edit', 'String', '20', 'Parent', vbox, 'Callback', @(s,e)set_register(o, i));
            end
            o.Sizes = [40 25 25 50 -1 25 70];
            resume(o);
        end
        
        function set_twogases(o,v)
            if v; stat = 'on';
            else stat = 'off'; end
            set(o.pres.curr2, 'Enable', stat);
            set(o.pres.targetbox2, 'Enable', stat);
            set(o.pres.linebox2, 'Enable', stat);
        end
        
        function clear_pres(o,~)
            hplot = get(o.pres.ax, 'Children');
            if isempty(hplot)
                return;
            else
                delete(hplot);
            end
        end
        
        function set_target(o,v)
            tar = str2double(get(o.pres.(['targetbox' num2str(v)]), 'String'));
            if isnan(tar) || tar < 0
                o.pres.(['target' num2str(v)]) = 150;
            else
                o.pres.(['target' num2str(v)]) = tar;
            end
            set(o.pres.(['linebox' num2str(v)]), 'String', sprintf('Set line to %3.0f mbar', o.pres.(['target' num2str(v)])*1.25));
        end
        
        function modbus_update(o,v)
            port = str2double(get(o.modbus.(['ui' num2str(v)]), 'String'));
            if port<1 || port>250 || isnan(port)
                o.modbus.(['val' num2str(v)]) = v;
            else
                o.modbus.(['val' num2str(v)]) = port;
            end
            set(o.modbus.(['ui' num2str(v)]), 'String', num2str(o.modbus.(['val' num2str(v)])));
        end
        
        function open_close(o,val)
            if ~isfield(o, 'ser1'); o.ser1 = tcpip(get(o.IP.ip1, 'String'), 502); end
            if ~isfield(o, 'ser2'); o.ser2 = tcpip(get(o.IP.ip2, 'String'), 502); end
            set(o.ser1, 'Terminator', 'CR/LF', 'Timeout', 1, 'InputBufferSize', 3000, 'ByteOrder', 'BigEndian');
            set(o.ser2, 'Terminator', 'CR/LF', 'Timeout', 1, 'InputBufferSize', 3000, 'ByteOrder', 'BigEndian');
            if val
                try
                    fopen(o.ser1);
                    disp(['TCP on ' get(o.IP.ip1, 'String') ' now open']);
                    fopen(o.ser2);
                    disp(['TCP on ' get(o.IP.ip2, 'String') ' now open']);
                    set(o.connect, 'String', 'CLOSE TCP');
                    %digital_io(o,-1);
                catch
                    disp(['Unable to open TCP ' get(o.IP.ip1, 'String') '!!']);
                end
            else
                fclose(o.ser1);
                fclose(o.ser2);
                set(o.connect, 'String', 'OPEN TCP');
                disp('TCP now closed!');
            end
        end
        
        function press = get_pressure(o, sw)
            %press = []; disp('yay'); return; % Debug and test
            if (~strcmp(o.ser1.Status, 'open') || ~(strcmp(o.ser2.Status, 'open')))
                disp('TCP not open!');
                return;
            end
            % the pressure gauges are on A0 & A1 (registers 20 & 21)
            % Will ask for both pressures and do maths for both - the
            % network latency will easily hide the overhead in processing
            % both values.
            % In addiion, if sw==1 then we enquire about temperatures as
            % well.
            messag = int16([0 0 6 (int16(256*1)+int16(4)) 20 20]);
            fwrite(o.ser1, messag, 'int16');
            for i=1:100
                pause(0.005);
                if o.ser1.BytesAvailable>3; break; end;
            end
            if i==100; 
                disp('No data from TCP port!!!');
                press = [0 0];
                return;
            end;
            reply = fread(o.ser1, o.ser1.BytesAvailable, 'uint8')';
            if sw; o.update_temperatures(reply); end;
            % Gas pressures are on the last two Ains
            v1 = reply(end-7:end-4);
            v2 = reply(end-3:end);
            U = [typecast(uint32(v1(3)*256^3 + v1(4)*256^2 + v1(1)*256 + v1(2)), 'single'),...
                typecast(uint32(v2(3)*256^3 + v2(4)*256^2 + v2(1)*256 + v2(2)), 'single')];
            %R0 = 46.9; %in ohms
            % whatever the conversion is!
            %press = (U-0.004*R0)./(16/R0)*200; % in mbars
            % Loop unrolled below
            press(1) = (U(1)-o.pres.calib(1).y0)*o.pres.calib(1).k; % in mbars
            press(2) = (U(2)-o.pres.calib(2).y0)*o.pres.calib(2).k; % in mbars
            %if press(1) > 0.0001; p(1) = press(1); else p(1) = nan; end;
            %if press(2) > 0.0001; p(2) = press(2); else p(2) = nan; end;
            p = press;
            % Some plotting as well
            % Ensure are only showing NN points, to keep her nimble
            NN = get(o.pres.hist, 'Value');
            tt = datenum(clock);
            hplot = get(o.pres.ax, 'Children');
            twogas = get(o.pres.gasnum, 'Value');
            if isempty(hplot) % In case we've not plotted anything before
                if twogas
                    plot(o.pres.ax, tt, p(1), 'ko', tt, p(2), 'ro');
                else
                    plot(o.pres.ax, tt, p(1), 'ko');
                end
                ylabel(o.pres.ax, 'Pressure / mbar');
                datetick(o.pres.ax, 'x', 'HH:MM:SS');
                return;
            end
            if length(hplot)==1 && twogas % if we just started two gases
                set(o.pres.ax, 'NextPlot', 'add');
                plot(o.pres.ax, tt, p(2), 'ro');
                datetick(o.pres.ax, 'x', 'HH:MM:SS');
                return;
            end
            % Finally for normal plot updating
            for i=1:(twogas+1)
                xdat = get(hplot(i), 'XData'); ydat = get(hplot(i), 'YData');
                % Crop to good length
                if length(xdat)>NN+1
                    xdat = xdat(end-NN:end); ydat = ydat(end-NN:end);
                end
                % Figure out which handle we have!
                if isequal(get(hplot(i), 'Color'), [1 0 0])
                    ppp = p(2);
                else
                    ppp = p(1);
                end
                set(hplot(i), 'XData', [xdat tt], 'YData', [ydat ppp]);
                
                if abs(p(i)-o.pres.(['target' num2str(i)]))< 1%0.01*o.pres.target
                    set(o.pres.(['curr' num2str(i)]), 'BackgroundColor', 'g', 'String', sprintf('%3.2f', p(i)));
                else
                    set(o.pres.(['curr' num2str(i)]), 'BackgroundColor', 'r', 'String', sprintf('%3.2f', p(i)));
                end
            end
            datetick(o.pres.ax, 'x', 'HH:MM:SS');
        end
        
        function set_register(o, it)
            value = str2double(get(o.temp(it).target, 'String'));
            tt = float2bin(single(value));
            uu = bin2dec(tt(1:16));
            ud = bin2dec(tt(17:end));
            reg_num = 38+2*it; % Registers 40 onwards
            mb = o.modbus.ui1;
            messag = int16([ 0 0 6 (int16(256*mb)+int16(6)) reg_num ud]);
            fwrite(o.ser1, messag, 'int16');
            messag = int16([ 0 0 6 (int16(256*mb)+int16(6)) reg_num+1 uu]);
            fwrite(o.ser1, messag, 'int16');
        end
        
        function update_temperatures(o, reply)
            if ~get(o.tempmon, 'Value'); return; end;
            temprep = fliplr(reply);
            for i=1:8
                %v2 = reply(end-3:end);
                v = fliplr(temprep(end-3-4*(i-1):end-4*(i-1)));
                ttemp = sprintf('%x%x%x%x', uint8([v(3) v(4) v(1) v(2)]));
                T = hexsingle2num(ttemp);
                set(o.temp(i).curr, 'String', sprintf('%2.3f', T));
            end
        end
        
        function fill_(o,~)
            if (~strcmp(o.ser1.Status, 'open') || ~(strcmp(o.ser2.Status, 'open')))
                disp('TCP not open!');
                return;
            end
            % Turn Vin on, then off again; Vin is on channels D0 & D1
            vset = 1; % = '0000 0001'
            if get(o.pres.gasnum, 'Value'); vset = 3; end; % = '0000 0011'
            mb = o.modbus.val2;
            for i=1:20
                messag = int16([0 0 6 (int16(256*mb)+int16(15)) 20 2 (int16(256*1)+int16(vset))]);
                fwrite(o.ser2, messag, 'int16');
                pause(0.4);
                messag = int16([0 0 6 (int16(256*mb)+int16(15)) 20 2 (int16(256*1)+int16(0))]);
                fwrite(o.ser2, messag, 'int16');
                pause(0.1);
                p = o.get_pressure(0);
                twogas = get(o.pres.gasnum, 'Value');
                if ~twogas
                    if p(1)>o.pres.target1+1
                        break;
                    end
                else
                    if (p(1)>o.pres.target1+1 && p(2)>o.pres.target2+1)
                        break;
                    end
                end
            end;
        end
        
        function regulate_(o,v)
            %disp('double yay'); return; %Debugg and test
            % First turn off the pressure monitoring
            vv = get(o.poll.poll, 'Value');
            set(o.pres.regbutton, 'String', 'Working...');
            if vv
                o.poll.start_stop(0);
                %set(o.poll.poll, 'Enable', 'off');
            end
            % Now fill the reservoir
            fill_(o,v)
            %pause(0.025);
            % Now, measure the pressure and start iterating towards the
            % correct pressure
            gasnum = get(o.pres.gasnum, 'Value');
            goodit = [0 30*(~gasnum)]; % If only using one gas, will break if gas1 is good
            for i=1:200
                sw = 0;
                disp(['it ' num2str(i)]);
                press = o.get_pressure(0);
                if sum(press<0.0002) % In case of a bad read by get_pressure
                    pause(0.05);
                    continue;
                end
                % For line 1
                if press(1) > o.pres.target1-1; % Avoid going too low
                    if abs(press(1)-o.pres.target1) > 1%0.01*o.pres.target2
                        sw = 1;
                        goodit(1) = 0;
                    else
                        goodit(1) = goodit(1)+1;
                    end
                end
                % If using two gases, line 2:
                if gasnum
                    if press(2) > o.pres.target2-1; % Avoid going too low
                        if abs(press(2)-o.pres.target2) > 1%0.01*o.pres.target2
                            sw = sw + 2; % If set to 1 previously, should work
                            goodit(2) = 0;
                        else
                            goodit(2) = goodit(2)+1;
                        end
                    end
                end
                o.release_(sw);
                if sum(goodit>15)==2; 
                    %set(o.pres.regbutton, 'String', 'GO');
                    break; 
                end; % Change number here depending on stability.
            end
            % And turn pressure monitoring back on, if was on before.
            set(o.pres.regbutton, 'String', 'GO');
            if vv
                o.poll.start_stop(1);
                %set(o.poll.poll, 'Enable', 'on');
            end
            
        end
        
        function release_(o,sw)
            if (~strcmp(o.ser1.Status, 'open') || ~(strcmp(o.ser2.Status, 'open')))
                disp('TCP not open!');
                return;
            end
            % Turn V1s on, then off again; V1s are on channels D2 & D3 on ser2
            % sw determines whether we release line 1, line2 or both
            % if sw = 1, vset = '0000 0001' or release 1 only
            % if sw = 2, vset = '0000 0010' or release 2 only
            % if sw = 3, vset = '0000 0011' or release both
            if (sw == 0); return; end;
            vset = sw;
            %vset = 1; % = '0000 0001'
            %if get(o.pres.gasnum, 'Value'); vset = 3; end; % = '0000 0011'
            mb = o.modbus.val2;
            messag = int16([0 0 6 (int16(256*mb)+int16(15)) 22 2 (int16(256*1)+int16(vset))]);
            fwrite(o.ser2, messag, 'int16');
            pause(0.025);
            messag = int16([0 0 6 (int16(256*mb)+int16(15)) 22 2 (int16(256*1)+int16(0))]);
            fwrite(o.ser2, messag, 'int16');
            pause(0.025);
            % Turn V2s on, then off again; V2s are on channel D4 & D5 on ser2
            messag = int16([0 0 6 (int16(256*mb)+int16(15)) 24 2 (int16(256*1)+int16(vset))]);
            fwrite(o.ser2, messag, 'int16');
            pause(0.025);
            messag = int16([0 0 6 (int16(256*mb)+int16(15)) 24 2 (int16(256*1)+int16(0))]);
            fwrite(o.ser2, messag, 'int16');
            pause(0.025);
        end
        
        function closefcn(o)
            opt = questdlg('Are you sure you want to quit?', 'Confirm exit', 'Yes','Cancel', 'Cancel');
            if strcmp(opt, 'Yes')
                delete(o);
                delete(1001);
            else
                return;
            end
        end
    end
end