% Modcom SCADA module to get data from the Omega WISE-7118 module
% Specify IP and you're good to go.
% Includes option to plot live graph or to write polls to file.

classdef ModcomControl < uiextrasX.VBox
    
    properties
        ip
        modbus
        ser
        poll
        control
        graph
        connect
        channels
        nAI = 10
        nDI = 6
    end
    
    methods
        function o = ModcomControl(varargin)
            if nargin==0
                Parent = figure;
            elseif nargin==1
                Parent = varargin{1};
            end
            set(Parent, 'Resize', 'off', 'Toolbar', 'none', 'MenuBar', 'none', 'Color', 'w', 'Position', [100 600 600 175]);
            o@uiextrasX.VBox('Parent', Parent);
            defer(o)
            hbox = uiextrasX.HBox('Parent', o);
            uicontrol('Style', 'text', 'String', 'IP:', 'Parent', hbox);
            o.ip = uicontrol('Style', 'edit', 'String', '192.168.255.1', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'Port', 'Parent', hbox);
            uicontrol('Style', 'edit', 'String', '502', 'Parent', hbox);
            o.connect = uicontrol('Style', 'togglebutton', 'String', 'OPEN TCP', 'Parent', hbox, 'Callback', @(s,e)open_close(o, get(o.connect, 'Value')));
            uicontrol('Style', 'text', 'String', 'Modbus ID', 'Parent', hbox);
            o.modbus.val = 1;
            o.modbus.ui = uicontrol('Style', 'edit', 'String', num2str(o.modbus.val), 'Parent', hbox, 'Callback', @(s,e)modbus_update(o,1));
            hbox = uiextrasX.HBox('Parent', o);
            o.poll = PollTimer(hbox);
            set(o.poll.polltime, 'String', '250');
            addlistener(o.poll, 'Poll', @(s,e)poll_data(o,1));
            o.graph.ctrl = uicontrol('Style', 'togglebutton', 'String', 'GRAPH', 'Parent', hbox, 'Callback', @(s,e)plot_(o,get(o.graph.ctrl, 'Value')));
            hbox = uiextrasX.HBox('Parent', o);
            hp = uiextrasX.Panel('Parent', hbox, 'Title', 'Analogue inputs', 'Padding', 4);
            hbox = uiextrasX.HBox('Parent', hp);
            for i=1:o.nAI
                vbox = uiextrasX.VBox('Parent', hbox);
                o.channels.achan(i).label = uicontrol('Style', 'edit', 'String', ['channel ' num2str(i-1)], 'Parent', vbox);
                o.channels.achan(i).box = uicontrol('Style', 'text', 'String', '0.5', 'Parent', vbox);
                o.channels.achan(i).val = 0.5;
                o.channels.achan(i).plotflag = 0;
                o.channels.achan(i).hplot = [];

            end
            hbox = uiextrasX.HBox('Parent', o);
            hp = uiextrasX.Panel('Parent', hbox, 'Title', 'Digital IO', 'Padding', 4);
            hbox = uiextrasX.HBox('Parent', hp);
            for i=1:o.nDI
                vbox = uiextrasX.VBox('Parent', hbox);
                o.channels.dchan(i).label = uicontrol('Style', 'edit', 'String', ['channel ' num2str(i-1)], 'Parent', vbox);
                o.channels.dchan(i).button = uicontrol('Style', 'pushbutton', 'String', 'OFF', 'Parent', vbox, 'Callback', @(s,e)digital_io(o,i));
            end
            resume(o);
            o.Sizes = [25 25 -1 -1];
        end
        
        function open_close(o,val)
            if ~isfield(o, 'ser')
                o.ser = tcpip(get(o.ip, 'String'), 502);
            end
            set(o.ser, 'Terminator', 'CR/LF', 'Timeout', 1, 'InputBufferSize', 30000, 'ByteOrder', 'BigEndian');
            if val
                try
                    fopen(o.ser);
                    disp(['TCP on ' get(o.ip, 'String') ' now open']);
                    set(o.connect, 'String', 'CLOSE TCP');
                    digital_io(o,-1);
                catch
                    disp(['Unable to open TCP ' get(o.ip, 'String') '!!']);
                end
            else
                fclose(o.ser);
                set(o.connect, 'String', 'OPEN TCP');
            end
        end
        
        function digital_io(o,val)
            if ~strcmp(o.ser.Status, 'open')
                return;
            end
            if val==-1
                messag = int16([0 0 6 (int16(256*o.modbus.val)+int16(1)) 20 o.nDI]);
                fwrite(o.ser, messag, 'int16');
                while o.ser.BytesAvailable < 4; pause(0.005); end
                reply = fread(o.ser, o.ser.BytesAvailable, 'uint8')';
                state = fliplr(dec2bin(reply(end),8));
                for i=1:o.nDI
                    %disp([ bool2onoff(str2double(state(i))) '  ' (state(i))]);
                    set(o.channels.dchan(i).button, 'String', upper(bool2onoff(str2double(state(i)))));
                end
            else
                if strcmpi(get(o.channels.dchan(val).button, 'String'), 'off')
                    state = 65280;
                    set(o.channels.dchan(val).button, 'String', 'ON');
                else
                    state = 0;
                    set(o.channels.dchan(val).button, 'String', 'OFF');
                end
                messag = int16([0 0 6 (int16(256*o.modbus.val)+int16(5)) (0019+val) state]);
                fwrite(o.ser, messag, 'int16');
                while o.ser.BytesAvailable < 4
                    pause(0.005);
                end
                reply = fread(o.ser, o.ser.BytesAvailable, 'uint8')';
            end
        end
        
        function poll_data(o,vv)
            
            if ~strcmpi(o.ser.Status, 'open')
                return;
            end
            messag = int16([0 0 6 (int16(256*1)+int16(4)) int16(0020) int16(20)]);
            fwrite(o.ser, messag, 'int16');
            while o.ser.BytesAvailable<4
                pause(0.0005);
            end
            reply = fread(o.ser, o.ser.BytesAvailable, 'uint8')';
            dat = reply(10:end);
            dat = reshape(dat,4,o.nAI)';
            for i=1:o.nAI
                v = dat(i,:);
                ttemp = sprintf('%x%x%x%x', uint8([v(3) v(4) v(1) v(2)]));
                o.channels.achan(i).val = hexsingle2num(ttemp);
                set(o.channels.achan(i).box, 'String', sprintf('%2.4f',o.channels.achan(i).val));
                if ~isempty(o.channels.achan(i).hplot) && get(o.graph.ctrl, 'Value') % do plotting if needed
                    set(o.channels.achan(i).hplot, 'XData',...
                        [get(o.channels.achan(i).hplot, 'XData') o.poll.tottime],'YData',...
                        [get(o.channels.achan(i).hplot, 'YData') o.channels.achan(i).val]);
                end
            end
        end
        
        function plot_(o,v)
            o.graph.fig = figure(98999);
            set(o.graph.fig, 'CloseRequestFcn', @(s,e) disp('Click CLOSE'), 'Position', [100 200 600 300]);
            if ~v
                delete(o.graph.fig);
                return;
            end
            
            hbox = uiextrasX.HBox('Parent', o.graph.fig);
            vbox = uiextrasX.VBox('Parent', hbox);
            hp = uipanel('Parent', hbox);
            o.graph.axes = axes('Parent', hp, 'NextPlot', 'add');
            xlabel('Time / s');
            ylabel('Value / a.u.');
            
            incols = jet(o.nAI);
            for i=1:o.nAI
                o.channels.achan(i).col = incols(i,:);
                o.channels.achan(i).plotbox = uicontrol('Style', 'checkbox', 'Parent', vbox, 'BackgroundColor', incols(i,:), 'String', get(o.channels.achan(i).label, 'String'), 'Value', o.channels.achan(i).plotflag, 'Callback', @(s,e)toggle_plotflag(o,i));
                if o.channels.achan(i).plotflag
                    o.channels.achan(i).hplot = plot(o.graph.axes, o.poll.tottime, o.channels.achan(i).val, 'Color', o.channels.achan(i).col);
                else
                    o.channels.achan(i).hplot = [];
                end
            end
            hbox.Sizes = [150 -1];
        end
        
        function toggle_plotflag(o,v)
            o.channels.achan(v).plotflag = get(o.channels.achan(v).plotbox, 'Value');
            if o.channels.achan(v).plotflag
                 o.channels.achan(v).hplot = plot(o.graph.axes, o.poll.tottime, o.channels.achan(v).val, 'Color', o.channels.achan(v).col);
            else
                delete(o.channels.achan(v).hplot);
                o.channels.achan(v).hplot = [];
            end
        end
        
        function modbus_update(o,~)
            port = str2double(get(o.modbus.ui, 'String'));
            if port<1 || port>250 || isnan(port)
                o.modbus.val = 1;
            else
                o.modbus.val = port;
            end
            set(o.modbus.ui, 'String', num2str(o.modbus.val));
        end
        
        function delete(o)
            open_close(o,0);
        end
    end
end