function v = onoff2bool(s)
%
v = strcmp(s, 'on');
