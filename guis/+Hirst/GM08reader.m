% Measuring magnetic fields
% For the Hirst GM08 device: need to flush the line to make sure you get
% the very latest reading

classdef GM08reader<uiextras.VBox
    
    properties
        serial
        sett
        meas
    end
    
    methods
        function o = GM08reader(parent)
            %o@uiextras.VBox();
            if strcmp(get(parent, 'Type'), 'figure')
                figure(parent);
                set(parent, 'MenuBar', 'none', 'HandleVisibility', 'callback', 'NumberTitle', 'off', 'Name', 'GM08 gaussmeter','DefaultUIControlFontSize', 14); % callback
                set(parent, 'Position', [400 400 500 80]);
                %uimenu(obj.Parent, 'Label', '');
            end
            o@uiextras.VBox('Parent', parent);
            vbox = uiextras.VBox('Parent', o);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('style', 'text', 'String', 'COM port:', 'Parent', hbox);
            o.serial.name = uicontrol('Style', 'edit', 'String', 'COM3', 'Parent', hbox);
            o.serial.openclose = uicontrol('Style', 'togglebutton', 'String', 'OPEN', 'Parent', hbox,'Callback', @(obj,val)connect(o, get(o.serial.openclose, 'Value')));
            hbox = uiextras.HBox('Parent', vbox);
            o.sett.single = uicontrol('style', 'pushbutton', 'Parent', hbox, 'String', 'Single', 'Callback', @(obj,val)getvalue(o,0));
            o.sett.cont = uicontrol('style', 'checkbox', 'Parent', hbox, 'String', 'Continuous', 'Callback', @(obj,val)contgetvalue(o,get(o.sett.cont, 'Value')));
            o.sett.type = uicontrol('Style', 'text', 'FontSize', 24, 'Parent', hbox, 'String', 'DC');
            o.sett.meas = uicontrol('Style', 'text', 'FontSize', 24, 'Parent', hbox, 'String', '0 T');
            o.sett.contn = false;
        end
        
        function connect(o,val)
            ser_port = get(o.serial.name, 'String');
            if val
                try
                    o.serial.port = serial(ser_port);
                    set(o.serial.port, 'InputBufferSize', 2048, 'BaudRate', 9600, ...
                        'DataBits', 8, 'Parity', 'none',...
                        'StopBits', 1, 'Terminator', 'CR');
                    fopen(o.serial.port);
                    set(o.serial.openclose, 'String', 'CLOSE');
                    %set_initial_state(o);
                    fprintf('Serial port %s is now %s \n', ser_port, o.serial.port.status);

                catch
                    fprintf('Opening port %s failed\n', ser_port);
                    instrreset;
                    fprintf('Running instrreset. Try again.\n');
                    set(o.serial.openclose, 'Value', 0);
                end
            else
                try
                    fprintf('Trying to close serial %s\n', ser_port);
                    fclose(o.serial.port);
                    delete(o.serial.port);
                catch
                    fprintf('Not able to close serial %s\n', ser_port);
                end
                set(o.serial.openclose, 'String', 'OPEN');
            end
        end
        
        function value = getvalue(o,~)
            tic;
            while toc<0.1
                data = fgetl(o.serial.port);
            end
            %chardata = (data)
            value = str2double(data(2:7));
            switch data(10)
                case '0'
                    unit = 'T';
                case '1'
                    unit = 'G';
                case '2'
                    unit = 'A/m';
                case '3'
                    unit = 'Oe';
                otherwise
                    unit = 'N/A';
            end
            if ~strcmp(data(9), '0') unit = ['m' unit]; end;
            la = [data(2:7) ' ' unit];
            %la = chardata(:2)
            set(o.sett.meas, 'String', la);
            switch data(11)
                case '0'
                    type = 'DC';
                case '1'
                    type = 'DC peak';
                case '2'
                    type = 'AC';
                case '3' 
                    type = 'AC max';
                case '4'
                    type = 'AC peak';
                otherwise 
                    type = 'N/A';
            end
            set(o.sett.type, 'String', type);    
            %fprintf('%s\n', data(9))
        end
        
        function contgetvalue(o, val)
            if ~val
                o.sett.contn = false;
                set(o.sett.cont, 'String', 'Continuous');
            else
                o.sett.contn = true;
                set(o.sett.cont, 'String', 'STOP');
                startstop(o);
            end
        end
        
        function startstop(o)
            while o.sett.contn
                getvalue(o,0);
                pause(0.1);
                if ~get(o.sett.cont, 'Value')
                    break;
                end
            end
        end
        
        function delete(o)
            connect(o,0);
        end
    end
        
end