classdef Controller < uiextrasX.VBox
    % Is meant to control the WTMCD-M stepper chopper
    % Can change the microstepping and the speed, also go to some counter
    % position
    % Can address individual devices with changing the address.
    
    
    properties
        serial_port
        serial_address
        serial_open_close
        serial_obj
        
        speed
        usteps
        current
        drive_button
        direction
        position
    end
    
    methods
        function o = Controller(varargin)
            if (nargin==0) Parent = figure;
            elseif (nargin==1) Parent = varargin{1}; end;
            o@uiextrasX.VBox('Parent', Parent);
            defer(o)
            mainbox = o;
            if strcmp(get(Parent, 'Type'), 'figure')
                set(o.Parent, 'MenuBar', 'none', 'NumberTitle', 'off', 'Name', 'Stepper controller');
                set(o.Parent, 'DefaultUIControlFontSize', 14, 'DefaultAxesFontSize', 10);
                set(o.Parent, 'Units', 'Pixels', 'Position', [500 300 400 200]);
            end
            hbox = uiextrasX.HBox('Parent', mainbox);
            uicontrol('Style', 'text', 'String', 'Serial port:', 'Parent', hbox);
            o.serial_port = uicontrol('Style', 'edit', 'String', 'COM3', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'Address', 'Parent', hbox);
            o.serial_address = uicontrol('Style', 'edit', 'String', '00000', 'Parent', hbox);
            o.serial_open_close = uicontrol('Style', 'togglebutton', 'String', 'OPEN', 'Parent', hbox,...
                'Callback', @(obj,val)connect(o, get(o.serial_open_close, 'Value')));
            hbox = uiextrasX.HBox('Parent', mainbox);
            uicontrol('Style', 'text', 'String', 'Speed', 'Parent', hbox);
            %o.speed = uicontrol('Style', 'edit', 'String', '1000', 'Parent', hbox);
            o.speed = javaNumSpinner2(1000, 50, 10000, 50, hbox, 'StateChangedCallBack', @(obj,val)set_speed(o,val));
            uicontrol('Style', 'text', 'String', 'Microsteps', 'Parent', hbox);
            o.usteps = uicontrol('Style', 'popupmenu', 'String', {'1' '2' '4' '8' '16' '32' '64'}, 'Value', 3,...
                'Parent', hbox, 'Callback', @(obj,val)set_usteps(o, get(o.usteps, 'Value')));
            hbox = uiextrasX.HBox('Parent', mainbox);
            o.direction = uicontrol('Style', 'togglebutton', 'String', '+', 'Parent', hbox, 'Callback', @(obj,val)set_dir(o,get(o.direction, 'Value')));
            o.drive_button = uicontrol('Style', 'togglebutton', 'String', 'DRIVE', 'Parent', hbox, 'Callback', @(obj,val)drive(o,get(o.drive_button, 'Value')));
            uicontrol('Style', 'text', 'String', 'Target', 'Parent', hbox);
            o.position.target = uicontrol('Style', 'edit', 'String', '190098', 'Parent', hbox);
            uicontrol('Style', 'pushbutton', 'String', 'GO', 'Parent', hbox, 'Callback', @(s,e)gototarget(o,0));
            hbox = uiextrasX.HBox('Parent', mainbox);
            uicontrol('Style', 'text', 'String', 'Current', 'Parent', hbox);
            o.current = javaNumSpinner2(0.5,0.1,20,0.1,hbox);
            uicontrol('Style', 'pushbutton', 'String', 'Get position', 'Parent', hbox,...
                'Callback', @(obj,val)get_position(o,val));
            o.position.current = uicontrol('Style', 'text', 'Parent', hbox, 'String', '0');
            resume(o)
            
            assignin('base', 'StepperController', o);
            loadstate(o,0);
        end
        
        function connect(o,val)
            ser_port = get(o.serial_port, 'String');
            if val
                try
                    o.serial_obj = serial(ser_port);
                    set(o.serial_obj, 'InputBufferSize', 2048);
                    set(o.serial_obj, 'BaudRate', 9600);
                    set(o.serial_obj, 'DataBits', 8);
                    set(o.serial_obj, 'Parity', 'none');
                    set(o.serial_obj, 'StopBits', 1);
                    set(o.serial_obj, 'Terminator', 'CR');
                    %set(o.serial_obj, 'BytesAvailableFcnCount', 2);
                    %set(o.serial_obj, 'BytesAvailableFcnMode', 'byte');
                    %set(o.serial_obj, 'BytesAvailableFcn', @(obj,val)readport(o,val));
                    fopen(o.serial_obj);
                    set(o.serial_open_close, 'String', 'CLOSE');
                    %set_initial_state(o);
                    fprintf('Serial port %s is now %s \n', ser_port, o.serial_obj.status);

                catch
                    fprintf('Opening port %s failed\n', ser_port);
                    instrreset;
                    fprintf('Running instrreset. Try again.\n');
                    %set(o.serial_open_close, 'Value', 0);
                end
            else
                try
                    fprintf('Trying to close serial %s\n', ser_port);
                    fclose(o.serial_obj);
                    delete(o.serial_obj);
                catch
                    fprintf('Not able to close serial %s\n', ser_port);
                end
                set(o.serial_open_close, 'String', 'OPEN');
            end
        end
        
        function readport(o,val)
            bav = o.serial_obj.BytesAvailable;
            if (bav == 0)
                return
            else
                char(fread(o.serial_obj, bav)')
            end
        end
        
        function set_speed(o,val)
            %disp('yesy');
            if strcmp(o.serial_obj.status, 'open')
                cmd = [sprintf('%sV%i', get_address(o), o.speed.value/50) char(13)];
                fprintf(o.serial_obj, cmd);
                fgetl(o.serial_obj);
                fprintf('Speed of drive %s set to %i\n', get_address(o), o.speed.value);
            else
                fprintf('Port %s still closed. Failed to set speed of %s\n', o.serial_port, get_address(o));
            end
        end
        
        function set_usteps(o, val)
            if strcmp(o.serial_obj.status, 'open')
                cmd = [sprintf('%sE%i', get_address(o), 2^(val-1)) char(13)];
                fprintf(o.serial_obj, cmd);
                fgetl(o.serial_obj);
                fprintf('Drive %s now has %i microsteps\n', get_address(o), 2^(val-1));
            else
                fprintf('Port %s still closed. Failed to set speed of %s\n', o.serial_port, get_address(o));
            end
        end
        
        function address = get_address(o)
            addr = bin2dec(get(o.serial_address, 'String'));
            if addr<16 && addr>=0
                address = char(65+addr);
            elseif addr>15 && addr<=32
                address = char(81+addr);
            end
        end
        
        function set_dir(o,val)
            if val
                set(o.direction, 'String', '-');
            else
                set(o.direction, 'String', '+')
            end
        end
        
       function set_current(o,val)
            if strcmp(o.serial_obj.status, 'open')
                cmd = [sprintf('%sC%i', get_address(o), o.current.value/0.1) char(13)];
                fprintf(o.serial_obj, cmd);
                fgetl(o.serial_obj);
                fprintf('Current of drive %s set to %i\n', get_address(o), o.speed.value);
                pause(0.05);
            else
                fprintf('Port %s still closed. Failed to set speed of %s\n', o.serial_port, get_address(o));
            end
       end
        
        function gototarget(o,val)
            if strcmp(o.serial_obj.status, 'open')
                target = str2double(get(o.position.target, 'String'));
                cmd = [sprintf('%sM%i', get_address(o), target) char(13)];
                fprintf(o.serial_obj, cmd);
                fgetl(o.serial_obj);
                %fprintf('Current of drive %s set to %i\n', get_address(o), o.speed.value);
                pause(0.05);
                %get_position(o,0);
            else
                fprintf('Port %s still closed. Failed to set speed of %s\n', o.serial_port, get_address(o));
            end
        end 
       
        function drive(o,val)
            if val
                try
                    cmd = [sprintf('%sD%s', get_address(o),get(o.direction, 'String')) char(13)];
                    fprintf(o.serial_obj, cmd);
                    fgetl(o.serial_obj);
                    set(o.drive_button, 'String', 'STOP');
                catch
                    set(o.drive_button, 'Value', 0);
                end
            else
                cmd = [sprintf('%sD', get_address(o)) char(13)];
                fprintf(o.serial_obj,cmd);
                fgetl(o.serial_obj);
                set(o.drive_button, 'String', 'DRIVE');
            end
        end
        
        function get_position(o,val)
            try
                cmd = [sprintf('%sP', get_address(o)) char(13)];
                fprintf(o.serial_obj, cmd);
                %pause(0.05)
                %va = fread(o.serial_obj, o.serial_obj.BytesAvailable);
                va = fgetl(o.serial_obj)
                pos = char(va);
                set(o.position.current, 'String', pos(2:end)); 
            catch
                fprintf('Failed to get position from drive %s\n', get_address(o));
            end
        end
        
        function set_initial_state(o)
            cmd=[sprintf('%sV', get_address(o)) char(13)]; fprintf(o.serial_obj, cmd); v0=char(fgetl(o.serial_obj));
            cmd=[sprintf('%sE', get_address(o)) char(13)]; fprintf(o.serial_obj, cmd); e0=char(fgetl(o.serial_obj));
            cmd=[sprintf('%sP', get_address(o)) char(13)]; fprintf(o.serial_obj, cmd); p0=char(fgetl(o.serial_obj));
            o.speed.value = v0*50;
            set(o.usteps, 'Value', e0);
        end
        
        function loadstate(o,flag)
            if flag
                
            else
                fol = fileparts(which('Stepper.Controller'));
                if (exist([fol '/state.mat'], 'file'))
                    load([fol '/state.mat']);
                end
            end
        end
        
        function delete(o)
            loadstate(o,1);
            if get(o.serial_open_close, 'Value')
                fprintf('Automatically closing COM port!\n')
                    connect(o,0);
            end
        end
    end
end
