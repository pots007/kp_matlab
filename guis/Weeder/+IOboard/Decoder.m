classdef Decoder < uiextrasX.VBox
    % Is meant to control the WTMCD-M stepper chopper
    % Can change the microstepping and the speed, also go to some counter
    % position
    % Can address individual devices with changing the address.
    
    
    properties
        serial_port
        serial_address
        serial_open_close
        serial_obj
        
        func
        vals
        send
        
    end
    
    methods
        function o = Decoder(varargin)
            if (nargin==0) Parent = figure;
            elseif (nargin==1) Parent = varargin{1}; end;
            o@uiextrasX.VBox('Parent', Parent);
            defer(o)
            mainbox = o;
            if strcmp(get(Parent, 'Type'), 'figure')
                set(o.Parent, 'MenuBar', 'none', 'NumberTitle', 'off', 'Name', 'Stepper decoder');
                set(o.Parent, 'DefaultUIControlFontSize', 14, 'DefaultAxesFontSize', 10);
                set(o.Parent, 'Units', 'Pixels', 'Position', [500 300 450 200]);
            end
            hbox = uiextrasX.HBox('Parent', mainbox);
            uicontrol('Style', 'text', 'String', 'Serial port:', 'Parent', hbox);
            o.serial_port = uicontrol('Style', 'edit', 'String', 'COM1', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'Address', 'Parent', hbox);
            o.serial_address = uicontrol('Style', 'edit', 'String', '00000', 'Parent', hbox);
            o.serial_open_close = uicontrol('Style', 'togglebutton', 'String', 'OPEN', 'Parent', hbox,...
                'Callback', @(obj,val)connect(o, get(o.serial_open_close, 'Value')));
            hbox = uiextrasX.HBox('Parent', mainbox);
            vbox = uiextrasX.VBox('Parent', hbox);
            uicontrol('Style', 'text', 'String', 'Ch.', 'Parent', vbox);
            uicontrol('Style', 'text', 'String', 'I/O', 'Parent', vbox);
            uicontrol('Style', 'text', 'String', 'St', 'Parent', vbox);
            for i=1:14
                vbox = uiextrasX.VBox('Parent', hbox);
                uicontrol('Style', 'text', 'String', sprintf('%02i',i), 'Parent', vbox);
                %o.func(i) = uicontrol('Style', 'checkbox', 'String', {}, 'Value', 1, 'Parent', vbox);
                o.func(i) = uicontrol('Style', 'togglebutton', 'String', 'O', 'Parent', vbox, 'Callback', @(obj,val)setIO(o, i));
                o.vals(i) = uicontrol('Style', 'edit', 'String', '1', 'Parent', vbox);
            end
            hbox = uiextrasX.HBox('Parent', mainbox);
            uicontrol('Style', 'pushbutton', 'String', 'Set IOs', 'Parent', hbox, 'Callback', @(obj,val)setIO(o,val));
            o.send = uicontrol('Style', 'pushbutton', 'String', 'SEND/RECEIVE', 'Parent', hbox, 'Callback', @(obj,val)sendIO(o,val));
            %uicontrol('Style', 'pushbutton', 'String', 'Get position', 'Parent', hbox,...
            %    'Callback', @(obj,val)get_position(o,val));
            %o.position.current = uicontrol('Style', 'text', 'Parent', hbox, 'String', '0');
            resume(o)
            
            if (nargin==0) assignin('base', 'StepperDecoder', o); end
            %loadstate(o,0);
        end
        
        function connect(o,val)
            ser_port = get(o.serial_port, 'String');
            if val
                try
                    o.serial_obj = serial(ser_port);
                    set(o.serial_obj, 'InputBufferSize', 2048);
                    set(o.serial_obj, 'BaudRate', 9600);
                    set(o.serial_obj, 'DataBits', 8);
                    set(o.serial_obj, 'Parity', 'none');
                    set(o.serial_obj, 'StopBits', 1);
                    set(o.serial_obj, 'Terminator', 'CR');
                    fopen(o.serial_obj);
                    fprintf('Serial port %s is now %s \n', ser_port, o.serial_obj.status);
                    set(o.serial_open_close, 'String', 'CLOSE');
                    fprintf(o.serial_obj, [get_address(o) 'X1' char(13)]); %Turn command echo off by default
                    %set_initial_state(o);
                catch
                    fprintf('Opening port %s failed\n', ser_port);
                    instrreset;
                    fprintf('Running instrreset. Try again.\n');
                    %set(o.serial_open_close, 'Value', 0);
                end
            else
                try
                    fprintf('Trying to close serial %s\n', ser_port);
                    fclose(o.serial_obj);
                    delete(o.serial_obj);
                catch
                    fprintf('Not able to close serial %s\n', ser_port);
                end
                set(o.serial_open_close, 'String', 'OPEN');
            end
        end
        
        function setIO(o,i)
            val = get(o.func(i), 'Value');
            if val
                set(o.func(i), 'String', 'I');
                fprintf(o.serial_obj, [get_address(o) 'I' get_channel(o,i) char(13)]);
                fgetl(o.serial_obj)
            else
                set(o.func(i), 'String', 'O');
                fprintf(o.serial_obj, [get_address(o) 'L' get_channel(o,i) char(13)]);
                fgetl(o.serial_obj);
            end
        end
        
        function sendIO(o, va)
            if strcmp(o.serial_obj.status, 'open')
                for i=1:14
                    if ~get(o.func(i), 'Value')
                        %write to output
                        fprintf(o.serial_obj, [get_address(o) get_bit(o,i) get_channel(o,i) char(13)]);
                        fgetl(o.serial_obj);
                        disp([get_bit(o,i) ' out to ' get_channel(o,i)]);
                    else
                        %read from input
                        fprintf(o.serial_obj, [get_address(o) 'R' get_channel(o,i) char(13)]);
                        dat = fgetl(o.serial_obj);
                        if (length(dat)==3)
                            if (strcmpi(dat(3), 'H'))
                                set(o.vals(i), 'String', '1');
                            elseif (strcmpi(dat(3), 'L'))
                                set(o.vals(i), 'String', '0');
                            end
                            disp([dat(3) ' in from ' get_channel(o,i)]);
                        end
                        
                    end
                end
            else
                fprintf('Port %s still closed. Failed to set speed of %s\n', o.serial_port, get_address(o));
            end
        end
        
        function bit = get_bit(o,i)
            v = get(o.vals(i), 'String');
            if strcmp(v, '1')
                bit = 'H';
            elseif strcmp(v,'0')
                bit = 'L';
            else
                bit = 'L';
            end
        end
        
        function address = get_address(o)
            addr = bin2dec(get(o.serial_address, 'String'));
            if addr<16 && addr>=0
                address = char(65+addr);
            elseif addr>15 && addr<=32
                address = char(81+addr);
            end
        end
        
        function chan = get_channel(o,i)
            chan = char(64+i);
        end
        
        
        function loadstate(o,flag)
            if flag
                
            else
                fol = fileparts(which('StepperDrice.Controller'));
                if (exist([fol '/state.mat'], 'file'))
                    load([fol '/state.mat']);
                end
            end
        end
        
        function delete(o)
            loadstate(o,1);
            if get(o.serial_open_close, 'Value')
                fprintf('Automatically closing COM port!\n')
                connect(o,0);
            end
        end
    end
end
