classdef ShortPulsePlay < uiextras.VBox
    %Play around with short pulses
    % Supports different temporal and spectral profiles
    % more explanation goes here
    
    properties
        time_axis
        omega_axis
        fnam
        
        domain
        pulse
        units
        extras
        c
        w
        t
    end
    
    methods
        function o = ShortPulsePlay(varargin)
            if (nargin==0) Parent = figure;
            elseif (nargin==1) Parent = varargin{1}; end;
            o@uiextras.VBox('Parent', Parent);
            %defer(o)
            mainbox = o;
            if strcmp(get(Parent, 'Type'), 'figure')
                set(o.Parent, 'NumberTitle', 'off', 'Name', 'Short pulse play');
                if (ismac)
                    set(o.Parent, 'DefaultUIControlFontSize', 14, 'DefaultAxesFontSize', 14);
                elseif ispc
                    set(o.Parent, 'DefaultUIControlFontSize', 11, 'DefaultAxesFontSize', 12);
                end
                set(o.Parent, 'Units', 'Pixels', 'Position', [500 200 800 600]);
            end
            hbox1 = uiextras.HBox('Parent', mainbox);
            %The column to have all controls etc
            vboxL = uiextras.VBox('Parent', hbox1);
            hbox = uiextras.HBox('Parent', vboxL);
            o.domain = uicontrol('Style', 'togglebutton', 'String', 'TIME', 'Parent', hbox,...
                'Callback', @(obj,val)domain_switch(o,get(o.domain, 'Value')));
            hbox = uiextras.HBox('Parent', vboxL);
            uicontrol('Style', 'text', 'String', 'Shape', 'Parent', hbox);
            o.pulse.shape = uicontrol('Style', 'popupmenu', 'Parent', hbox,...
                'String', {'Gaussian', 'Lorentzian', 'sech', 'sech^2', 'from file'}, 'Value', 1,'Callback', @(obj,v)calc_plot(o,get(o.domain,'Value')));
            hbox.Sizes = [60 140];
            hbox = uiextras.HBox('Parent', vboxL);
            uicontrol('Style', 'text', 'String', 'FWHM', 'Parent', hbox);
            o.pulse.fwhm = uicontrol('Style', 'edit', 'String', '30', 'Parent', hbox,'Callback', @(obj,v)calc_plot(o,get(o.domain,'Value')));
            o.pulse.units = uicontrol('Style', 'text', 'String', 'fs', 'Parent', hbox);
            hbox.Sizes = [70 100 30];
            hbox = uiextras.HBox('Parent', vboxL);
            uicontrol('style', 'pushbutton', 'String', 'File:', 'Parent', hbox, 'Callback', @(obj,v)set_file(o,get(o.domain,'Value')));
            o.fnam.name = uicontrol('style', 'edit', 'String', ' ', 'Parent', hbox);
            o.fnam.type = 1; %lambda-space by default
            hbox.Sizes = [70 130];
            hbox = uiextras.HBox('Parent', vboxL);
            uicontrol('style', 'text', 'String', 'Spot FWHM', 'Parent', hbox);
            o.pulse.w0 = uicontrol('style', 'edit', 'String', '20', 'Parent', hbox);
            uicontrol('style', 'text', 'String', 'um', 'Parent', hbox);
            hbox.Sizes = [100 70 30];
            hbox = uiextras.HBox('Parent', vboxL);
            uicontrol('style', 'text', 'String', 'Total energy', 'Parent', hbox);
            o.pulse.E = uicontrol('style', 'edit', 'String', '10', 'Parent', hbox);
            uicontrol('style', 'text', 'String', 'J', 'Parent', hbox);
            hbox.Sizes = [100 70 30]; 
            hbox = uiextras.HBox('Parent', vboxL);
            o.t.tlimshow = uicontrol('style', 'checkbox', 'String', 'Show transform limited', ...
                'Parent', hbox, 'Callback', @(obj,v)calc_plot(o,get(o.domain,'Value')));
            hbox = uiextras.HBox('Parent', vboxL);
            uicontrol('Style', 'text', 'String', 'Phase coefficients', 'Parent', hbox);
            grid = uiextras.Grid('Parent', vboxL);
            for i=1:4
                uicontrol('Style', 'text', 'String', [num2str(i) ' order'], 'Parent', grid,...
                    'HorizontalAlignment', 'Left');
            end
            for i=1:4
                o.pulse.coeff(i) = uicontrol('Style', 'edit', 'String', num2str(0), ...
                    'Parent', grid,'Callback', @(obj,v)calc_plot(o,get(o.domain,'Value')));
            end
            grid.ColumnSizes = [80 120];
            hbox = uiextras.HBox('Parent', vboxL);
            o.extras.FROG = uicontrol('style', 'togglebutton', 'Parent', hbox, ...
                'Value', 0, 'String', 'FROG trace', 'Callback', @(obj,v)FROG_enable(o,get(o.extras.FROG,'Value')));
            o.extras.Wigner = uicontrol('style', 'togglebutton', 'Parent', hbox, ...
                'Value', 0, 'String', 'Wigner plot', 'Callback', @(obj,v)Wigner_enable(o,get(o.extras.Wigner,'Value')));
            
            %The axes column
            vboxR = uiextras.VBox('Parent', hbox1);
            sb = uipanel('Parent', vboxR);
            o.time_axis = axes('Parent', sb);
            xlabel('Time');
            sb = uipanel('Parent', vboxR);
            o.omega_axis = axes('Parent', sb);
            xlabel('\omega');
            
            hbox1.Sizes = [200 600];
            vboxL.Sizes = [30 30 30 30 30 30 30 30 100 50];
            o.c = 299792458;
            o.pulse.omega0 = 2*pi*o.c/8e-7;
            %resume(o)
            
            assignin('base', 'ShortPulse', o);
        end
        
        function domain_switch(o,val)
            if val
                set(o.domain, 'String', 'SPECTRUM');
                set(o.pulse.units, 'String', 'nm');
            else
                set(o.domain, 'String', 'TIME');
                set(o.pulse.units, 'String', 'fs');
            end
            calc_plot(o,val);
        end
        
        function calc_plot(o,val)
            if val %spectral
                o.w.grid = (2000:0.1:2700)*1e12;
                switch get(o.pulse.shape, 'Value')
                    case 1 %Gaussian
                        dw = 1e-9*str2double(get(o.pulse.fwhm, 'String'))*sqrt(2);
                        dw = o.pulse.omega0^2*dw/(2*pi*o.c);
                        o.w.amp = exp(-4*log(2)*(o.w.grid-o.pulse.omega0).^2/dw^2);
                    case 2 %Lorentzian
                        dw = 1e-9*str2double(get(o.pulse.fwhm, 'String'))*2;
                        dw = o.pulse.omega0^2*dw/(2*pi*o.c);
                        o.w.amp = 1./(1+(2*(o.w.grid-o.pulse.omega0)./dw).^2);
                    case 3 % sech
                        dw = 1e-9*str2double(get(o.pulse.fwhm, 'String'))*1.55;
                        dw = o.pulse.omega0^2*dw/(2*pi*o.c);
                        o.w.amp = sech(1.76.*(o.w.grid-o.pulse.omega0)/dw);
                    case 4 %sech^2
                        dw = 1e-9*str2double(get(o.pulse.fwhm, 'String'))*1.55;
                        dw = o.pulse.omega0^2*dw/(2*pi*o.c);
                        o.w.amp = sech(1.76.*(o.w.grid-o.pulse.omega0)/dw).^2;
                    case 5
                        load_file(o,val);
                end
                totphase = calc_tot_phase(o,o.w.grid);
                tlim = ifftX(o.w.grid, o.w.amp, 2^15);
                o.w.amp =  o.w.amp.*exp(1i*totphase);
                t0 = ifftX(o.w.grid, o.w.amp, 2^15);
                t_fwhm = calc_fwhm(o,t0(:,1), abs(t0(:,2)).^2);
                w0 = 1e-6*str2double(get(o.pulse.w0, 'String'))/sqrt(2*log(2));
                o.t.grid = t0(:,1); 
                o.t.amp = t0(:,2)/max(abs(tlim(:,2)))*sqrt(str2double(get(o.pulse.E, 'String'))*0.3e-4/(w0^2*t_fwhm));
                tlim(:,2) = tlim(:,2)/max(abs(tlim(:,2)))*sqrt(str2double(get(o.pulse.E, 'String'))*0.3e-4/(w0^2*t_fwhm));
            else %temporal
                o.t.grid = (-1500:0.1:1500)*1e-15;
                switch get(o.pulse.shape, 'Value')
                    case 1 %Gaussian
                        dt = 1e-15*str2double(get(o.pulse.fwhm, 'String'))*sqrt(2);
                        o.t.amp = exp(-4*log(2)*o.t.grid.^2/dt^2);
                    case 2 %Lorentzian
                        dt = 1e-15*str2double(get(o.pulse.fwhm, 'String'))*2;
                        o.t.amp = 1./(1+(2*o.t.grid./dt).^2);
                    case 3 %sech
                        dt = 1e-15*str2double(get(o.pulse.fwhm, 'String'))*1.55;
                        o.t.amp = sech(1.76.*o.t.grid/dt);
                    case 4 %sech^2
                        dt = 1e-15*str2double(get(o.pulse.fwhm, 'String'))*1.55;
                        o.t.amp = sech(1.76.*o.t.grid/dt).^2;
                end
                totphase = calc_tot_phase(o,o.t.grid) + o.pulse.omega0.*o.t.grid;
                w0 = fftX(o.t.grid, o.t.amp.*exp(1i*totphase), 2^14);
                o.w.grid = w0(:,1); o.w.amp = w0(:,2);
            end
            axes(o.time_axis);
            if get(o.t.tlimshow, 'Value')
                plot(tlim(:,1), abs(tlim(:,2)).^2, '--r');
                hold on;
                %plot(o.t.grid, abs(o.t.amp).^2);
                q = myplotyy(o.time_axis, o.t.grid, abs(o.t.amp).^2, unwrap(angle(o.t.amp)));
                hold off;
            else
                %plot(o.t.grid, abs(o.t.amp).^2);
                q = myplotyy(o.time_axis, o.t.grid, abs(o.t.amp).^2, unwrap(angle(o.t.amp)));
            end
            [maxv, maxi] = max(abs(o.t.amp).^2);
            [~, minin] = min(abs( abs(o.t.amp(1:maxi(1))).^2-exp(-2)*maxv ));
            llim = o.t.grid(minin);
            [~, minin] = min(abs( abs(o.t.amp(maxi(1):end)).^2-exp(-2)*maxv));
            rlim = o.t.grid(maxi(1)+minin);
            set(q.ax1, 'xlim',(5*[llim rlim]));
            %set(q.ax2, 'xlim',(5*[llim rlim])); 
            tlims = 5*[llim rlim];
            title(['fwhm width ' sprintf('%2.2f',1e15*calc_fwhm(o,o.t.grid, abs(o.t.amp).^2)) ' fs']);
            xlabel(q.ax1, 't / s')
            ylabel(q.ax1, 'I / W cm^{-2}');
            %plot(o.w.grid, abs(o.w.amp).^2);
            q = myplotyy(o.omega_axis, o.w.grid, abs(o.w.amp).^2, totphase);
            [maxv, maxi] = max(abs(o.w.amp).^2);
            [~, minin] = min(abs( abs(o.w.amp(1:maxi(1))).^2-exp(-2)*maxv ));
            llim = o.w.grid(minin);
            [~, minin] = min(abs( abs(o.w.amp(maxi(1):end)).^2-exp(-2)*maxv));
            rlim = o.w.grid(maxi(1)+minin);
            xlim(o.pulse.omega0+5*([llim rlim]-o.pulse.omega0));
            title(['fwhm bandwidth ' sprintf('%2.2e',calc_fwhm(o,o.w.grid, abs(o.w.amp).^2)) ' rad/s']);
            xlabel(q.ax1, '\omega / rad s^{-1}');
            if get(o.extras.FROG, 'Value')
                Egrid = linspace(o.w.grid(1), o.w.grid(end), 256);
                Etgrid = linspace(-pi/(Egrid(2)-Egrid(1)),pi/(Egrid(2)-Egrid(1)),256);
                Ereal = interp1(o.w.grid, real(o.w.amp), Egrid);
                Eimag = interp1(o.w.grid, imag(o.w.amp), Egrid);
                FROG = genFROGalone(complex(Ereal,Eimag)');
                curfig = gcf;
                figure(12345)
                imagesc(Etgrid*1e15, Egrid*1e-15, FROG');
                xlabel('Time / fs'); ylabel('\omega / rad fs^{-1}');
                set(gca, 'XLim', tlims*1e15); setAllFonts(12345,20);
                %plot(o.w.grid, imag(o.w.amp), Egrid, Eimag, 'xr');
                figure(curfig);
            end
            if get(o.extras.Wigner, 'Value')
                Egrid = linspace(o.w.grid(1), o.w.grid(end), 1024);
                Etgrid = linspace(-pi/(Egrid(2)-Egrid(1)),pi/(Egrid(2)-Egrid(1)),1024);
                Ereal = interp1(o.w.grid, real(o.w.amp), Egrid);
                Eimag = interp1(o.w.grid, imag(o.w.amp), Egrid);
                Wigner = WignerCalc(complex(Ereal,Eimag)');
                curfig = gcf;
                figure(123456)
                imagesc(Etgrid*1e15, Egrid*1e-15, fliplr(Wigner));
                xlabel('Time / fs'); ylabel('\omega / rad fs^{-1}');
                set(gca, 'XLim', tlims*1e15); setAllFonts(123456,20);
                %plot(o.w.grid, imag(o.w.amp), Egrid, Eimag, 'xr');
                figure(curfig);
            end
        end
        
        function totphase = calc_tot_phase(o, grid)
            if get(o.domain, 'Value') %spectral
                baseunit = 10^-15;
                l0 = o.pulse.omega0;
            else %temporal
                baseunit = 10^15;
                l0 = 0;
            end
            totphase = zeros(size(grid));
            for i=1:4
                totphase = totphase+str2double(get(o.pulse.coeff(i),'String'))*baseunit^i*(grid-l0).^i/factorial(i);
            end
        end
        
        function val = calc_fwhm(o, grid, amp)
            [maxv, maxi] = max(abs(amp));
            [~, minin] = min(abs( abs(amp(1:maxi(1)))-0.5*maxv ));
            llim = grid(minin);
            [~, minin] = min(abs( abs(amp(maxi(1):end))-0.5*maxv));
            rlim = grid(maxi(1)+minin);
            val = abs(rlim-llim);
            
            HM = maxv*0.5; totX = length(grid);
            %Find first HM location.
            i = 2;
            while sign(amp(i)-HM) == sign(amp(i-1)-HM)
                i = i+1;
            end
            slope = (HM-amp(i-1)) / (amp(i)-amp(i-1));
            x1 = grid(i-1) + slope*(grid(i)-grid(i-1));
            %Find second HM location. Assume it's after first one and after max value.
            i = maxi+1;
            while ((sign(amp(i)-HM) == sign(amp(i-1)-HM)) && (i <= totX-1))
                i = i+1;
            end
            %Avoid NaN returns.
            if (i ~= totX)
                slope = (HM-amp(i-1)) / (amp(i)-amp(i-1));
                x2 = grid(i-1) + slope*(grid(i)-grid(i-1));
                val = abs(x2 - x1);
            else
                disp('No second edge.')
                return
            end
        end
        
        function set_file(o, val)
            if val
                [fname, pname, sind] = uigetfile({'*.*', 'Lambda-space'; '*.*', 'Omega-space'},...
                    'Select spectrum file', fileparts(which('ShortPulsePlay')));
                set(o.fnam.name, 'String', [pname '/' fname]);
                o.fnam.type = sind; % 1 for lambda-space, 2 for omega-space
            else
                disp('Really? Loading in a temporal file to look at spectrum from that? Really?? Try spectra, mate');
            end
        end
        
        
        function FROG_enable(o,val)
            if val
                o.extras.FROGwindow = figure(12345);%
                set(12345, 'NumberTitle', 'off', 'Name', 'Short play play: FROG');
                set(12345, 'Colormap', flipud(gray(256)));
            else
                try 
                    close(o.extras.FROGwindow)
                catch
                    
                end
            end
        end
        
        
        function Wigner_enable(o,val)
            if val
                o.extras.Wignerwindow = figure(123456);%
                set(123456, 'NumberTitle', 'off', 'Name', 'Short play play: Wigner');
                set(123456, 'Colormap', flipud(gray(256)));
            else
                try 
                    close(o.extras.Wignerwindow)
                catch
                    
                end
            end
        end
        
        
        function load_file(o, val)
            if val %spectral
                data = importdata(get(o.fnam.name, 'String'));
                if (isstruct(data))
                    data = data.data;
                end
                if size(data,1) == 2 %ensure everything is in columns
                    data = data';
                end
                if (o.fnam.type == 1) %file is in lambda-space
                    lambda = data(:,1);
                    omega = 2*pi*o.c./lambda;
                    amp = 1/2*pi*o.c.*lambda.^2.*data(:,2);
                else
                    omega = data(:,1);
                    amp = data(:,2);
                end
                amp = interp1(omega, sqrt(amp), o.w.grid);
                o.w.amp = amp./max(amp);
                %Take off the noise - works better for Parseval
                o.w.amp(isnan(o.w.amp)) = 0;
            else
                disp('Really? Loading in a temporal file to look at spectrum from that? Really?? Try spectra, mate');
            end
        end
    end
end