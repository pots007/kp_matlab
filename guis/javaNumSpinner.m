function h = javaNumSpinner(value, min, max, incr, varargin)
% Create Java Swing JSpinner with numeric model with uicontrol interface. 
%
% h = javaNumSpinner(value, min, max, incr, varargin)
% The user can type numbers which aren't multiples of the increment.
% To force integers, cast value, min, max and incr to int32.
% For no min/max, pass inf (or int32(inf)).
% The returned handle has similar properties to a uicontrol, making it
% compatible with layout managers which assume these properties such as
% uiextras. It is implemented using the uicomponent command.
% NOTE: the parent must be specified as a numeric handle rather than an
% object i.e. for a uiextras HBox, use 'Parent',double(hbox).
% 
% References:
% http://undocumentedmatlab.com/blog/using-spinners-in-matlab-gui/
%
% (c) Dane R. Austin 2012
h = uicomponent(javax.swing.JSpinner(javax.swing.SpinnerNumberModel(value, min, max, incr)), varargin{:});
