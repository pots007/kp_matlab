% Mock magnetic field generator
% samples gaussian random numbers; histogram of the resulting fieldmap
% should be gaussian!

classdef MockField < uiextras.HBox 
    
    properties
        reading
    end
    
    methods 
        function obj = MockField(parent)
            obj@uiextras.HBox('Parent', parent);
            obj.reading = uicontrol('style', 'text', 'Parent', obj, 'FontSize', 40, 'String', '0');
        end
        
        function val = getvalue(obj)
            val = randn(1);
            set(obj.reading, 'String', num2str(val));
        end
    end
    
end