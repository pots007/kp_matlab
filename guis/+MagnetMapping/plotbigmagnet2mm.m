if ispc
    load('F:\Kristjan\Documents\Uni\Dropbox\MATLAB\guis\+MagnetMapping\bigmagnet2mm.mat');
elseif ismac
    load('/Users/kristjanpoder/Dropbox/MATLAB/guis/+MagnetMapping/bigmagnet2mm.mat');
end
figure(1);
imagesc(y,x, -map');
cb = colorbar;
axis image;
hold on;
plot([148 448 448 148 148], [-10 -10 90 90 -10], 'k');
plot([298 298], [-10 90], 'k');
hold off;
xlabel('x / mm');
ylabel('y / mm');
ylabel(cb, 'Magnetic field / T');
setAllFonts(1,20);
load('invfire');
colormap(iijcm);
set(gca, 'CLim', [-0.01 0.05]);
export_fig('field_fringes', '-nocrop', '-transparent', '-pdf', 1);
set(gca, 'CLim', [-0.01 1.1]);
export_fig('field', '-nocrop', '-transparent', '-pdf', 1);