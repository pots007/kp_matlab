\documentclass[a4paper,11pt]{article}
\usepackage{graphicx, url, amsmath, subcaption, caption}

\author{Kristjan P\~oder}

\title{MagnetMapping user guide}
\date{July 2016}
\begin{document}
\maketitle
\tableofcontents

\newcommand{\matlab}{\textsc{matlab}}

%\newpage
\section{Introduction}

This document aims to give a quick start to how to use the MagnetMapping software. Written by the author in 2014, the software is a Graphical User Interface aimed to simplify mapping magnets in 2 dimensions. The design of the software is object oriented, with any drive controller and any field controller suitable to use, as long as a few methods as defined. The software will then take a user prescribed number of steps, measuring the field at all of these points, and save a mat file the measured field. In case something goes wrong, it is possible to save the map after each completed line and resume from a previously saved file. 

\section{Prerequisites}

The software required to run MagnetMapping:

\begin{itemize}
	\item \matlab\space (obviously) -- both HG1 and HG2 supported
	\item Instrument Control Toolbox (for \verb+serial+)
	\item ICMatlab folder in \matlab\space search path
\end{itemize}

The hardware required to run MagnetMapping:

\begin{itemize}
	\item 2 MagnetMapping compatible stages
	\item 1 MagnetMapping compatible Hall probe
\end{itemize}

The hardware requirements are mainly driven by the need to have a few methods implemented in the controller. The first of these, \verb|moveAbs|, must  take one double parameter which is the absolute position to move to. The second requirement is \verb|isMoving|, a function with no inputs returning a boolean value. Obviuosly the return value is true if the stage is still in motion and 0 otherwise.

For the B-field controller, only one method is a must. This is \verb|getvalue|, a method with no inputs and which returns a double value of the B-field strength.

In order to simplify debugging, and potentially test out the user's use of the program, mock controllers were written. These are part of the MagnetMapping class: \verb|MockLinear| ad \verb|MockField|. Using these allows to test out various parts of the program without any hardware having to be connected. Also, if only a 1D map is required, the missing axis can be replaced with the \verb|MockLinear| controller.

\section{Using the software}

\subsection{Working method}

The underlying code doing the actual mapping is extremely simple. Initially, it moves the stage locations to start locations in both x and y. It then scans all positions along the y axis. After reaching the end, y position is driven back to y starting location and the x stage is advanced by one step. This basic iteration is repeated until the final value in x is reached. 

There is an option to save the map after every completed line. This is highly useful and I recommend its use fro two reasons. Firstly, if the computer crashes or something else happens, the user does not need to start from the first position again, but can resume midway. Secondly, if the save location is within a file hosting folder, the updated map will be synchronised to any other computer after each line. If the software is left alone in the lab, it makes it very easy to check up on the progress and ascertain that nothing has gone wrong.

Finally a practical comment. The standard stags used at IC have very different speeds. As the y-axis is scanned most frequently, it is advisable to ensure that the y-direction is both the shorter dimension of the map and that the stage used for the y-motion is the faster one out of the pair.

\subsection{Starting the software}

The software is started from the \matlab\space command prompt. Assuming a clean start, the user must first set up the correct paths by running the function \verb|make_path_IClab|. Next, the MagnetMapping software is launched:

\begin{verbatim}
	MagnetMapping.Mapping(xcontrol, ycontrol, field, parentfig)
\end{verbatim}

The first of these is the controller name for the x-direction. As mentioned, make this your slower and longer stage. The second argument is the controller for the y-axis, the quicker and shorter stage. Third input is the B-field controller and finally a handle to the parent figure. This is a compatibility argument for cases when the user wants to use the software within other GUIs. If running the software on its own, it is sufficient to use the figure command. As an example, using the following arguments:
\begin{verbatim}
	MagnetMapping.Mapping('LongDrive.OLController',...
	'PI.ControllerGCS', 'Hirst.GM08reader', figure)
\end{verbatim}
allows the user to make use of the long 600~mm stage from IC, any PI CGS controlled stage and the Hirst GM08 Gaussmeter. The resulting figure is shown in Figure \ref{fig1}.
\begin{figure}[t!]
	\centering
	\includegraphics[width=\columnwidth]{MM_full2}
	\caption{The default GUI of the MagnetMapping interface.}
	\label{fig1}
\end{figure}

The GUI panel ought to be self-explanatory, but a short description of the various field is given anyway.

We start with the Settings panel, shown in Figure \ref{fig2}. This is the panel in the top of the GUI, responsible for setting the ranges of motion and the step sizes. Note all numbers are in units of mm.
\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{MM_Settings}
	\caption{The Settings panel on MagnetMapping.}
	\label{fig2}
\end{figure}
\begin{itemize}
	\item \verb|x start position| is the lower limit of motion for the $x$ axis;
	\item \verb|x end position| is the higher limit of motion for the $y$ axis;
	\item \verb|x step size| is the size of the step to be taken. Currently this is limited to 0.5~mm increments, due to the active area of the Hall rpobe usually being about $1\:\mathrm{mm}^2$.
\end{itemize}
All the commands are identical for the other axis. At the end of each line is a dynamically updated count of the number of steps the current setting would require. Below the settings for the y direction is a user reminder, displaying the total number of steps required and assuming an average of 2~s per step, the total time required. Note how easy it is to inflate the required time to days!

Below this line is the path and filename for saving the measured map. The \verb|save after every line| tickbox enables saveing the completed progress into a file after the completion of each y-scan. The button next to it allows for resuming from a half-completed state. Note upon resuming all the current settings will be overwritten by the data in the file, apart from save path and filename.

The left hand side is dedicated to various controllers and interacting with these. All the buttons remain functional during a mapping session, so asking the stage to go to somewhere else will result in undefined behaviour.

All the controllers are interfaced with serial ports. On Windows machines, these are simply COM ports. On Macs, they are `files' located in \verb|/dev/|, starting with \verb|tty|. Ensure that each controller points to the correct port, otherwise nothing will work. Secondly, for linear stages the controllers oftern feature a DIP switch to set the address of the device. The setting on that must match that in the controller for any particular stage.

Once the software has been run and settings have been determined, the controllers need to be set up. This constitutes typing in the correct address and COM port and pressing \verb|OPEN|. The pop-up windows will appear, asking the user to select a configuration file. This is a step ensuring highest level of compatibility and software re-use. The code in the controller is independent of the stage used, and thus the limits of motion, velocity, acceleration, scales etc need to be loaded from a file. On older machines, generating this popup may take up to quite a few seconds, so patience is required here. Each controller has a default file selected, so the user shuld simply press OK and not change the selected filename. It is then advisable to home the stages. This ensures the full range of motion is available: controllers, when connected to power, have no idea where the stage actually is. 
\end{document}

