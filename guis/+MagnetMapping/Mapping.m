
classdef Mapping < uiextras.HBox
    properties
        xcontroller
        ycontroller
        bfield
        settings
        plots
    end
    methods
        function obj = Mapping(xcontrol, ycontrol, field, parentfig)
            figure(parentfig);
            set(parentfig, 'Units', 'normalized', 'Position', [0 0 1 1], 'Menubar', 'none', 'HandleVisibility', 'callback', 'NumberTitle', 'off', 'Name', 'Magnet Mapping');
            obj@uiextras.HBox('Parent', parentfig);
            hpanel = uiextrasX.Panel('Parent', obj, 'Title', 'Controllers', 'Padding', 5);
            hbox1 = uiextrasX.VBox('Parent', hpanel);
            hbox = uiextrasX.Panel('Parent', hbox1, 'Title', 'x-axis', 'Padding', 5);
            obj.xcontroller = feval(xcontrol, hbox);
            %set(obj.xcontroller.serial, 'String', '79708641');
            hbox = uiextrasX.Panel('Parent', hbox1, 'Title', 'y-axis', 'Padding', 5);
            obj.ycontroller = feval(ycontrol, hbox);
            hbox = uiextrasX.Panel('Parent', hbox1, 'Title', 'magnetic field', 'Padding', 5);
            obj.bfield = feval(field, hbox);
            
            % xlim and ylims + step size, add option to call limits of stage
            % estimate time for the entire array
            % create bool array with grid, then step to the ones that
            % haven't been done yet. Save after every line in the longer
            % direction.
            vboxp = uiextrasX.VBox('Parent', obj);
            hpanel = uiextrasX.Panel('Parent', vboxp, 'Title', 'Settings', 'Padding', 5);
            vbox = uiextrasX.VBox('Parent', hpanel);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'x start position', 'Parent', hbox);
            obj.settings.xmin = javaNumSpinner2(0, -50, 600, 1, hbox, 'StateChangedCallback', @(s,e) lims_changed(obj));
            uicontrol('Style', 'text', 'String', 'x end position', 'Parent', hbox);
            obj.settings.xmax = javaNumSpinner2(100, 1, 600, 1, hbox, 'StateChangedCallback', @(s,e) lims_changed(obj));
            uicontrol('Style', 'text', 'String', 'x step size', 'Parent', hbox);
            obj.settings.xstep = javaNumSpinner2(1, 0.5, 20, 0.5, hbox, 'StateChangedCallback', @(s,e) lims_changed(obj));
            obj.settings.xsteps = uicontrol('Style', 'text', 'String', '100 steps in x', 'Parent', hbox);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'y start position', 'Parent', hbox);
            obj.settings.ymin = javaNumSpinner2(0, 0, 600, 1, hbox, 'StateChangedCallback', @(s,e) lims_changed(obj));
            uicontrol('Style', 'text', 'String', 'y end position', 'Parent', hbox);
            obj.settings.ymax = javaNumSpinner2(50, 1, 600, 1, hbox, 'StateChangedCallback', @(s,e) lims_changed(obj));
            uicontrol('Style', 'text', 'String', 'y step size', 'Parent', hbox);
            obj.settings.ystep = javaNumSpinner2(1, 0.5, 20, 0.5, hbox, 'StateChangedCallback', @(s,e) lims_changed(obj));
            obj.settings.ysteps = uicontrol('Style', 'text', 'String', '50 steps in y', 'Parent', hbox);
            hbox = uiextrasX.HBox('Parent', vbox);
            obj.settings.totsteps = uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Total of 5000 steps; expect it to take at least 15000 s or 4.166 hours');
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('style', 'text', 'Parent', hbox, 'String', 'Path:');
            obj.settings.savepath = uicontrol('style', 'edit', 'Parent', hbox, 'String', pwd);
            uicontrol('style', 'pushbutton', 'Parent', hbox, 'String', 'Browse', 'Callback', @(s,e)setpath(obj));
            uicontrol('style', 'text', 'Parent', hbox, 'String', 'Filename:');
            obj.settings.filename = uicontrol('style', 'edit', 'String', 'b-field', 'Parent', hbox);
            obj.settings.saveoften = uicontrol('style', 'checkbox', 'String', 'Save after every line', 'Parent', hbox);
            uicontrol('style', 'pushbutton', 'Parent', hbox, 'String', 'Reload half-completed state', 'Callback', @(s,e)reload(obj));
            hbox.Sizes = [50 -1 100 100 150 200 350];
            hbox = uiextrasX.HBox('Parent', vbox);
            obj.settings.startbutton = uicontrol('style', 'togglebutton', 'Parent', hbox, 'String', 'START MAPPING', 'Callback', @(s,e)map(obj));
            hpanel = uiextrasX.Panel('Parent', vboxp, 'Title', 'Results', 'Padding', 5);
            vbox = uiextrasX.VBox('Parent', hpanel);
            compan = uiextrasX.Panel('Parent', vbox, 'Title', 'Completed steps', 'Padding', 5);
            obj.settings.compaxis = axes('Parent', compan);
            %respan = uiextrasX.Panel('Parent', vbox, 'Title', 'Magnetic field', 'Padding', 5);
            %hbox = uiextrasX.HBox('Parent', respan);
            hbox = uipanel('Parent', vbox, 'Title', 'Magnetic field');
            obj.settings.fieldaxis = axes('Parent', hbox);
            obj.settings.bcolorbar = [];
            obj.settings.bimage = imagesc(randn(100), 'Parent', obj.settings.fieldaxis);
            vboxp.Sizes = [150 -1];
            
            obj.Sizes = [300 -1];
            la = load('owncolourmaps');
            obj.settings.colmaps = la.colmap;
            lims_changed(obj);
        end
        
        function lims_changed(obj)
            xmin = get(obj.settings.xmin, 'Value');
            xmax = get(obj.settings.xmax, 'Value');
            xstep = get(obj.settings.xstep, 'Value');
            ymin = get(obj.settings.ymin, 'Value');
            ymax = get(obj.settings.ymax, 'Value');
            ystep = get(obj.settings.ystep, 'Value');
            obj.settings.xgrid = xmin:xstep:xmax;
            obj.settings.ygrid = ymin:ystep:ymax;
            set(obj.settings.xsteps, 'String', [num2str(length(obj.settings.xgrid)) ' steps in x']);
            set(obj.settings.ysteps, 'String', [num2str(length(obj.settings.ygrid)) ' steps in y']);
            totsteps = length(obj.settings.ygrid)*length(obj.settings.xgrid);
            set(obj.settings.totsteps, 'String', ['Total of ' num2str(totsteps) ' steps; expect it to take at least ' num2str(totsteps*3) ' s or ' sprintf('%1.3f hours', totsteps/1000)]);
            obj.settings.compim = imagesc(zeros(length(obj.settings.ygrid), length(obj.settings.xgrid)), 'Parent', obj.settings.compaxis);
            xlabel(obj.settings.compaxis,'x'); ylabel(obj.settings.compaxis,'y');
        end
        
        function map(obj)
            obj.settings.completed = get(obj.settings.compim, 'CData');
            if sum(obj.settings.completed(:))==0
                ymin=1;
                xoffset=0;
                obj.settings.bfield = zeros(size(obj.settings.completed));
            else
                ymin = floor(sum(obj.settings.completed(:))/length(obj.settings.xgrid))+1;
                xoffset = mod(sum(obj.settings.completed(:)), length(obj.settings.xgrid));
                obj.settings.bfield = get(obj.settings.bimage, 'CData');
            end
            set(obj.settings.bimage, 'CData', obj.settings.bfield, 'XData', obj.settings.xgrid, 'YData', obj.settings.ygrid);
            if isempty(obj.settings.bcolorbar)
                obj.settings.bcolorbar = colorbar('peer', obj.settings.fieldaxis, 'Position', [0.93 0.13 0.02 0.8149]);
            end
            set(obj.settings.fieldaxis, 'Position', get(obj.settings.compaxis, 'Position'));
            xlabel(obj.settings.fieldaxis,'x'); ylabel(obj.settings.fieldaxis,'y');
            axis tight;
            set(obj.settings.startbutton, 'String', 'STOP MAPPING');
            first = true;
            for i=ymin:length(obj.settings.ygrid)
                obj.ycontroller.moveAbs(obj.settings.ygrid(i));
                while obj.ycontroller.isMoving
                    pause(0.01);
                end
                if first == true
                    j1 = 1+xoffset;
                    first = false;
                else
                    j1 = 1;
                end
                for j=j1:length(obj.settings.xgrid)
                    if ~get(obj.settings.startbutton, 'Value')
                        set(obj.settings.startbutton, 'String', 'START MAPPING');
                        return;
                    end
                    obj.xcontroller.moveAbs(obj.settings.xgrid(j));
                    while obj.xcontroller.isMoving
                        pause(0.01);
                    end
                    B = obj.bfield.getvalue;
                    obj.settings.bfield(i,j) = B;
                    set(obj.settings.bimage, 'CData', obj.settings.bfield);
                    obj.settings.completed(i,j) = 1;
                    set(obj.settings.compim, 'CData', obj.settings.completed);
                    pause(0.1);
                    fprintf('Mapped %6i out of %6i positions\n', sum(obj.settings.completed(:)), length(obj.settings.xgrid)*length(obj.settings.ygrid));
                    title(obj.settings.compaxis, [num2str(sum(obj.settings.completed(:))) '/' num2str(length(obj.settings.xgrid)*length(obj.settings.ygrid))]);
                end
                if get(obj.settings.saveoften, 'Value')
                    savemap(obj);
                end
            end
            finished = true;
            savemap(obj);
            
            set(obj.settings.startbutton, 'Value', 0, 'String', 'START MAPPING');
            if finished
                set(obj.settings.compim, 'CData', zeros(size(get(obj.settings.compim, 'CData'))));
            end
        end
        
        function setpath(obj)
            dirname = uigetdir(pwd, 'Set path for field map');
            if dirname==0
                return;
            else
                set(obj.settings.savepath, 'String', dirname);
            end
        end
        
        function savemap(obj)
            filename = [get(obj.settings.savepath, 'String') '/' get(obj.settings.filename, 'String')];
            x = obj.settings.xgrid;
            y = obj.settings.ygrid;
            map = obj.settings.bfield;
            completed = obj.settings.completed;
            save(filename, 'x', 'y', 'map', 'completed');
        end
        
        function reload(obj)
            [filename, pathname] = uigetfile('*.mat', 'Select file to load state from', pwd);
            if filename==0
                return;
            end
            filen = [pathname filename];
            r = load(filen);
            if (~isfield(r, 'x') || ~isfield(r, 'y') || ~isfield(r, 'completed'))
                disp('File insufficient!')
                return;
            end
            set(obj.settings.xmin, 'Value', min(r.x));
            set(obj.settings.xmax, 'Value', max(r.x));
            set(obj.settings.xstep, 'Value', r.x(2)-r.x(1));
            set(obj.settings.ymin, 'Value', min(r.y));
            set(obj.settings.ymax, 'Value', max(r.y));
            set(obj.settings.ystep, 'Value', r.y(2)-r.y(1));
            pause(0.1);
            obj.settings.compim = imagesc(r.completed, 'Parent', obj.settings.compaxis);
            set(obj.settings.bimage, 'CData', r.map, 'XData', r.x, 'YData', r.y);
        end
        
        function delete(obj)
            %butpressed = questdlg('Are you sure you want to quit?', 'Closing confirmation',...
            %    'Yes', 'No');
            %if strcmp(butpressed, 'No')
            %    return;
            %end
        end
        
    end
end