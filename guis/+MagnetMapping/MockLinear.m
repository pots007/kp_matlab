% Mock x stage for testing magnetmapping

classdef MockLinear < uiextras.HBox
    
    properties
        location
        inloc
        target
        counter
    end
    
    methods
        function o = MockLinear(parent)
            o@uiextras.HBox('Parent', parent);
            o.location = uicontrol('style', 'text', 'Parent', o, 'String', '0', 'FontSize', 40);
            o.target = uicontrol('style', 'text', 'Parent', o, 'String', '0', 'FontSize', 40);
        end
        
        function posn = getPosn(o)
            posn = str2double(get(o.location, 'String'));
        end
        
        function r = isMoving(o)
            if o.counter<10
                r = true;
                o.counter = o.counter + 1;
                tar = str2double(get(o.target, 'String'))-o.inloc;
                set(o.location, 'String', num2str(o.inloc + 0.1*o.counter*tar));
            else
                r = false;
            end
        end
        
        function moveAbs(o, posn)
            o.inloc = str2double(get(o.location, 'String'));
            if (o.inloc == posn)
                o.counter = 10;
                return;
            end
            o.counter = 0;
            set(o.target, 'String', num2str(posn));
        end
            
    end
    
end
