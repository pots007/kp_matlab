%GUI to control simple features of the Quantum Composer 9520 series via com
%port.

% Mod 01082015 - now save into user directory

%Kristjan Poder, IC, 2013

function hfig = QCcontrol()

hOb = figure('units','pixels',...
    'position',[150 400 600 300],... %[left bottom width height]
    'menubar','none',...
    'numbertitle','off',...
    'name','Quantum Composer control',...
    'resize','on', 'DeleteFcn', @closing);

S.nchan = 8;
S.working = [0,0,1,1,0,0,0,0];
S.grid = GetFigureArray(6,10,0.02, 0.02, 'down');
uicontrol('style', 'text', 'Units', 'Normalized', 'Position', S.grid(:,1), 'String', 'Channel');
uicontrol('style', 'text', 'Units', 'Normalized', 'Position', S.grid(:,11), 'String', 'Delay (ms)');
uicontrol('style', 'text', 'Units', 'Normalized', 'Position', S.grid(:,21), 'String', 'Amplitude (V)');
uicontrol('style', 'text', 'Units', 'Normalized', 'Position', S.grid(:,31), 'String', 'Width (us)');
uicontrol('style', 'text', 'Units', 'Normalized', 'Position', S.grid(:,41), 'String', 'Invert');
for i=1:S.nchan
    S.CH.chan(i).enable = uicontrol('style', 'checkbox', 'Units', 'Normalized', 'Position', S.grid(:,i+1), 'String', char(64+i), 'Value', S.working(i));
    S.CH.chan(i).delay = uicontrol('style', 'edit', 'Units', 'Normalized', 'Position', S.grid(:,i+11), 'String', '8');
    S.CH.chan(i).amp = uicontrol('style', 'edit', 'Units', 'Normalized', 'Position', S.grid(:,i+21), 'String', '15');
    S.CH.chan(i).wid = uicontrol('style', 'edit', 'Units', 'Normalized', 'Position', S.grid(:,i+31), 'String', '20');
    S.CH.chan(i).inv = uicontrol('style', 'checkbox', 'Units', 'Normalized', 'Position', S.grid(:,i+41), 'Value', 1);
end

if (exist([getuserdir '\state.mat'], 'file'))
    load([getuserdir '\state.mat']);
    for i=1:8
        ch = S.CH.chan(i);
        set(ch.enable, 'Value', oldstate(i,1));
        set(ch.delay, 'String', num2str(oldstate(i,2)));
        set(ch.amp, 'String', num2str(oldstate(i,3)));
        set(ch.wid, 'String', num2str(oldstate(i,4)));
        set(ch.inv, 'Value', oldstate(i,5));
    end
end
hfig = hOb;

S.pb = uicontrol('style', 'pushbutton', 'Units', 'Normalized', 'Position', S.grid(:,30), 'String', 'SEND', 'Callback', @send);
S.er = uicontrol('style', 'text', 'Units', 'Normalized', 'Position', S.grid(:,40), 'String', '...');
S.comport = uicontrol('style', 'edit', 'Units', 'Normalized', 'Position', S.grid(:,10), 'String', 'COM4');
S.opencombut = uicontrol('style', 'pushbutton', 'Units', 'Normalized', 'Position', S.grid(:,20), 'String', 'OPEN', 'BackGroundColor', 'r', 'Callback', @opencom);
S.runbut = uicontrol('style', 'pushbutton', 'Units', 'Normalized', 'Position', S.grid(:,50), 'String', 'RUN', 'Callback', @startstop);
guidata(hOb, S);
%uicontrol('style', 'text', 'Units', 'Normalized', 'Position', , 'String', 'Delay');

function opencom(hObject, ~)
S = guidata(hObject);
if (~isfield(S, 'com'))
    S.com = serial(get(S.comport, 'String'));
end
cmd = get(S.opencombut, 'String');
%opening ports
if (strcmp(cmd, 'OPEN'))
    try
        fopen(S.com);
    catch
        fprintf('Com port %s is not available! Running instrreset.\n', ...
            get(S.comport, 'String'));
        instrreset;
        return;
    end
    if (strcmp(S.com.Status, 'open'))
        set(S.opencombut, 'BackGroundColor', 'g', 'String', 'CLOSE');
    else
        fprintf('Error opening com port %s!\n', get(S.comport, 'String'));
    end
end
if (strcmp(cmd, 'CLOSE'))
    fclose(S.com);
    set(S.opencombut, 'BackGroundColor', 'r', 'String', 'OPEN');
end
guidata(hObject, S);


function send(hObject, ~)
S = guidata(hObject);
if (~isfield(S, 'com'))
    warndlg('Open com port first!', 'Warning!');
    return;
end
disp('Sending');
for i=1:8
    ch = S.CH.chan(i);
    if (~get(ch.enable, 'Value'))
        cmd = [':PULSE' num2str(i) ':STATE OFF' char(13) char(10)];
        fprintf(S.com, cmd);
        continue;
    else
        cmd = [':PULSE' num2str(i) ':STATE ON' char(13) char(10)];
        fprintf(S.com, cmd);
    end
    cmd = [':PULSE:WIDTH ' num2str(str2num(get(ch.wid, 'String'))*1e-6) char(13) char(10)]; fprintf(S.com, cmd);
    cmd = [':PULSE:DELAY ' num2str(str2num(get(ch.delay, 'String'))*1e-3) char(13) char(10)]; fprintf(S.com, cmd);
    cmd = [':PULSE:POLARITY ' getinversion(get(ch.inv, 'Value')) char(13) char(10)]; fprintf(S.com, cmd);
    cmd = [':PULSE:OUTPUT:MODE ADJUSTABLE' char(13) char(10)]; fprintf(S.com, cmd);
    cmd = [':PULSE:OUTPUT:AMPLITUDE ' get(ch.amp, 'String') char(13) char(10)]; fprintf(S.com, cmd);
    
end

function invstate = getinversion(value)
if (value)
    invstate = 'INVERTED';
else
    invstate = 'NORMAL';
end
    
function startstop(hObject, ~)
S = guidata(hObject);
if (~isfield(S, 'com'))
    return;
    if (strcmp(S.com.Status, 'open'))
        return;
    end
end
cmd1 = get(S.runbut, 'String');
if (strcmp(cmd1, 'RUN'))
    cmd = [':PULSE0:STATE ON' char(13) char(10)]; fprintf(S.com, cmd);
    set(S.runbut, 'String', 'STOP', 'BackGroundColor', 'g');
end
if (strcmp(cmd1, 'STOP'))
    cmd = [':PULSE0:STATE OFF' char(13) char(10)]; fprintf(S.com, cmd);
    set(S.runbut, 'String', 'RUN', 'BackGroundColor', 'r');
end


function closing(hObject, ~)
S = guidata(hObject);
oldstate = zeros(8,5);
for i=1:8
    ch = S.CH.chan(i);
    oldstate(i,1) = get(ch.enable, 'Value');
    oldstate(i,2) = str2double(get(ch.delay, 'String'));
    oldstate(i,3) = str2double(get(ch.amp, 'String'));
    oldstate(i,4) = str2double(get(ch.wid, 'String'));
    oldstate(i,5) = get(ch.inv, 'Value');
end
save([getuserdir '\state.mat'], 'oldstate');
if (~isfield(S, 'com'))
    return;
end
if (strcmp(S.com.Status, 'open'))
    fprintf('Closing com port %s\n', get(S.comport, 'String'));
    fclose(S.com);
end

%returns position array for figure array.

function out = GetFigureArray(nx, ny, margin, separation, dir)
%nx = 5;
%ny = 4;
%marg = 0.05;
%sep = 0.05;
flag = [];
if (strcmp(dir, 'across'))
    flag = 1;
end
if (strcmp(dir, 'down'))
    flag = 2;
end
if (isempty(flag))
    flag = 2;
end
xwidth = (1-2*margin - (nx-1)*separation)/nx;
yheight = (1-2*margin - (ny-1)*separation)/ny;
if (flag == 1)
    xloc0 = zeros(1,nx);
    xloc0(1) = margin;
    for i=2:nx
        xloc0(i) = xloc0(i-1) + xwidth + separation;
    end
    xloc = xloc0;
    if(ny>1)
        for i=2:ny
            xloc = [xloc xloc0];
        end
    end
    
    yloc0 = zeros(1, ny);
    yloc = zeros(1, nx*ny);
    yloc0(1) = 1 - margin - yheight;
    temp = ones(1,nx).*yloc0(1);
    yloc(1:nx) = temp;
    for j=2:ny
        yloc0(j) = yloc0(j-1) - separation - yheight;
        temp = ones(1,nx).*yloc0(j);
        yloc((j-1)*nx+1 : j*nx) = temp;
    end
    width = ones(1, nx*ny).*xwidth;
    height = ones(1, nx*ny).*yheight;
end


if (flag == 2)
    yloc0 = zeros(1,ny);
    yloc0(1) = 1 - margin - yheight;
    for i=2:ny
        yloc0(i) = yloc0(i-1) - yheight - separation;
    end
    yloc = yloc0;
    if(nx>1)
        for i=2:nx
            yloc = [yloc yloc0];
        end
    end
    
    xloc0 = zeros(1, nx);
    xloc = zeros(1, nx*ny);
    xloc0(1) =  margin;
    temp = ones(1,ny).*xloc0(1);
    xloc(1:ny) = temp;
    for j=2:nx
        xloc0(j) = xloc0(j-1) + separation + xwidth;
        temp = ones(1,ny).*xloc0(j);
        xloc((j-1)*ny+1 : j*ny) = temp;
    end
    width = ones(1, nx*ny).*xwidth;
    height = ones(1, nx*ny).*yheight;
end


out = [xloc; yloc; width; height];

function userDir = getuserdir

if ispc
    userDir = winqueryreg('HKEY_CURRENT_USER',...
        ['Software\Microsoft\Windows\CurrentVersion\' ...
         'Explorer\Shell Folders'],'Personal');
else
    userDir = char(java.lang.System.getProperty('user.home'));
end
