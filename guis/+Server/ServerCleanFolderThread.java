import java.nio.file.DirectoryStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryIteratorException;

public class ServerCleanFolderThread extends Thread
{
    String diagpath_;
    String date_;
    String ext_;
    public ServerCleanFolderThread(String diagpath, String date, String ext)
    {
        this.diagpath_ = diagpath;
        this.date_ = date;
        this.ext_ = ext;
    }
    @Override
    public void run(){
        Path dest = Paths.get(diagpath_ + date_);
        // Ensure the 'cleaned-to' path exists
        if (!Files.exists(dest)){
            try {
                Files.createDirectory(dest);
            }
            catch (IOException ex1) {
                System.out.println(ex1.toString());
            }
        }
        // List all files in folder
        
        try ( DirectoryStream<Path> dirStream = Files.newDirectoryStream(Paths.get(diagpath_))){ 
            for (Path path : dirStream) {
                // Get path string to test ending with extension!
                String stri = path.toString();
                // If is file and ends with ext, then move it
                if ((!Files.isDirectory(path)) && (stri.endsWith(ext_))){
                    Files.move(path, dest.resolve(path.getFileName()));
                }
            }
        } 
        catch (IOException ex) {}
    }
}