% Class to handle each plotting graph.

% Call to the method replot will replot new data onto the figure(s)
% associated with this diagnostic. It will also then generate an event to
% notify continious plotting routines that we wish to add data to these.

% These objects are created and updated as a callback routine to the main
% settings uitable.

classdef dataPlot < handle
    properties
        file
        active
        hfig
        hax = []
        plotfcn = [];
        limits = [];
        contplotdata
    end
    
    events
        % This event signals the need to update continious plotting
        UpdatePlots
    end
    
    methods
        function o = dataPlot(diagname, extension, savepath, figureh)
            o.file.path = savepath;
            o.file.date = [];
            o.file.run = [];
            o.file.name = diagname;
            o.file.ext = extension;
            o.hfig = figureh;
            %if nargin==5
             %   o.plotfcn = plotfcn;
            %end
        end
        
        function replot(o, shot)
            if ~o.active; return; end;
            fret = getdata(o, shot);
            if ~ishandle(o.hfig); 
                figure(o.hfig); 
                set(o.hfig, 'WindowStyle', 'docked');
            end;
            %set(o.hfig, 'Visible', 'off'); % Start using this when all
            % debugging is done and it's solid!!!!
            if ~isempty(o.plotfcn) % If a plotfcn exists
                try
                    rets = feval(o.plotfcn, fret.data, o.hfig);
                    set(o.hfig, 'Visible', 'on');
                    if ~isempty(rets)
                        o.contplotdata = rets;
                        notify(o, 'UpdatePlots');
                    end
                    disp('lulu')
                    return;
                catch err 
                    disp(['Diagnostic ' o.file.name ' plot fcn failed.']);
                    disp(err.message);
                    return;
                end
            end
            % But if no plotfcn is set, do stock plotting
            if fret.plot2d
                if isempty(o.hax) 
                    o.hax = axes('Parent', o.hfig);
                    set(o.hax, 'DrawMode', 'fast');
                    imagesc(fret.data, 'Parent', o.hax);
                else
                    him = get(o.hax, 'Children');
                    if isempty(him)
                        imagesc(fret.data, 'Parent', o.hax);
                    else
                        set(him, 'CData', fret.data);
                    end
                end
            else
                if isempty(o.hax)
                    o.hax = axes('Parent', o.hfig);
                    set(o.hax, 'DrawMode', 'fast');
                    line('XData', fret.data(:,1), 'YData', fret.data(:,2), 'Parent', o.hax);
                else
                    him = get(o.hax, 'Children');
                    if isempty(him)
                        line('XData', fret.data(:,1), 'YData', fret.data(:,2), 'Parent', o.hax);
                    else
                        set(him, 'XData', fret.data(:,1), 'YData', fret.data(:,2));
                    end
                end
            end
            if ~isempty(o.limits)
                if fret.plot2d
                    set(o.hax, 'CLim', o.limits);
                else
                    set(o.hax, 'YLim', o.limits);
                end
            end
            title(o.hax,sprintf('%s on shot %i',o.file.name, shot), 'Interpreter', 'none');
            set(o.hfig, 'Visible', 'on');
        end
        
        function dat = getdata(o, shot)
            datafile = sprintf('%s\\%i\\%ir%03i\\%ir%03is%03i_%s.%s', o.file.path,...
                o.file.date, o.file.date, o.file.run, o.file.date, o.file.run, shot, o.file.name, o.file.ext);
            if (~exist(datafile, 'file'))
                return;
            end
            switch (o.file.ext)
                case 'tif'
                    % Fastest way to read tif, apparently
                    %InfoImage = imfinfo(datafile);
                    %mImage = InfoImage(1).Width;
                    %nImage = InfoImage(1).Height;
                    %data = zeros(nImage,mImage,1,'uint16');
                    %TifLink = Tiff(datafile, 'r');
                    %TifLink.setDirectory(1);
                    %data = TifLink.read();
                    %TifLink.close();
                    data = imread(datafile);
                    plot2d = true;
                case 'CR2'
                    data = imread(datafile);
                    plot2d = true;
                case 'raw'
                    siz = importdata([o.file.name '.txt']);
                    data = ReadRAW16bit(datafile, siz(1), siz(2));
                    plot2d = true;
                case 'txt'
                    dataf = importdata(datafile);
                    if (isstruct(dataf))
                        data = dataf.data;
                    else
                        data = dataf;
                    end
                    plot2d = false;
                case 'sif'
                    data = sifread(datafile);
                    plot2d = true;
                case 'fit'
                    data = fitsread(datafile);
                    plot2d = true;
                case 'mat'
                    % figure something out! :)
                    data = load(filename);
            end
            dat.data = data;
            dat.plot2d = plot2d;
        end
        
        function delete(o)
            if ishandle(o.hfig)
                delete(o.hfig);
            end
        end
    end
end