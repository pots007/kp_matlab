% Server 2.0
% Fully object oriented, using a UDP object to listen to pings and using
% listeners to notify each diagnostic object that it should copy. Plotting
% is a separate object now that is only run after copying is done so should
% not block new file copying. Plotting is called by an object monitoring
% the status of each diagnostic object.

% Not tested/written: Contplot up to shot; clean folder; udpsender enabling
% 

classdef main < uiextrasX.VBox
    
    properties
        sett
        udppoll
        runn
        stealth
        grabbing
        plotting
        statbar
        numdiag = 25
        plotobjs
        contplotobjs
        listeners
    end
    
    events
        runplot
        doplot
    end
    
    methods
        function o = main
            Parent = figure(666);
            set(Parent, 'Resize', 'off', 'Toolbar', 'none', 'MenuBar', 'none', 'Color', 'w',...
                'Position', [100 100 1000 600], 'Name', 'ICL Server', 'NumberTitle', 'off');
            %, 'CloseRequestFcn',@(s,e) disp('don''t close me!'));
            o@uiextrasX.VBox('Parent', Parent);
            defer(o);
            hbox = uiextrasX.HBox('Parent', o);
            o.runn.spath = uicontrol('Style', 'edit', 'Parent', hbox, 'String', 'E:\ttemp\GeminiNajmudin2015');
            uicontrol('Style', 'PushButton', 'Parent', hbox, 'String', 'Set diagnostic path',...
                'Callback', @(s,e)set_diag_path(o,1));
            uicontrol('Style', 'Pushbutton', 'String', 'Add cont plot', 'Parent', hbox,...
                'Callback', @(s,e)add_cont_plot(o,1));
            % the really important bit - sounds and the stealth button!
            o.stealth.enable = uicontrol('Style', 'checkbox', 'Parent', hbox, 'String', 'Stealth mode');
            hbox.Sizes = [-1 150 150 100];
            for i=1:7
                [Y, Fs] = audioread(sprintf('%s\\sounds\\speech%i.wav', ...
                    fileparts(which('Server.main')), i));
                o.stealth.audio(i) = audioplayer(Y,Fs);
            end
            % bit with useful button and run and shot number etc
            hbox1 = uiextrasX.HBox('Parent', o);
            vboxL = uiextrasX.VBox('Parent', hbox1);
            uiextrasX.HBox('Parent', vboxL);
            hbox = uiextrasX.HBox('Parent', vboxL);
            uicontrol('Style', 'Pushbutton', 'String', 'Date', 'Parent', hbox, 'Callback', @(s,e)set_date(o,1));
            cc = clock;
            today = sprintf('%i%02i%02i', cc(1:3));
            o.runn.date = uicontrol('Style', 'text', 'String', today, 'FontSize', 11, 'Parent', hbox);
            hbox = uiextrasX.HBox('Parent', vboxL);
            uicontrol('Style', 'Pushbutton', 'String', 'Run', 'Parent', hbox, 'Callback', @(s,e)set_run(o,1));
            o.runn.run = uicontrol('Style', 'text', 'String', '1', 'FontSize', 11, 'Parent', hbox);
            % Next shot in big font
            hbox = uiextrasX.HBox('Parent', vboxL);
            uicontrol('Style', 'text', 'String', 'Next shot', 'Parent', hbox, 'FontSize', 17);
            hbox = uiextrasX.HBox('Parent', vboxL);
            o.runn.contplotdata = 1;
            o.runn.nextshot = uicontrol('Style', 'edit', 'String', '1', 'Parent', hbox, 'FontSize', 36);
            % Initial timeout - before we start looking for new files
            hbox = uiextrasX.HBox('Parent', vboxL);
            uicontrol('Style', 'text', 'String', 'Start timeout:', 'Parent', hbox);
            o.grabbing.inTO = uicontrol('Style', 'edit', 'String', '1', 'Parent', hbox);
            % Final timeout - give up after this much time
            hbox = uiextrasX.HBox('Parent', vboxL);
            uicontrol('Style', 'text', 'String', 'Final timeout', 'Parent', hbox);
            o.grabbing.finalTO = uicontrol('Style', 'edit', 'String', '10', 'Parent', hbox);
            % Some space....
            uiextrasX.HBox('Parent', vboxL);
            % Start grab button - actually UDPpoll object
            hbox = uiextrasX.HBox('Parent', vboxL);
            o.udppoll = Server.UDPpoll(hbox);
            o.udppoll.Sizes = [60 25];
            o.listeners = {};
            o.listeners{end+1} = addlistener(o.udppoll, 'shot', @(s,e)grab_data(o,1));
            % Grab one shot button
            hbox = uiextrasX.HBox('Parent', vboxL);
            o.grabbing.grabshot = uicontrol('Style', 'PushButton', 'String', 'GRAB SHOT',...
                'FontSize', 13,'Parent', hbox, 'Callback', @(s,e)grab_data(o,1));
            % Replot last shot button
            hbox = uiextrasX.HBox('Parent', vboxL);
            uicontrol('Style', 'PushButton', 'String', 'REPLOT', 'FontSize', 13,...
                'Parent', hbox, 'Callback', @(s,e)plot_(o,1));
            % Clean folder button - the function is multithreading java so...
            hbox = uiextrasX.HBox('Parent', vboxL);
            uicontrol('Style', 'PushButton', 'String', 'CLEAN FOLDERS', 'FontSize', 13,...
                'Parent', hbox, 'Callback', @(s,e)clean_folders(o,1));
            % Plot particular shot button
            hbox = uiextrasX.HBox('Parent', vboxL);
            uicontrol('Style', 'PushButton', 'String', 'Plot any shot', 'FontSize', 13', ...
                'Parent', hbox, 'Callback', @(s,e)plot_any_shot(o,1));
            % and now make sizes look nice
            vboxL.Sizes = [10 25 25 50 50 25 25 15 85 -1 -1 -1 -1];
            uiextrasX.VBox('Parent', hbox1);
            % The settings and diagnostics table
            vboxR = uiextrasX.VBox('Parent', hbox1);
            uiextrasX.HBox('Parent', vboxR);
            hbox = uiextrasX.HBox('Parent', vboxR);
            % Set up plotting objects
            for i=1:o.numdiag
                ttemp = feval('Server.dataPlot', 'lala', 'tif',...
                    get(o.runn.spath, 'String'), 20+i);
                o.plotobjs.o(i) = ttemp;
                %addlistener(o, 'runplot', @(s,e) ttemp.replot_(ttemp,2));
            end
            make_table(o, hbox);
            
            % The panel to host continious plotting objects
            hbox_ = uiextrasX.HBox('Parent', vboxR);
            hpan = uipanel('Parent', hbox_, 'Title', 'Continiuos plotting');
            % Prevent an annoying warning msg
            warning('off', 'MATLAB:uitabgroup:OldVersion');
            o.contplotobjs.par = uitabgroup('Parent', hpan);
            o.contplotobjs.obj = struct('tab',{},'obj',{});%.tab = uitab(o.contplotobjs.par, 'Title', 'Add some from above!');
            
            % The last bit - info on last plotted shot and next last buttons
            hbox_ = uiextrasX.HBox('Parent', vboxR);
            hbox = uiextrasX.HBox('Parent', hbox_);
            uiextrasX.HBox('Parent', hbox);
            uicontrol('Style', 'PushButton', 'String', '<', 'Parent', hbox,...
                'FontSize', 14, 'Callback', @(s,e)plot_next(o,-1))
            o.plotting.lastplot = uicontrol('Style', 'text', 'String', 'Last plotted shot:',...
                'FontSize', 12, 'Parent', hbox);
            uicontrol('Style', 'PushButton', 'String', '>', 'Parent', hbox,...
                'FontSize', 14, 'Callback', @(s,e)plot_next(o,1))
            uiextrasX.HBox('Parent', hbox);
            hbox.Sizes = [15 90 -1 90 15];
            % padding around settings table
            vboxR.Sizes = [15 -1 75 30];
            % Ratio of main columns:
            hbox1.Sizes = [200 10 -1];
            
            uiextrasX.HBox('Parent', o);
            o.Sizes = [35 -1 30];
            % Make the File Save, Load and Other settings menu
            fmenu = uimenu(666, 'Label', 'Settings');
            uimenu(fmenu, 'Label', 'Save', 'Callback', @(s,e)save_settings(o,1), 'Accelerator', 'S');
            uimenu(fmenu, 'Label', 'Load', 'Callback',...
                @(s,e)load_settings(o,[fileparts(which('Server.main')) '\diagnostics.txt']),...
                'Accelerator', 'L');
            uimenu(fmenu, 'Label', 'Others...', 'Callback', @(s,e)other_settings(o,1));
            
            o.statbar = statusbar(666, 'Checking for UDP...');
            o.udppoll.setStatBar(o.statbar);
            resume(o);
            % And finally, chack if UDP is there...
            stat = o.udppoll.checkLastGSN(1);
            if stat
                set(o.statbar, 'Text', 'Ready for shots; UDP stream sound.')
            else
                set(o.statbar, 'Text', 'No UDP detected!');
            end
            % And finally finally, add the folder to Java path
            javaaddpath(fileparts(which('Server.main')));
            % And listener for plotting
            o.listeners{end+1} = addlistener(o, 'doplot', @(s,e)plot_(o,1));
        end
        
        function grab_data(o, ~)
            % Keep track of how long everything is taking us
            tottime = tic;
            if ~get(o.stealth.enable, 'Value'); o.makesound; end;
            % Filename: YYYYMMMDDr00ns00n_diagname
            filepref = sprintf('%sr%03is%03i_', get(o.runn.date, 'String'), ...
                str2double(get(o.runn.run, 'String')), ...
                str2double(get(o.runn.nextshot, 'String')));
            % Savepath: spath\YYYYMMDD\YYYYMMDDr00n\
            savepath = sprintf('%s\\%s\\%sr%03i\\', get(o.runn.spath, 'String'),...
                get(o.runn.date, 'String'), get(o.runn.date,'String'),...
                str2double(get(o.runn.run,'String')));
            % Ensure there's somewhere to save the files!
            if (~isdir(savepath))
                mkdir(savepath);
            end
            
            tabl = get(o.sett, 'Data');
            samefileerror = false(1, size(tabl,1));
            nofileerror = false(1, size(tabl,1));
            grabs = tabl(:,2);
            filedone = false(1,size(tabl,1));
            initialpause = str2double(get(o.grabbing.inTO, 'String'));
            timeoutval = str2double(get(o.grabbing.finalTO, 'String'));
            % Start the progress bar
            set(o.statbar, 'text', 'Starting grab');
            set(o.statbar.ProgressBar, 'Minimum',1, 'Maximum',sum(cell2mat(grabs)), 'Value',1);
            o.statbar.ProgressBar.setVisible(true);
            %disp('here') % Debugging
            % Wait until initial timeout time has passed!
            while toc(tottime)<initialpause
                pause(0.05);
            end
            
            % Iterate over all diagnostics until copying thread has been
            % started for all of them - but not longer than FinalTimeOut
            % after which we give up on the not-found files.
            while true
                % Check whether it's time to give up
                 %toc(tottime); %Debugging!!!!!
                %if (toc(tottime)<timeoutval)
                 %   forcecopy = false; % We still have a chance to try again...
                %else
                    % Time is up - if no files are there raise nofileerror, 
                    % or if the file is same, raise samefileerror
                    %forcecopy = true;
                %end;
                forcecopy = toc(tottime)>timeoutval; % Oneliner of the above :)
                % Check whether we've done all files already
                if sum(filedone==true) == sum(cell2mat(grabs)); break; end;
                % Loop over diagnostics
                for i=1:o.numdiag
                    diagtime = tic;
                    if (~grabs{i}); continue; end;
                    if filedone(i); continue; end;
                    diagpath = tabl{i,3};
                    diagext = tabl{i,4};
                    lastdiagtime = tabl{i,6};
                    diagsavename = [savepath filepref tabl{i,1} '.' diagext];
                    %All files of diagnostic type in spath
                    filen = dir([diagpath '\*.' diagext]);
                    if (isempty(filen))
                        % If time is up, we didn't find it!
                        if forcecopy
                            nofileerror(i) = true;
                        end
                        pause(0.025); %CPU relief pause 
                        continue;
                    end
                    % Find last modified file
                    filenum = cell2mat({filen.datenum});
                    [~, filei] = max(filenum);
                    if (lastdiagtime ~= -1)
                        if (lastdiagtime == filen(filei).datenum)
                            % The timestamp is the same - but we only raise
                            % the error if time is up.
                            % Also check for file size - maybe it's been
                            % increased so it'd then have changed and
                            % contains new data!
                            if forcecopy
                                disp('shiiiiiiiiit');
                                samefileerror(i) = true;
                                start(ServerCopyFileThread([diagpath '\' filen(filei).name], diagsavename));
                                %copyfile([diagpath '\' filen(filei).name], diagsavename);
                                filedone(i) = true;
                            end
                        else
                            % Since we are doing this now, without waiting too long, ensure file size
                            % doesn't change and we have read access
                            %disp('I''m here');
                            [~, fattr] = fileattrib([diagpath '\' filen(filei).name]);
                            if ~fattr.UserRead
                                % We don't have read access - try again later!
                                continue;
                            end
                            ff1 = dir([diagpath '\' filen(filei).name]);
                            pause(0.075);
                            ff2 = dir([diagpath '\' filen(filei).name]);
                            if ff1.bytes ~= ff2.bytes
                                % File is still increasing - try again later!
                                continue;
                            end
                            % But it everything is good, launch copying
                            start(ServerCopyFileThread([diagpath '\' filen(filei).name], diagsavename));
                            %copyfile([diagpath '\' filen(filei).name], diagsavename);
                            filedone(i) = true;
                            tabl{i,6} = filen(filei).datenum;
                        end
                    else % No file copied yet - last file is game
                        % Tryin to be fancy and use multithreading here!
                        start(ServerCopyFileThread([diagpath '\' filen(filei).name], diagsavename));
                        %copyfile([diagpath '\' filen(filei).name], diagsavename);
                        filedone(i) = true;
                        tabl{i,6} = filen(filei).datenum;
                    end
                    tabl{i,8} = toc(diagtime);
                end % End of diagnostics loop
                set(o.statbar.ProgressBar, 'Value', sum(filedone));
            end
            % Now parse all errors that occurred
            if (sum(samefileerror) ~= 0 || sum(nofileerror) ~= 0)
                samefile = tabl(samefileerror, 1)';
                nofile = tabl(nofileerror, 1)';
                warndlg(['Same file for: ' samefile ' No file for: ' nofile],...
                    'Warning!','replace');
                % And play error sound!
                play(o.stealth.audio(7));
            end
            set(o.sett, 'Data', tabl);
            o.plotting.date = str2double(get(o.runn.date, 'String'));
            o.plotting.run = str2double(get(o.runn.run, 'String'));
            nextshot = str2double(get(o.runn.nextshot, 'String'));
            o.plotting.nextshot = nextshot;
            o.runn.contplotdata = nextshot;
            set(o.runn.nextshot, 'String', num2str(nextshot+1));
            % Finally, ensure all copying threads have finished - poll for .temp files
            for i=1:50
                flist = dir([savepath filepref '*.temp']);
                if isempty(flist); break; end; % If not using java at all...
                % If we have all the files we wanted to copy
                if length(flist) == (sum(cell2mat(grabs))-sum(nofileerror))
                    break;
                else
                    pause(0.1);
                end
            end
            if (i==50); disp('Some files didn''t finish copying! :('); end;
            %delete([savepath filepref '*.temp']); %Make into java soon - this leaks memory!!!
            start(ServerDeletetemps(savepath)); % Runs in another thread - no pileup.
            o.statbar.ProgressBar.setVisible(false);
            set(o.statbar, 'Text', 'Copying done!');
            o.plotting.date = str2double(get(o.runn.date,'String')); 
            o.plotting.run = str2double(get(o.runn.run,'String'));
            o.plotting.shot = str2double(get(o.runn.nextshot,'String'))-1;
            notify(o, 'doplot');
        end
        
        function plot_(o,~)
            %disp('this is doplot'); 
            for i=1:o.numdiag
                if (~o.plotobjs(1).o(i).active); continue; end;
                o.plotobjs(1).o(i).file.date = o.plotting.date;
                o.plotobjs(1).o(i).file.run = o.plotting.run;
                o.plotobjs(1).o(i).replot(o.plotting.shot);
                %disp('lala4')
            end
            % Update the last plotted text
            set(o.plotting.lastplot, 'String', ...
                sprintf('Last plotted shot: %i R %03i S %03i', ...
                o.plotting.date, o.plotting.run, o.plotting.shot));
        end
        
        function sett_edit(o,varargin)
            inds = varargin{2};
            i = inds.Indices(1);
            switch inds.Indices(2)
                case 1
                    o.plotobjs(1).o(i).file.name = inds.NewData;
                case 2
                    o.plotobjs(1).o(i).active = inds.NewData;
                case 4
                    o.plotobjs(1).o(i).file.ext = inds.NewData;
                case 5
                    o.plotobjs(1).o(i).limits = sscanf(inds.NewData, '%i')';
                case 7
                    if strcmp(inds.NewData, ' ')
                        o.plotobjs(1).o(i).plotfcn = [];
                    else
                        o.plotobjs(1).o(i).plotfcn = inds.NewData;
                    end
            end
            disp('This is cell-edit');
        end
        
        function set_diag_path(o,~)
            tabl = get(o.sett, 'Data');
            diaglist = tabl(:,1);
            [ind,sel] = listdlg('PromptString','Select diagnostic:',...
                'SelectionMode','single',...
                'ListString',diaglist);
            if (~sel)
                msgbox('No diagnostic selected.', 'Error.');
                return;
            end
            diagnam = diaglist{ind};
            savpath = uigetdir('C:\',['Path for ' diagnam]);
            tabl{ind,3} = [savpath '\'];
            set(o.sett, 'Data', tabl);
        end
        
        function make_table(o, par)
            columnname =   {'Diagnostic', 'Grab', 'Path', 'Filetype', ...
                'CLim', 'Diag time', 'PlotFunction', 'LastFileTime'};
            columneditable =  [true true true true true false true false];
            columnformat = {'char', 'logical', 'char',...
                {'tif', 'raw', 'txt', 'sif', 'fit', 'CR2'},...
                'char', 'numeric', 'char', 'numeric'};
            dat = cell(o.numdiag, length(columnname));
            diagnams = repmat({' '}, 1, o.numdiag);
            diagnams{1} = 'Top_view';
            diagnams{2} = 'Shadowgraphy';
            diagnams{3} = 'Sidespec';
            widths = {100 40 220 50 100 80 90 90};
            grabs = repmat({true}, 1, o.numdiag);
            paths = repmat({'D:\'}, 1, o.numdiag);
            filetypes = repmat({'tif'}, 1, o.numdiag);
            clims = repmat({'-1'}, 1, o.numdiag);
            times = zeros(1, o.numdiag);
            oldfil = -ones(1, o.numdiag);
            dat(:,1) = diagnams;
            dat(:,2) = grabs;
            dat(:,3) = paths;
            dat(:,4) = filetypes;
            dat(:,5) = clims;
            dat(:,6) = num2cell(oldfil);
            dat(:,7) = repmat({' '}, 1, o.numdiag);
            dat(:,8) = num2cell(times);
            o.sett = uitable('Parent', par,'Data', dat, 'ColumnName', columnname,...
                'ColumnFormat', columnformat, 'ColumnEditable', columneditable,...
                'ColumnWidth', widths, 'RowName',[],...
                'CellEditCallback', @o.sett_edit);
            % But also add plotting objects to all diagnostics
            for i=1:o.numdiag
                o.plotobjs.o(i).file.name = diagnams{i};
                o.plotobjs.o(i).file.ext = filetypes{i};
            end
        end
        
        function save_settings(o,~)
            data = get(o.sett, 'Data');
            fid = fopen([fileparts(which('Server.main')) '\diagnostics.txt'], 'W');
            for i=1:size(data,1)
                fprintf(fid, '%s,%i,%s,%s,%s,%f,%s\n', data{i,1:7});
            end
            fclose(fid);
            set(o.statbar, 'Text', 'Saved setting to default file.');
        end
        
        function load_settings(o,filename)
            disp('Loading default settings');
            data = csvimport(filename, 'outputAsChar', true);
            if (size(data,2)<7)
                errordlg('File insufficent', 'Error loading settings');
                return;
            end
            for i=1:size(data,1)
                try
                    temp = str2double(data{i,2});
                    temp2 = str2double(data{i,6});
                catch
                    temp = data{i,2};
                    temp2 = data{i,6};
                end
                try
                    temp3 = num2str(data{i,5});
                catch
                    temp3 = data{i,5};
                end
                data{i,2} = logical(temp);
                data{i,6} = temp2;
                data{i,5} = temp3;
            end
            %assignin('base', 'data', data); % Debugging!
            set(o.sett, 'Data', data);
            set(o.statbar, 'Text', 'Successfully loaded default settings!');
            for i=1:o.numdiag
                o.plotobjs.o(i).file.name = data{i,1};
                o.plotobjs.o(i).active = data{i,2};
                o.plotobjs.o(i).file.ext = data{i,4};
                if ~strcmp(data{i,7}, ' '); o.plotobjs.o(i).plotfcn = data{i,7}; end;
            end
        end
        
        function other_settings(o,~)
            [filen, filpath, ~] = uigetfile('*.txt',...
                'Select file with settings:');
            if filen == 0; return;  end
            load_settings(o, [filpath filen]);
        end
        
        function plot_any_shot(o,~)
            prompt = {'Enter date number:', 'Enter run number:',...
                'Enter shot number:'};
            def = {get(o.runn.date, 'String'), '1', '1'};
            options.WindowStyle = 'normal';
            options.Resize = 'on';
            answer = inputdlg(prompt,'Enter shot to plot',1,def, options);
            if (isempty(answer))
                return;
            end
            o.plotting.date = str2double(answer{1});
            o.plotting.run = str2double(answer{2});
            o.plotting.shot =  str2double(answer{3});
            notify(o, 'doplot');
        end
        
        function plot_next(o,v)
            if ~isfield(o.plotting, 'shot')
                set(o.statbar, 'Text', 'Plot any shot first!');
                return;
            end
            if o.plotting.shot == 1
                return;
            end
            o.plotting.shot = o.plotting.shot + v;
            notify(o, 'doplot');
        end
        
        function set_date(o,~)
            tim = clock;
            datest = sprintf('%i%02i%02i', tim(1:3));
            def = cellstr(datest);
            answer = inputdlg(['Enter date: P.S. Today would be ' datest],'Set run date:',1,def);
            set(o.runn.date, 'String', answer{1});
        end
        
        function set_run(o,~)
            def = {'1'};
            options.WindowStyle = 'normal';
            options.Resize = 'on';
            answer = inputdlg('Enter run number:','Set run number',1,def, options);
            if (isempty(answer))
                answer = def;
            end
            set(o.runn.run, 'String', answer{1});
        end
        
        function add_cont_plot(o,~)
            % This should add a continiuos plotting listener and plot
            % object
            o.contplotobjs.obj(end+1).tab = uitab(o.contplotobjs.par, 'Title', 'contPlot');
            hbox = uiextrasX.HBox('Parent', o.contplotobjs.obj(end).tab);
            o.contplotobjs.obj(end).obj = feval('Server.contDataPlot',hbox, o, 100+length(o.contplotobjs.obj));
            uicontrol('Style', 'pushbutton', 'String', 'Delete', 'Parent', hbox, ...
                'Callback',@(s,e)delete(o.contplotobjs.obj(length(o.contplotobjs.obj)).tab));
            hbox.Sizes = [-1 90];
        end
        
        function makesound(o)
            ind = randi(6);
            play(o.stealth.audio(ind));
        end
        
        function clean_folder(o,~)
            % This will be done in a bit - the previous hardcore approach
            % seemed to not work on some machines where it took a VERY
            % long time... So will try to write a Java class to do this,
            % and run a new thread for each diagnostic to be cleaned such
            % that everything is done in Java.
            return;
        end
    end
end