% This class serves to notify other listening devices of the server having
% received  shot.

% (c) Kristjan Poder, ICL, 2015

classdef UDPsend < handle
    properties
        remoteIP
        remoteport
        udp_obj
    end
    methods 
        function obj = UDPsend(IP,port)
            obj.remoteIP = IP;
            obj.remoteport = port;
            obj.udp_obj = dsp.UDPSender('RemoteIPAddress', obj.remoteIP,...
                'RemoteIPPort', obj.remoteport); 
        end
        
        function send_(obj,data)
            datasend = double(sprintf('%i',data));
            datasend = data;
            step(obj.udp_obj, uint8(datasend));
        end
    end
end