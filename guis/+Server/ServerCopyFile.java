import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/** JDK 7+. */
public final class ServerCopyFile {
  
  public static void main(String FROMp, String TOp) throws IOException {
    Path FROM = Paths.get(FROMp);
    Path TO = Paths.get(TOp);
    //overwrite existing file, if exists
    CopyOption[] options = new CopyOption[]{
      StandardCopyOption.REPLACE_EXISTING,
      StandardCopyOption.COPY_ATTRIBUTES
    }; 
    Files.copy(FROM, TO, options);
  }
  
  public static void mainNA(String FROMp, String TOp) throws IOException {
    Path FROM = Paths.get(FROMp);
    Path TO = Paths.get(TOp);
    //overwrite existing file, if exists
    CopyOption[] options = new CopyOption[]{
      StandardCopyOption.REPLACE_EXISTING,
    }; 
    Files.copy(FROM, TO, options);
  }
  
}