% Class to handle continious plotting 

% Upon a call to replot, queries the main server object for data to be
% plotted.
% Has a plotall method, which iterates through all shots in run and calls
% plotting routine for only those diagnostics it is plotting.
% 

classdef contDataPlot < uiextrasX.HBox
   
    properties
        xaxobj
        yaxobj
        shotx = false
        title
        enable
        doall
        hmain
        setup
        hfig
        hax = []
        listeners = {}
        lll = 0
    end
    
    methods
        function obj = contDataPlot(parent, hmain, hfig)
            obj@uiextrasX.HBox('Parent', parent);
            obj.hmain = hmain; % This is Server.main
            obj.enable = uicontrol('style', 'checkbox', 'Parent', obj, ...
                'String', 'Enable plotting', 'Value', 1);
            uicontrol('Style', 'text', 'String', 'x-axis:', 'Parent', obj);
            obj.xaxobj.box = uicontrol('Style', 'text', 'String', ' ', 'Parent', obj);
            uicontrol('Style', 'text', 'String', 'y-axis:', 'Parent', obj);
            obj.yaxobj.box = uicontrol('Style', 'text', 'String', ' ', 'Parent', obj);
            obj.doall = uicontrol('Style', 'pushbutton', 'String', 'Plot all in run',...
                'Parent', obj, 'Callback', @(s,e)contPlotAll(obj,1));
            obj.hfig = hfig;
            obj.setup = obj.makeSetupWizard;
        end
        
        function setup = makeSetupWizard(obj)
            figure(667);
            set(667, 'Visible', 'off', 'Name', 'Setup continious plotting', ...
                'Toolbar', 'none', 'Menubar', 'none');
            vbox = uiextrasX.VBox('Parent', 667);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Select x-axis:', 'Parent', hbox);
            dat = get(obj.hmain.sett, 'Data');
            diagnames = dat(:,1);
            setup.xaxs = uicontrol('Style', 'popupmenu', 'String', [diagnames; 'Shotnumber'],...
                'Parent', hbox);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Select y-axis:', 'Parent', hbox);
            setup.yaxs = uicontrol('Style', 'popupmenu', 'String', diagnames,...
                'Parent', hbox);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Plot:', 'Parent', hbox);
            setup.y(1) = uicontrol('Style', 'checkbox', 'String', 'Output 1', 'Value', 1, 'Parent', hbox);
            setup.y(2) = uicontrol('Style', 'checkbox', 'String', 'Output 2', 'Parent', hbox);
            setup.y(3) = uicontrol('Style', 'checkbox', 'String', 'Output 3', 'Parent', hbox);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('style', 'pushbutton', 'String', 'Done!', 'Parent', hbox, ...
                'Callback', @(s,e)completeWizard(obj));
            set(667, 'Visible', 'on');
        end
        
        function completeWizard(obj)
            totdiags = length(get(obj.setup.yaxs, 'String'));
            ind_y = get(obj.setup.yaxs, 'Value');
            obj.yaxobj.obj = obj.hmain.plotobjs.o(ind_y);
            ydiags = get(obj.setup.yaxs, 'String'); ydiag = ydiags{ind_y};
            % And which ones we plot as well!
            obj.yaxobj.logm = false(3,1);
            for i=1:3
                obj.yaxobj.logm(i) = get(obj.setup.y(i), 'Value');
            end
            set(obj.yaxobj.box, 'String', sprintf('%s [%i %i %i]',ydiag,obj.yaxobj.logm));
            ind_x = get(obj.setup.xaxs, 'Value'); 
            if ind_x>totdiags % This is if xaxis is shot number
                obj.xaxobj.obj = obj.hmain.runn;
                obj.shotx = true;
                obj.listeners{end+1} = addlistener(obj.hmain.plotobjs.o(ind_y),...
                    'UpdatePlots', @(s,e)listen(obj,0));
            else
                obj.xaxobj.obj = obj.hmain.plotobjs.o(ind_x);
                obj.listeners{end+1} = addlistener(obj.hmain.plotobjs.o(ind_y),...
                    'UpdatePlots', @(s,e)listen(obj,1));
                obj.listeners{end+1} = addlistener(obj.hmain.plotobjs.o(ind_x),...
                    'UpdatePlots', @(s,e)listen(obj,2));
            end
            xdiags = get(obj.setup.xaxs, 'String'); xdiag = xdiags{ind_x};
            set(obj.xaxobj.box, 'String', xdiag);
            obj.title = [ydiag ' vs ' xdiag];
            if strcmp(ydiag, xdiag)
                warndlg('Plotting the same thing on both axes! Please revise.',...
                    'Selection error');
                return;
            end
            % Try to set tab title as well
            hpar = get(obj, 'Parent');
            htab = get(hpar, 'Parent');
            set(htab, 'Title', sprintf('%s vs %s', ydiag, xdiag));
            delete(667);
        end
        
        function listen(obj, v)
            if ~get(obj.enable, 'Value'); return; end;
            % If v==0, only need to hear one notification!
            if v==0; obj.replot; end;
            if obj.lll==0 && v==1; obj.lll = -2; end; % If we hear y ping first
            if obj.lll==0 && v==2; obj.lll = -1; end; % If we hear x ping first
            % With the above, we never plot if only one of the axis is updated
            if obj.lll+v == 0; 
                obj.lll = 0;
                obj.replot; 
            end;
        end
        
        function replot(obj,~)
            % Make sure the figure window exists
            if ~ishandle(obj.hfig); figure(obj.hfig); end;
            % Hide it for plot update
            set(obj.hfig, 'Visible', 'off');
            % This doesn't work for some reason for shot on x axis
            if obj.shotx
                get(obj.hmain.runn.nextshot, 'String')
                newxdata = str2double(get(obj.hmain.runn.nextshot, 'String'));
            else
                newxdata = obj.xaxobj.obj.contplotdata; 
                % Use the first output for x-plotting!
                if length(newxdata)~=1; newxdata = newxdata(1); end;
            end
            newydata = obj.yaxobj.obj.contplotdata;
            if isempty(obj.hax)
                obj.hax = axes('Parent', obj.hfig);
                set(obj.hax, 'DrawMode', 'fast', 'NextPlot', 'add');
                for i=1:length(newydata)
                    plot(obj.hax, newxdata, newydata(i), 'o');
                end
            else
                him = get(obj.hax, 'Children');
                for i=1:length(him)
                    set(him(i), 'XData', [get(him(i), 'XData') newxdata],...
                        'YData', [get(him(i), 'YData') newydata]);
                end
            end
            
            set(obj.hfig, 'Visible', 'on');
        end
        
        function contPlotAll(obj,~)
            % Should plot all things up to current shot.
        end
    end
    
end