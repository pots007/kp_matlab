import java.io.IOException;
import java.io.File;

/** JDK 7+. */
public class ServerDeletetemps extends Thread
{
    String fold;
    public ServerDeletetemps(String path)
    {
        this.fold = path;
    }
    @Override
    public void run()
    {
        String target_file;
        File folderToScan = new File(fold);
        File[] listOfFiles = folderToScan.listFiles();
        // Slow and horrible, but brute force ought to work
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                target_file = listOfFiles[i].getName();
                if (target_file.endsWith(".temp")) {
                    listOfFiles[i].delete();
                }
            }
        }
    }
}
