import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
public class ServerCopyFileThread extends Thread
{
    String diagfile_;
    String savepath_;
    public ServerCopyFileThread(String diagpath, String savepath)
    {
        this.diagfile_ = diagpath;
        this.savepath_ = savepath;
    }
    @Override
    public void run()
    {
        try
        {
            Path FROM = Paths.get(diagfile_);
            Path TO = Paths.get(savepath_);
            //overwrite existing file, if exists
            CopyOption[] options = new CopyOption[]{
                StandardCopyOption.REPLACE_EXISTING,
                StandardCopyOption.COPY_ATTRIBUTES
            }; 
            Files.copy(FROM, TO, options);
            // And now write placeholder to tell Matlab we're done
            String filename = savepath_.substring(0, savepath_.lastIndexOf('.'));
            DataOutputStream out = new DataOutputStream(
                                     new FileOutputStream(filename + ".temp"));
            out.writeDouble(1.);
            out.close();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
}