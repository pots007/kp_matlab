% UDP poll object

% Notifies the main program when a new GSN is received.

% Based on (tested) core of udp_receiver.m from previous Server.

% K Poder, ICL, 2015

classdef UDPpoll < uiextrasX.VBox
    
    properties
        startbutton
        lastGSN
        lastshot = []
        hudpr
        statbar = {}
    end
    
    events
        shot
    end
    
    methods
        function o = UDPpoll(parent, varargin)
            o@uiextrasX.VBox('Parent', parent);
            o.startbutton = uicontrol('Parent', o, 'Style', 'togglebutton', ...
                'String', 'START GRAB', 'FontSize', 18,...
                'Callback', @(s,e)poll_start(o,get(o.startbutton, 'Value')));
            o.lastGSN = uicontrol('Parent', o, 'Style', 'text', ...
                'String', 'Last GSN 000000');
            o.hudpr = dsp.UDPReceiver('RemoteIPAddress', '0.0.0.0', ...
                'LocalIPPort', 6770, 'MessageDataType', 'uint8');
            if nargin == 2; o.statbar = varargin{1}; end;
        end
        
        function poll_start(o,v)
            if v
                set(o.startbutton, 'String', 'STOP GRAB', 'BackgroundColor', [0. 1. 0.]);
                poll_(o);
            else
                set(o.startbutton, 'String', 'START GRAB', 'BackgroundColor', [0.941 0.941 0.941]);
            end
        end
        
        function poll_(o)
            o.lastshot = [];
            newshot = [];
            %Added 26/09 to fix 'shots taken when server not active' bug
            k = 1;
            %while(isempty(lastshot))
            streamempty = false;
            while(k<20 && ~streamempty) % && isempty(lastshot))
                dataReceived = step(o.hudpr);
                k = k + 1;
                if (~isempty(dataReceived))
                    receivedstring = cast(dataReceived', 'char');
                    if (strcmp(receivedstring, ' GSN:TEST') == 1)
                        continue;
                    end
                    o.lastshot = str2double(receivedstring(6:11));
                    if (newshot == o.lastshot)
                        streamempty = true;
                    end
                    newshot = o.lastshot;
                end
                pause(0.1);
            end
            set(o.lastGSN, 'String', sprintf('Last GSN %i', o.lastshot));
            while get(o.startbutton, 'Value')
                %disp('Waiting...')
                dataReceived = step(o.hudpr);
                if (~isempty(dataReceived))
                    %lastshot = importdata('lastshot.txt');
                    receivedstring = cast(dataReceived', 'char');
                    if (strcmp(receivedstring, ' GSN:TEST') || strcmp(receivedstring, ' BANG'))
                        continue;
                    end
                    receivedshot = str2double(receivedstring(end-5:end));
                    if (receivedshot == o.lastshot)
                        continue;
                    end
                    newshot = receivedshot;
                    disp(receivedstring);
                    o.lastshot = newshot;
                    set(o.lastGSN, 'String', sprintf('Last GSN %i', o.lastshot));
                    notify(o, 'shot');%Server('takegrab', h, h2);
                    % disp('shot!') % Debugging!
                end
                pause(0.25);
            end
        end
    
        function stat = checkLastGSN(o, ~)
            nit = 60;
            if ~isempty(o.statbar)
                set(o.statbar.ProgressBar,...
                    'Minimum', 1, 'Maximum', nit, 'Value', 1);
                o.statbar.ProgressBar.setVisible(true);
            end
            receivedshot = -1;
            for i=1:nit
                if ~isempty(o.statbar); set(o.statbar.ProgressBar, 'Value', i); end;
                dataReceived = step(o.hudpr);
                if (~isempty(dataReceived))
                    receivedstring = cast(dataReceived', 'char');
                    if (strcmp(receivedstring, ' GSN:TEST')==1)
                        break;
                    end
                    receivedshot = str2double(receivedstring(6:11));
                    break;
                end
                pause(0.1);
            end
            if (receivedshot == -1)
                stat = 0;
            else
                set(o.lastGSN, 'String', sprintf('Last GSN %i',receivedshot));
                stat = 1;
            end
            o.statbar.ProgressBar.setVisible(false);
        end
        
        function setStatBar(o, statbar)
            o.statbar = statbar;
        end
    end
end