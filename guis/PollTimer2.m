% 

classdef PollTimer2 < uiextrasX.HBox
    
    properties
        polltime
        poll
        tottime
        timers
    end
    
    events
        Poll
    end
    methods
        function o = PollTimer2(parent)
            o@uiextrasX.HBox('Parent', parent);
            defer(o);
            hbox = uiextrasX.HBox('Parent', o);
            uicontrol('style', 'text', 'String', 'Polling interval', 'Parent', hbox);
            o.polltime = uicontrol('Style', 'edit', 'String', '100', 'Parent', hbox);
            uicontrol('style', 'text', 'String', 'ms', 'Parent', hbox);
            o.poll = uicontrol('Style', 'togglebutton', 'String', 'START POLL', 'Parent', hbox, 'Callback', @(s,e)start_stop(o, get(o.poll, 'Value')));
            o.timers = timer('TimerFcn', @(s,e)poll_(o),...
                'Period', 0.001*str2double(get(o.polltime, 'String')),...
                'ExecutionMode', 'fixedRate');
            resume(o);
        end
        
        function start_stop(o,val)
            if val
                set(o.poll, 'String', 'STOP POLL', 'Value', 1);
                o.tottime = 0;
                start(o.timerr);
            else
                set(o.poll, 'String', 'START POLL', 'Value', 0);
                stop(o.timerr);
            end
        end
        
        function poll_(o)
            if get(o.poll, 'Value')
                notify(o, 'Poll');
                %disp('Poll');
                %pause(0.001*str2double(get(o.polltime, 'String')));
                o.tottime = o.tottime + 0.001*str2double(get(o.polltime, 'String'));
            end
        end
        
        function delete(o)
            delete(o.timerr);
        end
          
        
    end
    
end