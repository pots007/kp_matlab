% User interface for MGMotor controller which is the ActiveX interface to a
% Thorlabs APT linear actuator.
%
% This class provides an example as to putting an ActiveX control in a
% uiextras GUI and encapsulating it in a MATLAB class.
%
% (c) Dane R. Austi 2012
%
% Modified by KP in 2014 to take mat files for drive specific parameters
%
% Seems the activex control fails with some pitch/stepsperrev/gearbox
% combos. Hence setting external calibration parameter to translate into
% real world.

classdef MotorControl < uiextras.VBox
    properties
        serial % editbox for typing in serial port
        start_stop % start-stop button
        serial_list % list of preset serial numbers
        calib_dist % Actual distance travelled
        
        control % the ActiveX control
        started = false; % ActiveX started control flag
        cal_loaded = false; % Motor calibration loaded
        calfilname = [];
    end
    
    methods
        function obj = MotorControl(parent, calfile)
            obj@uiextras.VBox('Parent', parent);
            if strcmp(get(obj.Parent, 'Type'), 'figure')
                set(obj.Parent, 'MenuBar', 'none', 'HandleVisibility', 'callback', 'NumberTitle', 'off', 'Name', 'MGMotorControl'); % callback
                %setPositionComponent(obj.Parent, 4, 400);
                %uimenu(obj.Parent, 'Label', '');
            end
            
            hbox = uiextras.HBox('Parent', obj);
            uicontrol('Parent', hbox, 'Style', 'text', 'String', 'S/N:');
            obj.serial = uicontrol('Parent', hbox, 'Style', 'edit');
            serial_speed_dial = {'40808761', '83841566', '83840867','83841281'};
            obj.serial_list = uicontrol('Parent', hbox, 'Style', 'popupmenu', 'String', serial_speed_dial, 'Callback', @(s, e) serialListSelected(obj));
            obj.start_stop = uicontrol('Parent', hbox, 'Style', 'pushbutton', 'String', 'Start control', 'Callback', @(s,e) startStop(obj));
            hbox = uiextras.HBox('Parent', obj);
            uicontrol('String', 'Target', 'FontSize', 30, 'Parent', hbox, 'Style', 'text');
            obj.calib_dist.current.target = uicontrol('String', '00000', 'FontSize', 30, 'Parent', hbox, 'style', 'edit', 'Callback', @(s,e) GoToTarget(obj));
            uicontrol('string', 'Current', 'FontSize', 30, 'Parent', hbox, 'style', 'text');
            obj.calib_dist.current.box = uicontrol('style', 'text', 'Parent', hbox, 'FontSize', 30, 'String', '0.000');
            hbox = uiextras.HBox('Parent', obj);
            uicontrol('style', 'pushbutton', 'Parent', hbox, 'String', '<-', 'FontSize', 30, 'callback', @(o,val)Jog(obj,-1));
            obj.calib_dist.jogstep = uicontrol('style', 'edit', 'Parent', hbox, 'String', '10', 'FontSize', 30);
            uicontrol('style', 'pushbutton', 'Parent', hbox, 'String', '->', 'FontSize', 30, 'callback', @(o,val)Jog(obj,1));
            hbox = uiextras.HBox('Parent', obj);
            control_container = ActiveXContainer('MGMOTOR.MGMotorCtrl.1', 'Parent', hbox);
            obj.control = control_container.control;
            obj.control.SetDispMode(3);
            obj.Sizes = [20 60 60 -1];
            if nargin==2
                obj.calfilname = calfile;
                %setparams(obj, calfile);
                %obj.cal_loaded = true;
            end
            
            %set(obj.control_container, 'Parent', obj);
        end
        
        function serialListSelected(obj)
            serial_list = get(obj.serial_list, 'String');
            set(obj.serial, 'String', serial_list(get(obj.serial_list, 'Value')));
        end
        
        function startStop(obj)
            if obj.started
                obj.control.StopCtrl();
                set(obj.start_stop, 'String', 'Start control');
                set(obj.serial, 'Enable', 'on');
                obj.started = false;
            else
                obj.control.HWSerialNum = str2double(get(obj.serial, 'String'));
                obj.control.StartCtrl();
                
                %if ~obj.cal_loaded
                    if isempty(obj.calfilname)
                        defaul = which('Parker165.mat');
                        [filename, pathname] = uigetfile(defaul, 'Pick a calibration file');
                        obj.calfilname = [pathname '\' filename];
                    end
                    
                    setparams(obj, obj.calfilname);
                    obj.cal_loaded = true;
                %end
                set(obj.start_stop, 'String', 'Stop control');
                set(obj.serial, 'Enable', 'off');
                obj.started = true;
            end
        end
        
        function setparams(obj, calfile)
            r = load(calfile);
            fset = r.fset;
            obj.calib_dist.calib = fset.cal;
            obj.control.SetHWLimSwitches(0, fset.fwlim, fset.bwlim);
            obj.control.SetMotorParams(0, int32(fset.StepsPerRev), int32(fset.GearBoxRatio));
            obj.control.SetStageAxisInfo(0, single(fset.MinPos), single(fset.MaxPos), int32(fset.units), single(fset.Pitch), fset.Dir);
            obj.control.SetVelParams(0, single(fset.MinVel), single(fset.Accn), single(fset.MaxVel));
            obj.control.SetJogVelParams(0, single(fset.MinVel), single(fset.Accn), single(fset.MaxVel));
            obj.control.SetJogStepSize(0, single(fset.JogStep*fset.cal));
            obj.control.SetBLashDist(0, single(fset.BackLash));
            obj.calib_dist.fset = fset;
        end
        
        function delete(obj)
            %disp('MGMotorControl.delete');
            % Doesn't work: the control seems to be gotten rid of before
            % the destructor is applied
            %if obj.started
            %    obj.control.StopCtrl();
            %end
        end
        
        function Jog(obj, val)
            cur = getPosn(obj);
            step = (str2double(get(obj.calib_dist.jogstep, 'String')));
            obj.control.SetJogStepSize(0, single(step*obj.calib_dist.fset.cal));
            %set(obj.calib_dist.current.target, 'String', num2str(newpos));
            %moveAbs(obj, newpos);
            if val<0
                dir = 2;
            else 
                dir = 1;
            end
            obj.control.MoveJog(0,dir);
            while isMoving(obj)
                pause(0.1)
                set(obj.calib_dist.current.box, 'String', num2str(getPosn(obj)));
            end
            set(obj.calib_dist.current.box, 'String', num2str(getPosn(obj)));
        end
        
        function GoToTarget(obj)
            
                target = str2double(get(obj.calib_dist.current.target, 'String'));

            if target<obj.calib_dist.fset.MinPos || target>obj.calib_dist.fset.MaxPos
                return;
            end
            moveAbs(obj,target);
        end
        
        function moveAbs(obj, posn)
            actpos = posn*obj.calib_dist.calib;
            obj.control.SetAbsMovePos(0, single(actpos));
            obj.control.MoveAbsolute(0, false);
            while isMoving(obj)
                pause(0.1)
                set(obj.calib_dist.current.box, 'String', num2str(getPosn(obj)));
            end
            set(obj.calib_dist.current.box, 'String', num2str(getPosn(obj)));
        end
        
        function r = isMoving(obj)
            status = obj.control.GetStatusBits_Bits(0);
            % True if motor is moving forward or backward. See APT Server
            % help.
            r = bitget(abs(status), 5) || bitget(abs(status), 6);
        end
        
        function posn = getPosn(obj)
            pos = obj.control.GetPosition_Position(0);
            posn = pos/obj.calib_dist.calib;
        end
    end
end