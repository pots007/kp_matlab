% When using with HG2, ensure you pass a handle not a figure number!
% Eg, hfig = figure(1); make_latex(hfig) will work
% make_latex(1) will give an error.

function make_latex(h)

%make everything on figure handle h use the latex interpreter
if ~verLessThan('matlab', '8.4.0') % This is 2014b, the first to use HG2
    set(findall(h, 'Type', 'axes'), 'TickLabelInterpreter', 'latex');
    set(findall(h, 'Type', 'colorbar'), 'TickLabelInterpreter', 'latex');
    hcb = get(findall(h, 'Type', 'colorbar'), 'YLabel');
    if length(hcb)>1
        for i=1:length(hcb)
            set(hcb{i}, 'Interpreter', 'latex');
        end
    else
        set(hcb, 'Interpreter', 'latex');
    end
%     hChildren = h.Children;
%     n_Children = length(hChildren);
%     for i = 1:n_Children
%         if strcmp(h.Children(i).Type, 'axes');
%             h.Children(i).TickLabelInterpreter = 'latex';
%             h.Children(i).XLabel.Interpreter = 'latex';
%             h.Children(i).YLabel.Interpreter = 'latex';
%             h.Children(i).Title.Interpreter = 'latex';
%             h.Children(i).ZLabel.Interpreter = 'latex';
%         end
%         if strcmp(h.Children(i).Type, 'legend') || strcmp(h.Children(i).Type, 'text');
%             h.Children(i).Interpreter = 'latex';
%         end
%         if strcmp(h.Children(i).Type, 'colorbar');
%             h.Children(i).TickLabelInterpreter = 'latex';
%             h.Children(i).Label.Interpreter = 'latex';
%         end
%         % any other types??
%     end
%     hh = findall(h, 'Type', 'axes');
%     
%     
% else
%     hChildren = findobj(h);
%     set(hChildren(isprop(hChildren, 'Interpreter')), 'Interpreter', 'latex');
end
hChildren = findobj(h);
set(hChildren(isprop(hChildren, 'Interpreter')), 'Interpreter', 'latex');
set(findall(hChildren, 'Interpreter', 'tex'), 'Interpreter', 'latex');
set(findall(h, 'Type', 'text'), 'Interpreter', 'latex');

end