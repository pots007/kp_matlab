function varargout = Server(varargin)
% SERVER MATLAB code for Server.fig
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Server_OpeningFcn, ...
    'gui_OutputFcn',  @Server_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function Server_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
handles.output = hObject;
configs = csvimport('config.txt');
handles.numdiag = str2num(configs{2});
if (handles.numdiag < 3)
    handles.numdiag = 3;
end
handles.GSN = 00000;
% Update handles structure
guidata(hObject, handles);
maketable(hObject, eventdata, handles);
tim = clock;
datest = [num2str(tim(1)) sprintf('%02u', tim(2)) sprintf('%02u', tim(3))];
set(handles.datefield, 'String', datest);
%Adds special user functions path to matlab path.
rtfol = which('Server.m');
addpath([rtfol(1:length(rtfol)-8) 'funcs\']);
set(handles.stealthbox, 'Value', 1);
checkLastGSN(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = Server_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
varargout{1} = handles.output;


function takegrab(hObject, handles)
makesound(hObject, handles);
shotnum = get(handles.shotfield, 'String');
timout = str2double(get(handles.waitfield, 'String'));
pause(timout);
disp(['Grabbing shot number ' shotnum]);
tts(['Grabbing shot number ' shotnum])
initcol = get(handles.grabbutton, 'BackgroundColor');
initstr = get(handles.grabbutton, 'String');
set(handles.grabbutton, 'String', 'Grabbing', 'BackgroundColor', [0. 1. 0.]);
grabstuff(hObject, handles);
plotstuff(hObject, handles);
writelog(hObject, handles);
tts('Finished grabbing shot')
set(handles.grabbutton, 'String', initstr, 'BackgroundColor', initcol);
%nextshot = str2double(get(handles.shotfield, 'String'));
%set(handles.shotfield, 'String', num2str(str2double(shotnum) + 1));


function grabstuff(hObject, handles)
%set(handles.ServerFigure, 'HandleVisibility', 'off');
%close all;
%set(handles.ServerFigure, 'HandleVisibility', 'on');
fileprefix = [get(handles.datefield, 'String') 'r' sprintf('%03u', ...
    str2num(get(handles.runfield, 'String'))) 's' sprintf('%03u', ...
    str2num(get(handles.shotfield, 'String'))) '_'];
tabl = get(handles.tab, 'Data');
samefileerror = false(1, size(tabl,1));
nofileerror = false(1, size(tabl,1));
grabs = tabl(:,2);
times = zeros(1, handles.numdiag+1);
for i=1:handles.numdiag
    tic;
    if (~grabs{i})
        continue;
    end
    diagname = [fileprefix tabl{i,1}];
    diagpath = tabl{i,3};
    savpath = [get(handles.spath, 'String') '\' get(handles.datefield, ...
        'String') '\' get(handles.datefield, ...
        'String') 'r' sprintf('%03u', str2num(get(handles.runfield, ...
        'String'))) '\'];
    guidata(hObject, handles);
    if (~isdir(savpath))
        mkdir(savpath);
    end
    diagext = tabl{i,4};
    lastdiag = tabl{i,6};
    diag = [savpath diagname '.' diagext];
    %All files of diagnostic type in spath
    filen = dir([diagpath '\*.' diagext]);
    if (isempty(filen))
        nofileerror(i) = true;
        continue;
    end
    %last saved file
    filenum = zeros(1,length(filen));
    for m=1:length(filen)
        filenum(m) = filen(m).datenum;
    end
    [mas, filei] = max(filenum);
    %disp([diagpath '\' filen(filei).name])
    %disp(diag)
    if (lastdiag ~= -1)
        if (lastdiag == filen(filei).datenum)
            disp('shiiiiiiiiit');
            samefileerror(i) = true;
            copyfile([diagpath '\' filen(filei).name], diag);
        else
            copyfile([diagpath '\' filen(filei).name], diag);
            tabl{i,6} = filen(filei).datenum;
            %set(handles.tab, 'Data', tabl); %maybe only do it at the end of the loop?
        end
    else
        copyfile([diagpath '\' filen(filei).name], diag);
        tabl{i,6} = filen(filei).datenum;
        %set(handles.tab, 'Data', tabl); %maybe only do it at the end of the loop?
    end
    %times(i+1) = toc - times(i);
    tabl{i,8} = toc;%1000*times(i+1);
end
if (sum(samefileerror) ~= 0 || sum(nofileerror) ~= 0)
    samefile = cell(1,sum(samefileerror));
    k=0;
    for m=1:length(samefileerror)
        if (samefileerror(m))
            k = k + 1;
            samefile{k} = tabl{m,1};
        end
    end
    nofile = cell(1, sum(nofileerror));
    k=0;
    for m=1:length(nofileerror)
        if (nofileerror(m))
            k = k+1;
            nofile{k} = tabl{m,1};
        end
    end
    
    warndlg(['Same file for: ' samefile ' No file for: ' nofile],...
        'Warning!','modal');
end
set(handles.tab, 'Data', tabl);
nextshot = str2double(get(handles.shotfield, 'String'));
set(handles.shotfield, 'String', nextshot+1);


function plotstuff(hObject, handles)
tabl = get(handles.tab, 'Data');
grabs = tabl(:,2);
savpath = [get(handles.spath, 'String') '\' get(handles.datefield, ...
    'String') '\' get(handles.datefield, ...
    'String') 'r' sprintf('%03u', str2num(get(handles.runfield, ...
    'String'))) '\'];
shotfil = [get(handles.datefield, 'String') 'r' sprintf('%03u', ...
    str2num(get(handles.runfield, 'String'))) 's' sprintf('%03u', ...
    str2num(get(handles.shotfield, 'String'))-1) '_'];
for i=1:size(tabl,1)
    if (~grabs{i})
        continue;
    end
    exten = tabl{i,4};
    datafile = [savpath shotfil tabl{i,1} '.' tabl{i,4}];
    if (~exist(datafile, 'file'))
        continue;
    end
    switch (exten)
        case 'tif'
            data = imread(datafile);
            plot2d = true;
        case 'CR2'
            data = imread(datafile);
            plot2d = true;
        case 'raw'
            siz = importdata([tabl{i,1} '.txt']);
            data = ReadRAW16bit(datafile, siz(1), siz(2));
            plot2d = true;
        case 'txt'
            dataf = importdata(datafile);
            if (isstruct(dataf))
                data = dataf.data;
            else
                data = dataf;
            end
            plot2d = false;
        case 'sif'
            data = sifread(datafile);
            plot2d = true;
        case 'fit'
            data = fitsread(datafile);
            plot2d = true;
    end
    figure(20+i);
    %set(20+i, 'WindowStyle', 'docked');
    clf;
    if (strcmp([tabl{i,7}], ' ') ~= 1)
        funcnam = tabl{i,7};
        try
            feval(funcnam, data, 20+i);
            continue;
        catch err
            disp(['Error in ' tabl{i,1} ' plotfunction!']);
        end
    else
        if (plot2d)
            imagesc(data);
%             [N,X] = hist(data(:), linspace(1,2^16,2^10));
%             su = sum(sum(N));
%             cs = cumsum(N)/su;
%             [~,indL] = min(abs(cs - 0.1));
%             [~,indH] = min(abs(cs - 0.995));
%             cL = X(indL);
%             cH = X(indH);
%             imagesc(data)
%             caxis([cL cH])
            colormap('Gray');
            colormap('Gray');
            %set(gca, 'YDir', 'normal');
        else
            plot(data(:,1), data(:,2));
        end
    end
    if ((strcmp(tabl{i,5}, '-1') ~= 1))
        if (plot2d)
            set(gca, 'CLim', sscanf(tabl{i,5}, '%i')');
        else
            set(gca, 'YLim', sscanf(tabl{i,5}, '%i')');
        end
    end
    title(tabl{i,1}, 'Interpreter', 'none');
end
handles.plotshotfil = shotfil;
guidata(hObject, handles);
set(handles.lastshotbox, 'String', ['Last plotted shot ' shotfil(1:end-1)]);


function writelog(hObject, handles)
savpath = [get(handles.spath, 'String') '\'];
shot = [get(handles.datefield, 'String') 'r' sprintf('%03u', ...
    str2num(get(handles.runfield, 'String'))) 's' sprintf('%03u', ...
    str2num(get(handles.shotfield, 'String'))-1)];
GSN = get(handles.LastGSNbox, 'String');
curtim = clock;
fid = fopen([savpath 'log.txt'], 'a');
fprintf(fid, '%4i/%02i/%2i %02i:%02i:%02.2g\t', curtim(1:6));
fprintf(fid, '%s\t', shot);
fprintf(fid, '%u\n', str2num(GSN));
fclose(fid);


function maketable(hObject, ~, handles)
numdiag = handles.numdiag;
%numdiag = 10;
columnname =   {'Diagnostic', 'Grab', 'Path', 'Filetype', ...
    'CLim', 'Acq time', 'PlotFunction', 'LastFileTime'};
%columnname =   {'Diagnostic', 'Grab', 'Path', 'Filetype', ...
%    'CLim', 'Acq time', 'LastFileTime', 'xlabel', 'xcalib', ...
%    'ylabel', 'ycalib'};
columneditable =  [true true true true true false true false];
columnformat = {'char', 'logical', 'char',...
    {'tif', 'raw', 'txt', 'sif', 'fit', 'CR2'},...
    'char', 'numeric', 'char', 'numeric'};
dat = cell(numdiag, length(columnname));
diagnams = repmat({' '}, 1, numdiag);
diagnams{1} = 'Top_view';
diagnams{2} = 'Shadowgraphy';
diagnams{3} = 'Sidespec';
widths = {100 40 200 50 100 80 100 100};
grabs = repmat({true}, 1, numdiag);
paths = repmat({'D:\'}, 1, numdiag);
filetypes = repmat({'tif'}, 1, numdiag);
clims = repmat({'-1'}, 1, numdiag);
times = zeros(1, numdiag);
oldfil = -ones(1, numdiag);
dat(:,1) = diagnams;
dat(:,2) = grabs;
dat(:,3) = paths;
dat(:,4) = filetypes;
dat(:,5) = clims;
dat(:,6) = num2cell(oldfil);
dat(:,7) = repmat({' '}, 1, numdiag);
dat(:,8) = num2cell(times);
%dat(:,8) = repmat({'x'}, 1, numdiag);
%dat(:,9) =
t = uitable('Units','normalized','Position',...
    [0.2 0.1 0.7 0.8],'Data', dat,...
    'ColumnName', columnname,...
    'ColumnFormat', columnformat,...
    'ColumnEditable', columneditable,...
    'ColumnWidth', widths, ...
    'RowName',[]);
handles.tab = t;
guidata(hObject, handles);


function makesound(hObject, handles)
flag = get(handles.stealthbox, 'Value');
if (flag)
    return;
else
    sel = 1+floor(5*rand(1));
    [y, Fs] = audioread([pwd '\sounds\speech' num2str(sel) '.wav']);
    player = audioplayer(y,Fs);
    play(player);
end


function grabbutton_Callback(hObject, ~, handles)
makesound(hObject, handles);
shotnum = get(handles.shotfield, 'String');
disp(['Grabbing shot number ' shotnum]);
set(handles.grabbutton, 'String', 'Grabbing', 'BackgroundColor', [0. 1. 0.]);
grabstuff(hObject, handles);
plotstuff(hObject, handles);
writelog(hObject, handles);
set(handles.grabbutton, 'String', 'GRAB SHOT', ...
    'BackgroundColor',  [0.9412 0.9412 0.9412]);



function replotbutton_Callback(hObject, eventdata, handles)
datal = get(handles.tab, 'Data');
assignin('base', 'datal', datal);
plotstuff(hObject, handles);


function savesettings_Callback(hObject, eventdata, handles)
data = get(handles.tab, 'Data');
fid = fopen('diagnostics.txt', 'w');
for i=1:size(data,1)
    fprintf(fid, '%s,%i,%s,%s,%s,%f,%s\n', data{i,1:7});
end
fclose(fid);


function loadsettings_Callback(hObject, eventdata, handles)
data = csvimport('diagnostics.txt');
for i=1:size(data,1)
    try
        temp = str2num(data{i,2});
        temp2 = str2double(data{i,6});
    catch err
        temp = data{i,2};
        temp2 = data{i,6};
    end
    try
        temp3 = num2str(data{i,5});
    catch err
        temp3 = data{i,5};
    end
    data{i,2} = logical(temp);
    data{i,6} = temp2;
    data{i,5} = temp3;
end
%assignin('base', 'data', data);
set(handles.tab, 'Data', data);

function startbutton_Callback(hObject, eventdata, handles)
curr = get(handles.startbutton, 'String');
m = [];
if (strcmp(curr, 'START GRAB') == 1)
    set(handles.startbutton, 'String', 'STOP GRAB');
    set(handles.startbutton, 'BackgroundColor', [0. 1. 0.]);
    %pause(timout);
    set(handles.text5, 'String', num2str(1));
    m = udp_receiver();
end
if (strcmp(curr, 'STOP GRAB') == 1)
    set(handles.startbutton, 'String', 'START GRAB');
    set(handles.startbutton, 'BackgroundColor', [0.941 0.941 0.941]);
    set(handles.text5, 'String', num2str(0));
    m = findobj(allchild(0), 'flat', 'Tag', 'udp_figure');
    pause(0.5);
    delete(m);
end

function datebutton_Callback(hObject, eventdata, handles)
tim = clock;
datest = [num2str(tim(1)) sprintf('%02u', tim(2)) sprintf('%02u', tim(3))];
prompt = {['Enter date: P.S. Today would be ' datest]};
dlg_title = 'Set run date:';
num_lines = 1;
def = cellstr(datest);
answer = inputdlg(prompt,dlg_title,num_lines,def);
set(handles.datefield, 'String', answer{1});

function runbutton_Callback(hObject, eventdata, handles)
prompt = {'Enter run number:'};
dlg_title = 'Set run number:';
num_lines = 1;
def = {'1'};
options.WindowStyle = 'normal';
options.Resize = 'on';
answer = inputdlg(prompt,dlg_title,num_lines,def, options);
if (isempty(answer))
    answer = def;
end
set(handles.runfield, 'String', answer{1});
set(handles.text5, 'String', num2str(0));


function pathbutton_Callback(hObject, eventdata, handles)
% hObject    handle to pathbutton (see GCBO)
tabl = get(handles.tab, 'Data');
diaglist = tabl(:,1);
[ind,sel] = listdlg('PromptString','Select diagnostic:',...
    'SelectionMode','single',...
    'ListString',diaglist);
if (~sel)
    msgbox('No diagnostic selected.', 'Error.');
    return;
end
diagnam = diaglist{ind};
savpath = uigetdir('C:\',['Path for ' diagnam]);
tabl{ind,3} = [savpath '\'];
set(handles.tab, 'Data', tabl);


function cleanbutton_Callback(hObject, eventdata, handles)
%move files in each folder to dated subfolder
return;
numdiag = handles.numdiag;
folnam = get(handles.datefield, 'String');
tabl = get(handles.tab, 'Data');
hwait = waitbar(0, 'Clearing out folders');
moveerr = false;
for i=1:numdiag
    diagext = tabl{i,4};
    diagpath = tabl{i,3};
    filen = dir([diagpath '\*.' diagext]);
    if (isempty(filen) || length(filen)<6)
        continue;
    end
    cleanpath = [diagpath folnam '\'];
    if (~exist(cleanpath, 'dir'))
        try
            mkdir(cleanpath);
        catch err
            
        end
    end
    for m=1:length(filen)
        try
            movefile([diagpath filen(m).name], [cleanpath filen(m).name]);
            %disp(mess);
        catch err
            moveerr = true;
        end
    end
    if (moveerr)
        disp(['Could not do ' tabl{i,1} '!']);
    end
    waitbar(i/numdiag);
end
close(hwait);


function othersettings_Callback(hObject, eventdata, handles)
[filen filpath filin] = uigetfile('*.txt',...
    'Select file with settings:');
data = csvimport([filpath filen]);
if (size(data,2)<7)
    errordlg('File insufficent', 'Error loading settings');
    return;
end
for i=1:size(data,1)
    try
        temp = str2num(data{i,2});
        temp2 = str2double(data{i,6});
    catch err
        temp = data{i,2};
        temp2 = data{i,6};
    end
    data{i,2} = logical(temp);
    data{i,6} = temp2;
end
%assignin('base', 'data', data);
set(handles.tab, 'Data', data);


function oneshotplot_Callback(hObject, eventdata, handles)
prompt = {'Enter date number:', 'Enter run number:',...
    'Enter shot number:'};
dlg_title = 'Enter shot to plot:';
num_lines = 1;
def = {get(handles.datefield, 'String'), '1', '1'};
options.WindowStyle = 'normal';
options.Resize = 'on';
answer = inputdlg(prompt,dlg_title,num_lines,def, options);
if (isempty(answer))
    return;
end
shotfil = [answer{1} 'r' sprintf('%03u',...
    str2num(answer{2})) 's' sprintf('%03u', str2num(answer{3})) '_'];
handles.plotshotfil = shotfil;
guidata(hObject, handles);
plotagain(hObject, handles);


function plotagain(hObject, handles)
%guidata(hObject,handles);
tabl = get(handles.tab, 'Data');
%savpath = [get(handles.spath, 'String') '\' answer{1} '\' answer{1} 'r' sprintf('%03u',...
%    str2num(answer{2})) '\'];
shotfil = handles.plotshotfil;
savpath = [get(handles.spath, 'String') '\' shotfil(1:8) '\' shotfil(1:12) '\'];
for i=1:size(tabl,1)
    exten = tabl{i,4};
    datafile = [savpath shotfil tabl{i,1} '.' tabl{i,4}];
    if (~exist(datafile, 'file'))
        continue;
    end
    switch (exten)
        case 'tif'
            data = imread(datafile);
            plot2d = true;
        case 'CR2'
            data = imread(datafile);
            plot2d = true;
        case 'raw'
            siz = importdata([tabl{i,1} '.txt']);
            data = ReadRAW16bit(datafile, siz(1), siz(2));
            plot2d = true;
        case 'txt'
            dataf = importdata(datafile);
            if (isstruct(dataf))
                data = dataf.data;
            else
                data = dataf;
            end
            plot2d = false;
        case 'sif'
            data = sifread(datafile);
            plot2d = true;
        case 'fit'
            data = fitsread(datafile);
            plot2d = true;
    end
    figure(20+i);
    %set(20+i, 'WindowStyle', 'docked');
    clf;
    if (strcmp(tabl{i,7}, ' ') ~= 1)
        funcnam = tabl{i,7};
        try
            feval(funcnam, data, 20+i);
            continue;
        catch err
            disp(['Error in ' tabl{i,1} ' plotfunction!']);
        end
    else
        if (plot2d)
%             [N,X] = hist(data(:), linspace(1,2^16,2^10));
%             su = sum(sum(N));
%             cs = cumsum(N)/su;
%             [~,indL] = min(abs(cs - 0.005));
%             [~,indH] = min(abs(cs - 0.995));
%             cL = X(indL);
%             cH = X(indH);
            imagesc(data)
%             caxis([cL cH])
            colormap('Gray');
            %imagesc(data);
        else
            plot(data(:,1), data(:,2));
        end
    end
    if (strcmp(tabl{i,5}, '-1') ~= 1)
        if (plot2d)
            set(gca, 'CLim', sscanf(tabl{i,5}, '%i')');
        else
            set(gca, 'YLim', sscanf(tabl{i,5}, '%i')');
        end
    end
    title(tabl{i,1}, 'Interpreter', 'none');
end
set(handles.lastshotbox, 'String', ['Last plotted shot ' shotfil(1:end-1)]);


function checkLastGSN(hObject, handles)
tic;
hudpr = dsp.UDPReceiver('RemoteIPAddress', '0.0.0.0', ...
    'LocalIPPort', 6770, 'MessageDataType', 'uint8');
receivedshot = -1;
for i=1:100
    dataReceived = step(hudpr);
    if (~isempty(dataReceived))
        receivedstring = cast(dataReceived', 'char');
        if (strcmp(receivedstring, ' GSN:TEST')==1)
            break;
        end
        receivedshot = str2double(receivedstring(6:11));
        break;
    end
    pause(0.1);
end
if (receivedshot == -1)
    h0 = warndlg('No UDP stream detected!',...
        'Warning!','modal');
    uiwait(h0);
else
    set(handles.LastGSNbox, 'String', num2str(receivedshot));
end

function shotfield_Callback(hObject, eventdata, handles)

function stealthbox_Callback(hObject, eventdata, handles)

function waitfield_Callback(hObject, eventdata, handles)

function runfield_Callback(hObject, eventdata, handles)

function file_Callback(hObject, eventdata, handles)

function datefield_Callback(hObject, eventdata, handles)

function spath_Callback(hObject, eventdata, handles)

function LastGSNbox_Callback(hObject, eventdata, handles)

function datefield_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function waitfield_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function shotfield_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function spath_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function runfield_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function LastGSNbox_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function prevshot_Callback(hObject, eventdata, handles)
if (isfield(handles, 'plotshotfil')) 
    prevshot = handles.plotshotfil;
else
    disp('Plot any shot first!');
    return
end
preshotno = str2double(prevshot(end-3:end-1));
handles.plotshotfil = [prevshot(1:end-4) sprintf('%03i', preshotno-1) '_'];
guidata(hObject, handles);
plotagain(hObject, handles);


function nextshot_Callback(hObject, eventdata, handles)
if (isfield(handles, 'plotshotfil')) 
    prevshot = handles.plotshotfil;
else
    disp('Plot any shot first!');
    return
end
preshotno = str2double(prevshot(end-3:end-1));
handles.plotshotfil = [prevshot(1:end-4) sprintf('%03i', preshotno+1) '_'];
guidata(hObject, handles);
plotagain(hObject, handles);
