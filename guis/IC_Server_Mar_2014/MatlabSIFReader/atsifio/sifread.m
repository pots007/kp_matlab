%opens andor file
function image = sifread(filename)
rc=atsif_setfileaccessmode(0);
rc=atsif_readfromfile(filename);
if (rc == 22002)
    signal=0;
    [rc,present]=atsif_isdatasourcepresent(signal);
    if present
        [rc,size]=atsif_getframesize(signal);
        [rc,left,bottom,right,top,hBin,vBin]=atsif_getsubimageinfo(signal,0);
        [rc,pattern]=atsif_getpropertyvalue(signal,'ReadPattern');
        [rc,data]=atsif_getframe(signal,0,size);
        data = reshape(data,(top-bottom+1),(right-left+1));
    end
    atsif_closefile;
else
    disp('Could not load file.  ERROR - ');
    disp(rc);
end
%imagesc(data);
image = data;
end