function xaxis = calibrateForwardSpec(date)

switch date
    case '20130819'
        lambda = [365.02 404.66 407.78 435.83 507.3 546.07 576.96 579.07 760.09 811.53];
        lambda = lambda*1e-9;
        pixels = [47 118 124 175 305 375 431 435 766 859];
    case '20130820'
        pixels = [11 82 88 138 268 338 394 398 728 820 1005];
        lambda = [365.02 404.66 407.78 435.83 507.3 546.07 576.96 579.07 760.09 811.53 912.30];
        lambda = lambda*1e-9;
end
coef = polyfit(pixels, lambda, 3);
xaxis = polyval(coef, 1:1:1024);

end