function hobj = udp_receiver()
% Pass data back and forth...
S.fh = figure('units','pixels',...
    'position',[50 50 200 130],...
    'menubar','none',...
    'numbertitle','off',...
    'name','udp_receiver',...
    'Tag', 'udp_figure',...
    'resize','off');
S.ed = uicontrol('style','text',...
    'units','pix',...
    'position',[10 60 180 60],...
    'string','Wait for UDPs');
S.ls = uicontrol('style','text',...
    'units','pix',...
    'position',[10 20 180 30],...
    'string','000000');
guidata(S.fh,S);
uicontrol(S.ed);
hudpr = dsp.UDPReceiver('RemoteIPAddress', '0.0.0.0', ...
    'LocalIPPort', 6770, 'MessageDataType', 'uint8');
lastshot = [];%importdata('lastshot.txt');
newshot = [];
%Added 26/09 to fix 'shots taken when server not active' bug
k = 1;
%while(isempty(lastshot))
streamempty = false;
while(k<100 && ~streamempty) % && isempty(lastshot))
    dataReceived = step(hudpr);
    k = k + 1;
    if (~isempty(dataReceived))
        receivedstring = cast(dataReceived', 'char');
        if (strcmp(receivedstring, ' GSN:TEST') == 1)
            continue;
        end
        lastshot = str2double(receivedstring(6:11));
        if (newshot == lastshot)
            streamempty = true;
        end
        newshot = lastshot;
    end
    pause(0.1);
end
set(S.ls, 'String', num2str(lastshot));
hobj = S.fh;
h = findobj(allchild(0), 'flat', 'Tag', 'ServerFigure');
h2 = guidata(h);
temp = true;
while (temp)
    dataReceived = step(hudpr);
    if (~isempty(dataReceived))
        %lastshot = importdata('lastshot.txt');
        receivedstring = cast(dataReceived', 'char');
        if (strcmp(receivedstring, ' GSN:TEST') || strcmp(receivedstring, ' BANG'))
            continue;
        end
        receivedshot = str2double(receivedstring(end-5:end));
        if (receivedshot == lastshot)
            continue;
        end
        newshot = receivedshot;
        disp(receivedstring);
        %fid = fopen('lastshot.txt', 'w');
        %fprintf(fid, '%i', newshot);
        %fclose(fid);
        set(h2.LastGSNbox, 'String', num2str(newshot));
        set(S.ls, 'String', num2str(newshot));
        Server('takegrab', h, h2);
        lastshot = newshot;
    end
    pause(0.25);
    temp = logical(str2num(get(h2.text5, 'String'))); %#ok<ST2NM>
end
