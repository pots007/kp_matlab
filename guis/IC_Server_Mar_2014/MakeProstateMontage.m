clear
% First prostate sample
% bestimages = [ 76  78  81  83  88  89  94; ...
%     72  70  65  63  60  53  42; ...
%     113 111 108 105 102 100  95; ...
%     117 121 123 126 130 132 135];

% Second prostate sample
bestimages = [ 108  105  100  99  95  92  88; ...
    21  18  13  3  5  7  10; ...
    25  27  31  34  35  38  41; ...
    64 60 56 54 52 48 45; ...
    67 69 71 75 77 81 87];

xoffset = [ 0 0 0 0 0 0 50; ...
    0 0 0 0 0 0 0; ...
    0 0 0 0 0 0 0; ...
    0 0 0 0 0 0 0; ...
    0 0 0 0 0 0 0];

% First prostate sample
% y = [1.5 0.5 -0.5 -1];
% x = [6.8 7.8 8.8 9.8 10.8 11.8 12.8];

% Second prostate sample
y = [1.5 0.5 -0.5 -1.5 -2.5];
x = [6.8 7.8 8.8 9.8 10.8 11.8 12.8];

pixpermm = 641;

imagewidth = 1024;
imageheight = 1024;
totalpix_x = imagewidth + ceil(pixpermm*(max(x) - min(x) + 1)) + 500;
totalpix_y = imageheight + ceil(pixpermm*(max(y) - min(y))) + 500;
montage = zeros(totalpix_y, totalpix_x);

for i = 1:length(y)
    for j = 1:length(x)
        % First prostate sample
        %I = medfilt2(double(imread(GetFilename('E:\2013GeminiNajmudin\Data\', 12, 6, 2, bestimages(i,j), 'x-ray.tif'))));
        % Second prostate sample
        I = medfilt2(double(imread(GetFilename('E:\2013GeminiNajmudin\Data\', 12, 6, 3, bestimages(i,j), 'x-ray.tif'))));
        idxlo =(max(x) - x(j))*pixpermm + 1 + xoffset(i,j);
        idxhi = idxlo + imagewidth - 1 + xoffset(i,j);
        idylo = (y(i) - min(y))*pixpermm + 1;
        idyhi = idylo + imageheight - 1;
        [N,X] = hist(I(:), linspace(1,2^16,2^10));
        [val, idx] = max(N(50:end));
        I = I/X(idx+49);
        xaxis = 1:imagewidth;
        yaxis = 1:imageheight;
        Is = csaps({xaxis, yaxis}, I, 1e-8, {xaxis, yaxis}); 
        I = I./Is;
        montage(idylo:idyhi, idxlo:idxhi) = I;
        figure(101)
        imagesc(montage)
        caxis([0 2])
        colormap(gray)
        drawnow
    end
end

% imagesontop = [ 0 0 0 0 0 0 0; ...
%     0 0 0 0 0 0 0; ...
%     0 0 0 0 0 0 0; ...
%     0 0 0 0 0 0 0];

imagesontop = [ 0 0 0 0 0 0 0; ...
    0 0 0 0 0 0 0; ...
    0 0 0 0 0 0 0; ...
    0 0 0 0 0 0 0; ...
    0 0 0 0 0 0 0];

for i = 1:length(y)
    for j = 1:length(x)
        if imagesontop(i,j) == 1
%             I = double(imread(GetFilename('E:\2013GeminiNajmudin\Data\', 12, 6, 2, bestimages(i,j), 'x-ray.tif')));
            I = double(imread(GetFilename('E:\2013GeminiNajmudin\Data\', 12, 6, 3, bestimages(i,j), 'x-ray.tif')));
            idxlo =(max(x) - x(j))*pixpermm + 1;
            idxhi = idxlo + imagewidth - 1;
            idylo = (y(i) - min(y))*pixpermm + 1;
            idyhi = idylo + imageheight - 1;
            montage(idylo:idyhi, idxlo:idxhi) = I;
            imagesc(montage)
            colormap(gray)
            drawnow
        end
    end
end

% imwrite(uint16(montage*2^16), 'ProstateMontage_clean.tif', 'tiff')
%imwrite(uint16(montage*2^14), 'ProstateMontage_clean.tif', 'tiff')
