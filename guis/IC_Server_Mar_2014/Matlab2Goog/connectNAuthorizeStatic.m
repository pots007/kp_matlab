function [aTokenDocs,aTokenSpreadsheet]=connectNAuthorizeStatic(userName, password)

[aTokenDocs,authorized]=urlread('https://www.google.com/accounts/ClientLogin','POST',...
    {'Email',userName,'Passwd',password,'source','CAG-Matlab-1','service','writely'})
if authorized==1
    aTokenDocs=['auth' aTokenDocs(strfind(aTokenDocs,'Auth')+4:end-1)];
    clear authorized;
else
    aTokenDocs='';
end
[aTokenSpreadsheet,authorized]=urlread('https://www.google.com/accounts/ClientLogin','POST',...
    {'Email',userName,'Passwd',password,'source','CAG-Matlab-1','service','wise'});
if authorized==1
    aTokenSpreadsheet=['auth' aTokenSpreadsheet(strfind(aTokenSpreadsheet,'Auth')+4:end-1)];
    clear authorized;
else
    aTokenSpreadsheet='';
end


% [str,authorized]=urlread('https://www.google.com/accounts/ClientLogin','POST',...
% {'Email',userName,'Passwd',password,'source','My-Matlab-1','service',service});
% 
% if authorized==1
% aToken=['auth' str(strfind(str,'Auth')+4:end-1)];
% else
% aToken='';
end 