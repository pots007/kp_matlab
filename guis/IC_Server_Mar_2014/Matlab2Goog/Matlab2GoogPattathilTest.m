clear variables; clc;
% data = GetGeminiShotData;
% allshots = data.shots;
% allpres = data.gaspressures;
% allens = data.uncompressedenergies;
% allcomens = data.compressedenergies;
userName='iclaserplasma@gmail.com'; %your '...@gmail.com' email address; if empty you can enter one in the dialog box. 
password = 'Imperial.1';
%[aTokenDocs,aTokenSpreadsheet]=connectNAuthorize(userName);
[aTokenDocs,aTokenSpreadsheet]=connectNAuthorizeStatic(userName, password);
pause(0.5);

if isempty(aTokenDocs) || isempty(aTokenSpreadsheet)
    warndlg('Could not obtain authorization tokens from Google.','');
    return;
end
disp('Logged in.');
sheetIwant = -1;
sheetlist = getSpreadsheetList(aTokenSpreadsheet);
if (~isempty(sheetlist))
    for i=1:length(sheetlist)
        if (strcmp('PattathilSummer2013_ShotSheet', sheetlist(i).spreadsheetTitle))
            sheetIwant = i;
        end
    end
end

worksheetlist = getWorksheetList(sheetlist(sheetIwant).spreadsheetKey, aTokenSpreadsheet);
worksheetIwant = -1;
if (~isempty(worksheetlist))
    for i=1:length(worksheetlist)
        if (strcmp('ShotSheet', worksheetlist(i).worksheetTitle))
            worksheetIwant = i;
        end
    end
end

if (sheetIwant<0 || worksheetIwant <0)
    if (sheetIwant<0)
        error('Incorrect worksheet name!'); 
    else
        error('Incorrect worksheet name!');
    end
end

return
for i=2500:2860
    disp(['Iteration ' num2str(i)]);
    GSN = getWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, i, 5, aTokenSpreadsheet);
    j=1;
    if(isempty(GSN))
        continue;
    end
    try
        while(str2double(GSN) ~= allshots(j))
            j = j+1; 
        end
    catch err
       continue 
    end
    %All pressures
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, ...
        i, 10, num2str(allpres(j)), aTokenSpreadsheet);
    
    % Uncompressed energies
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, ...
        i, 24, num2str(allens(j)), aTokenSpreadsheet);
    
    % Compressed energies
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, ...
        i, 25, num2str(allcomens(j)), aTokenSpreadsheet);
end
