clear variables; clc;
% data = GetGeminiShotData;
% allshots = data.shots;
% allpres = data.gaspressures;
% allens = data.uncompressedenergies;
% allcomens = data.compressedenergies;
userName='iclaserplasma@gmail.com'; %your '...@gmail.com' email address; if empty you can enter one in the dialog box. 
password = 'Imperial.1';
%[aTokenDocs,aTokenSpreadsheet]=connectNAuthorize(userName);
[aTokenDocs,aTokenSpreadsheet]=connectNAuthorizeStatic(userName, password);
%pause(0.5);

if isempty(aTokenDocs) || isempty(aTokenSpreadsheet)
    warndlg('Could not obtain authorization tokens from Google.','');
    return;
end
disp('Logged in.');
sheetIwant = -1;
sheetlist = getSpreadsheetList(aTokenSpreadsheet);
if (~isempty(sheetlist))
    for i=1:length(sheetlist)
        if (strcmp('NajmudinGeminiNov2013_DataSheet', sheetlist(i).spreadsheetTitle))
            sheetIwant = i;
        end
    end
end

worksheetlist = getWorksheetList(sheetlist(sheetIwant).spreadsheetKey, aTokenSpreadsheet);
worksheetIwant = -1;
if (~isempty(worksheetlist))
    for i=1:length(worksheetlist)
        if (strcmp('ShotSheet', worksheetlist(i).worksheetTitle))
            worksheetIwant = i;
        end
    end
end

if (sheetIwant<0 || worksheetIwant <0)
    if (sheetIwant<0)
        error('Incorrect worksheet name!'); 
    else
        error('Incorrect worksheet name!');
    end
end

acGSN = ParseGeminiShotData;
fGSN =  getWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, 3, 5, aTokenSpreadsheet);
i = 3+acGSN.shotid-str2double(fGSN);
GSN = getWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, i, 5, aTokenSpreadsheet);
while  (str2double(GSN) ~= acGSN.shotid)
    i = i+1;
    GSN = getWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, i, 5, aTokenSpreadsheet)
    if(isempty(GSN))
        continue;
    end 
     
end

editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, ...
        i, 20, num2str(acGSN.spulselength), aTokenSpreadsheet);

return
for i=98:101
    disp(['Iteration ' num2str(i)]);
    GSN = getWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, i, 5, aTokenSpreadsheet)
    j=1;    toc
    if(isempty(GSN))
        continue;
    end 

    if(str2double(GSN) ~= acGSN.shotid)
        continue;
    end
    
    %All pressures
    editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
        worksheetlist(worksheetIwant).worksheetKey, ...
        i, 20, num2str(acGSN.spulselength), aTokenSpreadsheet);

%     % Uncompressed energies
%     editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
%         worksheetlist(worksheetIwant).worksheetKey, ...
%         i, 24, num2str(allens(j)), aTokenSpreadsheet);
%     
%     % Compressed energies
%     editWorksheetCell(sheetlist(sheetIwant).spreadsheetKey, ...
%         worksheetlist(worksheetIwant).worksheetKey, ...
%         i, 25, num2str(allcomens(j)), aTokenSpreadsheet);
end
