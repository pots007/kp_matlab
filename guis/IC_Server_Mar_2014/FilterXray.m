clear

I = imread('E:\2013GeminiNajmudin\Data\20131204\SUM_20131204r006.tif');
imagesc(log(abs(fftshift(fft2(interp2(I))))))
fI = fftshift(fft2(interp2(I)));

centre = [2048 2048];
bottomright = [2069 2097];
bottommid = [2032 2178];
bottomleft = [2018 2085];

basis = zeros(2,3);
basis(:,1) = (bottomright - centre)/2;
basis(:,2) = (bottommid - centre)/3;
basis(:,3) = (bottomleft - centre)/2;
r = 5;
sigma = 3;
x = -r:r;
[xg yg] = meshgrid(x,x);
kernel = 1 - exp(-(xg.^2 + yg.^2)/(2*sigma^2));
imagesc(log(abs(fI)))
hold on
scatter(centre(1), centre(2))
for n1 = -10:10
    for n2 = -10:10
        for n3 = 0:0
            if ((abs(n1) > 0) && (abs(n2) > 0))
            indX = round(centre(1) - n1*basis(1,1) - n2*basis(1,2) - n3*basis(1,3));
            indY = round(centre(2) - n1*basis(2,1) - n2*basis(2,2) - n3*basis(2,3));
            scatter(indX, indY)
            fI(indY-r:indY+r, indX-r:indX+r) = fI(indY-r:indY+r, indX-r:indX+r).*kernel;
            end
        end
    end
end
hold off


imagesc(abs(ifft2(fftshift(fI))))



