%Reads AVT camera raw files.
function I = ReadRAW16bit(filename, width, height, varargin)

bands = 1;
precision = 'uint16';
offset = 0;
interleave = 'bip'; %smth
if nargin==4
    if varargin{1}==0
        byteorder = 'ieee-be'; %Little Endian
    else
        byteorder = 'ieee-le';
    end
else
    byteorder = 'ieee-be';
end
I = multibandread(filename, [height width bands], precision, offset, interleave, byteorder);
I = flipud(I);
end