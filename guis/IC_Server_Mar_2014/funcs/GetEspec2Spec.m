function counts = GetEspec2Spec(data)

mm0 = 1004;
mm200 = 293;
top = 378;
bot = 583;
rotation = 1.1;

dataorig = data;
data = double(data);
data = imrotate(data, rotation);

mmperpix = 200/(mm0 - mm200);
data = data(top:bot,1:mm0);
[ysize xsize] = size(data);
xaxis = 1:xsize;
xaxis = mmperpix*xaxis;
xaxis = fliplr(xaxis);
topfifth = round(0.2*ysize);
botfifth = round(0.8*ysize);
bgtop = mean(data(1:topfifth,:));
bgbot = mean(data(botfifth:end,:));
specdata = data(topfifth+1:botfifth-1,:);
[ysize xsize] = size(specdata);
for n = 1:length(bgtop)
   bg(:,n) = linspace(bgtop(n), bgbot(n),ysize); 
end
bgcorrected = specdata - bg;
counts = sum(bgcorrected);

end