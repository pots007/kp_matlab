function him = plotGspec(data, hfig)

data = double(data);


figure(hfig);
imagesc(data)
% load 'invfire.mat'
% colormap(iijcm);
% grid on;
colormap('Gray');
colorbar;
axis image xy
set(gca, 'CLim', [50 150]);
title('Gspec');
him = hfig;

end