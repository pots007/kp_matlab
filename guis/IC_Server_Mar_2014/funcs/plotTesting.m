function out = plotTesting(data,hfig)
% If the figure doesn't exist yet, make it.
if ~ishandle(hfig)
    figure(hfig);
end
% Find the axes object in this figure
hax = get(hfig, 'Children');
if isempty(hax) % If no axis yet exist, make axes and plot
    hax = axes('Parent', hfig);
    set(hax, 'DrawMode', 'fast');
    imagesc(data, 'Parent', hax);
else % Otherwise see if we have anything plotted yet
    him = get(hax, 'Children');
    if isempty(him) % if not, plot something
        imagesc(data, 'Parent', hax);
    else % if a plot exists, update its cdata
        set(him, 'CData', data);
    end
end
% Set colormap for this figure
colormap(hfig, 'gray');
% Set title for this figure
title(hax,'Testytestytest');
% Calculate whatever we want to add to continious plot graphs
out = mean(data(:));
end