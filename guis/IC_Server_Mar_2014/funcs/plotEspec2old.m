function him = plotEspec2(data, hfig)

data2 = double(data);

pixels = 1:size(data,2);
%distance = (-7e-5)*pixels.*pixels + 0.2704*pixels - 10.261;
loaddE = load('new_d_and_E.mat');
d_and_E = loaddE.d_and_E;

%energy_fitting = csaps(d_and_E(:,1), d_and_E(:,2));
energy_fitting = csaps(d_and_E(:,2), d_and_E(:,1));
% energy2 = fnval(distance, energy_fitting);

pxpoints = 100:100:600;
dpoints = (-7e-5)*pxpoints.*pxpoints + 0.2704*pxpoints - 10.261;
Epoints = fnval(dpoints, energy_fitting);

%now draw lines at pxpoints and put write on corresponding energies.
figure(hfig);
imagesc(data2); colormap('Gray'); colorbar; xlabel('Energy/MeV'); title('Espec2');
set(gca, 'XTick', pxpoints, 'XTickLabel', round(Epoints), ...
    'CLim', [0 15000]);

for n=1:length(pxpoints)
    
    line([pxpoints(n) pxpoints(n)], [1 size(data2,1)], 'Color', 'Red');
    %text(pxpoints(n) - 10, 20, strcat(num2str(round(Epoints(n))), ' MeV', 'color', 'R'));
    
end
%puts energy markers on the current image
him = hfig;
end