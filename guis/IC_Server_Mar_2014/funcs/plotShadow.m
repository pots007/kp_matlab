function him = plotShadow(data, hfig)
figure(hfig)
[N,X] = hist(data(:), linspace(1,2^16,2^10));
su = sum(sum(N));
cs = cumsum(N)/su;
[~,indL] = min(abs(cs - 0.005));
[~,indH] = min(abs(cs - 0.995));
cL = X(indL);
cH = X(indH);
[ysize, xsize] = size(data);
xaxis = 1:xsize;
yaxis = 1:ysize;
xaxis = xaxis*0.013;
yaxis = yaxis*0.013;
imagesc(xaxis, yaxis, data)
axis image xy
caxis([cL cH])
colormap('Gray');
title('Shadowgraphy');
xlabel('x /mm')
ylabel('y /mm')
him = hfig;
end