function [spectrum] = onaxis_sync_spectrum(E, Ecrit)
%returns the on axis synchrotron spectrum for a single monoenergetic
%electron S(E, Ecrit);

z = E./(2*Ecrit);
spectrum = E.*((besselk(2/3, z)).^2);

end