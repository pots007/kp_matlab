filen = dir('*.FIT');
tots = zeros(length(filen), 4045);
figure(6);
set(6, 'WindowStyle', 'docked');
for i=1:length(filen)
   %figure(6);
   disp(filen(i).name);
   if (filen(i).bytes < 10000000)
       continue
   end
   if (exist([filen(i).name(1:length(filen(i).name)-3) 'png']))
       continue;
   end
   I = fitsread(filen(i).name); 
   I = imrotate(I, thet);
   %tots(i,:) = sum(I);
   imagesc(xaxis, 1:1:size(I,1), I);
   set(gcf, 'ColorMap', divmap);
   set(gca, 'CLim', [0 15000]);
   saveas(6, [filen(i).name(1:length(filen(i).name)-3) 'png']);
   %close(6);
end