function him = plotForwardSpec(data, hfig)
figure(hfig)
xaxis = calibrateForwardSpec('20130820');
yaxis = 26*(1:1:256);
imagesc(xaxis*1e9, yaxis, data + 2^0);
xlabel('Wavelength (nm)');
ylabel('Vertical coordinate (\mu m)');
title('Forward spectrometer');
colorbar;
him = hfig;
end