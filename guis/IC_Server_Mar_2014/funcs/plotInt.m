function him = plotInt(data, hfig)
figure(hfig)
[N,X] = hist(data(:), linspace(1,2^16,2^10));
su = sum(sum(N));
cs = cumsum(N)/su;
[~,indL] = min(abs(cs - 0.005));
[~,indH] = min(abs(cs - 0.995));
cL = X(indL);
cH = X(indH);
imagesc(data)
caxis([cL cH])
colormap('Gray');
title('Interferometer');
him = hfig;
end