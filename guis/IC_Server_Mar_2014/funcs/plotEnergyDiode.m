function him = plotEnergyDiode(data, hfig)
figure(hfig);
plot(data(:,1), data(:,2));
set(gca, 'XLim', [-5e-2 5e-2]);
ylabel('Voltage (V)')
xlabel('Time (s)');
%maxvol = max(data(:,2));
vol = data(round(0.5*end),2);
%Calibration from 20130926 diode calibration shots
%Correct for 1001 and 1002
%en = 0.0568*maxvol^2 - 0.00738*maxvol + 0.0955;
%Corrected on 1003
%en = 0.2338*maxvol -0.0009;
en = (vol-4)/80*1000;
den = en*5.2e16;
title([num2str(vol) ' V = ' num2str(en) ' mbar = ' num2str(den*1e-18) ' 10^{18} cm^{-3}']);
him = hfig;