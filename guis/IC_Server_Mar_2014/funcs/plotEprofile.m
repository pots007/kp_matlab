function him = plotEprofile(data, hfig)

data = double(data);

%fig_no= fig_no+1;
% figure(fig_no);
% imagesc(ref)

Ref_Ver= sum(data');
% y_ref= find(max(Ref_Ver)==Ref_Ver);
y_ref=270;
Ref_Hor= sum(data);
% x_ref= find(max(Ref_Hor)==Ref_Hor);
x_ref=372;
%determine divergence anngle

% Im_Ver= sum(data');
% 
% Im_Hor= sum(data);
y_pixel= 1:length(Ref_Ver);
x_pixel= 1:length(Ref_Hor);

y_distance=1000*(y_pixel*0.0526-y_ref*0.0526)/400; % in mrad
x_distance= 1000*(x_pixel*0.0519*cos(pi/4)-x_ref*0.0519*cos(pi/4))/400;

% plot(x_distance,Ref_Hor)
% fig_no= fig_no+1;
% figure(fig_no);
% plot(y_distance,Ref_Ver)
% fig_no= fig_no+1;
figure(hfig);
imagesc(x_distance, y_distance, data)
load 'invfire.mat'
colormap(iijcm);
grid on;
colorbar;
set(gca, 'CLim', [0 2000]);
title('Eprofile');
him = hfig;
end