function him = plotExitMode(data, hfig)

data = double(data);

figure(hfig);
imagesc(data)
colormap('Gray');
colorbar;
axis image xy
%set(gca, 'CLim', [0 2000]);
title('Exit Mode');
him = hfig;

end