function him = plotEprofile2(data, hfig)

data = double(data);


figure(hfig);
imagesc(data)
% load 'invfire.mat'
% colormap(iijcm);
% grid on;
colormap('Gray');
colorbar;
axis image xy
set(gca, 'CLim', [0 10000]);

bg_rect =  [3 3 310 48];
data_rect = [26 85 296 268];

bg = mean(mean(data(bg_rect(2):(bg_rect(2)+bg_rect(4)), bg_rect(1):(bg_rect(1)+bg_rect(3)))));
data = data - bg;

counts = sum(sum(data(data_rect(2):(data_rect(2)+data_rect(4)), data_rect(1):(data_rect(1)+data_rect(3)))));

Qconvert = 3.3e-8;
Qtot = counts*Qconvert;

title(strcat('Eprofile. Q = ', num2str(Qtot), ' nC'));
him = hfig;

end