function [flatimage] =  L8x8bgflatten(ximage)
%suitable for any of the x-ray images with the 8x8 array. 
%This function normalises the selected filter regions of
%the image to the average of the surrounding background images. See lab
%book 2 29/08/13 for numbering details. Image coming in needs to be flipped
%upside down from the stored tif image.
%The input can be unit16 or double. The output is double.
%In this code where is says "background" it means "unfiltered signal", not
%some unwanted random noise.

%using 36 filters not 64.

%ximage = cast(inputimage, 'double');

filterload = load('20131126_filterpos.mat');
filterx = floor(filterload.filterx);
filtery = floor(filterload.filtery);

bgload = load('20131126_bgpos.mat');
bgx = floor(bgload.bgx_real);
bgy = floor(bgload.bgy_real);

%bgnload = load('bgnos_scint.mat');
bgnload = load('20131126_bgnos.mat');
bgn = bgnload.bgnos;

%this is a 16x4 matrix containing the bg region numbers associated with
%each filter (in order of number 1 to number 16). Some filters have <4 bg
%regions and thus have 0's in place of a bg number. Need to watch out for
%this.

for i=1:64
    
    fregion = ximage(filtery(2*i-1):filtery(2*i), filterx(2*i-1):filterx(2*i));
    %y is rows, x is columns
    
    bgvector = zeros(1,4);
    
    for j=1:4
        
        bgregionno = bgn(i,j);
        
        if bgregionno == 0
            bgvector(j) = 0;
        else
            bgregion = ximage(bgy(2*bgregionno-1):bgy(2*bgregionno), bgx(2*bgregionno-1):bgx(2*bgregionno));
            bgvector(j) = mean(mean(bgregion));
            %this is the mean counts per pixel in this bg region.
        end
        
    end
    
    nonzerobg = bgvector(bgvector>0);
    %want to ignore these because they don't correspond to an actual bg
    %region.
    
    fbg = mean(nonzerobg);
    %average bg level on bg regions surrounding this filter
    
    fregion = fregion./fbg;
    %normalises the filter region to the bg levels.
    
    ximage(filtery(2*i-1):filtery(2*i), filterx(2*i-1):filterx(2*i)) = fregion;
    %the ouput
    
end

flatimage = ximage;

end