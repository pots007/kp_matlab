function him = plotEspec(data, hfig)

data = double(data);

figure(hfig);
imagesc(data)
% load 'invfire.mat'
% colormap(iijcm);
% grid on;
colormap('Gray');
colorbar;
caxis([0 2000]);
axis image xy

Lrect = [116,209,366,58];
Lrectb = [107,265,462,15];

Lbg = mean(mean(data(Lrectb(2):(Lrectb(2)+Lrectb(4)), Lrectb(1):(Lrectb(1)+Lrectb(3)))));
data = data - Lbg;

Lcounts = sum(sum(data(Lrect(2):(Lrect(2)+Lrect(4)), Lrect(1):(Lrect(1)+Lrect(3)))));

Qconvert = 3.3e-8; %nC per lanex counts

Qtot = Lcounts*Qconvert;

%set(gca, 'CLim', [0 2000]);
title(strcat('Espec. Q = ', num2str(Qtot), ' nC'));
him = hfig;

end