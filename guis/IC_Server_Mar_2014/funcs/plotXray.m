function him = plotXray(data, hfig)
%refname = 'E:\2013GeminiNajmudin\Refs\';
%refname = [refname '20131125_princeton_ref.TIF'];
%referencename = 'P:\2013GeminiNajmudin\Princeton\20131204\xray_ref_2MHz_gain2.TIF';
%ref = imread(refname);
%ref = imread(referencename);
filtered_data = medfilt2(data);% - ref);
figure(hfig)
%subplot(2,1,1)

[ysize xsize] = size(filtered_data);
xaxis = 1:xsize;
xaxis = xaxis*2.16/1000;
yaxis = 1:ysize;
yaxis = yaxis*2.16/1000;
imagesc(xaxis, yaxis, filtered_data);
xlabel('x /mm')
ylabel('y /mm')
axis image

set(gca, 'Units', 'Normalized', 'Position', [0.05 0.05 0.9 0.9])

colorbar;
set(gca, 'CLim', [0 10000]);
%colormap(jet(256));
colormap(gray);
ncounts = sum(sum(data));%-sum(sum(ref));
%Ec = findEcfast8x8(data,ref);
title(num2str(ncounts/(1e9))); %', E_c = ' num2str(Ec) ' keV']);
% [N,X] = hist(data(:), linspace(1,2^16,2^10));
% subplot(2,1,2);
% plot(X,N);
% set(gca, 'XLim', [0 4000], 'YScale', 'linear');
him = hfig;
end