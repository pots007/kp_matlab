function him = plotEspec(data, hfig)

mm0 = 832;
mm200 = 229;
top = 461;
bot = 638;
rotation = -1.38;

dataorig = data;
data = double(data);
data = imrotate(data, rotation);

mmperpix = 200/(mm0 - mm200);
data = data(top:bot,1:mm0);
[ysize xsize] = size(data);
xaxis = 1:xsize;
xaxis = mmperpix*xaxis;
xaxis = fliplr(xaxis);
topfifth = round(0.2*ysize);
botfifth = round(0.8*ysize);
bgtop = mean(data(1:topfifth,:));
bgbot = mean(data(botfifth:end,:));
specdata = data(topfifth+1:botfifth-1,:);
[ysize xsize] = size(specdata);
for n = 1:length(bgtop)
   bg(:,n) = linspace(bgtop(n), bgbot(n),ysize); 
end
bgcorrected = specdata - bg;
counts = sum(bgcorrected);
load('ScreenCalibration.mat')
Eaxis = csaps(daxis1, Eaxis1, 1, xaxis);
MeVperpix = gradient(Eaxis);

figure(hfig)
subplot(2,1,1)
imagesc(data); axis image
title('Raw')
subplot(2,1,2)
imagesc(bgcorrected); axis image; caxis([0 500])
title('BG Subtracted')
colormap(hot)
figure(10*hfig)
smoothed = csaps(Eaxis, counts./MeVperpix, 5e-5, Eaxis);
plot(Eaxis,  counts./MeVperpix)
hold all
plot(Eaxis, smoothed, 'LineWidth', 4, 'LineStyle', '--')
hold off
title('Espec1 Spectrum')
xlabel('Energy /MeV')
ylabel('Counts per MeV /a.u')
ylim([0 2*max(smoothed)])
xlim([0 1500])

[~, inds] = findpeaks(smoothed);

for n = 1:length(inds)
    text(Eaxis(inds(n)), (smoothed(inds(n)) + 0.1*max(smoothed)), num2str(round(Eaxis(inds(n)))), 'BackgroundColor', [1 1 1], 'EdgeColor', [0 0 0])

end

him = hfig;
end