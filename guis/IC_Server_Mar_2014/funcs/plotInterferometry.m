function him = plotInterferometry(dataimage, hfig)

date = 20131017;
run = 2;
shot = 1;
datafolder = 'C:\Data\2013_ATA2_Pattathil\Data\';
%todayfolder = fullfile(datafolder, num2str(date));

%referencefile = fullfile(todayfolder, strcat('reference', num2str(date)));
referencefile = sprintf('%i\\%ir%03i\\%ir%03is%03i_interferometer.raw',...
    date, date, run, date, run, shot);
reference = double(ReadRAW16bit([datafolder referencefile], 1280, 960));

data = double(dataimage);

rotation = 2; % rot. on 15/10/2013
x = 22;
y = 425;
w = 880;
h = 280;
xfft = 373;
yfft = 124;
wfft = 47;
hfft = 34;

phimap = PhaseRetrieve(data, reference, rotation, x, y, w, h, xfft, yfft, wfft, hfft);

ymid = 159;
asmooth = 1e-2;
micperpix = 4.19;  % 3.2 till 15/10/2013
lambda = 800;
plotflag = 0;
npoints = 100;

rho = AbelInversion(phimap', ymid, asmooth, micperpix, lambda, plotflag, npoints);
%Plotting part
figure(hfig);
subplot(2,1,1);
xaxi = (1:1280)*4.19; %*3.16; before 15/10/2013
yaxi = (1:960)*4.19;
imagesc(xaxi, yaxi, fliplr(dataimage)); colorbar; axis xy;
subplot(2,1,2);
imagesc(rho); colorbar; axis xy;
him = hfig;

end


function rho = AbelInversion(Phi, ymid, asmooth, micperpix, lambda, plotflag, npoints)
%Phi = the phase from the last function (don't know why I changed the name) 
%ymid = pixel of symmetry axis, if zero it can be found automatically or 
%manually asmooth = the smoothing parameter for the spline fit micperpix = 
%self-explanatory lambda = wavelength in nm (probably not necessary as 
%800nm is hard-coded below) plotflag = set to 1 to plot the density at the 
%end - I may have messed this bit up npoints = how many points to sample 
%across the image - not tested so again may be a bit shit

Phi(isnan(Phi)) = 0;

if (ymid == 0)

   % Find symmetry axis automatically
   ymidautoflag = 0;

   if (ymidautoflag == 1)
       % Auto ymid finder
       [zsize ysize] = size(Phi);
       yaxis = 1:ysize;
       zpoints = 0.1:0.1:0.9;
       zpoints = zpoints*zsize;
       zpoints = round(zpoints);
       for n = 1:length(zpoints)
           yres(n) = trapz(Phi(zpoints(n),:).*yaxis)./trapz(Phi(zpoints(n),:));
       end
       ymid = round(mean(yres));
       imagesc(Phi)
       line([ymid ymid], [0 zsize], 'color', 'white')
       hold on
       scatter(yres, zpoints)
       hold off
       drawnow
       pause(5)
   else
       % Interactive ymid finder
       imagesc(Phi);%caxis([0 2*mean(mean(Phi))])
       [ymid zmid] = ginput(1);
       ymid = round(ymid);
       line([ymid ymid], [0 1e4], 'color', 'white'); drawnow
   end

end

ysize = length(Phi(1,:))-1;
zsize = length(Phi(:,1));
h = micperpix*1e-6;

lambda = lambda*1e-9;

%n=1+k(rho/rho_atm)
%rho_atm=(P/RT)*Na
rho_atm=(101.325e3*6.022e23)/(8.31*273); %units m^-3

%ncrit for 800nm in m^-3
k = rho_atm/(2*1748e24);

zindicies = linspace(1, zsize, npoints); zindicies = round(zindicies);

for z_counter = 1:npoints,

   z_index = zindicies(z_counter);
   Philine = Phi(z_index,:);
   Philine = Philine(ymid:ysize);
   Philineold = Philine;
   Philine = [fliplr(Philine) Philine];
   yaxis = 1:length(Philine);
   Philine = csaps(yaxis, Philine, asmooth, yaxis);
   Philine = Philine(end/2 + 1:end);
   yaxis = 1:length(Philine);

   % This plots the smoothed and unsmoothed phase for comparison
   %plot(Philineold, '.')
   %hold on
   %plot(Philine, 'red')
   %hold off

   dPhidy = -gradient(Philine,1);
   dPhidy = dPhidy/h;
   % Remove singularities at r = 0
   dPhidy(1) = 0.1*dPhidy(2);

   for r_index = 1:ysize-ymid,
       for y_index = r_index:ysize - ymid + 1
           y = (double(y_index) - 0.9)*h;
           r = (double(r_index) - 1.0)*h;
           integrand(y_index - r_index + 1) = dPhidy(y_index)*(y^2 - r^2)^-0.5;   
       end

       rho(z_counter, r_index) = ((lambda*rho_atm)/(2*pi^2*k))*h*trapz(integrand);
       integrand(end) = [];
   end

end

rho = [fliplr(rho) rho];
rho = imrotate(rho, -90);

yaxis = length(rho(:,1));
yaxis = 1:yaxis;
yaxis = yaxis*h*1e3;
zaxis = zindicies*h*1e3;

if (plotflag == 1)
   %pcolor(zaxis, yaxis, rho)
   size(yaxis)
   size(zaxis)
   size(rho)
   pcolor(zaxis, yaxis, rho); shading flat
   xlabel('z /mm')
   ylabel('y /mm')
   axis image xy
end

end


function phase = PhaseRetrieve(datafile, intref, rotation, x, y, w, h, xfft, yfft, wfft, hfft)
%x, y, w, h defines the region of the input image that we will work with.
 
% data = double(imread(datafile));
data = double(datafile);
 
% if (intref == 0)
%     intref = zeros(size(data));
% else
%     intref = double(imread(intref));
% end
% %reference image
 
data = imrotate(data, rotation);
intref = imrotate(intref, rotation);
 
data = data(y:y+h, x:x+w);
intref = intref(y:y+h, x:x+w);
 
fftim = fft2(data);
fftref = fft2(intref);
 
if (max([xfft yfft wfft hfft]) == 0)
    imagesc(log(abs(fftshift(fftim))))
    rect = getrect;
    xfft = round(rect(1))
    yfft = round(rect(2))
    wfft = round(rect(3))
    hfft = round(rect(4))
end
%this if statement gives you the option of manually choosing the area of
%interest in the fft'd image if you input [xfft yfft wfft hfft] = [0 0 0
%0].
 
fftim = fftshift(fftim);
fftref = fftshift(fftref);
newfftim = zeros(size(data));
newfftref = zeros(size(data));
newfftim(yfft:yfft+hfft, xfft:xfft+wfft) = fftim(yfft:yfft+hfft, xfft:xfft+wfft);
newfftref(yfft:yfft+hfft, xfft:xfft+wfft) = fftref(yfft:yfft+hfft, xfft:xfft+wfft);
newfftim = fftshift(newfftim);
newfftref = fftshift(newfftref);
 
datafiltered = ifft2(newfftim);
reffiltered = ifft2(newfftref);
 
if(sum(sum(isnan(reffiltered))) > 0)
    reffiltered = ones(size(datafiltered));
end
 
phase = angle(datafiltered./reffiltered);
mag = abs(datafiltered./reffiltered);
phase = unwrap(phase, 5.5);
 
% Here you can spline-smooth the phase, make 'smoothing' smaller for more smoothing
smoothing = 1;
[ysize xsize] = size(phase);
xsize = 1:xsize;
ysize = 1:ysize;
x = {ysize,xsize};
[phase,p] = csaps(x,phase,smoothing,x);
 
imagesc(phase)
 
end
