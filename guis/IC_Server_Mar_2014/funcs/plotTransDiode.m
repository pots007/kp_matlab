function him = plotTransDiode(data, hfig)
figure(hfig);
plot(data(:,1), data(:,2));
set(gca, 'XLim', [-5e-8 5e-8]);
ylabel('Voltage (V)')
xlabel('Time (s)');
maxvol = max(data(:,2));
%Calibration from 20130926 diode calibration shots
%en = 0.0568*maxvol^2 - 0.00738*maxvol + 0.0955;
en = 4.1036*maxvol + 0.0835;
title([num2str(maxvol) ' V']);
him = hfig;