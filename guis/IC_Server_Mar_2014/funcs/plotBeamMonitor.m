function him = plotBeamMonitor(data, hfig)

data = double(data);


figure(hfig);
imagesc(data)
% load 'invfire.mat'
% colormap(iijcm);
% grid on;
colormap('Gray');
colorbar;
axis image xy
set(gca, 'CLim', [0 3000]);

title('Beam Monitor');
him = hfig;

end