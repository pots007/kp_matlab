function Ecrit_best = findEcfast8x8(data, reference)

E = 0.5:0.5:500;
Ecrit = 10:1:100;

xoffset = -10;
yoffset = -10;
%These are in case the filter array moves w.r.t. the camera. Please put in
%number of pixels as an integer.

%------------ LOAD FILTER REGIONS --------------
filterload = load('20131126_filterpos.mat');
filterx = floor(filterload.filterx) - xoffset;
filtery = floor(filterload.filtery) - yoffset;

bgload = load('20131126_bgpos.mat');
bgx = floor(bgload.bgx_real) - xoffset;
bgy = floor(bgload.bgy_real) - yoffset;

%------------ LOAD FILTER DATA -----------------

Sn_thickness = 5e-3;
Au_thickness = 1e-3;
Mo_thickness = 5e-3;
Bi_thickness = 4e-3;
W_thickness = 2.5e-3;
Pb_thickness = 4e-3;
V_thickness = 3e-4;
Gd_thickness = 5e-3;
Yb_thickness = 5e-3;
Zn_thickness = 5e-4;
Al_thickness = 50e-7;
%thickness of Al on aluminised Mylar
Al_thickness2 = 12e-4;
%Al foil to shield camera
Mylar_thickness = 6e-4;
%filter material thicknesses in cm
%not sure if Mylar and Al thicknesses are correct! This was for the
%aluminised mylar
Be_thickness = (0.38+0.25)*1e-1;
Ag_thickness = 4e-3;

Sn_density = 7.31;
Au_density = 19.32;
Mo_density = 10.22;
Bi_density = 9.747;
W_density = 19.30;
Pb_density = 11.35;
V_density = 6.11;
Gd_density = 7.9;
Yb_density = 6.73;
Zn_density = 7.133;
Al_density = 2.699;
Mylar_density = 1.38;
Be_density = 1.848;
Ag_density = 10.5;
%densities of filter materials in gcm^-2. Taken from the NIST website.

filter_attenuation_load = load('filter_attenuation2.mat');
%list of matrices with Column 1 = photon energy in MeV. Column 2 = mu/rho
%in cm^2g^-1. Named after the substance they apply to.
Sn_coef = filter_attenuation_load.Sn_attenuation;
Au_coef = filter_attenuation_load.Au_attenuation;
Mo_coef = filter_attenuation_load.Mo_attenuation;
Bi_coef = filter_attenuation_load.Bi_attenuation;
W_coef = filter_attenuation_load.W_attenuation;
Pb_coef = filter_attenuation_load.Pb_attenuation;
V_coef = filter_attenuation_load.V_attenuation;
Gd_coef = filter_attenuation_load.Gd_attenuation;
Yb_coef = filter_attenuation_load.Yb_attenuation;
Zn_coef = filter_attenuation_load.Zn_attenuation;
Al_coef = filter_attenuation_load.Al_attenuation;
Mylar_coef = filter_attenuation_load.Mylar_attenuation;
Ag_coef = filter_attenuation_load.Ag_attenuation;
%the mapping of elements to filter number can be seen in lab book 2
%26/11/13

Be_coef_load = load('BeTransmission.mat');
Be_coef = Be_coef_load.BeTransmission;

Sn_mu = Sn_density*interp1(Sn_coef(:,1)*1000, Sn_coef(:,2), E, 'linear', 'extrap');
Au_mu = Au_density*interp1(Au_coef(:,1)*1000, Au_coef(:,2), E, 'linear', 'extrap');
Mo_mu = Mo_density*interp1(Mo_coef(:,1)*1000, Mo_coef(:,2), E, 'linear', 'extrap');
Bi_mu = Bi_density*interp1(Bi_coef(:,1)*1000, Bi_coef(:,2), E, 'linear', 'extrap');
W_mu = W_density*interp1(W_coef(:,1)*1000, W_coef(:,2), E, 'linear', 'extrap');
Pb_mu = Pb_density*interp1(Pb_coef(:,1)*1000, Pb_coef(:,2), E, 'linear', 'extrap');
V_mu = V_density*interp1(V_coef(:,1)*1000, V_coef(:,2), E, 'linear', 'extrap');
Gd_mu = Gd_density*interp1(Gd_coef(:,1)*1000, Gd_coef(:,2), E, 'linear', 'extrap');
Yb_mu = Yb_density*interp1(Yb_coef(:,1)*1000, Yb_coef(:,2), E, 'linear', 'extrap');
Zn_mu = Zn_density*interp1(Zn_coef(:,1)*1000, Zn_coef(:,2), E, 'linear', 'extrap');
Al_mu = Al_density*interp1(Al_coef(:,1)*1000, Al_coef(:,2), E, 'linear', 'extrap');
Mylar_mu = Mylar_density*interp1(Mylar_coef(:,1)*1000, Mylar_coef(:,2), E, 'linear', 'extrap');
Be_mu = Be_density*interp1(Be_coef(:,1)*1000, Be_coef(:,2), E, 'linear', 'extrap');
Ag_mu = Ag_density*interp1(Ag_coef(:,1)*1000, Ag_coef(:,2), E, 'linear', 'extrap');

%(the factor of 1000 converts MeV to keV).
%Don't use 'spline' interpolation- it works really badly because of the
%discontinuity at the k-edge

base = exp(-2*Mylar_mu*Mylar_thickness).*exp(-2*Al_mu*Al_thickness).*exp(-Be_mu*Be_thickness).*exp(-2*Al_mu*Al_thickness2);
%base = exp(-2*Mylar_mu*Mylar_thickness).*exp(-2*Al_mu*Al_thickness).*exp(-Be_mu*Be_thickness).*exp(-4*Al_mu*Al_thickness2);
%only had the 2 layers of mylar that the filter pack was actually on in the
%way plus the Be window.

filter(1,:) = base.*exp(-Mo_mu*Mo_thickness);
filter(2,:) = base.*exp(-Sn_mu*Sn_thickness);
filter(3,:) = base.*exp(-Yb_mu*Yb_thickness);
filter(4,:) = base.*exp(-Au_mu*Au_thickness);
filter(5,:) = base.*exp(-Gd_mu*Gd_thickness);
filter(6,:) = filter(3,:);
filter(7,:) = filter(5,:);
filter(8,:) = filter(3,:);
filter(9,:) = filter(4,:);
filter(10,:) = base.*exp(-Bi_mu*Bi_thickness);
filter(11,:) = base.*exp(-Pb_mu*Pb_thickness);
filter(12,:) = filter(1,:);
filter(13,:) = filter(2,:);
filter(14,:) = base.*exp(-Zn_mu*Zn_thickness);
filter(15,:) = base.*exp(-Ag_mu*Ag_thickness);
filter(16,:) = filter(4,:);
filter(17,:) = filter(3,:);
filter(18,:) = filter(5,:);
filter(19,:) = filter(2,:);
filter(20,:) = filter(15,:);
filter(21,:) = filter(4,:);
filter(22,:) = filter(5,:);
filter(23,:) = base.*exp(-W_mu*W_thickness);
filter(24,:) = filter(3,:);
filter(25,:) = filter(23,:);
filter(26,:) = filter(14,:);
filter(27,:) = filter(23,:);
filter(28,:) = filter(5,:);
filter(29,:) = filter(11,:);
filter(30,:) = filter(2,:);
filter(31,:) = filter(1,:);
filter(32,:) = filter(10,:);
filter(33,:) = filter(1,:);
filter(34,:) = filter(2,:);
filter(35,:) = base.*exp(-V_mu*V_thickness);
filter(36,:) = filter(23,:);
filter(37,:) = filter(1,:);
filter(38,:) = filter(4,:);
filter(39,:) = filter(23,:);
filter(40,:) = filter(2,:);
filter(41,:) = filter(10,:);
filter(42,:) = filter(15,:);
filter(43,:) = filter(5,:);
filter(44,:) = filter(4,:);
filter(45,:) = filter(3,:);
filter(46,:) = filter(5,:);
filter(47,:) = filter(15,:);
filter(48,:) = filter(1,:);
filter(49,:) = base;
filter(50,:) = filter(11,:);
filter(51,:) = filter(3,:);
filter(52,:) = filter(2,:);
filter(53,:) = filter(23,:);
filter(54,:) = filter(10,:);
filter(55,:) = filter(35,:);
filter(56,:) = filter(4,:);
filter(57,:) = filter(23,:);
filter(58,:) = filter(2,:);
filter(59,:) = filter(1,:);
filter(60,:) = filter(4,:);
filter(61,:) = filter(11,:);
filter(62,:) = filter(1,:);
filter(63,:) = filter(23,:);
filter(64,:) = filter(5,:);

% usefilters = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 ...
%     24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 ...
%     47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64];

% usefilters = [1 2 3 4 5 6 7 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 ...
%     24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 ...
%     47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64];

usefilters = [20 21 22 26 27 28 29 30 31 34 35 36 37 38 39 40 42 43 44 45 46 ...
    47 48 50 51 52 53 54 55 56 58 59 60 61 62 63 64];


%---------- CAMERA RESPONSE ------------
load('Princeton_CsI.mat');
%Now use 'Princeton_CsI' for the princeton response function
Princeton = interp1(Princeton_CsI(:,1), Princeton_CsI(:,2), E);

%---------- OPEN IMAGE ------------
%ximage = double(flipud(data));
ximage = double(data - reference);

%---------- CORRECT FOR BREHMSTRAHLUNG BACKGROUND --------------------

bremregion_load = load('bremregion.mat');
brx = floor(bremregion_load.bremx); %-xoffset
bry = floor(bremregion_load.bremy); %-yoffset
brembg = mean(mean(ximage(bry(1):bry(2), brx(1):bry(2))));

ximage = ximage - brembg;

%-------------  CORRECT FOR NON UNIFORM CHIP ILLUMINATION ----------------
ximageflat = L8x8bgflatten(ximage);

%--------  NORMALISE FILTER SIGNAL TO THE AVERAGE ACROSS THE FILTERS  -----

for j=1:length(usefilters)
    jj = usefilters(j);
    fregion = ximageflat(filtery(2*jj-1):filtery(2*jj), filterx(2*jj-1):filterx(2*jj));
    normvector(j) = mean(mean(fregion));
    %mean counts on each filter
end

norm = mean(normvector);
%mean counts per filter

for k=1:length(normvector)
    Ndata(k) = normvector(k)/norm;
    %this normalises the mean counts on each filter to the average
    %across all of the filters
end
%this seems to be wrong in previous version. Not 100% sure why but it
%is right now.

%----- ESTIMATED COUNTS TO FIND ECRIT -------
Nguess = zeros(length(Ecrit),length(usefilters));
%This is matrix with a row for each Ecrit and a column for each filter
%(numbering found near start of this code) containing the expected
%number of counts through the filter.
chi2 = zeros(1,length(Ecrit));
for m=1:length(Ecrit)
    for fno=1:length(usefilters)
        fno2 = usefilters(fno);
        Y = E.*Princeton.*filter(fno2,:).*onaxis_sync_spectrum(E, Ecrit(m));
        Nguess(m,fno) = trapz(E, Y);
    end
    normguess = mean(Nguess(m,:));
    Nguess2(m,:) = Nguess(m,:)/normguess;
    %normalised to the average of each filter like the
    %Ndata is.
    chi2(m) = sum((Ndata - Nguess2(m,:)).^2);
    
end
minchi2 = min(chi2);
Ecrit_best = Ecrit(chi2==minchi2);
messed_up = isempty(Ecrit_best);
if messed_up == 1
    Ecrit_best = 0;
else
end

%Ecrit_best

% ----------- PLOTTING -------------

% figure;
% subplot(2,2,1), imagesc(ximageflat);
% axis xy image;
% colorbar; caxis([0 1000]); %caxis([0 3000]);
% %title(savefilename);
% for fcounter = 1:0.5*length(filterx)
%     rectangle('Position', [filterx(2*fcounter-1), filtery(2*fcounter-1), filterx(2*fcounter)-filterx(2*fcounter-1), filtery(2*fcounter)-filtery(2*fcounter-1)], 'EdgeColor', 'White');
% end
% for bcounter = 1:0.5*length(bgx)
%     rectangle('Position', [bgx(2*bcounter-1), bgy(2*bcounter-1), bgx(2*bcounter)-bgx(2*bcounter-1), bgy(2*bcounter)-bgy(2*bcounter-1)], 'EdgeColor', 'Black');
% end
% %this bit is only plotting so it won't really mess the results up!
% 
% subplot(2,2,2), scatter(Ecrit, log(chi2));
% title(strcat('Ecrit best = ', num2str(Ecrit_best), 'keV'));
% xlabel('Ecrit/keV');
% ylabel('ln(chi^2)');
% ylim([-3 2]);
% 
% Nguess_best = Nguess2(chi2==minchi2,:);
% 
% subplot(2,2,[3 4]),
% hold on
% plot(Nguess_best);
% %plot(Nguess_neworder);
% plot(Ndata, 'o');
% %errorbar(Ndata, error_tot1vector, 'o');
% title('Scatter = Data, Line = Estimate from Spectrum');
% hold off
% ------------------------------------------------------------

end