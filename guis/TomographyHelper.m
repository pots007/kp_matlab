% Helper widget to maintain sanity during tomography runs.
% Will prompt upon receival of UDP to confirm whether shot was good or not,
% if it was good will move on to next angle (or do as many more repeats at
% given angle as needed). Status is saved after each shot. Has a fancy pie
% chart to show progress, along with counter showing remaining shots.

classdef TomographyHelper < uiextras.HBox
    properties
        udp
        ax
        angles
        state
        startbutton
        thet_cont
        rotcontroller
    end
    
    events
        shot
    end
    
    methods
        function obj = TomographyHelper(parent,srcIP, rotcontrol)
            if isempty(parent)
                parent = figure;
            end;
            obj@uiextras.HBox('Parent', parent);
            %defer(obj);
            vbox_ = uiextras.VBox('Parent', obj);
            
            % The total number of angles, and reps at each angle
            hbox_ = uiextras.HBox('Parent', vbox_);
            hpan = uiextras.Panel('Parent', hbox_, 'Title', 'Projection setup', 'Padding', 5);
            vbox = uiextras.VBox('Parent', hpan);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Total angles:', 'Parent', hbox);
            obj.angles.totangles = javaNumSpinner2(180,1,1080, 1, hbox, 'StateChangedCallback', @(s,e)update(obj,0));
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Repeats at each angle:', 'Parent', hbox);
            obj.angles.reps = javaNumSpinner2(1,1,10,1,hbox, 'StateChangedCallback', @(s,e)update(obj,0));
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'Parent', hbox, 'String', 'Scan method:');
            obj.angles.method = uicontrol('Style', 'popupmenu', 'Parent', hbox,...
                'String', {'Binary', 'Random', 'Sequential'}, 'Callback', @(s,e)update(obj,1));
            hbox = uiextras.HBox('Parent', vbox);
            obj.angles.check = uicontrol('Style', 'checkbox', 'Parent', hbox,...
                'String', 'Check each angle for quality', 'Value', 1);
            obj.angles.manualoverride = 0;
            hbox = uiextras.HBox('Parent', vbox);
            obj.angles.settle = uicontrol('Style', 'togglebutton', 'Value', 1,...
                'String', 'Start tomography', 'Parent', hbox, ...
                'Callback', @(s,e)fix_angles(obj,get(obj.angles.settle, 'Value')));
            
            % Rotation controller
            hpan = uiextras.Panel('Parent', vbox_, 'Title', 'Rotation controller','Padding', 5);
            obj.rotcontroller = feval(rotcontrol, hpan);
            
            % IP and source of UDP source, and button to open udp
            hbox = uiextras.HBox('Parent', vbox_);
            hpan = uiextras.Panel('Parent', hbox, 'Title', 'UDP server settings', 'Padding', 5);
            vbox = uiextras.VBox('Parent', hpan);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Parent', hbox, 'String', 'UDP source:', 'Style', 'text')
            obj.udp.ipbox = uicontrol('Style', 'edit', 'String', srcIP,...
                'Parent', hbox);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('style', 'text', 'String', 'Port:', 'Parent', hbox);
            obj.udp.portbox = uicontrol('Style', 'edit', 'String', '25000', 'Parent', hbox);
            hbox = uiextras.HBox('Parent', vbox);
            obj.udp.button = uicontrol('Style', 'togglebutton', 'String', 'Start UDP listen', ...
                'Parent', hbox, 'Callback', @(s,e)open_udp(obj,get(obj.udp.button, 'Value')));
            hbox = uiextras.HBox('Parent', vbox);
            obj.udp.last = uicontrol('Style', 'text', 'Parent', hbox, 'String', ...
                'Last received GSN: 000000');
            
            % Info on last angle, next angle and button to do manual angle
            hbox = uiextras.HBox('Parent', vbox_);
            hpan = uiextras.Panel('Parent', hbox, 'Title', 'Information and control', 'Padding', 5);
            vbox = uiextras.VBox('Parent', hpan);
            hbox = uiextras.HBox('Parent', vbox);
            obj.state.lastAngle = uicontrol('style', 'text', 'String', 'Last angle:', 'Parent', hbox);
            hbox = uiextras.HBox('Parent', vbox);
            obj.state.nextAngle = uicontrol('style', 'text', 'String', 'Next angle:', 'Parent', hbox);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'pushbutton', 'String', 'Manual advance', 'Parent', hbox,...
                'Callback', @(s,e)manual_angle(obj,1));
            vbox_.Sizes = [200 200 150 150];
            
            hbox = uiextras.HBox('Parent', obj);
            hpan = uipanel('Parent', hbox, 'Title', 'Status');
            obj.ax = axes('Parent', hpan);
            obj.Sizes = [300 -1];
            colormap([1 0 0; 0 1 0]);
            %resume(obj);
            obj.update(1);
            addlistener(obj, 'shot', @(s,e)set_angle(obj,1));
        end
        
        function fix_angles(obj,v)
            % Utility function to disable editing projection setup once
            % done. Also, rotates the controller to 1st angle position.
            set(obj.angles.reps, 'Enabled', v);
            set(obj.angles.totangles, 'Enabled', v);
            if v
                set(obj.angles.settle, 'String', 'Start tomography');
                set(obj.angles.method, 'Enable', 'on');
            else
                set(obj.angles.settle, 'String', 'Change projection data');
                set(obj.angles.method, 'Enable', 'off');
            end
            obj.update(1);
            obj.rotcontroller.moveAbs(obj.angles.next);
            set(obj.state.nextAngle, 'String', sprintf('Current angle: %2.3f,%i', obj.angles.next, 1));
        end
        
        function open_udp(obj,v)
            obj.udp.obj = dsp.UDPReceiver('LocalIPPort', str2double(get(obj.udp.portbox, 'String')),...
                    'RemoteIPAddress', get(obj.udp.ipbox, 'String'));
            obj.state.GSN = 000;
            if ~isfield(obj.udp, 'times')
                obj.udp.times = timer('TimerFcn', @(s,e)timer_fun(obj), 'Period', 1,...
                'ExecutionMode', 'fixedRate', 'StartFcn', @(s,e)running_set(obj,true),...
                'StopFcn',@(s,e)running_set(obj,false));        
            end
            if v
                start(obj.udp.times);
            else
                stop(obj.udp.times);
            end
        end
        
        function timer_fun(obj)
            dataReceived = step(obj.udp.obj);
            if isempty(dataReceived)
                return;
            else
                ttemp = char(dataReceived)';
                obj.state.GSN = sscanf(ttemp, '%i');
                set(obj.udp.last, 'String', sprintf('Last received GSN: %i', obj.state.GSN));
                notify(obj, 'shot');
            end
        end
        
        function running_set(obj,v)
            obj.udp.running=v;
            set(obj.udp.button,'Value',v);
            if v
                set(obj.udp.button,'BackgroundColor', [0 1 0], 'String', 'STOP UDP listen')
            else
                set(obj.udp.button,'BackgroundColor', [1 0 0], 'String', 'START UDP listen')
            end
        end
        
        function update(obj,~)
            reps = get(obj.angles.reps, 'Value');
            r1 = 10-(1:1:reps); r2 = 9-(1:1:reps);
            thet = linspace(0,180,get(obj.angles.totangles, 'Value'));
            dt = 0.5*(thet(2)-thet(1));
            obj.angles.thet_plot = thet; %linspace(0,180,get(obj.angles.totangles, 'Value'));
            patchx = zeros(length(thet),4);
            patchy = zeros(length(thet),4);
            cla(obj.ax);
            for k=1:reps
                for i=1:length(thet)
                    %patchx(i,:) = [r2(k)*cosd(thet(i-1)) r2(k)*cosd(thet(i)) r1(k)*cosd(thet(i)) r1(k)*cosd(thet(i-1))];
                    %patchy(i,:) = [r2(k)*sind(thet(i-1)) r2(k)*sind(thet(i)) r1(k)*sind(thet(i)) r1(k)*sind(thet(i-1))];
                    patchx(i,:) = [r2(k)*cosd(thet(i)-dt) r2(k)*cosd(thet(i)+dt) r1(k)*cosd(thet(i)+dt) r1(k)*cosd(thet(i)-dt)];
                    patchy(i,:) = [r2(k)*sind(thet(i)-dt) r2(k)*sind(thet(i)+dt) r1(k)*sind(thet(i)+dt) r1(k)*sind(thet(i)-dt)];
                end
                obj.angles.hh(k) = patch(patchx', patchy', 'red', 'Parent', obj.ax);
            end
            set(obj.ax, 'XTick', [], 'YTick', [], 'Box', 'on');
            obj.get_order_vector(1);
            obj.angles.next = obj.state.thetas(1,1);
            obj.angles.rep = 1;
        end
        
        function tempvec = get_order_vector(obj,~)
            reps = get(obj.angles.reps, 'Value');
            Nthet = get(obj.angles.totangles, 'Value');
            thets = linspace(0,180,Nthet);
            opt = get(obj.angles.method, 'Value');
            switch opt
                case 1 %Binary
                    if isinteger(log2(Nthet))
                        % Do a proper binary
                        order2 = zeros(size(thets));
                        dth = thets(2) - thets(1);
                        order2(1) = 0;
                        order2(2) = thets(end);
                        it = 3;
                        for i=nextpow2(Nthet):-1:0
                            dloc = dth*2^i;
                            temp = 0:dloc:180;
                            for k=1:length(temp)
                                if sum(order2==temp(k))==0
                                    if sum(thets==temp(k)) == 0; continue; end;
                                    order2(it) = temp(k);
                                    it = it+1;
                                end
                            end
                        end
                        tempvec = order2;
                    else
                        % a sloppy one
                        order3 = zeros(size(thets));
                        order3(1) = 1;
                        order3(2) = Nthet;
                        it = 3;
                        for i=1:nextpow2(Nthet)
                            temp = round(linspace(0,Nthet,2^i));
                            for k=1:length(temp)
                                if (temp(k)==order3)==0
                                    order3(it) = temp(k);
                                    it = it+1;
                                end
                            end
                        end
                        tempvec = thets(order3);
                    end
                case 2 %Random
                    tempvec = thets(randperm(Nthet));
                case 3 %Sequential
                    tempvec = linspace(0,180,Nthet);
            end
            obj.state.thetas = repmat(tempvec,reps,1);
            obj.state.comp = zeros(reps,length(tempvec)); % Array of completed angles
        end
        
        function manual_angle(obj,~)
            % We missed the UDP, so ask for angle input. Put in previous
            % GSN + 1 for default answer
            obj.angles.manualoverride = 1;
            if isfield(obj.state, 'GSN')
                GSN = obj.state.GSN;
            else
                GSN = 0;
            end
            anss = inputdlg('GSN for this angle:', 'Enter Gemini Shot Number', 1, {num2str(GSN+1)});
            if isempty(anss); return; end;
            newGSN = sscanf(anss{1}, '%i');
            if isempty(newGSN); disp('poo'); return; end;
            obj.state.GSN = newGSN;
            notify(obj, 'shot');
        end
        
        function set_angle(obj,~)
            % When the server says we've had a shot, ask the user whether
            % he's happy with it. If yes, go to next angle. Update the log
            % in any case.
            % We got a shot, so ask what we want to do
            if get(obj.angles.check, 'Value') && ~obj.angles.manualoverride
                anss = questdlg('Was the shot OK?', 'Shot confirmation', 'No', 'Yes', 'Yes');
            else
                anss = 'Yes'; % A bit cheeky, but should work!
            end
            if strcmp(anss, 'No')
                obj.write_log(-1);
                return;
            end;
            % First, write log. 
            obj.write_log(obj.angles.rep);
            % Now color that angle's patch green
            rep = obj.angles.rep;
            ind_plot = find(obj.angles.thet_plot==obj.angles.next);
            obj.state.comp(rep,ind_plot) = 1;
            set(obj.angles.hh(rep), 'FaceColor', 'flat', 'CData', obj.state.comp(rep,:)+1);
            % And update status boxes and set next angle.
            set(obj.state.lastAngle, 'String', sprintf('Last angle: %2.3f,%i', obj.angles.next, rep));
            % If all angles for this rep are done, move on to next!
            if sum(obj.state.comp(rep,:))==size(obj.state.comp,2)
                if sum(obj.state.comp(:))==numel(obj.state.comp) % We're all done!
                    disp('We''re done!!!!');
                    obj.open_udp(0);
                    helpdlg('Tomography done!', 'Done.');
                    return;
                end
                obj.angles.rep = rep+1;
                rep = rep+1;
                obj.angles.next = obj.state.thetas(rep,1);
            else
                ind_thet = find(obj.state.thetas(rep,:)==obj.angles.next);
                obj.state.thetas(rep,ind_thet+1)
                obj.angles.next = obj.state.thetas(rep,ind_thet+1);
            end
            set(obj.state.nextAngle, 'String', sprintf('Current angle: %2.3f,%i', obj.angles.next, rep));
            obj.rotcontroller.moveAbs(obj.angles.next);
            obj.angles.manualoverride = 0;
            
        end
        
        function write_log(obj, rep)
            filen = [getuserdir '\Tomography_log.txt'];
            fid = fopen(filen, 'A');
            dates = datestr(clock, 'DD-mm-YYYY');
            times = datestr(clock, 'HH:MM:SS');
            fprintf(fid,'%i,%s,%s,%.5f,%i\n\r', obj.state.GSN, dates, times, obj.angles.next, rep); 
            fclose(fid);
        end
    end  
end