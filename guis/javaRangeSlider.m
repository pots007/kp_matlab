function h = javaRangeSlider(min,max, low, high, Parent, varargin)

if ~exist('Parent','var')
    Parent=gcf;
end

% Initialize JIDE's usage within Matlab
com.mathworks.mwswing.MJUtilities.initJIDE;

jRangeSlider = com.jidesoft.swing.RangeSlider(min,max,low,high);  % min,max,low,high

if feature('HGUsingMATLABClasses')
    [h,c] = javacomponent(jRangeSlider, [10,10,60,20], (Parent));
else
    [h,c] = javacomponent(jRangeSlider, [10,10,60,20], double(Parent));
end
set(h,varargin{:});
set(c,'Tag','javax.swing.RangeSLider','UserData',h);