interface = visa('tek', 'USB::0x0699::0x0401::C021944::INSTR');
d = icdevice('dpo4054', interface);

% Connect to the device
connect(d);

% Get information about the instrument
d;

% Configure the trigger slope
set(d.Trigger(1), 'Slope', 'rising');
set(d.Acquisition, 'Timebase', 1e-2,  'Delay', -.01);
set(d.Channel(1), 'Position', -4);
% Get more information about the device object Waveform group
groupObj = get(d, 'Waveform')

% Get the waveform data from channel 1 of the scope
[y,x] = invoke(groupObj, 'readwaveform', 'Channel1');

%Generate a plot
plot(x,y);
xlabel('Time (s)'); ylabel('Voltage (V)');
grid on;
