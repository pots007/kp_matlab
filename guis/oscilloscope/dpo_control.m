% Kristjan Poder, IC, 2013
% GUI to get data from a Tektronic scope and save it on shot.
% Works with DPO4054, DPO4034
% Known bugs: Matlab fails to change the vertical division or the offset.

%Last modification 25/09/2013: Made each channel save into a separate folder for ease of the server

function hobj = dpo_control()
% Pass data back and forth...
hObject = figure('units','pixels',...
    'position',[150 150 800 600],... %[left bottom width height]
    'menubar','none',...
    'numbertitle','off',...
    'name','dpo control',...
    'Tag', 'dpo_figure',...
    'resize','off');
S.ax1 = axes('Parent',hObject, 'units', 'pix', 'Position',[50 250 450 300]);
S.startbutton = uicontrol('style','pushbutton',...
    'units','pix',...
    'position',[10 180 180 30],...
    'string','START GRAB',...
    'callback',{@startstop_call});
%Triggering
%Trigger channel
S.trigchan = uicontrol('Parent',hObject,'style','text',...
    'position',[10 150 130 20],...
    'string','Trigger channel');
S.trigchanval = uicontrol('Parent',hObject,'style','edit',...
    'position',[150 150 30 20],...
    'string','1', 'BackGroundColor', [1 1 1]);
%Trigger level
S.triglevel = uicontrol('Parent',hObject,'style','text',...
    'position',[10 120 130 20],...
    'string','Trigger level');
S.triglevelval = uicontrol('Parent',hObject,'style','edit',...
    'position',[150 120 30 20],...
    'string','1.0', 'BackGroundColor', [1 1 1]);
%Time base = division on screen
S.timebase = uicontrol('Parent',hObject,'style','text',...
    'position',[10 90 130 20],...
    'string','Timebase');
S.timebaseval = uicontrol('Parent',hObject,'style','edit',...
    'position',[150 90 30 20],...
    'string','1e-7', 'BackGroundColor', [1 1 1]);
%Trigger location timewise
S.delay = uicontrol('Parent',hObject,'style','text',...
    'position',[10 60 130 20],...
    'string','Trigger delay');
S.delayval = uicontrol('Parent',hObject,'style','edit',...
    'position',[150 60 30 20],...
    'string','0.0', 'BackGroundColor', [1 1 1]);

%Channel stuff
colors = [1,0,0; 0,1,0; 0,0.5,1; 0.5,0.5,0.5];
for i=1:4
    tickbox = ['tickbox' num2str(i)];
    offset = ['offset' num2str(i)];
    offsetval = ['offsetval' num2str(i)];
    scale = ['scale' num2str(i)]; %[left bottom width height]
    scaleval = ['scaleval' num2str(i)];
    S.(tickbox) = uicontrol('Parent',hObject,'style','checkbox',...
        'position',[210+(i-1)*150 190 130 20],...
        'string',['Channel ' num2str(i)],...
        'BackgroundColor', colors(i,:));
    %Time base = division on screen
    S.(offset) = uicontrol('Parent',hObject,'style','text',...
        'position',[210+(i-1)*150 160 90 20],...
        'string','Offset');
    S.(offsetval) = uicontrol('Parent',hObject,'style','edit',...
        'position',[310+(i-1)*150 160 30 20],...
        'string','0.1', 'BackGroundColor', [1 1 1]);
    S.(scale) = uicontrol('Parent',hObject,'style','text',...
        'position',[210+(i-1)*150 130 90 20],...
        'string','Scale');
    S.(scaleval) = uicontrol('Parent',hObject,'style','edit',...
        'position',[310+(i-1)*150 130 30 20],...
        'string','0.5', 'BackGroundColor', [1 1 1]);
    
    %The stats bit
    maxbox = ['maxbox' num2str(i)];
    minbox = ['minbox' num2str(i)];
    chanbox = ['maxbox' num2str(i)];
    S.(chanbox) = uicontrol('Parent',hObject,'style','text',...
        'position',[530 420-(i-1)*30 70 20],...
        'string',['Channel ' num2str(i)],...
        'BackGroundColor', colors(i,:));
    S.(maxbox) = uicontrol('Parent',hObject,'style','text',...
        'position',[620 420-(i-1)*30 70 20],...
        'string','0 V');
    S.(minbox) = uicontrol('Parent',hObject,'style','text',...
        'position',[710 420-(i-1)*30 70 20],...
        'string','0 V')
end

%File saving stuff
S.pathbox = uicontrol('Parent',hObject,'style','text',...
    'position',[530 550 130 20],...
    'string','Save path:');
S.path = uicontrol('Parent',hObject,'style','edit',...
    'position',[530 520 250 20],...
    'string','C:\Data\2013_ATA2_Pattathil\inputenergy',...
    'BackGroundColor', [1 1 1]);
S.filestembox = uicontrol('Parent',hObject,'style','text',...
    'position',[530 490 130 20],...
    'string','Filename:');
S.filename = uicontrol('Parent',hObject,'style','edit',...
    'position',[660 490 130 20],...
    'string','shot', 'BackGroundColor', [1 1 1]);
S.save = uicontrol('Parent',hObject,'style','checkbox',...
    'position',[660 550 130 20],...
    'string','SAVE');

set(S.tickbox1, 'Value', true);
S.acn = false;
guidata(hObject,S);
hobj = hObject;




%Connect to the scope
% interface = visa('tek', 'USB::0x0699::0x0401::C021889::INSTR');
% S.d = icdevice('dpo4054', interface);
% connect(d);
% guidata(hObject,S);

function [] = startstop_call(hObject, ~)
S = guidata(hObject);
curr = get(S.startbutton, 'String');
if (strcmp(curr, 'START GRAB') == 1)
    set(S.startbutton, 'String', 'STOP GRAB');
    set(S.startbutton, 'BackgroundColor', [0. 1. 0.]);
    S.acn = true;
    guidata(hObject, S);
    takedata(hObject);
end
if (strcmp(curr, 'STOP GRAB') == 1)
    set(S.startbutton, 'String', 'START GRAB');
    set(S.startbutton, 'BackgroundColor', [0.941 0.941 0.941]);
    S.acn = false;
    guidata(hObject, S);
end


function takedata(hObject)
S = guidata(hObject)
interface = visa('tek', 'USB::0x0699::0x0401::C021944::INSTR');
d = icdevice('dpo4054', interface);
connect(d);
groupObj = get(d, 'Waveform');
tic;
colors = [1,0,0; 0,1,0; 0,0.5,1; 0.5,0.5,0.5];
data = cell(2,4);
while (S.acn)
    S = guidata(hObject);
    set(d.Acquisition, 'State', 'run');
    set(d.Acquisition, 'Control', 'single');
    %set(d.Acquisition, 'Timebase', 1e-2,  'Delay', -.01);
    set(d.Acquisition, 'Timebase', str2double(get(S.timebaseval, 'String')),...
        'Delay', str2double(get(S.delayval, 'String')));
    set(d.Channel(1), 'Position', -2);
    triglev = get(S.triglevelval, 'String');
    chan = get(S.trigchanval, 'String');
    set(d.Trigger, 'Source', ['channel' chan],...
        'Slope', 'rising', ...
        'Level', str2double(triglev));
    no_acq = get(d.Acquisition, 'AcquisitionCount');
    if (no_acq == 0)
        continue;
    end
    % Get the waveform data from channels of the scope
    axes(S.ax1)
    cla;
    for i=1:4
        if (~get(S.(['tickbox' num2str(i)]), 'Value'))
            continue;
        end
        [y,x] = invoke(groupObj, 'readwaveform', ['Channel' num2str(i)]);
        data{1,i} = x;
        data{2,i} = y;
        hold on;
        plot(x,y, 'Color', colors(i,:));
        hold off;
        grid on;
        drawnow;
        set(S.(['minbox' num2str(i)]), 'String', ...
            [num2str(min(y)) ' V']);
        set(S.(['maxbox' num2str(i)]), 'String', ...
            [num2str(max(y)) ' V']);
    end
    xlim([-5e-7 5e-7]);
    
    %     if (get(S.tickbox1, 'Value'))
    %         [y1,x1] = invoke(groupObj, 'readwaveform', 'Channel1');
    %         data{1,1} = x1;
    %         data{2,1} = y1;
    %         plot(x1,y1, 'Color', colors(1,:));
    %     end
    %     if (get(S.tickbox2, 'Value'))
    %         [y2,x2] = invoke(groupObj, 'readwaveform', 'Channel2');
    %         data{1,2} = x2;
    %         data{2,2} = y2;
    %         hold on;
    %         plot(x2,y2, 'Color', colors(2,:));
    %         hold off;
    %     end
    %     if (get(S.tickbox3, 'Value'))
    %         [y3,x3] = invoke(groupObj, 'readwaveform', 'Channel3');
    %         data{1,3} = x3;
    %         data{2,3} = y3;
    %         hold on;
    %         plot(x3,y3, 'Color', colors(3,:));
    %         hold off;
    %     end
    %     if (get(S.tickbox4, 'Value'))
    %         [y4,x4] = invoke(groupObj, 'readwaveform', 'Channel4');
    %         data{1,4} = x4;
    %         data{2,4} = y4;
    %         hold on;
    %         plot(x4,y4, 'Color', colors(4,:));
    %         hold off;
    %     end
    if (get(S.save, 'Value'))
        disp('save');
        updateshot(hObject);
        savedata(hObject, data);
    end
    xlabel('Time (s)'); ylabel('Voltage (V)');
    
    disp ([' time taken ' num2str(toc)]);
end

function savedata(hObject, data)
S = guidata(hObject);
savpath = get(S.path, 'String');
stem = get(S.filename, 'String');
shotno = importdata('lastshot.txt');
for i=1:4
    if (isempty(data{1,i}) || isempty(data{2,i}))
        continue;
    end
    if (~exist([savpath '\channel' num2str(i)], 'dir'))
        mkdir([savpath '\channel' num2str(i)]);
    end
    filen = [savpath '\channel' num2str(i) '\' stem num2str(shotno) '.txt'];
    dat = [data{1,i}; data{2,i}]';
    save(filen, 'dat', '-ASCII');
end

function updateshot(hObject)
lastshot = importdata('lastshot.txt');
fid = fopen('lastshot.txt', 'w');
fprintf(fid, '%i', lastshot+1);
fclose(fid);
