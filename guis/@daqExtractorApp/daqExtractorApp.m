% daqExtractorApp.m
%
% This is GUI designed to simplify saving data saved in the DAQ onto disk.
% It allows the user to check for data saved in the DAQ, then choose which
% channels need to be written to disk. Aliases for different obscure DOOCS
% channel are supported (ie BOND.SHUTTER/AIN.1 -> GasJetPressure etc). Data
% can be saved either with the FLASH event IDs (into individual files, an hdf5
% asdile or a mat file) or into a
% date/run/diag/shotnumberinrun format. Saving and loading customised
% channel lists is supported and should make repetitive data extraction
% strightforward. A section to view comments about a particular run is
% envisaged, but requires something to save such data to be implemented first.
%
% Uses uiextras class from UILayout toolbox for nice graphics. As such,
% user must first navigate to /home/fflab28m/Matlab/kris/kp_matlab and run
% the command make_path_Kris
%
% KP, DESY, June 2018
%
% v1.4: Added 10Hz constant support (20190606)
% v1.5: Added option to have shot/diagnostic based saving into h5 (20190606)
% v1.6: Updated 10Hz constants saving logic (20190719)
% v1.7: Added 60s block poll to PID save option (20190823)

classdef daqExtractorApp < handle
    properties
        fig             % Structure that holds all GUI components
        dat             % Structure that holds all up-to-date data
        table           % uitable that holds the main data
        statbar         % Status bar to update user about progress
        saveNames = []  % GUI to hold table of aliases for saving
        constManager = []
    end
    
    methods
        %% GUI creation
        function self = daqExtractorApp
            % Class and GUI constructor
            self.fig.parent = figure(987789); clf(self.fig.parent);
            % Set consistent font sizes and make it look nice
            if (ismac)
                self.dat.fsize = 13;
            else
                self.dat.fsize = 9;
            end
            set(self.fig.parent, 'Toolbar', 'none', 'MenuBar', 'none',...
                'Name', 'DAQ data extractor app', 'NumberTitle', 'off',...
                'Units', 'Pixels', 'Position', [300 200 850 850],...
                'DefaultUIControlFontSize', self.dat.fsize, 'Color', 'w',...
                'HandleVisibility', 'callback', 'CloseRequestFcn', @self.delete);
            
            % Start  of GUI code
            vboxMain = uiextras.VBox('Parent', self.fig.parent);
            
            % ------------ Run controls
            hpanControl = uiextras.Panel('Parent', vboxMain, 'Title', 'Control', 'Padding', 3);
            vboxControl = uiextras.VBox('Parent', hpanControl);
            hbox = uiextras.HBox('Parent', vboxControl);
            uicontrol('Style', 'text', 'String', 'Run #:', 'Parent', hbox);
            self.fig.uiRunNumber = uicontrol('Style', 'edit', 'String', '22607',...
                'Parent', hbox, 'Tag', 'RunNumber');%, 'Callback', @self.updateData);
            uicontrol('Style', 'text', 'String', 'Exp:', 'Parent', hbox);
            self.fig.uiExp = uicontrol('Style', 'edit', 'String', 'flashfwd',...
                'Parent', hbox, 'Tag', 'Exp', 'Enable', 'off');%, 'Callback', @self.updateData);
            uicontrol('Style', 'text', 'String', 'DDir:', 'Parent', hbox);
            self.fig.uiDDir = uicontrol('Style', 'edit', 'String', '/daq/data_data/flashfwd',...
                'Parent', hbox, 'Tag', 'DDir', 'Enable', 'off');%, 'Callback', @self.updateData);
            self.fig.getAvail = uicontrol('Style', 'PushButton', 'String', 'Find available data',...
                'Parent', hbox, 'Callback', @self.listAvailableData);
            hbox.Sizes = [50 100 50 100 50 -1 -1];
            hbox = uiextras.HBox('Parent', vboxControl);
            uicontrol('Style', 'text', 'String', 'Start time:', 'Parent', hbox);
            self.fig.uiStartTime = uicontrol('Style', 'edit', 'String', '--',...
                'Parent', hbox, 'Tag', 'StartTime', 'Callback', @self.updateRunTimes);
            uicontrol('Style', 'text', 'String', 'Stop time:', 'Parent', hbox);
            self.fig.uiEndTime = uicontrol('Style', 'edit', 'String', '--',...
                'Parent', hbox, 'Tag', 'EndTime', 'Callback', @self.updateRunTimes);
            hbox.Sizes = [75 200 75 200];
            
            % -------------------- Setup controls
            hpanSetups = uiextras.Panel('Parent', vboxControl, 'Title', 'Setup controls', 'Padding', 3);
            hbox = uiextras.HBox('Parent', hpanSetups);
            uicontrol('Style', 'text', 'String', 'Saved setups:', 'Parent', hbox);
            self.fig.setupList = uicontrol('Style', 'popupmenu', 'String', {'Last session'},...
                'Parent', hbox);
            self.fig.loadSetup = uicontrol('Style', 'PushButton', 'String', 'Load',...
                'Parent', hbox, 'Callback', @self.loadSetup);
            self.fig.saveSetup = uicontrol('Style', 'PushButton', 'Parent', hbox,...
                'String', 'Save current conf to file', 'Callback', @self.saveSetup);
            
            % ------------------ Extraction settings
            hpanExtr = uiextras.Panel('Parent', vboxControl, 'Title', 'Extraction settings', 'Padding', 3);
            hboxL = uiextras.HBox('Parent', hpanExtr);
            vbox = uiextras.VBox('Parent', hboxL);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Output directory:', 'Parent', hbox);
            self.fig.uiSaveDir = uicontrol('Style', 'edit', 'Parent', hbox,...
                'String', '/home/fflab28m-data/Experiments/', 'Tag', 'SaveDir');
            uicontrol('Style', 'PushButton', 'String', 'Find', 'Parent', hbox, ...
                'Tag', 'SaveDir', 'Callback', @self.findPath);
            hbox.Sizes = [130 -1 50];
            
            hbox = uiextras.HBox('Parent', vbox);
            self.fig.extControl = uibuttongroup('Parent', hbox, 'BorderType', 'None');
            uicontrol('Style', 'radiobutton', 'Parent', self.fig.extControl,...
                'String', 'Extract with FLASH event IDs', 'Position', [1 1 200 30],...
                'Tag', '1', 'CallBack', @self.changeOutputType);
            uicontrol('Style', 'radiobutton', 'Parent', self.fig.extControl,...
                'String', 'Extract into run/shot structure', 'Position', [201 1 200 30],...
                'Tag', '2', 'CallBack', @self.changeOutputType);
            uicontrol('Style', 'radiobutton', 'Parent', self.fig.extControl,...
                'String', 'Save into hdf5', 'Position', [401 1 110 30],...
                'Tag', '3', 'CallBack', @self.changeOutputType);
            uicontrol('Style', 'radiobutton', 'Parent', self.fig.extControl,...
                'String', 'Save into mat', 'Position', [511 1 120 30],...
                'Tag', '4', 'CallBack', @self.changeOutputType);
            
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'text', 'String', 'Output filename:', 'Parent', hbox);
            self.fig.uiSaveName = uicontrol('Style', 'edit', 'String', 'run', ...
                'Parent', hbox, 'Enable', 'off');
            % Ability to change to shot/diagnostic or diagnostic/shot for h5
            self.fig.extFrmtControl = uibuttongroup('Parent', hbox, 'BorderType', 'None');
            uicontrol('Style', 'radiobutton', 'Parent', self.fig.extFrmtControl,...
                'String', 'diagnostic/shot', 'Position', [1 1 120 30],...
                'Tag', '1', 'Enable', 'off');
            uicontrol('Style', 'radiobutton', 'Parent', self.fig.extFrmtControl,...
                'String', 'shot/diagnostic', 'Position', [121 1 120 30],...
                'Tag', '2', 'Enable', 'off');
            hbox.Sizes = [130, 250, 240];
            
            
            self.fig.startExtr = uicontrol('Style', 'Pushbutton', 'Parent', hboxL,...
                'String', '<html><b>Start  extraction</b></html>', 'Callback', @self.startExtraction);
            hboxL.Sizes = [-1 200];
            
            % -------------- Data filter settings
            hpanFilt = uiextras.Panel('Parent', vboxControl, 'Title', 'Channel filters', 'Padding', 3);
            hbox = uiextras.HBox('Parent', hpanFilt);
            self.fig.uiFiltAll = uicontrol('Style', 'Checkbox', 'String', 'ALL',...
                'Parent', hbox, 'Tag', 'FiltAll', 'Callback', @self.setFilters);
            self.fig.uiFiltCam = uicontrol('Style', 'Checkbox', 'String', 'CAM',...
                'Parent', hbox, 'Tag', 'FiltCam', 'Callback', @self.setFilters);
            self.fig.uiFiltRoi = uicontrol('Style', 'Checkbox', 'String', 'ROI',...
                'Parent', hbox, 'Tag', 'FiltRoi', 'Callback', @self.setFilters);
            self.fig.uiFiltADC = uicontrol('Style', 'Checkbox', 'String', 'ADC+Ocean',...
                'Parent', hbox, 'Tag', 'FiltADC', 'Callback', @self.setFilters);
            self.fig.uiFiltMotor = uicontrol('Style', 'Checkbox', 'String', 'MOTOR',...
                'Parent', hbox, 'Tag', 'FiltMotor', 'Callback', @self.setFilters);
            self.fig.uiFiltDAQ = uicontrol('Style', 'Checkbox', 'String', 'DAQ+FLASH timing',...
                'Parent', hbox, 'Tag', 'FiltDAQ', 'Callback', @self.setFilters);
            self.fig.uiFiltTemp = uicontrol('Style', 'Checkbox', 'String', 'Temp/humid',...
                'Parent', hbox, 'Tag', 'FiltTemp', 'Callback', @self.setFilters);
            self.fig.uiFiltExist = uicontrol('Style', 'Checkbox', 'String', 'Existing data only',...
                'Parent', hbox, 'Tag', 'FiltExist', 'Callback', @self.setFilters);
            hbox.Sizes = [60 60 60 120 90 170 120 -1];
            
            % --------------- Main data table and whatnot
            hbox = uiextras.HBox('Parent', vboxMain);
            hpanData = uiextras.Panel('Parent', hbox, 'Title', 'Channel saving settings', 'Padding', 3);
            %htabs = uitabgroup('Parent', hpanData);
            %hTabData = uitab('Parent', htabs, 'Title', 'Available data');
            %hTabAlias = uitab('Parent', htabs, 'Title', 'Channel name aliases');
            vbox = uiextras.VBox('Parent', hpanData);
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'pushbutton', 'String', 'Select ALL', 'Parent', hbox,...
                'Callback', @self.selectAll, 'Tag', 'all');
            uicontrol('Style', 'pushbutton', 'String', 'Select NONE', 'Parent', hbox,...
                'Callback', @self.selectAll, 'Tag', 'none');
            uicontrol('Style', 'pushbutton', 'String', 'Manage save names', ...
                'Parent', hbox, 'Callback', @self.openSaveNames);
            uicontrol('Style', 'pushbutton', 'String', 'Manage 10Hz constants',...
                'Parent', hbox, 'Callback', @self.openConstantManager);
            hbox.Sizes = [150, 150, 150, 150];
            %hbox = uiextras.HBox('Parent', vbox);%hTabData);
            self.fig.tableMain = uitable('Parent', vbox, 'ColumnEditable', logical([1,0,0,0]), ...
                'ColumnName',{'Save', 'DOOCS name', 'Save name', '# shots'},...
                'ColumnWidth', {40, 'auto', 'auto', 100},'CellEditCallback', @self.updateMainTable,...
                'ColumnFormat', {'logical', 'char', 'char', 'numeric'});
            % Sorting from undocumented MLab
            jscrollpane = findjobj(self.fig.tableMain);
            jtable = jscrollpane.getViewport.getView;
            jtable.setSortable(true);		% or: set(jtable,'Sortable','on');
            jtable.setAutoResort(true);
            jtable.setMultiColumnSortable(true);
            jtable.setPreserveSelectionsAfterSorting(true);
            %hbox = uiextras.HBox('Parent', hTabAlias);
            %self.fig.tableAlias = uitable('Parent', hbox, ...
            %    'ColumnName', {'DOOCS name', 'Human readable name'},...
            %    'ColumnEditable', [false, true],...
            %    'ColumnWidth', {350, 300}, 'CellEditCallback', @self.updateAlias);
            %jscrollpane = findjobj(self.fig.tableAlias);
            %jtable = jscrollpane.getViewport.getView;
            %jtable.setSortable(true);		% or: set(jtable,'Sortable','on');
            %jtable.setAutoResort(true);
            %jtable.setMultiColumnSortable(true);
            %jtable.setPreserveSelectionsAfterSorting(true);
            vbox.Sizes = [40, -1];
              
            
            % ----------------   Statusbar
            uiextras.HBox('Parent', vboxMain);
            self.makeStatusBar;
            
            % Sizes and finishing up
            vboxControl.Sizes = [35, 30, 50 -1 50];
            vboxMain.Sizes = [280 -1 2];
            self.fixVertAlignment;
            self.getSetupFileList;
            self.loadAliasList;
            self.loadConstantList;
            %self.updateData(1);
            
            % ------######### Data structures
            self.dat.tabData = [];
            self.dat.db = [];
            self.dat.saveList = []; % An empty array to keep a future list of save channels.
            %self.dat.shot_based = 0;  % Flag to tell whether we want to have shot/diagnostic based saving
            assignin('base', 'daqExtractorAppHandle', self);
        end
        
        function makeStatusBar(self)
            % Try to create a status bar for updating the user about
            % progress
            try
                self.statbar.bar = statusbar(self.fig.parent, 'Select run to start');
                self.statbar.shotBar = javax.swing.JProgressBar;
                set(self.statbar.shotBar, 'Minimum', 0, 'Maximum', 10, 'Value', 4, 'Visible', false);
                self.statbar.bar.add(self.statbar.shotBar,'East');
            catch err
                fprintf('Error creating statusbar:\n%s\n', err.message);
                self.statbar = [];
            end
        end
        
        function openSaveNames(self, ~,~)
            % This function will open a small window which holds a table of
            % the save names of the DOOCS channels.
            this.parent = figure(2837140);
            clf(this.parent);
            set(this.parent, 'Toolbar', 'none', 'MenuBar', 'none',...
                'Name', 'DAQ data extractor app: Save names', 'NumberTitle', 'off',...
                'Units', 'Pixels', 'Position', [400 500 600 350],...
                'DefaultUIControlFontSize', self.dat.fsize, 'Color', 'w',...
                'HandleVisibility', 'callback', 'CloseRequestFcn', @self.closeSaveNames);
            vbox = uiextras.VBox('Parent', this.parent);
            this.table = uitable('Parent', vbox, 'ColumnWidth', {350, 230}, ...
                'ColumnName', {'DOOCS name', 'Save name'});
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'pushbutton', 'String', 'Add Alias', 'Parent', hbox,...
                'Callback', @self.addAlias);
            uicontrol('Style', 'pushbutton', 'String', 'Get aliases from DOOCS',...
                'Parent', hbox, 'Callback', @self.getAliasesFromDoocs);
            uicontrol('Style', 'pushbutton', 'String', 'SAVE alias list', ...
                'Parent', hbox, 'Callback', @self.saveAliasList);
            vbox.Sizes = [-1, 50];
            
            % Now populate the list based on the aliases we currently have
            tabData = [self.dat.aliasMap.keys', self.dat.aliasMap.values'];
            this.table.Data = tabData;
            
            self.saveNames = this;
        end
        
        function closeSaveNames(self, ~, ~)
            % Gracefully close the window for channel save names
            delete(self.saveNames.parent);
            self.saveNames = [];
        end
        
        function openConstantManager(self, ~, ~)
            % This function will open a small window which holds a table of
            % constants with ticks for the ones we want to save.
            this.parent = figure(2837150);
            clf(this.parent);
            set(this.parent, 'Toolbar', 'none', 'MenuBar', 'none',...
                'Name', 'DAQ data extractor app: Constant manager', 'NumberTitle', 'off',...
                'Units', 'Pixels', 'Position', [400 500 600 350],...
                'DefaultUIControlFontSize', self.dat.fsize, 'Color', 'w',...
                'HandleVisibility', 'callback', 'CloseRequestFcn', @self.closeConstantManager);
            vbox = uiextras.VBox('Parent', this.parent);
            this.table = uitable('Parent', vbox, 'ColumnWidth', {150, 330, 50}, ...
                'ColumnName', {'Constant subchannel', 'Name', 'Save'},...
                'ColumnFormat', {'numeric', 'char', 'logical'},...
                'ColumnEditable', logical([0, 0, 1]));
            hbox = uiextras.HBox('Parent', vbox);
            uicontrol('Style', 'pushbutton', 'String', 'Save settings', 'Parent', hbox,...
                'Callback', @self.saveConstantSaving);
            uicontrol('Style', 'pushbutton', 'String', 'Select all', 'Parent', hbox);
            uicontrol('Style', 'pushbutton', 'String', 'Select none', 'Parent', hbox);
                
            vbox.Sizes = [-1, 50];
            
            % Now populate the list based on the aliases we currently have
            tabData = [self.dat.constMap.keys', self.dat.constMap.values'];
            tabData(:, 3) = cell(size(tabData,1), 1);
            this.table.Data = tabData;
            this.table.Data(self.dat.constSaves, 3) = {true};
            
            self.constManager = this;
        end
        
        function closeConstantManager(self, ~, ~)
            delete(self.constManager.parent);
            self.constManager = [];
        end
        
        %% DAQ access functions
        function listAvailableData(self, ~, ~)
            % Function to query the DAQ for data that is available for a
            % particular run.
            self.updateStatusBar('Starting data request from DAQ...');
            addpath('/local/lib');
            if exist('doocsread')==3
                % Actual DAQ query stuff. We first request the data about shots from one
                % of the DAQ streams. We then use the times of the first and
                % last shot and use these with daq_list function.
                xmlFile = {'<DAQREQ>',...
                    '<ReqId  id=''1502899874''/>',...
                    sprintf('<RunFirst  number=''%s''/>', self.fig.uiRunNumber.String),...
                    '<Exp  name=''flashfwd''/>',...
                    '<DDir name=''/daq_data/flashfwd/EXP/''/>',...
                    '<CDir name=''/daq/ttf2/adm/''/>',...
                    '<ScanMode mode=''0x2000000f'' />',...
                    '<Chan name=''TTF2.DAQ/FLASHFWD.DISTRIBUTOR/DAQ.STREAM.1/EVB.EVENTS;STREAM1''/>',...
                    '</DAQREQ>'}';
                tempxmlFileName = 'tempChanList.xml';
                fid = fopen(tempxmlFileName, 'w');
                for k=1:length(xmlFile); fprintf(fid, '%s\n', xmlFile{k}); end
                [data,err] = daq_read(tempxmlFileName);
                delete(tempxmlFileName);
                if ~isempty(err)
                    self.updateStatusBar(err)
                    fprintf(err);
                    return;
                end
                ids = fieldnames(data);
                tStart = data.(ids{1}).time(1);
                tEnd = data.(ids{end}).time(1);
                % Save the POSIX times for later user
                self.dat.runStartEnd = [tStart, tEnd];
                % Now prepare life for daq_list:
                inp{1} = ['-TStart ' datestr(datetime(tStart, 'ConvertFrom', 'posixtime',...
                    'TimeZone','Europe/Berlin'), 'yyyy-mm-ddTHH:MM:SS')];
                inp{2} = ['-TStop ' datestr(datetime(tEnd, 'ConvertFrom', 'posixtime',...
                    'TimeZone','Europe/Berlin'), 'yyyy-mm-ddTHH:MM:SS')];
                self.fig.uiStartTime.String = inp{1}(9:end);
                self.fig.uiEndTime.String = inp{2}(8:end);
                inp{3} = '-Exp flashfwd';
                inp{4} = '-DDir /daq_data/flashfwd/EXP/';
                self.dat.inputCell = inp;
                [daqData, err] = daq_list(inp);
                if ~isempty(err)
                    self.updateStatusBar(err);
                    disp(err); 
                    return; 
                end
                self.updateStatusBar('Received data from DAQ');
            else
                % This is a workaround for debugging on my machine...
                ff = load('guis/@daqExtractorApp/daqlisttest.mat'); daqData = ff.a;
                self.updateStatusBar('Using debugging data')
            end
            
            % And convert everything to a table format
            % If the table already exists, keep the 'visible' and 'save'
            % columns
%             if ~isempty(self.dat.tabData)
%                 oldData = self.dat.tabData;
%             else
%                 tabData = cell(length(daqData), 5);
%             end
            % Also check if a list has been loaded from a saved setup: in
            % this case we tick the marked channels for saving and then
            % clear the database - any changes made will be reflected by
            % keeping the current settings when reloading new run.
            
            for k=1:length(daqData)
                tt = strsplit(daqData{k});
                tabData{k,2} = tt{3}; % The DOOCS name
                % Default to false for saving this channel
                tabData{k,1} = false;
                % Apply the old data - first check if we have any data
                % already loaded
                if ~isempty(self.dat.tabData)
                    inds = find(strcmp(self.dat.tabData(:,2), tt{3}));
                    if ~isempty(inds); tabData{k,1} = self.dat.tabData{inds,1}; end
                elseif ~isempty(self.dat.saveList)
                    % If the saveList is not empty, means we have no data
                    % loaded yet and thus need to check if we can load
                    % stuff from there
                    tabData{k,1} = ~isempty(find(strcmp(self.dat.saveList, tt{3})));
                end
                % Add safety for the cases where we have not set this up to
                % now!
                if isempty(tabData{k,1}); tabData{k,1} = false; end
                tabData{k,3} = self.getAlias(tabData{k,2});
                tabData{k,4} = str2double(tt{7}); % Number of shots
                tabData{k,5} = true; % Whether it is currently visible
            end
            % Filter out the repeats. These result from two channels for
            % spectrometers etc.
            [~, ind] = unique(tabData(:,2));
            self.dat.tabData = tabData(ind,:);
            self.fig.tableMain.Data = tabData(ind,1:4);
            self.fig.tableMain.ColumnWidth = {40, 500, 150, 55};
            
            % Set the filters, if needed
            self.setFilters(-1);
            %self.updateAlias(1)
        end
        
        function startExtraction(self, ~, ~)
            % Function to do the actual data extraction and writing to disk.
            
            self.updateStatusBar('Starting data extraction')
            extStartTime = tic;
            % Common things: make root folder, if it doesn't exist
            rootfol = self.fig.uiSaveDir.String;
            if ~exist(rootfol, 'dir'); mkdir(rootfol); end
            % And get the list of diagnostics we actually want to save
            saveInds = find(cell2mat(self.dat.tabData(:,1)));
            % Make a cell array with the channel name and the 'nickname' -
            % this is just the alias! To be on the safe side, we deblank
            % the alias.
            reqChans = cell(size(saveInds));
            aliases = cell(size(reqChans));
            for i=1:length(saveInds)
                chanName = self.dat.tabData{saveInds(i),2};
                aliases{i} = deblankCaps(self.dat.tabData{saveInds(i),3});
                reqChans{i} = ['-Chan ' chanName ';' aliases{i}];
            end
            
            % ---------############  Extraction start
            % The route taken now depends on how we want to extract the
            % data. If we do single diagnostic with flash PIDs, we'll only
            % pull one diagnostic at a time and save them each into a file.
            % Otherwise, use the 'algorithm' from daq2ServerStyle.
            
            % First, we grab the constants, if asked to 
            constants = self.extractConstants(any(contains(reqChans, 'CONSTANT_MANAGER')));
            if any(contains(reqChans, 'CONSTANT_MANAGER'))
                 aliases(contains(reqChans, 'CONSTANT_MANAGER')) = [];
                 reqChans(contains(reqChans, 'CONSTANT_MANAGER')) = [];
            end
            % This variable determines whether we save as FLASH ids or into one run
            extControl = str2double(self.fig.extControl.SelectedObject.Tag);
            if extControl == 1
                % First make the run structure: rootfolder/runDDDDD/diags...
                runfol = fullfile(rootfol, ['run' self.fig.uiRunNumber.String]);
                if ~exist(runfol, 'dir'); mkdir(runfol); end
                % For each diagnostic, simply extract the data and write to
                % a folder with its name. Need to check the dimensionality
                % and thus decide what file to write into.
                tRunStartRaw = posixtime(datetime(datenum(self.fig.uiStartTime.String, 'yyyy-mm-ddTHH:MM:SS'),...
                    'convertFrom', 'datenum', 'TimeZone','Europe/Berlin'));         
                tRunEndRaw = posixtime(datetime(datenum(self.fig.uiEndTime.String, 'yyyy-mm-ddTHH:MM:SS'),...
                    'convertFrom', 'datenum', 'TimeZone','Europe/Berlin'));
                t_range = tRunStartRaw:100:tRunEndRaw;
                for i=1:length(reqChans)
                    % Use the TStart and TStop from before
                    for t=2:length(t_range)
                        inp = self.dat.inputCell;
                        inp{1} = ['-TStart ' datestr(datetime(t_range(t-1), 'ConvertFrom', 'posixtime',...
                            'TimeZone','Europe/Berlin'), 'yyyy-mm-ddTHH:MM:SS')];
                        inp{2} = ['-TStop ' datestr(datetime(t_range(t), 'ConvertFrom', 'posixtime',...
                            'TimeZone','Europe/Berlin'), 'yyyy-mm-ddTHH:MM:SS')];
                        inp{end+1} = reqChans{i};
                        [daqData, err] = daq_read(inp);
                        if ~isempty(err)
                            disp(err.message);
                            continue;
                        end
                        % No error - let's extract the files!
                        shots = fieldnames(daqData);
                        fprintf('Request %i of %i yielded %i shots\n', t-1, length(t_range)-1, length(shots))
                        assignin('base', 'daqData', daqData);
                        if isempty(fieldnames(daqData))
                            fprintf('No fields in daq data for %s! Continuing.\n', reqChans{i});
                            continue;
                        end
                        dataType = daqData.(shots{1}).(aliases{i}).daqtype;
                        % And now, update the status bar:
                        self.updateStatusBar('Extracting data...', [length(reqChans), length(shots)]);
                        if all(dataType ~= [22, 33]) % Anythin but single points:
                            %dataType==27 || dataType==41 % image and spectrum - into separate files
                            diagFol = fullfile(runfol, aliases{i});
                            if ~exist(diagFol, 'dir'); mkdir(diagFol); end
                            for s=1:length(shots)
                                %self.updateStatusBar(['Extracting data: ' aliases{i}],...
                                %    [length(reqChans), i, length(shots), s]);
                                self.updateStatusBar(['Extracting data: ' aliases{i},...
                                    sprintf(', block %i(%i)', t-1, length(t_range)-1)],...
                                    [length(reqChans), i, length(shots), s]);
                                if dataType==27
                                    fname = fullfile(diagFol, [aliases{i} '_' shots{s} '.png']);
                                    shot_time = daqData.(shots{s}).(aliases{i}).sec + ...
                                        1e-6*daqData.(shots{s}).(aliases{i}).usec;
                                    assignin('base', 'shot_time', shot_time);
                                    creation_time = datestr(datetime(shot_time,...
                                        'ConvertFrom','posixTime','TimeZone','Europe/Berlin'),...
                                        'yyyy-mm-dd HH:MM:SS.FFF');
                                    unixComment = sprintf('unix=%2.6f', shot_time);
                                    imwrite(daqData.(shots{s}).(aliases{i}).data, fname,...
                                        'CreationTime', creation_time, 'Comment', unixComment);
                                elseif dataType==40 % BPM data
                                    fname = fullfile(diagFol, [aliases{i} '_' shots{s} '.txt']);
                                    data = daqData.(shots{s}).(aliases{i}).data;
                                    dlmwrite(fname, data, ',')
                                elseif any(dataType==[41, 10]) % spectrum data
                                    fname = fullfile(diagFol, [aliases{i} '_' shots{s} '.txt']);
                                    diag_strct = daqData.(shots{s}).(aliases{i});
                                    x_axis = diag_strct.start + (0:(diag_strct.ndata-1))*diag_strct.inc;
                                    y_axis = diag_strct.data;
                                    %fID = fopen(fname, 'w');
                                    %for k=1:length(y_axis)
                                    %    fprintf(fID, '%1.8e,%1.8e\n', x_axis(k), y_axis(k));
                                    %end
                                    %fclose(fID);
                                    dlmwrite(fname, [x_axis', y_axis], ',');
                                elseif dataType==45 % Ocean optic data
                                    fname = fullfile(diagFol, [aliases{i} '_' shots{s} '.txt']);
                                    diag_strct = daqData.(shots{s}).(aliases{i});
                                    data = diag_strct.data;
                                    dlmwrite(fname, data, ',');
                                end
                            end
                        else   % Single points
                            fname = fullfile(runfol, aliases{i});
                            data = struct;
                            for s=1:length(shots)
                                data.(shots{s}) = daqData.(shots{s}).(aliases{i}).data;
                            end
                            save(fname, 'data');
                        end
                    end
                end
                
                % And save the constants, if we wanted to
                saved_constants = fieldnames(constants);
                for i=1:length(saved_constants)
                    data = constants.(saved_constants{i});
                    save(fullfile(runfol, saved_constants{i}), 'data');
                end
            elseif extControl == 2
                % Saving into run structure...
                % First create the necessary cell array for the xmlFile
                % creation.
                xmlChans = cell(size(reqChans));
                for i=1:length(saveInds)
                    chanName = self.dat.tabData{saveInds(i),2};
                    aliases{i} = deblankCaps(self.dat.tabData{saveInds(i),3});
                    xmlChans{i} = ['<Chan name=''' chanName ';' aliases{i} '''/>'];
                end
                % We now create the object for the daq2ServerStyle work
                daq2Server = daq2ServerStyle(str2double(self.fig.uiRunNumber.String), xmlChans,...
                    rootfol, 5, 1);
                % Now attach a listener to the event given by daq2Server
                addlistener(daq2Server, 'shotProgress', @self.daq2ServerStatusbarUpdate);
                set(self.statbar.shotBar, 'Visible', true);
                self.updateStatusBar('Requesting data from the DAQ');
                daq2Server.extract;
            elseif extControl == 3
                % Saving everything into one hdf5 file.
                % Need to clean up the old file first...
                %fname = fullfile(rootfol, ['run' self.fig.uiRunNumber.String '.hdf5']);
                fname = fullfile(rootfol, self.fig.uiSaveName.String);
                if exist(fname, 'file'); delete(fname); end
                ext_start = datestr(now, 'yyyy-mm-ddTHH:MM:SS');
                % For each diagnostic, simply extract the data and write to
                % the correct hdf5 location. Need to check the dimensionality
                % and thus decide what to write.
                for i=1:length(reqChans)
                    % Try to speed things up by only querying one minute of
                    % data at a time - but only for images (?)
                    %
                    %tRunStart = datestr(datetime(self.dat.runStartEnd(1)-5, 'ConvertFrom', 'posixtime',...
                    %'TimeZone','Europe/Berlin'), 'yyyy-mm-ddTHH:MM:SS');
                    %tRunEnd = datestr(datetime(self.dat.runStartEnd(1)+5, 'ConvertFrom', 'posixtime',...
                    %'TimeZone','Europe/Berlin'), 'yyyy-mm-ddTHH:MM:SS');
                    %if strcmp(self.fig.uiStartTime.String, tRunStart)
                    %    tRunStartRaw = self.dat.runStartEnd(1);
                    %else
                    tRunStartRaw = posixtime(datetime(datenum(self.fig.uiStartTime.String, 'yyyy-mm-ddTHH:MM:SS'),...
                            'convertFrom', 'datenum', 'TimeZone','Europe/Berlin'));
                    %end
                    %if strcmp(self.fig.uiEndTime.String, tRunEnd)
                    %    tRunEndRaw = self.dat.runStartEnd(2);
                    %else
                    tRunEndRaw = posixtime(datetime(datenum(self.fig.uiEndTime.String, 'yyyy-mm-ddTHH:MM:SS'),...
                            'convertFrom', 'datenum', 'TimeZone','Europe/Berlin'));
                    %end
                    t_range = tRunStartRaw:60:tRunEndRaw;
                    for t=2:length(t_range)
                        inp = self.dat.inputCell;
                        inp{1} = ['-TStart ' datestr(datetime(t_range(t-1), 'ConvertFrom', 'posixtime',...
                            'TimeZone','Europe/Berlin'), 'yyyy-mm-ddTHH:MM:SS')];
                        inp{2} = ['-TStop ' datestr(datetime(t_range(t), 'ConvertFrom', 'posixtime',...
                            'TimeZone','Europe/Berlin'), 'yyyy-mm-ddTHH:MM:SS')];
                        inp{end+1} = reqChans{i};
                        [daqData, err] = daq_read(inp);
                        assignin('base', 'daqData', daqData);
                        if ~isempty(err)
                            disp(err.message);
                            continue;
                        end
                        if isempty(fieldnames(daqData))
                            fprintf('No fields in daq data for %s! Continuing.\n', reqChans{i});
                            continue;
                        end
                        % No error - let's extract the files!
                        shots = fieldnames(daqData);
                        fprintf('Request %i of %i yielded %i shots\n', t-1, length(t_range)-1, length(shots))
                        daqDataType = daqData.(shots{1}).(aliases{i}).daqtype;
                        % And now, update the status bar:
                        self.updateStatusBar('Extracting data...', [length(reqChans), length(shots)]); 
                        for s=1:length(shots)
                            self.updateStatusBar(['Extracting data: ' aliases{i},...
                                sprintf(', block %i(%i)', t-1, length(t_range)-1)],...
                                [length(reqChans), i, length(shots), s]);
                            if any(daqDataType==[27, 33, 22]) %image or point
                                data = daqData.(shots{s}).(aliases{i}).data;
                            elseif daqDataType==40 % This is BPM data
                                data = daqData.(shots{s}).(aliases{i}).data;
                            elseif daqDataType==41 || daqDataType==10 % This is spectrum or Toroid
                                diag_strct = daqData.(shots{s}).(aliases{i});
                                x_axis = diag_strct.start + (0:(diag_strct.ndata-1))*diag_strct.inc;
                                y_axis = diag_strct.data;
                                %[size(x_axis) size(y_axis)]
                                data = [x_axis', y_axis];
                            elseif daqDataType==45  % OCean optic spectrum
                                diag_strct = daqData.(shots{s}).(aliases{i});
                                data = diag_strct.data;
                            end
                            
                            % Now write the data to the hdf5 file.
                            % Determine the location of it in the file
                            if strcmp(self.fig.extFrmtControl.SelectedObject.Tag, '2')
                                dataLocation = ['/daqData/' shots{s} '/' aliases{i}];
                            else
                                dataLocation = ['/daqData/' aliases{i} '/' shots{s}];
                            end
                            if s==1; dataType = whos('data'); end
                            h5create(fname, dataLocation, size(data),...
                                'Datatype', dataType.class);%, 'Deflate', 6, 'ChunkSize', size(data));
                            h5write(fname, dataLocation, data);
                            % And the shot time as well
                            if all(daqDataType ~= [22,33])
                                shot_time = daqData.(shots{s}).(aliases{i}).sec + ...
                                    1e-6*daqData.(shots{s}).(aliases{i}).usec;
                                h5writeatt(fname, dataLocation, 'shot_time_unix', shot_time);
                                h5writeatt(fname, dataLocation, 'shot_time', datestr(datetime(shot_time,...
                                    'ConvertFrom','posixTime','TimeZone','Europe/Berlin'),...
                                    'yyyy-mm-ddTHH:MM:SS.FFF'));
                            end
                        end
                    end
                end
                
                % Write the constants
                saved_constants = fieldnames(constants);
                for i=1:length(saved_constants)
                    data_all = constants.(saved_constants{i});
                    shots = fieldnames(data_all);
                    for s = 1:length(shots)
                        data = data_all.(shots{s});
                        if strcmp(self.fig.extFrmtControl.SelectedObject.Tag, '2')
                            dataLocation = ['/daqData/' shots{s} '/' saved_constants{i}];
                        else
                            dataLocation = ['/daqData/' saved_constants{i} '/' shots{s}];
                        end
                        dataType = whos('data');
                        h5create(fname, dataLocation, size(data), 'Datatype', dataType.class);
                        h5write(fname, dataLocation, data);
                    end
                end
                
                % And write some info about the run
                h5writeatt(fname, '/', 'extraction_time', ext_start);
                h5writeatt(fname, '/', 'run_start', self.dat.inputCell{1}(9:end));
                h5writeatt(fname, '/', 'run_end', self.dat.inputCell{2}(8:end));
                h5writeatt(fname, '/', 'run_number', str2double(self.fig.uiRunNumber.String));
                
            elseif extControl == 4
                % Saving all data as a structure into a mat file.
                self.updateStatusBar('Requesting data from DAQ');
                inp = self.dat.inputCell;
                inp = [inp'; reqChans; '-ScanMode 0x2000000f']';
                [daqData, err] = daq_read(inp); %#ok<*ASGLU>
                if ~isempty(err)
                    disp(err);
                    self.updateStatusBar(err);
                    return;
                end
                fname = fullfile(rootfol, ['run' self.fig.uiRunNumber.String]);
                self.updateStatusBar('Writing data into mat file...')
                save(fname, 'daqData', '-v7.3');
            end
            % And we're done: hide the status bars
            self.updateStatusBar(sprintf('Extraction finished in %.0f seconds!',...
                toc(extStartTime)), 0)
        end
        
        function daq2ServerStatusbarUpdate(self, ~, e)
            % Function that keeps the user updated about the progress of
            % daq2ServerStyle.
            self.updateStatusBar('Extracting data into shot structure...',...
                [e.allShots, e.thisShot, -1]);
        end
        
        function constants = extractConstants(self, extractFlag)
           % This functiong oes through all the constants and returns
           % the extracted data as a structure. It tries to extract all
           % channels and checks the names in them.
           constants = struct;
           if ~extractFlag; return; end
           
           % We make a list of the constants we did want to save
           aliases = {};
           for i=1:length(self.dat.constSaves)
               v = self.dat.constSaves(i);
               aliases{end+1} = self.dat.constMap(v);
           end
           numConstExtract = length(self.dat.constSaves);
           numExtracted = 0;
           
           % Now we iterate through all the constants there are and keep
           % the ones we wanted all along. Will take some time, but the
           % most reliable and futureproof way I think.
           for i=0:220
               % Use the TStart and TStop from before
               inp = self.dat.inputCell;
               inp{end+1} = sprintf('-Chan FLASH.DIAG/FFWDCONSTANTS/CONSTANT_MANAGER:%i;T', i);
               [daqData, err] = daq_read(inp);
               if ~isempty(err)
                   continue
               end
               
               shots = fieldnames(daqData);
               % Check whether this is one of the constants we wanted
               if ~isempty(shots)
                   this_const = daqData.(shots{1}).T.names;
                   if ~any(contains(aliases, this_const)); continue; end
               
                   data = struct;
                   for s = 1:length(shots)
                       data.(shots{s}) = daqData.(shots{s}).T.data;
                   end
                   constants.(this_const) = data;
                   numExtracted = numExtracted + 1;
               end
               if numExtracted == numConstExtract
                   break
               end
           end
           
        end
        
        %% Data and alias manipulation function
        function alias = getAlias(self, doocsName)
            % Return an alias (save name) for a DOOCS channel.
            if self.dat.aliasMap.isKey(doocsName)
                alias = self.dat.aliasMap(doocsName);
            else
                tt = strsplit(doocsName, '/');
                alias = tt{end};
                alias(alias=='.') = '_';
            end
        end
        
        function addAlias(self, ~, ~)
            % This will add a NEW alias into the list.
            % Check whether we have a list of channels from the DAQ.
            if isempty(self.dat.tabData); return; end
            % First filter out the ones that already have aliases
            aliasDoocs = self.dat.aliasMap.keys;
            fullDoocs = self.dat.tabData(:,2);
            [~,iA,~] = intersect(fullDoocs, aliasDoocs);
            fullDoocs(iA) = []; fullDoocs = sort(fullDoocs);
            maxStrLen = max(cellfun(@length, fullDoocs));
            [indx,tf] = listdlg('PromptString','Select a channel to add alias for:',...
                           'SelectionMode','single', 'ListSize', [maxStrLen*7,250],...
                           'ListString', fullDoocs);
            if tf
                newDoocs = fullDoocs{indx};
                % A new dialog box we open...
                newAlias = inputdlg(['Enter alias for ' newDoocs ':'], 'Add new alias', 1);
                if ~isempty(newAlias)
                    self.dat.aliasMap(newDoocs) = newAlias{1};
                    self.saveNames.table.Data = [self.dat.aliasMap.keys', ...
                        self.dat.aliasMap.values'];
                end
            end
        end
        
        function getAliasesFromDoocs(self, ~, ~)
            % Get the 'ALIAS' property of motors and some other things
            % straight from DOOCS.
            warning('off', 'doocsread:processChannels:illegal_server');
            warning('off', 'doocsread:processChannels:unavailable_property');
            doocsAliases = {};
            base = 'FLASH.DIAG/';
            locations = {'FFW*MOTOR*'};
            avoids = {'_SVR', 'MBDEV'};
            for l = 1:length(locations)
                % Find all locations
                locs_ = doocsread([base, locations{l}]);
                locs = {locs_.channel}';
                for loc = 1:length(locs)
                    % Find all channels
                    location = locs{loc};
                    % Ensure there is no slash at the end - doocsread does
                    % not like double slashes....
                    while endsWith(location, '/'); location(end) = []; end
                    chans_ = doocsread([location '/*']);
                    chans = {chans_.channel}';
                    for c = 1:length(chans)
                        %channel = chans{c}
                        %while endsWith(channel, '/'); channel(end) = []; end
                        %if endsWith(channel, '/'); channel(end) = []; end
                        channel = strip(chans{c}, '/');
                        if contains(channel, avoids); continue; end
                        % Get the channels. But this seems to give random
                        % bit-errors every now and then - so if we get
                        % that, just return.
                        try
                            alias = doocsread([channel '/ALIAS']);
                            doocsAliases(end+1,:) = {channel, alias.data};
                            % If alias actually contains something, add it to
                            % our database!
                            if isempty(alias.error) && ~isempty(alias.data)
                                self.dat.aliasMap(channel) = alias.data;                            
                            end
                        catch err
                            self.updateStatusBar(err.message);
                            return;
                        end
                    end
                end
            end
            % If we make it here, we managed to get the channels
            % successfully. Update the gui table now.
            self.saveNames.table.Data = [self.dat.aliasMap.keys', ...
                        self.dat.aliasMap.values'];
        end
        
        function setFilters(self, h, ~)
            % Shows/hides rows of the DAQ data table according to the
            % user's wishes. Note hidden channels may still be saved!
            if isempty(self.dat.tabData); return; end
            
            % Now, if all is selected, all other filter disappear...
            if ishandle(h) 
                if strcmp(h.Tag, 'FiltAll')
                    
                else
                    self.fig.uiFiltAll.Value = 0;
                end
            end
            
            tabdat = self.dat.tabData;
            % Get the indices for different things...
            % Check if this is a callback or filter on the dataset
            if ishandle(h)
                id = h.Tag;
            else
                id = 'ALL';
            end
            % Now find the all indices.
            iCam = find(not(cellfun('isempty', strfind(tabdat(:,2), 'CAM')))); %#ok<*STRCL1>
            iRoi = find(not(cellfun('isempty', strfind(tabdat(:,2), 'ROI'))));
            iADC1 = find(not(cellfun('isempty', strfind(tabdat(:,2), 'ADC'))));
            iADC2 = find(not(cellfun('isempty', strfind(tabdat(:,2), 'OCEAN'))));
            iADC = unique([iADC1; iADC2]);
            iMot1 = find(not(cellfun('isempty', strfind(tabdat(:,2), 'MOTOR'))));
            iMot2 = find(not(cellfun('isempty', strfind(tabdat(:,2), 'HEXAPOD'))));
            iMot = unique([iMot1; iMot2]);
            iDaq1 = find(not(cellfun('isempty', strfind(tabdat(:,2), 'TTF2.DAQ'))));
            iDaq2 = find(not(cellfun('isempty', strfind(tabdat(:,2), 'TIMING'))));
            iDaq3 = find(not(cellfun('isempty', strfind(tabdat(:,2), 'TIMER'))));
            iDaq = unique([iDaq1; iDaq2; iDaq3]);
            iTem1 = find(not(cellfun('isempty', strfind(tabdat(:,2), 'MOTOR'))));
            iTem2 = find(not(cellfun('isempty', strfind(tabdat(:,2), 'HEXAPOD'))));
            iTem = unique([iTem1; iTem2]);
            iExist = find(cellfun(@(x) x~=0, tabdat(:,4)));
            % Now find the states of all the checkboxes
            sCam = self.fig.uiFiltCam.Value;
            sRoi = self.fig.uiFiltRoi.Value;
            sADC = self.fig.uiFiltADC.Value;
            sMot = self.fig.uiFiltMotor.Value;
            sDaq = self.fig.uiFiltDAQ.Value;
            sTem = self.fig.uiFiltTemp.Value;
            sExist = self.fig.uiFiltExist.Value;
            % Now do the actual filtering! The indices to show are the ones
            % where the corresponding value is set to 1.
            iTot = [sCam*iCam; sRoi*iRoi; sADC*iADC; sMot*iMot; sDaq*iDaq;sTem*iTem];
            itots = unique(iTot);
            % And apply the existing filter
            if sExist
                itots = intersect(itots, iExist);
            end
            itots = itots(itots~=0);
            % Now, set all to not show and then only show the ones we want.
            self.dat.tabData(:,5) = {false};
            if ~isempty(itots)
                %self.fig.tableMain.Data = tabdat(itots, :);
                self.dat.tabData(itots,5) = {true};
            else
                if sExist
                    %self.fig.tableMain.Data = tabdat(iExist, :);
                    self.dat.tabData(iExist,5) = {true};
                else
                    %self.fig.tableMain.Data = tabdat;
                    self.dat.tabData(:,5) = {true};
                end
            end
            %self.updateAlias(1);
            self.updateMainTable(-10);
        end
        
        function loadAliasList(self)
            % Import the known aliases from the default file.
            fdat = csvimport(fullfile(fileparts(which('daqExtractorApp.m')),...
                'ListofAliases.csv'), 'delimiter', ';');
            fdat = strtrim(fdat); % Remove leading and trailing spaces...
            self.dat.knownAliases = fdat;
            self.dat.aliasMap = containers.Map(fdat(:,1), fdat(:,2));
        end
        
        function loadConstantList(self)
            % Load the constant map and also the default save list.
            fdat = load(fullfile(fileparts(which('daqExtractorApp.m')),...
                'ConstantSubchannelMap'));
            self.dat.constMap = fdat.chanmap;
            fdat = load(fullfile(fileparts(which('daqExtractorApp.m')),...
                'ConstantSubchannelSaving'));
            self.dat.constSaves = fdat.save_chans;
        end
        
        function saveConstantSaving(self, ~ , ~)
            ss = cellfun(@isempty, self.constManager.table.Data(:,3));
            save_chans = find(ss == 0); save_chans = save_chans(:);
            self.dat.constSaves = save_chans;
            save(fullfile(fileparts(which('daqExtractorApp.m')),...
                'ConstantSubchannelSaving'), 'save_chans')
            
        end
        
        function saveAliasList(self, ~, ~)
            % Save an updated list of aliases into a file.
            fname = fullfile(fileparts(which('daqExtractorApp.m')),...
                'ListofAliases.csv');
            keys = self.dat.aliasMap.keys;
            fid = fopen(fname, 'w');
            for k=1:length(keys)
                fprintf(fid, '%s; %s\n', keys{k}, self.dat.aliasMap(keys{k}));
            end
            fclose(fid);
        end
        
        function updateAlias(self, h, e)
            % This function ensures that the alias entered is unique! Also updates
            % the data in the main, DAQ channel table.
            if ishandle(h)
                id = e.Indices(1); % Row of edit ...
            else % This is setting the table at the start
                doocsnames = self.fig.tableMain.Data(:,2);
                aliases = cell(length(doocsnames), 2);
                aliases(:,1) = doocsnames;
                for k=1:length(doocsnames)
                    % First set the alias in the alias table, and then in
                    % the main one as well.
                    tt = strsplit(doocsnames{k}, '/');
                    tt{end}(tt{end}=='.') = '_';
                    inds = strfind(self.dat.knownAliases(:,1), doocsnames{k});
                    idx = find(not(cellfun('isempty', inds)));
                    if ~isempty(idx)
                        dispName = self.dat.knownAliases{idx,2};
                    else
                        dispName = tt{end};
                    end
                    aliases{k,2} = dispName;
                    self.fig.tableMain.Data{k,3} = dispName;
                    self.dat.tabData{k,3} = dispName;
                end
                self.fig.tableAlias.Data = aliases;
            end
        end
        
        function updateMainTable(self, h, e)
            % This function ensures that ticking a channel makes the
            % correct one be saved. It will also update the visible table
            % to correspond to the 'Visible' column of the database.
            
            %self.dat.tabData{e.Indices(1)} = e.NewData;
            if ishandle(h)
                % We've received an update from the visible table. We must
                % then update the background database!
                dispInd = e.Indices(1);
                chanName = self.fig.tableMain.Data{dispInd,2};
                % Need an exact comparison here!
                inds = strcmp(self.dat.tabData(:,2), chanName);
                tabInd = find(inds);
                self.dat.tabData{tabInd,1} = e.NewData;
            else
                % We're updating the view only
                logm = logical(cell2mat(self.dat.tabData(:,5)));
                self.fig.tableMain.Data = self.dat.tabData(logm, 1:4);
            end
        end
        
        %% Utilities
        function updateRunTimes(self, h, ~)
            if strcmp(h.Tag, 'StartTime')
                self.dat.inputCell{1} = ['-TStart ' h.String];
            else
                self.dat.inputCell{2} = ['-TStop ' h.String];
            end
        end
        
        function changeOutputType(self, h, ~)
            % Filename is accessible for h5 and mat saving
            if str2double(h.Tag) > 2
                self.fig.uiSaveName.Enable = 'on';
            else
                self.fig.uiSaveName.Enable = 'off';
            end
            % Only allow the location to be changed for h5 files.
            if any(str2double(h.Tag)==[3])
                set(self.fig.extFrmtControl.Children, 'Enable', 'on')
            else
                set(self.fig.extFrmtControl.Children, 'Enable', 'off')
            end
        end
        
        function findPath(self, ~, ~)
            %inpath = self.dat.SaveDir;
            inpath = self.fig.uiSaveDir.String;
            outpath = uigetdir(inpath, 'Set root path for data extraction');
            if outpath~=0
                self.fig.uiSaveDir.String = outpath;
                self.dat.SaveDir = outpath;
            end
        end
        
        function fixVertAlignment(self)
            texts = findobj(self.fig.parent, 'Style', 'text');
            for i=1:length(texts)
                jh = findjobj(texts(i));
                jh.setVerticalAlignment(javax.swing.JLabel.CENTER)
            end
        end
        
        function getSetupFileList(self)
            fpath = fileparts(which('daqExtractorApp.m'));
            flist = dir(fullfile(fpath, '*.mat'));
            flist = {flist.name};
            setupList{1} = 'Last session';
            for k=1:length(flist)
                if strcmp(flist{k}, 'lastState.mat'); continue; end
                [~, setupList{end+1}, ~] = fileparts(flist{k}); %#ok<AGROW>
            end
            self.fig.setupList.String = setupList;
        end
        
        function loadSetup(self, ~, ~)
            % Deals with loading
            id = self.fig.setupList.Value;
            str = self.fig.setupList.String;
            fpath = fileparts(which('daqExtractorApp.m'));
            if id==1 % the latest session
                fdat = load(fullfile(fpath, 'lastState.mat'));
            else
                fdat = load(fullfile(fpath, str{id}));
            end
            
            % Now reassign all the fields...
            fields = fieldnames(fdat.state);
            for k=1:length(fields)
                % Finally update the save tick list
                if strcmp(fields{k}, 'saveList')
                    if ~isempty(self.dat.tabData)
                        for i=1:length(fdat.state.saveList)
                            tabInd = find(strcmp(self.dat.tabData(:,2), fdat.state.saveList{i}));
                            self.dat.tabData{tabInd, 1} = true;
                        end
                    else
                        self.dat.saveList = fdat.state.saveList;
                    end
                else
                    %self.dat.(fields{k}) = fdat.state.(fields{k});
                    if strcmp(self.fig.(['ui' fields{k}]).Style, 'edit')
                        self.fig.(['ui' fields{k}]).String = fdat.state.(fields{k});
                    else
                        self.fig.(['ui' fields{k}]).Value = fdat.state.(fields{k});
                    end
                end
            end
            
            
            self.updateStatusBar(['Loaded configuration ''' str{id} '''']); 
        end
        
        function saveSetup(self, h, ~)
            datNames = fieldnames(self.fig);
            state = struct;
            % Get the GUI state
            for k=1:length(datNames)
                if strcmp(datNames{k}(1:2), 'ui')
                    if strcmp(self.fig.(datNames{k}).Style, 'edit')
                        v = self.fig.(datNames{k}).String;
                    else
                        v = self.fig.(datNames{k}).Value;
                    end
                    state.(datNames{k}(3:end)) = v;% self.dat.(datNames{k}(3:end));
                end
            end
            % And the channel saving list: save the ones that need to be saved
            if ~isempty(self.dat.tabData)
                %state.saveList = self.fig.tableMain.Data(cell2mat(self.fig.tableMain.Data(:,1)),2);
                logm = logical(cell2mat(self.dat.tabData(:,1)));
                state.saveList = self.dat.tabData(logm,2);
            end
            
            if ishandle(h)
                fname = inputdlg('Select name for current setup', 'Saving current configuration');
                if isempty(fname); return; end
            else
                fname{1} = 'lastState.mat';
            end
            save(fullfile(fileparts(which('daqExtractorApp.m')), fname{1}), 'state');
            self.getSetupFileList;
            self.updateStatusBar(['Saved current configuration to ' fname{1}])
        end
        
        function updateData(self, h, ~)
            if ishandle(h) % Update a single field
                id = h.Tag;
                switch h.Style
                    case 'edit'
                        self.dat.(id) = h.String;
                    case 'checkbox'
                        self.dat.(id) = h.Value;
                end
            else % Update all field at once with recursion
                fields = fieldnames(self.fig);
                for k=1:length(fields)
                    if strcmp(fields{k}(1:2), 'ui')
                        self.updateData(self.fig.(fields{k}));
                    end
                end
            end
        end
        
        function updateStatusBar(self, text, vals)
            % This function will keep the statusbar up to date, moving the
            % progress bars and updating text.
            if isempty(self.statbar); return; end
            if ~isempty(text); set(self.statbar.bar, 'text', text); end
            if nargin==2; return; end % In case we forget the second argument
            if ~isempty(vals)% Means we're updating the progress bars...
                if length(vals) == 4
                    set(self.statbar.bar.ProgressBar, 'Value', vals(2))
                    set(self.statbar.shotBar, 'Value', vals(4))
                elseif length(vals) == 1 % We're done: hide them again
                    set(self.statbar.shotBar, 'Visible', false);
                    set(self.statbar.bar.ProgressBar, 'Visible', false);
                elseif length(vals) == 2 % We're starting: let's show them
                    set(self.statbar.bar.ProgressBar, 'Visible', true, 'Minimum', 1, 'Maximum', vals(1));
                    set(self.statbar.shotBar, 'Visible', true, 'Minimum', 1, 'Maximum', vals(2));
                elseif length(vals) == 3 % We're updating shot field only!!!
                    set(self.statbar.shotBar, 'Value', vals(2), ...
                    'Minimum', 1, 'Maximum', vals(1));
                end
            end
        end
        
        function selectAll(self, h, ~)
            id = h.Tag;
            if strcmp(id, 'all')
                %self.fig.tableMain.Data(:,1) =  {true};%cell(true(size(self.fig.tableMain.Data(:,1))));
                logm = logical(cell2mat(self.dat.tabData(:,5)));
                self.dat.tabData(logm,1) = {true};
            else
                %self.fig.tableMain.Data(:,1) = {false};%(size(self.fig.tableMain.Data(:,1)));
                logm = logical(cell2mat(self.dat.tabData(:,5)));
                self.dat.tabData(logm,1) = {false};
            end
            self.updateMainTable(-10);
        end
        
        function setExtract(self, h, ~)
            % Toggles the extraction style
            tag = h.Tag;   v = h.Value;    
            if v % This one was selected
                if strcmp(tag, 'SaveFlashShots')
                    self.fig.uiSaveRunShots.Value = 0;
                else
                    self.fig.uiSaveFlashShots.Value = 0;
                end
            else
                if strcmp(tag, 'SaveFlashShots')
                    self.fig.uiSaveRunShots.Value = 1;
                else
                    self.fig.uiSaveFlashShots.Value = 1;
                end
            end
        end
        
        function delete(self, ~, ~)
            self.saveSetup(1);
            delete(self.fig.parent);
        end
    end
end