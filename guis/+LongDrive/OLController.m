
classdef OLController < uiextrasX.VBox
    % For the M600 style stage, with openloop control.
    % Accuracy is reasonable, about 50um across the entire travel
    % Closed loop control soon to follow.
    
    
    properties
        serial_port
        serial_address
        serial_open_close
        serial_obj
        
        position
        calfile = [];
        calib
    end
    
    methods
        function o = OLController(varargin)
            if (nargin==0) Parent = figure;
            elseif (nargin==1) Parent = varargin{1}; end;
            o@uiextrasX.VBox('Parent', Parent);
            defer(o)
            mainbox = o;
            if strcmp(get(Parent, 'Type'), 'figure')
                set(o.Parent, 'MenuBar', 'none', 'NumberTitle', 'off', 'Name', 'M600 controller');
                set(o.Parent, 'DefaultUIControlFontSize', 14, 'DefaultAxesFontSize', 10);
                set(o.Parent, 'Units', 'Pixels', 'Position', [500 300 400 200]);
            end
            hbox = uiextrasX.HBox('Parent', mainbox);
            uicontrol('Style', 'text', 'String', 'Serial port:', 'Parent', hbox);
            o.serial_port = uicontrol('Style', 'edit', 'String', 'COM1', 'Parent', hbox);
            uicontrol('Style', 'text', 'String', 'Address', 'Parent', hbox);
            o.serial_address = uicontrol('Style', 'edit', 'String', '00000', 'Parent', hbox);
            o.serial_open_close = uicontrol('Style', 'togglebutton', 'String', 'OPEN', 'Parent', hbox,...
                'Callback', @(obj,val)connect(o, get(o.serial_open_close, 'Value')));
            hbox = uiextrasX.HBox('Parent', mainbox);
            %
            %uicontrol('Style', 'text', 'String', 'Speed', 'Parent', hbox);
            %o.speed = javaNumSpinner2(1000, 50, 10000, 50, hbox, 'StateChangedCallBack', @(obj,val)set_speed(o,val));
            %uicontrol('Style', 'text', 'String', 'Microsteps', 'Parent', hbox);
            %o.usteps = uicontrol('Style', 'popupmenu', 'String', {'1' '2' '4' '8' '16' '32' '64'}, 'Value', 3,...
            %   'Parent', hbox, 'Callback', @(obj,val)set_usteps(o, get(o.usteps, 'Value')));
            %hbox = uiextrasX.HBox('Parent', mainbox);
            %o.direction = uicontrol('Style', 'togglebutton', 'String', '+', 'Parent', hbox, 'Callback', @(obj,val)set_dir(o,get(o.direction, 'Value')));
            %o.drive_button = uicontrol('Style', 'togglebutton', 'String', 'DRIVE', 'Parent', hbox, 'Callback', @(obj,val)drive(o,get(o.drive_button, 'Value')));
            uicontrol('Style', 'text', 'String', 'Target:', 'Parent', hbox);
            o.position.target = uicontrol('Style', 'edit', 'String', '0.000', 'Parent', hbox, 'FontSize', 26);
            uicontrol('Style', 'pushbutton', 'String', 'GO', 'Parent', hbox, 'Callback', @(s,e)gototarget(o));
            hbox.Sizes = [-1 200 -1];
            hbox = uiextrasX.HBox('Parent', mainbox);
            %uicontrol('Style', 'text', 'String', 'Position:', 'Parent', hbox);
            %o.current = javaNumSpinner2(0.5,0.1,20,0.1,hbox);
            uicontrol('Style', 'pushbutton', 'String', 'Get position', 'Parent', hbox,...
                'Callback', @(s,e)get_position(o));
            o.position.current = uicontrol('Style', 'text', 'Parent', hbox, 'String', '0.000', 'FontSize', 26);
            uicontrol('Style', 'pushbutton', 'String', 'HOME', 'Parent', hbox, 'Callback', @(s,e)home(o));
            hbox.Sizes = [-1 200 -1];
            resume(o)
            if nargin==2
                o.calfile = varargin{2};
                loadstage(o,0);
            end
            assignin('base', 'StepperController', o);
        end
        
        function connect(o,val)
            ser_port = get(o.serial_port, 'String');
            if val
                try
                    o.serial_obj = serial(ser_port);
                    set(o.serial_obj, 'InputBufferSize', 2048);
                    set(o.serial_obj, 'BaudRate', 9600);
                    set(o.serial_obj, 'DataBits', 8);
                    set(o.serial_obj, 'Parity', 'none');
                    set(o.serial_obj, 'StopBits', 1, 'Timeout', 1);
                    set(o.serial_obj, 'Terminator', 'CR');
                    fopen(o.serial_obj);
                    set(o.serial_open_close, 'String', 'CLOSE');
                    fprintf('Serial port %s is now %s \n', ser_port, o.serial_obj.status);
                    % Set parameters:
                    if isempty(o.calfile)
                        loadstage(o,1);
                    end;
                    cmd = [sprintf('%sV%i', get_address(o), o.calib.speed) char(13)];
                    fprintf(o.serial_obj, cmd); fgetl(o.serial_obj);
                    cmd = [sprintf('%sE%i', get_address(o), o.calib.usteps) char(13)];
                    fprintf(o.serial_obj, cmd); fgetl(o.serial_obj);
                    cmd = [sprintf('%sC%i', get_address(o), o.calib.current/0.1) char(13)];
                    fprintf(o.serial_obj, cmd); fgetl(o.serial_obj);
                    cmd = [sprintf('%sI%i', get_address(o), 1) char(13)];
                    fprintf(o.serial_obj, cmd); fgetl(o.serial_obj);
                    cmd = [sprintf('%sR%i', get_address(o), o.calib.ramp) char(13)];
                    fprintf(o.serial_obj, cmd); fgetl(o.serial_obj);
                    cmd = [sprintf('%sP%i', get_address(o), 0) char(13)];
                    fprintf(o.serial_obj, cmd); fgetl(o.serial_obj);
                catch
                    fprintf('Opening port %s failed\n', ser_port);
                    instrreset;
                    fprintf('Running instrreset. Try again.\n');
                    %set(o.serial_open_close, 'Value', 0);
                end
            else
                try
                    fprintf('Trying to close serial %s\n', ser_port);
                    fclose(o.serial_obj);
                    delete(o.serial_obj);
                catch
                    fprintf('Not able to close serial %s\n', ser_port);
                end
                set(o.serial_open_close, 'String', 'OPEN');
            end
        end
        
        function address = get_address(o)
            addr = bin2dec(get(o.serial_address, 'String'));
            if addr<16 && addr>=0
                address = char(65+addr);
            elseif addr>15 && addr<=32
                address = char(81+addr);
            end
        end
        
        function moveAbs(o, posn)
            if strcmp(o.serial_obj.status, 'open')
                if posn<0 || posn>o.calib.max % Avoids driving to silly places
                    return;
                end
                counts = round(posn*o.calib.cal);
                cmd = [sprintf('%sM%i', get_address(o), counts) char(13)];
                fprintf(o.serial_obj, cmd);
                pause(0.05);
                set(o.position.target, 'String', num2str(posn, '%3.3f'));
                %fgetl(o.serial_obj);
            else
                fprintf('Port %s still closed', o.serial_port);
            end
        end
        
        function gototarget(o)
            target = str2double(get(o.position.target, 'String'));
            moveAbs(o, target);
            while isMoving(o)
                pause(0.25);
            end
            get_position(o);
        end
        
        function get_position(o)
            try
                pos = getPosn(o);
                set(o.position.current, 'String', num2str(pos, '%3.3f'));
            catch
                fprintf('Failed to get position from drive %s\n', get_address(o));
            end
        end
        
        function pos = getPosn(o)
            cmd = [sprintf('%sP', get_address(o)) char(13)];
            fprintf(o.serial_obj, cmd);
            pause(0.05)
            %fgetl(o.serial_obj);
            va = fgetl(o.serial_obj);
            while o.serial_obj.BytesAvailable~=0
                va = fgetl(o.serial_obj);
            end
            counts = str2double(char(va(2:end)));
            pos = counts/o.calib.cal;
            %o.serial_obj.BytesAvailable
        end
        
        function r = isMoving(o)
            tt = true;
            warning('off','MATLAB:serial:fgetl:unsuccessfulRead');
            while tt
                %cmd = [sprintf('%sP', get_address(o)) char(13)];
                %fprintf(o.serial_obj, cmd);
                lin = fgetl(o.serial_obj);
                tt = isempty(lin);
                r = true;
                pause(0.1);
            end
            r = false;
        end
        
        function home(o)
            % Set the counter to something large, then try to move to 0.
            % Limit switch will stop motion and there's our zero.
            if strcmp(o.serial_obj.status, 'open')
                cmd = [sprintf('%sP%i', get_address(o), 16000000) char(13)];
                fprintf(o.serial_obj, cmd); fgetl(o.serial_obj);
                cmd = [sprintf('%sM%i', get_address(o), 0) char(13)];
                fprintf(o.serial_obj, cmd);
                pause(0.05);
                while isMoving(o)
                    pause(0.25);
                end
                cmd = [sprintf('%sP%i', get_address(o), 0) char(13)];
                fprintf(o.serial_obj, cmd);
                fgetl(o.serial_obj);
                disp('Homed!');
                get_position(o);
            else
                fprintf('Port %s still closed', o.serial_port);
            end
        end
        
        function loadstage(o,flag)
            if flag % means no cal file given
                defpath = fileparts(which('LongDrive.OLController'));
                defaul = [defpath '/M600_1.mat'];
                [filename, pathname] = uigetfile(defaul, 'Pick a calibration file');
                o.calfile = [pathname '\' filename];
            end
            r = load(o.calfile);
            fset = r.fset;
            o.calib = fset;
        end
        
        function delete(o)
            if get(o.serial_open_close, 'Value')
                fprintf('Automatically closing COM port!\n')
                connect(o,0);
            end
        end
    end
end
