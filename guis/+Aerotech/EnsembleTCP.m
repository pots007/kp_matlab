% Should control the Aerotech Ensemble MP controller
% Needs manual input for limits
% Uses self-written command set to work over TCP port

classdef EnsembleTCP < uiextrasX.VBox
    
    properties
        serial
        settings
        
    end
    
    methods
        function o = EnsembleTCP(parent)
            if strcmp(get(parent, 'Type'), 'figure')
                figure(parent);
                set(parent, 'MenuBar', 'none', 'HandleVisibility', 'callback', 'NumberTitle', 'off', 'Name', 'Aerotech Ensemble Motion','DefaultUIControlFontSize', 14); % callback
                set(parent, 'Position', [400 400 500 120]);
            end
            o@uiextrasX.VBox('Parent', parent);
            vbox = uiextrasX.VBox('Parent', o);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('style', 'text', 'String', 'IP:', 'Parent', hbox);
            o.serial.name = uicontrol('Style', 'edit', 'String', '192.168.10.72', 'Parent', hbox);
            uicontrol('style', 'text', 'String', 'Port:', 'Parent', hbox);
            o.serial.port = uicontrol('Style', 'edit', 'String', '5555', 'Parent', hbox);
            o.serial.openclose = uicontrol('Style', 'togglebutton', 'String', 'OPEN', 'Parent', hbox,'Callback', @(obj,val)connect(o, get(o.serial.openclose, 'Value')));
            hbox.Sizes = [-1 150 -1 -1 -1];
            hbox = uiextrasX.HBox('Parent', vbox);
            %uicontrol('String', 'Target', 'FontSize', 30, 'Parent', hbox, 'Style', 'text');
            o.settings.current.target = uicontrol('String', '0.000', 'FontSize', 30, 'Parent', hbox, 'style', 'edit', 'Callback', @(s,e) GoToTarget(o));
            %uicontrol('string', 'Current', 'FontSize', 30, 'Parent', hbox, 'style', 'text');
            o.settings.current.box = uicontrol('style', 'text', 'Parent', hbox, 'FontSize', 30, 'String', '0.000');
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('style', 'pushbutton', 'Parent', hbox, 'String', 'HOME', 'Callback', @(s,e)homeStage(o));
            
            
        end
        
        function connect(o,val)
            IP = get(o.serial.name, 'String');
            port = str2double(get(o.serial.port, 'String'));
            if val
                try
                    o.serial.socket = tcpip(IP, port, 'NetworkRole', 'client');
                    set(o.serial.socket, 'Terminator', 'LF');
                    fopen(o.serial.socket);
                    set(o.serial.openclose, 'String', 'CLOSE');
                    %set_initial_state(o);
                    fprintf('TCP port %s is now %s \n', IP, o.serial.socket.status);
                catch
                    fprintf('Opening port %s failed\n', IP);
                    instrreset;
                    fprintf('Running instrreset. Try again.\n');
                    set(o.serial.openclose, 'Value', 0);
                end
            else
                try
                    fprintf('Trying to close TCP %s\n', IP);
                    fclose(o.serial.socket);
                    delete(o.serial.socket);
                catch
                    fprintf('Not able to close TCP %s\n', IP);
                end
                set(o.serial.openclose, 'String', 'OPEN');
            end
        end
        
        function GoToTarget(o)
            tar = str2double(get(o.settings.current.target, 'String'));
            moveAbs(o, tar);
            for i=1:1
                isMoving(o)
            end
            %while isMoving(o)
                pause(0.1)
            %end
        end
        
        function moveAbs(o, posn)
            set(o.settings.current.target, 'String', num2str(posn));
            if strcmp(o.serial.socket.Status, 'open')
                cmd = ['M ' num2str(posn) char(10)];
                fprintf(o.serial.socket, cmd);
                pause(0.1)
                o.serial.socket.BytesAvailable;
                rep = fgetl(o.serial.socket);
                pause(2.5)
                %disp(char(rep'));
                set(o.settings.current.box, 'String', num2str(posn));
            else
                disp([get(o.serial.name, 'String') ' still closed.']);                
            end
        end
        
        function r = isMoving(o)
            if strcmp(o.serial.socket.Status, 'open')
                cmd = ['STATUS\n' char(10)];
                fprintf(o.serial.socket, cmd);
                pause(0.1)
                %o.serial.socket.BytesAvailable
                rep = fread(o.serial.socket, o.serial.socket.BytesAvailable);
                disp((rep'));
                %la = str2num(cast(rep', 'char'));
                
                %r = dec2bin2(cast(rep', 'char'));
                r = 0;
            else
                disp([get(o.serial.name, 'String') ' still closed.']);
            end
        end
        
        function homeStage(o)
            if strcmp(o.serial.socket.Status, 'open')
                cmd = ['HOME'];
                fprintf(o.serial.socket, cmd);
                rep = fgetl(o.serial.socket)
            else
                disp([get(o.serial.name, 'String') ' still closed.']);                
            end
        end
        
        function delete(o)
            connect(o,0);
        end
        
    end
end