' ------------------------------------------------
' -------------- TCPServer.ab --------------------
' ------------------------------------------------
' 
' This program uses the COM command interface 
' to perform TCP/IP server communications 
' with a remote client.
'
' This program displays the flexibility of the
' COM commands by using a DEFINE statement to
' specify the communications channel. 
' 
' Note: Before running this program, set up the
' parameters for each communications channel. 
' If the parameters are appropriately set up,
' the channel numbers are interchangeable.
' For example, use Channel 0 for RS-232 
' instead of Ethernet.
'
' This example opens a socket and echoes
' all received data back to the client device.
'
' The parameter settings on Ethernet Socket 2 are:
'
' Socket2Port = 5555
' Socket2Setup = 0x1 (TCP server)
' Socket2TransmissionSize = 0
' ------------------------------------------------

HEADER

DEFINE CommChannel 2    ' Define for channel - 2 = Ethernet Socket 2

END HEADER


PROGRAM

DIM DataString AS STRING(20)
DIM PosString AS STRING(20)
DIM tempInSize AS INTEGER
DIM PosSize AS INTEGER
DIM ExitFlag AS INTEGER = 0
DIM Posn AS DOUBLE
DIM StrIndex AS INTEGER
DIM InChar as INTEGER
DIM CharIndex AS INTEGER = 0
DIM ReadStrg as STRING
DIM Status AS INTEGER
DIM StatString AS STRING

' DWELL 5    ' If this program is autorun, you must provide   
           ' time for the Ethernet code to start. 
CLOSECOM CommChannel
DWELL 1
OPENCOM CommChannel

WHILE ExitFlag = 0

    ' Wait until data is received.
	tempInSize = READCOMCOUNT(CommChannel)

    ' If data is received, echo it back.
	IF tempInSize > 0 THEN
        ' Echo back any strings that are received.
        READCOM CommChannel, DataString, tempInSize
		
        ' Watch for the exit string.
        IF DataString = "EXIT" THEN
            ExitFlag = 1
        END IF
		
		' moveAbs command calls this
		IF DataString(0) = "M" THEN
			StrIndex = 2
			CharIndex = 0
			WHILE (StrIndex NE tempInSize)
        		InChar = DataString(StrIndex)
        		ReadStrg(CharIndex) = InChar
        		CharIndex = CharIndex + 1
				StrIndex = StrIndex + 1
    		WEND
			ReadStrg(CharIndex) = 0
			Posn = CDBL(ReadStrg)
			MOVEABS X Posn
			WRITECOM CommChannel, "Done\n"
		END IF
		
		IF DataString(0) = "H" THEN
			HOME X
			WRITECOM CommChannel, "Homed\n"
		END IF
		
		IF DataString(0) = "S" THEN
			Status = AXISSTATUS(X)
			FORMAT StatString, "%i", INTV:Status
			WRITECOMINTEGER CommChannel, 1, Status
			'WRITECOM CommChannel, StatString
		END IF
		
		'WRITECOM CommChannel, DataString
    END IF

WEND

CLOSECOM CommChannel    ' Close communications port.

END PROGRAM

FUNCTION GetDoubleValue() AS DOUBLE

    DIM InChar as INTEGER
    DIM CharIndex as INTEGER
	DIM ReadStrg as STRING
    
    CharIndex = 0
    InChar = 0
    
    WHILE (InChar NE 10) AND (InChar NE 13)   ' Read for a line feed  
                                              ' or carriage return
                                              ' character.
        InChar = READCOMCHAR(2)
        WRITECOMCHAR 2,InChar
        ReadStrg(CharIndex) = InChar
        CharIndex = CharIndex + 1
    WEND

    ReadStrg(CharIndex) = 0            ' Terminate with a null
                                       ' character.
    
    GetDoubleValue = CDBL(ReadStrg)
   
END FUNCTION