% Should control the Aerotech Ensemble MP controller
% Needs manual input for limits
% Uses TCP/UDP/RS232 - commands are all the same it seems

classdef EnsembleRS232 < uiextrasX.VBox
    
    properties
        serial
        settings
        
    end
    
    methods
        function o = EnsembleRS232(parent)
            if strcmp(get(parent, 'Type'), 'figure')
                figure(parent);
                set(parent, 'MenuBar', 'none', 'HandleVisibility', 'callback', 'NumberTitle', 'off', 'Name', 'Aerotech Ensemble Motion','DefaultUIControlFontSize', 14); % callback
                set(parent, 'Position', [400 400 500 80]);
            end
            o@uiextrasX.VBox('Parent', parent);
            vbox = uiextrasX.VBox('Parent', o);
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('style', 'text', 'String', 'COM port:', 'Parent', hbox);
            o.serial.name = uicontrol('Style', 'edit', 'String', 'COM1', 'Parent', hbox);
            o.serial.openclose = uicontrol('Style', 'togglebutton', 'String', 'OPEN', 'Parent', hbox,'Callback', @(obj,val)connect(o, get(o.serial.openclose, 'Value')));
            hbox = uiextrasX.HBox('Parent', vbox);
            %uicontrol('String', 'Target', 'FontSize', 30, 'Parent', hbox, 'Style', 'text');
            obj.settings.current.target = uicontrol('String', '00000', 'FontSize', 30, 'Parent', hbox, 'style', 'edit', 'Callback', @(s,e) GoToTarget(o));
            %uicontrol('string', 'Current', 'FontSize', 30, 'Parent', hbox, 'style', 'text');
            obj.settings.current.box = uicontrol('style', 'text', 'Parent', hbox, 'FontSize', 30, 'String', '0.000');
            hbox = uiextrasX.HBox('Parent', vbox);
            uicontrol('style', 'pushbutton', 'Parent', hbox, 'String', 'HOME', 'Callback', @(s,e)homeStage(o));
            
            
        end
        
        function connect(o,val)
            ser_port = get(o.serial.name, 'String');
            if val
                try
                    o.serial.port = serial(ser_port);
                    set(o.serial.port, 'InputBufferSize', 2048, 'BaudRate', 9600, ...
                        'DataBits', 8, 'Parity', 'none',...
                        'StopBits', 1, 'Terminator', 'CR');
                    fopen(o.serial.port);
                    set(o.serial.openclose, 'String', 'CLOSE');
                    %set_initial_state(o);
                    fprintf('Serial port %s is now %s \n', ser_port, o.serial.port.status);
                    
                catch
                    fprintf('Opening port %s failed\n', ser_port);
                    instrreset;
                    fprintf('Running instrreset. Try again.\n');
                    set(o.serial.openclose, 'Value', 0);
                end
            else
                try
                    fprintf('Trying to close serial %s\n', ser_port);
                    fclose(o.serial.port);
                    delete(o.serial.port);
                catch
                    fprintf('Not able to close serial %s\n', ser_port);
                end
                set(o.serial.openclose, 'String', 'OPEN');
            end
        end
        
        function GoToTarget(o)
            tar = str2double(get(o.settings.current.target, 'String'));
            moveAbs(o, tar);
            while isMoving(o)
                pause(0.1)
            end
        end
        
        function moveAbs(o, posn)
            set(o.settings.current.target, 'String', num2str(posn));
            if strcmp(o.serial.port.Status, 'open')
                cmd = ['MOVEABS X ' num2str(posn) ' LF 200' char(10)];
                fprintf(o.serial.port, cmd);
                rep = fgetl(o.serial.port);
                set(o.settings.current.box, 'String', num2str(posn));
            else
                disp([get(obj.serial.name, 'String') ' still closed.']);                
            end
        end
        
        function r = isMoving(o)
            if strcmp(o.serial.port.Status, 'open')
                cmd = ['AXISSTATUS X' char(10)];
                fprintf(o.serial.port, cmd);
                rep = fgetl(o.serial.port);
                
                if rep==3 % 3 is for isActive
                    r = true;
                else
                    r = false;
                end
            else
                disp([get(obj.serial.name, 'String') ' still closed.']);
            end
        end
        
        function homeStage(o)
            if strcmp(o.serial.port.Status, 'open')
                cmd = ['HOME X' char(10)];
                fprintf(o.serial.port, cmd);
                rep = fgetl(o.serial.port);
            else
                disp([get(obj.serial.name, 'String') ' still closed.']);                
            end
        end
        
        function delete(o)
            connect(o,0);
        end
        
    end
end