function h = javaSlider(min,max, val, Parent, varargin)

if ~exist('Parent','var')
    Parent=gcf;
end

jSlider = javax.swing.JSlider(min, max, val);
if feature('HGUsingMATLABClasses')
    [h,c] = javacomponent(jSlider, [10,10,60,20], (Parent));
else
    [h,c] = javacomponent(jSlider, [10,10,60,20], double(Parent));
end
set(h,varargin{:});
set(c,'Tag','javax.swing.Slider','UserData',h);