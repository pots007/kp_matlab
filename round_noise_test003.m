% test of new NLDenoising technique 

% uses slow scheme so we can better control denoising 
% uses round patches...
% ... so we can rotate each patch and choose the best angle

%% loading the image
%clear all;
%close all;

input_data.dir1 = '/Users/nelsonlopes/YYY_Astra_2013/Data/';
%input_data.dir2 = '20131206/20131206r002/';
%input_data.dir3 = '20131206r002s';
input_data.dir4 = '_x-ray.tif';

input_data.dir2 = '20131210/20131210r008/';
input_data.dir3 = '20131210r008s';

jj=50;
%jj = 24;

this_image = imread([input_data.dir1,input_data.dir2,input_data.dir3,num2str(jj,'%1.3u'),input_data.dir4]);
this_image = double(this_image);

liini = 200;
lifin = 300;
coini = 200;
cofin = 300;

this_image = this_image(liini:lifin,coini:cofin);

[imageheight,imagewidth] = size(this_image);

%figure(1), imagesc(this_image), colormap(gray), title('original Image'), drawnow;

%haircut
figure(2)
subplot(5,5,1), imagesc(this_image),colormap(gray), title('original image'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;

this_better_image = haircut2D_R1(this_image,1.05);
this_better_image = haircut2D_R3(this_better_image,1.05);
removed_noise = this_image-this_better_image; 

subplot(5,3,2), imagesc(this_better_image),colormap(gray), title('hair cutted image'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;
subplot(5,3,3), imagesc(removed_noise),colormap(gray), title('removed noise on haircut'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;

this_better_image = haircut2D_R1(this_better_image,1.05);
this_better_image = haircut2D_R3(this_better_image,1.05);
removed_noise = this_image-this_better_image; 

subplot(5,3,5), imagesc(this_better_image),colormap(gray), title('hair cutted image'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;
subplot(5,3,6), imagesc(removed_noise),colormap(gray), title('removed noise on haircut'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;

this_better_image = haircut2D_R1(this_better_image,1.05);
this_better_image = haircut2D_R3(this_better_image,1.05);
removed_noise = this_image-this_better_image; 

subplot(5,3,8), imagesc(this_better_image),colormap(gray), title('hair cutted image'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;
subplot(5,3,9), imagesc(removed_noise),colormap(gray), title('removed noise on haircut'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;

this_better_image = haircut2D_R1(this_better_image,1.05);
this_better_image = haircut2D_R3(this_better_image,1.05);
removed_noise = this_image-this_better_image; 

subplot(5,3,11), imagesc(this_better_image),colormap(gray), title('hair cutted image'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;
subplot(5,3,12), imagesc(removed_noise),colormap(gray), title('removed noise on haircut'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;

this_better_image = haircut2D_R1(this_better_image,1.05);
this_better_image = haircut2D_R3(this_better_image,1.05);
removed_noise = this_image-this_better_image; 

subplot(5,3,14), imagesc(this_better_image),colormap(gray), title('hair cutted image'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;
subplot(5,3,15), imagesc(removed_noise),colormap(gray), title('removed noise on haircut'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;

%% slow_non_linear_denoise

grande = 4;
pequeno = 2;

w1coef = 0.5;
sigmacoef = 50;


[denoised_image,acc_weight] = ...
FNLD2D_Slow_RoundV3(this_better_image,grande,pequeno,w1coef,sigmacoef);


figure(5)
subplot(1,4,1), imagesc(this_better_image),colormap(gray), title('image to be denoised'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;
subplot(1,4,2), imagesc(denoised_image),colormap(gray), title('denoised image'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;
subplot(1,4,3), imagesc(acc_weight),colormap(gray), title('accumulated weight'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;
subplot(1,4,4), imagesc(this_better_image-denoised_image),colormap(gray), title('removed noise'), set(gca,'DataAspectRatio',[1,1,1]), colorbar, drawnow;







