function new_im = haircut2D_R1(im,ratio);


[v h]=size(im);


if nargin < 2, ratio = 1.1; max_points_to_cut = round(v*h*0.02); end
if nargin < 3, max_points_to_cut = round(v*h*0.02); end

ratiopos = ratio;
rationeg = 1/ratio;

k = 0;
l = 0;
m = 1/8;

w = [k,k,k,k,k,k,k; ...
     k,l,l,l,l,l,k; ...
     k,l,m,m,m,l,k; ...
     k,l,m,0,m,l,k; ...
     k,l,m,m,m,l,k; ...
     k,l,l,l,l,l,k; ...
     k,k,k,k,k,k,k];
 
am = weigthed_average(im,w);
bm = im./am;

h1 = (bm > ratiopos);
h2 = (bm<rationeg);
hair = h1+h2;
ok = ones(v,h)-hair;

new_im = ok.*im + hair.*am;
end

function wav_im = weigthed_average(image,w)
  
  [v h]   = size(image);
  [wv wh] = size(w);

  padded_im = padarray(image,[(wv-1)/2,(wh-1)/2],'symmetric','both');
  
  [pv ph]=size(padded_im); 
  
  am = zeros(v,h);
  
  for lili = 1:wv
    for coco = 1:wh
      am = am + w(lili,coco)*padded_im(lili:lili+v-1,coco:coco+h-1);    
    end
  end
  
  wav_im = am;
  
end
  
