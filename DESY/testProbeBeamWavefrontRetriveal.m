% testProbeBeamWavefrontRetriveal.m
%
% Analyse the test data I took on the probe line to see whether this
% retrieval method actually works or not

compName = getComputerName;
if compName ~= 'kristjan-latitude-e7470'
    datfol = 'C:\Users\kpoder\Work\HollowCoreFibre\WaveFrontTest';
else
    datfol = '/media/kristjan/01D3414399A79F70/Users/kpoder/Work/HollowCoreFibre/WaveFrontTest';
end
flist = dir(fullfile(datfol, '*.raw'));
verbose_setup = 1;
plotFlag = 1;
makeGIF = 0;

ROI1 = [80 80 200 200];
ROI2 = [430 170 200 200];
imm = ReadRAW16bit(fullfile(datfol, flist(3).name), 659, 494, 'DESY');
[y, x] = size(imm);
dx = 9.5e-6; % Resolution of the images
xGrid = dx*(1:x);
yGrid = dx*(1:y);
% Set up a suitable grid for FFTs
npnts = 2^9; % 512 points ought ot be enough!
imGrid = ((-(0.5*npnts-1):0.5*npnts) - 0.5)*0.5*dx;
if verbose_setup
    imagesc(imm)
    rectangle('Position', ROI1);
    rectangle('Position', ROI2, 'EdgeColor', 'r');
    set(gca, 'Clim', [0 1000]);
end
if plotFlag
    hfig = figure(5231); clf(hfig);
    set(hfig, 'Position', [200 50 1200 600], 'Color', 'w');
    locs = GetFigureArray(4, 2, [0.05 0.05 0.05 0.04], 0.08, 'across');
    clear axx;
    for k=1:8; axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'Box', 'on'); end
end

%% Actual algorithm of sorts
%    Filter, to get rid of hotspots;
inImage = imm(ROI1(2):ROI1(2)+ROI1(4), ROI1(1):ROI1(1)+ROI1(3));
inGridx = (1:size(inImage,2))*dx;
inGridy = (1:size(inImage,1))*dx;
cc = centroidlocationsmatrix(inImage)*dx;
inImage = medfilt2(inImage);
% Remove BG!
bgregion = imm(400:end, :);
inImage = inImage - mean(bgregion(:));
if plotFlag
    imagesc(inGridx-cc(1), inGridy-cc(2), inImage, 'Parent', axx(1));
    axis(axx(1), 'image');
    ylabel(axx(1), 'Input plane');
    title(axx(1), 'Measured spot')
end
% Now set up the image, as ready to be propagated. Make it centered on the
% pupil.
inImage_ = interp2(inGridx-cc(1), inGridy-cc(2),...
    inImage, imGrid, imGrid', 'linear', 0);
% And now make some BG! :)
imcnts = inImage_(inImage_~=0);
[N, pix] = histcounts(imcnts(:), 1000); pix = pix(1:end-1) + mean(diff(pix));
% Fit Gaussian to BG level, ie pixels with values of 2000 or less
pix_logm = pix<2000;
bgFit = fit(pix(pix_logm)', N(pix_logm)', 'gauss1');
% And now fill the zero locations with data from the fitted distribution.
inImage_lin = inImage_(:);
nZeros = sum(inImage_lin==0);
bgFills = bgFit.c1.*randn(nZeros,1) + bgFit.b1;
inImage_lin(inImage_lin==0) = bgFills;
inImage_ = reshape(inImage_lin, npnts, npnts);
if plotFlag
    imagesc(imGrid, imGrid, sqrt(abs(inImage_)), 'Parent', axx(2));
    axis(axx(2), 'image');
    title(axx(2), 'Measured spot')
end
% Now prepare the output image

% First sort out filename, if we will save stuff later on
outImage = imm(ROI2(2):ROI2(2)+ROI2(4), ROI2(1):ROI2(1)+ROI2(3));
% Filter, to get rid of hotspots;
outImage = medfilt2(outImage);
% Remove BG!
outImage = outImage - mean(bgregion(:));
if plotFlag
    imagesc(inGridx, inGridy, outImage, 'Parent', axx(5));
    axis(axx(5), 'image');
    ylabel(axx(5), 'Output plane');
    title(axx(5), 'Measured spot')
end
% Now set up the image, as ready to be propagated. Make it centered on the
% pupil.
cc = centroidlocationsmatrix(outImage);
outImage_ = interp2(inGridx-cc(1)*dx, inGridy-cc(2)*dx, outImage, imGrid, imGrid', 'linear', 0);
if plotFlag
    imagesc(imGrid, imGrid, sqrt(abs(outImage_)), 'Parent', axx(6));
    axis(axx(6), 'image');
    title(axx(6), 'Measured spot')
end

% And now the actual method... or so
deltaZ = 25e-3; % This is very rough... unfortunately
inFresnel_new = sqrt(abs(inImage_)); % Field is sqrt of intensity!!
pause(1);
for it = 1:50
    tic;
    outFresnel = (fresnelPropagator(inFresnel_new, dx, dx, deltaZ, 800e-9));
    outFresnel_amp = abs(outFresnel);
    outFresnel_ang = angle(outFresnel);
    if plotFlag
        imagesc(imGrid, imGrid, outFresnel_amp, 'Parent', axx(7)); axis(axx(7), 'image');
        title(axx(7), 'Amplitude');
        imagesc(imGrid, imGrid, outFresnel_ang, 'Parent', axx(8)); axis(axx(8), 'image');
        title(axx(8), 'Phase');
    end
    % Now replace the amplitude of the forward propagated image with that
    % of the actual image
    outFresnel_new = sqrt(abs(outImage_)).*exp(1j*outFresnel_ang);
    inFresnel = fresnelPropagator(outFresnel_new, dx, dx, -deltaZ, 800e-9);
    inFresnel_amp = abs(inFresnel);
    inFresnel_ang = angle(inFresnel);
    if plotFlag
        imagesc(imGrid, imGrid, inFresnel_amp, 'Parent', axx(3)); axis(axx(3), 'image');
        title(axx(3), sprintf('Amplitude after %i iterations', it));
        imagesc(imGrid, imGrid, inFresnel_ang, 'Parent', axx(4)); axis(axx(4), 'image');
        title(axx(4), sprintf('Phase after %i iterations', it));
    end
    % And now replace the amplitude of the initial image with the actual
    % intensity.
    inFresnel_new = sqrt(abs(inImage_)).*exp(1j*inFresnel_ang);
    drawnow;
    %pause(0.3);
    linkaxes(axx([2,3,4,6,7,8]), 'xy')
    fprintf('Iteration %2i took %2.2f s\n', it, toc);
    if plotFlag && makeGIF
        % Capture the plot as an image
        frame = getframe(hfig);
        im = frame2im(frame);
        [imind,cm] = rgb2ind(im,256);
        % Write to the GIF File
        if it == 1
            imwrite(imind,cm,[fname '.gif'],'gif', 'Loopcount',inf);
        else
            imwrite(imind,cm,[fname '.gif'],'gif','WriteMode','append');
        end
    end
end
%set(axx(2), 'XLim', [-20 20]*1e-6, 'YLim', [-20 20]*1e-6); drawnow;
export_fig(fname, '-png', '-nocrop', hfig);


