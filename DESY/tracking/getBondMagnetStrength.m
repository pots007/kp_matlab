% getBondMagnetStrength.m
%
% Function that returns a fraction of the field strength, as compared to
% one of the measured ones.

function Bratio = getBondMagnetStrength(Iref, Inew)

%Iref = 250; Inew = 130;
fdat = load(fullfile(fileparts(which('make_path_Kris.m')), 'DESY', 'tracking', ...
    'DESY_Bond_0_311A.mat'));
Bref = interp1(fdat.Magnet(:,1), fdat.Magnet(:,2), Iref);
Bnew = interp1(fdat.Magnet(:,1), fdat.Magnet(:,2), Inew);
Bratio = Bnew/Bref;