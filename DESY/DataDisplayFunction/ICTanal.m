% ICTanal.m

function Q = ICTanal(trace)
Q = 1;
try
    bg = mean(trace(1:570));
    tracebg = trace - bg;
    Q = 0.0167* sum(tracebg(582:600));
catch err
    disp(err.message);
end
