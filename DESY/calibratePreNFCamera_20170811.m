% calibratePreNFCamera_20170811.m
%
% Script to try to come up with a calibration for the NF diode...

datfol = 'C:\Users\kpoder\ownCloud\Experiments\2017_LWFA_campaign\PreNF_calibration';
flist_csv = dir(fullfile(datfol, '*.cvs'));

for f = 1
    fdat = getFileText(fullfile(datfol, flist_csv(f).name));
    fdata = cellfun(@(x) str2double(x), fdat);
    fdata = fdata(~isnan(fdata));
    [~, fname, ~] = fileparts(flist_csv(f).name);
    flist_ims = dir(fullfile(datfol, fname, '*.pgm'));
    % Now, I think the last number before the extension is the Id of the
    % trigger since it was started...
    fnames = {flist_ims.name}';
    ftriggers = zeros(size(fnames));
    for k=1:length(fnames)
        indss = strfind(fnames{k}, '_');
        [~, fnam, ~] = fileparts(fnames{k});
        ftriggers(k) = str2double(fnam(indss(end)+1:end));
    end
    % Let's get all the images now, and find the total signal...
    BGreg = [1:200 1000:1280];
    totCounts = zeros(size(ftriggers));
    for k=1:length(fnames)
        fid = fopen(fullfile(datfol, fname, fnames{k}), 'r');
        fseek(fid, 16, 0);
        imm = reshape(fread(fid, 1280*960, 'uint8'), 1280, 960)';
        fclose(fid);
        %imagesc(imm)
        BG = mean(mean(imm(:, BGreg)));
        totCounts(k) = sum(sum(imm - BG));
    end
    
    % Sooo, what's happening is that some files are saved at a later time.
    % But, thankfully, the trigger number is still sequential. So, we need
    % to order the shots in the order of ftimes, which holds the trigger
    % numbers.
    [ftriggers_sort, indss] = sort(ftriggers);
    totCounts_sort = totCounts(indss);
    % And filter out the ones that are too low
    logm = totCounts_sort>mean(totCounts);
    totCounts_sort = totCounts_sort(logm);
    ftriggers_sort = ftriggers_sort(logm);
    % This data is messed up :p
    logm2 = ftriggers_sort - ftriggers_sort(1)+1;
    logm2 = logm2(logm2<600);
end



