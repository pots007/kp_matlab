% plot20171109r005_monoEn.m

date_ = 20171109;
run_ = 5;
shot_ = 3;

imm = getEspec3RAWimage(date_, run_, shot_);
%imm = medfilt2(imm);
imm = RemoveHardHits(imm);
%%
fpath = parseShotTimeDESY(date_, run_, shot_);

[rootfol, shotstr] = fileparts(fpath);
enfile = fullfile(rootfol, 'ElectronSpectrometer3', 'rescaled_images', ...
    'energy_axis.mat');
datf = load(enfile);
enax = datf.energy;
mfile = fullfile(rootfol, 'ElectronSpectrometer3', 'rescaled_images', ...
    [shotstr '_ElectronSpectrometer3.mat']);
datf = load(mfile);
plot(enax, sum(datf.warpedimage, 1))
set(gca, 'XLim', [50 200]);

%%
espec = Analysis.Diagnostic(20171109, 5, 'ElectronSpectrometer3');
