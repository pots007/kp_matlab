% analyse20171208_EspecStability.m
%
% Look at the 190 odd run of Espec stability

%  First initialise the Espec diagnostic
calFile =  fullfile('/media/kpoder/KRISMOOSE6/28mReferences',...
    ['Espec3ref_fit180A_20171212CalibData.mat']);
BGim = fullfile('/media/kpoder/KRISMOOSE6/28mdata', '20171208',...
    '20171208r004', 'ElectronSpectrometer3', '20171208r004s005_ElectronSpectrometer3.png');
espec = Analysis.EspecDiagnostic(20171208, 8, 'ElectronSpectrometer3', calFile);
espec.setBackgroundImage(BGim);
espec.debugFlag = 1;
% And now the MPA2 nearfield
MP2NF = Analysis.Diagnostic(20171208, 8, 'MP2NF');
MP2NF.setAnalysisFunction(@(x) sum(x(:)));
MP2NF.debugFlag = 1;
% And now the pressures.
press = Analysis.Diagnostic(20171208, 8, 'GasJetPressure');
% And the DAMOOOOOOON
damon = Analysis.Diagnostic(20171208, 8, 'DAMON_CHARGE');
damon.setAnalysisFunction(@(x) getDamonCharge20gb(max(x(:,2))));
damon_all = damon.getAllAnalysed();

hfig = figure(783); clf(hfig);
ax = axes('Parent', hfig);
%plot(ax, espec.energyAxis.screen, espec.getElectronSpectrum(40, 3, 1))
%drawnow;
%allRAW = espec.getAllBinnedRAW();
allSpec = espec.getAllSpectra(3, 1);
allMP2NF = MP2NF.getAllAnalysed();

% Convert to linear grid...
newEnAx = linspace(40, 200, 500);
newAllSpec = zeros(500, size(allSpec,2));
for k=1:size(allSpec,2)
    newAllSpec(:,k) = interp1(espec.energyAxis.screen, allSpec(:,k), newEnAx);
end
imagesc(1:192, newEnAx, newAllSpec)
xlabel('Shot number');
ylabel('Energy / MeV');

%% Plot average spectra
lm = true(size(allMP2NF)); lm(1:7) = 0;
allSpec_c = newAllSpec(:,lm);
allMP2NF_c = allMP2NF(lm);
[allMP2NF_s, inds] = sort(allMP2NF_c);
allSpec_s = allSpec_c(:,inds);

hfig = figure(783); clf(hfig); hfig.Position = [1200 400 750 500];
ax = axes('Parent', hfig);
%imagesc(1:184, newEnAx, allSpec_s);
%gg = plot(newEnAx, mean(allSpec_s,2))
hh = shadedErrorBar(newEnAx, allSpec_s', {@mean,@std});
set(hh.mainLine, 'LineWidth', 2, 'Color', [0,0.4470,0.741]);
set(hh.patch, 'FaceColor', hh.mainLine.Color, 'FaceAlpha', 0.2);
set(hh.edge, 'Color', hh.mainLine.Color);
set(ax, 'Box', 'on', 'LineWidth', 2, 'Layer', 'top', 'XLim', [40 175]);
xlabel(ax, 'Energy / MeV');
ylabel(ax, {'Electrons per MeV per 0.1%' 'energy spread / arb. units'});
setAllFonts(hfig, 16);
%saveas(hfig, fullfile(Analysis.getAnalysisRootFol, '20171208r008_EnergySpectra.svg'), 'svg');
% also plot histogram of charge in the same plot?
%% Now plot the spectra ordered by charge...
Qs = sum(allSpec_c, 1);
[~, inds] = sort(Qs);
hfig = figure(784); clf(hfig); hfig.Position = [1200 200 600 700];
ax = axes('Parent', hfig);
%imagesc(newEnAx, 1:length(Qs), allSpec_c(:,inds)');
imagesc(newEnAx, 1:length(Qs), normaliseMatrix(allSpec_c(:,inds)'));
set(ax, 'Layer', 'top', 'LineWidth', 2, 'XLim', [40 160]);
xlabel('Energy / MeV');
ylabel('Shot number');
title('Spectra, ordered by charge')
cb = colorbar;
ylabel(cb, 'Electrons per MeV per 0.1% energy spread / arb. units');
colormap(jet_white(256));
setAllFonts(hfig, 16);
make_tex(hfig);
%saveas(hfig, fullfile(Analysis.getAnalysisRootFol, '20171208r008_EnergySpectraOrdered.svg'), 'svg');

%% Plot charge analysis of this day
damon_all_u = damon_all(damon_all>1);
hfig = figure(34876); clf(hfig);
axx = makeNiceAxes(hfig);
h1 = plot(damon_all_u, 'o');
ha = area([0 length(damon_all_u)+1],...
    [1 1]*(mean(damon_all_u)+1*std(damon_all_u)), 'BaseValue', mean(damon_all_u)-std(damon_all_u));
hm = plot([0 length(damon_all_u)+1], [1 1]*mean(damon_all_u), '--');
set(ha.BaseLine, 'LineStyle', 'none')
set(ha, 'FaceAlpha', 0.2, 'FaceColor', h1.Color, 'EdgeColor', 'none');
set(hm, 'LineWidth', 2, 'Color', h1.Color);
set(axx, 'YLim', [35 65], 'XLim', [0 length(damon_all_u)+1]);
xlabel(axx, 'Shot');
ylabel(axx, 'Charge / pC');
text(62,63.5, sprintf('Q = %2.0f \\pm %1.0f pC', mean(damon_all_u), std(damon_all_u)));
make_tex(hfig);
setAllFonts(hfig, 16);
%saveas(hfig, fullfile(Analysis.getAnalysisRootFol, '20171208r008_ChargeStability.svg'), 'svg');

%% I should also show spectra:
% But using the top 10% of charge density, to reduce the fluctuations due
% to pointing off sideways.