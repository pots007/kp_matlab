% cameraServerTimingHelper.m
%
% Small function to display a table with the most recently acquired EventID
% from different camera servers.
%
% KP, DESY, Feb 2018

classdef cameraServerTimingHelper < handle
    
    properties
        fig
        doocsList
        timer
    end
    
    methods
        function o = cameraServerTimingHelper
            
            o.doocsList.addr = {'FLASH.DIAG/FFWDDIAG2.CAM/MP2FF/IMAGE_EXT',...
                'FLASH.DIAG/FFWDDIAG2.CAM/MP2NF/IMAGE_EXT',...
                'FLASH.DIAG/FLASHFWDCAM2.CAM/TargetChamberPreNF/IMAGE_EXT',...
                'FLASH.DIAG/FLASHFWDCAM2.CAM/TargetChamberPreFF/IMAGE_EXT',...
                'FLASH.DIAG/FLASHFWDCAM2.CAM/ProbeInterferometer/IMAGE_EXT',...
                'FLASH.DIAG/FLASHFWDCAM3.CAM/TargetChamberProfile/IMAGE_EXT',...
                'FLASH.DIAG/FLASHFWDCAM4.CAM/ElectronSpectrometer1/IMAGE_EXT',...
                'FLASH.DIAG/FLASHFWDCAM4.CAM/ElectronSpectrometer2/IMAGE_EXT',...
                'FLASH.DIAG/FLASHFWDCAM4.CAM/ElectronSpectrometer3/IMAGE_EXT',...
                'FLASH.DIAG/FLASHFWDCAM8.CAM/GrenoulliTemporal/IMAGE_EXT',...
                'FLASH.DIAG/FFWD.SPECTRUM/S06096/SPECTRUM',...
                'FLASH.DIAG/SIS8300DMA/FWDDIAG2.0/CH01.TD',...
                'FLASH.DIAG/FFWD.ADC/DAMON_SIGNAL/CH00.TD'};
            o.doocsList.name = cell(size(o.doocsList.addr));
            for k=1:length(o.doocsList.name)
                ii = strsplit(o.doocsList.addr{k}, '/');
                o.doocsList.name{k} = fullfile(ii{2}, ii{3});
            end
            
            tabData = zeros(length(o.doocsList.name), 20);
            % Graphics first
            o.fig.hfig = figure(3251); clf(o.fig.hfig);
            o.fig.htab = uitable(o.fig.hfig, ...
                'Units', 'normalized', 'Position', [0.02 0.02 0.96 0.9],...
                'RowName', o.doocsList.name',...
                'ColumnWidth', num2cell(70*ones(1,20)), ...
                'ColumnName', 'numbered', 'Data', tabData);
            o.fig.startButton = uicontrol('Parent', o.fig.hfig, 'Style', 'Checkbox', ...
                'Units', 'normalized', 'Position', [0.02 0.93 0.2 0.06],...
                'String', 'Poll DOOCS', 'Value', 0, 'Callback', @o.startTimer);
            o.fig.singleButton = uicontrol('Parent', o.fig.hfig, 'Style', 'Pushbutton',...
                'Units', 'normalized', 'Position', [0.25 0.93 0.1 0.06],...
                'String', 'Update one shot', 'Callback', @o.updateTable);
            o.timer = timer('Period', 1, 'ExecutionMode', 'fixedRate', 'TimerFcn', @o.updateTable);
        end
        
        function startTimer(o,e,~)
            if e.Value % Turned on, start timer..
                disp('Starting polling');
                start(o.timer);
            else
                disp('Stopped.');
                stop(o.timer);
            end
        end
        
        function updateTable(o, ~, ~)
            warning('off', 'doocsread:processChannels:general_error');
            newDat = - ones(length(o.doocsList.addr), 1);
            for d=1:length(o.doocsList.addr)
                dat = doocsread(o.doocsList.addr{d});
                %fprintf('%s read: %s\n', o.doocsList.name{d}, dat.error);
                if isstruct(dat.data) && isfield(dat.data, 'event')
                    newDat(d) = dat.data.event;
                else
                    if isfield(dat, 'timestamp') && ~isnan(dat.timestamp)
                        newDat(d) = dat.systemstamp;
                    end
                end
                
            end
            datt = fliplr(int32(get(o.fig.htab, 'Data')));
            datt(:,end+1) = int32(newDat);
            o.fig.htab.Data = fliplr(datt(:,2:end));
        end
    end
end