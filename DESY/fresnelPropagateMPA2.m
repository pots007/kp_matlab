% fresnelPropagateMPA2.m
%
% Try to Fresnel propagate the nearfield of MPA2 to see if hotshots are
% likely to develop.

imfol = 'C:\Users\kpoder\Work\MPA2_NF';
dataf = load(fullfile(imfol, 'NF_KP'));

savePlots = 0;
%% One field, many distances

hfig = figure(89991); clf(hfig);
locs = GetFigureArray(3, 3, [0.05 0.05 0.01 0.01], 0.04, 'across');
clear axx;

phaseFlag = 0;

if phaseFlag
    totPhase = rand(659, 494)*0.3;
else
    totPhase = zeros(659, 494);
end

dx = 1e-4;
xax = (1:494)*dx*100;
yax = (1:659)*dx*100;

immID = 123;
for i=1:9
    axx(i) = axes('Parent', hfig, 'Position', locs(:,i));
    imm = dataf.imagesCell{1,immID};
    if i==1
        imagesc(xax, yax, imm); 
    else
        propimm = fresnelPropagator(double(imm).*exp(1i*totPhase), dx, dx, (2^(i-1)-1), 800e-9);
        imagesc(xax, yax, abs(propimm)); drawnow;
    end
    axis image;
    title(sprintf('z=%i m', 2^(i-1) - 1));
end
set(axx, 'XTick', [], 'YTick', []);
set(hfig, 'Position', [2100 150 800 950]);

if savePlots
    export_fig('C:\Users\kpoder\ownCloud\Experiments\2017_FFWD\MPA2_NF_Fresnel', '-pdf', '-nocrop', hfig);
end