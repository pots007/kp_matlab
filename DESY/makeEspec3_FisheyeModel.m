% makeEspec3_FisheyeModel.m
%
% This will look into making a matlab own FishEye model for the removal of
% the effect.



datfol = '/media/kpoder/KRISMOOSE6/28mReferences/EspecReferences_20171211';
flist = dir(fullfile(datfol, '*.raw'));
for k=1:length(flist)
    imm = ReadRAW16bit(fullfile(datfol, flist(k).name), 1920, 400, [], 'DESY');
    [~,fstem,~] = fileparts(flist(k).name);
    imwrite(uint16(imm), fullfile(datfol, [fstem '.png']));
end
%%
flist = dir(fullfile(datfol, '*.png'));
imm = imread(fullfile(datfol, flist(6).name));
imm = imrotate(imm,180);
% Crop it a little bit...
subimm = imm(95:290, 200:1730);
% Let's try to binarise it a tad... should make corner life easier...
subimm2 = subimm;% < 50;

hfig = figure(23511); clf(hfig);
%axx = gca; set(axx, 'NextPlot', 'add', 'Box', 'on', 'CLim', [0 750]);
imagesc(subimm2)
axis image
set(gca, 'CLim', [0 250], 'NextPlot', 'add');
corners = corner(subimm2,500, 'QualityLevel', 0.01, 'SensitivityFactor', 0.04);
plot(corners(:,1), corners(:,2), 'r*')

%% Try it with soomeone else's code....
datfol = '/media/kpoder/KRISMOOSE6/28mReferences/EspecReferences_20171122';

flist = dir(fullfile(datfol, '*.png'));
imm = imread(fullfile(datfol, flist(8).name));

fhig = figure(12342); clf(hfig);
ax1 = subplot(2,1,1);

imagesc(imm); colorbar;

ax2 = subplot(2,1,2);
imm2 = lensdistort(imm, -1e-1, 'bordertype', 'fit');
imagesc(imm2); colorbar
set([ax1 ax2], 'CLIm', [0 700]);

%% Try just to find the center of the curvature and unfisheye using that
datfol = '/media/kpoder/KRISMOOSE6/28mReferences/EspecReferences_20171122';
flist = dir(fullfile(datfol, '*.png'));
imm = double(imread(fullfile(datfol, flist(8).name)));

hfig = figure(2394824); clf(hfig);
subimm = imm(85:130, :);
imagesc(subimm)
set(gca, 'NextPlot', 'add', 'CLim', [0 500]);
for k=1:100:size(subimm,2)
    plot(subimm(:,k)/max(subimm(:,k))*100 + k,1:size(subimm,1),  '-r')
end

% And now ginput some points... too tricky to do it automatically I think
[X,Y] = ginput;
%% Now try to fit a circle to these points....
[xc,yc,R,a] = circfit(X,Y+85);

hfig = figure(89); clf(hfig);
imagesc(imm);
set(gca, 'nextplot', 'add', 'CLim', [0 500]);
t = 0:0.001:(2*pi);
plot(cos(t)*R + xc, sin(t)*R + yc, '-r', 'LineWidth', 0.2);
%%
fhig = figure(12342); clf(hfig);
ax1 = subplot(2,1,1);

imagesc(imm); colorbar;

ax2 = subplot(2,1,2);
imm2 = removeFishEye(imm, 13);
imagesc(imm2); colorbar
set([ax1 ax2], 'CLIm', [0 400]);