% testFROGSpectrum.m
%
% Script to figure out how to measure spectrum from FROG trace...

if ismac
    datfol = '/home/fflab28m/Python/Pauls/lazor/FROG/';
else
    datfol = '/media/kpoder/DATA/OldSpectrum/';
end

fdat = load(fullfile(datfol, '20170518_specs.mat'));

lambdaax = fdat.lambda;
% A wee bit of smsoothing never hurt anyone...
spec_l_raw = smooth(fdat.spec1);
% Find the BG
logm = lambdaax < 740 | lambdaax > 860;
bg = fit(lambdaax(logm)', spec_l_raw(logm), 'poly7');
plot(lambdaax, spec_l_raw, lambdaax, bg(lambdaax), 'r'); % Check the bg...

spec_l = smooth(spec_l_raw - bg(lambdaax));
spec_l(logm) = 0;
spec_l = sqrt(spec_l); % We measure the intensity, not field!!!!
plot(lambdaax, spec_l)

% Now convert to omega space
omegas = 2*pi*c./(lambdaax*1e-9);
spec_w_raw = (2*pi*c)./(lambdaax*1e-9).^2 .* spec_l';
plot(omegas, spec_w_raw);

% Now onto a uniform grid we go
npnts = 2^11;
omegaax = linspace(1.9, 2.8, npnts)*1e15;
spec_w = interp1(omegas, spec_w_raw, omegaax);
plot(omegaax, spec_w);

% And applying no spectral phase, get the temporal shape:
tt = fftX(omegaax, spec_w, npnts*1);
disp(tt(:, 1))
pulse_int = abs(tt(:,2)).^2;
plot(tt(:,1), pulse_int);
fwhm_val = widthfwhm(pulse_int)*mean(diff(tt(:,1)));
fprintf('Fourier limitet FWHM: %2.1f fs\n', fwhm_val*1e15);
xlim([-150 150]*1e-15);

% Now get the FROG trace
frogim = FROGCalc(tt(:,2));

% And calculate the spectrum from FROG image:
spec_frog = sum(frogim, 2);
plot(omegaax, spec_w./max(spec_w), omegaax, spec_frog./max(spec_frog));

% Plot the AC and FWHM of FROG trace
pulse_frog = sum(frogim, 1);
pulse_AC = xcorr(pulse_int, pulse_int);
pulse_AC = pulse_AC(1024:1024+2047);
plot(tt(:,1), pulse_frog./max(pulse_frog), tt(:,1), pulse_AC/max(pulse_AC), 'x');
xlim([-150 150]*1e-15);

%% Nice plotting

hfig = figure(412233); clf (hfig);
hfig.Position = [500 200 1050 700];
locs = GetFigureArray(3, 2, [0.04 0.03 0.07 0.07], [0.05 0.1], 'down');
clear axx;
axx(1) = makeNiceAxes(hfig, locs(:,1));
plot(axx(1), lambdaax, spec_l_raw);
xlabel(axx(1), 'Wavelength / nm');
ylabel(axx(1), 'Spectral intensity / a.u.');
title(axx(1), 'Raw spectrometer data');

axx(2) = makeNiceAxes(hfig, locs(:,2));
plot(axx(2), lambdaax, spec_l);
xlabel(axx(2), 'Wavelength / nm');
ylabel(axx(2), 'Spectral intensity / a.u.');
title(axx(2), 'Clean spectrometer data');

axx(3) = makeNiceAxes(hfig, locs(:,3));
plot(axx(3), omegaax*1e-15, spec_w);
ylabel(axx(3), 'Spectrum / a.u.');
xlabel(axx(3), 'Angular frequency / rad/fs');
title(axx(3), 'Spectrum');

axx(4) = makeNiceAxes(hfig, locs(:,4));
imagesc(tt(:,1)*1e15, omegaax*1e-15, frogim, 'Parent', axx(4));
xlabel(axx(4), 'Delay / fs');
ylabel(axx(4), 'Angular frequency / rad/fs');
title(axx(4), 'FROG trace');
colormap(axx(4), jet_white(256));
set(axx(4), 'Layer', 'top', 'XLim', [-1 1]*250, 'YLim', [1.8 2.8]);

axx(5) = makeNiceAxes(hfig, locs(:,5));
hh = plot(axx(5), omegaax*1e-15, spec_w./max(spec_w),...
    omegaax*1e-15, spec_frog./max(spec_frog));
legend(hh, {'Analytical', 'FROG'}, 'Box', 'off', 'Location', 'NW');
xlabel(axx(5), 'Angular frequency / rad/fs');
ylabel(axx(5), 'Spectral intensity / a.u.');
title(axx(5), 'Comparing spectra');

axx(6) = makeNiceAxes(hfig, locs(:,6));
hh = plot(axx(6), tt(:,1)*1e15, pulse_frog./max(pulse_frog),...
    tt(:,1)*1e15, pulse_AC/max(pulse_AC), 'x');
legend(hh, {'Analytical', 'FROG'}, 'Box', 'off', 'Location', 'NW');
set(axx(6), 'XLim', [-1 1]*150);
xlabel(axx(6), 'Delay / fs');
ylabel(axx(6), 'Intensity / a.u.');
title(axx(6), 'Comparing autocorrelations');

export_fig('~/work/FROG_spectrum', '-pdf', '-nocrop', hfig);
%% Sanity check:
ww = ifftX(tt(:,1), tt(:,2), npnts);
plot(omegaax, spec_w./max(spec_w), omegaax, abs(ww(:,2))./max(abs(ww(:,2))))