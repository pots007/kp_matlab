% makeThorlabsDiodeComplaint.m
%
% Plot the measured beam profile to send to Thorlabs and complain

imloc = '/home/kpoder/ownCloud/Experiments/2017_LaserLabCW';
im = imread(fullfile(imloc, '20180221_LDM785_BeamShape_acA1300-30gm.png'));

imd = medfilt2(double(im));
cval = exp(-1)*max(imd(:));
cc = contourc(imd, cval*[1 1]);

% Filter out the correct contour:
inds = find(cc(1,:) > 0.99*cval & cc(1,:) < 1.01*cval); % Find indcices of starts of new contours
clens = cc(2,inds); % The lengths of each of these contours
[npnts, ind0] = max(clens);
logm = inds(ind0)+1:inds(ind0)+npnts; 
x = cc(1,logm); y = cc(2, logm);
% Yaaay! This was more work than necessary, though, wasn't it? But it feels
% GOOOOOOOD

hfig = figure(23857); clf(hfig);
ax = makeNiceAxes(hfig);

imlo = imd>max(imd(:))*exp(-1);
imagesc(imd)
ell = fit_ellipse(x, y, ax);

set(findobj('Parent', ax, 'Color', 'r'), 'LineWidth', 2, 'Color', 'k');
set(ax, 'Layer', 'top', 'XTickLabel', [], 'YTickLabel', [])

axis(ax, 'image');
title(ax, sprintf('Fitted ellipse: %2.1f x %2.1f mm', ell.a*3.75e-3, ell.b*3.75e-3))
export_fig(fullfile(imloc, 'BeamSize'), '-pdf', '-nocrop', hfig);