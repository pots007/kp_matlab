% makeProbeSpatialCal.m
%
% Script to extract the spatial calibration from probe imaging setup

datfol = '/media/kristjan/KRISMOOSE6/probe/20171106/';
flist = dir(fullfile(datfol, '*.pgm'));
fnames = {flist.name}';

i = 6;
imm = imread(fullfile(datfol, flist(i).name));
pixAx = 1:size(imm,1);
llines = zeros(size(imm,1), length(flist));
poss = zeros(size(flist));
for i=1:length(flist)
    imm = imread(fullfile(datfol, flist(i).name));
    llines(:,i) = sum(imm(:, 800:900), 2);
    % Will be well dirty here - find the position of 1400 in the image. This is
    % roughly in the middle of the rising...
    poss(i) = find(llines(:,i)>1400, 1, 'last');
end
% Tye y locations:
hexapod_ys = 1e3*cellfun(@(x) str2double(x(3:7)), fnames);
ff = polyfit(poss, hexapod_ys, 1);
fprintf('One pixel corresponds to %2.2f microns\n', abs(ff(1)))