% keepCameraOnline.m
%
% Small script, that sets camera back online if it is seen to be offline

function keepCameraOnline(address)

fprintf('Keeping %s online!!!!\n Control C to exit\n', address);
while 1
    ff = doocsread([address '/START']);
    if isempty(ff); continue; end
    if ~isstruct(ff); continue; end
    if isnan(ff.data); continue; end
    if ~ff.data
        disp(datestr(now));
        stat = doocswrite([address '/START'], 1);
    end
    pause(0.1);
end