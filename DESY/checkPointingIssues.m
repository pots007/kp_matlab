% checkPointingIssues.m
%
% Check what happens to spot whwen the tilt chnges by small amounts

npnts = 2^10;
boxD = 0.03; % Box half width
r1 = 0.003; % Small diode beam
R2 = 0.02; % Full beam

% Tip is equal to 2*r*cos(theta), where r is radius is aperture and theta
% is radial angle.

% Say we move by 2 cm, 18m away - total deviation angle is
thet = 0.02/18; % Or about 1 mrad.

% Set up box axes
ax = linspace(-boxD, boxD, npnts);
[X,Y] = meshgrid(ax, ax);
R = sqrt(X.^2 + Y.^2);
thet_r = atan2(Y,X);
%R = R./R2; % Radius normalised to larger aperture
tip_full = 2*(R/R2).*cos(thet_r);

% And now do some far fields:

mask_small = (R<r1);
mask_big = R<R2;
spot_small = fftshift(fft2(mask_small.*exp(1j*tip_full.*mask_small)));
spot_big = fftshift(fft2(mask_big.*exp(1j*tip_full)));

% Do the graphics:
locs = GetFigureArray(2, 3, [0.1 0.02 0.05 0.05], 0.03, 'across');
clear axx;
hfig = figure(4523); clf(hfig);
for k=1:size(locs,2); axx(k) = axes('Parent', hfig, 'Layer', 'top', 'Position', locs(:,k)); end;
% First the standard ones:
imagesc(abs(fftshift(fft2(mask_small))), 'Parent', axx(1));
imagesc(abs(fftshift(fft2(mask_big))), 'Parent', axx(2));
imagesc(abs(fftshift(fft2(mask_small.*exp(1j*tip_full)))), 'Parent', axx(3));
imagesc(abs(fftshift(fft2(mask_big.*exp(1j*tip_full)))), 'Parent', axx(4));
imagesc(abs(fftshift(fft2(mask_small.*exp(1j*10*tip_full)))), 'Parent', axx(5));
imagesc(abs(fftshift(fft2(mask_big.*exp(1j*10*tip_full)))), 'Parent', axx(6));
lims = 0.5*npnts + [-30 30];
set(axx, 'XLim', lims, 'YLim', lims);