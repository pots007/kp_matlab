% Quick script to figure out a useful current for the diode: want to get 3
% mW of laser power out!

I = [90 80 70];
BGs = [-7.07 -6.8; -6.8 -6.71; -6.71 -6.55]';
signals = [12.8 6.34 1.15];

corr_sig = signals - mean(BGs, 1);

currfit = polyfit(I, corr_sig, 1);

% We want a power of 3 mW:

currtarget = 3;
I_3mW = (currtarget - currfit(2))/currfit(1);

fprintf('The current for %2.1f mW is %2.2f mA\n', currtarget, I_3mW);

