% calibrateFROG.m
%
% Using an interferometer image and a spectrometer trace, gets a pixel
% calibration for the FROG.
%
% KP, DESY, 2018

% Paths to FROG image and spectrometer trace.
% THese are the new files!!!
c = 3*10^8;
kris_pc = 1;
if kris_pc
    dpath = '/media/kpoder/DATA/grenouille/Calibration/20180215_old';
    FROGfile = fullfile(dpath, 'FROGimage01.png');
    specfile = fullfile(dpath, 'FLASH.DIAG_FFWD.SPECTRUM_S08899_1.txt');
    % Also load the current calibration
    load('/media/kpoder/DATA/grenouille/software/FROGed2016/LaserLabGrenouille20.mat')
else
    % This is for the one files on the machine
    dpath = '/home/fflab28m/DATA/grenouille/Calibration/20180517';
    FROGfile = fullfile(dpath, 'GrenoulliTemporal_20180517T130144_750_1695601025.pgm');
    specfile = fullfile(dpath, 'FLASH.DIAG_FFWD.SPECTRUM_S08464_1.txt');
    % Also load the current calibration
    load('/home/fflab28m/Matlab/grenouille/FROGed2016/LaserLabGrenouille20.mat')
end

% Read in spectrometer data, convert to frequency space, transform to find 
% pulse separation.
fdat = importdata(specfile); 
lambdaax = fdat.data(:,1);
% A wee bit of smsoothing never hurt anyone...
spec_l_raw = smooth(fdat.data(:,2));
% Find the BG
logm = lambdaax < 740 | lambdaax > 860;
bg = fit(lambdaax(logm), spec_l_raw(logm), 'poly7');
plot(lambdaax, spec_l_raw, lambdaax, bg(lambdaax), 'r'); % Check the bg...

spec_l = smooth(spec_l_raw - bg(lambdaax));
spec_l(logm) = 0;
spec_l = sqrt(spec_l); % We measure the intensity, not field!!!!
plot(lambdaax, spec_l)

% Now convert to omega space
omegas = 2*pi*c./(lambdaax*1e-9);
spec_w_raw = (lambdaax*1e-9).^2/(2*pi*c) .* spec_l;
plot(omegas, spec_w_raw);

% Now onto a uniform grid we go
npnts = 2^11;
omegaax = linspace(1.9, 2.8, npnts)*1e15;
spec_w = interp1(omegas, spec_w_raw, omegaax);
spec_w = abs(spec_w./max(spec_w));
plot(omegaax, spec_w);
[~,inds] = findpeaks(spec_w, 'MinPeakHeight', 0.5, 'MinPeakDistance', 50, 'Annotate', 'peaks');
delta_omega = mean(diff( omegaax(inds) ));
fprintf('Spectral peak separation of %2.2e rad/fs -> delta t = %2.2f fs\n',...
    delta_omega*1e-15, 2*pi/delta_omega*1e15)
delta_t = 2*pi/delta_omega;
%% Get the temporal shape:
tt = fftX(omegaax, spec_w, npnts*2);
pulse_int = abs(tt(:,2)).^2;
pulse_int = pulse_int./max(pulse_int);
plot(tt(:,1), pulse_int);
fwhm_val = widthfwhm(pulse_int)*mean(diff(tt(:,1)));
fprintf('Fourier limited FWHM: %2.1f fs\n', fwhm_val*1e15);
xlim([-1 1]*250e-15);
findpeaks(pulse_int, 'MinPeakHeight', 0.05, 'Annotate', 'peaks');
[~,inds] = findpeaks(pulse_int, 'MinPeakHeight', 0.05,'MinPeakDistance', 10, 'Annotate', 'peaks');
delta_t = mean(diff(tt(inds,1)));
fprintf('Delta t from FFT = %2.2f fs \n', delta_t*1e15);

%% Now move on to the FROG trace.
FROGim = fliplr(double(imread(FROGfile)));
image(FROGim);

% First temporal axis - FWB is the AC of the pulse
FROG_AC = sum(FROGim, 2);
FROG_AC = FROG_AC/max(FROG_AC);
[~, inds] = findpeaks(FROG_AC, 'MinPeakHeight', 0.4, 'MinPeakDistance', 100, 'Annotate', 'peaks');
delta_t_pix = mean(diff(inds));
newtCal = delta_t*1e15/delta_t_pix;
fprintf(' ---------- Temporal ------------\n');
fprintf('Peak separation on image = %2.2f pixels\n', delta_t_pix);
fprintf('\t => cal is %2.2f fs/pix\n', newtCal);
fprintf('Current cal: %2.2f vs new %2.2f => %2.2f\n', fCal.dt, newtCal, fCal.dt/newtCal);

% For spectrum, take 10 pixel wide region around the peak:
ww = 3;
FROG_spec = smooth(sum(FROGim(inds(2)-ww:inds(2)+ww,:), 1));
FROG_spec = FROG_spec./max(FROG_spec);
FROG_spec(1:170) = 0;
plot(FROG_spec);
findpeaks(FROG_spec, 'MinPeakHeight', 0.3, 'Annotate', 'peaks');
[~, inds] = findpeaks(FROG_spec, 'MinPeakHeight', 0.3, 'MinPeakDistance', 20, 'Annotate', 'peaks');
delta_l_pix = mean(diff(inds));

% Now assume central wavelength of 400nm or twice the fundamental:
omega0 = 2*pi*c/(800*1e-9);
delta_lambda = 2*pi*c*delta_omega./(2*omega0)^2*1e9;
newlCal = delta_lambda / delta_l_pix;

fprintf(' ---------- Spectral ------------\n');
fprintf('Peak separation on image = %2.2f pixels\n', delta_l_pix);
fprintf('\t => cal is %2.4f nm/pix\n', newlCal);
fprintf('Current cal: %2.4f vs new %2.4f => %2.2f\n', fCal.dl, newlCal, fCal.dl/newlCal);

%% Alternative calibration: Try to fit a few peak locations from image to
% their actual wavelength positions.
[~,spec_l_inds] = findpeaks(abs(spec_l/max(spec_l)), 'MinPeakHeight', 0.55, 'Annotate', 'peaks');
% 0.25 as it's second harmonic and then 
spec_fit = fit(inds, 0.25*lambdaax(spec_l_inds), 'poly1');

%fprintf('Current cal: %2.4f vs new %2.4f => %2.2f\n', fCal.dl,...
%   spec_fit.p1, fCal.dl/spec_fit.p1);

% Update the calibration
fCalOld = fCal;
fCal.dt = newtCal;
fCal.dl = newlCal;
%fCal.lCenter = round(0.5*length(FROG_spec));
fCal.lCenter = 2*spec_fit(round(0.5*length(FROG_spec))); % Factro of two to get back to norm lambda
% And print out the change:
fprintf('-----##### Calibration change #####-----\n');
fprintf('%10s\t%12s\t%12s\n', 'Calib', 'Old', 'New');
fprintf('%10s\t%12.4f\t%12.4f\n', 'dl', fCalOld.dl, fCal.dl)
fprintf('%10s\t%12.4f\t%12.4f\n', 'dt', fCalOld.dt, fCal.dt)
fprintf('%10s\t%12.4f\t%12.4f\n', 'lCenter', fCalOld.lCenter, fCal.lCenter)
%save('/home/fflab28m/DATA/grenouille/Calibration/20180517/GrenCalibration.mat', 'fCal');
%save('/home/fflab28m/Matlab/grenouille/FROGed2016/LaserLabGrenouille20180517.mat', 'fCal')