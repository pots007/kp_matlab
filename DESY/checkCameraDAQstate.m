% checkCameraDAQstate.m
%
% Check over the 1e3 shot stability run to see how often these cameras are
% saving.

daqState = checkSavedDataInDAQ(21243, '/home/fflab28m/Kris/ChanList3.xml', 0);
names = {daqState.name};
logm = cellfun(@(x) ~isempty(strfind(x, 'CAM')), names);
locs = GetFigureArray(1, sum(logm), [0.02 0.03 0.02 0.05], 0.05, 'down');
camState = daqState(logm);

hfig = figure(234143); clf(hfig);
for k=1:length(camState)
    ax(k) = makeNiceAxes(hfig, locs(:,k));
    plot(ax(k), camState(k).shotEvents, gradient(camState(k).shotEvents), 'x-');
    title(camState(k).name)
end

linkaxes(ax, 'x')

