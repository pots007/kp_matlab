
if getComputerName == 'kristjan-latitude-e7470'
    datfol = '/home/kristjan/ownCloud/Experiments/2017_LWFAProbe/20171017';
else
    datfol = 'C:\Users\kpoder\Work\2017_Gabi_interferometry\20170912';
end
flist = dir(fullfile(datfol, '*.pgm'));

convertFiles = 0;
if convertFiles
    for k=1:length(flist)
        imm = ReadRAWpgm(fullfile(datfol, flist(k).name));
        imwrite(imm, fullfile(datfol, [flist(k).name(1:end-3) 'png']));
    end
end

fileID =48;

imm0 = double(ReadRAWpgm(fullfile(datfol, flist(fileID).name)));
% The image and FFT space sizes
boxSize = [350, 75];
boxStart = [100, 160];
fftSize = [8, 7];
fftStart = [229, 36];
    
imagesc(imm0)
rectangle('Position', [boxStart, boxSize], 'EdgeColor', 'r');
imm = imm0(boxStart(2):boxStart(2)+boxSize(2), boxStart(1):boxStart(1)+boxSize(1));

fftim = fftshift(fft2(imm));
fftref = fftshift(fft2(refim));

imagesc(log10(abs(fftim)))
rectangle('Position', [fftStart, fftSize], 'EdgeColor', 'r');
%imagesc(log10(abs(fftref)))
%%
maskimm = zeros(size(imm));
rm = [fftStart fftSize];
maskimm(rm(2):rm(2)+rm(4), rm(1):rm(1)+rm(3)) = 1;
%
filtimm = ifft2(fftshift(maskimm.*fftim));
%filtref = ifft2(fftshift(maskimm.*fftref));
%imagesc(log10(abs((maskimm.*fftref))))
%fphase = unwrap(angle(filtimm./filtref), 5.5, 2);
fphaseraw = unwrap(angle(filtimm), 3.5, 2);
sigroi = [1 10 350 65];
zerophase = createBackground(fphaseraw, sigroi, [], [], 2);
fphase = fphaseraw - zerophase; 
imagesc(fphaseraw - zerophase)
%%
clf;
ymid = 40;
axx(1) = subplot(411);
imagesc(imm); colorbar;
axx(2) = subplot(4, 1, 2);
imagesc(fphase); colorbar;
%imagesc(fphaseraw - zerophase2); colorbar;
finalphase = imrotate(fphase, 1.);
ffinalphase = (finalphase);
%ffinalphase(ffinalphase<0) = 0;
finalphase = fliplr((ffinalphase)');
axx(3) = subplot(4, 1, 3);
imagesc(finalphase'); 
set(gca, 'NextPlot', 'add');
plot([0, 2048], [ymid ymid], '-k')
%set(gca, 'CLim', [0 3.5]);
colorbar;
axx(4) = subplot(4, 1, 4);
imagesc(imm)

fSet.ymid = ymid; fSet.micperpix = 14.55; fSet.asmooth = 1e-3;
ne = AbelInversionNew(finalphase, fSet, 'Plasma', 800, 0, 150);
imagesc(fliplr(ne)*1e-6)
colorbar
%set(gca, 'CLim', [0 3]);


micperpix = 0.55;
press = 350; %mbar;