% makeDESY_BondMagnetMapComparison.m
%
% See if we can easily extrapolate the field strength of the Bond magnet
% for currents that have not been measured
%
if strcmp(getComputerName, 'flalap01-ubuntu')
    datpath = '/media/kpoder/KRISMOOSE6/28mReferences/FieldMeasurement';
else 
    datpath = '/home/fflab28m/Kris/References/FieldMeasurement';
end
I_fols = {'BO_180A', 'BO_250A', 'BO_311A'};

% Plot the maps, and ratios to 311A one in all axes
hfig = figure(23513); clf(hfig);
locs = GetFigureArray(2, 3, [0.1, 0.05, 0.05, 0.05], [0.05,0.02], 'down');
clear axs;
for k=1:size(locs,2)
    axs(k) = makeNiceAxes(hfig, locs(:,k));
end
%
for I_id = 1:3

    fpath = fullfile(datpath, I_fols{I_id});
    fname = dir(fullfile(fpath, 'bo2_ypm0*.dat'));
    datf = importdata(fullfile(fpath, fname.name));

    % Now set up all axes:
    z_old = datf(:,3);
    y_old = datf(:,1);
    z_new = min(z_old):max(z_old);
    y_new = min(y_old):max(y_old);
    [Z,Y] = meshgrid(z_new, y_new);
    map_new = griddata(z_old, y_old, datf(:,5), Z, Y);
    imagesc(z_new, y_new, map_new*1000, 'Parent', axs(I_id));
    axis(axs(I_id), 'image');
    cb = colorbar('peer', axs(I_id));
    ylabel(cb, 'B / mT')
    ylabel(axs(I_id), sprintf('B=%s', I_fols{I_id}(4:end)));
    drawnow;
end

% And now find the ratios....
ratios = [1,1,1];
for i=1:3
    mref = get(findobj(axs(3), 'Type', 'image'), 'CData');
    mdat = get(findobj(axs(i), 'Type', 'image'), 'CData');
    rat = mref./mdat;
    imagesc(z_new, y_new, rat, 'Parent', axs(3+i));
    axis(axs(3+i), 'image');
    cb = colorbar('Peer', axs(3+i));
    ylabel(cb, sprintf('B_{311}/B_{%s}', I_fols{i}(4:end-1)));
    ratios(i) = mean(mean(rat(20:200, 400:600)));
end
ratios = fliplr(ratios);

% Make things nice, as always!
set(axs, 'Layer', 'top', 'XTick', [], 'YTick', []);
%export_fig(fullfile(datpath, 'MapRatios'), '-pdf', '-nocrop', hfig)

%% And now for the ratios and fitting...

hfig = figure(33124235); clf(hfig);
ax = makeNiceAxes(hfig);
hh = [];
curratios = [1, 250/311, 180/311];
hh(end+1) = plot(ax, curratios, 1./ratios, 'o', 'MarkerSize', 10);
xlabel(ax, 'Ratio of currents');
ylabel(ax, 'Ratio of average field');

% And fitting :D :D
fitX = 0.0:0.01:1;
f1 = polyfit(curratios, 1./ratios, 1);
hh(end+1) = plot(ax, fitX, polyval(f1,fitX), '-');
f2 = polyfit(curratios, 1./ratios, 2);
hh(end+1) = plot(ax, fitX, polyval(f2,fitX), '-');
f3 = fit(curratios', 1./ratios', @(a,b,x) a*log(b*x));
hh(end+1) = plot(ax, fitX, f3(fitX), '-');
f4 = polyfit([0, curratios], [0, 1./ratios], 1);
hh(end+1) = plot(ax, fitX, polyval(f4,fitX), '-');
set(hh, 'LineWidth', 2);
uistack(hh(1), 'top');
legend(hh, {'Data', 'Linear fit', 'Parabolic fit', 'Logarithmic fit',...
    'Linear w 0 contraint'}, 'Location', 'SE', 'Box', 'off');
set(ax, 'YLim', [0, 1.05]);

%export_fig(fullfile(datpath, 'FieldRatios'), '-pdf', '-nocrop', hfig);