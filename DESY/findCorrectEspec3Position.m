% findCorrectEspec3Position.m
%
% The tracking data in Paul's espec code is useful, but contains a
% discontinuity and is for only current. Ideally I would redo the tracking
% with MagnetTracker, to allow for other currents and understanding the
% screen limits.
%
% Thus we will move the z position and the angle of the screen to match
% Slava's results from Paul's espec folder.
%
% And then, perhaps once people reply to me, I can compare the best results
% to actual, CAD based positions!

%ll = load('DESY2017_Espec3_Settings');  % THIS WAS WRONG
ll = load('DESY2019_Espec3_Settings');  % THIS IS CORRECT
load('owncolourmaps');
datf = importdata('~/python/especdevelopment/ressources/ESPECHigh311A_correctAngle.txt');
poss = datf(:,3)*1000;
enss = datf(:,1);

% The lines below give axial tracking, I think.
ll.dat.Rin = []; ll.dat.pin = [];
fret = track_electrons2(ll.dat);
%% Moving the screen is independent of tracking!
z = linspace(-2, 2, 12);
locs = GetFigureArray(4, 3, [0.04 0.02 0.05 0.04], [0.04 0.07], 'across');
hfig = figure(7365); clf(hfig);
clear axx;

for k = 1:length(z)
    i=1; % Do screen 1
    yl = ll.dat.(sprintf('y%i', i));
    zl = ll.dat.(sprintf('z%i', i)) + z(k);
    Ll = ll.dat.(sprintf('l%i', i));
    thetl = ll.dat.(sprintf('thet%i', i));
    % Find intersection with screen i:
    % Endpoints of screen:
    a = [zl yl]; b = a + Ll*[cosd(thetl) sind(thetl)];
    % Now select central angle
    div = 2;
    els = fret.div(div).el;
    ppp = nan(2,length(els));
    for en = 1:length(els)
        % Look at this particular energy.
        R = els(en).r;
        [zi,yi,iii] = intersections(R(:,3),R(:,2), [a(1) b(1)], [a(2) b(2)]);
        ppp(2,en) = els(en).energy*1.8712e21;
        if ~isempty(zi)
            ppp(1,en) = yi;
        end
    end
    % Convert to distance along the screen
    ppp(1,:) = (-yl + ppp(1,:))/sind(thetl);
    % And fit a spline to allow finding residuals
    logm = ~isnan(ppp(1,:));
    divfit = fit(ppp(1,logm)', ppp(2,logm)', 'spline');
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add');
    set(gca, 'nextPlot', 'add', 'Box', 'on');
    %plot(ppp(1,:), ppp(2,:), 'x');
    h_h(1) = plot(axx(k), -100:250,divfit(-100:250), '-r');
    possc = max(ppp(1,:)) - poss;
    fprintf('z = %1.1f, possc(1) = %2.2f\n', zl, possc(end));
    h_h(2) = plot(axx(k), possc, enss, 'ok');
    % And the residuals
    h_h(3) = plot(axx(k), possc, abs(divfit(possc) - enss)*100, 'd');
    legend(h_h, {'MagnetTracker', 'Slava', 'Error*100'}, 'Location', 'Best')
    xlabel(axx(k), 'Position on screen / mm');
    ylabel(axx(k), 'Energy / MeV');
    title(axx(k), sprintf('Screen z = %2.1f mm', zl));
end

%export_fig('/home/kpoder/matlab/kp_matlab/DESY/tracking/ScreenPosScan_finer', '-pdf', '-nocrop', hfig);