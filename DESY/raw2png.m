% raw2png.m
%
% Small function to convert RAW files into more useable pngs.
%
% KP, DESY, 2018

function raw2png(filename, folderOutputFlag, width, height)

% Parse arguments...
useDESY = nargin < 3;

if nargin<2
    folderOutputFlag = 0;
end
% 
if iscell(filename)
    nFiles = length(filename);
    for f=1:nFiles
        if useDESY
            imm = ReadRAW16bit(filename{f}, [], [], [], 'DESY');
        else
            imm = ReadRAW16bit(filename{f}, width, height);
        end
        writeOutput(imm, filename{f}, folderOutputFlag);
    end
    
else
    if useDESY
        imm = ReadRAW16bit(filename, [], [], [], 'DESY');
    else
        imm = ReadRAW16bit(filename, width, height);
    end
    writeOutput(imm, filename, folderOutputFlag);
end
end


function writeOutput(imm, filename, fFlag)
    [path, fstem, ~] = fileparts(filename);
    assignin('base', 'lal', imm)
    if fFlag    
        if ~exist(fullfile(path, 'png'), 'dir')
            mkdir(fullfile(path, 'png'));
        end
        fname = fullfile(path, 'png', fstem);
    else
        fname = fullfile(path, fstem);
    end
    imwrite(uint16(imm), [fname, '.png'])
end
