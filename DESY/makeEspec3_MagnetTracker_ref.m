% makeEspec3_MagnetTracker_ref.m
%
% This will make a MagnetTrcker compatible reference file for the DESY
% espec3 screen.
mode = 2;
if mode==1 % The old style Espec3
    datt.path = Analysis.getDataRootFol;
    datt.fname = 'reference_ElectronSpectrometer3_dewarp.png';
elseif mode==2 % The newer one, but without fisheye removal
    pp = fileparts(Analysis.getDataRootFol);
    datt.path = fullfile(pp, '28mReferences', 'EspecReferences_20171122');
    datt.fname = 'ElectronSpectrometer3_20171122T110221_718_1543501448.png';
end
datt.ref = imread(fullfile(datt.path, datt.fname));
% Run the getRotCropWrap function!
kk= getRotCropWarp(datt.ref);

%% Try loading a suitable tracking file
ftrack = load('tracking_DESY2017_Espec3_BO_311A.mat');
rr = ftrack.tracking.screen(1);
% Col 1 is energies, col3 is alogn screen for axial electrons
logm = ~isnan(rr.alongscreen(:,3));
datf = importdata('~/python/especdevelopment/ressources/ESPECHigh311A_correctAngle.txt');
hfig = figure(8632); clf(hfig);
set(gca, 'NextPlot', 'add');
% Make a new axis, to reconcile the fact that I have no idea as to where
% the hell the screen starts in real life. NEEEEEED CAD to sort this out
% for good.
newScrAxCalFact = - max(rr.alongscreen(:,3)) - min(datf(:,3))*1000; 
newScrAx =  rr.alongscreen(:,3) + newScrAxCalFact;
plot(-newScrAx, (rr.alongscreen(:,1)), '-x')
plot(datf(:,3)*1000, datf(:,1), 'o-');
trackfit = fit(-newScrAx(logm), rr.alongscreen(logm,1), 'cubicspline');
plot(0:300, trackfit(0:300), '-r');

% BUT if we want any other currents, simply need to load another file,
% subtract the tracking offset and done.
trackCurrent = '180A';
if ~strcmp(trackCurrent, '311A')
    ftrack = load(['tracking_DESY2017_Espec3_BO_' trackCurrent '.mat']);
    rr = ftrack.tracking.screen(1);
    logm = ~isnan(rr.alongscreen(:,3));
    newScrAx =  rr.alongscreen(:,3) + newScrAxCalFact;
    trackfit = fit(-newScrAx(logm)*0.1, rr.alongscreen(logm,1), 'cubicspline');
end

%%
datt.warpdat = kk.dat;
% Now need fields:
% pixels - pixels corresponding to lengths
% This was done before - can just call ginput(2) on a suitable screen
% figure; imagesc(kk.dat.q.I2);
% THis is for the version when I thought the taped edge was the end of the
% screen
%pixs = [1449.9 117; 118.5 114.4];
% This is new nad much better and improved.
%pixs = [1484.2 123.4; 94.9 128];
pixs = [1831 151; 163 152];
%pixs = ginput(2);
datt.pixels = round(pixs);
% lengths - point along screen
datt.lengths = [0 24.5];
% Read in the tracking data from before
%datf = importdata('~/python/especdevelopment/ressources/ESPECHigh311A_correctAngle.txt');
%plot(datf(:,1), datf(:,3), 'x-');
%logm = datf(:,3) < 0.248 & ~isnan(datf(:,3));
% Lengths need to be in cm!!!!!
%datt.tracking = [datf(logm,1), datf(logm,3)*100];
datt.tracking = [rr.alongscreen(logm,1) -newScrAx(logm)];
% Let's do the position fit so it covers more than the screen to avoid the
% tail at the end.
logm2 = ~isnan(datf(:,3));
datt.pfit = trackfit; %fit(datf(logm2,3)*100, datf(logm2,1), 'cubicspline');
datt.efit = polyfit(pixs(:,1), datt.lengths', 1);
Qcalib = 1e-6;

%% Most importantly, save the bloody thing!
fname = fullfile('/media/kpoder/KRISMOOSE6/28mReferences',...
    ['Espec3ref_fit' trackCurrent '_20171212CalibData.mat']);

save(fname, 'datt');
