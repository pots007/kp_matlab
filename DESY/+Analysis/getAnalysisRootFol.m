% getAnalysisRootFol.m
%
% Function to return root path to analysis. If not set on a particular machine,
% will open a GUI to select the folder and then save it.
%
% KP, DESY, Nov 2017

function path = getAnalysisRootFol()
    fpath = fileparts(which('Analysis.Diagnostic'));
    fname = fullfile(fpath, 'rootfol.dat');
    if ~exist(fname, 'file')
        ftemp = uigetdir('.', 'Select root folder for ServerStyle data');
        fid = fopen(fname, 'w');
        fprintf(fid, '%s', ftemp);
        fclose(fid);
    end
    path = importdata(fname);
    path = path{1};
    path = fullfile(fileparts(path), '28manalysis');
end