% MPA2AttenuatorDiagnostic.m
%
% Subclass to ease MPA2 attenuator analysis. Loads the characterisation
% tables of the MPA2 attenuator and sets the Diagnostic AnalysisFunc to
% return the fraction of total energy for a given shot:
%   
% energyFracOfTotal = mp2.getAnalyseddiag(shotNumber)
% or
% energyFracOfTotal = mp2.getMPA2EnergyFraction(shotNumber)
%
% KP, DESY, Nov 2017

classdef MPA2AttenuatorDiagnostic < Analysis.Diagnostic
    properties
        entables
        realEnergyFlag = 1 % If set, returns the ratio of actual energy, not DOOCS
    end
    
    methods
        function self = MPA2AttenuatorDiagnostic(date_, run_)
            self@Analysis.Diagnostic(date_, run_, 'AMP2Energy');
            % These are the default energy settings!!!!
            self.loadEnergyTables;
            self.dataStore.AnalysisFunc = @(x) self.getEnergyValue(x);
        end
        
        function loadEnergyTables(self)
            fpath = fileparts(which('Analysis.MPA2AttenuatorDiagnostic'));
            datf = importdata(fullfile(fpath, 'AMP2_Energy_characterisation.txt'));
            % This fit will take one from MPA2 percent as shown in DOOCS to
            % actual fraction of peak energy.
            self.entables.percent2energyRAW = [datf.data(:,1), datf.data(:,2)];
            self.entables.percent2energyFIT = fit(datf.data(:,1),...
                datf.data(:,2)/datf.data(end,2), 'cubicspline');
            datf = importdata(fullfile(fpath, 'AMP2_Energy_settings.txt'));
            self.entables.motpos2percentRAW = datf;
            self.entables.motpos2percentFIT = fit(datf(:,2), datf(:,1), 'poly1');
        end
        
        function energyFrac = getEnergyValue(self, motorPosition)
            percents = self.entables.motpos2percentFIT(motorPosition);
            energyFrac = self.entables.percent2energyFIT(percents);
        end
        
        function energyFrac = getMPA2EnergyFraction(self, shot)
            energyFrac = self.getAnalyseddiag(shot);
        end
    end
end