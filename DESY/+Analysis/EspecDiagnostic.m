% EspecDiagnostic.m
%
% Class handling an espec diagnostic, derived from base Diagnostic class.
% Methods to import MagnetTracker style reference and to analyse it,
% returning a valid energy axis and spectrum.
%
% KP, DESY, Nov 2017

classdef EspecDiagnostic < Analysis.Diagnostic
    properties
        calibData = [] % Structure holding full structure of calibration
        energyAxis % energy value of each pixel, both raw and linear
    end
    
    methods
        function self = EspecDiagnostic(date_, run_, diagName, calibfile)
            self@Analysis.Diagnostic(date_, run_, diagName);
            % If user provided reference file, open it. Can be set later on
            % as well
            if nargin==4
                self.setCalibration(calibfile);
            end
            self.runInfo.BGfile = [];
        end
        
%%
        function setCalibration(self, calibfile)
            % setCalibration allows the user to specify the espec cal file.
            %
            % The file needs to be of specific format, containing
            % information about the region of the actual screen and
            % parameters to use to account for angled imaging.
            try
                fdat = load(calibfile);
                self.calibData = fdat.datt;
            catch err
                fprintf('Error loading calibration for diag %s: %s\n', ...
                    self.runInfo.diagnostic, err.message);
                return
            end
            self.calibData.medFilt = 1;
            % The energy axis is calibrated for the FULL warped image, the
            % size of which is stored!
            pixelaxis = 1:size(self.calibData.warpdat.inputimage,2);
            scrPos = polyval(self.calibData.efit,pixelaxis);
            self.energyAxis.raw = self.calibData.pfit(scrPos);
            self.energyAxis.linear = linspace(round(min(self.energyAxis.raw)),...
                round(max(self.energyAxis.raw)), 1000);
            % The following screen energyAxis needs to be applicable only
            % to the screen portion of the image. Thus use the same logm.
            q = self.calibData.warpdat.q;
            logmx = q.x>0 & q.x<q.screenx(3);
            self.energyAxis.screen = self.energyAxis.raw(logmx);
        end
        
        function setBackgroundImage(self, fname)
            % setBackgroundImage allows the user to specify BG image.
            %
            % The argument fname should specify an image file of the same
            % dimensions as the data file. This can the be used to subtract
            % from the data image to account for BG.
            % 
            % See also SUBTRACTBACKGROUND. 
            if ~exist(fname, 'file')
                self.runInfo.BG = [];
                fprintf('File %s does not exist.\n', fname)    
                return;
            end
            imm = imread(fname);
            if ~isequal(size(imm), self.dataInfo.dimensions)
                fprintf('BG image size is not correct!\n');
                return;
            end
            self.runInfo.BG = double(imm);
            if ~isempty(self.calibData)
                self.runInfo.warpBG = self.getUnwarpedCroppedDiag(self.runInfo.BG, 0);
            else
                self.runInfo.warpBG = [];
            end
        end
        
        function setMedianFiltering(self, flag)
            self.calibData.medFilt = flag;
        end
        
        function im = getUnwarpedCroppedDiag(self, shot, BGmethod)
            % Return an unwarped and cropped image.
            %
            % This will return an image suitable for further analysis. The
            % argument BGmethod determines the type of background
            % subtraction used:
            %
            %  BGmethod=0 is for no subtraction, image is untouched
            %  BGmethod=1 subtracts a reference image, nothing else
            %  BGmethod=2 uses 'natural' 2d fit over outside signal area
            %  BGmethod=3 uses 'linear' 2d fit over outside signal area
            %
            % See ANALYSIS.ESPECDIAGNOSTIC/SUBTRACTBACKGROUND for implementation of
            % the background subtraction process.
            if numel(shot)==1
                im = double(self.getRAWdiag(shot));
            else
                im = shot;
            end
            if self.calibData.medFilt; im = medfilt2(im); end
            if nargin<3; BGmethod = 2; end
            % And now crop, rotate and warp the image!
            if isempty(self.calibData); return; end
            ss = self.calibData.warpdat.cropsize;
            cropim = im(ss(2):ss(2)+ss(4),ss(1):ss(1)+ss(3));
            rotim = imrotate(cropim, self.calibData.warpdat.thet);
            warpim = imwarp(rotim, self.calibData.warpdat.q.tform, 'cubic', 'Fillvalues', 0);
            warpim = warpim./self.calibData.warpdat.q.J2;
            warpim(isnan(warpim)) = 0;
            warpim = self.subtractBackground(warpim, BGmethod);
            q = self.calibData.warpdat.q;
            logmx = q.x>0 & q.x<q.screenx(3);
            logmy = q.y>0 & q.y<q.screeny(3);
            if self.debugFlag > 4
                hfig = figure(6755); clf(hfig);
                imagesc(q.x, q.y, warpim);
                rectangle('Position', [0 0 q.screenx(3) q.screeny(3)]);
            end
            if numel(shot)==1
                im = warpim(logmy, logmx);
            else
                im = warpim;
            end
        end
        
        function im = getDeconvolvedDiag(self, shot, enScale)
            % Will return an image, with the magnetic effect removed
            % enScale is 0 for raw, 1 for linear
            %
            % STUB!
            if nargin==2; enScale = 0; end
            im = [];
            if enScale
                dE = gradient(self.energyaxis.linear);
            else
                dE = gradient(self.energyaxis.raw);
            end
            dE = [dE(1) dE];
        end
        
        function cleanim = subtractBackground(self, imm, method)
            % subtractBackground removes the background from an image.
            %  Subtracts the background according to the method specified:
            % 
            %  BGmethod=0 is for no subtraction, image is untouched
            %  BGmethod=1 subtracts a reference image, nothing else
            %  BGmethod=2 uses 'natural' 2d fit over outside signal area
            %  BGmethod=3 uses 'linear' 2d fit over outside signal area
            %
            q = self.calibData.warpdat.q;
            
            if method == 0 % Pass image untouched!!!
                cleanim = imm;
                return
            end
            if method==1 % This is subtracting an image
                if isempty(self.runInfo.warpBG)
                    cleanim = imm;
                    return;
                end
                BG = self.runInfo.warpBG;
            end
            % Just to be sure with methods 2 and 3, add some extra padding. 
            % This padding extends the area around the 'signal' region
            % that is cropped out to create a background.
            padWidth = 15;
            logmx = q.x>-padWidth & q.x<(q.screenx(3)+padWidth);
            logmy = q.y>-padWidth & q.y<(q.screeny(3)+padWidth);
            bgim = imm;
            if method==2 || method==3
                bgim(logmy, logmx) = nan;
                % Filter out the padding zeros;
                bgim(bgim==0) = nan;
                [Xq,Yq] = meshgrid(1:size(imm,2), 1:size(imm,1));
                X = Xq(~isnan(bgim)); Y = Yq(~isnan(bgim));
                dat1 = bgim(~isnan(bgim));
                if method==2
                    BG = griddata(X,Y,dat1,Xq,Yq, 'natural');
                else
                    BG = griddata(X,Y,dat1,Xq,Yq, 'linear');
                end
            end
            if self.debugFlag > 5
                hfig = figure(6754); clf(hfig);
                imagesc(bgim);
            end
            cleanim = imm - BG;
        end
        
        function spec = getElectronSpectrum(self, shot, specType, BGmethod)
            % getElectronSpectrum returns an actual energy spectrum.
            %
            % BGmethod specifies the BG subtraction method:
            %
            %  BGmethod=0 is for no subtraction, image is untouched
            %  BGmethod=1 subtracts a reference image, nothing else
            %  BGmethod=2 uses 'natural' 2d fit over outside signal area
            %  BGmethod=3 uses 'linear' 2d fit over outside signal area
            %
            % specType specifies the spectrum returned:
            % 
            %   specType=1 gives dQ/dx, from the image
            %   specType=2 gives dQ/dE, accounting for non-linear dispersion
            %   specType=3 gives dQ/(dE/E), gives spectra per bin width
            if nargin==2 || isempty(specType); specType = 2; end
            if nargin <4 || isempty(BGmethod); BGmethod = 2; end
            im = self.getUnwarpedCroppedDiag(shot, BGmethod);
            spec_raw = sum(im, 1)';
            dE = gradient(self.energyAxis.screen);% dE = [dE(1); dE];
            %[size(spec_raw) size(dE)]
            % specTypes: 1 for dQ/dx, 2 for dQ/dE, 3 for dQ/dE/E
            switch specType
                case 1
                    spec = spec_raw;
                case 2
                    spec = spec_raw./dE;
                case 3
                    spec = spec_raw./dE.*self.energyAxis.screen;
            end
        end
        
        function binneddata = getAllSpectra(self, specType, BGmethod)
            % getAllSpectra returns energy spectra for all shots.
            % If specType and BGmethod not specified, will give dQ/dE and
            % use natural fit over the signal region.
            if nargin==1 || isempty(specType); specType = 2; end
            if nargin <3 || isempty(BGmethod); BGmethod = 2; end
            binneddata = zeros(length(self.energyAxis.screen), self.nShots);
            for shot=1:self.nShots
                if self.debugFlag>0
                    fprintf('Espec %i, run %i: Working on shot %3i\n', ...
                        self.runInfo.date, self.runInfo.run, shot);
                end
                binneddata(:,shot) = self.getElectronSpectrum(self.shotNumbers(shot), specType, BGmethod)';
            end
        end
        
    end
    
end