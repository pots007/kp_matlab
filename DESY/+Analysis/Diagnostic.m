% Diagnostic.m
%
% Base class for one particular diagnostic in a run. Once initialised,
% stores information about the number of shots, shot numbers, shot times
% and EventIDs. Provides base methods to get the RAW diagnostic for one
% shot and for getting a cropped version. Also allows setting crop region
% for the all diagnostics.
%
% KP, DESY, Nov 2017

classdef Diagnostic < handle
    properties
        nShots = 0 % Total number of shots available
        shotNumbers = [] % Shot numbers available
        runInfo % Contains date, run #, DAQ run # and start-end times
        dataInfo % Information about size and properties of data
        dataStore % Contains list of Files etc
        debugFlag = 0 % Flag to allow display of information
    end
    
    methods
%%        
        function self = Diagnostic(date_, run_, diagName)
            % Diagnostic constructor.
            %
            % Diagnostic will set up an object for one particular
            % diagnostic. It will automatically determine whether it's 1D-point,
            % 2D-trace or 3D-image data.
            self.runInfo.date = date_;
            self.runInfo.run = run_;
            self.runInfo.diagnostic = diagName;
            self.dataInfo.rootfol = Analysis.getDataRootFol();
            self.dataInfo.datefol = fullfile(self.dataInfo.rootfol, self.getDateFol);
            self.dataInfo.runfol = fullfile(self.dataInfo.datefol, self.getRunFol);
            flist = dir(fullfile(self.dataInfo.runfol, [diagName '*']));
            if isempty(flist)
                fprintf('Empty diagnostic!\n');
                self.dataInfo.dataType = 0;
                return;
            end
            if flist(1).isdir % This means it's either a trace or image
                flistims = dir(fullfile(self.dataInfo.runfol, diagName, '*.png'));
                flisttxt = dir(fullfile(self.dataInfo.runfol, diagName, '*.txt'));
                if length(flistims) > length(flisttxt) % Full of images
                    self.dataInfo.dataType = 3;
                    flist = dir(fullfile(self.dataInfo.runfol, diagName, '*.png'));    
                else
                    self.dataInfo.dataType = 2;
                    flist = dir(fullfile(self.dataInfo.runfol, diagName, '*.txt'));
                end 
                self.dataStore.Files = fullfile(self.dataInfo.runfol,...
                        diagName, {flist.name}');
                self.nShots = length(self.dataStore.Files);
                nums = cellfun(@(x) regexp(x, 's\d{6}', 'match'), self.dataStore.Files, 'UniformOutput', 0);
                self.shotNumbers = cellfun(@(x) str2double(x{1}(2:end)), nums);
            else
                try
                    self.dataInfo.dataType = 1;
                    datf = load(fullfile(self.dataInfo.runfol, diagName));
                    self.dataStore.data = datf.data;
                    self.dataStore.Files = fieldnames(self.dataStore.data);
                    self.nShots = length(self.dataStore.Files);
                    self.shotNumbers = cellfun(@(x) str2double(x(2:end)), ...
                        self.dataStore.Files);
                catch err
                    fprintf('Unable to load file for diagnostic %s: %s\n', diagName, err.message);
                    self.dataInfo.dataType = 0;
                end
                
            end
            self.dataStore.AnalysisFunc = '';   
            self.dataStore.ROI = [];
            % For future, we need a row vector of shot numbers!
            if iscolumn(self.shotNumbers); self.shotNumbers = self.shotNumbers'; end
            % We also want to know the dimensions of the datafiles!!!
            self.dataInfo.dimensions = size(self.getRAWdiag(self.shotNumbers(1)));
        end
        
%%
        function data_ = getRAWdiag(self, shot)
            % getRAWdiag returns the raw data for this diagnostic for a shot.
            %
            % No analysis is performed, whatever is saved is returned
            % untouched.
            data_ = [];
            if ischar(shot); return; end
            % Find the right index in the file list
            fileInd = find(self.shotNumbers==shot);
            if isempty(fileInd)
                fprintf('Error: shot %i does not exist for diagnostic %s\n',...
                    shot, self.runInfo.diagnostic);
                return;
            end
            switch self.dataInfo.dataType
                case 1
                    % For points
                    data_ = self.dataStore.data.(self.dataStore.Files{fileInd});
                case 2
                    % For traces
                    data_ = importdata(self.dataStore.Files{fileInd});
                    if isstruct(data_)
                        data_ = data_.data;
                    end
                case 3
                    % For images
                    data_ = imread(self.dataStore.Files{fileInd});
            end
        end
        
        function data_ = getAnalyseddiag(self, shot)
            % getAnalyseddiag returns the output from the analysis fn.
            %
            % The output from whatever function the user has set to be used
            % will be returned untouched.
            %
            % See also SETANALYSISFUNCTION for specifics on the function to
            % be used.
            if isempty(self.dataStore.AnalysisFunc)
                return;
            end
            data_ = feval(self.dataStore.AnalysisFunc, self.getRAWdiag(shot));
        end
        
        function data_ = getCroppeddiag(self, shot)
            % getCroppeddiag returns the raw data, cropped to specified size.
            %
            % This assumes the user has run setCropRegion before.
            data_ = self.getRAWdiag(shot);
            if isempty(data_)
                return;
            end
            if isempty(self.dataStore.ROI); return; end
            roi = self.dataStore.ROI;
            % Check for dataype !
            switch self.dataInfo.dataType
                case 2
                    data_ = data_(roi(1):roi(1)+roi(2),:);
                case 3
                    try
                        data_ = data_(roi(2):roi(2)+roi(4), roi(1):roi(1)+roi(3));
                    catch err
                        fprintf('Error cropping diag %s: %s\n', self.runInfo.diagnostic, err.message);
                    end
            end
        end
        
        function binneddata = getAllBinnedRAW(self, ndim)
            if nargin==1; ndim = 1; end
            binneddata = self.getAllBinned(ndim, 0);
        end
        
        function binneddata = getAllBinnedCropped(self, ndim)
            if nargin==1; ndim = 1; end
            binneddata = self.getAllBinned(ndim, 1);
        end
        
        function binneddata = getAllBinned(self, ndim, cropFlag)
            % getAllBinned returns data where every shot is binned.
            %
            % getAllBinned(ndim, cropFlag)
            % 
            % ndim specifies dimension of summation for images, according
            % to usual matlab notation. For traces, this specifies the
            % column of data to be returned.
            % cropFlag specifies whether the returned data should be
            % cropped accoring to the ROI set up earlier.
            % 
            if self.dataInfo.dataType == 1
                % for points, just plot it.
                binneddata = zeros(1, self.nShots);
                for shot=1:self.nShots
                    binneddata(shot) = self.dataStore.data.(self.dataStore.Files{shot});
                end
            else
                % For traces, the ndim param defines the column to plot
                if cropFlag
                    shot1 = self.getCroppeddiag(self.shotNumbers(1));
                else
                    shot1 = self.getRAWdiag(self.shotNumbers(1));
                end
                if self.dataInfo.dataType == 2
                    binneddata = zeros(size(shot1, 1), self.nShots);
                    for i = 1:self.nShots
                        shot = self.shotNumbers(i);
                        if cropFlag
                            shotdata = self.getCroppeddiag(shot);
                        else
                            shotdata = self.getRAWdiag(shot);
                        end
                        binneddata(:,i) = shotdata(:,ndim);
                    end
                else
                    shot1 = shot1';
                    binneddata = zeros(size(shot1, ndim), self.nShots);
                    for i=1:self.nShots
                        if cropFlag
                            shotdata = self.getCroppeddiag(self.shotNumbers(i));
                        else
                            shotdata = self.getRAWdiag(self.shotNumbers(i));
                        end
                        if ndim==1
                            binneddata(:,i) = sum(shotdata,1)';
                        else
                            binneddata(:,i) = sum(shotdata, 2);
                        end
                    end
                end
            end
        end
        
        function analyseddata = getAllAnalysed(self)
            % getAllAnalysed returns analysed data for all shots.
            analyseddata = [];
            if isempty(self.dataStore.AnalysisFunc); return; end
            shot1 = self.getAnalyseddiag(self.shotNumbers(1));
            if isempty(shot1); return; end
            if numel(shot1) == 1 % Single valued result
                analyseddata = zeros(1, self.nShots);
                for shot=1:self.nShots
                    if self.debugFlag>1
                        fprintf('%s, %i, run %i: Working on shot %3i\n', ...
                            self.runInfo.diagnostic, self.runInfo.date, self.runInfo.run, shot);
                    end
                    analyseddata(shot) = self.getAnalyseddiag(self.shotNumbers(shot));
                end
                return
            end
            if isvector(shot1) % Vector result, output is a 1D matrix
                analyseddata = zeros(length(shot1), self.nShots);
                for shot=1:self.nShots
                    if self.debugFlag>1
                        fprintf('%s, %i, run %i: Working on shot %3i\n', ...
                            self.runInfo.diagnostic, self.runInfo.date, self.runInfo.run, shot);
                    end
                    analyseddata(:,shot) = self.makeColumn(self.getAnalyseddiag(self.shotNumbers(shot)));
                end
                return;
            end
            % If we make it here, output must be an image
            analyseddata = zeros([size(shot1) self.nShots]);
            for shot=1:self.nShots
                if self.debugFlag>1
                        fprintf('%s, %i, run %i: Working on shot %3i\n', ...
                            self.runInfo.diagnostic, self.runInfo.date, self.runInfo.run, shot);
                end
                analyseddata(:,:,shot) = self.getAnalyseddiag(self.shotNumbers(shot));
            end
        end
        
        function setCropRegion(self, ROI)
            % setCropRegion allows setting a crop region.
            %
            % For data of traces, this needs to be a two-element vector,
            % with ROI(2)>ROI(1).
            %
            % For data of images, this needs to be a four element vector,
            % in the usual MATLAB ROI format: [x y w h].
            self.dataStore.ROI = ROI;
        end
        
        function setAnalysisFunction(self, fun)
            % setAnalysisFunction sets a function handle for analysis of the diagnostic
            %
            % The specified function must accept only 1 parameter, which is
            % the raw data from this diagnostic.
            self.dataStore.AnalysisFunc = fun;
        end

        function plotRAWdiag(self, shot, ax)
            if ~ishandle(ax); return; end
            if nargin < 3; ax = gca; end
            data_ = self.getRAWdiag(shot);
            if self.dataInfo.dataType == 2
                plot(ax, data_(:,1), data_(:,2));
            elseif self.dataInfo.dataType == 3
                imagesc(data_, 'Parent', ax);
            end 
        end
    end
    %%
    methods (Access = private)
        function date_str = getDateFol(self)
            date_str = num2str(self.runInfo.date);
        end
        
        function run_str = getRunFol(self)
            run_str = sprintf('%ir%03i', self.runInfo.date, self.runInfo.run);
        end
        
        function output = makeColumn(self, input)
            % makeColumn always returns a column vector.
            if iscolumn(input)
                output = input;
            else
                output = input';
            end
        end
    end
end
