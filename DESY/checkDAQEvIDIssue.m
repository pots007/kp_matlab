% checkDAQEvIDIssue.m

xmlfile = '/home/fflab28m/Matlab/Simon/DAQRead.xml';
xmlfile = '/home/fflab28m/Kris/ChanList2.xml';
[data,err] = daq_read(xmlfile)
%%
EvIDs = fieldnames(data); % gets a list of the PIDs (shots taken/saved)
Ev1 = data.(char(EvIDs(1))); % gets the first PID structure
DOOCSNames = fieldnames(Ev1); %gets a list of the channels in the first PID, this shouldb be the same for all PIDs

% Last entry is time....
EvArray = zeros(length(DOOCSNames)-1, length(EvIDs));
EvTimes = zeros(size(EvIDs));
for EvID = 1:length(EvArray)
    Ev = data.(EvIDs{EvID});
    for diag = 1:length(DOOCSNames)-1
        diag_str = Ev.(DOOCSNames{diag});
        EvArray(diag, EvID) = diag_str.daqtype;
        if diag_str.daqtype ~=0
            EvTimes(EvID) = diag_str.sec;
        end
    end
end
    
%% Plot it all
hfig = figure(19500); clf(hfig);
set(gcf, 'Position', [2000 3000 1000 900]);
ax1 = axes('Position', [0.3 0.05  0.67 0.5]);
ax2 = axes('Position', [0.3 0.555 0.67 0.4], 'XAxisLocation', 'top',...
    'Box', 'on', 'NextPlot', 'add');
imagesc(logical(EvArray), 'Parent', ax1);
plot(ax2, EvTimes- EvTimes(1), 'x', 'Color', [0.5 0.5 0], 'LineWidth', 10);
ylabel(ax2, 'Time since first shot (s)');
xlabel(ax2, 'EvID in run');
xlabel(ax1, 'EvID in run');
set(ax1, 'YTick', 1:(length(DOOCSNames)-1), 'YTickLabel', DOOCSNames);
set(ax2, 'XLim', get(ax1, 'XLim'));
colormap([1 1 1; 0.5 0.5 0]);
set([ax1, ax2], 'FontSize', 12);
