% propagateLaserPastXChamber.m
%
% Look at what happens if we clip the beam by the x-chamber...
%
% KP, 2017 Oct

if getComputerName=='kristjan-latitude-e7470'
    savfol = fullfile('/home', 'kristjan', 'ownCloud', 'Experiments', '2017_FFWD', 'Beamline');
else
    savfol = fullfile(getDropboxPath, 'MATLAB', 'DESY');
end
F = 18; % in meters
D = [0.04 0.045]; % Beam diameter
Xdist = 2.2; % X-chamber distance
Xheight = 0.032; % Width of x-chamber
Npnts = 2^10;
w0 = 450e-6; % Spot size at focus
lambda0 = 800e-9;
k0 = 2*pi/lambda0;

% Set up plotting
hfig = figure(784682); clf(hfig); 
set(hfig, 'Color', 'w', 'Position', [100 100 1500 700]);
locs = GetFigureArray(3, 2, [0.05 0.07 0.07 0.05], 0.1, 'down');
for k=1:size(locs,2)
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k), 'Box', 'on',...
        'LineWidth', 1.5, 'Layer', 'top');
end

% Set up the near field
x_grid = linspace(-0.05, 0.05, Npnts); dx = mean(diff(x_grid));
[X, Y] = meshgrid(x_grid, x_grid);
R = X.^2 + Y.^2;
w = D*0.5*0.025;
N = 8;
NF_ampB = exp(-(R/w(1)).^N);
NF_ampS = exp(-(R/w(2)).^N);
% Normalise to contain same total energy
NF_ampB = NF_ampB/sum(NF_ampB(:));
NF_ampS = NF_ampS/sum(NF_ampS(:));
%imagesc(x_grid, x_grid, NF);
zR = pi*w0^2/lambda0;
R_curv = F*(1 + (zR/F)^2);
NF_phase = 100*k0 * R.^2 / (2*R_curv);
NF = NF_ampB.*exp(-1j*NF_phase);
imagesc(x_grid, x_grid, NF_ampB.^2, 'Parent', axx(1));
title(axx(1), 'Nearfield intensity');
imagesc(x_grid, x_grid, NF_phase, 'Parent', axx(2));
title(axx(2), 'Nearfield phase');


% Now the far field - firstly with Fourier transform of big one and then
% cut
FF = fftshift(fft2(NF_ampB, Npnts*4, Npnts*4));
FFTmax = 2*pi/dx;
ff_grid = linspace(-0.5*FFTmax/k0*F, 0.5*FFTmax/k0*F, Npnts*4)*1e3;
imagesc(ff_grid, ff_grid, abs(FF).^2, 'Parent', axx(3));
title(axx(3), ['FF with Fourier, ' num2str(D(1)*1000) ' mm beam']);
colorbar(axx(3));
% Cut in NF: big beam
logm = abs(X)<0.5*Xheight*(1+Xdist/F);
enLossFact = sum(NF_ampB(:))/sum(sum(NF_ampB.*logm));
FF_cut = fftshift(fft2(NF_ampB.*logm*enLossFact, 4*Npnts, 4*Npnts));
imagesc(ff_grid, ff_grid, abs(FF_cut).^2, 'Parent', axx(4));
title(axx(4), ['FF with NF cut, energy loss of ' num2str(enLossFact^2-1, 3)]);
imagesc(x_grid, x_grid, NF_ampB.*logm, 'Parent', axx(2));
colorbar(axx(4)); 

% And now smaller beam
FF = fftshift(fft2(NF_ampS, Npnts*4, Npnts*4));
FFTmax = 2*pi/dx;
ff_grid = linspace(-0.5*FFTmax/k0*F, 0.5*FFTmax/k0*F, Npnts*4)*1e3;
imagesc(ff_grid, ff_grid, abs(FF).^2, 'Parent', axx(5));
title(axx(5), ['FF with Fourier, ' num2str(D(2)*1000) ' mm beam']);
colorbar(axx(5));
% Cut in NF: big beam
logm = abs(X)<0.5*Xheight*(1+Xdist/F);
enLossFact = sum(NF_ampS(:))/sum(sum(NF_ampS.*logm));
FF_cut = fftshift(fft2(NF_ampS.*logm*enLossFact, 4*Npnts, 4*Npnts));
imagesc(ff_grid, ff_grid, abs(FF_cut).^2, 'Parent', axx(6));
title(axx(6), ['FF with NF cut, energy loss of ' num2str(enLossFact^1-1, 3)]);
imagesc(x_grid, x_grid, NF_ampS.*logm, 'Parent', axx(2));
colorbar(axx(6)); 


% Limits etc
FFlims = [-1 1]*1e0;
set(axx([3:end]), 'XLim', FFlims, 'YLim', FFlims)
for k=3:length(axx)
    xlabel(axx(k), 'x / mm');
    ylabel(axx(k), 'y / mm');
end

colormap(jet_white(256))
export_fig(fullfile(savfol, 'NF_cut_in_Xchamber'), '-pdf', '-nocrop', hfig);