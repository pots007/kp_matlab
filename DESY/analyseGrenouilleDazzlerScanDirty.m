% s
%
% Try to get the AC widths of the Grenouille scan for simplistic
% analysis...
%

if ispc
    datfol = 'C:\Users\kpoder\Work\grenouille\rawData\20170905';
elseif ismac
    datfol = '/home/fflab28m/DATA/grenouille/rawData/20170905';
end

dt = 0.8878;
deconvfactor = sqrt(2);

follist = dir(datfol);
follist = follist(3:end);
follist = follist(cell2mat({follist.isdir}));
folnames = {follist.name}';

ACtraces = cell(size(folnames));
ACtracesnorm = cell(size(folnames));
ACwidths = cell(size(folnames));
FROGimages = cell(size(folnames));
for fol = 1:length(folnames);
    filelist = dir(fullfile(datfol, folnames{fol}, '*.bmp'));
    for f=1:length(filelist)
        filename = fullfile(datfol, folnames{fol}, filelist(f).name);
        imm = imread(filename);
        FROGimages{fol}(:,:,end+1) = imm;
        ACtraces{fol}(end+1,:) = sum(imm, 2)';
        ACtracesnorm{fol}(end+1,:) = ACtraces{fol}(end,:)/max(ACtraces{fol}(end,:));
        ACwidths{fol}(end+1) = widthfwhm(ACtraces{fol}(end,:))*dt/deconvfactor;
    end
end
%%
chirps = cellfun(@str2double, folnames);
tauVals = cellfun(@mean, ACwidths);
tauErrs = cellfun(@std, ACwidths);

% Not actually a parabola it seems!!!!!!
fit_ = polyfit(chirps, tauVals, 2);
fitfcn = @(tin, phi0, x) sqrt(tin.^2 + (x-phi0).^2*16*(log(2))^2./tin.^2);
fit_p = fit(chirps, tauVals, fitfcn, 'Startpoint', [30, 35500]);

hfig = figure(3237562); clf(hfig);
ax = gca; ax.NextPlot = 'add';
hh(1) = errorbar(chirps, tauVals, tauErrs, 'o');
plotchirps = (-2500:10:2500) - 2*fit_(3)/fit_(2);
hh(2) = plot(plotchirps, polyval(fit_,plotchirps), '-r');
hh(3) = plot(plotchirps, fit_p(plotchirps), '-k');

legend(hh, {'Data', 'Parabolic fit', '$\tau_{out} = \sqrt{1 + \frac{\phi_L^2}{4\beta^2} }$'}, ...
    'Location', 'North')
title('Pulse length variation from autocorrelation');
xlabel('Chirp in Dazzler / $\mathrm{fs}^2$');
ylabel('Pulse length / fs');
set(gca, 'Box', 'on');
make_latex(hfig); setAllFonts(hfig, 16);
%export_fig(fullfile(datfol, 'DazzlerScanACvariation'), '-pdf', '-nocrop', hfig);