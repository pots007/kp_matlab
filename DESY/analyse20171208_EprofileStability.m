% analyse20171208_EprofileStability.m

date_ = 20171208;
run_ = 3;

Eprofile = Analysis.Diagnostic(date_, run_, 'TargetChamberProfile');
Eprofile.setAnalysisFunction(@(x) sum(x(:)));
prof_Qs = Eprofile.getAllAnalysed();

% Use a high threshold fraction to try to remove effect of only half the
% beam being present on the screen!
Eprofile.setAnalysisFunction(@(x) centroidlocationsmatrix(medfilt2(x),0.5));
prof_centroids = Eprofile.getAllAnalysed();

% Very ROUGH calibration: 1045 pixels is width of the screen; this in real
% life is 5ish cm I think. And the distance to screen is about a metre or
% so. So total screen width is 50mrad, and one pixel is 
pixCal = 50/1045;
%% And plot the pointing stability
xpoint = prof_centroids(1,3:end); xpoint = xpoint - mean(xpoint);
ypoint = prof_centroids(2,3:end); ypoint = ypoint - mean(ypoint);
hfig = figure(34762); clf(hfig); hfig.Position = [1100 300 700 450];
axx = makeNiceAxes(hfig);
h1 = plot(axx, xpoint*pixCal, 'o');
h2 = plot(axx, ypoint*pixCal, 'd');
npnts = length(xpoint);
he1 = plot([0 npnts+1], [1 -1; 1 -1]*std(xpoint*pixCal), 'Color', h1.Color);
he2 = plot([0 npnts+1], [1 -1; 1 -1]*std(ypoint*pixCal), 'Color', h2.Color);
set([he1 he2], 'LineStyle', '-', 'LineWidth', 2);
set([h1 h2], 'LineWidth', 1, 'MarkerSize', 8)
leglabs{1} = sprintf('Horizontal, \\sigma=%2.1f mrad', std(xpoint*pixCal));
leglabs{2} = sprintf('Vertical, \\sigma=%2.1f mrad', std(ypoint*pixCal));
hleg = legend([h1 h2], leglabs, 'Location', 'North',...
    'Orientation', 'horizontal', 'Box', 'off');
xlabel(axx, 'Shot');
ylabel(axx, 'Pointing / mrad');
set(axx, 'YLim', [-10 10], 'XLim', [0 npnts+1]);
set([axx.XLabel axx.YLabel], 'Interpreter', 'tex');
setAllFonts(hfig, 16);
export_fig(fullfile(Analysis.getAnalysisRootFol, '20171208r003_PointingStability'), '-pdf', '-nocrop', hfig);

%% And plot a typical beam profile
shotID = 41;
hfig = figure(34763); clf(hfig); hfig.Position = [1100 300 600 450];
axx = makeNiceAxes(hfig);
imm = medfilt2(Eprofile.getRAWdiag(shotID));
kernn = ones(4)/25;
cc = centroidlocationsmatrix(imm);
xax = ((1:size(imm,2)) - cc(1))*pixCal;
yax = ((1:size(imm,1)) - cc(2))*pixCal;
imagesc(xax, yax, imm-2);
set(gca, 'CLim', [0 43], 'XLim', 20*[-1 1], 'YLim', 15*[-1 1], 'Layer', 'top');
xlabel('Horizontal divergence / mrad');
ylabel('Vertical divergence / mrad');
make_tex(hfig);
export_fig(fullfile(Analysis.getAnalysisRootFol, '20171208r003_TypicalBeam'), '-pdf', '-nocrop', hfig)
setAllFonts(hfig, 16);