% changeServerStyleDataShotPadding.m
%
% Does what it says on the tin: changes the number of padding around the
% shot number for all files and mat file fields in a particular root
% folder.
%
% Arguments: 
%   rootfolder - base of all data files
%   oldPadding - integer number of old padding, 005 corresponds to pad of 3
%   newPadding - integer number of new padding.
%
% Note only files where the shot padding matches that of oldPadding are
% changed!
%
% This will only work for MATLAB2016b onwards as it relies on
% Mathworks having __finally__ fixed 'dir' to be sensible!
%
% KP, DESY, May 2018

function changeServerStyleDataShotPadding(rootfolder, oldPadding, newPadding, verbosity)

%rootfolder = '/home/fflab28m/Kris/ServerStyleData/20170112'; %'/media/kpoder/KRISMOOSE6/28mdata/20171114';
%oldPadding = 3;
%newPadding = 6;
fmtSpec = sprintf('%%0%ii', newPadding);

%if nargin==3; verbosity = 0; end
%verbosity = 1;   
doChange = 1;
dispNum = 100;

% ---------- Dealing with simple data files ----------
exts = {'png', 'txt'};
for ext=exts
    fprintf('\nPerforming dir for extension %s. This may take minutes...\n', ext{1});
    flist = dir(fullfile(rootfolder, '**', ['*s*.' ext{1}]));
    fprintf('\tFound %6i files with extension %s. Starting conversion...\n',...
        length(flist), ext{1});
    for f=1:length(flist)
        if mod(f, dispNum) == 0
            fprintf('\t\t Renaming file %9i out of %9i\n', f, length(flist));
        end
        fn = flist(f).name;
        fp = flist(f).folder;
        [ss,ee] = regexp(fn, 's\d+');
        % Ensure we only convert the ones we asked to
        if ee-ss ~= oldPadding; continue; end
        shotNo = str2double(fn(ss+1:ee));
        newfn = [fn(1:ss) sprintf(fmtSpec, shotNo) fn(ee+1:end)];
        if verbosity > 2; fprintf('%s -> %s\n', fn, newfn); end
        % Actual renaming happens here:
        if doChange
            movefile(fullfile(fp, fn), fullfile(fp, newfn));
        end
    end
end

% ---------- Dealing with mat files ------------

fprintf('\nPerforming dir for extension mat. This may take minutes...\n');
flist = dir(fullfile(rootfolder, '**', '*.mat'));
fprintf('\tFound %6i files with extension %s. Starting conversion...\n',...
        length(flist), 'mat');
for f=1:length(flist)
    if mod(f, dispNum) == 0
        fprintf('\t\t Renaming file %9i out of %9i\n', f, length(flist));
    end
    fdat = load(fullfile(flist(f).folder, flist(f).name));
    data = renameStructFieldNames(fdat.data, oldPadding, newPadding); %#ok<NASGU>
    if doChange
        save(fullfile(flist(f).folder, flist(f).name), 'data');
    end
end

% ---------- Dealing with json files ------------
fprintf('\nPerforming dir for extension json. This may take minutes...\n');
flist = dir(fullfile(rootfolder, '**', '*.json'));
fprintf('\tFound %6i files with extension %s. Starting conversion...\n',...
        length(flist), 'json');
for f=1:length(flist)
    clear data;
    dat = jsondecode(fileread(fullfile(flist(f).folder, flist(f).name)));
    data.serverrun = dat.serverrun;
    data.daqrun = dat.daqrun;
    data.times = renameStructFieldNames(dat.times, oldPadding, newPadding);
    % And now iterate through the diags
    diags = fieldnames(dat.diags);
    for i=1:length(diags)
        d = diags{i};
        data.diags.(d) = renameStructFieldNames(dat.diags.(d), oldPadding, newPadding);
    end
    if doChange
        str2 = jsonencode(data);
        try
            py.importlib.import_module('json');
            collections = py.importlib.import_module('collections');
            formattedJson = char(py.json.dumps(py.json.loads(str2,...
                pyargs('object_pairs_hook',collections.OrderedDict)),...
                pyargs('indent',int32(4))));
        catch err
            fprintf('Error formatting json: %s\n', err.message);
            formattedJson = str2;
        end
        fid = fopen(fullfile(flist(f).folder, flist(f).name), 'w');
        fprintf(fid, '%s', formattedJson);
        fclose(fid);
    end
end
assignin('base', 'data', data);

function outStr = renameStructFieldNames(inStr, oldPadding, newPadding)
fmtSpec = sprintf('s%%0%ii', newPadding);
% Keep the input one untouched...
outStr = inStr;
fields = fieldnames(inStr);
for i=1:length(fields)
    [ss,ee] = regexp(fields{i}, 's\d+');
    if ee-ss ~= oldPadding; continue; end
    outStr.(sprintf(fmtSpec, str2double(fields{i}(ss+1:ee)))) = inStr.(fields{i});
    outStr = rmfield(outStr, fields{i});
end