% plotDamon_ICT_Screen_stability.m
%
% Script to look through a run and plot the ICT trace, Damon trace and
% Eprofile screen trace to check for stability. Also histograms with nbins
% to look at distribution.

date_ = 20171207;
run_ = 4;

saveFlag = 1;

% Set up graphics.
hfig = figure(123212); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [100 100 1200 900]);
locs = GetFigureArray(3, 2, [0.07 0.05 0.08 0.1], 0.1, 'across');
clear axx;
axx(1) = axes('Parent', hfig, 'Position', [locs(1,1) locs(2,1) 0.85 locs(4,1)]);
axx(2) = axes('Parent', hfig, 'Position', locs(:,4));
axx(3) = axes('Parent', hfig, 'Position', locs(:,5));
axx(4) = axes('Parent', hfig, 'Position', locs(:,6));
set(axx, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);

%% Set up diagnostics
damon = Analysis.Diagnostic(date_, run_, 'DAMON_CHARGE');
ict = Analysis.Diagnostic(date_, run_, 'ICT');
screen = Analysis.Diagnostic(date_, run_, 'TargetChamberProfile');
% Set necessary analysis functions.
damon.setAnalysisFunction(@(x) getDamonCharge20gb(max(x(:,2))));
ict.setAnalysisFunction(@(x) ICTanal(x(:,2)));
screen.setAnalysisFunction(@(x) sum(x(:))*1e-5); % Arb factor used here...

% Get the data analysed!
damon_all = damon.getAllAnalysed;
ict_all = ict.getAllAnalysed;
screen_all = screen.getAllAnalysed;

%% Plot the data
% First, shot by shot for all three
cla(axx(1));
h(1) = plot(axx(1), damon.shotNumbers, damon_all, '--x');
h(2) = plot(axx(1), ict.shotNumbers, ict_all, '--o');
h(3) = plot(axx(1), screen.shotNumbers, screen_all, '--d');

xlabel(axx(1), 'Shot number'); ylabel(axx(1), 'Charge / pC');
title(axx(1), sprintf('Date %i, run %i', date_, run_));
legend(h, {'DaMon', 'ICT', 'Screen'});

% Now plot the histograms.
nbins = 20;
plotQ = {damon_all, ict_all, screen_all};
plotL = {'DaMon / pC', 'ICT / pC', 'Screen / arb.units'};
for k=2:4
    cla(axx(k));
    hh(k-1) = histogram(axx(k), plotQ{k-1}, nbins, 'Normalization', 'pdf',...
        'DisplayStyle', 'stairs');
    xlabel(axx(k), plotL{k-1});
    ylabel(axx(k), 'Normalised frequency')
    title(axx(k), sprintf('$\\langle Q \\rangle = %2.2f \\quad \\sigma_Q = %2.2f$',...
        mean(plotQ{k-1}), std(plotQ{k-1})));
end
set(h, 'LineWidth', 2, 'MarkerSize', 9);
set(hh, 'LineWidth', 2);

setAllFonts(hfig, 14); make_latex(hfig);
if saveFlag
    savpath = fileparts(Analysis.getDataRootFol);
    savfol = fullfile(savpath, '28manalysis');
    if ~exist(savfol, 'dir')
        savfol = fullfile(savpath, 'Analysis');
    end
    savfname = fullfile(savfol, ...
        sprintf('ChargeDiags_%i_r%03i_stab', date_, run_));
    %export_fig(savfname, '-pdf', '-nocrop', hfig);
    %if pngFlag
        export_fig(savfname, '-png', '-m3', '-nocrop', hfig);
    %end
end