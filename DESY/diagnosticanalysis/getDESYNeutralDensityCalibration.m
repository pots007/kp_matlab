% getDESYNeutralDensityCalibration.m
%
% Get density of neutral gas calibrations

% Folder for the data
datfol = 'C:\Users\kpoder\Work\Gasjet_argon';
% And the filename
filen = '50bar_1mm_2msDelay_3msExp_vac.png';
filen = '15bar_1mm_2msDelay_3msExp_vac.png';
% Read the data in, and rotate a little bit to get hor. fringes
data = double(imread(fullfile(datfol, filen)));
data = imrotate(data, 1, 'bilinear', 'crop');
imagesc(data);

% This is the ROI - where the data is
xx = 200; ww = 300;
yy = 25;  hh = 350;
% Crop the image
dat = data(yy:yy+hh, xx:xx+ww);
imagesc(dat);
% And make a reference image, in a very cheeky way. Basically we
% interpolate each line of the image across the data region as the fringes
% should be straight here. Kind of works.
refim = data;
immax = 1:size(data,2);
logm = immax > xx & immax < (xx+ww);
for k=yy:yy+hh
    % Interpolate every line.... :p
    refim(k, logm) = interp1(immax(~logm), data(k, ~logm), immax(logm));
end
imagesc(refim);
% And use the same ROI of the "reference" image
ref = refim(yy:yy+hh, xx:xx+ww);
% Fourier transform both ref and data
fftim = fftshift(fft2(dat));
fftref = fftshift(fft2(ref));
%imagesc(abs(fftim))

% This is the ROI in Fourier space. Playing with this changes the amount of
% noise in the retrieved image and also the amount of Fourier suppression
fftx = 146; ffty = 153;
fftw = 9; ffth = 7;

% Apply the Fourier mask - no fancy supergaussians, simple cut
filtfftim = zeros(size(fftim));
filtfftim(ffty:ffty+ffth, fftx:fftx+fftw) = fftim(ffty:ffty+ffth, fftx:fftx+fftw);
filtfftref = zeros(size(fftref));
filtfftref(ffty:ffty+ffth, fftx:fftx+fftw) = fftref(ffty:ffty+ffth, fftx:fftx+fftw);
% Inverse FT
filtdat = ifft2(ifftshift(filtfftim));
filtref = ifft2(ifftshift(filtfftref));
% And the key part - the reference image gives us the spatially varying
% phase, introduced by the fringes. The division on the following line
% filters this out.
imphase = angle(filtdat./filtref);
% And unwrap the phase.
imphase = unwrap(imphase, 5.5);
imagesc(abs(imphase)); colorbar;