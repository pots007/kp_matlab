% daq2ServerEvent.m
%
% Class to allow other, enclosing classes get information about the
% progress of daq2ServerStyle work.
%
% Based on examples from
% https://de.mathworks.com/help/matlab/matlab_oop/class-with-custom-event-data.html

classdef (ConstructOnLoad) daq2ServerEvent < event.EventData
   properties
      allShots = 1
      thisShot = 0
   end
   methods
      function eventData = daq2ServerEvent(all, this)
         eventData.allShots = all;
         eventData.thisShot = this;
      end
   end
end