% fixOldDataFiles.m
%
% Pad old images with a little bit of zeros...
%
% Useage: fixOldDataFiles(1970, 1, 'DiagnosticName', [640,480])

function fixOldDataFiles(date_, run_, diagnostic, targetSize)

rootpath = parseShotTimeDESY(date_, run_, 1);
diagfol = fullfile(rootpath, diagnostic);

if ~exist(diagfol, 'dir')
    error('Something does not exist');
end
newdiagfol = [diagfol '_p'];
mkdir(newdiagfol);

if nargin<4
    targetSize = [963 1288];
end

flist = dir(fullfile(diagfol, '*.png'));
for f = 1:length(flist)
    fprintf('Working on file %i out of %i\n', f, length(flist));
    imm = imread(fullfile(diagfol, flist(f).name));
    newimm = zeros(targetSize);
    newimm(1:size(imm,1), 1:size(imm,2)) = imm;
    newimm = uint16(newimm);
    [~, fstem, ~] = fileparts(flist(f).name);
    imwrite(newimm, fullfile(rootpath, [diagnostic '_p'], ...
        [fstem '_p.png']));
end

