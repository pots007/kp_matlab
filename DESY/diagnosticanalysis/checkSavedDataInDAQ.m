% checkSavedDataInDAQ.m
%
% Given an xml file, will check to see which channels actually have some
% data saved. This could be a useful diagnostic if some of the parts of the
% request file have nothing saved in the DAQ.
%
% If no xml file is given, the code checks ALL available channels and
% returns the amount of shots found.
%
% checkSavedDataInDAQ(runName, xmlFileName, forceNew, showExistingOnly)
%       runName - integer specifying the DAQ run number
%       xmlFileName - file name of xml file specifying channel of interest.
%                   If this is set to [], will poll for ALL stuff saved in
%                   DAQ
%       forceNew - if set to 1, will query the DAQ for status. Otherwise,
%                  will load the previous report, if it exists.
%       showExistingOnly - if set to 1, will filter out all diagnostics
%                   that did not save anything. Defaults to 0.
%
%
% KP, DESY, 2018

function daqState = checkSavedDataInDAQ(runName, xmlFileName, forceNew, showExistingOnly)

reportPath = '/home/fflab28m-data/People/Kris/daqStateDataReports';
% By default, don't force new read, use the saved report.
if nargin<3; forceNew = 0; end
if nargin<4; showExistingOnly = 0; end

% First check if we've already done this run ...
reportFileName = fullfile(reportPath, sprintf('run%i.mat', runName));
if exist(reportFileName, 'file') && ~forceNew
    load(reportFileName);
    fprintf('    Using presaved file\n');
else
    % Check the run
    addpath('/local/lib'); % Just to be sure ...
    
    % Additions for alllllll checking
    if ~isempty(xmlFileName)
        xmlFileCmds = getFileText(xmlFileName);
        if isempty(xmlFileCmds)
            error('No data in xml file! Please check!');
        end
        
        inds = cellfun(@(x) ~isempty(strfind(x, 'number')), xmlFileCmds); %#ok<STREMP>
        % If a run number is not found, quit!
        if sum(inds)==0
            error('No DAQ run number specified in xml file!');
        else
            % Update the run number in the file's contents
            xmlFileCmds{inds} = sprintf('<RunFirst  number=''%i''/>', runName);
        end
        
        % Now find the list of specified data channels
        inds = cellfun(@(x) ~isempty(strfind(x, 'Chan name')), xmlFileCmds); %#ok<STREMP>
        if isempty(inds)
            error('No data channels specified in the xml file!');
        end
        inds = (1:length(inds))'.*inds; inds = inds(inds~=0);
        
        % Set up some useful variables:
        tempxmlFileName = 'tempChanList.xml';
        daqState = struct;
        
        % Start the main loop over the specified data channels
        fprintf('    Starting data query from DAQ....\n');
        
        for chan = 1:length(inds)
            % First, the name of the channel:
            temps = strsplit(xmlFileCmds{inds(chan)}, '''');
            daqState(chan).name = temps{2};
            fprintf('Checking channel %i(%i): %s\n', chan, length(inds), daqState(chan).name);
            tempxml = {};
            for k=1:(min(inds)-1) % Dump the header of the xml file
                tempxml{k} = xmlFileCmds{k};
            end
            tempxml{end+1} = xmlFileCmds{inds(chan)};
            tempxml{end+1} = '</DAQREQ>';
            fid = fopen(tempxmlFileName, 'w');
            for k=1:length(tempxml); fprintf(fid, '%s\n', tempxml{k}); end
            fclose(fid);
            [data,err] = daq_read(tempxmlFileName);
            if ~isempty(err)
                daqState(chan).status = ['DAQ error:' err];
                daqState(chan).nShots = 0;
                daqState(chan).shotEvents = [];
            else
                nShots = length(fieldnames(data));
                if nShots == 0
                    daqState(chan).status = 'No data   ';
                else
                    daqState(chan).status = 'Data found';
                end
                daqState(chan).nShots = nShots;
                daqState(chan).shotEvents = cellfun(@(x) str2double(x(4:end)), fieldnames(data));
            end
        end
        % Save this for later use...
        save(reportFileName, 'daqState');
    else
        % This is the part that actually finds all stuff, in a somewhat
        % convoluted way....
        xmlFile = {'<DAQREQ>',...
            '<ReqId  id=''1502899874''/>',...
            sprintf('<RunFirst  number=''%i''/>', runName),...
            '<Exp  name=''flashfwd''/>',...
            '<DDir name=''/daq_data/flashfwd/EXP/''/>',...
            '<CDir name=''/daq/ttf2/adm/''/>',...
            '<ScanMode mode=''0x2000000f'' />',...            
            '<Chan name=''TIMINGINFO/TIME1.BUNCH_FIRST_INDEX.1;STREAM1''/>',...
            '</DAQREQ>'};
        %'<Chan name=''TTF2.DAQ/FLASHFWD.DISTRIBUTOR/DAQ.STREAM.1/EVB.EVENTS;STREAM1''/>',...
        tempxmlFileName = 'tempChanList.xml';
        fid = fopen(tempxmlFileName, 'w');
        for k=1:length(xmlFile); fprintf(fid, '%s\n', xmlFile{k}); end
        [data,err] = daq_read(tempxmlFileName);
        if ~isempty(err)
            fprintf(err);
            return;
        end
        ids = fieldnames(data);
        tStart = data.(ids{1}).time(1);
        tEnd = data.(ids{end}).time(1);
        % Now prepare life for daq_list:
        inp{1} = ['-TStart ' datestr(datetime(tStart-5, 'ConvertFrom', 'posixtime',...
            'TimeZone','Europe/Berlin'), 'yyyy-mm-ddTHH:MM:SS')];
        inp{2} = ['-TStop ' datestr(datetime(tEnd+5, 'ConvertFrom', 'posixtime',...
            'TimeZone','Europe/Berlin'), 'yyyy-mm-ddTHH:MM:SS')];
        inp{3} = '-Exp flashfwd';
        inp{4} = '-DDir /daq_data/flashfwd/EXP/';
        inp'
        [data, err] = daq_list(inp);
        if ~isempty(err); disp(err); end
        for chan=1:length(data)
            tt = strsplit(data{chan});
            daqState(chan).name = tt{3};
            daqState(chan).status = 'Data found';
            daqState(chan).nShots = str2double(tt{7});
            daqState(chan).shotEvents = [];
        end
        % Save this for later use...
        save(reportFileName, 'daqState');
    end
end

% And print out a summary table:
fprintf('\n\n -------------#######  Summary for run %i ##### ------------\n', runName);
fprintf('%60s\t%10s\t%10s\n', 'Channel name', 'Status', 'nShots');
for k=1:length(daqState)
    if showExistingOnly && daqState(k).nShots==0; continue; end
    fprintf('%60s\t%10s\t%10i\n', daqState(k).name, daqState(k).status(1:10),...
        daqState(k).nShots);
end


