% parseShotTimeDESY.m
%
% Function to yield a datafile name from shot date, run, shot or
% shotstring. 
%
% KP, DESY, 2017

function [datafol, shotstr] = parseShotTimeDESY(date_, run_, shot_)
% Default name...
datafol = 'error';
shotstr = [];
% Set up suitable path
if strcmp(getComputerName, 'flalap01-ubuntu')
    rootfol = '/media/kpoder/KRISMOOSE6/28mdata';
    if ~exist(rootfol, 'dir')
        disp('Root folder does not exist. Hard drive connected?????')
        return;
    end
else
    rootfol = '/home/fflab28m/Kris/ServerStyleData';
end

if nargin==1 && ischar(date_) && length(date_)==16
    % This means its a shotstring already
    datefol = date_(1:8);
    runfol = date_(1:12);
    shotname = date_;
else
    % Need all three...
    if nargin~=3
        disp('Function parseShotTimeDESY needs three arguments');
        return;
    end
    datefol = sprintf('%i', date_);
    runfol = sprintf('%ir%03i', date_, run_);
    shotname = sprintf('%ir%03is%06i', date_, run_, shot_);
end

datafol = fullfile(rootfol, datefol, runfol);
shotstr = shotname;
    
    