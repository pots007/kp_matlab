% getRAWdiagnostic.m
%
% Generic function to retrieve data from a particular diagnostic, for a
% particular shot.
%
% KP, DESY, 2017

function rawDiag = getRAWdiagnostic(date_, run_, shot_, diagName, diagType)
% Defaults
rawDiag = [];
if nargin<4; diagName = 'ElectronSpectrometer3'; end
if nargin<5; diagType = 'image'; end

% Do all input parsing. If numel date is more than one, it is an image
[rootfol, shotstr] = parseShotTimeDESY(date_, run_, shot_);
if strcmp(rootfol, 'error')
    disp('Error parsing shot name!');
    return;
end
if strcmp(diagType, 'image')
    try
        rawDiag = imread(fullfile(rootfol, diagName,...
            [shotstr '_' diagName '.png']));
    catch err
        fprintf('Error getting image: %s\n', err.message);
    end
elseif strcmp(diagType, 'trace')
    try
        fname = fullfile(rootfol, diagName,[shotstr '_' diagName '.txt']);
        rawDiag = importdata(fname);
        if isstruct(rawDiag); rawDiag = rawDiag.data; end
    catch err
        fprintf('Error getting trace: %s\n', err.message);
    end
elseif strcmp(diagType, 'point')
    try
        rawDiagFile = load(fullfile(rootfol, diagName));
        if isfield(rawDiagFile.data, shotstr(end-3:end))
            rawDiag = rawDiagFile.data.(shotstr(end-3:end));
        end
    catch err
        fprintf('Error getting point: %s\n', err.message); 
    end
end