% getEspec3RAWimage.m
%
% Wrapaper function to get Espec3 raw image
%
% KP, DESY, 2017

function rawImage = getEspec3RAWimage(date_, run_, shot_)

%rawImage = [];
% Do all input parsing. If numel date is more than one, it is an image
%fname = parseShotTimeDESY(date_, run_, shot_);
%if strcmp(fname, 'error')
%    disp('Error parsing shot name!');
%    return;
%end
%[rootfol, shotstr, ~] = fileparts(fname);
%rawImage = imread(fullfile(rootfol, 'ElectronSpectrometer3',...
%    [shotstr '_ElectronSpectrometer3.png']));
rawImage = getRAWdiagnostic(date_, run_, shot_, 'ElectronSpectrometer3', 'image');
