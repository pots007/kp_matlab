% removeFishEye.m
%
% Matlab version of Paul's Espec dewarping thingy in python.
% Settings saved for screens 2 and 3.
%
% Accessing newer references is via adding tens... Soo, for old references,
% use screenID=3. For the updated Espec3, as of 20171122, use screenID=13,
% etc.
%
% KP, 2017, DESY

function dewarpedImage = removeFishEye(image, screenID)
%image = imread('/home/kristjan/python/especdevelopment/ressources/ElectronSpecCam3.png');
factW = -0.5; factH = -0.5; % Ddefault center locations
switch screenID
    case  {3 300} % Old Espec3 setup
        padwidth = 100;
        k1 = 2.16e-7;
        theta = 1.2;
    case  2 % For old Espec2 setup
        padwidth = 100;
        k1 = 1.78e-7;
        theta = .3;
    case 13 % New espec3 setup, installed 20171122
        padwidth = 100;
        k1 = .4109e-8;% -.025e-7;
        theta = -0.15;
        factH = 2.06;
        factW = -0.0632;
end

image = padarray(image, padwidth*[1 1], 0, 'both');

[height, width] = size(image);
[grid_x, grid_y] = meshgrid(1:width,1:height);

grid_x = round(grid_x + factW*width);
grid_y = round(grid_y + factH*height);
map_r2 = (grid_y.^2 + grid_x.^2);
map_x = grid_x./(1 + map_r2*k1);
map_y = grid_y./(1 + map_r2*k1);

dewarpedImage = interp2(grid_x, grid_y, double(image), map_x, map_y);
dewarpedImage = imrotate(dewarpedImage, theta);
