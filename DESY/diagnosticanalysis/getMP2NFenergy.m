% getMP2NFenergy.m
%
% Function to extract the total uncompressed energy from MPA2 NF images as
% used at DESY in the 2 floor laser.
%
% The calibration is most important one; a list is included in the file and
% a suitable one will be chosen automatically, based on the date of the
% shot. Use of a certain one can, however, be forced.
%
% Expects standard server style data structure, but can also be used for
% individual files or even numerical arrays.
%
% Arguments:
%
%
% KP, DESY, 2017

function energy = getMP2NFenergy(date_, run_, shot_, calib)

%fname = '/home/kristjan/work/MP2NF/20171110r004s018_MP2NF.tif';

% Do all input parsing. If numel date is more than one, it is an image

if ischar(date_)
    % If it's a char, it is perhaps shotstring? But also perhaps file name?
    % Check whether it exists: if it does, try to load it. Else, parse
    % name.
    if exist(date_, 'file')
        imm = imread(date_);
    else
        imm = getRAWdiagnostic(date_, run_, shot_, 'MP2NF', 'image');
    end
else
    if numel(date_)==1
        imm = getRAWdiagnostic(date_, run_, shot_, 'MP2NF', 'image');
    else
        imm = date_;
    end
end
bg = mean(mean(imm(:,[1:50 600:659])));
imm = double(imm) - bg;
totCounts = sum(imm(:));

% Now select calibration! This will be somewhat tricky :(

calFact = .6e-7;
energy = totCounts*calFact;