% daq2ServerStyle.m
%
% Function to extract DAQ saved data into a more familiar (at least for
% me) style data structure. Tries to read in as many channels as possible
% and saves them into the appropriate folder with appropriate names.
%
%
% Update Nov 2017 - now saving files into dedicated folders for each
% diagnostic:
% /rootfol/YYYYMMDD/YYYMMDDrRRR/diagnostic/YYYYMMDDrRRRsSSS_diag.ext
% for images and traces, and
% /rootfol/YYYYMMDD/YYYMMDDrRRR/diagnostic.ext
% for single valued diagnostics.
%
% Update Dec 2017 - Fixed saving of slow-collector data to now necessarily
% refer to the same shot as all the other data (or the one with the latest
% EvID).
%
% Update Feb 2018 - If the daq fails, the code will now run
% checkSavedDataInDAQ function and then try the retrieval again on the
% channels, that have actually saved some data. Should make it more
% useable!
%
% Update Mar 2018 - Added subsecond resolution for times, allowing for 0.5
% second differentiation between shots.
%
% Arguments:
%           runName - number of run to extract from the DAQ or filename
%               of a mat file containing the struct data that was extracted
%               and saved. The mat file name must contain a 5 digit run
%               number.
%           chanlist - List of channels to extract as a cell array or path
%               to an xml file to read from DAQ. The cell array needs to
%               be a column vector and all entries need to be proper:
%               '<Chan name='FLASH.DIAG/FFWDDIAG2.CAM/MP2NF' dtype='27'/>'
%           rootfol - folder where to save the converted data
%           verbosity - 0 for no output, 1 for output on shot ID being
%               saved, 2 for information about diagnostics, 3 for
%               information about the total number of PIDs in run
%
% Use:
%       daq2ServerStyle(20202, 'xmlfile.xml', [], 1) - writes out run 20202
%           into default path using the channels in file xmlfile.xml.
%       daq2ServerStyle(20202, {'Channel one', 'Channel 2'}, [], 1) -
%           writes out channels one and 2 from run 20202 into default path.
%       daq2ServerStyle('run20202.mat', 'xmlfile.xml', [], 1) - writes out
%           run 20202 saved into mat file into default path.
%       daq2ServerStyle([], 'xmlfile.xml', [], 1) - writes out run with the
%           number extracted from the xml file.
%
% KP, DESY, 2017

%%
classdef daq2ServerStyle < handle
    properties
        runName
        chanlist
        rootfol
        verbosity
    end
    
    events
        shotProgress
    end
    
    methods
        function self = daq2ServerStyle(runName, chanlist, rootfol, verbosity, varargin)
            %This is somewhat dirty. I use this class initialiser to allow
            %me to catch the events from the class in another class. If
            %only four input argument are given, then we start extraction
            %right away: should be no different to the user!
            
            self.runName = runName;
            self.chanlist = chanlist;
            self.rootfol = rootfol;
            % Default verbosity is all
            if nargin==3
                self.verbosity = 2;
            else
                self.verbosity = verbosity;
            end
            if nargin < 5
                self.extract;
            end
        end
        
        function extract(self)
            %runName = self.runName;
            %verbosity = self.verbosity;
            %chanlist = self.chanlist;
           
            % Parse arguments. Firstly runName
            if ischar(self.runName)
                load(self.runName)
                daq_run_num = regexp(self.runName, '\d{5}', 'match');
                if iscell(daq_run_num); daq_run_num = daq_run_num{1}; end
            else
                [data, daq_run_num] = self.loadDAQfile;
                if isempty(data)
                    % There was an error. Let's try running daqState control:
                    if ischar(self.chanlist)
                        daqState = checkSavedDataInDAQ(self.runName, self.chanlist);
                        goodChannels = {};
                        for k=1:length(daqState)
                            if ~isempty(strfind(daqState(k).status, 'Data found'))
                                goodChannels{end+1} = ['<Chan name=''' daqState(k).name,...
                                    ''' />'];
                            end
                        end
                        % Sweet ol' recursion
                        daq2ServerStyle(self.runName, goodChannels', self.rootfol, self.verbosity);
                    end
                    return;
                end
            end
            shotIDs = fieldnames(data); % gets a list of the PIDs (shots taken/saved)
            shot1 = data.(char(shotIDs(1))); % gets the first PID structure
            DOOCSNames = fieldnames(shot1); %gets a list of the channels in the first PID, this shouldb be the same for all PIDs
            DiagNamesRAW = cell(size(DOOCSNames));
            for k=1:length(DiagNamesRAW)
                ttemp = strsplit(DOOCSNames{k}, '/');
                DiagNamesRAW{k} = ttemp{end};
            end
            % And filter out the 'time' entry from Diag list
            logm = cellfun(@(x) isempty(strfind(x, 'time')), DiagNamesRAW); %#ok<STREMP>
            DOOCSNames = DOOCSNames(logm);
            DiagNamesRAW = DiagNamesRAW(logm);
            DiagNamesTimed = false(size(DiagNamesRAW)); % This will stire info about shot time being present
            
            % Now get the time of the run, from the first shot
            shot_unix_time = shot1.time(1) + 1e-6*shot1.time(2); % Add the sub-second timing
            shot_norm_time = shot_unix_time/(24*3600) + datenum(1970,1,1);
            % Still need tdatefolo sort out the time offset !
            
            % Now make the server structure for this run.
            % This is the base folder for all data
            if nargin==2 || isempty(self.rootfol)
                self.rootfol = '/home/fflab28m/Kris/ServerStyleData';
            end
            shot_date = datestr(shot_norm_time, 'YYYYmmdd');
            datefol = fullfile(self.rootfol, shot_date);
            if ~exist(datefol, 'dir'); mkdir(datefol); end
            % Now, runnumber is important ! :)
            serverrunnumber = self.getNextServerRunNumber(daq_run_num, datefol);
            run_name = sprintf('%sr%03i', shot_date, serverrunnumber);
            runfol = fullfile(datefol, run_name);
            if ~exist(runfol, 'dir'); mkdir(runfol); end
            
            % And now folders for the individual diagnostics. We also get the more
            % useful names of some diagnostics, and store them for future use.
            DiagNames = cell(size(DiagNamesRAW));
            for k=1:length(DiagNamesRAW)
                if strcmp(self.getDiagnosticName(DOOCSNames{k}), '')
                    DiagNames{k} = DiagNamesRAW{k};
                else
                    DiagNames{k} = self.getDiagnosticName(DOOCSNames{k});
                end
            end
            % Make a folder for each diagnostic. Remember to use the more useful names!
            for k=1:length(DiagNames)
                diagfol = fullfile(runfol, DiagNames{k});
                if ~exist(diagfol, 'dir')
                    mkdir(diagfol);
                end
            end
            % Print out some useful info, if requested
            if self.verbosity > 3
                for k=1:length(DiagNames)
                    if ~isempty(DiagNames{k})
                        fprintf('Channel %s is saved as diag %s\n', DOOCSNames{k}, DiagNames{k});
                    end
                end
            end
            singleValueData = struct;
            % We now have all the necessary run directories.
            % Start looping over diagnostics
            % and saving them in the appropriate place, as an appropriate file.
            % For all constants, make a structure where the field name is the shot
            % number. Save these into a file later on.
            
            % Use .png files for all images
            % Use .txt files for spectra or ADC traces
            % Write all constants into a mat file with shot number as field name
            
            % Write also a report, which details which PID corresponds to a shot
            % number, for each diagnostic.
            convReport.diags = struct;
            convReport.times = struct;
            convReport.daqrun = daq_run_num;
            convReport.serverrun = run_name;
            
            % Main loop!
            % At the time of writing (11/11/2017) there is an issue where one shot is
            % saved across different PIDs. This necessitates the sorting.
            shottimes = zeros(size(shotIDs)); % Array of PID saving times
            last_shot_time = shot_unix_time; % Value for the last shot, used for sorting
            shotID = 1;
            for thisPID = 1:length(shotIDs)
                fprintf('Analysing Event number %i, out of %i\n', thisPID, length(shotIDs));
                self.notify('shotProgress', daq2ServerEvent(length(shotIDs), thisPID));
                shot_strct = data.(shotIDs{thisPID});
                for diag = 1:length(DOOCSNames)
                    diag_strct = shot_strct.(DOOCSNames{diag});
                    % Check whether any data was actually saved for this diagnostic
                    if diag_strct.daqtype == 0
                        if self.verbosity > 5
                            fprintf('\t\t Diagnostic not present for this Event: %s\n', DiagNamesRAW{diag});
                        end
                        continue;
                    else
                        if self.verbosity > 5
                            fprintf('\t Writing diagnostic %s.\n', DiagNamesRAW{diag});
                        end
                    end
                    % Now do the shot number filtering:
                    if diag_strct.daqtype ~= 33
                        diag_time = diag_strct.sec + 1e-6*diag_strct.usec;
                        shottimes(thisPID) = diag_time - shot_unix_time;
                        convReport.times.(sprintf('s%06i', shotID)) = diag_time;
                        % Now do the sorting based on the time of the PID. If the
                        % difference from previous is large enough, we have a new shot!!!!!
                        if abs(last_shot_time - shottimes(thisPID)) > 0.5
                            shotID = shotID + 1;
                            last_shot_time = shottimes(thisPID);
                        end
                        DiagNamesTimed(diag) = true;
                    end
                    
                    % ------------ ADC traces
                    if diag_strct.daqtype==41 || diag_strct.daqtype==10 % ADC trace or Toroid
                        x_axis = diag_strct.start + (0:(diag_strct.ndata-1))*diag_strct.inc;
                        y_axis = diag_strct.data;
                        diagfilename = sprintf('%ss%06i_%s.txt', run_name, shotID, DiagNames{diag});
                        fID = fopen(fullfile(runfol, DiagNames{diag}, diagfilename), 'w');
                        for k=1:length(y_axis)
                            fprintf(fID, '%1.8e,%1.8e\n', x_axis(k), y_axis(k));
                        end
                        fclose(fID);
                        convReport.diags.(DiagNames{diag}).(sprintf('s%06i', shotID)).EvID = shotIDs{thisPID};
                        convReport.diags.(DiagNames{diag}).(sprintf('s%06i', shotID)).sec = diag_time;
                    end
                    
                    if diag_strct.daqtype == 40 || diag_strct.daqtype == 45 % BPM trace or OceanOptics
                        bpm = diag_strct.data;
                        diagfilename = sprintf('%ss%06i_%s.png', run_name, shotID, DiagNames{diag});
                        dlmwrite(diagfilename, bpm, ',');
                    end
                    
                    % ------------- Camera images
                    if diag_strct.daqtype == 27 % Image
                        imm = diag_strct.data';
                        diagfilename = sprintf('%ss%06i_%s.png', run_name, shotID, DiagNames{diag});
                        creation_time = datestr(datetime(diag_time,...
                            'ConvertFrom','posixTime','TimeZone','Europe/Berlin'),...
                            'yyyy-mm-dd HH:MM:SS.FFF');
                        unixComment = sprintf('unix=%2.6f', diag_time);
                        imwrite(imm, fullfile(runfol, DiagNames{diag}, diagfilename),...
                            'CreationTime', creation_time, 'Comment', unixComment);
                        convReport.diags.(DiagNames{diag}).(sprintf('s%06i', shotID)).EvID = shotIDs{thisPID};
                        convReport.diags.(DiagNames{diag}).(sprintf('s%06i', shotID)).sec = diag_time;
                    end
                    
                    % ------------- Constants -> Dealt with below!
                    %if diag_strct.daqtype == 33 % Single number
                    %dat = diag_strct.data;
                    % Put the data in the correct place in the structure.
                    %singleValueData.(DiagNames2{diag}).(sprintf('s%03i', shotID)) = dat;
                    %convReport.diags.(DiagNames2{diag}).(sprintf('s%03i', shotID)).EvID = shotIDs{thisPID};
                    %convReport.diags.(DiagNames{diag}).(sprintf('s%03i', thisPID)).sec = diag_struct.sec;
                    %end
                end
            end
            
            % ----------- UPDATE for saving constants...
            % I think the previous method wasn't working too well, so will use a
            % different approach. For every saved shot, we check the conversion report
            % for all different EvIDs assigned to this shot. Then we check those EvIDs
            % and see whether the slow collector saved anything on these particular
            % EvIDs.
            shotss = fieldnames(convReport.times);
            TimedDiags = DiagNames(DiagNamesTimed);
            for s=1:length(shotss)
                EvIDs = {};
                shotID = shotss{s};
                % The loop gets all EvIDs saved for shot s by diagnostic with timing
                for d=1:length(TimedDiags)
                    try
                        EvIDs{end+1} = convReport.diags.(TimedDiags{d}).(shotID).EvID;
                    catch
                    end
                end
                EvIDs_unique = unique(EvIDs);
                for ev=1:length(EvIDs_unique)
                    shot_strct = data.(EvIDs_unique{ev});
                    for diag=1:length(DOOCSNames)
                        diag_strct = shot_strct.(DOOCSNames{diag});
                        % ------------- Constants
                        if diag_strct.daqtype == 33 % Single number
                            dat = diag_strct.data;
                            % Put the data in the correct place in the structure.
                            singleValueData.(DiagNames{diag}).(shotID) = dat;
                            convReport.diags.(DiagNames{diag}).(shotID).EvID = EvIDs_unique{ev};
                        end
                    end
                end
            end
            
            % I was lazy before - remove the folders for diagnostics which are actually
            % empty!
            for k=1:length(DiagNames)
                flist = dir(fullfile(runfol, DiagNames{k}));
                if self.verbosity==2
                    fprintf('%s : %i files\n', DiagNames{k}, length(flist));
                end
                if length(flist)==2
                    rmdir(fullfile(runfol, DiagNames{k}));
                end
            end
            
            % And write the single value data to individual files
            singleValueDataNames = fieldnames(singleValueData);
            for k=1:length(singleValueDataNames)
                data = singleValueData.(singleValueDataNames{k}); %#ok<NASGU>
                save(fullfile(runfol, singleValueDataNames{k}), 'data');
            end
            
            % And write a readme - most important thing ever!
            self.writeDAQreadme(daq_run_num, run_name, datefol)
            assignin('base', 'convReport', convReport);
            self.writeConvReport(runfol, convReport);
            % And also try to copy over the focal spot images.
            self.copyFocalSpotImages(shot_date, datefol)
        end % function
        
        %% Other functions
        function [data, runNumber] = loadDAQfile(self)
            addpath('/local/lib');
            tempxmlFileName = 'tempChanList.xml';
            if ~isempty(self.runName) % Run number given - otherwise just use the file!
                if ischar(self.chanlist) % It is a filename - use the given runName
                    xmlFileName = self.chanlist;
                    % Need to replace the run name in the file with the one we have...
                    % Get the file contents
                    xmlFileCmds = getFileText(xmlFileName);
                    % Find the line where the run number is mentioned
                    inds = cellfun(@(x) ~isempty(strfind(x, 'number')), xmlFileCmds); %#ok<STREMP>
                    % If a run number is not found, quit!
                    if sum(inds)==0
                        error('No DAQ run number specified!');
                    else
                        %runline = xmlFileCmds(inds);
                        %runNumber = regexp(runline, '\d{5}', 'match');
                        % Update the run number in the file's contents
                        xmlFileCmds{inds} = sprintf('<RunFirst  number=''%i''/>', self.runName);
                    end
                else % Chanlist is a cell array. Write the stuff around it!
                    xmlFileCmds = {'<DAQREQ>';...
                        '<ReqId  id=''1502899874''/>';...
                        sprintf('<RunFirst  number=''%i''/>', self.runName);...
                        '<Exp  name=''flashfwd''/>';...
                        '<DDir name=''/daq_data/flashfwd/EXP/''/>';...
                        '<CDir name=''/daq/ttf2/adm/''/>';...
                        '<ScanMode mode=''0x2000000f'' />'};
                    xmlFileCmds = [xmlFileCmds; self.chanlist];
                    xmlFileCmds{end+1} = '</DAQREQ>';
                end
                % xmlFileCmds
                % And pipe the text into a temp xml file.
                xmlFileName = tempxmlFileName;
                fid = fopen(xmlFileName, 'w');
                for k=1:length(xmlFileCmds); fprintf(fid, '%s\n', xmlFileCmds{k}); end
                fclose(fid);
                runNumber = self.runName;
            else % No run number present - use the file as it is.
                xmlFileName = self.chanlist;
                % But also extract the run number...
                xmlFileCmds = getFileText(xmlFileName);
                % Find the line where the run number is mentioned
                inds = cellfun(@(x) ~isempty(strfind(x, 'number')), xmlFileCmds); %#ok<STREMP>
                runline = xmlFileCmds{inds};
                runNumber = regexp(runline, '\d{5}', 'match');
                runNumber = runNumber{1};
            end
            
            % Get the actual data from the prepped xml file.
            [data,err] = daq_read(xmlFileName);
            % And clean up the mess we left behind
            if exist(tempxmlFileName, 'file')
                delete(tempxmlFileName);
            end
            if ~isempty(err)
                disp(err);
                return
            end
            
        end
        
        
        function writeDAQreadme(self, daqrunno, serverrun, datefol)
            fname = fullfile(datefol, 'README.txt');
            % User must know - the last entry is the correct one...
            % But also, if the run has already been extracted, there is no need to do
            % it again.
            nextRun = self.getNextServerRunNumber(daqrunno, datefol);
            if nextRun~=1 % This means that README.txt already exists...
                ftext = getFileText(fullfile(datefol, 'README.txt'));
                if length(ftext)>=nextRun
                    return
                end
            end
            fid = fopen(fname, 'a');
            if ischar(daqrunno)
                fprintf(fid, '{"DAQ run": %s, ', daqrunno);
            else
                fprintf(fid, '{"DAQ run": %i, ', daqrunno);
            end
            fprintf(fid, '"Server run": "%s"}\n', serverrun);
            fclose(fid);
        end
        
        
        function nextRun = getNextServerRunNumber(self, daqrunno, datefol)
            fname = fullfile(datefol, 'README.txt');
            if exist(fname, 'file')
                % If the README exists, check whether this DAQ run has already been
                % saved.
                readm = getFileText(fname);
                %nextRun = length(readm) + 1;
                runss = jsondecode(readm{1});
                for k=2:length(readm)
                    runss(k) = jsondecode(readm{k});
                end
                daqruns = cell2mat({runss.DAQRun});
                serverruns = {runss.ServerRun};
                ind = find(daqrunno == daqruns);
                % If this DAQ run number is present, rewrite the data instead of making
                % a new one. But if not, make a new run.
                if ~isempty(ind)
                    nextRun = str2double(serverruns{ind(1)}(end-2:end)); % Hardcoded filth
                else
                    nextRun = length(runss) + 1;
                end
            else
                nextRun = 1;
            end
        end
        
        
        function diagName = getDiagnosticName(self, DOOCSname)
            % returns a human readable diagnostic name
            fpath = fileparts(which('DataDisplay.Display1D'));
            flines = getFileText(fullfile(fpath,'ListofKnownAddresses.txt'));
            displayList = cell(1);
            doocsList = cell(1);
            for k=1:length(flines)
                ind = strfind(flines{k}, '=');
                displayName = allwords(strtrim(flines{k}(1:ind-1)));
                for i=1:length(displayName); displayName{i}(1) = upper(displayName{i}(1)); end
                displayList{k} = strjoin(displayName, '');
                doocsList{k} = strtrim(flines{k}(ind+1:end));
            end
            inds = strcmp(doocsList, DOOCSname);
            if sum(inds)==0
                diagName = '';
            else
                diagName = displayList{inds};
            end
        end
        
        
        function writeConvReport(self, runfol, convReport)
            str2 = jsonencode(convReport);
            try
                py.importlib.import_module('json');
                collections = py.importlib.import_module('collections');
                formattedJson = char(py.json.dumps(py.json.loads(str2,...
                    pyargs('object_pairs_hook',collections.OrderedDict)),...
                    pyargs('indent',int32(4))));
            catch err
                fprintf('Error formatting json: %s\n', err.message);
                formattedJson = str2;
            end
            fid = fopen(fullfile(runfol, 'ConversionReport.json'), 'w');
            fprintf(fid, '%s', formattedJson);
            fclose(fid);
        end
        
        
        function copyFocalSpotImages(self, shot_date, datefol)
            % This will look into the /home/fflab28m/DATA/FocalSpot/TargetChamber
            % and copy all the files it finds over.
            % We assume the folder name is in the correct format!!!!!!
            infol = fullfile('/home/fflab28m/DATA/FocalSpot/TargetChamber', shot_date);
            flist = dir(fullfile(infol, '*.*'));
            if isempty(flist); return; end
            fspotSaveFol = fullfile(datefol, 'FocalSpot');
            if ~exist(fspotSaveFol, 'dir'); mkdir(fspotSaveFol); end
            for k=3:length(flist)
                copyfile(fullfile(infol, flist(k).name), fullfile(fspotSaveFol, flist(k).name));
            end
        end
    end
end