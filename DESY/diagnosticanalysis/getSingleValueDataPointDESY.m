% getSingleValueDataPointDESY.m
%
% Gets a single datapoint 

function dataPoint = getSingleValueDataPointDESY(diagnostic,date_,run_,shot_)

% dataPoint = nan;
% Parse file name
% [rootfol, shotstr] = parseShotTimeDESY(date_, run_, shot_);
% if strcmp(rootfol, 'error')
%     return;
% end
% datafilename = fullfile(rootfol, [diagnostic '.mat']);
% if ~exist(datafilename, 'file')
%     fprintf('Diagnostic error, file does not exist: %s\n', datafilename);
%     return;
% else
%     fdat = load(datafilename);
%     if isfield(fdat.data, shotstr(end-3:end))
%         dataPoint = fdat.data.(shotstr(end-3:end));
%     else
%         return;
%     end
% end
datapoint = getRAWdiagnostic(date_, run_, shot_, diagnostic, 'point');