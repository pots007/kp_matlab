% getNumberOfShotsInRun.m
%
% Returns the total maximum number of shots in a run.
%
% Accepts date and run formats and also a date in runstring format
%
% KP, DESY, 2017

function shotNum = getNumberOfShotsInRun(date_, run_)
% Only look into folders with items in, and not worry about the mat files

shotNum = -1;
if ischar(date_) && length(date_) == 12
    date_temp = [date_ 's001'];
    fname = parseShotTimeDESY(date_temp, 001, 001);
else
    fname = parseShotTimeDESY(date_, run_, 1);
end
if strcmp(fname, 'error')
    fprintf('This run does not exist!\n');
    return
end
[rootfol, ~, ~] = fileparts(fname);
if ~exist(rootfol, 'dir')
    fprintf('This run does not exist: %s\n', rootfol);
    return;
end
itemList = dir(rootfol);
shotNums = zeros(length(itemList) - 2,1);
for k=3:length(itemList)
    if itemList(k).isdir
        filelist = dir(fullfile(rootfol, itemList(k).name));
        shotNums(k-2) = length(filelist) - 2;
    end
end
shotNum = max(shotNums);