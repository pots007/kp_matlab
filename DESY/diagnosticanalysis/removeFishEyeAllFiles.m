% removeFishEyeAllFiles.m
%
% Script to go through a lot of runs and remove the fisheye effect from
% them. 
% The user can specify whether the folder given is a run, a date or a
% collection of dates. 
% 
% Useage: 
%   removeFishEyeAllFiles(/path/to, 'run', screenID) to
%   removeFishEyeAllFiles(/parh/to, 'date', ,screenID)
%   removeFishEyeAllFiles(/parh/to, 'collection', screenID)
%
% KP, 2017, DESY

function removeFishEyeAllFiles(rootfol, foldertype, screenID)

% Defaults: one run for screen 3 
if nargin<3; screenID = 3; end
if nargin<2; foldertype = 'run'; end

% Useful variables
if screenID == 3
    screenName = 'ElectronSpectrometer3';
elseif screenID == 300
    screenName = 'ElectronSpectrometer3_p';
elseif screenID == 2
    screenName = 'ElectronSpectrometer2';
end
% The image file extension
fileExt = '.png';

if strcmp(foldertype, 'run') % For one run
    fprintf('\tWorking in folder %s\n', rootfol);
    flist = dir(fullfile(rootfol, screenName, ['*' screenName '*']));
    outfol = fullfile(rootfol, [screenName '_dewarp']);
    if ~exist(outfol, 'dir'); mkdir(outfol); end
    for f = 1:length(flist)
        tic
        fprintf('\t\tProcessing file %3i out of %i...', f, length(flist));
        imm = imread(fullfile(rootfol, screenName, flist(f).name));
        imm_ = removeFishEye(imm, screenID);
        [~,fname,~] = fileparts(flist(f).name);
        imwrite(uint16(imm_), fullfile(outfol, [fname '_dewarp' fileExt]));
        fprintf(' in %2.3f seconds\n', toc);
    end
elseif strcmp(foldertype, 'date')
    flist = dir(rootfol);
    for fol=3:length(flist)
        if flist(fol).isdir
            removeFishEyeAllFiles(fullfile(rootfol, flist(fol).name), 'run', screenID);
        end
    end
end