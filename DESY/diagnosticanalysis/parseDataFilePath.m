% parseDataFilePath.m
%
% This function will return a suitably formatted filename based on the
% formatstring input. formatstring consists of letters YMDRS in some
% combination such as to format filename pattern. 
% Eg YYYYMMDD/YYYYMMDDrRRR/YYYYMMDDrRRRsSSSS_Diagname.raw would for a year
% 2018, month 5, date 6, run 9 and shot 34 yield a filename of
% 20180506/20180506r009/20180506r009s0034_Diagname.txt.
%
% The function will not replace single instances of tokens, so as above,
% Diagname does not become 6iagname.
%
% KP, DESY, May 2018

function outString = parseDataFilePath(fmtString, y, m, d, r, s) %#ok<*INUSD>

% Debugging:
%fmtString = 'YYYYMMDD/YYYYMMDDrRRR/YYYYMMDDrRRRsSSSSSS_diagname.raw';
%fmtString = 'YYYYMMDD/YYYYMMDDsSSSSSS_Diagname.raw';
%y = 2018; m = 4; d = 21; r = 9; s = 34;

% These are the things we will start matching
tokens = 'YMDRS';
outString = fmtString;

% Main loop, we go over each token and find places where multiples of these
% are seen. Then replace them with suitable formatted string
for t=tokens
    [starts, ends] = regexp(fmtString, [t '+']);
    if ~isempty(starts) % Catch out not found tokens
        for i=1:length(starts)
            ss = starts(i); ee = ends(i); 
            % The next line should avoid getting confused over instances of
            % the tokens appearing on their on, say as a capital letter in
            % the diagnostic name or the extension. 
            if ss == ee; continue; end 
            outString(ss:ee) = sprintf(['%0' num2str(ee-ss+1) 'i'], eval(lower(t)));
        end
    end
end
