% analyse20171207r006_Dazzler.m
%
% This will analyse the dazzler scan we did with 0.5% Ar gas. Mainly
% looking at charge, but also beam pointing.
%
% make a list of dazzlers for the analysis of this run.

date_ = 20171207; run_ = 6;

% The numbers below don't exactly mathc what's written in the log book!
dazzdata2 = [];
dazzdata2(1 :11) = 36600;
dazzdata2(12:21) = 36400;
dazzdata2(22:33) = 36200;
dazzdata2(34:42) = 36800;
dazzdata2(43:51) = 37000;
dazzdata2(52:61) = 37200;
dazzdata2(62:95) = 36600;
% Seems impossible to map out the 3rd order ones....
dazzdaat3 = [];
dazzdata3(1 :61) = 73.40e3;
dazzdata3(62:71) = 68.40e3; 
dazzdata3(72:76) = 63.40e3;
dazzdata3(77:81) = 78.40e3;
dazzdata3(82:90) = 70.40e3;

fold = parseShotTimeDESY(date_, run_, 1);
data = struct;
for k=1:length(dazzdata2)
    data.(sprintf('s%03i', k)) = dazzdata2(k);
end
save(fullfile(fold, 'Dazzler2order'), 'data');
data = struct;
for k=1:length(dazzdata3)
    data.(sprintf('s%03i', k)) = dazzdata3(k);
end
save(fullfile(fold, 'Dazzler3order'), 'data');

%% And now analyse that run...
damon = Analysis.Diagnostic(date_, run_, 'DAMON_CHARGE');
Eprofile = Analysis.Diagnostic(date_, run_, 'TargetChamberProfile');
dazz2 = Analysis.Diagnostic(date_, run_, 'Dazzler2order');
dazz3 = Analysis.Diagnostic(date_, run_, 'Dazzler3order');

% Set analysis functions
damon.setAnalysisFunction(@(x) getDamonCharge20gb(max(x(:,2))));
damon_all = damon.getAllAnalysed();
dazz2_all = dazz2.getAllBinnedRAW();
dazz3_all = dazz3.getAllBinnedRAW();

%% And plot the variation of Q with 2nd order:
dazz_pars = unique(dazz2_all(1:61));
dazz_clean = []; damon_clean = [];
damon_stat = {};
for k=1:length(dazz_pars)
    logm = dazz2_all == dazz_pars(k) & dazz2.shotNumbers<62;
    % Find the shot numbers with this dazzler param
    shotss = dazz2.shotNumbers(logm);
    damon_stat{k} = [];
    for i=1:length(shotss)
        if ~isempty(find(damon.shotNumbers==shotss(i),1))
            dazz_clean(end+1) = dazz_pars(k);
            damon_clean(end+1) = damon.getAnalyseddiag(shotss(i));
            damon_stat{k}(end+1) = damon.getAnalyseddiag(shotss(i));
        end
    end
end
hfig = figure(38131); clf(hfig);
set(gca, 'NextPlot', 'add');
hh2 = shadedErrorBar(unique(dazz_pars), cellfun(@mean, damon_stat), cellfun(@std, damon_stat));
set(hh2.mainLine, 'LineWidth', 2, 'Color', [0,0.4470,0.741]);
set(hh2.patch, 'FaceColor', hh2.mainLine.Color, 'FaceAlpha', 0.2);
set(hh2.edge, 'Color', hh2.mainLine.Color);
hh1 = plot(dazz_clean, damon_clean, 'rd', 'LineWidth', 2);
ylabel('DAMON charge / pC');
xlabel('Dazzler GDD value / fs^{-2}');
set(gca, 'XLim', [36100 37300], 'LineWidth', 2, 'Box', 'on');
saveas(hfig, fullfile(Analysis.getAnalysisRootFol, '20171207r006_Dazzler2nd'), 'svg')

%% Look at the pointing stability.
dazz_pars = unique(dazz2_all(1:61));
dazz_clean = []; damon_clean = [];
Eprofile.setAnalysisFunction(@(x) centroidlocationsmatrix(medfilt2(x)));
Eprofile_point = {};
Eprofile_clean = [];
for k=1:length(dazz_pars)
    logm = dazz2_all == dazz_pars(k) & dazz2.shotNumbers<62;
    % Find the shot numbers with this dazzler param
    shotss = dazz2.shotNumbers(logm);
    Eprofile_point{k} = [];
    for i=1:length(shotss)
        if ~isempty(find(Eprofile.shotNumbers==shotss(i),1))
            dazz_clean(end+1) = dazz_pars(k);
            Eprofile_point{k}(end+1,:) = Eprofile.getAnalyseddiag(shotss(i));
            Eprofile_clean(end+1,:) = Eprofile_point{k}(end,:);
        end
    end
end

% Plot the pointing stabilities...
hfig = figure(38131); clf(hfig);
set(gca, 'NextPlot', 'add');
% x first
hh2 = shadedErrorBar(unique(dazz_pars), cellfun(@(x) mean(x(:,1)), Eprofile_point),...
    cellfun(@(x) std(x(:,1)), Eprofile_point));
set(hh2.mainLine, 'LineWidth', 2, 'Color', [0,0.4470,0.741]);
set(hh2.patch, 'FaceColor', hh2.mainLine.Color, 'FaceAlpha', 0.2);
set(hh2.edge, 'Color', hh2.mainLine.Color);
hh1 = plot(dazz_clean, Eprofile_clean(:,1), 'rd', 'LineWidth', 2);
% then y

hh2 = shadedErrorBar(unique(dazz_pars), cellfun(@(x) mean(x(:,2)), Eprofile_point),...
    cellfun(@(x) std(x(:,2)), Eprofile_point));
set(hh2.mainLine, 'LineWidth', 2, 'Color', [0,0.4470,0.741]);
set(hh2.patch, 'FaceColor', hh2.mainLine.Color, 'FaceAlpha', 0.2);
set(hh2.edge, 'Color', hh2.mainLine.Color);
hh1 = plot(dazz_clean, Eprofile_clean(:,2), 'ko', 'LineWidth', 2);
%ylabel('DAMON charge / pC');
%xlabel('Dazzler GDD value / fs^{-2}');
%set(gca, 'XLim', [36100 37300], 'LineWidth', 2, 'Box', 'on');
%saveas(hfig, fullfile(Analysis.getAnalysisRootFol, '20171207r006_Dazzler2nd'), 'svg')
