% getDamonCharge20db.m
%
% Get Damon charge for a particular shot or raw value.
%
% KP, DESY, 2017

function Q = getDamonCharge20gb(date_, run_, shot_)

Q = nan;
if ~ischar(date_) && date_< 2^16
    rawVal = date_;
else
    rawVal = getRAWdiagnostic(date_, run_, shot_, 'DAMON_CHARGE', 'trace');
%     fname = parseShotTimeDESY(date_, run_, shot_);
%     if strcmp(fname, 'error')
%         return;
%     end
%     [rootf, shotstr, ~] = fileparts(fname);
%     datafilename = fullfile(rootf, 'DAMON_CHARGE', [shotstr '_DAMON_CHARGE.txt']);
%     if ~exist(datafilename, 'file')
%         fprintf('File does not exist error: %s\n', datafilename);
%         return;
%     end
%     dat = importdata(datafilename);
%     if isstruct(dat)
%         rawVal = max(dat.data(:,2));
%     else
%         rawVal = max(dat(:,2));
%     end
end
a0= 3.353;
a1=5.847e-5;
a2=7.56e-5;
Q = a2* 10 .^ (a0+a1*rawVal);