% plotDamon_ICT_Screen.m
%
% Script to look through a run and plot the ICT trace vs Damon trace vs
% Eprofile screen trace to check for stability and correlations.

date_ = 20171208;
run_ = 5;

saveFlag = 1;
pngFlag = 0;    

% Set up graphics.
hfig = figure(12321); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [100 100 1200 900]);
locs = GetFigureArray(2, 2, [0.07 0.05 0.08 0.1], 0.1, 'across');
clear axx;
axx(1) = axes('Parent', hfig, 'Position', [locs(1,1) locs(2,1) 0.85 locs(4,1)]);
axx(2) = axes('Parent', hfig, 'Position', locs(:,3));
axx(3) = axes('Parent', hfig, 'Position', locs(:,4));
set(axx, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);

%% Set up diagnostics
damon = Analysis.Diagnostic(date_, run_, 'DAMON_CHARGE');
ict = Analysis.Diagnostic(date_, run_, 'ICT');
screen = Analysis.Diagnostic(date_, run_, 'TargetChamberProfile');
% Set necessary analysis functions.
damon.setAnalysisFunction(@(x) getDamonCharge20gb(max(x(:,2))));
ict.setAnalysisFunction(@(x) ICTanal(x(:,2)));
screen.setAnalysisFunction(@(x) sum(x(:))*1e-5); % Arb factor used here...

% Get the data analysed!
damon_all = damon.getAllAnalysed;
ict_all = ict.getAllAnalysed;
screen_all = screen.getAllAnalysed;

%% Plot the data
% First, shot by shot for all three
cla(axx(1));
h(1) = plot(axx(1), damon.shotNumbers, damon_all, '--x');
h(2) = plot(axx(1), ict.shotNumbers, ict_all, '--o');
h(3) = plot(axx(1), screen.shotNumbers, screen_all, '--d');

xlabel(axx(1), 'Shot number'); ylabel(axx(1), 'Charge / pC');
title(axx(1), sprintf('Date %i, run %i', date_, run_));
legend(h, {'DaMon', 'ICT', 'Screen'});

% Now correlation between Damon and ICT
cla(axx(3));
hh = plot(axx(3), damon_all, ict_all, 'd');
xlabel(axx(3), 'DaMon charge / pC');
ylabel(axx(3), 'ICT charge / pC');

% For stuff involving the screen, need to ensure we plot the same shots.
[shots_comm, indA,indB] = intersect(screen.shotNumbers, ict.shotNumbers);
cla(axx(2));
hhh(1) = plot(axx(2), screen_all(indA), damon_all(indB), 'x');
hhh(2) = plot(axx(2), screen_all(indA), ict_all(indB), 'o');
legend(hhh, {'DaMon', 'ICT'});
xlabel(axx(2), 'Total screen signal / au');
ylabel(axx(2), 'Charge / pC');

set([h, hh, hhh], 'LineWidth', 2, 'MarkerSize', 9);

setAllFonts(hfig, 14); make_latex(hfig);
if saveFlag
    savpath = fileparts(Analysis.getDataRootFol);
    savfol = fullfile(savpath, '28manalysis');
    if ~exist(savfol, 'dir')
        savfol = fullfile(savpath, 'Analysis');
    end
    savfname = fullfile(savfol, ...
        sprintf('ChargeDiags_%i_r%03i', date_, run_));
    export_fig(savfname, '-pdf', '-nocrop', hfig);
    if pngFlag
        export_fig(savfname, '-png', '-m3', '-nocrop', hfig);
    end
end