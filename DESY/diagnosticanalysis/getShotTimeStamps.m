% getShotTimeStamps.m
%
% Function that returns a hash map, where the FLASH shot IDs are the keys
% and each entry contains the shot time and the shot unix timestamp
%
% Arguments: 
%           folder - path to where the dataset lives.

function [hMap, mat] = getShotTimeStamps(folder)
% Get file list, filter out folders.
flist = dir(folder);
flist = flist(~logical(cell2mat({flist.isdir})));
nFiles = length(flist);

mat = cell(nFiles, 3);
for k=1:length(mat)
    if nFiles>500 && mod(k,100)==0
        fprintf('\tExtracting info of file %5i of %i.\n', k, nFiles);
    end
    info = imfinfo(fullfile(flist(k).folder, flist(k).name));
    % Shot number:
    mat{k,1} = str2double(regexp(flist(k).name, '\d{9}', 'match'));
    %if ~isempty(info.CreationTime); mat(k,2) = info.CreationTime; end;
    mat{k,2} = info.CreationTime;
    if ~isempty(info.Comment)
        unix_time = str2double(regexp(info.Comment, '\d+.\d+', 'match'));
        mat{k,3} = unix_time;
    end
end
hMap = [];