% plotDAQ2ServerReport.m
%
% This function aims to plot a report of the data extraction, highlighting
% what time any shots were taken and what DAQ shot any actualy data shot
% belonged to. Also highlights the missing data for some shots.

function plotDAQ2ServerReport(date_, run_, saveFlag)

if nargin<3; saveFlag = 0; end
% Read in the extraction report
datafol = parseShotTimeDESY(date_, run_, 1);
try
    ftext = fileread(fullfile(datafol, 'ConversionReport.json'));
    fdat = jsondecode(ftext);
catch err
    fprintf('Error in producing conversion report: %s\n', err.message);
    return;
end

% And now for the funnnnnnnn
% For each diagnostic, read the PID of the shot it was saved for.
nShots = length(fieldnames(fdat.times));
diags = fieldnames(fdat.diags);
nDiags = length(diags);
EvIDdat = nan(nDiags, nShots);
timedat = nan(nDiags, nShots);
for nDiag=1:nDiags
    diag = fdat.diags.(diags{nDiag});
    for s = 1:nShots
        shotstr = sprintf('s%03i', s);
        if isfield(diag, shotstr)
            EvID = diag.(shotstr).EvID;
            EvIDdat(nDiag, s) = str2double(EvID(4:end));
            if isfield(diag.(shotstr), 'sec')
                timedat(nDiag,s) = diag.(shotstr).sec;
            end
        end
    end
end

hfig = figure(876432); clf(hfig);
set(hfig, 'Position', [100 100 1300 800]);
ax1 = axes('Parent', hfig, 'Position', [0.13 0.06 0.85 0.6]);
ax2 = axes('Parent', hfig, 'Position', [0.13 0.66 0.85 0.34]);
minss = min(EvIDdat, [], 1);
EvIDdatplot = EvIDdat-repmat(minss, nDiags, 1);
EvIDdatplot(isnan(EvIDdatplot)) = -1;
imagesc((1:nShots)-0.5, (1:nDiags),EvIDdatplot, 'Parent', ax1);
set(ax1, 'YTick', 1:nDiags, 'YTickLabel', diags, 'CLIm', [-2 6],...
    'TickLabelInterpreter', 'none');
xlabel(ax1, 'Shot number');
% Make a grid as well
grid(ax1, 'on'); grid(ax1, 'minor');
drawnow;
xg = get(ax1, 'XGridHandle'); yg = get(ax1, 'YGridHandle');
xg.MinorTick = 1:nShots-1;
set(yg, 'MajorMinor', 'minor', 'GridLineStyle', 'none',...
    'MinorGridLineStyle', '-', 'MinorTick', (1:nDiags)-0.5)
% ------------ Now plot things to help us recognise the EvID.
colmapdat = nan(4, nShots);
for k=1:nShots
    IDs = unique(EvIDdatplot(:,k));
    if ~isempty(find(IDs==-1,1)); colmapdat(1,k) = -1; end
    IDss = unique(EvIDdat(~isnan(EvIDdat(:,k)),k));
    for i=1:length(IDss); colmapdat(i+1, k) = i-1; end
end
him = imagesc((1:nShots)-0.5, 1:4, colmapdat, 'Parent', ax2);
for k=1:nShots
    if ~isnan(colmapdat(1,k))
        text(ax2, k-0.5, 1.5, 'none', 'Rotation', 90, 'VerticalAlignment', 'middle');
    end
    for j=2:size(colmapdat,1)
        if ~isnan(colmapdat(j,k))
            text(ax2, k-0.5, j+.5, sprintf('PID%i', colmapdat(j,k)+minss(k)),...
                'Rotation', 90, 'VerticalAlignment', 'middle', 'FontSize', 6);
        end
    end
end
set(ax2, 'YLim', [0.5 4.5], 'XLim', ax1.XLim, 'XTickLabel', [],...
    'CLIm', [-2 6], 'YTickLabel', [], 'YTick', []);
grid(ax2, 'on'); grid(ax2, 'minor');
drawnow;
linkaxes([ax1, ax2], 'x');
xg = get(ax2, 'XGridHandle'); 
xg.MinorTick = 1:nShots-1;
% And set a suitabele colormap...
colzz = brewermap(8, 'Set2');
colzz(end+1,:) = [1 1 1];
colormap(flipud(colzz));
% And put some info on it...
ha = annotation('textbox', [0.02 0.7 0.11 0.25], 'String', ...
    {sprintf('DAQ run: %i', fdat.daqrun),...
    ['Server run: ' fdat.serverrun],...
    ['Report generated: ' datestr(now)]},...
    'LineStyle', 'none');
if saveFlag
    %export_fig(fullfile(datafol, 'ConversionReport'), '-pdf', '-nocrop', hfig);
    saveas(hfig, fullfile(datafol, 'ConversionReport.svg'), 'svg');
end