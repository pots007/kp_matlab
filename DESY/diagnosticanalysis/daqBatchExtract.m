% daqBatchExtract.m
%
% Command line tool to extract and save data from daq. This function is expected to
% be run from the cluster and provides the following:
%   Writing the raw data to disk, into individual files or hdf5 file
%   Applying an analysis function (such as FWB, SUM or etc) to data and
%       saving the results into a .mat file
%
% KP, DESY, Jan 2019

classdef daqBatchExtract < handle
   properties
       dat
   end
   
   methods 
       function self = daqBatchExtract(daqInput, funcList, filename)
           addpath('/local/lib')
       end
   end
end