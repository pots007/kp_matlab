datfol = 'C:\Users\kpoder\Work\2017_Gabi_interferometry\20170912';

flist = dir(fullfile(datfol, '*.tiff'));

fileID = 2;
if fileID==1
    imm = double(imread(fullfile(datfol, flist(7).name)));
elseif fileID ==2
    imm = double(imread(fullfile(datfol, flist(2).name)));
end
    
logm = 750:1100;
refim = double(imread(fullfile(datfol, flist(1).name)));

rotangle = 0;
imm = imrotate(imm(logm,:), rotangle);
refim = imrotate(refim(logm,:), rotangle);
imagesc(imm)


fftim = fftshift(fft2(imm));
fftref = fftshift(fft2(refim));

imagesc(log10(abs(fftim)))
%imagesc(log10(abs(fftref)))
%%
%%rm = [1078 1015 15 15];
if fileID==1
    rm = [1054 174 8 4];
elseif fileID==2
    rm = [1054 174 8 4];
end

maskimm = zeros(size(imm));
maskimm(rm(2):rm(2)+rm(4), rm(1):rm(1)+rm(3)) = 1;
%
filtimm = ifft2(fftshift(maskimm.*fftim));
filtref = ifft2(fftshift(maskimm.*fftref));
%imagesc(log10(abs((maskimm.*fftref))))

fphase = unwrap(angle(filtimm./filtref), 5.5, 2);
fphaseraw = unwrap(angle(filtimm), 5.5, 2);
sigroi = [1 100 2047 150];
zerophase = createBackground(fphase, sigroi, [], [], 2);
%zerophase2 = createBackground(fphaseraw, sigroi, [], [], 2);
%%
clf;
ymid = 200;
axx(1) = subplot(411);
imagesc(imm); colorbar;
axx(2) = subplot(4, 1, 2);
imagesc(fphase); colorbar;
%imagesc(fphaseraw - zerophase2); colorbar;
finalphase = imrotate(fphase - zerophase, 1.);
ffinalphase = (-finalphase);
%ffinalphase(ffinalphase<0) = 0;
finalphase = fliplr((ffinalphase)');
axx(3) = subplot(4, 1, 3);
imagesc(finalphase'); 
set(gca, 'NextPlot', 'add');
plot([0, 2048], [ymid ymid], '-k')
set(gca, 'CLim', [0 3.5]);
colorbar;
axx(4) = subplot(4, 1, 4);
imagesc(imm)

fSet.ymid = ymid; fSet.micperpix = 0.55; fSet.asmooth = 1e-3;
ne = AbelInversionNew(finalphase, fSet, 'Plasma', 800, 0, 150);
imagesc(fliplr(ne)/8.7e24)
colorbar
set(gca, 'CLim', [0 3]);


micperpix = 0.55;
press = 350; %mbar;