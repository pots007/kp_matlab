% makePlotofTheWeek_BestFocus.m
%
% Load the data and look at it.

fdat = load('C:\Users\kpoder\ownCloud\Experiments\2017_LWFA_campaign\20170810_bestfocus_data');
flist = dir('C:\Users\kpoder\ownCloud\Experiments\2017_LWFA_campaign\20170810\*1759*.raw');

load('C:\Users\kpoder\ownCloud\Experiments\2017_LWFA_campaign\20170810_BestFocusData');

hfig = figure(123321); clf(hfig);
hfig.Position = [100 100 700 900];

ax = gca;
set(ax, 'XLim', [-40 40], 'YLim', [-40 40], 'NextPlot', 'add', 'Box', 'off');
calib = 0.3779;

cc = centroidlocationsmatrix(imData);

xax = ((1:size(imData,2))-cc(1))*calib;
yax = ((1:size(imData,1))-cc(2))*calib;

%him = imagesc(xax, yax, imData);
plotimm = medfilt2(imData);
[X,Y] = meshgrid(xax, yax);
mask = X.^2 + Y.^2 > 30^2;
plotimm(mask) = nan;
hSurf = surf(yax, xax, plotimm');
%view(3);
view(-128.7, 20.4);

mx = min(xax); mmx = max(xax);
my = min(yax); mmy = max(yax);
X = [my my; mmy mmy];
Y = [-40 -40; -40 -40]+39;
Z = [mx mmx; mx mmx];

%X = [0 0; 1 1];
%Y = [0 0; 0 0];
%Z  [0 1; 0 1]
him(1) = surface(X,Z,Y, medfilt2(imData), 'FaceColor', 'texturemap', 'Parent', ax);
shading flat;

xlabel('x / micron');
ylabel('y / micron');
zlabel('$a_0$');

% And add the parameters
ha(1) = annotation('textbox', [0.3 0.9 0.8 0.1], 'String', sprintf('$w_x = (%2.1f \\pm %2.1f)\\, \\mathrm{micron}$',...
    mean(fdat.focdata(1,:)), std(fdat.focdata(1,:))));
ha(2) = annotation('textbox', [0.3 0.85 0.8 0.1], 'String', sprintf('$w_y = (%2.1f \\pm %2.1f)\\, \\mathrm{micron}$',...
    mean(fdat.focdata(2,:)), std(fdat.focdata(2,:))));
ha(3) = annotation('textbox', [0.3 0.8 0.8 0.1], 'String', sprintf('$E_{1/e^2}/E_\\mathrm{total} = (%2.0f \\pm %2.0f) \\,$ percent',...
    mean(fdat.focdata(4,:)), std(fdat.focdata(4,:))));

cb = colorbar(ax, 'Location', 'Southoutside');
set(cb, 'Position', [0.12 0.08 0.78 0.03]);
xlabel(cb, '$a_0$');
set(ax, 'Position', [0.146 0.2 0.76 0.79], 'ZTick', 0:0.5:2);
set(ha, 'EdgeColor', 'none', 'FontSize', 20);

make_latex(hfig); setAllFonts(hfig, 20);
%export_fig('20170810_BestFocus', '-pdf', '-nocrop', hfig);