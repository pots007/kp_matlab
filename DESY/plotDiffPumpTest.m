% plotDiffPumpTest.m
%
% Plot the data we took in a nice manner

if isunix
    datfol = '/media/kpoder/DATA/DESYcloud/Experiments/2018_10Hz_shooting/PumpingData/';
end

bondFiles = {'DiffCubeTest_1bar_Air.txt',...
    'DiffCubeTest_6bar_He.txt',...
    'DiffCubeTest_10bar_He.txt',...
    'DiffCubeTest_17bar_He.txt',...
    'DiffCubeTest_17bar_He_withComp.txt',...
    'DiffCubeTest_17bar_He_withComp_comp.txt',...
    'DiffCubeTest_all_backing.txt'};

% For each file, read the data into a matrix of three columns:
% datenum (of the point) | ShotID (FLASH I think) | pressure value

bondData = cell(size(bondFiles));
for f=1:length(bondFiles)
    fprintf('Opening and parsing %s\n', bondFiles{f});
    fdat =  getFileText(fullfile(datfol, bondFiles{f}));
    tempdat = zeros(length(fdat)-1,3);
    for l=2:length(fdat)
        [~,endind] = regexp(fdat{l}, '"(.*?)"');
        vals = sscanf(fdat{l}(endind+1:end), '%ld %d %f');
        tempdat(l-1,1) = datenum(fdat{l}(1:endind));
        tempdat(l-1,2) = vals(1);
        tempdat(l-1,3) = vals(3);
    end
    bondData{f} = tempdat;
end

%% Now for the plotting
% Make a plot of the different pumping tests we did. 
% For each case, first show the single shot, and then the sustained run.
hfig = figure(23612); clf(hfig);
hfig.Position = [1200 500 800 600];
ax = makeNiceAxes(hfig);

hh = [];
for k=1:length(bondData)-2
    pdat = bondData{k};
    [~,ind2] = min(diff(pdat(:,3))); % Index of end
    [~,ind1] = max(diff(pdat(1:ind2,3))); % Index of start
    logm = (ind1-5):(ind2+5);
    hh(k) = plot(pdat(logm,1)-pdat(min(logm)), pdat(logm,3), '-');
end
hleg = legend(hh, cellfun(@(x) strrep(x(14:end-4), '_', ', '),...
    bondFiles(1:end-1), 'UniformOutput', 0), 'EdgeColor', 'none');
set(hh, 'LineWidth', 2);
ylabel('Target chamber pressure / mbar');
xlabel('Time');
grid on;
datetick('x', 'MM:SS');
title('Valve open for 7ms, closed for 93 ms');
ax.XTickLabelRotation = 30;

setAllFonts(hfig, 16);
%export_fig(fullfile(datfol, 'BondPressure'), '-png', '-m2', '-nocrop', hfig);

%% Now plot the run with the main shutter opened
hfig = figure(23613); clf(hfig);
hfig.Position = [1200 500 800 600];
ax = makeNiceAxes(hfig);

hh = [];
pdat = bondData{end-2};
[~,ind2] = min(diff(pdat(:,3))); % Index of end
[~,ind1] = max(diff(pdat(1:ind2,3))); % Index of start
logm = (ind1-5):(ind2+6);
hh(1) = plot(ax, pdat(logm,1), pdat(logm,3), '-');
starttime = min(pdat(logm,1));
pdat = bondData{end-1};
indn = find(pdat(:,1)==starttime, 1);
logm = indn:(indn+length(logm));
hh(2) = plot(ax, pdat(logm,1), pdat(logm,3), '-');
set(ax, 'YScale', 'log', 'YLim', [7e-7 5e-4], 'XTickLabelRotation', 30)
grid(ax, 'on')
datetick('x', 'HH:MM:SS')
set(hh, 'LineWidth', 2);
legend(hh, {'Target chamber', 'Compressor'});
ylabel(ax, 'Pressure / mbar');
xlabel(ax, 'Time');
title(ax, '10 Hz pulsing, 17 bar, vacuum shutter open');
setAllFonts(hfig, 16);

%export_fig(fullfile(datfol, 'BothPressures'), '-png', '-m2', '-nocrop', hfig);

%% And now with the backing pressures

% Make a plot of the different pumping tests we did. 
% For each case, first show the single shot, and then the sustained run.
hfig = figure(23612); clf(hfig);
hfig.Position = [1200 500 800 600];
loc = [0.12 0.14 0.75 0.8];
ax = makeNiceAxes(hfig, loc);
ax2 = axes('Position', loc, 'YAxisLocation', 'right',...
    'Color', 'none', 'NextPlot', 'add');

hh = [];
presDat = bondData{end};
for k=1:length(bondData)-2
    pdat = bondData{k};
    [~,ind2] = min(diff(pdat(:,3))); % Index of end
    [~,ind1] = max(diff(pdat(1:ind2,3))); % Index of start
    logm = (ind1-5):(ind2+5);
    hh(k) = plot(ax, pdat(logm,1)-pdat(min(logm)), pdat(logm,3), '-');
    % And plot backing pressure for each test as well
    [~,tind1] = min(abs(presDat(:,1) - pdat(logm(1),1)));
    [~,tind2] = min(abs(presDat(:,1) - pdat(logm(end),1)));
    plot(ax2, presDat(tind1:tind2, 1)-presDat(tind1),...
        presDat(tind1:tind2,3), '-k', 'LineWidth', 1);
end
labels = cellfun(@(x) strrep(x(14:end-4), '_', ', '),...
    bondFiles(1:end-2), 'UniformOutput', 0);
labels{end} = '17bar, He, comp';
hleg = legend(hh, labels, 'EdgeColor', 'none');
set(hh, 'LineWidth', 2);
ylabel(ax, 'Target chamber pressure / mbar');
xlabel(ax, 'Time');
grid(ax, 'on'); grid(ax2, 'on');
datetick(ax, 'x', 'MM:SS');
title(ax, 'Valve open for 7ms, closed for 93 ms');
ax.XTickLabelRotation = 30;
set(ax2, 'XLim', ax.XLim, 'YLim', [1 21], 'YTick', 1:21,...
    'XTickLabel', [], 'XTick', [], 'Layer', 'top');
set(ax, 'Layer', 'top');
ylabel(ax2, 'Gas jet backing pressure / bar');

setAllFonts(hfig, 16);

%export_fig(fullfile(datfol, 'BondPressure+Backing'), '-png', '-m2', '-nocrop', hfig);
export_fig(fullfile(datfol, 'BondPressure+Backing'), '-pdf', '-nocrop', hfig);
