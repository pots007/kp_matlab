% plot_20181109_TimingStageStability.m
% 
% Plot some stuff about how stable the spot is when moved

% Load the data
load('/home/fflab28m/DATA/FocalSpot/TargetChamber/20181109/Moving_analysis');
hfig = figure(2487234); hfig.Position = [1000 500 600 900];
clf(hfig);
locs = GetFigureArray(1, 2, [0.03 0.06 0.07 0.12], 0.08, 'down');
ax1 = makeNiceAxes(hfig, locs(:,1));
ax2 = makeNiceAxes(hfig, locs(:,2));
set([ax1, ax2], 'NextPlot', 'add');
h1 = plot(ax1, focdata(13,:), 'x');
h2 = plot(ax1, focdata(14,:), 'o');
legend([h1, h2], {'x', 'y'}, 'Location', 'SE');
%plot(ax2, focdata(13,:), focdata(14, :), '-');
scatter(ax2, focdata(13,:),focdata(14,:), 40, 1:1001, 'filled');
cb = colorbar(ax2, 'Location', 'northoutside');

% Make it nice:
xlabel(ax1, 'Shot number');
ylabel(ax1, 'Spot position / micron');
xlabel(ax2, 'x / micron');
ylabel(ax2, 'y / micron');
%ylabel(cb, 'Shot number');
xlim(ax1, [0, 1000]);
axis(ax2, 'equal');

setAllFonts(hfig, 16);
make_latex(hfig);
export_fig('/home/fflab28m/DATA/FocalSpot/TargetChamber/20181109/Moving_analysis',...
    '-png', '-m4', '-nocrop', hfig)