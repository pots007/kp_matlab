% compareOldNewMPA2_pointing.m
%
% Script to compare the pointing fluctuations with old and new single shot
% shutters.

datfol = 'C:\Users\kpoder\Work\MPA2_FF\';
fols = {'20170905_NewShutter', '20170905_OldShutter'};
centroids = [];
for f = 1:2
    flist = dir(fullfile(datfol, fols{f}, '*.pgm'));
    for ff = 1:length(flist)
        imm = ReadRAWpgm(fullfile(datfol, fols{f}, flist(ff).name));
        centroids(end+1, :) = centroidlocationsmatrix(imm);
    end
end
%%
hfig = figure(43213); clf(hfig);
[NxN, xN] = hist(centroids(1:300, 1), 15);
[NyN, yN] = hist(centroids(1:300, 2), 15);
[NxO, xO] = hist(centroids(301:end, 1), 15);
[NyO, yO] = hist(centroids(301:end, 2), 15);

ax1 = subplot(2,2, 1:2); ax1.NextPlot = 'add';
hhh = plot(1:600, centroids(:,1), 'k', 1:600, centroids(:,2), 'r');
YL = ax1.YLim;
ha = area([301 600], [YL(1) YL(2)-YL(1); YL(1) YL(2)-YL(1)]);
set(ha(1), 'EdgeColor', 'none', 'FaceColor', 'none');
set(ha(2), 'EdgeColor', 'none', 'FaceColor', 0.65*[1 1 1]);
uistack(ha, 'bottom');
ax1.YLim = YL;
text(315, 310, 'OLD SHUTTER', 'HorizontalAlignment', 'left');
text(285, 315, 'NEW SHUTTER', 'HorizontalAlignment', 'right');
text(520, 335, 'y-axis', 'Color', 'r');
text(520, 310, 'x-axis', 'Color', 'k');
title('Centroid locations');
xlabel('Shot'); ylabel('Pixel');
ax2 = subplot(2,2,3); ax2.NextPlot = 'add';
hh(1) = stairs(xN, NxN, 'k');
hh(2) = stairs(xO, NxO, 'r');
legend(hh(1:2), {'New', 'Old'}, 'box', 'off');
title('x-axis'); xlabel('Pixel'); ylabel('N');
ax3 = subplot(2,2,4); ax3.NextPlot = 'add';
hh(3) = stairs(yN, NyN, 'k');
hh(4) = stairs(yO, NyO, 'r');
legend(hh(3:4), {'New', 'Old'}, 'box', 'off');
title('y-axis'); xlabel('Pixel'); ylabel('N');
% Make things look nice.
set([ax1 ax2 ax3], 'Box', 'on', 'LineWidth', 2, 'Layer', 'top');
set(hh, 'LineWidth', 2);

%% And saving
export_fig(fullfile(datfol, 'ShutterComparisons'), '-jpg', '-nocrop', hfig);