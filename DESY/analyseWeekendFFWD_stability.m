% analyseWeekendFFWD_stability.m
%
% Quick script to look at the stability of the beamline over the weekend

% Get data
if getComputerName == 'flalap01'
    datfol = 'C:\Users\kpoder\Work\FFWD_beamline\LASER_INTERCOUPLING_1';
else
    datfol = '/home/fflab28m/DATA/FFWD_beamline/LASER_INTERCOUPLING_1';
end

% Make analysis folder
if ~exist(fullfile(datfol, 'imanal'), 'dir')
    mkdir(fullfile(datfol, 'imanal'));
end

flist = dir(fullfile(datfol, '*.png'));
nfiles = length(flist);

hfig = figure(768967); clf(hfig);
hfig.Position = [2100 100 320 240];
axx = axes('Parent', hfig, 'Position', [0.02 0.02 0.96 0.96], 'Nextplot', 'add',...
    'Box', 'on', 'XLim', [0 1288], 'YLim', [0 963], 'Layer', 'top', 'XTick', [], 'YTick', []);
colormap(hfig, jet_white(256));

tic;
lasttime = toc;
cent_locs = zeros(nfiles, 2);
for f = 1:nfiles
    cla(axx)
    imm = medfilt2(imread(fullfile(datfol, flist(f).name)));
    imagesc(imm, 'Parent', axx);
    cent_locs(f,:) = centroidlocationsmatrix(imm);
    plot(cent_locs(f,1), cent_locs(f,2), 'xr', 'LineWidth', 3, 'MarkerSize', 15);
    drawnow;
    export_fig(fullfile(datfol, 'imanal', sprintf('imm%04i', f)), '-png', '-nocrop', hfig);
    fprintf('Analysed file %4i out of %4i, expected time left %2.2f s\n',...
        f, nfiles, (nfiles-f)*(toc - lasttime));
    lasttime = toc;
end

%save(fullfile(datfol, 'cent_location'), 'cent_locs');

%% Now the plotting
% Get the first time:
dateraw = flist(1).name(24:33);
fnames = {flist.name}';
shot_times = cellfun(@(x) datenum(datetime(x(24:33), 'InputFormat', 'yyMMddHHmm')), fnames, 'UniformOutput', 1);
hfig = figure(8979); clf(hfig);
ax = gca;
set(ax, 'NextPlot', 'add', 'Box', 'on', 'Layer', 'top');
hh1 = plot(ax, shot_times, cent_locs, '-');
hh2 = plot(ax, shot_times, movmean(cent_locs, 75), '-', 'LineWidth', 2);
hleg = legend(hh1, {'x-axis', 'y-axis'}, 'Orientation', 'horizontal');

% Make things nicer...
hh1(1).Color = 'k';
hh2(1).Color = hh1(1).Color; hh2(2).Color = hh1(2).Color;
datetick('x', 'ddd,HH:MM');
xlabel('Time');
ylabel('Centroid location / pixel');
axis tight

% And plot a zoom in!
ax2 = axes('Parent', hfig, 'Position', [0.2 0.5 0.1 0.2], 'NextPlot', 'add', 'Box', 'on');
plot(ax2, shot_times(1:75), cent_locs(1:75,1), '-k');
datetick('x', 'HH:MM');
set(ax2, 'XTick', shot_times(1:33:75), 'XLim', shot_times([1 75]))
setAllFonts(hfig, 14);
%export_fig(fullfile(datfol, 'centroid_location'), '-pdf', '-nocrop', hfig);
