% plot20180206_FocusScan.m
%
% Plot the array of images taken to see what is happening

if ismac
    
else
    datafol = '/media/kpoder/DATA/Focus/20180206';
end
flist = dir(fullfile(datafol, '*.raw'));

im = ReadRAW16bit(fullfile(datafol, flist(1).name), 1280, 960, [], 'DESY');
ims = zeros([size(im) length(flist)], 'uint16');
for k=1:length(flist)
    ims(:,:,k) = uint16(ReadRAW16bit(fullfile(datafol, flist(k).name),...
        1280, 960, [], 'DESY'));
end

% A wee bit of cropping...
ims = ims(1:600, 200:end,:);
saveAgain = 1;
if saveAgain
    for k=1:size(ims,3)
        imwrite(ims(:,:,k),fullfile(datafol, 'png', sprintf('image%04i.png',k)));
    end
end
