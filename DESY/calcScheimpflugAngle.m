% calcScheimpflugAngle.m
%
% Calculate Scheimpflug angle for the mirror imaging angle

u = 350;
f = 100;
v = u*f/(u-f);

uv = u/v;

tantheta = 0.5*(sqrt(1 + 6*uv + uv^2) - (1 + uv));

theta = atand(tantheta);

phi = 45 - theta;

fprintf('Magnification of %2.2f\n', 1/uv);