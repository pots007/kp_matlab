% rename2ServerStyle.m
%
% Will rename all files in a DOOCS-s saved folder to IC server format, 
% Will try to rename files with ext. If not given, will try .pgm, .png and
% .raw files.

function rename2ServerStyle(folder, ext)
ext_def = {'pgm', 'png', 'raw'};
haveFiles = false;
if nargin==1
    for i=1:length(ext_def)
        flist = dir(fullfile(folder, ['*.' ext_def{i}]));
        if ~isempty(flist)
            haveFiles = true;
            break
        end
    end
else
    flist = dir(fullfile(folder, ['*.' ext]));
    if ~isempty(flist); haveFiles = true; end
end

if haveFiles
    for f=1:length(flist)
        [~, fname, fext] = fileparts(flist(f).name);
        timestamp = regexp(fname, '\d{8}T\d{6}');
        if ~isempty(timestamp)
            date_ = fname(timestamp:timestamp+7);
            newfname = sprintf('%sr001s%03i_%s%s',...
                date_, f, fname(1:timestamp-2), fext);
            fprintf('Renaming %s to %s\n', flist(f).name, newfname)
            movefile(fullfile(folder, flist(f).name), ...
                fullfile(folder, newfname))
        end
    end
else
    if nargin==1
        fprintf('No files in folder %s with extensions:\n', folder)
        disp(ext_def)
    else
        fprintf('No files in folder %s with extension %s\n', folder, ext)
    end
end

