% iota1_cludge.m
%
% Will open a port to the driver, and then continously push the same values
% to it.

function iota1_cludge

IP = '192.168.145.198';
port = 4001;

onTime = 7;
offTime = 0;
onUnit = 1; % 0=us, 1=ms, 2=s, 3=min
offUnit = 1; %0=ms, 1=s, 2=min, 3=Hz
mode = 2; %0 - internal one shot, 1 - internal cycle, 2 - external cycle

fprintf('You may need to run instrreset after you ctrl+C me...\n')
%params = [onTime, offTime, onUnit, offUnit, mode];
msg = ['{AF' sprintf('%04i', onTime), num2str(onUnit),...
    sprintf('%04i', offTime), num2str(offUnit), num2str(mode) '}'];
% This is the infamous checksum character...
msg = [msg char(mod(sum(uint8(msg) - 32), 95)+32)];

con = tcpip(IP, port);
con.BytesAvailableFcn = @(s,e) getPort(con);
fopen(con);
while 1
    %disp(msg)
    fprintf(con, msg);
    pause(0.1);
end

fclose(con);
delete(con)

end

function getPort(con)
    disp(['\t' fread(con, con.BytesAvailable)]);
end

