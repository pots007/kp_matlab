% plotSequoiaScans.m
%
% Plot some recent scans from the Sequoia

if ismac
    rootfol = '/home/fflab28m/DATA/Sequoia';
else
    rootfol = '/home/kpoder/ownCloud/Experiments/LaserContrast';
end

flist = dir(fullfile(rootfol, '*Vacuum*.dat'));
dat = {};
for k=1:length(flist)
    dat{k} = importdata(fullfile(rootfol, flist(k).name));
end

legs = {'2017 06 07, regen optimised', '2017 06 07', '2018 01 16',...
    '2018 05 17', '2018 05 18', '2018 05 18, optimised', '2018 05 25, 1','2018 05 25, 2'};

hfig = figure(32413); clf(hfig);
hfig.Position = [800 100 800 900];
locs = GetFigureArray(1, 2, [0.02 0.02 0.09 0.13], 0.08, 'down');
cols = brewermap(9, 'Set1');
% ------------- First axes - overview
ax1 = makeNiceAxes(hfig, locs(:,1));
hh = [];
for k=1:length(flist)
    [~,logm] = sort(dat{k}(:,1));
    hh(k) = plot(ax1, dat{k}(logm,1), dat{k}(logm,2), '-', 'Color', cols(k,:));
end
legend(hh, legs, 'Location', 'northwest', 'EdgeColor', 'none');
set(ax1, 'YScale', 'log', 'MinorGridLineStyle', 'none', 'XLim', [-35 14]*1e-11);
ylabel(ax1, 'Intensity / norm.');
xlabel(ax1, 'Delay / s');
grid(ax1, 'on');

% -------------- Second axes - zoom near peak
ax2 = copyobj(ax1, hfig);
set(ax2, 'Position', locs(:,2), 'XLim', [-5 5]*1e-11);
set(findobj(ax2, 'Type', 'line'), 'LineWidth', 1.5);
set([ax1 ax2], 'YLim', [1e-11 1.1], 'YTick', 10.^[-11:0]);

setAllFonts(hfig, 16);
%export_fig(fullfile(rootfol, 'Scans_20180517'), '-pdf', '-nocrop', hfig);
%export_fig(fullfile(rootfol, 'Scans_20180525'), '-png', '-m4', '-nocrop', hfig);