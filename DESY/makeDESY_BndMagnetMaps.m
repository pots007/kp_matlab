% makeDESY_BondMagnetMaps.m
%
% Make a nice high res magnet map from the measurement points from the guys
% here at DESY.
%
% KP, Nov 2017

if strcmp(getComputerName, 'flalap01-ubuntu')
    datpath = '/media/kristjan/KRISMOOSE6/28mReferences/FieldMeasurement';
else 
    datpath = '/home/fflab28m/Kris/References/FieldMeasurement';
end
I_fols = {'BO_0A', 'BO_180A', 'BO_250A', 'BO_311A'};

I_id = 4; % 1 for 0, 2 for 180A, 3 for 250A, 4 for 311A.

fpath = fullfile(datpath, I_fols{I_id});
fname = dir(fullfile(fpath, 'bo2_ypm0*.dat'));
datf = importdata(fullfile(fpath, fname.name));

hfig = figure(39847947); clf(hfig);
scatter(datf(:,3), datf(:,1), 10, datf(:,5));
colorbar;
% Now set up all axes:
z_old = datf(:,3);
y_old = datf(:,1);
z_new = min(z_old):max(z_old);
y_new = min(y_old):max(y_old);
[Z,Y] = meshgrid(z_new, y_new);
map_new = griddata(z_old, y_old, datf(:,5), Z, Y);
imagesc(z_new, y_new, map_new);
axis image; colorbar;
[~, fnameroot, ~] = fileparts(fname.name);
save(fullfile(datpath, I_fols{I_id}), 'z_new', 'y_new', 'map_new');
% Also make it into a field suitable for MagnetTracker
B.z0 = z_new;
B.y0 = y_new;
B.B = map_new;
B.z = z_new + 1000;
B.y = y_new;
load('Gemini2015Settings.mat');
dat.B1 = B;
dat.m1map = 99;
save(fullfile(fileparts(datpath), 'DESY_Espec3_Settings.mat'), 'dat');

%% The script below makes settings with different magnet maps
% This is based on the correct screen positions found by moving the z
% coordinate around. Thus we only replace the B1 component of the maps.

if strcmp(getComputerName, 'flalap01-ubuntu')
    datpath = '/media/kpoder/KRISMOOSE6/28mReferences/FieldMeasurement';
else 
    datpath = '/home/fflab28m/Kris/References/FieldMeasurement';
end
I_fols = {'BO_0A', 'BO_180A', 'BO_250A', 'BO_311A'};

for I_id = 1:4 % 1 for 0, 2 for 180A, 3 for 250A, 4 for 311A.

    fpath = fullfile(datpath, I_fols{I_id});
    fname = dir(fullfile(fpath, 'bo2_ypm0*.dat'));
    datf = importdata(fullfile(fpath, fname.name));

    hfig = figure(39847947); clf(hfig);
    scatter(datf(:,3), datf(:,1), 10, datf(:,5));
    colorbar;
    % Now set up all axes:
    z_old = datf(:,3);
    y_old = datf(:,1);
    z_new = min(z_old):max(z_old);
    y_new = min(y_old):max(y_old);
    [Z,Y] = meshgrid(z_new, y_new);
    map_new = griddata(z_old, y_old, datf(:,5), Z, Y);
    imagesc(z_new, y_new, map_new);
    axis image; colorbar;
    %[~, fnameroot, ~] = fileparts(fname.name);
    %save(fullfile(datpath, I_fols{I_id}), 'z_new', 'y_new', 'map_new');
    % Also make it into a field suitable for MagnetTracker
    B.z0 = z_new;
    B.y0 = y_new;
    B.B = -map_new;
    B.z = z_new + 1000;
    B.y = y_new;
    load('DESY2017_Espec3_Settings.mat');dat.B1 = B;
    dat.m1map = 90 + I_id ;
    save(fullfile('/home/kpoder/matlab', 'DESY', 'tracking',...
        ['DESY2017_Espec3_' I_fols{I_id} '_Settings.mat']), 'dat');
    drawnow;
end





