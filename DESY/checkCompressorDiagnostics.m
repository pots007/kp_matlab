% checkCompressorDiagnostics.m
%
% Quick check of where and how and why for (new) compressor diagnostics

f1 = 0.3; % Focal length of focussing lens
f2 = 0.03; % Focal length of collimating lens
d1 = 1.5; % Distance from last mirror to first lens
d2 = f1+f2+0.00; % Distance between the lenses
d3 = 0.03; % Distance after the second lens

Mprop = @(d) [1, d; 0 1];
Mlens = @(f) [1, 0; -1/f 1];

% Initial beam positions. yoff is offset from axis.
yoff = 0;
ylocs = yoff + linspace(-0.02, 0.02, 10);
thetas = zeros(size(ylocs));
thetas = ylocs./d1+0.003;
ylocs = zeros(size(ylocs));

rays = cell(size(ylocs));
for k=1:length(ylocs)
    rays{k} = [ylocs(k); thetas(k)];
    rays{k}(:,end+1) = Mprop(d1) * rays{k}; % First propagation
    rays{k}(:,end+1) = Mlens(f1) * rays{k}(:,end); % First lens
    rays{k}(:,end+1) = Mprop(d2) * rays{k}(:,end); % between lenses
    rays{k}(:,end+1) = Mlens(f2) * rays{k}(:,end); % Second lens
    rays{k}(:,end+1) = Mprop(d3) * rays{k}(:,end); % To final plane
end
xs = [0, d1, d1, d1+d2, d1+d2, d1+d2+d3];

hfig = figure(46281); clf(hfig);
ax = makeNiceAxes(hfig);
for k=1:length(ylocs)
    plot(ax, xs, rays{k}(1,:), '.-k');
end

