% makePlotofTheWeek_SIresults.m
%
% This is to make a few plots of the week for 2017/11/20.
% Actually make some use of the new classes!!!!!!!!!!!
%

%% Firstly, monoenergetic results.

drs = [20171109, 5, 3;...
    %20171107, 2, 7;...
    %20171107, 2, 25;...
    20171109, 4, 42;...
    20171115, 3, 76];
    
calFile2 = '/media/kpoder/KRISMOOSE6/28mReferences/Espec3ref_fit_20171203CalibData.mat';
specs = cell(3,2);
for i=1:3
    espec = Analysis.EspecDiagnostic(drs(i,1), drs(i,2), 'ElectronSpectrometer3_dewarp', calFile2);
    specs{i,2} = espec.getElectronSpectrum(drs(i,3), 3, 2);
    specs{i,1} = espec.energyAxis.screen;
end
%%
hfig = figure(20170); clf(hfig);
set(hfig, 'Position', [1200 300 600 500]);
ax = makeNiceAxes(hfig);

clear hh;
for k=1:size(drs,1)
    specc = smooth(specs{k,2}/max(specs{k,2}));
    hh(k) = plot(ax, specs{k,1}, specc);
    lm = specs{k,1} > 70 & specs{k,1} < 200;
    specc(~lm) = 0;
    ind1 = find(specc > 0.5, 1, 'first');
    ind2 = find(specc > 0.5, 1, 'last');
    fwhm = specs{k,1}(ind2) - specs{k,1}(ind1);
    [~,peakk] = max(specc);
    fprintf('FWHM = %2.1f MeV == %2.1f percent\n', fwhm, fwhm/specs{k,1}(peakk)*100)
    text(specs{k,1}(ind2)+5, 0.4, sprintf('%2.1f MeV = %2.1f %%',...
        fwhm, fwhm/specs{k,1}(peakk)*100), 'Rotation', 90, 'Color', hh(k).Color);
    set(hh, 'LineWidth', 2);
    set(ax, 'XLim', [65, 200], 'Ylim', [-0.02 1.12], 'LineWidth', 2)
    drawnow;
    [x1,y1] = ds2nfu(ax, specs{k,1}(ind1), 0.5); 
    [x2,y2] = ds2nfu(ax, specs{k,1}(ind2), 0.5);    
    ha(k) = annotation('doublearrow', [x1 x2], [y1 y2], 'Color', hh(k).Color);
    drawnow;
end
set(ha, 'Head1Width', 7, 'Head2Width', 7, 'Head1Length', 5, 'Head2Length', 5,...
    'LineWidth', 1.5);

xlabel('Energy / MeV');
ylabel('dQ/(dE/E) / arb. unit');
setAllFonts(hfig, 16); make_latex(hfig);
%export_fig('/media/kpoder/KRISMOOSE6/28manalysis/SIruns_monoEns3', '-pdf', '-nocrop', hfig)

%% Then the injection at low laser energies.
hfig = figure(20171); clf(hfig);
set(hfig, 'Position', [1200 300 600 500]);
ax = gca; ax.NextPlot = 'add'; ax.Box = 'on';
ylabel(ax, 'Charge / pC'); xlabel(ax, 'Fraction of full energy');
date_ = 20171109;
run_ = 5;
hh = [];
for run_=5:6
    damon = Analysis.Diagnostic(date_, run_, 'DAMON_CHARGE');
    % Set the crop region
    damon.setCropRegion([500 200]);
    % Set the analysis function - maximum value of column 2!
    damon.setAnalysisFunction(@(x) getDamonCharge20gb(max(x(:,2))));
    % And get all the data
    damon_data = damon.getAllAnalysed;
    
    % And now the MP2 attenuator motor
    MP2_att = Analysis.MPA2AttenuatorDiagnostic(date_, run_);
    mp2_data = MP2_att.getAllAnalysed;
    
    % Now, the DAQ fucks up for some shots. Find the indices of overlap between
    % shot numbers:
    [commonShots, indA, indB] = intersect(damon.shotNumbers, MP2_att.shotNumbers);
    mp2_en = mp2_data(indB); damon_Q = damon_data(indA);
    %hh(end+1) = plot(mp2_en, damon_Q, 'sb');
    scanPos = unique(mp2_en); 
    scanVals = zeros(length(scanPos),2);
    for k=1:length(scanPos)
        logm = mp2_en == scanPos(k);
        scanVals(k,1) = mean(damon_Q(logm));
        scanVals(k,2) = std(damon_Q(logm));
    end
    hErr(run_-4) = errorbar(scanPos, scanVals(:,1), scanVals(:,2), 'd'); 
end

set(ax, 'XLim', [0.5 1.05], 'YLim', [-5 120], 'LineWidth', 2);
set(hErr, 'LineWidth', 1.5, 'MarkerSize', 10);
legend(hErr, {'20 bar', '25 bar'}, 'Location', 'Northwest');
title('Self-injected charge for varying laser energy');
setAllFonts(hfig, 16); make_latex(hfig);
%export_fig('/media/kpoder/KRISMOOSE6/28manalysis/20171109_EnergyScan', '-pdf', '-nocrop', hfig)
%% Plot the first downramp results: run 20171114r001
damon = Analysis.Diagnostic(20171114, 1, 'DAMON_CHARGE');
damon.setAnalysisFunction(@(x) getDamonCharge20gb(max(x(:,2))));
% And get all the data
damon_data = damon.getAllAnalysed;
hfig = figure(20172); clf(hfig);
ax = gca; set(ax, 'NextPlot', 'add');
set(hfig, 'Position', [1200 300 600 500]);
% First plot the highlights...
ha(1) = area([0 14], [30 30], 'FaceColor', 0.9*[1 1 1]);
ha(2) = area([15 27], [30 30], 'FaceColor', 'g');
ha(3) = area([28 32], [30 30], 'FaceColor', [0.85 0.33 0.1]);
plot(damon.shotNumbers, damon_data, '--ok', 'LineWidth', 1.5, 'MarkerSize', 10);

ht(1) = text(7, 10, 'Blade in wrong position');
ht(2) = text(21, 10, 'Blade position moved');
ht(3) = text(30, 10, 'Blade moved OUT');
set(ht, 'Rotation', 90);
set(ha, 'EdgeColor', 'none');
set(ax, 'Box', 'on', 'LineWidth', 2, 'Layer', 'top', 'XLim', [0 70], 'YLim', [0 25]);
xlabel(ax, 'Shot number');
ylabel('Beam charge / pC');
title('First shots with blade: 11 bar H$_2$');
setAllFonts(hfig, 16); make_latex(hfig);
export_fig('/media/kpoder/KRISMOOSE6/28manalysis/20171109_FirstBladeShots', '-pdf', '-nocrop', hfig)

%% Best II beams: 11bar, 2% Argon
eprof = Analysis.Diagnostic(20171110, 5, 'TargetChamberProfile');
hfig = figure(87586); clf(hfig); hfig.Position = [200 450 1300 500];
locs = GetFigureArray(10, 4, [0.02 0.02 0.02 0.05], [0.02,0.04], 'across');
% Make annotations for SI and II
ax0 = axes('Parent', hfig, 'Position', [0.01 0.01 0.98 0.98], 'Color', 'none',...
    'XLim', [0 1], 'YLim', [0 1]);
colzz = brewermap(8, 'Set3');
rectangle(ax0, 'Position', [0 0.5 1 0.5], 'EdgeColor','none', 'FaceColor', colzz(1,:));
rectangle(ax0, 'Position', [0 0 1 0.5], 'EdgeColor','none', 'FaceColor', colzz(4,:));
ht(1) = text(0.01, 0.55, 'Ionisation injection', 'Rotation', 90);
ht(2) = text(0.01, 0.15, 'Self-injection', 'Rotation', 90);
set(ht, 'FontWeight', 'bold');
axis(ax0, 'off');

clear axx;
for k=1:20
    rawim = double(eprof.getRAWdiag(eprof.shotNumbers(k)));
    rawim = rawim(200:end, 200:1100);
    rawim = medfilt2(rawim);
    bg = mean(mean(rawim(700:end,:)));
    filtim = imfilter(rawim-bg, ones(5)/25);
    axx(k) = axes('Parent', hfig, 'Position', locs(:,k));
    imagesc(filtim, 'Parent', axx(k));
    set(axx(k), 'CLim', [0 max(filtim(:))+0.01]);
    drawnow;
end
colormap(brewermap(256, 'PuBuGn'));

% And now to compare with a self-injected beam...

eprof2 = Analysis.Diagnostic(20171201, 1, 'TargetChamberProfile');
%figure(670);
for k=1:20%eprof2.nShots
    rawim = double(eprof2.getRAWdiag(eprof2.shotNumbers(k)));
    filtim = imfilter(rawim, ones(5)/25);
    axx(k+20) = axes('Parent', hfig, 'Position', locs(:,k+20));
    imagesc(filtim, 'Parent', axx(k+20));
    drawnow;
end

set(axx, 'XTick', [], 'YTick', [], 'Layer', 'top', 'LineWidth', 2);
setAllFonts(hfig, 14);
export_fig('/media/kpoder/KRISMOOSE6/28manalysis/II_SI_ProfileComp', '-pdf', '-nocrop', hfig)

%%

eprof3 = Analysis.Diagnostic(20171114, 1, 'TargetChamberProfile');
figure(670);
for k=1:eprof3.nShots
    rawim = double(eprof3.getRAWdiag(eprof3.shotNumbers(k)));
    imagesc(rawim);
    title(eprof3.shotNumbers(k)); drawnow; pause(0.5);
    %filtim = imfilter(rawim, ones(5)/25);
    %axx(k+20) = axes('Parent', hfig, 'Position', locs(:,k+20));
    %imagesc(filtim, 'Parent', axx(k+20));
    drawnow;
end

set(axx, 'XTick', [], 'YTick', []);