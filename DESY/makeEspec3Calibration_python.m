% makeEspec3Calibration_python.m
%
% Make a calibration structure and save it, ready to use with the new
% python espec monitor tool. As Version 1, it omits all warping effects.

%fset.rotation = -0.95;
%fset.roi = [185, 595, 1650, 170];
fset.rotation = 180-0.95;
fset.roi = [125, 485, 1650, 170];
along_screen_fit = polyfit([fset.roi(1),fset.roi(1)+fset.roi(3)], [0, 255],  1);

dfol = '/media/kpoder/DATA/Espec/References/20190130';
im = imread(fullfile(dfol, 'ElectronSpectrometer3_20190130T100542_991_24266001.pgm'));
fset.imagesize = size(im);
im = imrotate(im, fset.rotation);
imagesc(im)

rectangle('Position', fset.roi, 'EdgeColor', 'r');
set(gca, 'XLim', [90, 1900], 'YLim', [450, 700], 'CLim', [0 100])

fset.alongscreen = polyval(along_screen_fit, fset.roi(1)+(1:fset.roi(3)));

fname = '/home/kpoder/python/ffwd_python/doocs/espec/Bond_Espec3_20190424.mat';
save(fname, '-struct', 'fset');

