% compareLaserBondLabFrog.m
%
% Some analysis to try to figure out wtf is happening to the pulse between
% the laser lab and the bond lab. This is the data taken on 20171214.

if isunix
    datfol = '/media/kpoder/DATA/grenouille/GrenouilleImages/20171214';
end
datfolss = {'Bondlab', 'Laserlab2', 'Laserlab_+1500GDD2'};

rets = cell(3,1);
for f=1:3
    Bondfol = fullfile(datfol, datfolss{f}, 'FROGed', 'ImgGridPass_002');
    Bond = struct;
    flist = dir(fullfile(Bondfol, '*.mat'));
    npnts = 256;
    wlims = [1.97 2.77];
    wgrid = linspace(wlims(1), wlims(2), npnts);
    dt = pi/mean(diff(wgrid));
    tgrid = linspace(-dt, dt, npnts);
    tgrid = tgrid+0.5*mean(diff(tgrid)); % Offset it a wee bit....
    r.ws = nan(length(flist), npnts);
    r.ts = nan(size(r.ws));
    
    for i=1:length(flist)
        try
            fdat = load(fullfile(Bondfol, flist(i).name), 'matStruct');
            rr = fdat.matStruct.retrievals;
            r.ws(i,:) = interp1(rr.w, rr.E_w, wgrid);
            r.ts(i,:) = interp1(rr.t, rr.E_t, tgrid);
        catch
        end
    end
    r.t_mean = mean(abs(r.ts).^2, 'omitnan');
    r.t_std = std(abs(r.ts).^2, 'omitnan');
    r.w_mean = mean(abs(r.ws).^2, 'omitnan');
    normf = max(r.w_mean);
    r.w_mean = r.w_mean/normf;
    r.w_std = std(abs(r.ws).^2, 'omitnan')/normf;
    rets{f} = r;
end
%% Plot all average spectra and temporal traces...
hfig = figure(42413); clf(hfig); hfig.Position = [750 450 1100 450];
locs = GetFigureArray(2,1, [0.02 0.1 0.1 0.1], [0.03 0.03], 'across');
clear axx;
axx(1) = axes('Parent', hfig, 'Position', locs(:,1), 'NextPlot', 'add');
axx(2) = axes('Parent', hfig, 'Position', locs(:,2), 'NextPlot', 'add', 'YAxisLocation', 'right');
colzz = brewermap(9, 'Set1');
legh = [];
for k=1:3
    axes(axx(1));
    ht = shadedErrorBar(tgrid, rets{k}.t_mean, rets{k}.t_std);
    axes(axx(2));
    hw = shadedErrorBar(wgrid, rets{k}.w_mean, rets{k}.w_std);
    % Make them noice
    set([ht.edge hw.edge], 'LineStyle', 'none');
    set([ht.mainLine hw.mainLine], 'LineWidth', 2, 'Color', colzz(k,:));
    set([ht.patch hw.patch], 'FaceColor', colzz(k,:), 'FaceAlpha', 0.2);
    legh(k) = ht.mainLine;
end
legend(legh, {'Bond lab', 'Laser lab', 'Laser lab, +1500GDD'});
set(axx, 'Box', 'on', 'LineWidth', 2);
set(axx(1), 'XLim', 200*[-1 1]); xlabel(axx(1), 'Time / fs'); ylabel(axx(1), 'Intensity');
set(axx(2), 'XLim', [2.15 2.5]); xlabel(axx(2), 'Frequency / red/fs'); ylabel(axx(2), 'Spectrum');
%saveas(hfig, fullfile(datfol, 'Average_traces1'), 'svg');
% Conclusion from the above, ie from the figure: central frequeny knob was
% indeed moved. Need to change fCal to get it back....

%% Let's check the spectral phase of all shots.
hfig = figure(42413); clf(hfig); hfig.Position = [750 450 1100 450];
locs = GetFigureArray(3, 1, [0.02 0.1 0.1 0.1], [0.03 0.03], 'across');
clear axx;
for i=1%:3
    axx(i) = makeNiceAxes(hfig, locs(:,i));
    t = rets{i};
    %ht = shadedErrorBar(tgrid, rets{i}.t_mean, rets{i}.t_std);
    % Pick the first phase as the 'reference'
    fixPhase = unwrap(angle(t.ws(1,:)));
    [~,ind0] = min(abs(wgrid-omega0*1e-15));
    fixPhase = fixPhase - fixPhase(ind0);
    lm = t.w_mean>0.1*max(t.w_mean);
    phh = [];
    tsnew = [];
    for k=1:10%size(t.ws,1)
        locPhase = unwrap(angle(t.ws(k,:)));
        locPhase = locPhase - locPhase(ind0);
        % Not the greatest method:
        phaseDiff = gradient(locPhase);
        % phaseSign = sign(phaseDiff(ind0));
        % So try comparing to a fixed phase, picked randomly.
        A = sum(abs(fixPhase(lm) - locPhase(lm)));
        B = sum(abs(fixPhase(lm) + locPhase(lm)));
        phaseSign = (-1)^(A>B);
        % Not the best either! 
        %plot(wgrid(lm), locPhase(lm)*phaseSign, '--k');
        gg = abs(t.ts(k,:)).^2;
        %gg(gg>0.3) = 0.3;
        %
        % Try a third way:
        [~,maxind] = max(abs(t.ts(k,:)).^2);
        [~, indd] = max(abs(gradient(abs(t.ts(k,:)).^2)));
        phaseSign = (1)^(indd<maxind);
        fprintf('maxind: %i, indd %i phaseSign %i\n', maxind, indd, phaseSign);
        plot(tgrid*phaseSign, (abs(t.ts(k,:)).^2), '--')
        %locPhase(~lm) = nan;
        phh(end+1,:) = locPhase(lm)*phaseSign;
        % Deal with temporal now
        
        if phaseSign>0
            tsnew(end+1,:) = fliplr(t.ts(k,:)); 
        else
            tsnew(end+1,:) = t.ts(k,:); 
        end
    end
    continue;
    meanPhase = mean(phh, 'omitnan');
    plot(axx(i), wgrid(lm), meanPhase, '-c', 'Linewidth', 1.5);
    % Go over and highlight the outliers...
    hh = findobj(axx(i), 'Type', 'line', 'Color', 'k');
    for k=1:length(hh)
        yy = hh(k).YData;
        if sum(abs(yy - meanPhase))>30; hh(k).Color = 'r'; end
    end
    gg = mean(cell2mat(get(findobj(axx(i), 'Type', 'line', 'Color', 'k'),...
        'YData')), 'omitnan');
    plot(wgrid(lm), gg, '-m', 'LineWidth', 2);
end