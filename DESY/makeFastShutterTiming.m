% makeFastShutterTiming.m
%
% Makes a plot describing the timings we want for the fast shutter

% Make the timings
t = 0:200; % in milliseconds, repeats in 200ms units
trig = zeros(size(t)); trig(60:65) = 1;
laser = zeros(size(t)); laser(100) = 1;
coiltrig = zeros(size(t)); 
coiltrig([60:90, 110:140]) = 1;
coilI = zeros(size(t)); 
coilI(60:90) = 1; coilI(110:140) = -1;
signals = [trig; laser; coiltrig; coilI];
labels = {'Trigger', 'Laser pulse', 'Coil trigger', 'Coil current'};

% Plot them 
hfig = figure(34134); clf(hfig);
hfig.Position = [1000 400 800 400];
locs = GetFigureArray(1, 4, [0.01 0.01 0.1 0.06], 0.04, 'down');

axx = [];
for i=1:4
    axx(i) = axes('Parent', hfig, 'Position', locs(:,i));
    plot(axx(i), t, signals(i,:), '-k', 'LineWidth', 2);
    ylabel(axx(i), labels{i});
    %axis(axx(i), 'off');
end

set(axx, 'XColor', 'k', 'XTick', [0:10:200], 'YTick', []);
set(axx(1:end-1), 'XTickLabel', []);
xlabel(axx(end), 'Time (ms)');

export_fig('/home/kpoder/DESYcloud/Hardware/2018_FastShutter/TimingDiagram',...
    '-pdf', '-nocrop', hfig);