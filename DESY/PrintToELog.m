classdef PrintToELog
    %PRINTTOELOG Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods(Static)
        
        function printToFFAmplitudeLaserLog( fig_handle, texts)
            % texts are  texts{1} = 'author'; texts{2} = 'title'; texts{3} = 'body';
            PrintToELog.printToLabLog( fig_handle, texts, 'ffamplaslog');
        end
        
        function printToFFBondLabLog( fig_handle, texts)
            % texts are  texts{1} = 'author'; texts{2} = 'title'; texts{3} = 'body';
            PrintToELog.printToLabLog( fig_handle, texts, 'fflaslablog');
        end
        
        function printToFlashLog( fig_handle, texts)
            % texts are  texts{1} = 'author'; texts{2} = 'title'; texts{3} = 'body';
            PrintToELog.printToLabLog( fig_handle, texts, 'ttflog');
        end
        
        function printToLabLog( fig_handle, texts, printer )
            %copied from my_printtottflog from ttf2 user space
            %   Detailed explanation goes here

            if nargin<2
                texts{1} = 'author';
                texts{2} = 'title';
                texts{3} = 'body';
            end

            author = texts{1};
            title  = texts{2};
            body   = texts{3};

            fileName = tempname;
            set(fig_handle,'PaperPositionMode','auto')
            print(fig_handle, '-r0', '-djpeg', fileName);

            fid = fopen([fileName '.jpg'],'rb');
            bytes = fread(fid);
            fclose(fid);
            encoder = org.apache.commons.codec.binary.Base64;
            myimg = char(encoder.encode(bytes))';

             
             printme = PrintToELog.createElogEntry(author, title,...
                     'NONE','NONE',body, myimg);
                 disp(printme);
            % 
            %         
             if ispc()
                 disp('printing windows');
                 printme = [fileName '.jpg'];
               dos(['print ' printme ' /d:\\adprint7\' printer]);
             elseif ismac() 
                 disp('printing mac');
               unix(['lpr -l -P' printer ' ' printme]);
             elseif isunix()
                 disp('printing unix');
               unix(['lpr -l -P' printer ' ' printme]);
             else
               disp('Unknown OS');
             end

              delete([fileName '.jpg'])

            end
        
        function xmlFileName = createElogEntry(author, title, severity, keyword, text, image)
        % copied from ttf2linac
        % xmlDoc = createElogEntry(author, title, severity, keyword, text, image)
        % Input arguments:
        %   author:     Author name string
        %   title:      Title string
        %   severity:   Severity of entry {INFO, DOCU, ERROR, ...}
        %   keyword:    Keyword for this entry
        %   text:       Text string
        %   image:      Base 64 encoded image - optional
        %
        % Output:
        %   W3C XML document as stringPrint
        %
        % See also XMLWRITE
        %
        elems = {'author' 'title' 'severity' 'keyword' 'text' 'image'};
        vals = { author title severity keyword text image};
        entry = cell2struct(vals,elems,2);

        docNode = com.mathworks.xml.XMLUtils.createDocument('entry');
        docRootNode = docNode.getDocumentElement;

        for i=1:length(fieldnames(entry))
            thisNode=docNode.createElement(elems{i});
            thisNode.appendChild(docNode.createTextNode(getfield(entry,char(elems(i)))));
            docRootNode.appendChild(thisNode);
        end

        xmlDoc = xmlwrite(docNode);
        xmlFileName = [tempname,'.xml'];
        xmlwrite(xmlFileName,docNode);
        end
    end
    
end

