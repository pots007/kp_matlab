% checkGrenouilleCalibration.m
%
% Some code to compare the interferometer data with measured spectra. Also
% calibrate the omega-axis of the grenouille.

if ismac
    
else
    rootfol = '/media/kpoder/DATA/grenouille/Calibration/';
end

specfiles = dir(fullfile(rootfol, '*.txt'));
spec = importdata(fullfile(rootfol, specfiles(1).name));
specdata = spec.data;
logm = specdata(:,1) < 650 | specdata(:,1) > 875;
bg = fit(specdata(logm,1), specdata(logm,2), 'poly4');
spec = specdata(:,2) - bg(specdata(:,1));

frogfol = fullfile(rootfol, 'Interferometer');
frogfiles = dir(fullfile(frogfol, '*.pgm'));
imID = 12;
frogim = imread(fullfile(frogfol, frogfiles(imID).name));

calfile = '/media/kpoder/DATA/grenouille/software/FROGed2016/LaserLabGrenouille20.mat';
load(calfile)
[H,W] = size(frogim);
frogax = 2*((-(1:W) + W/2-80)*fCal.dl*2.1 + fCal.lCenter);

hfig = figure(93846); clf(hfig);
ax = makeNiceAxes(hfig);
plot(frogax, (sum(frogim,1)/max(sum(frogim,1))).^2, 'r')
plot(specdata(:,1), spec./max(spec), 'k');

set(ax, 'XLim', [750 900]);