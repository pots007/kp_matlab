% analyseLaserStabilityData.m

% Get the data
datfol = 'C:\Users\kpoder\ownCloud\Writing\2017_EAAC_VI\talk';
datfile = 'LaserSheet_20170920.xlsx';
[~,~,raws] = xlsread(fullfile(datfol, datfile));

%%
% First column is dates - get them into datestr format?
% First get the rows where we actually have a date!
logm = cellfun(@(x) sum(isnan(x)), raws(:,1));
logm(4) = 1;
raw_dates = raws(~logm, 1);
data_days = cellfun(@(x) datenum(datetime(x, 'InputFormat', 'dd.MM.yyyy')), raw_dates, 'UniformOutput', 1);

raw_data = raws(~logm, [11 14]);
MPA2_en = zeros(size(raw_data));
for k=1:size(raw_data,1)
    if ischar(raw_data{k,1})
        MPA2_en(k,1) = str2double(raw_data{k,1});
    else
        MPA2_en(k,1) = raw_data{k,1};
    end
    enerr = raw_data{k,2};
    if ischar(enerr)
        % Replace the bloody commas
        enerr(strfind(enerr, ',')) = '.';
        MPA2_en(k,2) = str2double(enerr);
    else
        MPA2_en(k,2) = enerr;
    end
end

% And now we can start plotting
hfig = figure(4827486); clf(hfig);
lm = 1:1:size(MPA2_en,1);
errorbar(data_days(lm), MPA2_en(lm,1), MPA2_en(lm,2), 'x');
set(gca, 'XLim', [min(data_days) max(data_days)], 'XTick', linspace(min(data_days), max(data_days), 5));
datetick('x', 'mmm yyyy', 'keeplimits', 'keepticks');
ylabel('Uncompressed energy / mJ');
setAllFonts(hfig, 16);
export_fig(fullfile(datfol, 'EnergyFigure'), '-png', '-nocrop', hfig)