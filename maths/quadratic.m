function x = quadratic(a, b, c)
if nargin==1 && length(a)==3
    c = a(3);
    b = a(2);
    a = a(1);
end
D = b.^2 - 4.*a.*c;
if D>0
    %This is not numerically stable!
    %x1 = 0.5*(-b + sqrt(D))./a;
    %x2 = 0.5*(-b - sqrt(D))./a;
    % The below is much better.
    q = -0.5*(b+sign(b).*sqrt(b.*b-4.*a.*c));
    x1 = q./a;
    x2 = c./q;
    x = [x1 x2];
elseif D==0
    x = -0.5.*b./a;
else
    x = nan;
end