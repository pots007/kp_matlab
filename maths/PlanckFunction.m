% PlanckFunction.m
%
% amplitude
% lambda in SI

function irradiance = PlanckFunction(amp, T, lambda)
c = 299792458; %m/s
h = 6.62606957e-34; %J/s
kB = 1.3806488e-23; %J/K
irradiance = 1 ./lambda.^5 * 1 ./ ( exp( h*c ./(lambda *kB*T) ) - 1);
irradiance = amp*irradiance/max(irradiance);