%Second derivative derived from taylor expansion. Assume double precision.

function out = fpprime(f, x0)
x0 = double(x0);
h = x0*2e-6;
out = (f(x0+h)-2*f(x0)+f(x0-h))./(h.^2);
end