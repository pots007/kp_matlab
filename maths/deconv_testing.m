ens = 1200:1:2000;

meas_sig = exp(-(ens-1500).^2/80^2);
%meas_sig = meas_sig + randn(size(ens))*0.00001;

hfig = figure(55); clf(hfig);
plot(ens, meas_sig);

wc = 70;
c = exp(-(ens-min(ens)).^2/wc^2) + exp(-(ens-max(ens)).^2/wc^2);

%ydc=deconv(meas_sig,c).*sum(c);     % Attempt to recover y by 
ydc=ifft(fft(meas_sig)./fft(c)).*sum(c);

plot(ens, smooth(ydc,10), ens, meas_sig, 'k')

%%

load('GemTrackMat');
% Find the scrbins of the central energy:
[~,ind] = min(abs(GemDeconvMat.energies-1510));
scr_sig = GemDeconvMat.mat(:,ind);
scr_sig_fit = fit(GemDeconvMat.scrbins', scr_sig, 'gauss1');
scr_ax = 1:0.1:100;

hfig = figure(56); clf(hfig); hfig.Color = 'w';
subplot(2,1,1)
plot(GemDeconvMat.scrbins, scr_sig, 'o', scr_ax, scr_sig_fit(scr_ax));
xlim([0 100]);
xlabel('Distance on screen / mm'); ylabel('Signal / a.u.');
title('Spread of 1.52 GeV, 1 mrad beam on screen')
dataf = load(fullfile(getGDrive, 'Experiment', '2015_Gemini', 'EspecAnalysis',...
    '20150902r005', 'mat_manual', '20150902r005s015_Espec1.mat'));

scr_dat_ax = dataf.shotdata.alongscreen;
scr_dat_sig = smooth(dataf.shotdata.dQ_dx);
logm = scr_dat_ax<23;
scr_dat_fit = fit(scr_dat_ax(logm)', scr_dat_sig(logm), 'gauss1');
subplot(2,1,2);
plot(scr_dat_ax, scr_dat_sig, scr_dat_ax, scr_dat_fit(scr_dat_ax));
xlim([0 50]);
xlabel('Distance on screen / mm'); ylabel('Signal / a.u.');
title('Data for 20150902r005s015, Espec 1');

w_deconv_sig = sqrt(scr_dat_fit.c1^2 - scr_sig_fit.c1^2);


%%

% Demonstration of Gaussian convolution and deconvolution.
% Create a rectangular function y, 400 points wide
increment=.01;
x=0:increment:20;
y=zeros(size(x));
y(1000:1100)=1;           

% Add pre-convolution random noise
y=y+.005.*randn(size(y));    % Noise added before the convolution

% Create a centered Gaussian convolution function (maximum at x=zero)
w=.7; % w=width of the convolution function
c=gaussian(x,0,w)+gaussian(x,max(x),w);  % centered Gaussian convolution function

% Convolute rectangular function with Gaussian
yc=ifft(fft(y).*fft(c))./sum(c);  % Create Gaussian convoluted rectangular function

% Add a little bit of post-convolution random noise
yc=yc+.00000001.*randn(size(yc)); % Noise added after the convolution

% Now attempt to recover the original signal by deconvolution (2 methods)
ydc=ifft(fft(yc)./fft(c)).*sum(c);  % Deconvolution by fft/ifft
% ydc=deconvgauss(x,yc,w);  % Deconvolution by "deconvgauss" function

% Plot all the steps
subplot(2,2,1); plot(x,y); title('original y');subplot(2,2,2); 
plot(x,c);title('Convolution function, c'); subplot(2,2,3); plot(x,yc(1:2001)); 
title('yc=y convoluted with c'); subplot(2,2,4); plot(x,ydc);title('ydc=recovered y');