% getLaguerreMatrix.m

% This function returns a matriz of numerical values for given input
% argument, up to given order (max 11).

function polys = getLaguerreMatrix(X, XX, order)

if order<0;
    error('Cannot have negative order');
end
if order>15
    warning('No need for that many orders, bro. Giving you 15.');
    order = 15;
end
polys = exp(-X);
for i=1:order
    polys = [polys; exp(-X).*laguerreL(i,XX)];
end