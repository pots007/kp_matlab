%Fractional FFT function. Returns with FFT space xaxis, the product of the
%two being 2pi.
%Pads the edges with zeros for numpnts bigger than input length

function omegarel = FracFFTX(xaxis, yaxis, numpnts, a)

if(a>1)
    a=1;
end
phi = 0.5*pi*a; %last factor is a
prefact = exp(1i*(0.25*pi - 0.5*phi))/sqrt(sin(phi));
%integral
dt = (max(xaxis)-min(xaxis))/20000;
tstep = min(xaxis):dt:max(xaxis);
omegamax = 2*pi/(xaxis(2)-xaxis(1)) * 0.5;
omega = linspace(-omegamax, omegamax, numpnts);
omegasig = zeros(1,length(omega));
initial = floor(0.5*(numpnts-length(xaxis)));
final = initial + length(xaxis);
inty = interp1(xaxis, yaxis, tstep);
for i=initial:final 
    kernel = tstep.*omega(i)-0.5*(tstep.^2+4*pi^2.*omega(i)^2).*cos(phi);
    kernel = kernel./sin(phi);
    steps = dt.*exp(-1i*kernel).*inty;
    omegasig(i) = prefact*sum(steps);
end
omegarel = [omega; omegasig]';
end