%Third derivative derived from taylor expansion. Assume double precision.

function out = fppprime(f, x0)
x0 = double(x0);
h = x0*1e-6;
out = 0.375*(f(x0+2*h)+2*f(x0-h)-2*f(x0+h)-f(x0-2*h))./(h.^3);
end