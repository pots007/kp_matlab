function q = getProjectiveTransformImage2(inputImage, xin, yin, varargin)

% This makes the transformed image as big as before
lwidth = min([abs(xin(3)-xin(2)) abs(xin(4)-xin(1))]);
%lwidth  = 1500;
lheight = min([abs(yin(2)-yin(1)) abs(yin(3)-yin(4))]);
%lheight = 750;
inputImage = double(inputImage);
imageSize = size(inputImage);

if nargin==3
    parent_ = figure(88);
    figure(parent_);
elseif nargin==4
    parent_ = varargin{1};
end
subplot(2,2,1, 'Parent', parent_);

imagesc(inputImage)
axis image xy
colormap(jet)
xlabel('u')
ylabel('v')
title('Input Image')

%mapshow([xin; xin(1)],[yin; yin(1)], 'Color', 'white');

drawnow;

% fitgeotrans(movingPoints,fixedPoints,transformationType)
% movingPoints are the corners of the screen that need to move
% fixedPoints are formed by a rectangle of minimum width and height
xp = [0 0 lwidth lwidth]';
yp = [0 lheight lheight 0]';

tform = fitgeotrans([xin yin], [xp yp], 'projective');

u = 1:1:imageSize(2);
v = 1:1:imageSize(1);
J = zeros(imageSize);

for n = 1:length(u)
    J(:,n) = abs(projectiveTransformJacobian2(u(n), v, tform));
end

%[outputImage,xdata,ydata] = imtransform(inputImage, tform, 'bicubic', ...
%    'udata', udata,...
%    'vdata', vdata,...
%    'XYScale', 1);
%[Jnew,xdata,ydata]        = imtransform(J, tform, 'bicubic', ...
%    'udata', udata,...
%    'vdata', vdata,...
%    'XYScale', 1);
RA = imref2d(imageSize);
[outputImage, RB] = imwarp(inputImage, RA, tform, 'cubic');
[Jnew, RB] = imwarp(J, RA, tform, 'cubic');

% And get the axis for output image. ceil & floor ensure the axis are the
% same size as the image.
xaxis = ceil(RB.XWorldLimits(1)):floor(RB.XWorldLimits(2)); 
yaxis = ceil(RB.YWorldLimits(1)):floor(RB.YWorldLimits(2)); 

% Correct intensity and avoid nans
outputImage = outputImage./Jnew;
outputImage(isnan(outputImage)) = 0;

% And finish plotting the stuff
subplot(2,2,2, 'Parent', parent_)
imagesc(xaxis,yaxis,outputImage);
hold on
plot([xp xp], [yp yp], 'w', 'LineWidth', 2)
hold off
axis image xy
xlabel('x'); ylabel('y');
title('Output Image')

subplot(2,2,3, 'Parent', parent_)
imagesc(J); axis image xy
xlabel('u'); ylabel('v');
title('Jacobian (u,v)')

subplot(2,2,4, 'Parent', parent_)
imagesc(xaxis, yaxis, Jnew); axis image xy
xlabel('x'); ylabel('y');
title('Jacobian (x,y)')

% And create output structure
q.I = inputImage;
q.I2 = outputImage;
q.J = J;
q.J2 = Jnew;
q.tform = tform;
q.x = xaxis;
q.y = yaxis;
q.u = u;
q.v = v;
q.screenu = xin;
q.screenv = yin;
q.screenx = xp;
q.screeny = yp;

end

% Shamelessly stolen from the Juffalo
function J = projectiveTransformJacobian2(u,v,params)

[a, b, c, d, e, f, g, h] = deal(params.T(1,1), params.T(2,1), params.T(3,1), ...
    params.T(1,2), params.T(2,2), params.T(3,2), ...
    params.T(1,3), params.T(2,3));

lambda = 1./(g*u + h*v + 1);

J = lambda.^2*(a*e - b*d) + lambda.^3.*( (d*h - e*g).*(a*u + b*v + c) + (b*g - a*h).*(d*u + e*v + f) );

end