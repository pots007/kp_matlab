function yprime = circle(x,y)
yprime = zeros(2,1);
yprime(1) = y(2);
yprime(2) = (-1-2*y(2)^2)/y(1);

