% findZeroCrossings.m
%
% Locate the indices of zero crossings in a signal.

function [inds,upcross,downcross] = findZeroCrossings(x)
% upward zero-crossings to nearest time step
upcross = find(x(1:end-1) <= 0 & x(2:end) > 0);
% interpolate
upcross = upcross - x(upcross) ./ (x(upcross+1)-x(upcross));
% downward zero-crossings
downcross = find(x(1:end-1) >= 0 & x(2:end) < 0);
downcross = downcross - x(downcross) ./ (x(downcross+1)-x(downcross));

inds = sort([upcross(:); downcross(:)]);
