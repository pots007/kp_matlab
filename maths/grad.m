% Numerical gradient for a single point
% func(x,y)

function g = grad(f, x0, y0)
x0 = double(x0);
y0 = double(y0);
h = x0*4e-5;
g.x = (f(x0+h,y0)-f(x0-h,y0))./(2.*h);
h = y0*4e-5;
g.y = (f(x0,y0+h)-f(x0,y0-h))./(2.*h);
end
