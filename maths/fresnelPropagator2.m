function U = fresnelPropagator2 (U0, dx, dy, z, lambda)
% The function receives a field U0 at wavelength lambda
% and returns the field U after distance z, using the Fresnel
% approximation. dx, dy, are spatial resolution.
% From http://stackoverflow.com/questions/20971945/fresnel-diffraction-in-two-steps
k=2*pi/lambda;
[ny, nx] = size(U0); 

Lx = dx * nx;
Ly = dy * ny;

dfx = 1./Lx;
dfy = 1./Ly;

u = ones(nx,1)*((1:nx)-nx/2)*dx;    
v = ((1:ny)-ny/2)'*ones(1,ny)*dy;   

%[X,Y] = meshgrid(dfx*linspace(-0.5*nx,0.5*nx,nx),dfy*linspace(-0.5*ny,0.5*ny,ny));

%O = fftshift(fft2(U0));

%H = exp(1i*k*z).*exp(-1i*pi*lambda*z*(u.^2+v.^2));  
H = exp(1i*k*z)/(1i*lambda*z).*exp(1i*k*(u.^2+v.^2)/(2*z));

U = ifftshift(ifft2(fft2(U0).*fft2(H)));

% Mod 20170608:
%U = ifft2(ifftshift(O.*fftshift(fft2(H))));
