%the three point rule derivative. Assume double precision.

function out = fprime(f, x0)
x0 = double(x0);
h = x0*4e-5;
out = (f(x0+h)-f(x0-h))./(2.*h);
end