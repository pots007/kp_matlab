% interpolates yaxis between numpnts linearly spaced points
% transforms and shifts the yaxis
% gives xaxis as set of linear points based on sampling theorem

function out = fftX(xaxis, yaxis, numpnts)

%newxaxis = linspace(xaxis(1), xaxis(length(xaxis)), numpnts);
%newyaxis = interp1(xaxis, yaxis, newxaxis);
%resample and interpolate with old number of points
newxaxis = linspace(xaxis(1), xaxis(length(xaxis)), length(xaxis));
newyaxis = interp1(xaxis, yaxis, newxaxis);
%resample xaxis to new number of points
%newxaxis = linspace(xaxis(1), xaxis(length(xaxis)), numpnts);
%calculate resolution in Fourier space
FFTxmax = 2*pi/(xaxis(2)-xaxis(1));
samp = linspace(-0.5*FFTxmax, 0.5*FFTxmax, numpnts);
sig = fftshift(fft(newyaxis, numpnts));
out = [samp' sig'];
end