%This lens solver

function imagedist = lenscalc(focallength, objectdist)
    imagedist = focallength.*objectdist./(objectdist - focallength);
end