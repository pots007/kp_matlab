% normaliseMatrix.m
%
% Will normalise each row or col of the matrix to its max or sum.
%
% Arguments: 
%           normMethod - 1 to use max, 2 to use sum
%           direction - 1 to normalise columns, 2 to normalise rows

function outM = normaliseMatrix(inMatrix, normMethod, direction)

if nargin<3 || isempty(direction)
    % Normalise rows by default!
    direction = 2;
end
if nargin<2 || isempty(normMethod)
    % Normalise using max by default
    normMethod = 1;
end

[nRows, nCols] = size(inMatrix);
if normMethod == 1
    normV = max(inMatrix, [], direction);
else
    normV = sum(inMatrix, direction);
end

if direction==1
    % normV is [1xnCols]
    normM = repmat(normV, nRows, 1);
else
    normM = repmat(normV, 1, nCols);
end
outM = inMatrix./normM;