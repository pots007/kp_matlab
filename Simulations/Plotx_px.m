files = dir('data/*.sdf');
if (~(exist('x_px', 'dir')==7))
    mkdir('x_px');
end
figure(2)
cla;
set(2, 'WindowStyle', 'docked');
for i=1:length(files)
    if (exist(['x_px/x_px_' files(i).name(1:4) '.png'], 'file'))
        continue;
    end
    dataf = GetDataSDF(['data/' files(i).name]);
    if (~isfield(dataf, 'dist_fn'))
        continue;
    end
    x = dataf.dist_fn.x_px.electron.grid.x;
    %x = x(1:length(x)-1);
    px = dataf.dist_fn.x_px.electron.grid.y;
    px = px./5.344e-22; %Get to MeV/c
    %y = y(1:length(y)-1);
    time = dataf.time*1e15;
    data = dataf.dist_fn.x_px.electron.data';
    %data = 1e-6.*data;
    subplot(2,1,1)
    imagesc(x, px, data);
    title(['x_px ' files(i).name ', after ' num2str(time) ' fs'], ...
        'Interpreter', 'none');
    colorbar;
    xlabel('Longitudonal distance (m)');
    ylabel('px (MeV/c)');
    ylabel(colorbar, 'Number of particles');
    set(gca, 'CLim', [0 5e8], 'YDir', 'normal');
    subplot(2,1,2)
    imagesc(x, px, data);
    title(['x_px ' files(i).name ', after ' num2str(time) ' fs'], ...
        'Interpreter', 'none');
    colorbar;
    xlabel('Longitudonal distance (m)');
    ylabel('px (MeV/c)');
    ylabel(colorbar, 'Number of particles');
    set(gca, 'CLim', [0 max(max(data))], 'YDir', 'normal');
    axis tight;
    drawnow;
    %saveas(2, ['x_px/x_px_' files(i).name(1:4) '.png']);
    export_fig(['x_px/x_px_' files(i).name(1:4) '.png'], '-png',2);
end
