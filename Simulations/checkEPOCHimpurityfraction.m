% Compare the actual, simulated fractions of C and O

rootf = '/Volumes/SimKris/ATA2II/';
%rootf = '/Users/kristjanpoder/epochdata/';
dfols = {'II_3D1', 'II_3D2', 'II_3D3', 'ionisplay'};
figure(90);
for i=3
    fdat = GetDataSDFchoose(fullfile(rootf, dfols{i}, 'data', 'normal0024.sdf'), 'number_density/electron')
end

%%
imagesc(cdat{4})
for i=1:4
    (mean(mean(cdat{i}(120:260, 2050:2350)))*ncrit(800))/1.1e19
end
%%

ne = ones(300);
[xg, yg] = meshgrid(linspace(-50, 50, 300),linspace(-50, 50, 300));
ne(abs(xg)>25) = ne(abs(xg)>25).*(30-abs(xg(abs(xg)>25)))/5;
ne(ne<0.01) = 0;
imagesc(ne)

%%
species = {'electron', 'carbon', 'oxygen'};
for i=1:3
    fdat{i} = GetDataSDFchoose(fullfile(rootf, 'II_3D1','data', 'normal0024.sdf'), ['number_density/' species{i}])
end
for i=1:3
    nemean(i) = mean(mean(mean(fdat{i}.Derived.Number_Density.(species{i}).data(350,130:250, 130:250))))
end