function plotPhotonCodePhaseSpace(fol, filename)

%fol = 'negchirp';
%filename = 'test.gif';
inp = getFileText([fol '/inputs.txt']);
dxi = str2double(inp{1}(regexp(inp{1}, ' '):end));
ncell = str2double(inp{2}(regexp(inp{2}, ' '):end));
k0 = str2double(inp{6}(regexp(inp{6}, ' '):end));
k_a = linspace(0, 2*k0, ncell);
xi_a = 0:dxi:(ncell-1)*dxi;
files = dir([fol '/dumps/*phase*']);
figure(900);
set(900, 'color', 'w');
data = importdata([fol '/dumps/' files(1).name]);
wign = hist2(data(:,2), data(:,1), k_a/k0,xi_a);
imagesc(xi_a, k_a, wign);
cm = 0.2*max(wign(:));
set(gca, 'CLim', [0 cm], 'YDir', 'normal');
%set(gca, 'nextplot', 'replacechildren', 'visible', 'off');
f = getframe(900);
im = frame2im(f);
[imw,map] = rgb2ind(im, 256);
imwrite(imw,map, filename, 'Delaytime', 0, 'LoopCount', inf);
for i=2:length(files)
    data = importdata([fol '/dumps/' files(i).name]);
    wign = hist2(data(:,2), data(:,1), k_a/k0,xi_a);
    imagesc(xi_a, k_a, wign);
    set(gca, 'CLim', [0 cm], 'YDir', 'normal');
    f = getframe(900);
    im = frame2im(f);
    imw = rgb2ind(im, 256);
    imwrite(imw, map, filename, 'Delaytime', 0, 'writemode', 'append');
end
close(900)