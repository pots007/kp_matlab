tic
npart = 1e4;
xp = zeros(npart, 3);
x = zeros(npart, 3);
divthet = 0.002;
sigma_x = 0.002;
for i=1:npart
    phi = (0.5-rand(1))*2*pi;
    %Factor with theta is twice the beam divergence
    %theta = (0.5-G4RandGauss::shoot())*divangle;
    %Uncomment next two lines to get energy spread in electrons
    ParEn = 2000;
    %particleGun->SetParticleEnergy(ParEn);
    %theta = 0.511/(ParEn);
    theta = acos(2.*rand(1)-1.)/pi*divthet;
    xp(i,1) = cos(phi)*theta;
    xp(i,2) = sin(phi)*theta;
    xp(i,3) = (1-theta*theta*0.5);
    x(i,1) = randn(1)*sigma_x;
    x(i,2) = randn(1)*sigma_x;
    x(i,3) = randn(1)*sigma_x;
end
toc

emit = sqrt(mean(x(:,1).^2)*mean(xp(:,1).^2)-mean(x(:,1).*xp(:,1)).^2)