%x in metres

function him = plotEspectrumwithphase(E, x, omega0, lambdaflag)
if(nargin==0)
    disp('Give me something, bro!');
    return
end
t = x./3e8;
numpnts = 2^(nextpow2(length(E)) + 1);
Ehil = hilbert(E);
Ei = abs(Ehil).*exp(1i.*angle(Ehil));
omegadata = fftX(t, Ei, numpnts);
specphase = unwrap(angle(omegadata(:,2)));
[minv in0] = min(abs(omegadata(:,1) - omega0));
zerophase = specphase(in0);
logmat = (abs(omegadata(:,2))<0.1*max(abs(omegadata(:,2))));

specphase = specphase - zerophase;
%assignin('base', 'omegadata', omegadata(:,2));
%tma = 2*pi/(omegadata(2,1) - omegadata(1,1))
%tpr = linspace(-0.5*tma, 0.5*tma, numpnts);
%tpr = tpr - tpr(in0);
%1/omega0*(omegadata(in0+1,1)-omegadata(in0,1))
%specphase(in0+1)-specphase(in0)
%specphase = specphase - 2*pi*(omegadata(:,1) - omegadata(in0,1)).*tpr';
specphase(logmat) = nan;
specphase(omegadata(:,1)<0)=nan;
if (~lambdaflag)
    [AX H1 H2] = plotyy(omegadata(:,1), abs(omegadata(:,2)), ...
        omegadata(:,1), specphase);
    set(get(AX(1),'Ylabel'), 'String', 'Spectrum');
    set(get(AX(2),'Ylabel'), 'String', 'Phase (rad)');
    set(get(AX(1),'XLabel'), 'String', 'Frequency (rad)');
    set(AX(1), 'XLim', [omega0-0.25*omega0 omega0+0.25*omega0]);
    set(AX(2), 'XLim', [omega0-0.25*omega0 omega0+0.25*omega0]);
    set(AX(2), 'XTick', []);
else
    lambda = 2*pi*3e8./omegadata(:,1);
    [minv fend] = min(abs(lambda - 3e-7));
    [minv fin] = min(abs(lambda - 1.2e-6));
    lambdasig = 2*pi*3e8*omegadata(fin:fend,2)./(omegadata(fin:fend,1).^2);
    [AX H1 H2] = plotyy(lambda(fin:fend), abs(lambdasig), ...
        lambda(fin:fend), specphase(fin:fend));
    set(get(AX(1),'Ylabel'), 'String', 'Spectrum');
    set(get(AX(2),'Ylabel'), 'String', 'Phase (rad)');
    set(get(AX(1),'XLabel'), 'String', 'Wavelength (m)');
    lambda0 = 2*pi*3e8/omega0;
    %set(AX(1), 'XLim', [lambda0-0.25*lambda0 lambda0+0.25*lambda0]);
    %set(AX(2), 'XTick', []);

end
him = AX;
end