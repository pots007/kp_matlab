% Check what the speed of the window actually is

datfol = '/Volumes/SimKris/Gem3D_2/data';
flist = dir(fullfile(datfol, 'normal*.sdf'));
prop_data = zeros(length(flist), 2);
for i=1:length(flist)
    dataf = GetDataSDFchoose(fullfile(datfol, flist(i).name), 'grid');
    prop_data(i,1) = dataf.time;
    prop_data(i,2) = max(dataf.Grid.Grid.x);
end

hfig = figure(901); clf(hfig);
%plot(prop_data(:,1), prop_data(:,2), '-o')
plot(diff(prop_data(:,2))./diff(prop_data(:,1))/c, '-o')
ylabel('$\beta$'); xlabel('Dump no');
make_latex(hfig); setAllFonts(hfig, 20);