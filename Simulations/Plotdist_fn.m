%Plot ne of one datafile

function him = plotDist_fn(datafile, species, dist_fn)

dataf = GetDataSDFchoose(datafile, 'grid');
time = dataf.time*1e15;
dataf = GetDataSDFchoose(datafile, ['grid/' dist_fn '/' species]);
x = dataf.Grid.(dist_fn).(species).x;
x = x(1:length(x)-1)*1e3; % in mm
if (strcmp(dist_fn, 'py_px'))
    x = x./5.344e-19;
end
y = dataf.Grid.(dist_fn).(species).y;
y = y./5.344e-22;

dataf = GetDataSDFchoose(datafile, [dist_fn '/' species]);
dfn = dataf.dist_fn.(dist_fn).(species).data';
him = imagesc(x,y,log10(dfn));
set(gca, 'YDir', 'normal');
title(['{' dist_fn '} of ' species ' at t=' num2str(time*1e-3) ' ps']);

