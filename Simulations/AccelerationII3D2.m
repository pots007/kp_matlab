%% Get all the data
Constants;
load divmap.mat; load invfire.mat;
fold = 'data/';
plotgraphs = true;

filen = dir([fold '/normal*.sdf']);

specieslist = {'electron', 'Cshell5', 'Cshell6', 'Oshell7', 'Oshell8'};

if (~(exist('ne', 'dir')==7))
    mkdir('ne');
end
if (exist('ne/nedata.mat','file')==2)
    load ne/nedata.mat;
else
    nedat = cell(1000,3+2*length(specieslist));
    ionexist=false;
end

for i=1:length(filen)
    if strcmp(nedat{i,1},filen(i).name)
        continue;
    end
    disp(filen(i).name);
    nedat{i,1} = filen(i).name;
    try
        dataf = GetDataSDFchoose([fold filen(i).name], 'grid');
        x = dataf.Grid.Grid.x;
        nedat{i,2} = x(2:end);
        nedat{i,3} = dataf.time;
    catch
    end
    
    datafl = GetBlocksSDF([fold filen(i).name]);
    datafids = cell(1, length(datafl));
    for k=1:length(datafl)
        datafids{k} = datafl(1,k).id;
    end
    % Find out how many species have their dist fns in the file
    nspecies = 0;
    species = cell(0,1);
    for k=1:length(datafids)
        if isempty(strfind(datafids{k}, 'x_px'))
            continue;
        end
        if (strfind(datafids{k}, 'x_px')==1)
            nspecies = nspecies + 1;
            species{nspecies} = datafids{k}(6:end);
        end
    end
    % Now add these into struct transverse
    
    for k=1:nspecies
        dataf = GetDataSDFchoose([fold filen(i).name], ['x_px/' species{k}]);
        px = sum(dataf.dist_fn.x_px.(species{k}).data,1)';
        dataf = GetDataSDFchoose([fold filen(i).name], ['grid/x_px/' species{k}]);
        pxg = dataf.Grid.x_px.(species{k}).y;
        pxg = pxg./5.344e-22;
        dataf = GetDataSDFchoose([fold filen(i).name], ['number_density/' species{k}]);
        ne = dataf.Derived.Number_Density.(species{k}).data;
        it = 2+2*sum(strcmp(species{k}, specieslist).*(1:length(specieslist)));
        nedat{i,it} = ne(:,floor(0.5*size(ne,2)),floor(0.5*size(ne,3)));
        nedat{i,it+1} = [px pxg];
    end
    
end

save('ne/nedata.mat', 'nedat');

%% And now for the plotting part
totf = sum(~cellfun(@isempty, nedat(:,1)));
% Find which species actually exist somewhere
exists = zeros(size(specieslist));
for i=1:totf
    ttemp = cellfun(@isempty, nedat(i,:));
    for k=1:length(specieslist)
        if (ttemp(2+2*k)==0)
            exists(k) = 1;
        end
    end
end
exists = logical(exists);
% Get axis locations
locs = GetFigureArray(sum(exists), 2, 0.07,0.03,'down');
axit = 1;
figure(55)
set(55, 'WindowStyle', 'docked');
clf;
axs = 0;
for i=1:sum(exists)
    if ~exists(i) continue; end
    % Plasma density part
    axs(axit) = axes('Parent', 55, 'Position', locs(:,axit)); axit = axit+1;
    propa = zeros(1,totf);
    n_es = zeros(length(nedat{1,2}),totf);
    for m=1:totf
        propa(m) = max(nedat{m,2})*1e3;
        n_es(:,m) = nedat{m,2+2*i};
    end
    imagesc(propa, 1e6*(nedat{1,2}-nedat{1,2}(end)), n_es);
    title(specieslist{i});
    set(axs(axit-1), 'XTick', []);
    %distfn part
    axs(axit) = axes('Parent', 55, 'Position', locs(:,axit)); axit = axit+1;
    
    %find index of largest px
    maxs = cellfun(@(x) max(x(:,2)), nedat(1:totf,3+2*i), 'UniformOutput', false, 'Errorhandler', @(y,x) 0);
    tempmax = cell2mat(maxs); tempmax(tempmax>1e10) = 0;
    [maxen, maxind] = max(tempmax);
    px = nedat{maxind,3+2*i}(:,2);
    px_e = zeros(length(px),totf);
    for m=1:totf
        if isempty(nedat{m,3+2*i}) continue; end;
        px_e(:,m) = interp1(nedat{m,3+2*i}(:,2), nedat{m,3+2*i}(:,1), px);
    end
    imagesc(propa, px, log10(px_e));  
end
set(axs, 'YDir', 'normal');
set(axs(1:2:end), 'CLim', [0 1.3e26]);
colormap(parula)
setAllFonts(55, 18);
export_fig('ne/nedat', '-nocrop', '-transparent', '-pdf', 55)