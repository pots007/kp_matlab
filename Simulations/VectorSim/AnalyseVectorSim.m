% Should plot everything for the old type sim data
% Here have 6 files and a README

%function AnalyseVectorSim(simname)
simname = 'F400_defocus200000um_sim_';
figure(990); clf;
locs = GetFigureArray(3,2,0.07,0.07, 'across');
readm = getFileText([simname 'README.txt']);
numpnts = str2double(readm{2}(strfind(readm{2},':')+1:end));
lim1 = str2double(readm{3}(strfind(readm{3},':')+1:end));
xax = linspace(lim1, -lim1, numpnts);
xaxt = SetSmartUnit(xax);
yax = linspace(lim1, -lim1, numpnts);
yaxt = SetSmartUnit(yax);
fields = {'E_x', 'E_y', 'E_z', 'H_x', 'H_y', 'H_z'};
axs = zeros(1,6);
Constants;
C = 0.5*c*1e-4*[ones(1,3)*epsilon0 ones(1,3)*4*pi*1e-7];
for i=1:6
    dataf = importdata([simname fields{i} '.txt']);
    locf = complex(dataf(:,4), dataf(:,5));
    locf = reshape(locf, numpnts, numpnts);
    axs(i) = axes('Parent', 990, 'Position', locs(:,i));
    imagesc(xaxt.t, yaxt.t, locf.*conj(locf)*C(i));
    
    %imagesc(xaxt.t, yaxt.t, angle(locf));
    colorbar;
    title(fields{i});
    drawnow;
end
colormap(parula)