simfolder = 'data/';
filen = dir([simfolder '*.sdf']);
if (~exist('ne+nI','dir'))
    mkdir('ne+nI');
end
figure(8);
for i=1:length(filen)
    if (exist(['ne+nI/' filen(i).name(1:4) '.png'], 'file'))
        continue;
    end
    disp(['File ' filen(i).name]);
    %dataf = GetDataSDF('data/0020.sdf');
    dataf = GetDataSDF([simfolder files(i).name]);
    clf;
    x = 1e3*dataf.Grid.Grid.x;
    y = 1e6*dataf. Grid.Grid.y;
    try
        CO2 = 1e-6*dataf.Derived.Number_Density.shell5.data';
        ne = 1e-6*dataf.Derived.Number_Density.electron.data';
    catch
        continue;
    end
    cmap2 = [iijcm; gray(256)];
    maxne = max(ne(:)); nemax = 3;
    ne1 = nemax*ne./maxne; ne1(ne1>1)=1;
    CO2max = max(CO2(:));
    CO21 = CO2./CO2max+1;
    CO21(CO21<1.15)=0;
    h1 = imagesc(x,y,CO21+ne1);
    set(gca, 'CLim', [0 2])
    xlabel('x /mm'); ylabel('y /\mum');
    colormap(cmap2);
    set(gca, 'Position', get(gca,'Position')+[0 0 -0.2 0]);
    hb1 = colorbar;
    set(hb1, 'Position', get(hb1,'Position')+[0.2 0 0 0], 'YLim', [1 2],...
        'YTick', 1:0.2:2);
    hb2 = colorbar;
    set(hb2, 'Position', get(hb2,'Position')+[0.1 0 0 0], 'YLim', [0 1],...
        'YTick', 0:0.2:1);
    ticks = get([hb1;hb2], 'YTick');
    ticklabels = cell(2, length(ticks{1}));
    for k=1:length(ticks{1})
        ticklabels{1,k} = sprintf('%1.1f', (ticks{1}(k)-1)*CO2max*1e-19);
        ticklabels{2,k} = sprintf('%1.1f', (ticks{2}(k))*maxne/nemax*1e-19);
    end
    set(hb1, 'YTickLabel', ticklabels(1,:));
    set(hb2, 'YTickLabel', ticklabels(2,:));
    set(get(hb1, 'YLabel'),'String', 'shell5 density /10 ^{19} cm^{-3}');
    set(get(hb2, 'YLabel'),'String', 'electron density /10 ^{19} cm^{-3}');
    set(gca, 'Position', get(gca, 'Position')+[-0.05 0 0.1 0]);
    %title();
    setAllFonts(8,20);
    export_fig(['ne+nI/' filen(i).name(1:4)], '-transparent', '-nocrop', '-png', 8);
end