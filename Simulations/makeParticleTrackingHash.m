% makeAccHastable
%
% This function will make a hashmap of all particles as they accelerate.
% The key is ID, the fields are x,y,z, px,py,pz, gamma.

trackItem = struct;
%trackItem.ID = [];
trackItem.y = [];
trackItem.z = [];
trackItem.px = [];
trackItem.py = [];
trackItem.pz = [];
trackItem.gamma = [];
trackItem.time = [];

filen = dir('data/particles*.sdf');
IDs = [];

trackMap = containers.Map('KeyType', 'int64', 'ValueType', 'any');
%

for i=2:length(filen)
    tic;
    fprintf('File %3i\n', i);
    dataf = GetDataSDF(fullfile('data', filen(i).name));
    if ~isfield(dataf, 'Particles')
        fprintf('\t No data in file %3i\n', i); 
        continue;
    end
    IDs = dataf.Particles.ID.subset_highgamma.electron.data;
    for p = 1:1000:length(IDs)
        if isKey(trackMap, IDs(p))
            temp = trackMap(IDs(p));
            temp.x(end+1) = dataf.Particles.ID.subset_highgamma.electron.grid.x(p);
            temp.y(end+1) = dataf.Particles.ID.subset_highgamma.electron.grid.y(p);
            temp.z(end+1) = dataf.Particles.ID.subset_highgamma.electron.grid.z(p);
            temp.px(end+1) = dataf.Particles.Px.subset_highgamma.electron.data(p);
            temp.py(end+1) = dataf.Particles.Py.subset_highgamma.electron.data(p);
            temp.pz(end+1) = dataf.Particles.Pz.subset_highgamma.electron.data(p);
            temp.gamma(end+1) = dataf.Particles.Gamma.subset_highgamma.electron.data(p);
            temp.time(end+1) = dataf.time;
            trackMap(IDs(p)) = temp;
        else
            temp = trackItem;
            temp.x = dataf.Particles.ID.subset_highgamma.electron.grid.x(p);
            temp.y = dataf.Particles.ID.subset_highgamma.electron.grid.y(p);
            temp.z = dataf.Particles.ID.subset_highgamma.electron.grid.z(p);
            temp.px = dataf.Particles.Px.subset_highgamma.electron.data(p);
            temp.py = dataf.Particles.Py.subset_highgamma.electron.data(p);
            temp.pz = dataf.Particles.Pz.subset_highgamma.electron.data(p);
            temp.gamma = dataf.Particles.Gamma.subset_highgamma.electron.data(p);
            temp.time = dataf.time;
            trackMap(IDs(p)) = temp;
        end
    end
    fprintf('\t\t Done with file %3i in %2.3f\n', i, toc);
end

save('test', 'trackMap')