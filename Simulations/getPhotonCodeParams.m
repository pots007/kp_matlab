function p = getPhotonCodeParams(simf)
textf = getFileText([simf '/README.txt']);
p.ne = str2double(textf{2}(23:end-5));
p.tau = str2double(textf{3}(23:end-3));
p.ddphi = str2double(textf{4}(16:end-5));
p.tdump = str2double(textf{5}(13:end-3))*1e-15;
p.tend = str2double(textf{6}(24:end-3))*1e-12;
