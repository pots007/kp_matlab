lambda0 = 0.8e-6;
L = 0.02; 
omega0 = 2*pi*c/lambda0;
l = 30*1e-15*c; %Laser pulse length
Rs_in = (L+l)/lambda0;
w0 = 10e-6; 
gamma = sqrt(L/l);
gamma = 7;
beta = sqrt(1-1/gamma^2);
L_ = L/gamma;
lambda0_ = lambda0*gamma*(1+beta);
omega0_ = omega0/(gamma*(1+beta));
l_ = l*gamma*(1+beta);
Rs_out = (L_+l_)/lambda0_;
fprintf('Frame with gamma=%2.1f and beta=%2.5f:\n\n', gamma, beta);
fprintf('%10s\t%10s\t%10s\t%10s\t%10s\n', 'frame', 'R_s', 'L_0', 'lambda_0', 'l_0');
fprintf('%10s\t%10.2e\t%10.2e\t%10.2e\t%10.2e\n', 'lab', Rs_in, L, lambda0, l);
fprintf('%10s\t%10.2e\t%10.2e\t%10.2e\t%10.2e\n\n', 'boosted', Rs_out, L_, lambda0_, l_);

