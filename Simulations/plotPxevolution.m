%To be called after Acceleration is complete

figure(79)
imagesc(propa, px_s*1e-3, px_s6+px_s7);
set(gca, 'YDir', 'normal', 'FontSize', fsize, 'CLim', [0 1e12])
ylabel('p_x / GeV/c', 'FontSize', fsize);
xlabel('Propagation / mm', 'FontSize', fsize);
%title('p_x of shell6+7 vs propagation')
setAllFonts(79,20)
export_fig('s6s7px', '-transparent', '-nocrop', '-pdf', 79);