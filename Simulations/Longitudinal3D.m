load divmap.mat;
fold = 'data';
plotgraphs = true;

filen = dir([fold '/*_Ex.sdf']);
if (isempty(filen))
    filen = dir([fold '/normal*.sdf']);
end

if (~(exist('Ex', 'dir')==7))
    mkdir('Ex');
end
if (exist('Ex/longitudinal.mat','file')==2)
    load Ex/longitudinal.mat;
else
    longitudinal = cell(500,6);
end

for i=1:length(filen)
    if (strcmp(longitudinal{i,1}, filen(i).name)==1)
        continue
    end
    longitudinal{i,1} = filen(i).name;
    dataf = GetDataSDFchoose([fold '/' filen(i).name], 'ex');
    Ex = dataf.Electric_Field.Ex.data;
    try
        grid = GetDataSDFchoose([fold '/' filen(i).name], 'grid');
        x = grid.Grid.Grid.x;
        longitudinal{i,3} = x(1:length(x)-1);
    catch
        
    end
    cind = round(size(Ex,2)*0.5);
    longitudinal{i,2} = Ex(:,cind,cind);
    nav = 5; 
    Ecol = Ex(:, cind-nav:cind+nav, cind-nav:cind+nav);
    tt = mean(Ecol, 3); tt = mean(tt, 2);
    longitudinal{i,4} = tt;
    longitudinal{i,5} = dataf.time;
    %longitudinal{i,6} = widthfwhm(longitudinal{i,2}');
    disp(['File ' filen(i).name]);
end
save('Ex/longitudinal.mat', 'longitudinal');
%%
if (plotgraphs)
    figure(4)
    set(4, 'WindowStyle', 'docked');
    clf;
    ax1 = subplot(1,2,1);
    accfield = zeros(length(longitudinal{2,2}),i);
    fieldamp = NaN(1,i);
    times = zeros(1,i);
    %fwhms = NaN(1,i);
    for m = 2:i
        accfield(:,m) = longitudinal{m,2};%/max(longitudinal{m,2});
        fieldamp(m) = min(longitudinal{m,2});
        times(m) = longitudinal{m,5};
        %fwhms(m) = longitudinal{m,6};
    end
    y = [];
    m=2;
    while(isempty(y))
        y = longitudinal{m,3};
        m = m+1;
    end
    y = y-y(end);
    prop = times*3e8*1e3; %propagation distance in mm;
    imagesc(prop, y*1e6, accfield);
    set(ax1, 'YDir', 'normal');
    ylabel('Longitudinal dimension (\mum)');
    xlabel('Propagation distance (mm)');
    title('Accelerating field as a function of propagation distance');
    ax2 = subplot(1,2,2);
    plot(prop, fieldamp);
    xlabel('Propagation distance (mm)');
    ylabel('Peak slice accelerating field');
    title('Peak accelerating field');
    cb = colorbar('Peer', ax1);
    set(ax2, 'XLim', get(ax1, 'XLim'));
    ylabel(cb, 'E_x (V/m)');
    setAllFonts(4,20);
    export_fig('Ex/longitudinal.pdf', '-pdf', '-nocrop', '-transparent',4);
end