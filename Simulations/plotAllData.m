% Looks through folders and checks whether there's any data to be plotted.

rfol = '/Users/kristjanpoder/epochdata/Gem2015scans/';
runss = dir(rfol);
figure(77); set(77, 'Position', [500 500 1280 720], 'Color', 'w');
locs = GetFigureArray(2, 2, 0.07, 0.05, 'down');
axs = zeros(1,4);
load('owncolourmaps');
for i=3:length(runss)
    if ~runss(i).isdir
        continue;
    end
    runname = [rfol runss(i).name];
    dfiles = dir([runname '/data/normal*.sdf']);
    if ~exist([runname '/plots'], 'dir')
        mkdir([runname '/plots']);
    end
    % Figure out what parameters were used for this run!
    inputd = getFileText([runname '/data/input.deck']);
    rho0 = str2double(inputd{23}(9:end));
    Rstep = str2double(inputd{24}(10:end));
    Lstep = str2double(inputd{26}(8:10));
    for k=1:length(dfiles)
        if exist([runname '/plots/' dfiles(k).name(1:end-3) 'png'], 'file')
            continue;
        end
        disp([runname '  ' dfiles(k).name]);
        figure(77); clf;
        axs(1) = axes('Parent',77, 'Position', locs(:,1));
        PlotDensity([runname '/data/' dfiles(k).name], 'electron');
        colorbar;
        annotation('textbox', [0.4 0.94, 0.5, 0.05], 'String', ...
            sprintf('L_{step} = %i um; R_{step} = %1.1f; n_e = %1.2f\\times 10^{17} cm^{-3}',...
            Lstep, Rstep, rho0*1e-23), 'FontSize', 14, 'EdgeColor', 'none');
        axs(2) = axes('Parent', 77, 'Position', locs(:,2));
        Plotdist_fn([runname '/data/' dfiles(k).name], 'electron', 'x_px');
        colorbar;
        axs(3) = axes('Parent', 77, 'Position', locs(:,3));
        EVPlotIntensity(axs(3), [runname '/data/' dfiles(k).name], 'x_px');
        title('Intensity');
        colorbar;
        axs(4) = axes('Parent', 77, 'Position', locs(:,4));
        PlotField([runname '/data/' dfiles(k).name], 'Ex');
        colorbar;
        colormap(77, colmap.jet_white);
        
        setAllFonts(77,12); 
        set(axs(1), 'CLim', [0 6*rho0*1e-6/ncrit(800)]);
        set(axs(3), 'CLim', [0 7]);
        export_fig([runname '/plots/' dfiles(k).name(1:end-4)], '-nocrop', '-png', 77);
    end
    
end