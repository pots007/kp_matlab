simfolder = 'data/';
filen = dir([simfolder 'particles*.sdf']);
if (~exist('phasespace','dir'))
    mkdir('phasespace');
end
figure(8); set(8, 'Position', [1000 487 1189 751]);
locs = GetFigureArray(2,3,0.1,0.07, 'down');
axs = zeros(1,6);
ylims = [-40e-6 40e-6];
yplims = [-2 2];
yprimelims = [-0.1 0.1];
emittances = NaN(length(filen),4);
for i=97:97%(length(filen)-1)
    if (exist(sprintf('phasespace/%04i.png', i), 'file'))
        %continue;
    end
    figure(8); clf;
    fprintf('File %i\n', i);
    % Shell 7 stuff
    axs(1) = axes('Parent', 8, 'Position', locs(:,1)-[0 0 0.03 0]');
    datf = GetDataSDFchoose(sprintf('%s/normal%04i.sdf', simfolder,i), 'grid');
    x0 = datf.Grid.Grid.x;
    y0 = datf.Grid.Grid.y;
    datf = GetDataSDFchoose(sprintf('%s/normal%04i.sdf', simfolder,i), 'number_density/electron');
    ne0 = datf.Derived.Number_Density.electron.data'*1e-6;
    imagesc(x0,y0, ne0, 'Parent', axs(1));
    colormap(flipud(gray));
    freezeColors;
    axs(4) = axes('Parent', 8, 'Position', locs(:,4)-[0 0 0.03 0]');
    imagesc(x0,y0, ne0, 'Parent', axs(4));
    %set([axs(4) axs(1)], 'CLim', [0 0.3*max(ne0(:))]);
    freezeColors;
    
    %try
    specs = {'electron', 'shell7'};
    for k=1:1
        datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['grid/subset_highgamma/' specs{k}]);
        y = datp.Grid.Particles.subset_highgamma.(specs{k}).y*1e3;
        x = datp.Grid.Particles.subset_highgamma.(specs{k}).x;
        datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['px/subset_highgamma/' specs{k}]);
        px = datp.Particles.Px.subset_highgamma.(specs{k}).data;
        datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['py/subset_highgamma/' specs{k}]);
        py = datp.Particles.Py.subset_highgamma.(specs{k}).data;
        yprime = py./px*1e3;
        datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['gamma/subset_highgamma/' specs{k}]);
        gamma = datp.Particles.Gamma.subset_highgamma.(specs{k}).data;
        logm = gamma > 70;
        y = y(logm); x = x(logm); yprime = yprime(logm); gamma = gamma(logm); px = px(logm);
        e_rms = sqrt(mean(y(px~=0).^2)*mean(yprime(px~=0).^2) - mean(y(px~=0).*yprime(px~=0))^2);
        fprintf('%s rms emittance = %1.2e pi*mm*mrad\n', specs{k}, e_rms);
        %e_rms_n = e_rms*mean(sqrt(gamma(px~=0).^2-1)); %VERY SIMPLE, aka
        %wrong
        %e_rms_n = sqrt(mean(y(px~=0).^2)*mean(py(px~=0).^2) - mean(y(px~=0).*py(px~=0))^2);
        e_rms_n = sqrt(mean(y(px~=0).^2)*mean(gamma(px~=0).^2.*yprime(px~=0).^2) - mean(y(px~=0).*gamma(px~=0).*yprime(px~=0))^2);
        fprintf('%s  normalised rms emittance = %1.2f pi*mm*mrad\n', specs{k}, e_rms_n);
        emittances(i,:) = [e_rms abs(e_rms_n) mean(gamma(px~=0)) datp.time];
        
        axs(6+k) = axes('Parent', 8, 'Position', get(axs(1+(k-1)*3), 'Position'), 'XTick', [],...
            'YTick', [], 'Box', 'off', 'Color', 'none');
        logm = gamma > 80;
        yp = y(logm); xp = x(logm); yprimep = yprime(logm); gammap = gamma(logm);
        if (size(gammap,1)==0)
            continue;
        end
        logm = randi(size(gammap,1),5e4,1);
        yp = yp(logm); xp = xp(logm); yprimep = yprimep(logm); gammap = gammap(logm);
        hold on;
        scatter(axs(6+k), xp, yp, 5, gammap*0.511);
        colormap(jet);
        p0 = get(axs(1+(k-1)*3), 'Position');
        cb1 = colorbar('Peer', axs(7), 'Position', [p0(3)+p0(1)+0.01, p0(2), 0.01, p0(4)]);
        ylabel(cb1, '\gamma / MeVc^{-1}')
        hold off;
        linkaxes([axs(1+(k-1)*3) axs(6+k)], 'xy');
        set(axs(1), 'YLim', ylims)
        % Now phase space plots
        axs(2+(k-1)*3) = axes('Parent', 8, 'Position', locs(:, 2+(k-1)*3));
        scatter(axs(2+(k-1)*3), yp*1e6, yprimep, 5);
        set(axs(2+(k-1)*3), 'Xlim', yplims, 'YLim', yprimelims, 'Box', 'on');
        xlabel('y / um'); ylabel('p_y / p_x ');
        ybins = linspace(yplims(1), yplims(2), 500);
        yprimebins = linspace(yprimelims(1), yprimelims(2), 500);
        phasehist = hist2(yp*1e6, yprimep, ybins, yprimebins);
        axs(3+(k-1)*3) = axes('Parent', 8, 'Position', locs(:, 3+(k-1)*3));
        imagesc(ybins, yprimebins, phasehist', 'Parent', axs(3+(k-1)*3));
        set(axs(3+(k-1)*3), 'YDir', 'normal');
        xlabel('y / um'); ylabel('p_y / p_x ');

    end
    %catch
    %    disp(['Malformed dump no ' num2str(i) '!']);
    %end;
    set([axs(4) axs(1)], 'CLim', [0 0.3*max(ne0(:))], 'YLim', ylims);
    setAllFonts(8,20);
    set(8, 'Color', 'w');
    export_fig(sprintf('phasespace/%04i',i), '-nocrop', '-png', 8);
end

