Constants;
load divmap.mat; load invfire.mat;
fold = 'data';
plotgraphs = true;

filen = dir([fold '/*.sdf']);

if (~(exist('ne', 'dir')==7))
    mkdir('ne');
end
if (exist('ne/nedata.mat','file')==2)
    load ne/nedata.mat;
else
    transverse = cell(1000,10);
    ionexist=false;
end

for i=1:length(filen)
    if (sum(strcmp(transverse{i,1}, filen(i).name))==1)
        continue
    end
    transverse{i,1} = filen(i).name;
    datafl = GetBlocksSDF(['data/' filen(i).name]);
    datafids = cell(1, length(datafl));
    for k=1:length(datafl)
        datafids{k} = datafl(1,k).id;
    end
    %dataf = GetDataSDF([fold '/' filen(i).name]);
    % Electrons first
    dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
        'number_density/electron');
    n_e = dataf.Derived.Number_Density.electron.data';
    if(sum(strcmp(datafids, 'x_px/electron'))~=0)
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'grid/x_px/electron');
        px = dataf.Grid.x_px.electron.y;
        x_px.e.px = px./5.344e-22;
        if (max(x_px.e.px)>1e6)
            x_px.e.px = linspace(-1, 1, length(px))';
        end
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'x_px/electron');
        x_px.e.data = dataf.dist_fn.x_px.electron.data';
    end
    % Then Ionisation species. N2 first:
    if(sum(strcmp(datafids, 'x_px/shell6'))~=0)
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'grid/x_px/shell6');
        px = dataf.Grid.x_px.shell6.y;
        x_px.s6.px = px./5.344e-22;
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'x_px/shell6');
        x_px.s6.data = dataf.dist_fn.x_px.shell6.data';
        ionexist = 1;
    end
    if(sum(strcmp(datafids, 'x_px/shell7'))~=0)
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'grid/x_px/shell7');
        px = dataf.Grid.x_px.shell7.y;
        x_px.s7.px = px./5.344e-22;
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'x_px/shell7');
        x_px.s7.data = dataf.dist_fn.x_px.shell7.data';
        ionexist = 1;
    end
    if(sum(strcmp(datafids, 'number_density/shell6'))~=0)
        %try
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'number_density/shell6');
        n_s6 = dataf.Derived.Number_Density.shell6.data';
    end
    if(sum(strcmp(datafids, 'number_density/shell7'))~=0)
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'number_density/shell7');
        n_s7 = dataf.Derived.Number_Density.shell7.data';

    end
    %Now CO2 species
    if(sum(strcmp(datafids, 'x_px/shell5'))~=0)
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'grid/x_px/shell5');
        px = dataf.Grid.x_px.shell5.y;
        x_px.s5.px = px./5.344e-22;
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'x_px/shell5');
        x_px.s5.data = dataf.dist_fn.x_px.shell5.data';
        ionexist = 2;
    end
    if(sum(strcmp(datafids, 'number_density/shell5'))~=0)
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'number_density/shell5');
        n_s5 = dataf.Derived.Number_Density.shell5.data';
    end
    %Now for Ne, shells 7 and 8
    if(sum(strcmp(datafids, 'x_px/shell8'))~=0)
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'grid/x_px/shell8');
        px = dataf.Grid.x_px.shell8.y;
        x_px.s8.px = px./5.344e-22;
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'x_px/shell8');
        x_px.s8.data = dataf.dist_fn.x_px.shell8.data';
        ionexist = 3;
    end
    if(sum(strcmp(datafids, 'number_density/shell8'))~=0)
        dataf = GetDataSDFchoose([fold '/' filen(i).name], ...
            'number_density/shell8');
        n_s8 = dataf.Derived.Number_Density.shell8.data';
    end
    %The grid
    try
        grid = GetDataSDFchoose([fold '/' filen(i).name], 'grid');
        x = grid.Grid.Grid.x;
        transverse{i,2} = x(1:end-1);
    catch
        %lala
    end
    %transverse{i,3} = sum(n_e,1);
    transverse{i,3} = n_e(ceil(0.5*size(n_e,1)),:);
    if (strfind(filen(i).name, '0015') ~= 0)
        %n0 = mean(mean(n_e(10:ceil(0.05*size(n_e,1)),:)));
        n0 = mean(mean(n_e(floor([0.4 0.6]*size(n_e,1)),ceil(0.95*size(n_e,2)):end)));
    end
    transverse{i,4} = [sum(x_px.e.data,2) x_px.e.px];
    if (ionexist==1)
        %transverse{i,5} = sum(n_s6,1) + sum(n_s7,1);
        transverse{i,5} = n_s6(ceil(0.5*size(n_e,1)),:)+n_s7(ceil(0.5*size(n_e,1)),:);
        transverse{i,6} = [sum(x_px.s6.data,2) x_px.s6.px];
        transverse{i,7} = [sum(x_px.s7.data,2) x_px.s6.px];
    elseif (ionexist==2)
        %transverse{i,5} = sum(n_s5,1);
        transverse{i,5} = n_s5(ceil(0.5*size(n_e,1)),:);
        transverse{i,6} = [sum(x_px.s5.data,2) x_px.s5.px];
    elseif (ionexist==3)
        %transverse{i,5} = sum(n_s7,1) + sum(n_s8,1);
        transverse{i,5} = n_s7(ceil(0.5*size(n_e,1)),:)+n_s8(ceil(0.5*size(n_e,1)),:);
        transverse{i,6} = [sum(x_px.s7.data,2) x_px.s7.px];
        transverse{i,7} = [sum(x_px.s8.data,2) x_px.s8.px];
    end
    
    disp(['File ' filen(i).name]);
end
save('ne/nedata.mat', 'transverse', 'ionexist', 'n0');

i = sum(~cellfun(@isempty, transverse(:,1)));

if (plotgraphs)
    fsize = 14;
    figure(55)
    set(55, 'WindowStyle', 'docked');
    clf;
    %Fix for error in dist fn resolution
    [~,dirn,~] = fileparts(pwd);
    if (strcmp(dirn, 'He4e16a'))
        for z=1:i
            if (str2double(filen(z).name(1:4))<225 && str2double(filen(z).name(1:4))>150)
                transverse{z,6}(:,1) = transverse{z,6}(:,1)*2;
                transverse{z,7}(:,1) = transverse{z,7}(:,1)*2;
            end
        end
    end
    
    propa = zeros(1,i);
    n_es = zeros(length(transverse{1,2}),i);
    for m=1:i
        propa(m) = max(transverse{m,2})*1e3;
        n_es(:,m) = transverse{m,3};
    end
    if (ionexist)
        subplot(2,2,1)
    else
        subplot(1,2,1)
    end
    imagesc(propa, transverse{1,2}-max(transverse{1,2}), n_es./n0);
    set(gca, 'YDir', 'normal', 'FontSize', fsize, 'CLim', [0 10]);
    ylabel('\xi /mm', 'FontSize', fsize);
    xlabel('Propagation /mm', 'FontSize', fsize);
    title(['n_e of e^- vs propagation, n_0=' sprintf('%2.1f 10^{18}cm^{-3}', n0*1e-24)])
    cb = colorbar; set(get(cb, 'YLabel'), 'String', 'n_e/n_0');
    freezeColors;
    if (ionexist)
        subplot(2,2,2)
    else
        subplot(1,2,2)
    end
    %find index of largest px
    maxs = cellfun(@(x) max(x(:,2)), transverse(:,4), 'UniformOutput', false, 'Errorhandler', @(y,x) NaN);
    k=5;
    mx = maxs{5};
    mind = 5;
    while (~isnan(maxs{k}))
        if (maxs{k} > mx)
            mx = maxs{k};
            mind = k;
        end
        k=k+1;
    end
    px = transverse{mind,4}(:,2);
    px_e = zeros(length(px),i);
    for m=1:i
        px_e(:,m) = interp1(transverse{m,4}(:,2), transverse{m,4}(:,1), px);
    end
    imagesc(propa, px, (px_e)); 
    if (max(px)>10)
        set(gca, 'YLim', [10 max(px)], 'CLim', [0 0.5*max(max(px_e(px>10,:)))]); 
    else
        set(gca, 'CLim', [0 max(px_e(:))*1e-5]);
    end
    set(gca, 'YDir', 'normal', 'FontSize', fsize, 'CLim', [0 5e12]);
    ylabel('p_x /MeV', 'FontSize', fsize);
    xlabel('Propagation /mm', 'FontSize', fsize);
    title('p_x of e^- vs propagation')
    if (ionexist~=0)
        n_es = zeros(length(transverse{1,2}),i);
        for m=1:i
            propa(m) = max(transverse{m,2})*1e3;
            n_es(:,m) = transverse{m,5};
        end
        subplot(2,2,3)
        imagesc(propa, transverse{1,2}-max(transverse{1,2}), n_es);
        set(gca, 'YDir', 'normal', 'FontSize', fsize, 'CLim', [0 0.25*max(n_es(:))])
        ylabel('\xi /mm', 'FontSize', fsize);
        xlabel('Propagation /mm', 'FontSize', fsize);
        if (ionexist==1)
            title('n_e of shell6+7 vs propagation')
        elseif(ionexist==2)
            title('n_e of shell5 vs propagation')
        elseif(ionexist==3)
            titel('n_e of shell7+8 vs propagation');
        end

        %find index of largest px for shell 6
        maxs = cellfun(@(x) max(x(:,2)), transverse(:,6), 'UniformOutput', false, 'Errorhandler', @(y,x) NaN);
        k=5;
        mx = maxs{5};
        mind = 5;
        while (~isnan(maxs{k}))
            if (maxs{k} > mx)
                mx = maxs{k};
                mind = k;
            end
            k=k+1;
        end
        px6 = transverse{mind,6}(:,2);
        px_s6 = zeros(length(px),i);
        for m=3:i
            px_s6(:,m) = interp1(transverse{m,6}(:,2), transverse{m,6}(:,1), px6);
        end
        if (ionexist==1 || ionexist==3)
            %find index of largest px for shell 7
            maxs = cellfun(@(x) max(x(:,2)), transverse(:,7), 'UniformOutput', false, 'Errorhandler', @(y,x) NaN);
            k=5;
            mx = maxs{5};
            mind = 5;
            while (~isnan(maxs{k}))
                if (maxs{k} > mx)
                    mx = maxs{k};
                    mind = k;
                end
                k=k+1;
            end
            px7 = transverse{mind,7}(:,2);
            px_s7 = zeros(length(px),i);
            for m=3:i
                px_s7(:,m) = interp1(transverse{m,7}(:,2), transverse{m,7}(:,1), px7);
            end
            if (max(px6)>max(px7))
                for m=3:i
                    px_s7(:,m) = interp1(px7, px_s7(:,m), px6);
                end
                px_s = px6;
            else
                for m=3:i
                    px_s6(:,m) = interp1(px6, px_s6(:,m), px7);
                end
                px_s = px7;
            end
        end
        subplot(2,2,4)
        if(ionexist==1)
            imagesc(propa, px_s, px_s6+px_s7);
            set(gca, 'YDir', 'normal', 'FontSize', fsize, 'CLim', [0 1e12])
            title('p_x of shell6+7 vs propagation')
        elseif(ionexist==2)
            imagesc(propa, px6, px_s6);
            set(gca, 'YDir', 'normal', 'FontSize', fsize, 'CLim', [0 7e12])
            title('p_x of shell5 vs propagation')
        elseif(ionexist==3)
            imagesc(propa, px_s, px_s6+px_s7);
            set(gca, 'YDir', 'normal', 'FontSize', fsize, 'CLim', [0 1e12])
            title('p_x of shell7+8 vs propagation')
        end
        ylabel('p_x /MeV', 'FontSize', fsize);
        xlabel('Propagation /mm', 'FontSize', fsize);
    end
    setAllFonts(55,20);
    export_fig('ne/transverse.pdf', '-pdf',55);
end