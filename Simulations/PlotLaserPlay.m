cd('GuidedSuperGauss');
simtype = 'O4data';
files = dir([simtype '/*.sdf']);
figure(1)
set(1, 'WindowStyle', 'docked');
ma = zeros(1, 1563);
for i=1:length(files)
   dataf = GetDataSDF([ simtype '/' files(i).name]);
   x = dataf.Grid.Grid.x;
   x = x(1:length(x)-1);
   y = dataf.Grid.Grid.y;
   y = y(1:length(y)-1);
   time = dataf.time*1e15;
   Ex = dataf.Electric_Field.Ex.data';
   Ey = dataf.Electric_Field.Ey.data';
   ma = [ma; sum(abs(Ey'))];
   lineoutEx = Ex(1037, :);
   lineoutEy = Ey(937, :);
   envEx = abs(hilbert(lineoutEx));
   envEy = abs(hilbert(lineoutEy));
   subplot(2,3,1)
   imagesc(x, y, Ex)
   title(['Ex after ' num2str(time) ' fs']);
   colorbar;
   subplot(2,3,2)
   imagesc(x, y, Ey);
   title(['Ey after ' num2str(time) ' fs']);
   colorbar;
   subplot(2,3,3)
   %plot(x, lineoutEx, 'b', x, envEx, 'r');
   plot(y, sum(abs(Ey')));
   axis tight;
   title(['Sum of Ey after ' num2str(time) ' fs']);
   subplot(2,3,4)
   plot(x, lineoutEy, 'b', x, envEy, 'r');
   axis tight;
   title(['Ey after ' num2str(time) ' fs']);
   subplot(2,3,5)
   title('n_e');
   ne = dataf.Derived.Number_Density.electron.data';
   hne = plotnewithlaser(x, y, ne, Ey);
   set(gca, 'CLim', [0 1e24]);
   drawnow;
   saveas(1, [simtype(1:2) '_' files(i).name(1:4) '_' num2str(time) '.png']);
end
cd('..');