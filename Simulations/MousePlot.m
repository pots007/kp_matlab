mousef = '/Users/kristjanpoder/cpp/NLNoiseCode/data/';
sigma = [1343 5243 443 886];
locs = GetFigureArray(length(sigma),3,0.05,0.05, 'down');
figure(90); clf;
for i=1:length(sigma)
    fol = [mousef sprintf('mouse_sigma_%i/', sigma(i))];
    noisy = imread([fol 'denoised_mouse.png']);
    %la(:,:,i) = noisy;
    weigh = imread([fol 'weights_mouse.png']);
    mous = imread([fol 'mouse.png']);
    ax((i-1)*3+1) = axes('Position', locs(:,(i-1)*3+1));
    imagesc(noisy); title(['\sigma = ' num2str(sigma(i))]);
    ax((i-1)*3+2) = axes('Position', locs(:,(i-1)*3+2));
    imagesc(weigh);
    ax((i-1)*3+3) = axes('Position', locs(:,(i-1)*3+3));
    imagesc(mous-noisy);
end
linkaxes(ax, 'xy');
set(ax, 'CLim', [0 10000]);
colormap(gray);
