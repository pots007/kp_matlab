%Plots the electric field along with temporal phase
%Accepts both epoch style inputs with real values
%and complex style input
%
%just field: plota0withphase(Efield)
%
%with x axis: plota0withphase(Efield, x)
%
%with x axis and frequency: plota0withphase(Efield, x, omega0)

function himage = plota0withphase(varargin)
if(nargin==0)
    disp('Give me something, bro!');
    return
end
Constants;
%just the field wo coordinates

E = varargin{1};
if(nargin ==1)
    Egrid = 1:length(E);
end
if(nargin == 2)
    Egrid = varargin{2};
end
if (~isreal(E))
    Ehil = E;
else
    Ehil = hilbert(E);
end
Ephase = unwrap(angle(Ehil));
[maxval maxi] = max(abs(Ehil));
zerophase = Ephase(maxi);
logmat = (abs(Ehil)<0.1*max(abs(Ehil)));
Ephase(logmat) = nan;
Ephase = Ephase - zerophase;
if (nargin == 3)
    Egrid = varargin{2};
    t = Egrid./c;
    t = t-t(maxi);
    Ephase = Ephase - varargin{3}.*t;
end
a0 = qe*abs(Ehil)./(me*c*2*pi*c/8e-7);
[AX H1 H2] = plotyy(Egrid, a0, Egrid, Ephase);
%axes(AX(1)); axis tight;
%axes(AX(2)); axis tight;
set(get(AX(1),'Ylabel'), 'String', 'a_0');
set(get(AX(2),'Ylabel'), 'String', 'Phase (rad)');
set(AX(1), 'XLim', [min(Egrid) max(Egrid)]);
set(AX(2), 'XLim', [min(Egrid) max(Egrid)]);
himage = AX;

end