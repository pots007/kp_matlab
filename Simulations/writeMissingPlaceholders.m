% Rename files to a sequential order
% Assumes all have file stem normal

writefiles = 1;
datfol = 'data';
flist_mat = dir(fullfile(datfol, 'normal*mat')); % Use thiis for Matlab splits
%flist_mat = dir(fullfile(datfol, 'normal*sdfi')); % Use this for sdf splits
flist_sdf = dir(fullfile(datfol, 'normal*sdf'));
% Hacky, but all files have a number from elements 7:10...
flist_mat_ = {flist_mat.name}';
fstems = cellfun(@(x) x(1:10), flist_mat_, 'UniformOutput', 0);
fstems_mat_uniq = unique(fstems);
flist_sdf_ = {flist_sdf.name}';
fstems = cellfun(@(x) x(1:10), flist_sdf_, 'UniformOutput', 0);
fstems_sdf_uniq = unique(fstems);

for i=1:length(fstems_mat_uniq)
    sdf_fname = fullfile(datfol, [fstems_mat_uniq{i} '.sdf']);
    %isthere = exist(sdf_name, 'file');
    isthere = dir(sdf_fname);
    fprintf('%15s <-> %15s\n', fstems_mat_uniq{i}, [' ' isthere.name]);
    if isempty(isthere) && writefiles
        fprintf('Wrote file %s\n', sdf_fname);
        fid = fopen(sdf_fname, 'w');
        fprintf(fid, 'This is a placeholder\n');
        fclose(fid);
    end
end
