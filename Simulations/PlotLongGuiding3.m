load divmap.mat;
simtype = 'data';
files = dir([simtype '/*.sdf']);
figure(1)
set(1, 'WindowStyle', 'docked');
if (~(exist('plots', 'dir')==7))
    mkdir('plots');
end
plotall = false;
for i=1:length(files)
    if plotall
        %Plot all
    else
        if (exist(['plots/' files(i).name(1:4) '.png'], 'file'))
            continue;
        end
    end
    disp(['File ' files(i).name]);
    datafl = GetBlocksSDF(['data/' files(i).name]);
    datafids = cell(1, length(datafl));
    for k=1:length(datafl)
        datafids{k} = datafl(1,k).id;
    end
    %dataf = GetDataSDF([ simtype '/' files(i).name]);
    dataf = GetDataSDFchoose([simtype '/' files(i).name], 'grid');
    x = dataf.Grid.Grid.x;
    x = x(1:length(x)-1);
    y = dataf.Grid.Grid.y;
    y = y(1:length(y)-1);
    time = dataf.time*1e15;
    dataf = GetDataSDFchoose([simtype '/' files(i).name], 'ex');
    Ex = dataf.Electric_Field.Ex.data';
    dataf = GetDataSDFchoose([simtype '/' files(i).name], 'ey');
    Ey = dataf.Electric_Field.Ey.data';
    
    lineoutEx = Ex(floor(length(y)*0.5), :);
    lineoutEy = Ey(floor(0.5*length(y)), :);
    
    subplot(2,3,1)
    imagesc(x, y, Ex)
    title(['Ex after ' num2str(time) ' fs']);
    colormap(divmap);
    %set(gca, 'CLim', [-3e10 3e10]);
    %colorbar;
    subplot(2,3,2)
    imagesc(x, y, Ey);
    title(['Ey after ' num2str(time) ' fs']);
    %set(gca, 'CLim', [-2e13 2e13]);
    %colorbar;
    subplot(2,3,3)
    %plot(x, lineoutEx, 'b', x, envEx, 'r');
    %plot(y, sum(abs(Ey')));
    plota0withphase(lineoutEy, x', 2*pi*3e8/8e-7);
    %axis tight;
    title(['Ey after ' num2str(time) ' fs']);
    subplot(2,3,4)
    plot(x, lineoutEx, 'b');%, x, envEy, 'r');
    axis tight;
    title(['Ex after ' num2str(time) ' fs']);
    subplot(2,3,5)
    title('n_e');
    dataf = GetDataSDFchoose([simtype '/' files(i).name],...
        'number_density/electron');
    ne = dataf.Derived.Number_Density.electron.data';
    hne = plotnewithlaser(x, y, ne, Ey);
    %colormap(jet)
    %set(gca, 'CLim', [0 1e25]);
    title('n_e');
    subplot(2,3,6)
    cla;
    if(sum(strcmp(datafids, 'number_density/shell6'))~=0)
        dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
            'number_density/shell6');
        He = dataf.Derived.Number_Density.shell6.data';
        hHee = plotnewithlaser(x, y, He, Ey);
        %set(gca, 'CLim', [0 9e23]);
        title('6th shell N electrons density');
    else if(sum(strcmp(datafids, 'x_px/electron'))~=0)
            dataf = GetDataSDF([simtype '/' files(i).name]);%, ...
            %'x_px/electron');
            x = dataf.dist_fn.x_px.electron.grid.x;
            px = dataf.dist_fn.x_px.electron.grid.y;
            px = px./5.344e-22; %Get to MeV/c
            data = dataf.dist_fn.x_px.electron.data';
            imagesc(x, px, (data));
            set(gca, 'YDir', 'normal');
            set(gca, 'CLim', [0 1e10]);
            ylabel('p_x (MeV/c)');
        end
    end
    %drawnow;
    %saveas(1, ['plots/' files(i).name(1:4) '.png']);
    export_fig(['plots/' files(i).name(1:4) '.png'], '-png',1);
end
