% makeSimMovieFFMPEG.m

% This acts as a wrapper to call ffmpeg and make nice simulation movies!

% makeSimMovieFFMPEG(fnamepattern, outname, dump_im_rate)

% Inputs:   fnamepattern - in the ffmpeg format!! eg, normal%04d.png 
%           outname - name of produced mp4 file. If extension omitted,
%                       automatically added
%           dump_im_rate - number of simulation dumps to show, per second.
%                           Defaults to 10 dumps per second

function makeSimMovieFFMPEG(ffmpeg_path, fnamepattern, outname, dump_im_rate)

if nargin<4
    dump_im_rate = 10;
    if nargin < 3
        outname = 'out';
    end
end

[~,froot,exten] = fileparts(outname);
if isempty(exten); exten = 'mp4'; end;

cmd = [fullfile(ffmpeg_path, 'ffmpeg') ' -framerate ' num2str(dump_im_rate) ,...
    ' -i ' fnamepattern ' -c:v libx264 -pix_fmt yuv420p -r 30 ' froot '.' exten];
system(cmd);