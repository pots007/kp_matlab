function DrishtiWriteUcharVolume(A, filename, Amin, Amax)

if (nargin < 4)
    Amax = max(A(:));
    Amin = min(A(:));
end

A(A > Amax) = Amax;
A(A < Amin) = Amin;
A = A - Amin;
A = A*255/(Amax - Amin);

rawfile = [filename '.pvl.nc.001'];
xmlfile = [filename '.pvl.nc'];

NX = length(A(:,1,1));
NY = length(A(1,:,1));
NZ = length(A(1,1,:));

fid = fopen(rawfile, 'w');

firstByte = 0;

fwrite(fid, firstByte, 'uint8');
fwrite(fid, NX, 'uint32');
fwrite(fid, NY, 'uint32');
fwrite(fid, NZ, 'uint32');

h = waitbar(0, 'Writing pvl.nc file');

for ii = 1:NX
    for jj = 1:NY
            fwrite(fid, A(ii,jj,:), 'uchar');
    end
    waitbar(ii/NX, h)
end

close(h)

fclose(fid);

fid = fopen(xmlfile, 'w');

str =      '<!DOCTYPE Drishti_Header>\n';
str = [str '<PvlDotNcFileHeader>\n'];
str = [str '  <rawfile></rawfile>\n'];
str = [str '  <voxeltype>unsigned short</voxeltype>\n']; 
str = [str '  <pvlvoxeltype>unsigned char</pvlvoxeltype>\n'];
str = [str '  <gridsize>' num2str(NX) ' ' num2str(NY) ' ' num2str(NZ) '</gridsize>\n'];
str = [str '  <voxelunit>micron</voxelunit>\n'];
str = [str '  <voxelsize>1 1 1</voxelsize>\n'];
str = [str '  <description></description>\n'];
str = [str '  <slabsize>' num2str(NX+1) '</slabsize>\n'];
str = [str '  <rawmap>0 255.0 </rawmap>\n'];
str = [str '  <pvlmap>0 255 </pvlmap>\n'];
str = [str '</PvlDotNcFileHeader>\n'];

fprintf(fid, str);
fclose(fid);

end