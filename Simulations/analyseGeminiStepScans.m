%% Looks through folders and builds up a collactive database
datafol = '/Users/kristjanpoder/epochdata/Gem2015scans/';
Rstep = [1.5 2 3 4];
rho0 = (2.5:2.5:12.5)*1e17;
Lstep = [100 200 400 600];
dbase = cell(80, 6);
it = 1;
for i=1:length(rho0)
    for j=1:length(Rstep)
        for k=1:length(Lstep)
            foln = sprintf('%sGem2015%s%s%s/data/', ...
                datafol, char(97+j), char(97+i), char(97+k))
            dbase{it,1} = rho0(i);
            dbase{it,2} = Rstep(j);
            dbase{it,3} = Lstep(k);
            dbase{it,4} = foln;
            % Col 5 is number of macroparticles in highgamma subspecies
            dataf = GetDataSDF([foln 'particles0028.sdf']);
            try
                dbase{it,5} = size(dataf.Particles.Px.subset_highgamma.electron.data,1);
            catch
                dbase{it,5} = 0;
            end
            % Col is integral of px_x of energies above 5 MeV
            dataf = GetDataSDFchoose([foln 'normal0028.sdf'], 'grid/x_px/electron');
            x = dataf.Grid.x_px.electron.x;
            x = x(1:length(x)-1)*1e3; % in mm
            y = dataf.Grid.x_px.electron.y;
            y = y./5.344e-22;
            dataf = GetDataSDFchoose([foln 'normal0028.sdf'], 'x_px/electron');
            x_px = dataf.dist_fn.x_px.electron.data';
            logm = y>5;
            dbase{it,6} = sum(sum(x_px(logm,:)));
            it = it+1;
        end
    end
end

%% Now attempts at plotting
datafol = '/Users/kristjanpoder/epochdata/Gem2015scans/';
figure(77); set(77, 'Position', [500 500 900 700], 'Color', 'w'); clf;
locs = GetFigureArray(2, 2, 0.07, 0.05, 'down');
axs = zeros(1,4);
load('owncolourmaps');
plotdata = zeros(80,5);
for i=1:80
    plotdata(i,1) = dbase{i,1};
    plotdata(i,2) = dbase{i,2};
    plotdata(i,3) = dbase{i,3};
    plotdata(i,4) = dbase{i,5};
    plotdata(i,5) = dbase{i,6};
end
plotopt = 4;
switch plotopt
    case 1
        scatter3(plotdata(:,1), plotdata(:,2), plotdata(:,3), 55, plotdata(:,5), 'filled');
        colorbar;
    case 2
        for i=1:4
            logm = plotdata(:,2) == Rstep(i);
            tempdat = plotdata(logm,:);
            axs(i) = axes('Parent', 77, 'Position', locs(:,i));
            scatter(tempdat(:,1), tempdat(:,3), 45, tempdat(:,5), 'filled');
            colorbar;
        end
    case 3
        tempdata = [];
        markers = {'o', 'x', '*', 'd'};
        axs(1) = axes('Parent', 77, 'NextPlot', 'add', 'Box', 'on');
        for i=1:4
            logm = plotdata(:,2) == Rstep(i);
            tempdat = plotdata(logm,:);
            tempdat(:,1) = tempdat(:,1) + (-1)^i*(i>2)*0.075e17;
            tempdat(:,3) = tempdat(:,3) + (-1)^i*(i<3)*7;
            hs(i) = scatter(tempdat(:,1)*1e-17, tempdat(:,3), 65, tempdat(:,5), markers{i}, 'LineWidth', 2);
            set(axs(1), 'YScale', 'log', 'XScale', 'lin');
            set(axs(1), 'YTick', Lstep, 'YLim', [90 650]);
            set(axs(1), 'XTick', rho0*1e-17, 'XLim', [2 13]);
            xlabel('Plasma density / 10^{17} cm^{-3}');
            ylabel('Density step length / \mu m');
        end
        hleg = legend(hs, {'n_H/n_L = 1.5', 'n_H/n_L = 2', 'n_H/n_L = 3', 'n_H/n_L = 4'},...
            'Location', 'NorthOutside', 'Orientation', 'Horizontal');
        cb = colorbar;
        ylabel(cb, 'Injected charge / a.u.');
        setAllFonts(77, 24);
        export_fig([datafol 'Results'], '-nocrop', '-pdf', 77);
    case 4
        clf;
        locs = GetFigureArray(2,2, 0.1, 0.07, 'down');
        xlims = [2 13];
        ylims = [90 650];
        for i=1:4
            logm = plotdata(:,2) == Rstep(i);
            tempdat = plotdata(logm,:);
            axs(i) = axes('Parent', 77, 'NextPlot', 'add', 'Box', 'on', 'Position', locs(:,i));
            %scatter(tempdat(:,1)*1e-17, tempdat(:,3), 65, tempdat(:,5), 'd', 'LineWidth', 2);
            %xq = reshape(repmat(linspace(xlims(1), xlims(2), 100), 100,1),1,10000);
            %yq = repmat(linspace(ylims(1), ylims(2), 100), 1,100);
            %vq = griddata(tempdat(:,1)*1e-17, tempdat(:,3), tempdat(:,5), xq, yq, 'v4');
            
            %imagesc(linspace(xlims(1), xlims(2), 100), linspace(ylims(1), ylims(2), 100),...
                %reshape(vq, 100,100));
            scatter(tempdat(:,1)*1e-17, tempdat(:,3), 85, tempdat(:,5), 'd', 'LineWidth', 7);
            set(axs(i), 'YScale', 'log', 'XScale', 'lin', 'YDir', 'normal');
            set(axs(i), 'YTick', Lstep, 'YLim', ylims);
            set(axs(i), 'XTick', rho0*1e-17, 'XLim', xlims);
            %xlabel('Plasma density / 10^{17} cm^{-3}');
            %ylabel('Density step length / \mu m');
            title(sprintf('Step ratio %1.1f', Rstep(i)));
        end
        set(axs(3:4), 'YTickLabel', []);
        set(axs([1 3]), 'XTickLabel', []);
        xlabel(axs(2),'Plasma density / 10^{17} cm^{-3}');
        xlabel(axs(4),'Plasma density / 10^{17} cm^{-3}');
        ylabel(axs(1), 'Density step length / \mu m');
        ylabel(axs(2), 'Density step length / \mu m');
        
        %hleg = legend(hs, {'n_H/n_L = 1.5', 'n_H/n_L = 2', 'n_H/n_L = 3', 'n_H/n_L = 4'},...
         %   'Location', 'NorthOutside', 'Orientation', 'Horizontal');
        cb = colorbar;
        set(cb, 'Position', [0.91 0.10 0.01 0.8])
        ylabel(cb, 'Injected charge / a.u.');
        setAllFonts(77, 24);
        export_fig([datafol 'Results2x2'], '-nocrop', '-pdf', 77);
end

