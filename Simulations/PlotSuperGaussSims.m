clear;
SG6 = 'LaserPlay8';
SG4 = 'LaserPlay10';
SG2 = 'LaserPlay9';
SG8 = 'LaserPlay11';
filn = 0:50;
%figure(1)
%set(1, 'WindowStyle', 'docked');
for i=1:length(filn)
   root = ['/data/' sprintf('%04d', filn(i)) '.sdf'];
   SG2f = GetDataSDF([SG2 root]);
   SG4f = GetDataSDF([SG4 root]);
   SG6f = GetDataSDF([SG6 root]);
   SG8f = GetDataSDF([SG8 root]);
   x = SG2f.Grid.Grid.x;
   x = x(1:length(x)-1);
   y = SG2f.Grid.Grid.y;
   y = y(1:length(y)-1);
   time = SG2f.time*1e15;
   subplot(2,2,1)
   imagesc(x, y, SG2f.Electric_Field.Ey.data');
   title(['E_y of Gaussian of order 2 after ' num2str(time) ' fs']);
   colorbar;
   subplot(2,2,2)
   imagesc(x, y, SG4f.Electric_Field.Ey.data');
   title(['E_y of Gaussian of order 4 after ' num2str(time) ' fs']);
   colorbar;
   subplot(2,2,3)
   imagesc(x, y, SG6f.Electric_Field.Ey.data');
   title(['E_y of Gaussian of order 6 after ' num2str(time) ' fs']);
   colorbar;
   subplot(2,2,4)
   h2 = plot(y, sum(abs(SG2f.Electric_Field.Ey.data)));
   hold on;
   h4 = plot(y, sum(abs(SG4f.Electric_Field.Ey.data)), 'r');
   h6 = plot(y, sum(abs(SG6f.Electric_Field.Ey.data)), 'g');
   h8 = plot(y, sum(abs(SG8f.Electric_Field.Ey.data)), 'k');
   legend([h2 h4 h6 h8], 'O 2', 'O 4', 'O 6', 'O 8');
   set(gca, 'XLim', [-2e-5 2e-5]);
   title(['E_y transverse sums ' num2str(time) ' fs']);
   hold off;
   drawnow;   
   saveas(1, ['SuperGaussPlots/' num2str(time) '.png']);
end