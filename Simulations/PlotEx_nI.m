simfolder = 'data/';
filen = dir([simfolder 'normal*.sdf']);
if (~exist('Ex+nI','dir'))
    mkdir('Ex+nI');
end
figure(8); set(8, 'Position', [1000 587 1189 751], 'Color', 'w');
load('divmap.mat')
for i=1:length(filen)
    if (exist(['Ex+nI/' filen(i).name(1:end-4) '.png'], 'file'))
        %continue;
    end
    disp(['File ' filen(i).name]);
    %dataf = GetDataSDF('data/0020.sdf');
    dataf = GetDataSDF([simfolder filen(i).name]);
    clf;
    x = 1e3*dataf.Grid.Grid.x;
    y = 1e6*dataf. Grid.Grid.y;
    try
        CO2 = 1e-6*(dataf.Derived.Number_Density.shell7.data'+dataf.Derived.Number_Density.shell6.data');
        Ex = dataf.Electric_Field.Ex.data';
        %ne = 1e-6*dataf.Derived.Number_Density.electron.data';
    catch
        continue;
    end
    cmap2 = [divmap; flipud(gray(256))];
    maxEx = 0.5*max([abs(min(Ex(:))) abs(max(Ex(:)))]);
    Ex1 = 0.5*Ex./maxEx + 0.5; Ex1(Ex1<0) = 0; Ex1(Ex1>1) = 1;
    %maxne = max(ne(:)); nemax = 3;
    %ne1 = nemax*ne./maxne; ne1(ne1>1)=1;
    CO2max = max(CO2(:));
    CO21 = CO2./CO2max+1;
    CO21(CO21<1.15)=0;
    h1 = imagesc(x,y,CO21+Ex1);
    set(gca, 'CLim', [0 2])
    xlabel('x /mm'); ylabel('y /\mum');
    colormap(cmap2);
    set(gca, 'Position', get(gca,'Position')+[0 0 -0.2 0]);
    hb1 = colorbar;
    set(hb1, 'Position', get(hb1,'Position')+[0.2 0 0 0], 'YLim', [1 2],...
        'YTick', 1:0.2:2);
    hb2 = colorbar;
    set(hb2, 'Position', get(hb2,'Position')+[0.1 0 0 0], 'YLim', [0 1],...
        'YTick', 0:0.2:1);
    ticks = get([hb1;hb2], 'YTick');
    ticklabels = cell(2, length(ticks{1}));
    for k=1:length(ticks{1})
        ticklabels{1,k} = sprintf('%1.1f', (ticks{1}(k)-1)*CO2max*1e-17);
        ticklabels{2,k} = sprintf('%1.1f', (ticks{2}(k)*2*maxEx - maxEx)*1e-9);
    end
    set(hb1, 'YTickLabel', ticklabels(1,:));
    set(hb2, 'YTickLabel', ticklabels(2,:));
    set(get(hb1, 'YLabel'),'String', 'shell5 density /10 ^{17} cm^{-3}');
    set(get(hb2, 'YLabel'),'String', 'E_x (GV/m)');
    set(gca, 'Position', get(gca, 'Position')+[-0.05 0 0.1 0]);
    %title();
    setAllFonts(8,20);
    export_fig(['Ex+nI/' filen(i).name(1:end-4)], '-nocrop', '-png', 8);
end