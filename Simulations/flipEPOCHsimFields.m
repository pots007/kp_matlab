% Reverse EPOCH dumps, and write them into the correct place to run a
% simulation!

infol = '/Users/kristjanpoder/epochdata/directionplay/cx1run/';
outfol = '/Volumes/cx2work/SI_3D4/data/';
fstem = 'normal0010.sdf';
fields = {'Ex', 'Ey', 'Ez', 'Bx', 'By', 'Bz'};
for i=1:6
    try 
        dataf = GetDataSDFchoose(fullfile(infol, fstem), fields{i});
    catch
        continue;
    end
    ff = fieldnames(dataf);
    dat = dataf.(ff{end}).(fields{i}).data;
    dat = -flip(dat,1);
    fid = fopen(fullfile(outfol, [fields{i} '.dat']), 'W');
    fwrite(fid, dat, 'double');
    fclose(fid);
    fprintf('Finished writing %s \n', fields{i});
end