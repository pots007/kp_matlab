files = dir('data/*.sdf');
if (~(exist('distfn', 'dir')==7))
    mkdir('distfn');
end
species = 'shell5';
figure(9)
cla;
set(9, 'Position', [100 100 1200 900]);
for i=1:length(files)
    if (exist(['distfn/' species files(i).name(1:4) '.png'], 'file'))
        continue;
    end
    clf;
    ax1 = axes('Units', 'normalized','Position', [0.43 0.43 0.55 0.55]);
    try
        h1 = Plotdist_fn(['data/' files(i).name], species, 'py_px');
    catch
        continue;
    end
    set(h1, 'CData', (get(h1, 'CData')));
    set(ax1, 'YDir', 'normal', 'YTick', [], 'XTick', []);
    set(get(ax1, 'Title'), 'String', []);
    ax2 = axes('Units', 'normalized','Position', [0.12 0.43 0.3 0.55]);
    Plotdist_fn(['data/' files(i).name], species, 'x_py');
    set(ax2, 'XDir', 'normal', 'XTick', []);
    set(get(ax2, 'Title'), 'String', []);
    ylabel('p_y / Mevc^{-1}');
    xlabel('x / mm');
    ax3 = axes('Units', 'normalized','Position', [0.43 0.12 0.55 0.3]);
    h3 = Plotdist_fn(['data/' files(i).name], species, 'x_px');
    set(get(ax3, 'Title'), 'String', []);
    set(h3, 'CData', get(h3, 'CData')', 'XData', get(h3, 'YData'), 'YData', get(h3, 'XData'));
    set(ax3, 'XLim', [min(get(h3, 'XData')) max(get(h3, 'XData'))],...
        'YLim', [min(get(h3, 'YData')) max(get(h3, 'YData'))], 'YTick', []);
    xlabel('p_x / Mevc^{-1}');
    ylabel('x / mm');
    xlims = linspace(min(get(h3, 'YData'))+0.01, max(get(h3, 'YData'))-0.02, 4)';
    set(ax3, 'YTick', xlims, 'YTickLabel', num2str(xlims, 3));
    set(ax2, 'XTick', xlims, 'XTickLabel', num2str(xlims, 3));
    dataf = GetDataSDFchoose(['data/' files(i).name], 'grid');
    tim = dataf.time*1e12;
    a1 = annotation('textbox', 'Position', [0.1 0.1 0.15 0.2], 'String', ...
        sprintf('dist fns for %s at t=%1.1f ps', species, tim), 'FontSize', 20, ...
        'LineStyle', 'none');
    setAllFonts(9, 20);
    drawnow;
    export_fig(['distfn/' species files(i).name(1:4)], '-transparent', '-nocrop', '-pdf', 9);
end