xmin = -50e-6;
xmax = 50e-6;
ymin = -60e-6;
lambda0 = 0.8e-6; k = 2*pi/lambda0;
nx = floor(30/0.8e-6*(xmax-xmin));
ny = floor(4/0.8*abs(2*ymin*1e6));
xax = linspace(xmin, xmax, nx); dx = xax(2)-xax(1);
yax = linspace(ymin, -ymin, ny); dy = yax(2)-yax(1);
x = repmat(xax, ny,1);
y = repmat(yax', 1, nx);
xfoc = 510e-6;
wai = 5.92e-6;
rayleigh_range = pi*wai^2/lambda0;
wz = wai * sqrt(1+((x-xfoc)./rayleigh_range).^2);
radius_of_curv = (x-xfoc) .* (1.0+(rayleigh_range./(x-xfoc)).^2);
Ey_spatial = wai./wz.*exp(-((y./wz).^2));
Ez = - sqrt(wai./wz).*exp(-(((y+0.5*dy)./wz).^2)).*sin(k.*x).*exp(-x.^2./5e-6^2);
Ez = - Ez_spat.*sin(k.*x).*exp(-x.^2./6e-6^2);
wz = wai * sqrt(1+(((x+0.5*dx)-xfoc)./rayleigh_range).^2);
Bx = -2*y./(k*wz.^2).*Ez_spat.*cos(k.*(x+0.5*dx)).*exp(-(x+0.5*dx).^2./5e-6^2)./c;
imagesc(xax,yax,Ez)

By = Ez_spat.*sin(k.*(x+0.5*dx)).*exp(-(x+0.5*dx).^2./5e-6^2)./c;
%%
fol = 'E:\Kristjan\Google Drive\epoch\fieldplay\data1';
if (~exist(fol, 'dir'))
    mkdir(fol)
end
fid1 = fopen([fol '/Ez.dat'], 'w');
fwrite(fid1, 1e13*Ez', 'double');
fclose(fid1);
fid2 = fopen([fol '/By.dat'], 'w');
fwrite(fid2, 1e13*By', 'double');
fclose(fid2);
fid3 = fopen([fol '/Bx.dat'], 'w');
fwrite(fid3, 1e13*Bx', 'double');
fclose(fid3);