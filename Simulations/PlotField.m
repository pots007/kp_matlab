%Plot field of one datafile

function him = plotField(datafile, field)

dataf = GetDataSDFchoose(datafile, 'grid');
x = dataf.Grid.Grid.x;
x = x(1:length(x)-1)*1e3;
xt = SetSmartUnit(x);
dims = 1;
if isfield(dataf.Grid.Grid, 'y')
    y = dataf.Grid.Grid.y;
    y = y(1:length(y)-1)*1e3;
    yt = SetSmartUnit(y);
    dims = 2;
end
if isfield(dataf.Grid.Grid, 'z')
    z = dataf.Grid.Grid.z;
    z = y(1:length(z)-1)*1e3;
    zt = SetSmartUnit(z);
    dims = 3;
end

time = dataf.time*1e15;
dataf = GetDataSDFchoose(datafile, lower(field));
if strcmpi('E', field(1))
    fiel = dataf.Electric_Field.(field).data;
elseif (strcmp('B', upper(field(1))))
    fiel = dataf.Magnetic_Field.(field).data;
else
    disp('No such field')
    return;
end
if dims==1
    plot(xt.t, fiel);
elseif dims==2
    imagesc(xt.t,yt.t,fiel');
elseif dims==3
    imagesc(xt.t, yt.t, fiel(:,:,round(0.5*size(fiel,3)))');
end
title([field ' at t=' num2str(time*1e-3) ' ps']);

