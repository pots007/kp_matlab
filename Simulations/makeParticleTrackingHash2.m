% makeAccHastable
%
% This function will make a hashmap of all particles as they accelerate.
% The key is ID, the fields are x,y,z, px,py,pz, gamma.

trackItem = struct;
%trackItem.ID = [];
trackItem.y = [];
trackItem.z = [];
trackItem.px = [];
trackItem.py = [];
trackItem.pz = [];
trackItem.gamma = [];
trackItem.time = [];

filen = dir('data/particles*.sdf');
IDs = [];
% Compare the sizes of one map for all variables and one for each.
trackMap = containers.Map('KeyType', 'int64', 'ValueType', 'any');
trackMapx = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapy = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapz = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapPx = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapPy = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapPz = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapGamma = containers.Map('KeyType', 'int64', 'ValueType', 'single');
%

for i=25%:length(filen)
    tic;
    fprintf('File %3i\n', i);
    dataf = GetDataSDF(fullfile('data', filen(i).name));
    if ~isfield(dataf, 'Particles')
        fprintf('\t No data in file %3i\n', i);
        continue;
    end
    IDs = dataf.Particles.ID.subset_highgamma.electron.data;
    for p = 1:length(IDs)
        temp = trackItem;
        %temp = dataf.Particles.ID.subset_highgamma.electron.grid.x(p);
        temp.x = dataf.Particles.ID.subset_highgamma.electron.grid.x(p);
        temp.y = dataf.Particles.ID.subset_highgamma.electron.grid.y(p);
        temp.z = dataf.Particles.ID.subset_highgamma.electron.grid.z(p);
        temp.px = dataf.Particles.Px.subset_highgamma.electron.data(p);
        temp.py = dataf.Particles.Py.subset_highgamma.electron.data(p);
        temp.pz = dataf.Particles.Pz.subset_highgamma.electron.data(p);
        temp.gamma = dataf.Particles.Gamma.subset_highgamma.electron.data(p);
        temp.time = dataf.time;
        trackMap(IDs(p)) = temp;
        trackMapx(IDs(p)) = dataf.Particles.ID.subset_highgamma.electron.grid.x(p);
        trackMapy(IDs(p)) = dataf.Particles.ID.subset_highgamma.electron.grid.y(p);
        trackMapz(IDs(p)) = dataf.Particles.ID.subset_highgamma.electron.grid.z(p);
        trackMapPx(IDs(p)) = dataf.Particles.Px.subset_highgamma.electron.data(p);
        trackMapPy(IDs(p)) = dataf.Particles.Py.subset_highgamma.electron.data(p);
        trackMapPz(IDs(p)) = dataf.Particles.Pz.subset_highgamma.electron.data(p);
        trackMapGamma(IDs(p)) = dataf.Particles.Gamma.subset_highgamma.electron.data(p);
        
    end
    fprintf('\t\t Done with file %3i in %2.3f\n', i, toc);
end

%save('test', 'trackMap')