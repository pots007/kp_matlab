function [reconstructedDens,reconstructedTemp,reconstructedVely,xaxis,yaxis,time, gridrect] = open_FLASHv1(filename,resolution)
%filename = Image_Path;

if nargin < 2,
    if nargin < 1,
        display('No filename specified')
        display('Format open_FLASH(filename,resolution)');
    end
    display('no resolution specified, estimating original resolution')
    adjust_reso = 1;
    
else
    adjust_reso = 0;
end


coords=h5read(filename,'/coordinates');  %these h5 commands only come with matlab 2011 and later

%%%%%%%%%%%%%%%%%% Start variable block
dens=h5read(filename,'/dens');
temp=h5read(filename,'/temp');
vely = h5read(filename,'/vely');
%%%%%%%%%%%%%%%%%% End variable block

block_size=h5read(filename,'/block size');
gid=h5read(filename,'/gid');   %grid id ; see the flash manual for more information
p=h5read(filename,'/real scalars');
pvalues = cellstr(p.name');

time = p.value(strcmp('time',pvalues) == 1);  %this is the time in seconds
%p=h5read(filename,'/real runtime parameters');

gridrect = zeros(4,size(coords,2));
for i=1:size(coords,2)
    gridrect(1,i) = coords(1,i) - 0.5*block_size(1,i);
    gridrect(2,i) = coords(2,i) - 0.5*block_size(2,i);
    gridrect(3,i) = block_size(1,i);
    gridrect(4,i) = block_size(2,i);
    %rectangle('Position', [x0 y0 block_size(1,i) block_size(2,i)]);
end

block_num = size(block_size,2); %this is the number of blocks (can change for same simulatoin at different time-steps due to AMR)
block_contents = size(dens,1); %This is the number of 'pixels' in one direction inside each block  (if ever nxx != nxy in setup, this needs to be modified)

if adjust_reso == 1,
    reso = round(sqrt(block_num))*block_contents; %this is approximate and not very true for sims with high degree of AMR
    display(['Resolution set to ',num2str(reso),' in each direction'])
else    
    reso=resolution;  %this is the resolution of the final output
end


%%

coordinates=zeros(2,block_num*block_contents*block_contents);
%%%%%%%%%%%%%%%%%% Start variable block
temp_wa = zeros(1,block_num*block_contents*block_contents);
dens_wa = zeros(1,block_num*block_contents*block_contents);
vely_wa = zeros(1,block_num*block_contents*block_contents);
%%%%%%%%%%%%%%%%%% End variable block


count = 1;
for n = 1:block_num,

    child = gid(9,n);  %this tells me if the block has any children, and if it does have children then ignore it
    if child == -1,
     cell_coords(1)=coords(1,n); %x 
     cell_coords(2)=coords(2,n); %y
        for m = 1:block_contents,
            for o=1:block_contents,
                
                %%%%%%%%%%%%%%%%%% Start variable block
                temp_wa(count)=temp(m,o,1,n);
                dens_wa(count)=dens(m,o,1,n);
                vely_wa(count)=vely(m,o,1,n);
                %%%%%%%%%%%%%%%%%% End variable block
                
                coordinates(1,count)=cell_coords(1)+(m-(1+block_contents/2))*block_size(1,n)/block_contents; %x
                coordinates(2,count)=cell_coords(2)+(o-(1+block_contents/2))*block_size(2,n)/block_contents; %y
                count = count + 1;
            end
        end
    end 
end

%%
[X,Y] = meshgrid( 0:max(coordinates(1,:))/reso:max(coordinates(1,:)),0:max(coordinates(2,:))/reso:max(coordinates(2,:)));

%%%%%%%%%%%%%%%%%% Start variable block
interpolator_temp = TriScatteredInterp(coordinates',temp_wa','natural');
interpolator_dens = TriScatteredInterp(coordinates',dens_wa','natural');
interpolator_vely = TriScatteredInterp(coordinates',vely_wa','natural');

reconstructedTemp = interpolator_temp(X,Y);
reconstructedDens = interpolator_dens(X,Y);
reconstructedVely = interpolator_vely(X,Y);
%%%%%%%%%%%%%%%%%% End variable block

xmax = [0 max(coordinates(1,:))];
ymax = [0 max(coordinates(2,:))];

xaxis = xmax;
yaxis = ymax;



