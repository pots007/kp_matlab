logf = getFileText('gascellcone.log');
dtfl = strfind(logf, 'dt=');
logm = cellfun(@isempty, dtfl);
tstepslines = logf(~logm); dtfl = dtfl(~logm);
dts = cellfun(@(x) str2double(x(strfind(x, 'dt=')+3:end)), logf, 'UniformOutput', 0);
hfig = figure;
plot(dts);
% for i=1:length(logf)
%     dts(i) = str2double(logf{i}(strfind(logf{i}, 'dt='):end));
% end