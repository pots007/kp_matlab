
function t = getFLASHtime(fname)

p=h5read(fname,'/real scalars');
pvalues = cellstr(p.name');

t = p.value(strcmp('time',pvalues) == 1);  %this is the time in seconds