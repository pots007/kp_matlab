%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is a code to take FLASH output from gas jet simulations, import into
% Matlab and plot important quantities
%
% v2: Sep 2015, based off 'onaxisgradients.m'
%
% Dependencies: open_FLASHv1.m, constants.m, colormaprev.m
%
%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Load constants, set m file parameters
%clear all
%close all

base_save_name = 'gasjet';

constants;
U = 4; %Mass of ion species (to work out 
Z = 2; %Z of ion species
gamma = 1.67; %specific heat capacity

save_data = 0; %save any plots from the simulation, 1 for yes
save_in_dir = 1; %make a child directory to store plots in, 1 for yes

%% Load in FLASH sdf file, find temperature density and pressure plots

present_dir = pwd;
%cd //Volumes/Nick_Work1/FlashGJ18/  %path to file with data in

[FileName,PathName,FilterIndex] = uigetfile('*.*','Please select a FLASH data file to analyse');
Image_Path=[PathName FileName];
display(['You chose to open ' PathName FileName])
    
cd(present_dir)

if save_in_dir == 1,
    LayerLoc=strfind(PathName,'/');
    EndSlash=LayerLoc(end);
    BeginSlash=LayerLoc(end-1);
    dirname=PathName(BeginSlash+1:EndSlash-1);
else 
    dirname = [];
end


exist(dirname,'dir')

if exist(dirname,'dir')==0,
   
    mkdir(dirname)
end
%%
% root_name = trim_ext(FileName);
root_name = 'gasjet_hdf5_chk_0009'; %FileName;

clearvars dens temp x_axis y_axis dumptime
[dens,temp,vely,x_axis,y_axis,dumptime] = open_FLASHv1(root_name);

display(['This timestep at ' num2str(dumptime*10^3) ' ms']);


%convert dens into molecular number
dens = dens/(U*amu*1e3);
%%  Fundamental simulation outputs
figure(1);imagesc(x_axis,y_axis,log10(temp))
xlabel('Radius (cm)')
ylabel('Height (cm)')
title('log( Temperature [K] )')
colorbar;
set(gca,'YDir','normal');
set(gca,'fontsize',15)
colormap('bone');colormaprev;


figure(2);imagesc(x_axis,y_axis,(dens))
%colormap('bone');colormaprev;
colormap(jet_white(256))
set(gca,'YDir','normal');
xlabel('Radius (cm)')
ylabel('Height (cm)')
set(gca,'fontsize',15)
title('log( Density [cm^-3] )')
colorbar;

figure(3);imagesc(x_axis,y_axis,vely/100)
colormap('bone');colormaprev;
set(gca,'YDir','normal');
xlabel('Radius (cm)')
ylabel('Height (cm)')
set(gca,'fontsize',15)
title('velocity in y [m/s]')
colorbar;




%% Plot data for a given y-position

lineout = 0.7; %lineout position in y direction

if (lineout > y_axis(2) || lineout < y_axis(1)),
    display ('lineout position invalid, taking y_max for lineout')
    lineout = y_axis(2);
end

y_grid = linspace(y_axis(1),y_axis(2),size(dens,1));
[~,I] = min(abs(y_grid-lineout));
dens_lineout = dens(I,:);
x_grid = linspace(x_axis(1),x_axis(2),length(dens_lineout));
figure(4),plot(x_grid,dens_lineout);
xlabel('Radius (cm)')
ylabel('Density (cm-3)')
set(gca,'fontsize',15)

%% Save lineout data

if save_data == 1,
    
    Save_Path_pdf=[dirname '/' base_save_name 'lineout.pdf'];
    Save_Path_fig=[dirname '/' base_save_name 'lineout.fig'];
    print(3, '-dpdf','-painters',Save_Path_pdf);
    saveas(4,Save_Path_fig);
    
end

%% Work out derived quantities
vdw_rad = 140e-12; %for helium, van der Waal's radius
sigma = pi*(100*vdw_rad)^2; %cross section, in cm2
mfp = 1./(sqrt(2)*sigma*dens); %mean free path, in cm

figure(5),imagesc(x_axis,y_axis,log10(mfp))
colormap('bone');colormaprev;
set(gca,'YDir','normal');
xlabel('Radius (cm)')
ylabel('Height (cm)')
set(gca,'fontsize',15)
title('log( Mean free path (cm) )')
colorbar;

cs = sqrt(gamma*Na*kb*(1/(U*1e-3))*temp);
Mach = (vely/100)./cs;
figure(6),imagesc(x_axis,y_axis,Mach)
colormap('bone');colormaprev;
set(gca,'YDir','normal');
xlabel('Radius (cm)')
ylabel('Height (cm)')
set(gca,'fontsize',15)
title('Mach number')
colorbar;

%% Axial measurements

temp_axis = temp(:,1);
dens_axis = dens(:,1);
vely_axis = vely(:,1);
Mach_axis = Mach(:,1);

figure(7),[hAx,hLine1,hLine2] = plotyy(y_grid,dens_axis,y_grid,temp_axis);

title('Gas jet y-axis dens, temp')
xlabel('Distance from throat (cm)')

ylabel(hAx(1),'Density (cm-3)') % left y-axis
ylabel(hAx(2),'Temperature (K)') % right y-axis


figure(8),[hAx,hLine1,hLine2] = plotyy(y_grid,vely_axis./1e5,y_grid,Mach_axis);
title('Gas jet y-axis vely, Mach')
xlabel('Distance from throat (cm)')

ylabel(hAx(1),'Velocity (km/s)') % left y-axis
ylabel(hAx(2),'Mach number') % right y-axis