% Run the data collection first

compareFlatSimulations;

%%
nav = 10;

locs = GetFigureArray(length(fols), 1, [0.1 0.05 0.1 0.05], 0.01, 'across');

hfig = figure(36); clf(hfig); hfig.Color = 'w';
h = [];
ax = axes('Parent', hfig, 'NextPlot', 'add');
for i=1:length(fols)  
    dens = f{i}.dens;
    yax = linspace(f{i}.yaxis(1), f{i}.yaxis(2), size(dens,1));
    xax = linspace(f{i}.xaxis(1), f{i}.xaxis(2), size(dens,2));
    % Average a region of nav pixels:
    ax_line = mean(dens(:,1:nav),2);
    peakv = mean(ax_line(yax<0.1 & yax > 0.02));
    [~,ind1] = min(abs(ax_line - 0.95*peakv)); yax(ind1);
    [~,ind2] = min(abs(ax_line - 0.05*peakv)); yax(ind2);
    fprintf('Scale length from 0.95 to e^-2 is %f mm\n', 10*(yax(ind2) - yax(ind1)))
    h(i) = plot(ax, yax, ax_line, 'LineWidth', 1);
    plot(yax(ind1)*[1 1], ax_line(ind1)*[0.7 1.3], ':m');
    plot(yax(ind2)*[1 1], ax_line(ind2)*[0.5 1.5], ':m');
    title(fols{i})
end
plot([0.165 0.165 nan 0.21 0.21], [0 3e18 nan 0 3e18], '--k');
% Plot ATA2 thingy from Juffalo
JuffDens= @(x0,w,x) 2.45e18*(x<=x0) + 2.45e18*(x>x0).*exp(-(x-x0)/w);
densFun = @(x0,w1,a1,w2,a2,x) 2.45e18*(x<=x0)+a1*(x>x0).*exp(-(x-x0)/w1)+a2*(x>x0).*exp(-(x-x0)/w2);
densFit = fit(yax(1:end-1)', ax_line(1:end-1), densFun, 'StartPoint', [0.1 0.04 1e18 0.2 1e18]);
h(end+1) = plot(yax, JuffDens(.16, 0.05, yax), '-.k', 'LineWidth', 2);
plot(yax, densFit(yax), '.m')
set(ax, 'XLim', [0.1 0.5], 'YLim', [0 2.7e18], 'Box', 'on');
ylabel(ax, 'Density / $\mathrm{cm}^{-3}$');
xlabel(ax, 'Height / cm');
legend(h, [fols 'Juff density data']);
make_latex(hfig); setAllFonts(hfig, 14);