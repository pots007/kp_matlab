% Compare the results from the FLASH simulations

fols = {'FlashCone3', 'FlashCone4', 'FlashCone4a', 'FlashCone4b', 'FlashCone4c', 'FlashCone4d', 'FlashCone4_2', 'FlashCone5'};
% The time we would ideally plot
t_ideal = 20e-6;

for i=1:length(fols)
    % Try to find the best file to plot
    flist = dir(fullfile(fols{i}, 'gascellcone_hdf5_chk_*'));
    ts = zeros(size(flist));
    for k=1:length(flist);
        if sum(strcmp({'png' 'pdf'}, flist(k).name(end-2:end))); continue; end;
        ts(k) = getFLASHtime(fullfile(fols{i}, flist(k).name));
    end
    [~,ind] = min(abs(ts - t_ideal));
    fprintf('Opening file %s in folder %s\n', flist(ind).name, fols{i});
    try
        %f{i} = getFLASHData(fullfile(fols{i}, flist(ind).name), 1000);
    catch err
        disp(err.message);
    end
end

%% Plot the 2D density distributions

locs = GetFigureArray(length(fols), 1, [0.1 0.05 0.1 0.05], 0.01, 'across');

hfig = figure(35); clf(hfig); hfig.Color = 'w';
ax  = [];
for i=1:length(fols)
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add');
    try
        dens = f{i}.dens;
        xax = linspace(f{i}.xaxis(1), f{i}.xaxis(2), size(dens,2));
        yax = linspace(f{i}.yaxis(1), f{i}.yaxis(2), size(dens,1));
        imagesc(xax, yax, dens, 'Parent', ax(i));
        set(ax(i), 'YDir', 'normal');
        title(fols{i})
    catch
        
    end
end
linkaxes(ax, 'xy');
set(ax, 'XLim', [0 0.5], 'YLim', [0 0.9], 'Box', 'on', 'CLim', [0 .1e18]);
cb = colorbar(ax(end), 'Position', [0.96 0.1 0.01 0.8]);
ylabel(cb, 'Density / $\mathrm{cm}^{-3}$');
set(ax(2:end), 'YTickLabel', []);
make_latex(hfig); setAllFonts(hfig, 14);

%% Plot lineouts at different heights

h_lines = [0.3 0.5 0.7 0.82];

locs = GetFigureArray(length(h_lines), 1, [0.1 0.05 0.1 0.05], 0.09, 'across');

hfig = figure(35); clf(hfig); hfig.Color = 'w';
ax  = [];
for k=1:length(h_lines)
    ax(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add');
    hh = [];
    for i=1:length(fols)
        try
            dens = f{i}.dens;
            xax = linspace(f{i}.xaxis(1), f{i}.xaxis(2), size(dens,2));
            yax = linspace(f{i}.yaxis(1), f{i}.yaxis(2), size(dens,1));
            [~,ind] = min(abs(yax - h_lines(k)));
            hh(i) = plot(ax(k), xax, smooth(dens(ind,:)));
        catch
            
        end
    end
    legend(ax(k), hh(ishandle(hh)), fols);
    title(sprintf('Line at %1.1f cm', h_lines(k)));
end
set(ax, 'XLim', [0 0.5], 'Box', 'on');
ylabel(ax(1), 'Density / $\mathrm{cm}^{-3}$');
make_latex(hfig); setAllFonts(hfig, 14);
set(ax(1), 'YLim', [0 5e17]);
set(ax(2), 'YLim', [0 2e17]);
set(ax(3), 'YLim', [0 1e17]);
set(ax(4), 'YLim', [0 1e17]);

%% Plot the axial lineouts

% Numebr of pixels to average over for axial lineout
nav = 5;

locs = GetFigureArray(length(fols), 1, [0.1 0.05 0.1 0.05], 0.01, 'across');

hfig = figure(36); clf(hfig); hfig.Color = 'w';
h = [];
ax = axes('Parent', hfig, 'NextPlot', 'add');
for i=1:length(fols)
    try
        dens = f{i}.dens;
        yax = linspace(f{i}.yaxis(1), f{i}.yaxis(2), size(dens,1));
        % Average a region of nav pixels:
        ax_line = mean(dens(:,1:nav),2);
        
        peakv = mean(ax_line(yax<0.1 & yax > 0.02));
        [~,ind1] = min(abs(ax_line - 0.95*peakv)); yax(ind1);
        [~,ind2] = min(abs(ax_line - exp(-2)*peakv)); yax(ind2);
        fprintf('Scale length for %s from 0.95 to e^-2 is %f mm\n', fols{i}, 10*(yax(ind2) - yax(ind1)))
        plot(yax(ind1)*[1 1], ax_line(ind1)*[0.7 1.3], ':m');
        plot(yax(ind2)*[1 1], ax_line(ind2)*[0.5 1.5], ':m');
        h(i) = plot(ax, yax, ax_line);
        title(fols{i})
    catch
        
    end
end
plot(0.15*[1 1], [0 1e20], '--k');
set(ax, 'XLim', [0 2], 'YLim', [0 2.9e18], 'Box', 'on');
ylabel(ax, 'Density / $\mathrm{cm}^{-3}$');
xlabel(ax, 'Height / cm');
legend(h, fols);
make_latex(hfig); setAllFonts(hfig, 14);

