
function f = getFLASHData(fname, res)

[dens,temp,vely,x_axis,y_axis,dumptime, gridrect] = open_FLASHv1(fname, res);
display(['File ' fname ' is timestep at ' num2str(dumptime*10^3) ' ms']);
%convert dens into molecular number
U = 4;
gamma = 5/3;
Constants;
f.dens = dens/(U*amu*1e3);
f.temp = temp;
f.vely = vely;
% Derived quantities as well
vdw_rad = 140e-12; %for helium, van der Waal's radius
sigma = pi*(100*vdw_rad)^2; %cross section, in cm2
f.mfp = 1./(sqrt(2)*sigma*dens); %mean free path, in cm
f.cs = sqrt(gamma*Na*kB*(1/(U*1e-3))*temp);
f.Mach = (vely/100)./f.cs;
f.xaxis = x_axis;
f.yaxis = y_axis;
f.gridrect = gridrect;