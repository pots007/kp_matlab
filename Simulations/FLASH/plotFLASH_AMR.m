% This function plots an example of AMR

function plotFLASH_AMR(filename)
f = getFLASHData(filename,500);
%
hfig = figure(340); clf(hfig);
for i=1:size(f.gridrect,2)
    %x0 = coords(1,i) - 0.5*block_size(1,i);
    %y0 = coords(2,i) - 0.5*block_size(2,i);
    x0 = f.gridrect(1,i);
    y0 = f.gridrect(2,i);
    rectangle('Position', [x0 y0 f.gridrect(3,i) f.gridrect(4,i)], 'LineWidth', 0.1);
end
set(gca, 'LineWidth', 2, 'Box', 'on', 'XLim', [0 0.5], 'YLim', [0 0.8]);
xlabel('x / cm'); ylabel('y / cm');
