% plotFLASHGasjet.m

% Heavily based on Nick's great work, plot the output of FLASH gas jet
% simulations

function plotFLASHGasjet(root_name, ftype)
if nargin==1
    ftype = '-pdf';
end

Constants;
U = 4; %Mass of ion species (to work out 
Z = 2; %Z of ion species
gamma = 1.67; %specific heat capacity

plot_together = 1; % Plots all graphs in single figure
save_data = 0; % save any plots from the simulation, 1 for yes
save_in_dir = 1; % make a child directory to store plots in, 1 for yes

%% Load in FLASH sdf file, find temperature density and pressure plots
%root_name = 'gascellcone_hdf5_chk_0011'; %FileName;
clearvars dens temp x_axis y_axis dumptime
[dens,temp,vely,x_axis,y_axis,dumptime] = open_FLASHv1(root_name, 800);
display(['This timestep at ' num2str(dumptime*10^3) ' ms']);
%convert dens into molecular number
dens = dens/(U*amu*1e3);
% Derived quantities as well
vdw_rad = 140e-12; %for helium, van der Waal's radius
sigma = pi*(100*vdw_rad)^2; %cross section, in cm2
mfp = 1./(sqrt(2)*sigma*dens); %mean free path, in cm
cs = sqrt(gamma*Na*kB*(1/(U*1e-3))*temp);
Mach = (vely/100)./cs;

lineout = 0.65; %lineout position in y direction
if (lineout > y_axis(2) || lineout < y_axis(1)),
    display ('lineout position invalid, taking y_max for lineout')
    lineout = y_axis(2);
end

y_grid = linspace(y_axis(1),y_axis(2),size(dens,1));
[~,I] = min(abs(y_grid-lineout));
dens_lineout = dens(I,:);
x_grid = linspace(x_axis(1),x_axis(2),length(dens_lineout));
temp_axis = temp(:,1);
dens_axis = dens(:,1);
vely_axis = vely(:,1);
Mach_axis = Mach(:,1);

dd.dens = dens; dd.yax = y_grid; dd.xax = x_grid;
assignin('base', 'dd', dd);
%% Sort out the plotting/saving
if save_data
    if save_in_dir
        if ~exist(savfol, 'dir')
            mkdir(savfol);
        end
    end
end
if plot_together
    locs = GetFigureArray(4,2,[0.1 0.05 0.1 0.05], 0.08, 'across');
    fig_n = 789*ones(1,8);
else
    locs = repmat([0.15 0.15 0.7 0.8]', 1, 8);
    fig_n = 788 + (1:8);
end

%  Fundamental simulation outputs
for i=1:8; 
    hfig = figure(fig_n(i)); clf(hfig); 
    set(hfig, 'Color', 'w', 'Position', [500 500 1400 760]);
end;
axs = zeros(1,8);
for i=1:8
    axs(i) = axes('Parent', fig_n(i), 'Position', locs(:,i));
    xlab = 'Radius (cm)';
    ylab = 'Height (cm)';
    switch i
        case 1
            imagesc(x_axis,y_axis,log10(temp), 'Parent', axs(i));
            titl = 'log( T / K)';
        case 2
            imagesc(x_axis,y_axis,(dens), 'Parent', axs(i));
            titl = 'Density / $\mathrm{cm}^{-3}$';
            set(gca, 'CLim', [0 2.5e18]);
        case 3
            imagesc(x_axis,y_axis,vely/100, 'Parent', axs(i));
            titl = '$v_y / \mathrm{ms}^{-1}$';
        case 4
            imagesc(x_axis,y_axis,log10(mfp), 'Parent', axs(i));
            titl = 'log( Mean free path / cm )';
        case 5
            imagesc(x_axis,y_axis,Mach, 'Parent', axs(i));
            titl = 'Mach number';
        case 6
            plot(axs(i), x_grid, dens_lineout);
            titl = ['Lineout at y = ' num2str(lineout*10) ' mm'];
            ylab = 'Density / $\mathrm{cm}^{-3}$';
        case 7
            [hhax,~,~] = plotyy(y_grid,dens_axis,y_grid,temp_axis);
            titl = 'Gas jet y-axis dens, temp';
            xlab = 'Height / cm';
            set(hhax(1), 'YLim', [0 2.5e18]);
            ylabel(hhax(1), 'Density / $\mathrm{cm}^{-3}$');
            ylabel(hhax(2), '           T / K');
        case 8
            [hhax, ~,~] = plotyy(y_grid,vely_axis./1e5,y_grid,Mach_axis);
            titl = 'Gas jet y-axis vely, Mach';
            xlab = 'Height / cm';
            ylabel(hhax(1), 'v / $\mathrm{kms}^{-1}$           ');
            ylabel(hhax(2), 'Mach number');
    end
    set(axs(i), 'YDir', 'normal', 'Box', 'on');
    if i<7; ylabel(axs(i), ylab); end
    if i<6; 
        cb = colorbar('Peer', axs(i)); 
        ylabel(cb, titl);
    end;
    xlabel(axs(i), xlab);
    drawnow;
end
if plot_together; set(axs(1:4), 'XAxisLocation', 'top'); end;
hh = annotation('textbox', 'Position', [0.2 0.93 0.1 0.05], 'String', sprintf('t=%1.3f ms', dumptime*1e3),...
    'LineStyle', 'none', 'HorizontalAlignment', 'center', 'Interpreter', 'latex', 'FontSize', 20);
for i=1:8; 
    setAllFonts(handle(fig_n(i)), 16); 
    make_latex(handle(fig_n(i)));
end;
colormap(jet_white(256));
if plot_together
    export_fig(root_name, ftype, '-nocrop', fig_n(1));
end