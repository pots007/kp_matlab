
% Test out things for FLASH cone setup 
xCoord = linspace(0, 0.55, 600);
yCoord = linspace(0, 0.85, 500);

mapp = zeros(length(yCoord), length(xCoord));
for i=1:length(xCoord)
    for j=1:length(yCoord)
        rw = 0.5*0.03 - 0.025*tand(30) + yCoord(j)*tand(30);
        if xCoord(i) >= rw
            if yCoord(j) <= 0.025 & xCoord(i) <=0.015
                mapp(j,i) = -1;
            else
                mapp(j,i) = 1;
            end
        else
            mapp(j,i) = -1;
        end
    end
end
figure(11);
imagesc(xCoord, yCoord, mapp)
set(gca, 'YDir', 'normal');

%% Now for try 2 - 300um hole still

xCoord = linspace(0, 0.5, 600);
yCoord = linspace(0, .85, 500);
ystart = 0.1;

mapp = zeros(length(yCoord), length(xCoord));
for i=1:length(xCoord)
    for j=1:length(yCoord)
        if yCoord(j) < ystart
            mapp(j,i) = -1;
            mapp(j,i) = 0.5;
        else
            if xCoord(i) < 0.015
                mapp(j,i) = -1;
            else
                yout = 0.085 + xCoord(i);
                yin = 0.099 + xCoord(i)*tand(60);
                if yCoord(j) < yout
                    mapp(j,i) = -1;
                    mapp(j,i) = 0.5;
                elseif yCoord(j) > yin
                    mapp(j,i) = -1;
                else
                    mapp(j,i) = 1;
                end
            end
        end
    end
end
figure(11);
imagesc(xCoord, yCoord, mapp)
set(gca, 'YDir', 'normal');
axis image

%% Now try 3 - 1mm hole

xCoord = linspace(0, 0.5, 600);
yCoord = linspace(0, .85, 500);
ystart = 0.15;
r0 = 0.025;

mapp = zeros(length(yCoord), length(xCoord));
for i=1:length(xCoord)
    for j=1:length(yCoord)
        if yCoord(j) < ystart
            mapp(j,i) = -1;
        else
            if xCoord(i) < r0
                mapp(j,i) = -1;
            else
                yout = (ystart-r0) + xCoord(i);
                yin = (ystart-tand(60)*r0)+ xCoord(i)*tand(60);
                if yCoord(j) < yout || yCoord(j) > yin
                    mapp(j,i) = -1;
                else
                    mapp(j,i) = 1;
                end
            end
        end
    end
end
figure(11);
imagesc(xCoord, yCoord, mapp)
set(gca, 'YDir', 'normal');
axis image

%% Now try x: 500um hole
xCoord = linspace(0, 0.6, 600);
yCoord = linspace(0, 2, 1000);
holeD = 0.0625;
ystart = 0.1+holeD

mapp = zeros(length(yCoord), length(xCoord));
for i=1:length(xCoord)
    for j=1:length(yCoord)
        if yCoord(j) < ystart
            mapp(j,i) = -1;
            mapp(j,i) = 0.5;
        else
            if xCoord(i) < holeD
                mapp(j,i) = -1;
            else
                yout = 0.1 + xCoord(i);
                yin = 0.099 + xCoord(i)*tand(60);
                if yCoord(j) < yout
                    mapp(j,i) = -1;
                    mapp(j,i) = 0.5;
                elseif yCoord(j) > yin
                    mapp(j,i) = -1;
                else
                    mapp(j,i) = 1;
                end
            end
        end
        if xCoord(i)>0.55 && yCoord(j)>1
            mapp(j,i) = 1;
        end
    end
end
figure(11);
imagesc(xCoord, yCoord, mapp)
set(gca, 'YDir', 'normal');
axis image
fprintf('Hole diameter = %2.2f mm\n', holeD*2*10);
