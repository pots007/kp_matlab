% getDanimalData.m
%
% Write data into a friendly format for Danimal's people to use.
%
% Reades in file, and saves each macroparticle's position, momentum and
% weight.

%function getDanimalData(fname, species, plotFlag)

datafol = '/media/kpoder/KRISMOOSE6/fbpic/Danimal/runb/hdf5';
fname = 'data00640328.h5'; species = 'shell67'; plotFlag = 1;
% Get number of particles saved for preallocation
ww = GetDataOpenPMD(fullfile(datafol, fname), '/particles/shell67/weighting');
nPart = length(ww);
mevf = 5.344e-22;

%% Read positions and momenta:
posmom = zeros(nPart,7);
pp = {'position', 'momentum'}; cc = 'xyz';
for p=0:1
    for c=1:3
        posmom(:,p*3+c) = GetDataOpenPMD(fullfile(datafol, fname),...
            ['/particles/shell67/' pp{p+1} '/' cc(c)]);
    end
end
posmom(:, end) = ww;

% Now how do we dump this?
% 
% Two options: hdf5 file or a raw binary file.
% First, raw binary.
[dataroot, runn, ~] = fileparts(fileparts(datafol));
fid = fopen(fullfile(dataroot, [runn '_' fname(1:end-3) '.dat']), 'wb');
for k=1:size(posmom,2)
    fwrite(fid, posmom(:,k), 'double');
end
fclose(fid);
%%
% Then the h5 file
h5name = fullfile(dataroot, [runn '_' fname]);
if exist(h5name, 'file'); delete(h5name); end
% First momenta
args = {'DataType', 'double', 'ChunkSize', [1000,3], 'Deflate', 0};
h5create(h5name, '/data/momentum', [size(posmom,1), 3], args{:});
h5write(h5name, '/data/momentum', posmom(:, 4:6))
h5create(h5name, '/data/position', [size(posmom,1), 3], args{:});
h5write(h5name, '/data/position', posmom(:, 1:3))
args{4} = [1000,1];
h5create(h5name, '/data/weights', [size(posmom,1), 1], args{:});
h5write(h5name, '/data/weights', posmom(:, end))

%% And plot the spectrum as well
hfig = figure(231325); clf(hfig);
histogram(posmom(:,6)/(qe/c*1e6), 2000);
xlabel(gca, 'Energy / MeV');
ylabel(gca, 'Counts / a.u.');
setAllFonts(hfig, 14);
export_fig(fullfile(dataroot, [runn '_' fname(1:end-3)]), '-png', '-m4', '-nocrop', hfig);