%Plot ne of one datafile

function him = PlotDensity(datafile, species)

dataf = GetDataSDFchoose(datafile, 'grid');
x = dataf.Grid.Grid.x;
x = x(1:length(x)-1)*1e3;
xt = SetSmartUnit(x);
dims = 1;
if isfield(dataf.Grid.Grid, 'y')
    y = dataf.Grid.Grid.y;
    y = y(1:length(y)-1)*1e3;
    yt = SetSmartUnit(y);
    dims = 2;
end
if isfield(dataf.Grid.Grid, 'z')
    z = dataf.Grid.Grid.z;
    z = y(1:length(z)-1)*1e3;
    zt = SetSmartUnit(z);
    dims = 3;
end
time = dataf.time*1e15;
dataf = GetDataSDFchoose(datafile, ['number_density/' species]);
ne = dataf.Derived.Number_Density.(species).data;
ne = 1e-6.*ne./ncrit(800);
if dims==1
    plot(xt.t, ne);
elseif dims==2
    him = imagesc(xt.t,yt.t,ne');
elseif dims==3
    him = imagesc(xt.t, yt.t, ne(:,:,round(0.5*size(ne,3)))');
end
title(['n of ' species ' at t=' num2str(time*1e-3) ' ps']);

