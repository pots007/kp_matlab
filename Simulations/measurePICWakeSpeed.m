% look at speed of wake!

if exist('Ex/longitudinal.mat','file')
    load Ex/longitudinal.mat;
end
logm = ~cellfun(@isempty, longitudinal(:,1), 'Uniformoutput', 1);
longdat = longitudinal(logm, :);
axnes = cell2mat(longdat(:,2))';
[~, inds] = max(axnes(2500:end,:), [],1);

figure(90000); clf;
set(gca, 'NextPlot', 'add');
imagesc(axnes(2500:end,:));
plot(1:size(axnes,2), inds, 'x');
%%
%hfig = figure(77); clf(hfig);
set(hfig, 'Position', [500 600 500 400], 'Color', 'w');
x_pos = cell2mat(longdat(:,5))*c;
y_pos = longdat{1,3}(inds) + 4.5e-5;
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
nu_etch = 2e18/ncrit(800);
plot([0 max(x_pos)], 2e-5 + [0 -1*max(x_pos)*nu_etch], '--k');
h2 = plot(x_pos, y_pos, '.k');
set(gca, 'YDir', 'reverse');
ylabel('$z-v_gt$ / micron');
xlabel('x / mm');
make_latex(hfig); setAllFonts(hfig, 16);
