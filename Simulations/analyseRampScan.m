% analyseRampScan.m

% Look into the folder and plot the intensities as timehistories, along
% with spot size. Then collate one graph with peak intensities.

% Cheat sheet:
%Gem2015RS and:
%       F40 F20
% 0     g   h
% 1000  a   d
% 1500  c   f
% 2000  b   e
% 5000  i   j


nsims = 10;
datafol = '/Volumes/KRISMOOSE3/Gem2015RampScan';

fols = cell(nsims,1);
sims = 'gacbihdfej';
for i=1:nsims; fols{i} = sprintf('Gem2015RS%s', sims(i)); end;
optic = cell(nsims,1);
[optic{1:5}] = deal('F40'); [optic{6:10}] = deal('F20');
ramplen = [0 1000 1500 2000 5000 0 1000 1500 2000 5000];

%% Intensities as a time history

locs = GetFigureArray(nsims/2,2,[0.05 0.12 0.09 0.08], 0.015, 'across');
axs = zeros(1, nsims);
figure(44); clf(44);
set(44, 'Position', [100 100 1000 700], 'Color', 'w');
intensities = cell(1,6);

for i=1:nsims
    try
        ll = load(fullfile(datafol, fols{i}, 'Ez', 'transverse.mat'));
    catch
        continue;
    end
    transverse = ll.transverse;
    logm = ~cellfun(@isempty, transverse(:,1));
    nfiles = sum(logm);
    %nfiles = sum(cell2mat(isempty(transverse(:,1))));
    axialint = zeros(length(transverse{2,2}),nfiles);
    energies = NaN(1,nfiles);
    times = zeros(1,nfiles);
    fwhms = NaN(1,nfiles);
    for m = 2:nfiles
        axialint(:,m) = transverse{m,2}';
        energies(m) = transverse{m,4};
        times(m) = transverse{m,5};
        fwhms(m) = transverse{m,6};
    end
    y = [];
    m=2;
    while(isempty(y))
        y = transverse{m,3};
        m = m+1;
    end
    prop = times*3e8*1e3; %propagation distance in mm;
    axs(i) = axes('Parent', 44, 'Position', locs(:,i), 'NextPlot', 'add');
    ints = 0.5*axialint*c*epsilon0*1e-4;
    a0s = 0.856*0.8*sqrt(ints*1e-18);
    imagesc(prop, y*1e6, a0s); 
    plot(prop, 0.5*fwhms*(y(2)-y(1))*1e6, 'Linewidth', 1.5, 'Color', 'w');
    % Now compile maximum axial intensities
    intensities{i} = max(a0s(:,1:179),[],1);
end
linkaxes(axs, 'xy');
set(axs, 'YDir', 'normal', 'CLim', [0 5], 'YLim', [0 100], 'XLim', [0 18]);
set(axs([2,3,4,5,7,8,9,10]), 'YTickLabel', []);
set(axs(1:5), 'XTickLabel', []);
for i=1:5; title(axs(i), sprintf('L_r = %1.1f mm', ramplen(i)*0.001)); end;
cb = colorbar('Peer', axs(4), 'Position', [0.89 0.09 0.02 0.86]);
ylabel(cb, 'a_0');
setAllFonts(44, 16);
%export_fig('/Volumes/KRISMOOSE3/Gem2015RampScan/a0s', '-pdf', '-nocrop', 44);
%%
figure(45); clf(45);
set(45, 'Position', [100 100 1000 700], 'Color', 'w');
set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 2);
l_s = {'-k', '-r', '-b', '-g', '-c', '--k', '--r', '--b', '--g', '--c'};
hh = [];
for i=1:nsims
    %dataf = GetDataSDFchoose(fullfile(datafol, fols{i}, 'data', 'normal0002.sdf'), 'ey');
    %Ey = dataf.Electric_Field.Ey.data;
    %toten(i) = sum(sum(Ey.^2));
    hh(i) = plot(prop(1:179), intensities{i}, l_s{i});
end
set(hh, 'LineWidth', 2);
xlabel('Propagation distance / mm');
ylabel('Axial a_0');
for i=1:nsims
    leglabels{i} = sprintf('%s, %1.1f mm', optic{i}, ramplen(i)*1e-3);
end
legend(hh, leglabels, 'Location', 'EastOutside');
setAllFonts(45, 20);
%export_fig('/Volumes/KRISMOOSE3/Gem2015RampScan/Axiala0s', '-pdf', '-nocrop', 45);

%% Also, plot plasma wave histories and acceleration history to see if we inject

locs = GetFigureArray(nsims/2,2,[0.05 0.12 0.09 0.08], 0.015, 'across');
axs = zeros(1, nsims);
axs2 = zeros(size(axs));
figure(45); clf(45);
figure(46); clf(46);
set(45, 'Position', [100 100 1000 700], 'Color', 'w');
set(46, 'Position', [1200 100 1000 700], 'Color', 'w');
intensities = cell(1,nsims);

for i=1:nsims
    try
        nn = load(fullfile(datafol, fols{i}, 'ne', 'nedata.mat'));
        ll = load(fullfile(datafol, fols{i}, 'Ey', 'transverse.mat'));
    catch
        continue;
    end
    transverse = ll.transverse;
    nedat = nn.nedat;
    logmTV = ~cellfun(@isempty, transverse(:,1));
    logmne = ~cellfun(@isempty, transverse(:,1));
    nfilesTV = sum(logmTV);
    nfilesne = sum(logmne);
    nfiles = min([nfilesTV nfilesne]);
    
    n_es = zeros(length(nedat{1,2}),nfiles);
    
    energies = NaN(1,nfiles);
    times = zeros(1,nfiles);
    fwhms = NaN(1,nfiles);
    for m = 2:nfiles
        n_es(:,m) = nedat{m,3};
        %energies(m) = transverse{m,4};
        times(m) = transverse{m,5};
        fwhms(m) = transverse{m,6};
    end
    y = [];
    m=2;
    while(isempty(y))
        y = transverse{m,3};
        m = m+1;
    end
    
    %find index of largest px
    maxs = cellfun(@(x) max(x(:,2)), nedat(:,4), 'UniformOutput', false, 'Errorhandler', @(y,x) NaN);
    k=5;
    mx = maxs{5};
    mind = 5;
    while (~isnan(maxs{k}))
        if (maxs{k} > mx)
            mx = maxs{k};
            mind = k;
        end
        k=k+1;
    end
    px = nedat{mind,4}(:,2);
    px_e = zeros(length(px),i);
    for m=1:nfiles
        px_e(:,m) = interp1(nedat{m,4}(:,2), nedat{m,4}(:,1), px);
    end
    
    % And now plot things
    
    prop = times*3e8*1e3; %propagation distance in mm;
    axs(i) = axes('Parent', 45, 'Position', locs(:,i), 'NextPlot', 'add');
    axs2(i) = axes('Parent', 46, 'Position', locs(:,i), 'NextPlot', 'add');
    imagesc(prop, y*1e6, n_es, 'Parent', axs(i)); 
    imagesc(prop, px, log10(px_e), 'Parent', axs2(i)); 
    plot(axs(i), prop, 0.5*fwhms*(y(2)-y(1))*1e6, 'Linewidth', 1.5, 'Color', 'r');
    % Now compile maximum axial intensities
    %intensities{i} = max(a0s(:,1:179),[],1);
    drawnow;
end
linkaxes(axs, 'xy');
set(axs, 'YDir', 'normal', 'CLim', [0 8e24], 'YLim', [0 100], 'XLim', [0 12]);
set(axs([2,3,4,5,7,8,9,10]), 'YTickLabel', []);
set(axs2, 'YDir', 'normal', 'CLim', [10 14], 'YLim', [0 1500], 'XLim', [0 12]);
set(axs2([2,3,4,5,7,8,9,10]), 'YTickLabel', []);
set(axs(1:5), 'XTickLabel', []);
set(axs2(1:5), 'XTickLabel', []);
for i=1:5; 
    title(axs(i), sprintf('L_r = %1.1f mm', ramplen(i)*0.001)); 
    title(axs2(i), sprintf('L_r = %1.1f mm', ramplen(i)*0.001)); 
end;
cb = colorbar('Peer', axs(4), 'Position', [0.89 0.09 0.02 0.86]);
ylabel(cb, 'a_0');
cb = colorbar('Peer', axs2(4), 'Position', [0.89 0.09 0.02 0.86]);
ylabel(cb, 'log_{10} (n_e)');
setAllFonts(45, 16);
setAllFonts(46, 16);
colormap(45, 'parula');
export_fig('/Volumes/KRISMOOSE3/Gem2015RampScan/nes', '-pdf', '-nocrop', 45);
export_fig('/Volumes/KRISMOOSE3/Gem2015RampScan/pxs', '-pdf', '-nocrop', 46);
