%% Get all the data
Constants;
load divmap.mat; load invfire.mat;
fold = 'data/';
plotgraphs = true;

filen = dir([fold '/normal*.sdf']);

if (~(exist('Ex', 'dir')==7))
    mkdir('Ex');
end
if (exist('Ex/Exdata.mat','file')==2)
    load Ex/Exdata.mat;
else
    exdat = cell(1000,4);
    ionexist=false;
end

for i=1:length(filen)
    if strcmp(exdat{i,1},filen(i).name)
        continue;
    end
    disp(filen(i).name);
    exdat{i,1} = filen(i).name;
    try
        dataf = GetDataSDFchoose([fold filen(i).name], 'grid');
        x = dataf.Grid.Grid.x;
        exdat{i,2} = x(2:end);
        exdat{i,3} = dataf.time;
    catch
    end
    dataf = GetDataSDFchoose([fold filen(i).name], 'ex');
    ex = dataf.Electric_Field.Ex.data;
    if length(size(ex))==3
        exdat{i,4} = ex(:,floor(0.5*size(ex,2)),floor(0.5*size(ex,3)));
    elseif length(size(ex))==2
        exdat{i,4} = ex(:,floor(0.5*size(ex,2)));
    end
    
end

save('Ex/Exdata.mat', 'exdat');

%% And now for the plotting part
totf = sum(~cellfun(@isempty, exdat(:,1)));

axit = 1;
figure(57); clf;
set(57, 'WindowStyle', 'docked');
axs = 0;



propa = zeros(1,totf);
exs = zeros(length(exdat{1,4}), totf);
for m=1:totf
    propa(m) = max(exdat{m,2})*1e3;
    exs(:,m) = exdat{m,4};
end


imagesc(propa, 1e6*(exdat{1,2}-min(exdat{1,2})),exs);

%set(axs, 'YDir', 'normal');
colormap(divmap)
setAllFonts(57, 18);
export_fig('Ex/Exdat', '-nocrop', '-transparent', '-pdf', 57)
