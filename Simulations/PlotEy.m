files = dir('data/*.sdf');
if (~(exist('Ey', 'dir')==7))
    mkdir('Ey');
end
figure(1)
cla;
set(1, 'WindowStyle', 'docked');
for i=1:length(files)
    if plotall
        %Plot all
    else
        if (exist(['Ey/Ey_' files(i).name(1:4) '.png'], 'file'))
            continue;
        end
    end
    disp(['File ' files(i).name]);
    dataf = GetDataSDFchoose(['data/' files(i).name], 'grid');
    x = dataf.Grid.Grid.x;
    x = x(1:length(x)-1);
    y = dataf.Grid.Grid.y;
    y = y(1:length(y)-1);
    time = dataf.time*1e15;
    dataf = GetDataSDFchoose([simtype '/' files(i).name], 'ey');
    data = dataf.Electric_Field.Ey.data';
    data = 1e-4*0.5*3e8*8.85e-12.*data.^2;
    subplot(2,2,1)
    h = imagesc(x, y, data);
    title([files(i).name ', E_y after ' num2str(time) ' fs']);
    colorbar;
    ylabel(colorbar, 'Intensity (W/cm^2)');
    subplot(2,2,2);
    lineout = dataf.Electric_Field.Ey.data(:,floor(length(y)*0.5));
    plotEwithphase(lineout, x, 2*pi*3e8/8e-7);
    %plot(x, lineout, x, abs(env), 'r');
    %axis tight;
    %title(['E_y lineout along axis, ' num2str(time) ' fs']);
    subplot(2,2,3)
    plot(y, sum(abs(dataf.Electric_Field.Ey.data)));
    axis tight;
    title(['E_y transverse sum, ' num2str(time) ' fs']);
    subplot(2,2,4)
    plotEspectrumwithphase(lineout, x, 2*pi*3e8/8e-7, 0);
    drawnow;
    %saveas(1, ['Ey/Ey_' files(i).name(1:4) '.png'], 'png');
    export_fig(['Ey/Ey_' files(i).name(1:4)], '-transparent', '-nocrop', '-png',1);
end



