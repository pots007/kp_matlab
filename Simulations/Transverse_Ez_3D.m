load divmap.mat;
fold = 'data';
plotgraphs = true;

filen = dir(fullfile(fold, '*_Ez.sdf'));
if (isempty(filen))
    filen = dir(fullfile(fold, 'normal*.sdf'));
end

if (~(exist('Ez', 'dir')==7))
    mkdir('Ez');
end
if (exist('Ez/transverse.mat','file')==2)
    load Ez/transverse.mat;
else
    transverse = cell(500,6);
end

for i=1:length(filen)
    if (strcmp(transverse{i,1}, filen(i).name))
        continue;
    end
    transverse{i,1} = filen(i).name;
    dataf = GetDataSDFchoose([fold '/' filen(i).name], 'ez');
    Ez = dataf.Electric_Field.Ez.data;
    try
        grid = GetDataSDFchoose([fold '/' filen(i).name], 'grid');
        x = grid.Grid.Grid.x;
        y = grid.Grid.Grid.y;
        z = grid.Grid.Grid.z;
        transverse{i,2} = x(1:length(x)-1);
        transverse{i,3} = y(1:length(y)-1);
        transverse{i,4} = z(1:length(z)-1);
    catch
        
    end
    %transverse{i,2} = sum(Ez.^2,2); % For true transverse, below longitudinal
    transverse{i,5} = abs(hilbert(Ez(:, round(0.5*size(Ez,2)), round(0.5*size(Ez,3))))).^2;
    transverse{i,6} = Ez(:, round(0.5*size(Ez,2)), round(0.5*size(Ez,3)));
    transverse{i,7} = squeeze(sum(Ez.^2,1));
    transverse{i,8} = dataf.time;
    
    %transverse{i,6} = widthfwhm(sum(Ez.^2,2)')/gridfact;
    disp(['File ' filen(i).name]);
end
save('Ez/transverse.mat', 'transverse');

if (plotgraphs)
    figure(15)
    set(15, 'WindowStyle', 'docked');
    clf;
    subplot(1,2,1);
    logm = cellfun(@isempty, transverse(:,1), 'UniformOutput', 0);
    logm = ~cell2mat(logm);
    
    times = cell2mat(transverse(logm,8));
    energies = cellfun(@(x) sum(x(:)), transverse(logm,7));
    prop = times*3e8*1e3; %propagation distance in mm;
    imagesc(prop, y*1e6, focusing);
    hold on;
    plot(prop, 0.5*fwhms*(y(2)-y(1))*1e6, 'Linewidth', 1.5, 'Color', 'w');
    %plot(prop, -0.5*fwhms*(y(2)-y(1))*1e6, 'Linewidth', 1.5, 'Color', 'w');
    ylabel('Transverse dimension (\mum)');
    xlabel('Propagation distance (mm)');
    title('E_z spot size as a function of propagation distance');
    ylabel('longitudinal dimension (\mum)');
    title('Axial intensity as a function of propagation distance');
    hold off;
    set(gca, 'YDir', 'normal');
    subplot(1,2,2);
    energies = energies./max(energies);
    plot(prop, energies);
    xlabel('Propagation distance (mm)');
    ylabel('Normalised integrated field');
    title('Integrated E_z as a function of propagtion distance');
    export_fig('Ez/transverse.pdf', '-pdf',15);
end