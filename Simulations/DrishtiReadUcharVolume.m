function A = DrishtiReadUcharVolume(filename)

fid = fopen(filename, 'r');

firstByte = fread(fid, 1, 'uint8');
NX = fread(fid, 1, 'uint32');
NY = fread(fid, 1, 'uint32');
NZ = fread(fid, 1, 'uint32');

A = zeros(NX,NY,NZ);

h = waitbar(0, 'Reading pvl.nc file');

for ii = 1:NX
    for jj = 1:NY
            A(ii,jj,:) = fread(fid, NZ, 'uchar');
    end
    waitbar(ii/NX, h)
end

close(h)

fclose(fid);

end