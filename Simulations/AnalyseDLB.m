rootf = '/Volumes/KRISMOOSE3/DLBsims/';
dlbs = [0 2 3 4 5 6 7 8 9];
dlbdat = zeros(3,length(dlbs));
for i=1:length(dlbs)
    fid = fopen([rootf sprintf('dlb0_%1i',dlbs(i)) '/log.txt']);
    totl=0;
    lbline = 0;
    while (~feof(fid))
        fline = fgetl(fid);
        totl=totl+1;
        if (strfind(fline, 'Load balancing'))
            lbline = lbline + 1;
        end
    end
    fclose(fid);
    dlbdat(1,i) = totl;
    dlbdat(2,i) = lbline;
    i1 = regexp(fline, '\d* hours');
    i2 = regexp(fline, ' hours');
    hs = str2double(fline(i1:i2-1));
    i1 = regexp(fline, '\d* minutes');
    i2 = regexp(fline, ' minutes');
    mins = str2double(fline(i1:i2-1));
    i1 = regexp(fline, '\d*.\d* seconds');
    i2 = regexp(fline, ' seconds');
    secs = str2double(fline(i1:i2-1));
    dlbdat(3,i) = 3600*hs + 60*mins + secs;
end
figure(101)
clf
plot(dlbs*0.1, dlbdat(3,:)/3600, '-xk', 'MarkerSize',10, 'LineWidth', 2);
set(gca, 'XLim', [-0.05 1.05], 'Position', [0.15 0.15 0.7 0.75], 'YAxisLocation', 'left');
ylabel('Runtime / hours');
xlabel('dlb threshold');
ax1 = gca;
ax2 = axes('Position', get(ax1, 'Position'));
scatter(dlbs*0.1, dlbdat(2,:)*1e-3, 'ob', 'Parent', ax2, 'LineWidth', 2);
set(ax2, 'XAxisLocation', 'bottom',...
    'YAxisLocation', 'right', 'Color', 'none', 'XColor', 'k', 'YColor', 'b',...
    'XLim', get(ax1, 'XLim'), 'XTickLabel', []);
set(get(ax2, 'YLabel'), 'String', 'No of load balancings / \times 10^3');
setAllFonts(101, 22);

export_fig([rootf 'DLBresults'], '-transparent', '-nocrop', '-pdf', 101);



