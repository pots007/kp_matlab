% save3Dsim_LongIntegral.m
%
% Save field^2, integrated along

function save3Dsim_LongIntegral(datfol, field, fstem)
% Get list of files we have
if nargin==2; fstem = 'normal'; end
flist = dir(fullfile(datfol, [fstem '*.sdf']));
% We assume the grid based diagnostic remain the same throughout the
% simulation!

grid_y = cell(1,length(flist));
grid_time = zeros(1,length(flist));
grid_z = cell(1, length(flist));
slicedata = cell(1, length(flist));

for f=1:length(flist)
    fprintf('Opening file %s for diagnostic %s\n', flist(f).name, field);
    gridf = GetDataSDFchoose(fullfile(datfol, flist(f).name), 'grid');
    try
        dataf = GetDataSDFchoose(fullfile(datfol, flist(f).name), field);
    catch err
        fprintf('Failed to get data for %s, diagnostic %s: %s\n', flist(f).name, field, err.message);
    end
    % Now use a few trys in case the grid size has changed
    grid_y{f} = gridf.Grid.Grid.y;
    grid_z{f} = gridf.Grid.Grid.z;
    grid_time(f) = gridf.time;
    
    switch field
        case 'Ex'
            slice_ = dataf.Electric_Field.Ex.data;
        case 'Ey'
            slice_ = dataf.Electric_Field.Ey.data;
        case 'Ez'
            slice_ = dataf.Electric_Field.Ez.data;
        case 'Bx'
            slice_ = dataf.Magnetic_Field.Bx.data;
        case 'By'
            slice_ = dataf.Magnetic_Field.By.data;
        case 'Bz'
            slice_ = dataf.Magnetic_Field.Bz.data;
        otherwise
    end
    slicedata{f} = squeeze(sum(slice_.^2, 1));
end

save(fullfile(datfol, [field '_LongIntegral']), 'slicedata', 'grid_y', 'grid_z', 'grid_time');