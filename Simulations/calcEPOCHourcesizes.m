%% Get the data
simfolder = 'data4/';
filen = dir([simfolder 'particles*.sdf']);
ens = [100 200 500 1000 1500];
widths = NaN(length(filen),length(ens));
centrs = NaN(length(filen),length(ens));
propa = NaN(length(filen),1);
for i=1:length(filen)-1
    disp(['File ' num2str(i)]);
    specs = {'electron'};%{'ebeam', 'shell7'};
    for k=1:1
        try
            datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['grid/subset_highgamma/' specs{k}]);
            y = datp.Grid.Particles.subset_highgamma.(specs{k}).y*1e6; %in microns
            x = datp.Grid.Particles.subset_highgamma.(specs{k}).x;
            propa(i) = c*datp.time*1e3;
            %datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['px/subset_highgamma/' specs{k}]);
            %px = datp.Particles.Px.subset_highgamma.(specs{k}).data;
            %datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['py/subset_highgamma/' specs{k}]);
            %py = datp.Particles.Py.subset_highgamma.(specs{k}).data;
            %yprime = py./px*1e3;
            datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['gamma/subset_highgamma/' specs{k}]);
            gamma = datp.Particles.Gamma.subset_highgamma.(specs{k}).data;
            for l=1:length(ens)
                logm = gamma>ens(l);
                if sum(logm)==0 continue; end
                xp = x(logm); yp = y(logm);
                logm = xp > (max(xp) - 10e-6) & abs(yp) < 10;
                widths(i,l) = sqrt(var(yp(logm)));
                centrs(i,l) = mean(yp(logm));
            end
        catch
            
        end
    end
end
%% plot the data
figure(900); clf;
locs = GetFigureArray(1,2,0.1, 0.06, 'down');
ax1 = axes('Parent', 900, 'Position', locs(:,1));
hs1 = plot(propa, widths', '-');
legend(hs1, strcat('\gamma > ', num2str(ens')), 'Location', 'Northeast')
set(ax1, 'XTickLabel', []);
ax2 = axes('Parent', 900, 'Position', locs(:,2));
hs2 = plot(propa, centrs', '-');
legend(hs2, strcat('\gamma > ', num2str(ens')), 'Location', 'Northeast')
linkaxes([ax1 ax2], 'x');
set([hs1 hs2], 'LineWidth', 1.5);
xlabel(ax2, 'Propagation distance / mm');
ylabel(ax1, '\sigma_y / \mu m');
ylabel(ax2, 'Mean y location / \mu m');
setAllFonts(900,18);