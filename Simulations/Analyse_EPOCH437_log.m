logfiles = {'log1.txt', 'log2.txt', 'log3.txt', 'log4.txt', 'log5.txt'};
totlines = 0;
timedata = zeros(3,100); % time, iteration, walltime
offset = 0;
for i=1:length(logfiles)
    fil = getFileText(logfiles{i});
    for k=1:length(fil)
        s = fil{k};
        if length(s)>4 && strcmp(s(1:4), 'Time')
            totlines = totlines + 1;
            timedata(1,totlines) = str2double(s(7:24))*1e12;
            timedata(2,totlines) = str2double(s(40:50));
            timedata(3,totlines) = 24*3600*str2double(s(57:59)) + 3600*str2double(s(61:62)) + ...
                60*str2double(s(64:65)) + str2double(s(67:end)) + offset; 
        end
    end
    offset = timedata(3,totlines);
end
timedata(:,totlines+1:end) = nan;
figure(7)
plot(timedata(1,:), timedata(3,:), '-xr');
xlabel('Simulation time / ps');
ylabel('Total spent walltime / days');
ndays = ceil(timedata(3,totlines)/(24*3600));
set(gca, 'YTick', 24*3600*(1:ndays), 'YTickLabel', 1:ndays);
setAllFonts(7,20);
export_fig('SimLog', '-transparent', '-nocrop', '-pdf', 7);