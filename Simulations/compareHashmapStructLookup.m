% compareHashmapStructLookup.m

% This is  8.9 MB binary file.
dataf = GetDataSDF('data/particles0022.sdf');

trackItem = struct;
trackItem.ID = [];
trackItem.x = [];
trackItem.y = [];
trackItem.z = [];
trackItem.px = [];
trackItem.py = [];
trackItem.pz = [];
trackItem.gamma = [];
trackItem.time = [];

trackingMap = containers.Map('KeyType', 'int64', 'ValueType', 'any');
IDs = dataf.Particles.ID.subset_highgamma.electron.data;
% Preallocate structure array
trackingStruct(length(IDs)) = trackItem;
tic
for p=1:length(IDs)
    temp = trackItem;
    temp.ID = IDs(p);
    temp.x = dataf.Particles.ID.subset_highgamma.electron.grid.x(p);
    temp.y = dataf.Particles.ID.subset_highgamma.electron.grid.y(p);
    temp.z = dataf.Particles.ID.subset_highgamma.electron.grid.z(p);
    temp.px = dataf.Particles.Px.subset_highgamma.electron.data(p);
    temp.py = dataf.Particles.Py.subset_highgamma.electron.data(p);
    temp.pz = dataf.Particles.Pz.subset_highgamma.electron.data(p);
    temp.gamma = dataf.Particles.Gamma.subset_highgamma.electron.data(p);
    temp.time = dataf.time;
    trackingMap(IDs(p)) = temp;
    % Trying out using bytestreams to m ake it faster perhaps - not faster
    %bStream = getByteStreamFromArray(temp);
    %trackingMap(IDs(p)) = bStream;
    trackingStruct(p) = temp;
end
toc


%%
ntest = 100;
inds = randi(length(IDs), [ntest 1]);

tic 
for i=1:ntest
    ind = inds(i);
    temp_ = trackingMap(IDs(ind)); xx = temp_.x;
end
t1 = toc;

tic
for i=1:ntest
    ind = inds(i);
    IDinds = cell2mat({trackingStruct.ID});
    hh = find(IDinds == IDs(ind));
    xx_ = trackingStruct(hh).x;
end
t2 = toc;

fprintf('Hashmap time for %i ID searches: %2.3f\n', ntest, t1);
fprintf('Struct time for %i ID searches: %2.3f\n', ntest, t2);
fprintf('\t Speedup factor: %2.1f\n', t2/t1);