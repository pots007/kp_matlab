% Rename files to a sequential order
% Assumes all have file stem normal

datfol = 'data';
flist = dir(fullfile(datfol, 'normal*'));
% Hacky, but all files have a number from elements 7:10...
flist_ = {flist.name}';
fstems = cellfun(@(x) x(1:10), flist_, 'UniformOutput', 0);
fstems_uniq = unique(fstems);
for i=1:length(fstems_uniq)
    flist1 = dir(fullfile(datfol, [fstems_uniq{i} '*']));
    for k=1:length(flist1)
        oldf = flist1(k).name;
        newf = oldf;
        newf(7:10) = num2str(i, '%04i');
        fprintf('%40s -> %40s\n', oldf, newf);
        try
            movefile(fullfile(datfol, oldf), fullfile(datfol, newf));
        catch err
            disp(err.message);
        end
    end
end
