%pause(6000);

save3DsimPlane_var('data', {'Ex'}, 'y', 'normal');
save3DsimPlane_var('data', {'Ex'}, 'z', 'normal');
save3DsimPlane_var('data', {'Ey'}, 'y', 'normal');
save3DsimPlane_var('data', {'Ey'}, 'z', 'normal');
save3DsimPlane_var('data', {'Ez'}, 'y', 'normal');
save3DsimPlane_var('data', {'Ez'}, 'z', 'normal');
save3DsimPlane_var('data', {'Bx'}, 'y', 'normal');
save3DsimPlane_var('data', {'Bx'}, 'z', 'normal');
save3DsimPlane_var('data', {'By'}, 'y', 'normal');
save3DsimPlane_var('data', {'By'}, 'z', 'normal');
save3DsimPlane_var('data', {'Bz'}, 'y', 'normal');
save3DsimPlane_var('data', {'Bz'}, 'z', 'normal');
save3DsimPlane_var('data', {'Number_Density/electron'}, 'y', 'normal');
save3DsimPlane_var('data', {'Number_Density/electron'}, 'z', 'normal');

save3Dsim_LongIntegral('data', 'Ez', 'normal')


