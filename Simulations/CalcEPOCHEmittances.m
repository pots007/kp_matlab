simfolder = 'data/';
filen = dir([simfolder 'particles*.sdf']);
emittances = NaN(length(filen),4,2);
for i=1:length(filen)-10
    disp(['File ' num2str(i)]);
    specs = {'ebeam', 'shell7'};
    for k=1:1
        datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['grid/subset_highgamma/' specs{k}]);
        y = datp.Grid.Particles.subset_highgamma.(specs{k}).y*1e3;
        x = datp.Grid.Particles.subset_highgamma.(specs{k}).x;
        datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['px/subset_highgamma/' specs{k}]);
        px = datp.Particles.Px.subset_highgamma.(specs{k}).data;
        datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['py/subset_highgamma/' specs{k}]);
        py = datp.Particles.Py.subset_highgamma.(specs{k}).data;
        yprime = py./px*1e3;
        datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['gamma/subset_highgamma/' specs{k}]);
        gamma = datp.Particles.Gamma.subset_highgamma.(specs{k}).data;
        logm = (gamma > 40) & (px ~= 0) & abs(y) < 4e-3;
        y = y(logm); x = x(logm); yprime = yprime(logm); gamma = gamma(logm); px = px(logm);
        e_rms = sqrt(mean(y.^2)*mean(yprime.^2) - mean(y.*yprime)^2);
        %fprintf('%s rms emittance = %1.2e pi*um*mrad\n', specs{k}, e_rms*1e3);
        %e_rms_n = e_rms*mean(sqrt(gamma(px~=0).^2-1));
        e_rms_n = sqrt(mean(y.^2)*mean(gamma.^2.*yprime.^2) - mean(y.*gamma.*yprime)^2);
        %fprintf('%s  normalised rms emittance = %1.2f pi*um*mrad\n', specs{k}, e_rms_n*1e3);
        emittances(i,:,k) = [e_rms abs(e_rms_n) mean(gamma) datp.time];
    end

end
%%
figure(3648); clf;
set(3648, 'Color', 'w', 'Position', [200 200 800 600]);
hs = zeros(1,6); labels = cell(1,6);
x = emittances(:,4,1)*c*1e3; %% Distance in mm
ms = {'x', 'o'};
labs = {' \epsilon_{RMS}', ' \epsilon_{RMS n}'};
norm_emit_fac = 1000;
for k=1:1
    hs((k-1)*2+1) = plot(x, emittances(:,2,k), ['r-' ms{k}], 'MarkerSize', 10, 'LineWidth', 1.5);
    labels{(k-1)*2+1} = [specs{k} labs{1}];
    hold on;
    hs((k-1)*2+2) = plot(x, norm_emit_fac*emittances(:,1,k), ['k-' ms{k}], 'MarkerSize', 10, 'LineWidth', 1.5);
    labels{(k-1)*2+2} = [specs{k} labs{2}];
    %hs((k-1)*2+3) = plot(x, emittances(:,3,k), ['g-' ms{k}]);
    
end
hold off;
ylabel({'Normalised RMS emittance / mm mrad', [num2str(norm_emit_fac) ' \times RMS emittance / mm mrad']});
legend(hs(hs~=0), labels(hs~=0), 'Location', 'NorthEast');
xlabel('Propagation distance / mm');
setAllFonts(3648, 20);

%% Saving part 
savfol = '/Volumes/ATA2_2013/ATA2II/';
title('Low a_0 II emittance evolution');
fname = 'II_low_emittance';
setAllFonts(3648, 20);
export_fig([savfol fname], '-nocrop', '-pdf', 3648);