datal = '/Volumes/KRISMOOSE3/EPOCH43V42/';
fols = {'EPOCH429', 'EPOCH4310'};
mkdir([datal 'plots']);
locs = GetFigureArray(2, 3, 0.05, 0.05, 'down');
fils = zeros(size(fols));
for i=1:length(fols)
    fils(i) = length(dir([datal fols{i} '/data/0*.sdf']));
end
axs = zeros(length(fols)*3,1);
for i=1:15%max(fils)
    figure(455); clf;  
    it = 1;
    for k=1:length(fols)
        
        filn = [datal fols{k} '/data/' sprintf('%04i.sdf', i-1+(k-1)*1)];
        axs(it) = axes('Position', locs(:,it));
        try
            EVPlotIntensity(axs(it), filn, 0);
            cb = colorbar; ylabel(cb, 'Intensity / 10^{18} Wcm^{-2}');
            %title('Laser intensity');
        catch
        end
        title(fols{k});
        it = it+1;
        axs(it) = axes('Position', locs(:,it));
        try
            EVPlotDensity(axs(it), filn, 'electron');
            cb = colorbar; ylabel(cb, 'n_e / n_c');
            title('Electron density');
        catch            
        end
        it = it+1; axs(it) = axes('Position', locs(:,it));
        try
            EVPlotDensity(axs(it), filn, 'nitrogen');
            cb = colorbar; ylabel(cb, 'n_N / n_c');
            title('Nitrogen density');
        catch 
        end
        it = it+1;
    end
    drawnow;
    set(axs([1,2,4,5]), 'XTick', []); ylabel(axs(1), 'y / \mu m');ylabel(axs(2), 'y / \mu m');ylabel(axs(3), 'y / \mu m');
    set(axs(4:6), 'YTick', []); xlabel(axs(3), 'x / \mu m'); xlabel(axs(6), 'x / \mu m');
    setAllFonts(455,16);
    export_fig(sprintf('%splots/%04i', datal, i-1), '-nocrop', '-transparent', '-png', 455);
end

