% So we'll try to plot some of the images!
% Let's do all the images, zoomed into the middle first.

if ispc
    dataf = 'E:\Kristjan\Google Drive\GEANT4\PepperpotSims/YAGdet';
elseif ismac
    dataf = '/Users/kristjanpoder/Google Drive/GEANT4/PepperpotSims/YAGdet';
end
if ~exist(fullfile(dataf, 'plots', 'pdf'), 'dir')
    mkdir(fullfile(dataf, 'plots', 'pdf'));
    mkdir(fullfile(dataf, 'plots', 'png'));
end
flist = dir(fullfile(dataf,'*.root'));
fnames = {flist.name};

% Import the data into a 3D matrix;
hitdata = zeros(1000,1000,length(fnames));
for i=1:length(flist)
    hitdata(:,:,i) = importdata(fullfile(dataf,[fnames{i}(1:end-5) '_hits.txt']));
end

%% Now imagesc all the files, and save nicer images

hfig = 70;
figure(hfig); clf(hfig);
set(hfig, 'Position', [100 100 800 750], 'Color', 'w');
ax = axes('Parent', hfig, 'Position', [0.00 0.02 0.9 0.9]);
zoom_log = 200:800;
cmap = brewermap(256, 'YlGnBu');
colormap(hfig, cmap);
for i=1:size(hitdata,3)
    cla(ax);
    imagesc(hitdata(zoom_log,zoom_log,i), 'Parent', ax);
    set(ax, 'YTick', [], 'XTick', []);
    axis image;
    cb = colorbar('Peer', ax, 'Position', [0.88 0.02 0.02 0.9]);
    ylabel(cb, 'Energy deposition / MeV');
    title(ax, sprintf('$\\sigma_{x,y} = %s \\:\\mathrm{um}, \\sigma_{x'',y''} = %i \\: \\mathrm{mrad}$', ...
        fnames{i}(3), str2double(fnames{i}(8:9))));
    setAllFonts(hfig, 20);
    make_latex(hfig);
    drawnow;
    export_fig(fullfile(dataf, 'plots', 'pdf', fnames{i}(1:end-5)), '-pdf', '-nocrop', hfig);
    export_fig(fullfile(dataf, 'plots', 'png', fnames{i}(1:end-5)), '-png', '-nocrop', hfig);
end

%% And now let's plot lineouts along the x-axis for scans: sourcesizes first

hfig = 71; hfigcoll = 72;
figure(hfig); clf(hfig);
figure(hfigcoll); clf(hfigcoll);
set(hfig, 'Position', [100 100 1100 700], 'Color', 'w');
ax = axes('Parent', hfig, 'Position', [0.1 0.1 0.8 0.8], 'NextPlot', 'add');
locs = GetFigureArray(2, 2, [0.07 0.03 0.03 0.07], 0.07, 'down');
zoom_logx = 400:600;
zoom_logy = 450:550;
colz = 'krgb';
for k=1:4
    cla(ax);
    hplot = zeros(1,4);
    for i=1:4
        ind = i+(k-1)*4; % This does divergences!
        lineout = hitdata(zoom_logy, zoom_logx, ind);
        lineout = mean(lineout,1);
        hplot(i) = plot(ax, zoom_logx*0.013, lineout, ['--' colz(i)], 'LineWidth', 1.5);
        titl{i} = sprintf('$\\sigma_{x'',y''} = %i\\: \\mathrm{mrad}$',...
            str2double(fnames{ind}(8:9)));
    end
    set(ax, 'Box', 'on', 'LineWidth', 2);
    ylabel('Energy deposition / MeV');
    xlabel('Position on scintillator / mm');
    title(ax, ['$\sigma_{x,y} = ' fnames{ind}(3) '\:\mathrm{um}$']);
    setAllFonts(hfig,20);
    legend(hplot, titl);
    make_latex(hfig);
    drawnow;
    %export_fig(fullfile(dataf, 'plots', fnames{ind}(1:3)), '-pdf', '-nocrop', hfig);
    % And now collated plot
    %axs(k) = axes('Parent', hfigcoll, 'Position', locs(:,k));
    if (k==1); clf(hfigcoll); end;
    axs(k) = copyobj(ax, hfigcoll);
    set(axs(k), 'Position', locs(:,k) + [0.02 0 0 0]');
    set(get(axs(1), 'XLabel'), 'String', '');
    %axis(axs(k), 'tight');
end
linkaxes(axs, 'xy');
set(hfigcoll, 'Position', [100 100 1100 700], 'Color', 'w');
set(axs, 'XTick', []);
set(axs(3:4), 'YTick', []);
hleg = legend(axs(4), hplot, titl);
make_latex(72);
set(hleg, 'Position', [0.45 0.4 0.18 0.16]);
export_fig(fullfile(dataf, 'plots', fnames{ind}(1:2)), '-pdf', '-nocrop', hfigcoll);

%% And now let's plot lineouts along the x-axis for scans: Now divergences

hfig = 71; hfigcoll = 72;
figure(hfig); clf(hfig);
figure(hfigcoll); clf(hfigcoll);
set(hfig, 'Position', [100 100 1100 700], 'Color', 'w');
ax = axes('Parent', hfig, 'Position', [0.1 0.1 0.8 0.8], 'NextPlot', 'add');
locs = GetFigureArray(2, 2, [0.07 0.03 0.03 0.07], 0.07, 'down');
zoom_logx = 400:600;
zoom_logy = 450:550;
colz = 'krgb';
for k=1:4
    cla(ax);
    hplot = zeros(1,4);
    for i=1:4
        ind = k+(i-1)*4; % This does source sizes!
        lineout = hitdata(zoom_logy, zoom_logx, ind);
        lineout = mean(lineout,1);
        hplot(i) = plot(ax, zoom_logx*0.013, lineout, ['--' colz(i)], 'LineWidth', 1.5);
        titl{i} = sprintf('$\\sigma_{x,y} = %i\\: \\mathrm{um}$',...
            str2double(fnames{ind}(3)));
    end
    set(ax, 'Box', 'on', 'LineWidth', 2);
    ylabel('Energy deposition / MeV');
    xlabel('Position on scintillator / mm');
    title(ax, ['$\sigma_{x'',y''} = ' sprintf('%i', str2double(fnames{ind}(8:9))) '\:\mathrm{mrad}$']);
    setAllFonts(hfig,20);
    legend(hplot, titl);
    make_latex(hfig);
    drawnow;
    export_fig(fullfile(dataf, 'plots', fnames{ind}(4:9)), '-pdf', '-nocrop', hfig);
    % And now collated plot
    %axs(k) = axes('Parent', hfigcoll, 'Position', locs(:,k));
    if (k==1); clf(hfigcoll); end;
    axs(k) = copyobj(ax, hfigcoll);
    set(axs(k), 'Position', locs(:,k) + [0.02 0 0 0]');
    set(get(axs(1), 'XLabel'), 'String', '');
    %axis(axs(k), 'tight');
end
linkaxes(axs, 'x');
set(hfigcoll, 'Position', [100 100 1100 700], 'Color', 'w');
set(axs, 'XTick', []);
set(axs(3:4), 'YTick', []);
hleg = legend(axs(4), hplot, titl);
make_latex(72);
set(hleg, 'Position', [0.45 0.4 0.18 0.16]);
export_fig(fullfile(dataf, 'plots', 'thet'), '-pdf', '-nocrop', hfigcoll);
