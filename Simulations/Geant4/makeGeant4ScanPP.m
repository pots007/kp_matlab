%% Make runscripts for G4 sims for pepperpot runs

if ~ismac
    return
end
basefol = '/Users/kristjanpoder/geant4data/PepperPotScans/scripts/';
if ~exist([basefol 'macros'], 'dir')
    mkdir([basefol 'macros']);
end

runscript0 = {'#!/bin/sh',...
    '#PBS -N G4',...
    '#PBS -l walltime=24:00:00',...
    '#PBS -l select=1:ncpus=1:mem=3000mb',...
    '',...
    'module load intel-suite',...
    '',...
    'OUTDIR=',...
    'mkdir -p $OUTDIR/data',...
    '',...
    'cp $HOME/G4queuePP/bin/PPsims $OUTDIR/PPsims',...
    'cp $HOME/G4queuePP/macros/ $OUTDIR/run.mac',...
    '',...
    'cd $OUTDIR',...
    './PPsims run.mac >> log.txt',...
    'cd ..',...
    'cp -r $OUTDIR/data/*.root $WORK/PPsim/'};

macro0 = {'/PPsims/gun/setSourceSize ',...
    '/PPsims/gun/setDivergenceAngle ',...
    '/PPsims/histo/setCCDOnly 1',...
    '/PPsims/det/setTapeThickness 75 um',...
    '/PPsims/det/setHoleSeparation ',...
    '/PPsims/det/setHoleR1 ',...
    '/PPsims/det/setHoleR2 ',...
    '/PPsims/histo/setHisto 5 500 0 25e-3',...
    '/PPsims/histo/setHisto 1 500 0 500',...
    '/run/beamOn 2000000'};

% Central energies: runs of 200MeV range upt o 2000, then 0:50 and 0:1000
% and 0:2000

divangles = 6:2:12;
sourcesizes = 1:1:4;
radii = [15 30 60 120];
separations = [150 250 400];
% Make the runscripts to manage file copying etc
% Make the macro files in the same loop as well.
it = 1;
% And make a file to contain runscript names
fid2 = fopen([basefol 'runnames.txt'], 'w');
for a=1:4 % sourcesize iterator
    for b=1:4 % divergence angle iterator
        for c=1:3 % hole separation iterator
            for d = 1:4 % hole radius iterator
                currunscript = runscript0;
                currun = sprintf('cfg%i', it); it = it+1;
                curfol = sprintf('ss%ithet%02i_sep%03i_r%03i', sourcesizes(a), divangles(b), separations(c), radii(d));
                fprintf(fid2, '%s.sh\n', curfol);
                currunscript{2} = [currunscript{2} currun];
                currunscript{8} = [currunscript{8} curfol];
                %currunscript{11} = ['cp $HOME/G4queue/bin/' currun(1:4) ' $OUTDIR/PPsims'];
                currunscript{12} = ['cp $HOME/G4queuePP/macros/' curfol '.mac  $OUTDIR/run.mac'];
                currunscript{17} = [currunscript{17} curfol '.root'];
                % Save the runscript
                fid = fopen([basefol curfol '.sh'], 'w');
                for l=1:length(currunscript)
                    fprintf(fid, '%s\n', currunscript{l});
                end
                fclose(fid);
                curmacro = macro0;
                curmacro{1} = sprintf('%s %i um', curmacro{1}, sourcesizes(a));
                curmacro{2} = sprintf('%s %i mrad', curmacro{2}, divangles(b));
                curmacro{5} = sprintf('%s %i um', curmacro{5}, separations(c));
                curmacro{6} = sprintf('%s %i um', curmacro{6}, radii(d));
                curmacro{7} = sprintf('%s %i um', curmacro{7}, radii(d));
                
                fid = fopen([basefol 'macros/' curfol '.mac'], 'w');
                for l=1:length(curmacro)
                    fprintf(fid, '%s\n', curmacro{l});
                end
                fclose(fid);
            end
        end
    end
end
fclose(fid2);
%% Install all files into cx1

fillist = dir(basefol);
for i=3:length(fillist)
    disp(fillist(i).name);
    copyfile([basefol fillist(i).name], ['/Volumes/CX1home/G4queuePP/' fillist(i).name]);
end