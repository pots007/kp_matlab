function lala = plotGeantSPCmomenta(filename)
filename = '/Users/kristjanpoder/Dropbox/Geant4_sims/GeminiShielding/test4_momenta.txt';
figure(78); clf;
% All units in m
%First chamber
hchaminner = drawBox([-0.1126,0.076, 0.4255], 2*[0.725,0.625,1.35]);
set(hchaminner, 'FaceAlpha', 0.25);
hold on;
hchamouter = drawBox([-0.1126,0.076, 0.4255], 2*(0.06+[0.725,0.625,1.35]));
set(hchamouter, 'FaceAlpha', 0.25);
hflange1 = drawBox([0.1374, -0.0245, 0.4255+1.35+0.0175+0.06], ...
    2*[0.4985, 0.4985,0.0175]);
set(hflange1, 'FaceAlpha', 0.25, 'EdgeColor', 'g');
hflange2 = drawBox([-0.101, -.009, 0.4255+1.35+0.06+0.035+0.0105],...
    2*[0.184,0.334,0.0105]);
set(hflange2, 'FaceAlpha', 0.05, 'EdgeColor', 'k', 'FaceColor', 'none');
hCCD = drawBox([0,0,3.5], [0.013, 0.013, 1e-4]);
set(hCCD, 'FaceAlpha', 0.4, 'EdgeColor', 'm', 'FaceColor', 'm');
hPbwall = drawBox([0,-0.108,4.5],2*[1,0.35,0.4]);
set(hPbwall, 'FaceAlpha', 0.2, 'EdgeColor', 'g', 'FaceColor', 'none');
%
%view(-100,130);
view(3)
%view([.1,1,.1]);
hs = [hflange2(6) hflange1(6) hchamouter(6)];
%Now read in the file and momenta
la = importdata(filename);
% Columns are: px, py, pz, Edep, xp, yp;
P1s = zeros(size(la,1),3);
for k=1:size(la,1)
    P0 = 13e-6*(500 - [la(k,5) la(k,6)]);
    P0(3) = 3.5; %All coming from CCD plane
    if la(k,3)>0 %came frm TCC side
        for l=1:length(hs)
            zp = max(get(hs(l), 'ZData'));
            P1(1) = P0(1) + la(k,1)/la(k,3)*(zp-P0(3));
            P1(2) = P0(2) + la(k,2)/la(k,3)*(zp-P0(3));
            P1(3) = zp;
            xs = (get(hs(l), 'XData')); ys = (get(hs(l), 'YData'));
            if P1(1)>max(xs) || P1(1)<min(xs) || P1(2)>max(ys) || P1(2)<min(ys)
                continue;
            else
                P1s(k,:) = P1; 
                break;
            end
        end
        if (P1s(k,3)==0) 
            if (abs(P1(1))>1.5) P1(1) = 1.5; end;
            if (abs(P1(2))>1.5) P1(2) = 1.5; end;
            P1s(k,:) = P1;
        end;
    else
        zp = min(get(hPbwall(5), 'ZData'));
        P1(1) = P0(1) + la(k,1)/la(k,3)*(zp-P0(3));
        P1(2) = P0(2) + la(k,2)/la(k,3)*(zp-P0(3));
        P1(3) = zp;
        if (abs(P1(1))>1.5) P1(1) = 1.5; end;
        if (abs(P1(2))>1.5) P1(2) = 1.5; end;
        P1s(k,:) = P1;
    end
    
end
load('owncolourmaps.mat');
colm = colmap.fireice;
%cols = interp1(linspace(min(la(:,4)),max(la(:,4)),size(colm,1)), colm, la(:,4));
scatter3(P1s(:,1),P1s(:,2),P1s(:,3), 15, 'Cdata', la(:,4));
colormap(colm);
cb = colorbar;
hold off; axis image;
fracrear = (sum(la(:,3)<0))/size(la,1);
fprintf('Fraction of stuff from %7s: %0.3f\n', 'rear', fracrear);
fprintf('Fraction of stuff from %7s: %0.3f\n', 'front', 1-fracrear);
xlabel('x / m'); ylabel('y / m');
ylabel(cb,'Energy deposited in CCD / MeV');
axis(gca, 'vis3d')

set(gca, 'ZLim', [0 4], 'CLim', [0 0.04]);
lala = 78;
[path,base,~] = fileparts(filename);
set(gca, 'CameraPosition', [-17 11 22], 'CameraUpVector', [0.23 0.9 -0.34],...
    'CameraTarget', [0.07 -0.08 1.91])
export_fig([path '/' base], '-nocrop', '-pdf', '-CMYK', '-transparent', 78);
end


function h = drawBox ( origin, size )
x=([0 1 1 0 0 0;1 1 0 0 1 1;1 1 0 0 1 1;0 1 1 0 0 0]-0.5)*size(1)+origin(1);
y=([0 0 1 1 0 0;0 1 1 0 0 0;0 1 1 0 1 1;0 0 1 1 1 1]-0.5)*size(2)+origin(2);
z=([0 0 0 0 0 1;0 0 0 0 0 1;1 1 1 1 0 1;1 1 1 1 0 1]-0.5)*size(3)+origin(3);
h = zeros(1,6);
for i=1:6
    h(i)=patch(x(:,i),y(:,i),z(:,i),'w');
    set(h(i),'edgecolor','r')
end
end