% When run, checks the cx1 work directory and copies over 30 files from
% each different run. 
% Then hopefully runs root to do extraction as well...

workfol = '/Volumes/work/GeantRun/';
datafol = '/Users/kristjanpoder/geant4data/GeminiShieldingScan/data/';
compruns = dir([workfol 'cfg*']);
% Loop through finished jobs
for i=1:length(compruns) 
    % Check how many root files are in that folder and copy 30.
    % If less than copy, notify the need to do more!
    compfiles = dir([workfol compruns(i).name '/*.root']);
    if length(compfiles) < 30
        fprintf('\tDo %i more simulations for %s !!!\n', 30-length(compfiles), compruns(i).name);
    end
    if ~exist([datafol compruns(i).name], 'dir')
        mkdir([datafol compruns(i).name]);
    end
    maxfiles = min([30 length(compfiles)]);
    for k=1:maxfiles
        copyfile([workfol compruns(i).name '/' compfiles(k).name], ...
            [datafol compruns(i).name '/' compfiles(k).name]);
    end
    % Copy the analysis files over as well...
    copyfile([workfol 'CCDanalysis3.C'], [datafol compruns(i).name '/CCDanalysis3.C'])
    copyfile([workfol compruns(i).name '/log.txt'], [datafol compruns(i).name '/log.txt'])
    copyfile([datafol 'CCDextract2.C'], [datafol compruns(i).name '/CCDextract2.C']);
    if maxfiles == 30
        fprintf('Simulation %s is copied successfully.\n', compruns(i).name);
    else
        fprintf('%i files of %s copied successfully. Do %i more!\n',...
            maxfiles, compruns(i).name, 30-maxfiles);
    end
end