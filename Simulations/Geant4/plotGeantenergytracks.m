function lala = plotGeantenergytracks(filename)
filename = '/Users/kristjanpoder/Dropbox/Geant4_sims/GeminiShielding/EnTracks.txt';
figure(78); clf;
% All units in m
%First chamber
hchaminner = drawBox([-0.1126,0.076, 0.4255], 2*[0.725,0.625,1.35]);
set(hchaminner, 'FaceAlpha', 0.25);
hold on;
hchamouter = drawBox([-0.1126,0.076, 0.4255], 2*(0.06+[0.725,0.625,1.35]));
set(hchamouter, 'FaceAlpha', 0.25);
hflange1 = drawBox([0.1374, -0.0245, 0.4255+1.35+0.0175+0.06], ...
    2*[0.4985, 0.4985,0.0175]);
set(hflange1, 'FaceAlpha', 0.25, 'EdgeColor', 'g');
hflange2 = drawBox([-0.101, -.009, 0.4255+1.35+0.06+0.035+0.0105],...
    2*[0.184,0.334,0.0105]);
set(hflange2, 'FaceAlpha', 0.05, 'EdgeColor', 'k', 'FaceColor', 'none');
hCCD = drawBox([0,0,3.5], [0.013, 0.013, 1e-4]);
set(hCCD, 'FaceAlpha', 0.4, 'EdgeColor', 'm', 'FaceColor', 'm');
hPbwall = drawBox([0,-0.108,4.5],2*[1,0.35,0.4]);
set(hPbwall, 'FaceAlpha', 0.2, 'EdgeColor', 'g', 'FaceColor', 'none');
hPbshield = drawBox([0,0,3.5],2*[.1,0.15,0.15]);
set(hPbshield, 'FaceAlpha', 0.2, 'EdgeColor', 'g', 'FaceColor', 'none', 'LineStyle', ':');
hPbshield2 = drawBox([0,0,3.5],2*[.1,0.2,0.2]);
set(hPbshield2, 'FaceAlpha', 0.2, 'EdgeColor', 'k', 'FaceColor', 'none', 'LineStyle', '-.');
%view(-100,130);
view(3)
%view([.1,1,.1]);
hs = [hflange2(6) hflange1(6) hchamouter(6)];
%Now read in the file and momenta
la = importdata(filename);
lal = sortrows(la, 4);
cols = jet(20);
for i=1:0.5*size(lal,1)
    if lal(2*i,5) == lal(2*i-1,5)
        disp(['Only one hit point for energy ' num2str(lal(2*i,4))]);
        continue;
    end
    slope = (lal(2*i,2)-lal(2*i-1,2))/(lal(2*i,3)-lal(2*i-1,3));
    if lal(2*i,5)>lal(2*i-1,5)
        Zs = [lal(2*i-1,3) lal(2*i,3) 4500];
        Ys = [lal(2*i-1,2) lal(2*i,2) lal(2*i,2)+abs(slope)*(4500-lal(2*i,3))];
    else
        Zs = [lal(2*i,3) lal(2*i-1,3) 4500];
        Ys = [lal(2*i,2) lal(2*i-1,2) lal(2*i,2)+abs(slope)*(4500-lal(2*i,3))];
    end
    plot3([0 0 0], Ys*0.001, Zs*0.001, '-o', 'Color', cols(i,:));
    text(0, Ys(3)*0.001, 4.7, num2str(round(lal(2*i,4))));
end
xlabel('x / m'); ylabel('y / m'); zlabel('z / m');
%cb = colorbar;
%set(cb, 'YTick', 0.:0.1:1, 'YLim', [0 1], 'YTickLabel', num2str((0:200:2000)'),...
%    'Position', [0.9 0.11 0.01 0.8]);
%ylabel(cb, 'Electron energy / MeV');
%axis(gca, 'vis3d')

set(gca, 'ZLim', [0 5], 'YLim', [-0.2 1], 'Box', 'on');
set(gca, 'CameraPosition', [30 0.4 2.4], 'CameraUpVector', [1 1 0],'CameraTarget', [0 0.4 2.4])
setAllFonts(78,20);
[path,base,~] = fileparts(filename);
%set(gca, 'CameraPosition', [-17 11 22], 'CameraUpVector', [0.23 0.9 -0.34],...
%    'CameraTarget', [0.07 -0.08 1.91])
export_fig([path '/' base], '-nocrop', '-pdf', '-CMYK', '-transparent', 78);
end


function h = drawBox ( origin, size )
x=([0 1 1 0 0 0;1 1 0 0 1 1;1 1 0 0 1 1;0 1 1 0 0 0]-0.5)*size(1)+origin(1);
y=([0 0 1 1 0 0;0 1 1 0 0 0;0 1 1 0 1 1;0 0 1 1 1 1]-0.5)*size(2)+origin(2);
z=([0 0 0 0 0 1;0 0 0 0 0 1;1 1 1 1 0 1;1 1 1 1 0 1]-0.5)*size(3)+origin(3);
h = zeros(1,6);
for i=1:6
    h(i)=patch(x(:,i),y(:,i),z(:,i),'w');
    set(h(i),'edgecolor','r')
end
end