%% Make energy and config and shielding dependent runscripts for G4 sims

if ~ismac
    return
end
basefol = '/Users/kristjanpoder/geant4data/GeminiShieldingScan/scripts/';
if ~exist([basefol 'macros'], 'dir')
    mkdir([basefol 'macros']);
end

runscript0 = {'#!/bin/sh',...
    '#PBS -N G4',...
    '#PBS -l walltime=24:00:00',...
    '#PBS -l select=1:ncpus=1:mem=3000mb',...
    '',...
    'module load intel-suite',...
    '',...
    'OUTDIR=',...
    'mkdir -p $OUTDIR',...
    '',...
    'cp $HOME/G4queue/bin/ $OUTDIR/GeminiShielding',...
    'cp $HOME/G4queue/macros/ $OUTDIR/run.mac',...
    '',...
    'cd $OUTDIR',...
    './GeminiShielding run.mac >> log.txt',...
    'cd ..',...
    'cp -r $OUTDIR $WORK/GeantRun/'};

macro0 = {'/GeminiShielding/det/toggleShieldingBox ',...
    '/GeminiShielding/gun/setEnergyRange ', ...
    '/GeminiShielding/gun/setCentralEnergy ',...
    '/GeminiShielding/histo/setCCDOnly 1',...
    '#/GeminiShielding/det/setTapeLocation 1',...
    '/GeminiShielding/det/setTapeThickness 10 um',...
    '#/GeminiShielding/det/setTapeMaterial Kapton',...
    '#/GeminiShielding/det/setFlangeMaterial Plastic',...
    '/GeminiShielding/det/setFlangeMaterial Aluminium',...
    '/GeminiShielding/histo/setHisto 5 500 0 1e-2',...
    '/GeminiShielding/histo/setHisto 1 500 ',...
    '/run/beamOn 5000000'};

% Central energies: runs of 200MeV range upt o 2000, then 0:50 and 0:1000
% and 0:2000

cenens = [100:200:1900 25 500 1500 1000];
enranges = [ones(1,10)*200 50 1000 1000 2000];
simulationenergies = [cenens' enranges'];

% Make the runscripts to manage file copying etc
% Make the macro files in the same loop as well.

%Configurations:
% 0 - Al flange
% 1 - Plastic flange
% 2 - behind bunker CCD Al flange
% 3 - Al flange, tapedrive 20um Al at 100mm
% 4 - Al flange, tapedrive 20um Kapton at 100mm

for i=0:5
    for j=0:4
        for k=1:length(cenens)
            currunscript = runscript0;
            currun = sprintf('cfg%iS%i', i, j);
            curfol = sprintf('%s_%04i_%04i',currun, cenens(k)-0.5*enranges(k),cenens(k)+0.5*enranges(k));
            currunscript{2} = [currunscript{2} currun];
            currunscript{8} = [currunscript{8} curfol];
            %currunscript{11} = ['cp $HOME/G4queue/bin/' currun(1:4) ' $OUTDIR/GeminiShielding'];
            currunscript{11} = ['cp $HOME/G4queue/bin/cfg' num2str(i==2) ' $OUTDIR/GeminiShielding'];
            currunscript{12} = ['cp $HOME/G4queue/macros/' curfol '.mac  $OUTDIR/run.mac'];
            % Save the runscript
            fid = fopen([basefol curfol '.sh'], 'w');
            for l=1:length(currunscript)
                fprintf(fid, '%s\n', currunscript{l});
            end
            fclose(fid);
            curmacro = macro0;
            curmacro{1} = sprintf('%s %i', curmacro{1}, j);
            curmacro{2} = sprintf('%s %i MeV', curmacro{2}, enranges(k));
            curmacro{3} = sprintf('%s %i MeV', curmacro{3}, cenens(k));
            curmacro{11} = sprintf('%s %i %i', curmacro{11}, cenens(k)-0.5*enranges(k),cenens(k)+0.5*enranges(k));
            % Now the configs
            if (i==1)
                curmaco{8} = curmacro{8}(2:end);
            end
            if (i==3 || i==4 || i==5)
                curmacro{5} = curmacro{5}(2:end);
            end
            if (i==4)
                curmacro{7} = curmacro{7}(2:end);
            end
            if (i==5)
                curmacro{6} = [curmacro{6}(1:end-5) '0.5 um'];
                curmacro{7} = '/GeminiShielding/det/setTapeMaterial Iron';
            end
            fid = fopen([basefol 'macros/' curfol '.mac'], 'w');
            for l=1:length(curmacro)
                fprintf(fid, '%s\n', curmacro{l});
            end
            fclose(fid);
        end
    end
end

%% Install all files into cx1

fillist = dir(basefol);
for i=3:length(fillist)
    disp(fillist(i).name);
    copyfile([basefol fillist(i).name], ['/Volumes/CX1home/G4queue/' fillist(i).name]);
end