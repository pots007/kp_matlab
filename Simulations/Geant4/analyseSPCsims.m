
%For the 2015 Jan SPC Geant4 runs. These already need the 'C' tree to be
%present.

datafol = '/Users/kristjanpoder/geant4data/GeminiShieldingScan/data/';
savefol = '/Users/kristjanpoder/geant4data/GeminiShieldingScan/analysis/';
%sims = {'cfg0S0_0000_1000', 'No shielding, Al flange';...
%sims = {    'cfg1S0_0000_1000', 'No shielding, plastic flange';...
sims = {'cfg6S0_0000_1000', 'All SPC stuff, Al flange';...
    'cfg3S0_0000_1000', ' Al flange, 20um Al tape @ 100mm';...
    'cfg7S0_0000_1000', 'All SPC stuff, 1cm@2T magnet';...
    'cfg5S0_0000_1000', 'Al flange, 1um Fe @ 100 mm'};
    %'cfg4S0_0000_1000', 'Al flange, 20um Kapton @ 100mm';...
% Columns of data are:
% 1 - CCD hits locations along with Edep
% 2 - z-momenta of hitting particles
% 3 - y-momenta of hitting particles
% 4 - z-positions of creation vertices
% 5 - y-positions of creation vertices
% 6 - total momenta of hitting particles
% 7 - energies of the initial electrons
shdata = cell(size(sims,1), 7);
for i=1:size(sims,1)
    locdat = importdata([datafol sims{i,1} '/out_momentaC.txt']);
    shdata{i,1} = [locdat(:,5) locdat(:,6) locdat(:,4)];
    shdata{i,2} = locdat(:,3);
    shdata{i,3} = locdat(:,2);
    shdata{i,4} = locdat(:,9);
    shdata{i,5} = locdat(:,8);
    shdata{i,6} = sqrt(locdat(:,1).^2+locdat(:,2).^2+locdat(:,3).^2);
    if (size(locdat,2)==10)
        shdata{i,7} = locdat(:,10);
    end
end

%% Plot the hit maps

saveplot = 1;
locs = GetFigureArray(2,2,0.05,0.05,'down');
figure(44); clf; set(44, 'Position', [1000 350 1050 990], 'Color', 'w');
axs = zeros(1, size(sims,1));
for i=1:length(sims)
    axs(i) = axes('Parent', 44, 'Position', locs(:,i));
    locdat = shdata{i,1};
    scatter(locdat(:,1), locdat(:,2), 30, '.k');
    title([sims{i,2} sprintf(': %i hits', size(locdat,1))]);
end
set(axs, 'XTick', [], 'YTick', [], 'Box', 'on');
setAllFonts(44,18);
if saveplot
    export_fig([savefol 'hitmap'], '-pdf', '-nocrop', 44);
end

%% Plot the hit maps with energy depositions

saveplot = 0;
locs = GetFigureArray(2,2,0.05,0.05,'down');
figure(44); clf; set(44, 'Position', [880 350 1160 990]);
axs = zeros(1, size(sims,1));
for i=1:length(sims)
    axs(i) = axes('Parent', 44, 'Position', locs(:,i));
    locdat = shdata{i,1};
    scatter(locdat(:,1), locdat(:,2), 30, 1e3*locdat(:,3), '.');
    title([sims{i,2} sprintf(': %i hits', size(locdat,1))]);
    cb = colorbar;
    ylabel(cb, 'Energy deposition / keV');
    set(gca, 'CLim', [0 0.2*max(1e3*locdat(:,3))]);
end
set(axs, 'XTick', [], 'YTick', [], 'Box', 'on');
setAllFonts(44,18);
if saveplot
    export_fig([savefol 'hitmap_energies'], '-pdf', '-nocrop', 44);
end

%% Plot the p_tot of the hitting particles, their pz-s and py-s and z0-s all together

saveplot = 0;
locs = GetFigureArray(2,2,0.07,0.07,'down');
figure(44); clf; set(44, 'Position', [880 350 1160 990]);
for i=1:4
    axs(i) = axes('Parent', 44, 'Position', locs(:,i), 'NextPlot', 'add', 'Box', 'on');
end
cols = lines(4);
for k=1:4
    for i=1:length(sims)
        if k==1 ind = 6; end;
        if k==2 ind = 3; end;
        if k==3 ind = 2; end;
        if k==4 ind = 4; end;
        locdat = shdata{i,ind};
        [N,X] = hist(locdat, 200);
        hs(i) = plot(axs(k), X, N./max(N), '-', 'Color', cols(i,:));
        
        drawnow;
    end
    if k==1 xlabel(axs(k), 'Total momentum / MeV c^{-1}'); end
    if k==2 xlabel(axs(k), 'p_z / MeVc^{-1}'); end
    if k==3 xlabel(axs(k), 'p_y / MeVc^{-1}'); end
    if k==4 xlabel(axs(k), 'z_0 / mm'); end
    ylabel(axs(k), 'Counts (normalised)');
    legend(hs, {sims{1,2}; sims{2,2}; sims{3,2}; sims{4,2}}, 'Location', 'NorthEast');
    %set(axs(k), 'YLim', [0 200]);
end
%set(axs, 'XTick', [], 'YTick', [], 'Box', 'on');
setAllFonts(44,18);
if saveplot
    export_fig([savefol 'ParentData'], '-pdf', '-nocrop', 44);
end

%% Plot the p_tot of the hitting particles, their pz-s and py-s and z0-s individually

figure(44); clf; set(44, 'Position', [880 350 1000 790]);
axs = axes('Parent', 44, 'NextPlot', 'add', 'Box', 'on');
fnames = {'p_tot', 'p_z', 'p_y', 'z_0', 'Endep'};

plott = 5;
saveplot = 0;
hs = zeros(4,1);
cols = lines(4);
for k=plott:plott
    for i=1:length(sims)
        if k==1 ind = 6; end;
        if k==2 ind = 3; end;
        if k==3 ind = 2; end;
        if k==4 ind = 4; end;
        locdat = shdata{i,ind};
        if k==5 locdat = shdata{i,1}(:,3)*1e3; end;
        [N,X] = hist(locdat, 200);
        hs(i) = stairs(axs, X, N./max(1), '-', 'Color', cols(i,:));
        drawnow;
    end
    if k==1 xlabel(axs, 'Total momentum / MeV c^{-1}'); end
    if k==2 xlabel(axs, 'p_z / MeVc^{-1}'); end
    if k==3 xlabel(axs, 'p_y / MeVc^{-1}'); end
    if k==4 xlabel(axs, 'z_0 / mm'); end
    if k==5 xlabel(axs, 'Deposited energy / keV'); end
    ylabel(axs, 'Counts (normalised)');
    set(hs, 'LineWidth', 1.5);
    legend(hs, {sims{1,2}; sims{2,2}; sims{3,2}; sims{4,2}}, 'Location', 'NorthEast');
    
end
%set(axs, 'XTick', [], 'YTick', [], 'Box', 'on');
setAllFonts(44,18);
if saveplot
    export_fig([savefol 'ParentData_' fnames{plott}], '-pdf', '-nocrop', 44);
end
