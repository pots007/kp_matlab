% makeGem2015Sim_FourierSpotEPOCH.m
%
% This file will write an input deck that will try to simulate, in 3D, the
% actual Gemini spot shape.
%
% Note the files are as follows:
%    input.deck - this is copied straight from the cluster, used to run
%    Gem3D_5
%
%    input0.deck - modified to work as a base for the new spot
%
%    inputFourier.deck - deck ready to be run 

% This is the path where the initial input deck lives, and where the new
% one will be saved
savfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D', 'Gem3D_5', 'data');
infil = getFileText(fullfile(savfol, 'input0.deck'));
outfil = {};
% Everything will be insterted after line 33:
for k=1:32; outfil{k} = infil{k}; end;
outfil = outfil';

% Ensure the fitting has been run - using the last saved coefficients!!!!
load(fullfile(getDropboxPath, 'MATLAB', 'Simulations', 'Gem2015Simulations', 'fourierCoef'));

nmods = sqrt(length(Fcoef));
[n_, m_] = meshgrid(1:nmods, 1:nmods);
outfil{end+1} = sprintf('  Fspot1 = %+2.8f * sin(%i * xm) * sin(%i * ym)',...
        Fcoef(1), n_(1), m_(1));

for k=2:nmods^2
    outfil{end+1} = sprintf('  Fspot%i = Fspot%i %+2.8f * sin(%i * xm) * sin(%i * ym)',...
        k, k-1, Fcoef(k), n_(k), m_(k));
end

% And put in the rest of the input deck:
for k=33:length(infil); outfil{end+1} = infil{k}; end;

% But ensure that we normalise everything to the total energy contained in
% the spot - need to integrate intensity pattern over time and space.
% Should have saved this in the fitting script.
peakI = 11/(45e-15*sqrt(pi/4*log(2))*imSumdxdy*1e-12)

fid = fopen(fullfile(savfol, 'inputFourier.deck'), 'w');
for k=1:length(outfil)
    fprintf(fid, '%s\n', outfil{k});
end
fclose(fid);
