% Compare the number of cells and particles needed for 3D Gemini run....

% This is from the 3D ATA2 run I did:

xlim0 = [-15 50];
ylim0 = [-50 50];
ncellx0 = 30;
ncelly0 = 3;
npart0 = 2;

% Cells in x,y,z
cellsx0 = ceil(ncellx0*(xlim0(2)-xlim0(1))/0.8);
cellsy0 = ceil(ncelly0*(ylim0(2)-ylim0(1))/0.8);
cells0 = [cellsx0 cellsy0 cellsy0];
nparts0 = 152375000;
fprintf('\nTotal number of cells in ATA2 run: %i\n', prod(cells0))
fprintf('breakdown for x,y,z: %i, %i, %i\n', cells0)

% And now for Gemini, assuming same resolution
xlim1 = [-15 50];
ylim1 = [-140 140];
ncellx1 = 30;
ncelly1 = 2;
cellsx1 = ceil(ncellx1*(xlim1(2)-xlim1(1))/0.8);
cellsy1 = ceil(ncelly1*(ylim1(2)-ylim1(1))/0.8);
cells1 = [cellsx1 cellsy1 cellsy1];
fprintf('\nTotal number of cells in Gem run: %i\n', prod(cells1))
fprintf('breakdown for x,y,z: %i, %i, %i\n', cells1)
fprintf('\nRatio of cells: %2.2f\n', prod(cells1)/prod(cells0))

% And now for asymmetric Gemini, assuming same resolution
xlim3 = [-25 50];
ylim3 = [-140 140];
zlim3 = [-110 110];
ncellx3 = 25;
ncelly3 = 2;
cellsx3 = ceil(ncellx3*(xlim3(2)-xlim3(1))/0.8);
cellsy3 = ceil(ncelly3*(ylim3(2)-ylim3(1))/0.8);
cellsz3 = ceil(ncelly3*(zlim3(2)-zlim3(1))/0.8);
cells3 = [cellsx3 cellsy3 cellsz3];
fprintf('\nTotal number of cells in Gem run: %i\n', prod(cells3))
fprintf('breakdown for x,y,z: %i, %i, %i\n', cells3)
fprintf('\nRatio of cells: %2.2f\n', prod(cells3)/prod(cells1))