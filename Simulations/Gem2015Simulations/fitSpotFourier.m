% fitSpotFourier.m
%
% We take a compound focus from some run, and fit 2D Fourier modes across it.
% This should help us in running with actual intensity patterns in 3D
% simulations.

%% First, make the compound image:

saving = 0;

%focalSpotTotalSum; % Ensure this is run already.

% spotim contains the actual compound focal spot.

% make the fourier mode

%% The code below will make a nice movie to show off how the fits change
% Using sine modes

savfol = fullfile(getDropboxPath, 'MATLAB', 'Simulations', 'Gem2015Simulations', ...
    'FourierFits');
% This is the size we will use in the sim - fit over the same range!
lm = spotax>-125 & spotax<125;
spotim0 = sqrt(spotim(lm,lm));

for nmod = 15%:20
    [n_, m_] = meshgrid(1:nmod, 1:nmod);
    
    fmodes = zeros([size(spotim0), nmod^2]);
    a_ = length(spotax(lm)); b_ = a_;
    [x_, y_] = meshgrid(1:length(spotax(lm)), 1:length(spotax(lm)));
    
    Fcoef = zeros(nmod^2,1);
    
    for k=1:nmod^2
        fmodes(:,:,k) = sin(n_(k)*pi*x_/a_).*sin(m_(k)*pi*y_/b_);
        % This fits the intensity
        %Fcoef(k) = 4/(a_*b_)*sum(sum(spotim0.*fmodes(:,:,k)));
        % BUT! We need to input the field into the simulation:
        Fcoef(k) = 4/(a_*b_)*sum(sum(spotim0.*fmodes(:,:,k)));
    end
    % The full precision ones
    Fim = sum(fmodes.*repmat(reshape(Fcoef, [1 1 length(Fcoef)]), [size(spotim0), 1]), 3);
    % The truncated ones...
    Fcoeft = round(Fcoef, 1, 'significant');
    Fim = sum(fmodes.*repmat(reshape(Fcoef, [1 1 length(Fcoeft)]), [size(spotim0), 1]), 3);
    % Don't seem to make such a large difference!
    hfig = figure(1010); clf(hfig);
    locs = GetFigureArray(3, 1, [0.1 0.1 0.1 0.05], 0.05, 'across');
    axx(1) = axes('Parent', hfig, 'Position', locs(:,1));
    imagesc(abs(Fim)); title(sprintf('Fourier decomposition with %i modes', nmod));
    axis image
    axx(2) = axes('Parent', hfig, 'Position', locs(:,2));
    imagesc(spotim0); title('Original image');
    axis image;
    axx(3) = axes('Parent', hfig, 'Position', locs(:,3));
    imagesc(abs(Fim) - spotim0); 
    axis image;
    drawnow;
    ppos = axx(3).Position;
    cb = colorbar('Position', [0.93 ppos(2) 0.02 ppos(4)]);
    title('Difference');
    drawnow;
    %export_fig(fullfile(savfol, ['N_modes=' num2str(nmod, '%02i')]), '-nocrop', '-png', hfig);
end

% To calculate the total integral, we need to know the spatial extent of
% the simulation box. This is -125->125;
dx = mean(diff(spotax));
imSumdxdy = sum(Fim(:).^2)*dx*dx

save(fullfile(getDropboxPath, 'MATLAB', 'Simulations', 'Gem2015Simulations', 'fourierCoef'), 'Fcoef', 'imSumdxdy');