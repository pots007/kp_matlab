% makeDanDataFile.m
%
% Repsonding to Danimal's email from Mon 10/07/2017 10:09

fname =  'C:\Users\kpoder\simulations\Gem3D_sims\Gem3D_5\particles0110.sdf';
dataf = GetDataSDF(fname);

% Make the data: Columns represent x, y, z, px/pz, py/pz, gamma
npart = length(dataf.Particles.Gamma.subset_highgamma.electron.data);
logm = 1:20:npart;
x = dataf.Grid.Particles.subset_highgamma.electron.x;
y = dataf.Grid.Particles.subset_highgamma.electron.y;
z = dataf.Grid.Particles.subset_highgamma.electron.z;
px = dataf.Particles.Px.subset_highgamma.electron.data;
py = dataf.Particles.Py.subset_highgamma.electron.data;
pz = dataf.Particles.Pz.subset_highgamma.electron.data;
gamma = dataf.Particles.Gamma.subset_highgamma.electron.data;

partdata = [x y z px./pz py./pz gamma];
partdata = partdata(logm,:);

columnNames = {'x' 'y', 'z', 'x''=px/pz' 'y''=py/pz' 'gamma'};
save('C:\Users\kpoder\simulations\Gem3D_sims\Gem3D_5\particleData4Dan_particles0110', ...
    'partdata', 'columnNames');