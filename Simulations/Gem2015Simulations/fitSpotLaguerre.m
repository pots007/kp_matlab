% We take a compound focus from some run, and fit Laguerre polynomials to
% each quadrant of the focal profile. This isn't strictly speaking useful,
% but proves that one doesn't need many polynomials for a real spot.

%% First, make the compound image:

saving = 0;

%focalSpotTotalSum;
if saving
    export_fig(fullfile(getDropboxPath, 'MATLAB', 'simulations', 'Gem2015Simulations', ...
        'CompoundSpot'), '-pdf', '-nocrop', 8);
end

spotwidth = ax_calib*size(spotim,1);
spotax = linspace(-0.5*spotwidth, 0.5*spotwidth, size(spotim,1));
lineind = round(0.5*size(spotim,1));

spotlines = zeros(4,lineind);
spotlines(1,:) = fliplr(spotim(1:lineind,lineind)'); % Top
spotlines(2,:) = spotim(lineind:end,lineind); % Bottom
spotlines(3,:) = fliplr(spotim(lineind,1:lineind)); % Left
spotlines(4,:) = spotim(lineind,lineind:end); % Right
% Find the w0s, crude and easy way
w0s = zeros(4,1);
for i=1:4
    temp = find(spotlines(i,:)<exp(-1), 1, 'first');
    w0s(i) = spotax(lineind+temp);
end

hfig = figure(8); clf(hfig); set(hfig, 'Color', 'w');
set(gca, 'NextPlot', 'add', 'Box', 'on');
hhdat = plot(spotax(lineind:5:end),spotlines(:,1:5:end), 'o');
legend(hhdat, {'Top', 'Bottom', 'Left', 'Right'}, 'best');

% The fitting part
% Radial axis ought ot stay the same.
%r = -150:0.5:150;
r = spotax(lineind:end);
%w0 = 29;
order = 5;
coeffs = zeros(order+1,4);
for i=1:4
    X = r.^2/w0s(i)^2;
    XX = 2*X;
    
    polys = getLaguerreMatrix(X, XX, order);
    
    coeffs(:,i) = polys'\spotlines(i,:)';
    
    % And let's see how we did
    plot(r, polys'*coeffs(:,i), '-', 'Color', hhdat(i).Color);
    plot(r, polys'*coeffs(:,i)./max(polys'*coeffs(:,i)), '--', 'Color', hhdat(i).Color);
end
xlabel('r / $\mu m$');
ylabel('Amplitude / a.u.');
setAllFonts(8,16); make_latex(hfig);
axis tight;
title(['Using ' num2str(order+1) ' Laguerre polynomials']);
if saving
    export_fig(fullfile(getDropboxPath, 'MATLAB', 'simulations', 'Gem2015Simulations', ...
        ['LaguerreFit' num2str(order+1) 'orders']), '-pdf', '-nocrop', hfig);
end


%% And now try to create a spot like this for simulations.

rax = 0:0.5:120;
totax = -120:0.5:120;

spotmat = ones(length(totax), length(totax), 4);
spotprof = ones(length(totax));
for k=1:4
    X = rax.^2/w0s(k)^2;
    XX = 2*X;
    polys = getLaguerreMatrix(X, XX, order);
    radprof = polys'*coeffs(:,k);
    radprof = radprof/max(radprof);
    spotprof(totax>=0,:) = repmat(radprof, [1, length(totax)]);
    spotmat(:,:,k) = imrotate(spotprof, 90*k);
end

hfig = figure(8); clf(hfig);
imagesc(prod(spotmat, 3))
axis image
% Conclusion - this looks poo! Need to use proper, azimuthally nonsymmetric
% Laguerre polynomials...
