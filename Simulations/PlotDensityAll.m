files = dir('data2/normal*.sdf');
if (~(exist('ne', 'dir')==7))
    mkdir('ne');
end
figure(11); clf;
set(11, 'Position', [100 100 900 600], 'Color', 'w');
fsiz = 20;
load('owncolourmaps');
for i=1:length(files)
   if (exist(['ne/ne_' files(i).name(1:end-4) '.png'], 'file'))
       %continue;
   end
   dataf = GetDataSDFchoose(['data2/' files(i).name], 'grid');
   tim = dataf.time*1e12;
   disp(['File ' files(i).name]);
   hne = PlotDensity(['data2/' files(i).name], 'electron');
   set(gca, 'Position', [0.08 0.1 0.83 0.85]);
   %title([files(i).name ', after ' sprintf('%1.1f ps', tim)], ...
    %   'Interpreter', 'none', 'FontSize', fsiz);
   cb = colorbar;
   title('')
   xlabel('Longitudinal distance / mm', 'FontSize', fsiz);
   ylabel('Transverse distance / \mu m', 'FontSize', fsiz);
   set(gca, 'CLim', [0 5e-2], 'FontSize', fsiz, 'YLim', [-25 25]);
   set(cb, 'FontSize', fsiz);
   set(get(cb, 'YLabel'), 'String', 'Electron density /n_c', 'FontSize', fsiz);
   drawnow;
   colormap(jet_white(256));
   export_fig(['ne/ne_' files(i).name(1:end-4)], '-nocrop', '-png', 11);
end
