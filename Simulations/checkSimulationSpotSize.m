% Check that I haven't fucked up the simulation spot sizes and intensities
% etc
lambda0 = 800e-9;
% From input deck

xfoc_0 = 1500e-6;

wai = 45e-6; % * micron #field 1/e value, intensity 1/e^2 value
rayleigh_range_0 = pi*wai^2/lambda0;
wz = wai * sqrt(1 + ((0-xfoc_0)/rayleigh_range_0)^2);
radius_of_curv = (0-xfoc_0) * (1.0+(rayleigh_range_0/(0-xfoc_0))^2);
timefwhm = 45e-15;
timeomega = timefwhm /( sqrt(2*log(2.0)));

I0 = 9.53e22; % #in W/m2
Ientry = I0*wai/wz;
Eamp = sqrt(2*Ientry/(epsilon0*c));

% And now from the simulation itself
datfol = '/Volumes/SimKris/Gem2015sim/Gem2015j/vac_prop/data/';
flist = dir(fullfile(datfol, 'normal*.sdf'));

wai_sim = nan(4,length(flist));
for i=1:length(flist)
    fprintf('Working on file %i out of %i\n', i, length(flist));
    datf = GetDataSDF(fullfile(datfol, flist(i).name));
    xg = datf.Grid.Grid.x(2:end);
    yg = datf.Grid.Grid.y(2:end);
    Ez = datf.Electric_Field.Ez.data;
    hilE = abs(hilbert(Ez));
    I = 0.5*epsilon0*c*(hilE').^2;
    imagesc(xg, yg, I');
    wai_sim(1,i) = widthfwhm(sum(I,1)).*mean(diff(xg));
    wai_sim(2,i) = width1e(sum(hilE,1)).*mean(diff(yg))*0.5;
    wai_sim(3,i) = width1ee(sum(I,2)').*mean(diff(yg))*0.5;
    [~,ind] = max(sum(I,1));
    wai_sim(4,i) = xg(ind);
end