% For speeding up doing plane cut things, let's save these into arrays!
% Then can load the large files back into Box.

function save3DsimPlane(datfol, diag, plane, fstem)
% Get list of files we have
if nargin==3; fstem = 'normal'; end
flist = dir(fullfile(datfol, [fstem '*.sdf']));
% We assume the grid based diagnostic remain the same throughout the
% simulation!

tt = GetBlocksSDF(fullfile(datfol, flist(1).name))';
diag_file = {tt.id}';
if isempty(diag); diag = diag_file; end

% And now iterate over the diagnostics, get a plane, save it, bam.
% But ensure grid exists in each file as well.
for d=1:length(diag)
    diag_ = diag{d};
    if ~strcmpi(diag_, diag_file);
        fprintf('Diagnostic %s not present in file %s!\n', diag_, flist(1).name);
        continue;
    end;
    
    % Load file 1 and allocate dataarrays!
    gridf = GetDataSDFchoose(fullfile(datfol, flist(1).name), 'grid');
    grid_x = zeros(length(gridf.Grid.Grid.x), length(flist));
    grid_time = zeros(1,length(flist));
    if strcmp(plane, 'y')
        grid_t = zeros(length(gridf.Grid.Grid.z), length(flist));
        indc = round(0.5*length(gridf.Grid.Grid.y));
        slicedata = zeros(length(gridf.Grid.Grid.x)-1, length(gridf.Grid.Grid.z)-1, length(flist));
    else % Default to cutting perpendicular to z plane
        grid_t = zeros(length(gridf.Grid.Grid.y), length(flist));
        indc = round(0.5*length(gridf.Grid.Grid.z));
        slicedata = zeros(length(gridf.Grid.Grid.x)-1, length(gridf.Grid.Grid.y)-1, length(flist));
    end
    
    indd = strfind(diag{d},'_');    
    if length(indd)>1; diag_(indd(end)) = '/'; end;
    for f=1:length(flist)
        fprintf('Opening file %s for diagnostic %s\n', flist(f).name, diag_);
        gridf = GetDataSDFchoose(fullfile(datfol, flist(f).name), 'grid');
        try
            dataf = GetDataSDFchoose(fullfile(datfol, flist(f).name), diag_);
        catch err
            fprintf('Failed to get data for %s, diagnostic %s: %s\n', flist(f).name, diag_, err.message);
        end
        grid_x(:,f) = gridf.Grid.Grid.x;
        grid_time(f) = gridf.time;
        % Now we put the diagnostics into their slices:
        switch diag_
            case 'Ex'
                slice_ = dataf.Electric_Field.Ex.data;
            case 'Ey'
                slice_ = dataf.Electric_Field.Ey.data;
            case 'Ez'
                slice_ = dataf.Electric_Field.Ez.data;
            case 'Bx'
                slice_ = dataf.Magnetic_Field.Bx.data;
            case 'By'
                slice_ = dataf.Magnetic_Field.By.data;
            case 'Bz'
                slice_ = dataf.Magnetic_Field.Bz.data;
            otherwise
                % For number densities
                if sum(strncmpi('Number_Density', diag_, 14))
                    indd = strfind(diag_, '/');
                    species = diag_(indd(end)+1:end);
                    slice_ = dataf.Derived.Number_Density.(species).data;
                else
                    break;
                end
        end
        if strcmp(plane, 'y')
            grid_t(:,f) = gridf.Grid.Grid.z;
            slicedata(:,:,f) = squeeze(slice_(:,indc,:));
        else
            grid_t(:,f) = gridf.Grid.Grid.y;
            slicedata(:,:,f) = slice_(:,:,indc);
        end
    end
    save(fullfile(datfol, [diag{d} '_all']), 'slicedata', 'grid_x', 'grid_t', 'grid_time');
end
