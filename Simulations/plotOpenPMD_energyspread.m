% plotOpenPMD_energyspread.m
%
% For a lot of dumps, plots the evolution of the simulation energy spread.
%
%

dfol = '/media/kpoder/KRISMOOSE6/fbpic/Danimal/runa/hdf5';
flist = dir(fullfile(dfol, '*.h5'));
enrange = linspace(0, 3000, 1000);


enImage = zeros(length(flist), length(enrange)-1);
times = zeros(length(flist),1);
for k=1:length(flist)
    fprintf('Opening file %i out of %i\n', k, length(flist));
    dataf = GetDataOpenPMD(fullfile(dfol, flist(k).name),...
        '/particles/shell67/momentum/z');
    if isempty(dataf); continue; end;
    kk = histcounts(dataf/(qe/c*1e6), enrange);
    enImage(k,:) = kk;
    times(k) = GetTimeOpenPMD(fullfile(dfol, flist(k).name));
end

%%
hfig = figure(54145); clf(hfig);
imagesc(times*c*1e3, enrange, enImage');
colormap(jet_white(256));
colorbar;
set(gca, 'YDir', 'normal', 'LineWidth', 2, 'Layer', 'top');
set(gca, 'CLim', [0 1e5]);
xlabel('Propagation / mm');
ylabel('Energy / MeV');
make_latex(hfig);
setAllFonts(hfig, 14);
grid(gca, 'on'); drawnow;
makeNiceGridHG2(gca); drawnow;
export_fig(fullfile(fileparts(dfol), 'EnergyEvolution'), '-pdf', '-nocrop', hfig);