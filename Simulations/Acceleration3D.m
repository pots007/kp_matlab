Constants;
load divmap.mat; load invfire.mat;
fold = 'data';
plotgraphs = true;

filen = dir(fullfile(fold, 'normal*.sdf'));

if (~(exist('ne', 'dir')==7))
    mkdir('ne');
end
if (exist('ne/nedata.mat','file')==2)
    load ne/nedata.mat;
else
    transverse = cell(1000,10);
    ionexist=false;
end

for i=1:length(filen)
    if (sum(strcmp(transverse{i,1}, filen(i).name))==1)
        continue
    end
    transverse{i,1} = filen(i).name;
    % Electrons first
    dataf = GetDataSDFchoose(fullfile(fold, filen(i).name), ...
        'number_density/electron');
    n_e = dataf.Derived.Number_Density.electron.data;
    try
        dataf = GetDataSDFchoose(fullfile(fold, filen(i).name), ...
            'grid/x_px/electron');
        px = dataf.Grid.x_px.electron.y;
        x_px.e.px = px./5.344e-22;
        if (max(x_px.e.px)>1e6)
            x_px.e.px = linspace(-1, 1, length(px))';
        end
        dataf = GetDataSDFchoose(fullfile(fold, filen(i).name), ...
            'x_px/electron');
        x_px.e.data = dataf.dist_fn.x_px.electron.data';
    catch err
        fprintf('\t Error getting x_px for file %s: %s\n', filen(i).name, err.message);
    end
    %The grid
    try
        grid = GetDataSDFchoose(fullfile(fold, filen(i).name), 'grid');
        x = grid.Grid.Grid.x;
        transverse{i,2} = x(1:end-1);
    catch
        %lala
    end
    %transverse{i,3} = sum(n_e,1);
    transverse{i,3} = n_e(:,ceil(0.5*size(n_e,2)),ceil(0.5*size(n_e,3)));
    if (strfind(filen(i).name, '0015') ~= 0)
        %n0 = mean(mean(n_e(10:ceil(0.05*size(n_e,1)),:)));
        n0 = mean(mean(n_e(floor([0.4 0.6]*size(n_e,1)),ceil(0.95*size(n_e,2)):end)));
    end
    transverse{i,4} = [sum(x_px.e.data,2) x_px.e.px];
      
    fprintf('File %s\n', filen(i).name);
end
save('ne/nedata.mat', 'transverse', 'n0');

i = sum(~cellfun(@isempty, transverse(:,1)));

if (plotgraphs)
    fsize = 14;
    figure(55)
    set(55, 'WindowStyle', 'docked');
    clf;
    %Fix for error in dist fn resolution
    [~,dirn,~] = fileparts(pwd);
        
    propa = zeros(1,i);
    n_es = zeros(length(transverse{1,2}),i);
    for m=1:i
        propa(m) = max(transverse{m,2})*1e3;
        pdnum = size(n_es,1) - length(transverse{m,3});
        %try
            n_es(:,m) = padarray(transverse{m,3}, pdnum, 0, 'pre');
        %catch
         %   n_es(:,m) = padarray(transverse{m,3}, pdnum);
        %end
    end
    if (ionexist)
        subplot(2,2,1)
    else
        subplot(1,2,1)
    end
    imagesc(propa, transverse{1,2}-max(transverse{1,2}), n_es./n0);
    set(gca, 'YDir', 'normal', 'FontSize', fsize, 'CLim', [0 10]);
    ylabel('\xi /mm', 'FontSize', fsize);
    xlabel('Propagation /mm', 'FontSize', fsize);
    title(['n_e of e^- vs propagation, n_0=' sprintf('%2.1f 10^{18}cm^{-3}', n0*1e-24)])
    cb = colorbar; set(get(cb, 'YLabel'), 'String', 'n_e/n_0');
    freezeColors;
    if (ionexist)
        subplot(2,2,2)
    else
        subplot(1,2,2)
    end
    %find index of largest px
    maxs = cellfun(@(x) max(x(:,2)), transverse(:,4), 'UniformOutput', false, 'Errorhandler', @(y,x) NaN);
    k=5;
    mx = maxs{5};
    mind = 5;
    while (~isnan(maxs{k}))
        if (maxs{k} > mx)
            mx = maxs{k};
            mind = k;
        end
        k=k+1;
    end
    px = transverse{mind,4}(:,2);
    px = linspace(10, 1800, 2000);
    px_e = zeros(length(px),i);
    for m=1:i
        px_e(:,m) = interp1(transverse{m,4}(:,2), transverse{m,4}(:,1), px);
    end
    imagesc(propa, px, (px_e)); 
    if (max(px)>10)
        set(gca, 'YLim', [10 max(px)], 'CLim', [0 0.05*max(max(px_e(px>10,:)))]); 
    else
        set(gca, 'CLim', [0 max(px_e(:))*1e-5]);
    end
    set(gca, 'YDir', 'normal', 'FontSize', fsize, 'CLim', [1e4 8e7]);
    ylabel('p_x /MeV', 'FontSize', fsize);
    xlabel('Propagation /mm', 'FontSize', fsize);
    title('p_x of e^- vs propagation')
 
    setAllFonts(55,20);
    export_fig('ne/transverse.pdf', '-pdf', '-nocrop', '-transparent',55);
end