% Try to characterise the evolution of the back of the bubble in 3D
% simulations

datfol = '/Volumes/SimKris/ATA2II/SI_3D1/data2';
savfol = '/Volumes/SimKris/ATA2II/SI_3D1/Ex';
fnames = {'normal0051.sdf'};

hfig = figure(120); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [200 200 900 600]);
colormap(hfig, brewermap(259, 'RdBu'))
Exdata = struct;
for i=1:length(fnames)
    tic;
    clf(hfig);
    fprintf('Looking into file %s\n', fnames{i});
    set(gca, 'NextPlot', 'add', 'Box', 'on', 'LineWidth', 3);
    grid = GetDataSDFchoose(fullfile(datfol, fnames{i}), 'grid');
    ksi = grid.Grid.Grid.x(1:end-1);
    y = grid.Grid.Grid.y;
    Ex = GetDataSDFchoose(fullfile(datfol, fnames{i}), 'Ex');
    [nx, ny, nz] = size(Ex.Electric_Field.Ex.data);
    logm_t = abs((1:ny) - round(0.5*ny)) < 5;
    Ex_sub = Ex.Electric_Field.Ex.data(:, logm_t, logm_t);
    Ex_plane = sum(Ex_sub, 3); Ex_line = sum(Ex_plane, 2);
    imagesc(ksi, y, Ex.Electric_Field.Ex.data(:,:, round(0.5*nz))');
    scalefac = min(y)/max(abs(Ex_line));
    plot(ksi, -Ex_line*scalefac, '-k', 'LineWidth', 2);
    set(gca, 'CLim', max(abs(Ex.Electric_Field.Ex.data(:)))*[-1 1], 'YDir', 'normal');
    axis tight;
    xlabel('$\xi$'); ylabel('y');
    cb = colorbar; ylabel(cb, '$E_x / \mathrm{Vm^{-1}}$')
    make_latex(hfig); setAllFonts(hfig, 14);
    Exdata(i).t = grid.time;
    Exdata(i).ksi = ksi;
    Exdata(i).Exline = Ex_line;
    toc
end