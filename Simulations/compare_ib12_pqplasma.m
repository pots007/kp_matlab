if ispc
    rootf = 'E:\Kristjan\Google Drive\epoch\ib12vspqplasma';
end

fnames = dir(fullfile(rootf, 'log*.txt'));
dlbdat = zeros(3,length(fnames));
%%
% Assuming the files are correctly ordered!
for i=1:length(fnames)
    fid = fopen(fullfile(rootf, fnames(i).name));
    totl=0;
    lbline = 0;
    while (~feof(fid))
        fline = fgetl(fid);
        totl=totl+1;
        if sum(strfind(fline, 'Load balancing')) || sum(strfind(fline, 'Initial load imbalance'))
            lbline = lbline + 1;
        end
    end
    fclose(fid);
    dlbdat(1,i) = totl;
    dlbdat(2,i) = lbline;
    i1 = regexp(fline, '\d* hours');
    i2 = regexp(fline, ' hours');
    hs = str2double(fline(i1:i2-1));
    i1 = regexp(fline, '\d* minutes');
    i2 = regexp(fline, ' minutes');
    mins = str2double(fline(i1:i2-1));
    i1 = regexp(fline, '\d*.\d* seconds');
    i2 = regexp(fline, ' seconds');
    secs = str2double(fline(i1:i2-1));
    dlbdat(3,i) = 3600*hs + 60*mins + secs;
end

%dlbdat(3,:) = dlbdat(3,:)./nodes;
%%
hfig = figure(101); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [100 100 800 500]);
ax1 = axes('Parent', hfig, 'NextPlot', 'add', 'Position', [0.1 0.03 0.39 0.77]);
ax2 = axes('Parent', hfig, 'NextPlot', 'add', 'Position', [0.51 0.03 0.39 0.77],...
    'YAxisLocation', 'right');
markers = {'or', 'ok', 'dr', 'dk'};
hs = zeros(2,4);
for i=1:length(markers)
    hs(1,i) = plot(ax1, i+[-0.1 0 0.1], dlbdat(2, (1:3)+(i-1)*3)/1000, markers{i});
    hs(2,i) = plot(ax2, i+[-0.1 0 0.1], dlbdat(3, (1:3)+(i-1)*3)/3600, markers{i});
end
set(hs, 'MarkerSize', 11, 'LineWidth', 2);
ylabel(ax2, 'Runtime / hours'); ylabel(ax1, 'DLB iterations / 1000');
set([ax1 ax2], 'XTick', [], 'Box', 'on');
legend(ax1, hs(1,1:2), {'ib12, 72, v4.2.13', 'ib12, 72, v4.7.5'}, 'Position', [0.1 0.82, 0.35 0.2],...
    'EdgeColor', 'w')
legend(ax2, hs(2,3:4), {'pqplasma, 200, v4.2.13', 'pqplasma, 200, v4.7.5'}, 'Position', [0.55 0.82, 0.35 0.2],...
    'EdgeColor', 'w')
setAllFonts(hfig, 15);

export_fig(fullfile(rootf, 'results'), '-nocrop', '-pdf', hfig);

%% Now per node, average them as well

nodes = [6*ones(1,6) 10*ones(1,6)];
cores = [72*ones(1,6) 200*ones(1,6)];
pernode = reshape(dlbdat(3,:).*nodes, 3, 4);
percore = reshape(dlbdat(3,:).*cores, 3, 4);
pernodemean = mean(pernode,1)/3600;
percoremean = mean(percore,1)/3600;
% Looking at ratio for both codes:
pernoderatio = pernodemean([1 2])./pernodemean([3 4])
percoreratio = percoremean([1 2])./percoremean([3 4])