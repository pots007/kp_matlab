%Assume the n_e and field matrix have been transposed already

function hne = plotnewithlaser(x, y, ne, Ey)
    hne = imagesc(x, y, ne);
    hold on;
    %colorbar;
    Eyhil = abs(hilbert(Ey'))';
    m = max(max(Eyhil));
    levels = [exp(-2) exp(-1) 0.5 0.95]*m;
    contour(x,y,(Eyhil), 'w', 'LevelList', levels);
    hold off;
end