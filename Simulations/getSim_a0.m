% getSim_a0.m
%
% Calculate a0, accounting for wavelength shifting, from a simulation dump
% Neglects plasma dispersion relation, which should be OK up 1e19 or so
% Inputs
%       E  - electric field, real
%       x - if scalar, dx, if vector, E grid
%       sm - Number of points to smooth the phase over
%       ax - if supplied, will plot a few things

function [a0, hh] = getSim_a0(E, x, sm, ax)
Constants;

hilE = hilbert(E); % Get the complex version of field
phiE = phase(hilE); % This is the phase

% If not given, or empty, default to average over 100 points...
if nargin<3 || isempty(sm)
    npnts = 100;
else
    npnts = sm;
end
if numel(x)==1
    dx = x;
else
    dx = mean(diff(x));
end
    
k_las = smooth(gradient(phiE, dx), npnts); 
lambda_las = 2*pi./k_las;
omega_las = c.*k_las;
% Assume linear polarisation as well, and average over cycles

%a0 = qe*(abs(hilE)/sqrt(2))./(me*c*omega_las); % The sqrt(2) gives incorrect results... 
a0 = qe*(abs(hilE)/sqrt(1))./(me*c*omega_las);
%Ilas = 0.5*epsilon0*c*abs(hilE).^2;
%a0 = 0.856*lambda_las*1e6.*sqrt(Ilas*1e-22);

if nargin~=4
    hh = [];
    return;
end
% But if ax is given, plot stuff 
[AX,hh] = plotyy(ax, x, a0, x, lambda_las);
set(AX(1), 'YLim', [0 10], 'YTick', [0 5 10]);
set(AX(2), 'YLim', [400e-9 2000e-9], 'YTick', 1e-7*[4:2:20]);
ylabel(AX(1), 'a_0')
ylabel(AX(2), 'Wavelength / m');