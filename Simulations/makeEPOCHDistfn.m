fnum = 17;
if ismac
    fname = sprintf('/Volumes/SimKris/ATA2II/II_3D1/data/particles%04i.sdf', fnum);
    fname = sprintf('/Volumes/SimKris/ATA2II/II_3D1/data/normal%04i.sdf', fnum);
end
fname = 'E:\Kristjan\Documents\Uni\Dropbox\particles0017.sdf';
dataf = GetDataSDF(fname);
species = fieldnames(dataf.Grid.Particles.subset_highgamma);
maxens = zeros(size(species));
for i=1:length(species)
    maxens(i) = max(dataf.Particles.Gamma.subset_highgamma.(species{i}).data)*0.511;
end
linengrid = linspace(10, max(maxens),500);

for i=1:length(species)
    x = dataf.Grid.Particles.subset_highgamma.(species{i}).x; %x = x(x~=0); 
    y = dataf.Grid.Particles.subset_highgamma.(species{i}).y; %y = y(y~=0);
    z = dataf.Grid.Particles.subset_highgamma.(species{i}).z; %z = z(z~=0);
    px = dataf.Particles.Px.subset_highgamma.(species{i}).data; px = px(px~=0);
    py = dataf.Particles.Py.subset_highgamma.(species{i}).data; py = py(py~=0);
    pz = dataf.Particles.Pz.subset_highgamma.(species{i}).data; pz = pz(pz~=0);
    gamma = dataf.Particles.Gamma.subset_highgamma.(species{i}).data*0.511; gamma = gamma(gamma~=0);
    
end