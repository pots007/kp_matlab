%Plot ne of one datafile

function him = PlotOnen_e(datafile)

dataf = GetDataSDFchoose(datafile, 'grid');
x = dataf.Grid.Grid.x;
x = x(1:length(x)-1)*1e3;
y = dataf.Grid.Grid.y;
y = y(1:length(y)-1)*1e3;
time = dataf.time*1e15;
dataf = GetDataSDFchoose(datafile, 'number_density/electron');
ne = dataf.Derived.Number_Density.electron.data';
ne = 1e-6.*ne./ncrit(800);
imagesc(x,y,ne);
