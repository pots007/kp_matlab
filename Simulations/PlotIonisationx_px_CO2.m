load divmap.mat;
simtype = 'data';
files = dir([simtype '/*.sdf']);
figure(1)
set(1, 'WindowStyle', 'docked');
if (~(exist('x_px', 'dir')==7))
    mkdir('x_px');
end
plotall = false;
for i=1:length(files)
    if plotall
        %Plot all
    else
        if (exist(['x_px/' files(i).name(1:4) '.png'], 'file'))
            continue;
        end
    end
    disp(['File ' files(i).name]);
    datafl = GetBlocksSDF([simtype '/' files(i).name]);
    datafids = cell(1, length(datafl));
    for k=1:length(datafl)
        datafids{k} = datafl(1,k).id;
    end
    dataf = GetDataSDFchoose([simtype '/' files(i).name], 'grid');
    x = dataf.Grid.Grid.x;
    x = x(1:length(x)-1);
    y = dataf.Grid.Grid.y*1e6;
    y = y(1:length(y)-1);
    time = dataf.time*1e15;
    dataf = GetDataSDFchoose([simtype '/' files(i).name], 'ez');
    Ey = dataf.Electric_Field.Ez.data';
    dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
        'number_density/shell5');
    n_s5 = dataf.Derived.Number_Density.shell5.data';
%     dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
%         'number_density/shell7');
%     n_s7 = dataf.Derived.Number_Density.shell7.data';
    dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
        'number_density/electron');
    n_e = dataf.Derived.Number_Density.electron.data';
    
    subplot(2,2,1)
    %plotnewithlaser(x,y,n_s6, Ey);
    imagesc(x,y,n_s5);
    title(['shell5 density after ' num2str(time) ' fs']);
    xlabel('x (m)');
    colormap(divmap);
    %set(gca, 'CLim', [-3e10 3e10]);
    %colorbar;
    %subplot(2,3,2)
    %plotnewithlaser(x,y,n_s7, Ey);
    %imagesc(x,y,n_s7);
    %title(['shell7 density after ' num2str(time) ' fs']);
    %xlabel('x (m)');
    %set(gca, 'CLim', [-2e13 2e13]);
    %colorbar;
    subplot(2,2,2)
    %plotnewithlaser(x,y,n_e, Ey);
    imagesc(x,y,n_e);
    title(['electron density after ' num2str(time) ' fs']);
    xlabel('x (m)');
    %Now x_px ones.
    subplot(2,2,3)
    cla;
    if(sum(strcmp(datafids, 'x_px/shell5'))~=0)
        dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
            'grid/x_px/shell5');
        px = dataf.Grid.x_px.shell5.y;
        px = px./5.344e-22;
        dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
            'x_px/shell5');
        x_px_s5 = dataf.dist_fn.x_px.shell5.data';
        imagesc(x, px, x_px_s5);
        %set(gca, 'CLim', [0 9e23]);
        set(gca, 'YDir', 'normal');
        title('5th shell N electrons x-px');
        ylabel('px (MeV/c)');
        xlabel('x (m)');
    end
%     subplot(2,3,5)
%     cla;
%     if(sum(strcmp(datafids, 'x_px/shell7'))~=0)
%         dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
%             'grid/x_px/shell7');
%         px = dataf.Grid.x_px.shell7.y;
%         px = px./5.344e-22;
%         dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
%             'x_px/shell7');
%         x_px_s7 = dataf.dist_fn.x_px.shell7.data';
%         imagesc(x, px, x_px_s7);
%         %set(gca, 'CLim', [0 9e23]);
%         set(gca, 'YDir', 'normal');
%         title('7th shell N electrons x-px');
%         ylabel('px (MeV/c)');
%         xlabel('x (m)');
%     end
    subplot(2,2,4)
    cla;
    if(sum(strcmp(datafids, 'x_px/electron'))~=0)
        dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
            'grid/x_px/electron');
        px = dataf.Grid.x_px.electron.y;
        px = px./5.344e-22;
        dataf = GetDataSDFchoose([simtype '/' files(i).name], ...
            'x_px/electron');
        x_px_e = dataf.dist_fn.x_px.electron.data';
        imagesc(x, px, x_px_e);
        %set(gca, 'CLim', [0 9e23]);
        set(gca, 'YDir', 'normal');
        title('All other electrons x-px');
        ylabel('px (MeV/c)');
        xlabel('x (m)');
    end
    drawnow;
    export_fig(['x_px/' files(i).name(1:4) '.png'], '-png',1);
end
