function estimateEPOCHtimeleft(filename, fulltime)
%fulltime = 10.5e-12;
%filename = 'log1.txt';
ftext = getFileText(filename);
nentries = sum(cell2mat(strfind(ftext, 'Time')));
tdata = zeros(nentries, 3);
it = 1;
for i=1:length(ftext)
    if (length(ftext{i})<4) continue; end;
    if (strcmp(ftext{i}(1:4), 'Time')~=1) continue; end;
    ind1 = strfind(ftext{i}, 'and');
    tdata(it,1) = str2double(ftext{i}(5:ind1-1));
    ind2 = regexp(ftext{i}, '\d*:\d*:\d*:\d*');
    %[days,hrs,mins,secs]
    tt = sscanf(ftext{i}(ind2:end), '%d:%d:%d:%f');
    tdata(it,2) = (tt(4) + 60*tt(3) + 3600*tt(2) + 3600*24*tt(1))/3600;
    it = it+1;
end
timeslope = diff(tdata(:,2))./diff(tdata(:,1));
tdata(:,3) = fulltime*[nan; timeslope];
%plot(tdata(:,2), tdata(:,3), 'o')
figure(9080); clf;
hh = myplotyy(gca, tdata(:,2), tdata(:,1)*1e12, tdata(:,3), '-xk', 'or');
set([hh.ax1 hh.ax2], 'Nextplot', 'add');
plot(hh.ax2, get(hh.ax2, 'XLim'), ones(2,1)*mean(tdata(2:end,3)), '--r');
xlabel(hh.ax1, 'Elapsed time / h');
ylabel(hh.ax1, 'Simulation progress / ps')
ylabel(hh.ax2, 'Total simulation time / h');
title(sprintf('Sim progress and finish time for t_{end} = %1.2f ps', fulltime*1e12));
setAllFonts(9080, 16);