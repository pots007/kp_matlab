function SDF2mat3(source_filepath,destination_filepath,sdfs,overwrite)
%opens SDF files and saves each variable as a .mat file
%v3 changed to extract all variables
%inputs:
%sourcefilepath: filepath containing run/ which contains your sdf files
%destinationfilepath: filepath where you want run_mat to be
%sdfs: which sdf files to process, must be a continuous range
%overwrite: optional, if set to one will continue even if it means
%overwriting files

if nargin<4
    overwrite=0;
end

%this section makes sure that both filepaths end in a /, otherwise things
%go wrong
if source_filepath(end)~='/'
    source_filepath=[source_filepath '/'];
end
if destination_filepath(end)~='/'
    destination_filepath=[destination_filepath '/'];
end


%make a list of all the variables in the sdf file- look at two files and
%choose the one with the fewest variables in it so we don't get a restart
%dump (would cause a crash!
blocklistA=GetBlocksSDF([source_filepath 'run/' sprintf('%04d',min(sdfs)) '.sdf']);
blocklistB=GetBlocksSDF([source_filepath 'run/' sprintf('%04d',min(sdfs)+1) '.sdf']);
if length(blocklistA)>length(blocklistB)
    blocklist=blocklistB;
else
    blocklist=blocklistA;
end

for i=[1:length(blocklist)]
    blocklist2{i}=blocklist(i).id;
end

%edit the list to remove non-variables and also create a seperate list of
%anything with it's own grid, namely mometa variables
all_variables=blocklist2;
k=1;
list=[];
for j=[1:length(blocklist2)]
    if length(blocklist2{j})==8
        if blocklist2{j}=='run_info' | blocklist2{j}=='cpu_rank' 
             all_variables(j-length(blocklist2)+length(all_variables))=[];%remove run_info and cpu_rank
        end
    end
    
    if length(blocklist2{j})>=4
        if blocklist2{j}(1:4)=='grid'
            all_variables(j-length(blocklist2)+length(all_variables))=[];%remove all the grid variables
            if length(blocklist2{j})>7
                momenta_variables{k}=blocklist2{j}(6:end); %make a list of the momeneta variables
                k=k+1;
            end
        end
    end
end

%momenta_variables
        

%check to see if files are already there, don't want to overwrite!
if exist([destination_filepath 'run_mat/' all_variables{1} '/' sprintf('%04d',sdfs(1)) '_ex.mat'])==2
    if overwrite==0
        error([destination_filepath 'run_mat/' all_variables{1} '/' sprintf('%04d',sdfs(1)) '_ex.mat already exists. Set overrule to 1 to skip this error.'])
    else
       disp('Warning, data already there but overruling')
    end
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This section creates the neccesery file structure
    if ~exist([destination_filepath 'run_mat/'], 'dir')
        mkdir([destination_filepath 'run_mat/'])
    end
    
    
    for i=[1:length(all_variables)]
        filename=all_variables{i};
        filename(filename=='/')='_'; %remove the forward slashes from the filenames
        if ~exist([destination_filepath 'run_mat/', filename], 'dir')
            mkdir([destination_filepath 'run_mat/', filename])
        end
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This sections loads all the data variable by variable and saves it into
%the file structure

    %load the spatial grid
    if ~exist([destination_filepath 'run_mat/grid.mat'])
    	SDF2mat_var('grid',source_filepath,destination_filepath,min(sdfs))
    end

    if exist('momenta_variables')
        parfor timestep=[sdfs]
            disp(['Processing timestep ' num2str(timestep)])
            %loop through all variables
            for j=[1:length(all_variables)]
                SDF2mat_var(all_variables{j},source_filepath,destination_filepath,timestep)
            end
            %load the grids for the momenta

            for k=[1:length([momenta_variables])]
                SDF2mat_var(['grid/' momenta_variables{k}],source_filepath,destination_filepath,timestep)
            end
        end
    else
        parfor timestep=[sdfs]
            disp(['Processing timestep ' num2str(timestep)])
            %loop through all variables
            for j=[1:length(all_variables)]
                SDF2mat_var(all_variables{j},source_filepath,destination_filepath,timestep)
            end
            %load the grids for the momenta

%             for k=[1:length([momenta_variables])]
%                 SDF2mat_var(['grid/' momenta_variables{k}],source_filepath,destination_filepath,timestep)
%             end
        end
    end

end


function SDF2mat_var(variable_name,source_filepath,destination_filepath,timestep)
%loads a given variable from a given sdf file when supplied with a variable
%name, source and destination filepaths and a timestep. Variable names with
%a '/' replace the '/' with an '_'.
    disp(['Processing timestep ' num2str(timestep) ' variable ' variable_name])
    temp_var = GetDataSDFchoose([source_filepath 'run/' sprintf('%04d',timestep) '.sdf'],variable_name);

    variable_name2=variable_name;
    variable_name2(variable_name=='/')='_';
    eval([variable_name2 '=temp_var;'])

    if variable_name(1:2)=='gr'
        if length(variable_name)<=4
            %save the spatial grid jsut in /run_mat
            eval(['save([destination_filepath ''run_mat/'',''',variable_name2,'''],''' variable_name2 ''')'])
        else
            %save the momenta grid variables in their respective momentun
            %folders
            eval(['save([destination_filepath ''run_mat/',variable_name2(6:end),'/'',''',sprintf('%04d',timestep),'_',variable_name2,'''],''' variable_name2 ''')'])
        end
    else
        %save the other variables in their own folders
        eval(['save([destination_filepath ''run_mat/',variable_name2,'/'',''',sprintf('%04d',timestep),'_',variable_name2,'''],''' variable_name2 ''')'])
    end
end

