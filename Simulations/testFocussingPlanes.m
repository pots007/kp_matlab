% testFocussingPlanes.m
%
% Script to test whether the simulations I've run are actually correct or
% not :p

lambda0 = 800e-9;
micron = 1e-6;
% The following is copied from Gem3D_5/data/input.deck
xfoc = 10500 * micron;
waix = 37.0 * micron; %field 1/e value, intensity 1/e^2 value
waiy = 48 * micron;
rayleigh_rangex = pi*waix^2/lambda0;
wzx = waix * sqrt(1 + ((0-xfoc)/rayleigh_rangex)^2);
radius_of_curvx = (0-xfoc) * (1.0+(rayleigh_rangex/(0-xfoc))^2);
rayleigh_rangey = pi*waiy^2/lambda0;
wzy = waiy * sqrt(1 + ((0-xfoc)/rayleigh_rangey)^2);
radius_of_curvy = (0-xfoc) * (1.0+(rayleigh_rangey/(0-xfoc))^2);

axx = (-200:200)*1e-6;
dx = mean(diff(axx));
[X,Y] = meshgrid(axx,axx);

phase = -2*pi/lambda0*(X.^2/(2*radius_of_curvx) + Y.^2/(2*radius_of_curvy) );
gauss = @(r, r0, w) exp(-((r-r0)/w).^2);
profile = gauss(Y, 0, wzy).*gauss(X, 0, wzx);

imagesc(profile)
axis image

for i=1:20
    ffield = fresnelPropagator(profile.*exp(-1i*phase), dx, dx, i*1e-3, 800e-9);
    imagesc(abs(ffield)); 
    title(num2str(i));
    drawnow; axis image; pause(1)
end