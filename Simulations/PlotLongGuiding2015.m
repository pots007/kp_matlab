files = dir('data/normal*.sdf');
if (~(exist('plots', 'dir')==7))
    mkdir('plots');
end
load('owncolourmaps.mat');
figure(11); clf;
locs = GetFigureArray(2,2, 0.1, 0.1, 'down');
axs = zeros(1,4);
set(11, 'Position', [100 100 1200 800]);
for i=1:length(files)
   if (exist(['plots/' files(i).name(1:end-4) '.png'], 'file')==2)
       continue;
   end
   figure(11); clf;
   disp(files(i).name);
   axs(1) = axes('Parent', 11, 'Position', locs(:,1));
   PlotField(['data/' files(i).name], 'Ex'); colorbar;
   title('E_x')
   axs(2) = axes('Parent', 11, 'Position', locs(:,2));
   PlotField(['data/' files(i).name], 'Ey'); colorbar;
   title('E_y');
   axs(3) = axes('Parent', 11, 'Position', locs(:,3));
   EVPlotIntensity(axs(3), ['data/' files(i).name], 'lala'); colorbar;
   axs(4) = axes('Parent', 11, 'Position', locs(:,4));
   EVPlotDensityelectron(axs(4), ['data/' files(i).name], 'electron'); colorbar;
   colormap(colmap.jet_white);
   setAllFonts(11, 14);
   drawnow;
   set(11, 'Color', 'w');
   export_fig(['plots/' files(i).name(1:end-4)], '-nocrop', '-png', 11);
end
