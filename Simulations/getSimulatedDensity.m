%fol = '/Volumes/SimKris/LongGuiding/Gem2015h/data4/';
%fol = '/Users/kristjanpoder/epochdata/Jena3Dplay/data2/';
savedata = true;
fol = '/Volumes/cx2work/Gem2D_cone2/data/';
filen = dir_names([fol 'normal*.sdf']);
figure(122); clf;
set(gca, 'NextPlot', 'add');
nedata = cell(length(filen),2);
for i=1:length(filen)
    dataf = GetDataSDFchoose([fol filen{i}], 'grid');
    disp(filen{i});
    x = dataf.Grid.Grid.x; x = x(2:end)*1e3;
    y = dataf.Grid.Grid.y;
    [~,yind] = min(abs(y - 200e-6));
    dataf = GetDataSDFchoose([fol filen{i}], 'number_density/electron');
    ne = dataf.Derived.Number_Density.electron.data*1e-6;
    neline = mean(ne(:,yind-20:yind+20),2);
    %plot(x(1:100:end),smooth(neline(1:100:end)), 'x');
    plot(x, smooth(neline), '-k');
    drawnow
    nedata{i,1} = x;
    nedata{i,2} = neline;
end

ylabel('n_e / cm^{-3}');
xlabel('x / mm');
setAllFonts(122,16)
