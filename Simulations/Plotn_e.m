files = dir('data/*.sdf');
if (~(exist('n_e', 'dir')==7))
    mkdir('n_e');
end
figure(11)
cla;
set(11, 'Position', [100 100 1200 900]);
for i=85:length(files)
   if(exist(['n_e/n_e_' files(i).name '.png'], 'file'))
   	continue;
   end
   dataf = GetDataSDFchoose(['data/' files(i).name], 'grid');
   disp(['File ' files(i).name]);
   x = dataf.Grid.Grid.x;
   x = x(1:length(x)-1)*1e3;
   y = dataf.Grid.Grid.y;
   y = y(1:length(y)-1)*1e6;
   time = dataf.time*1e12;
   dataf = GetDataSDFchoose(['data/' files(i).name], 'number_density/electron');
   ne = dataf.Derived.Number_Density.electron.data';
   ne = 1e-6.*ne./ncrit(800);
   dataf = GetDataSDFchoose(['data/' files(i).name], 'ez');
   Ey = dataf.Electric_Field.Ez.data';
   %subplot(2,1,1)
   imagesc(x, y, ne);
   %hne = plotnewithlaser(x,y,ne, Ey);
   set(gca, 'Position', [0.08 0.07 0.85 0.85]);
   %title([files(i).name ', after ' num2str(time) ' fs'], ...
   %    'Interpreter', 'none');
   title(sprintf(' t = %4.1f ps', time));
   cb = colorbar;
   fsiz = 20;
   xlabel('Longitudonal distance /mm', 'FontSize', fsiz);
   ylabel('Transverse distance /\mu m', 'FontSize', fsiz);
   set(gca, 'CLim', [0 4e-2], 'FontSize', fsiz);
   set(cb, 'FontSize', fsiz);
   set(get(cb, 'YLabel'), 'String', 'Electron density /n_c', 'FontSize', fsiz);
   %subplot(2,1,2)
   set(gca, 'YLim', [-30 30]);
   drawnow;
   colormap(flipud(gray));
   set(11, 'Color', 'w');
   setAllFonts(11,24)
   export_fig(['n_e/n_e_' files(i).name(1:4) '.png'], '-nocrop', '-png', 11);
end
