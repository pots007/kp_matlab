% omegas(1) = reddest edge
% omegas(2) = mean frequency
% omegas(3) = bluest edge

function omega = getSpectrum(filename)
Constants;
simtype = '.';
%filename = '0220.sdf';
dataf = GetDataSDFchoose([simtype '/' filename], 'grid');
x = dataf.Grid.Grid.x;
x = x(1:length(x)-1);
dataf = GetDataSDFchoose([simtype '/' filename], 'ey');
Eyl = dataf.Electric_Field.Ey.data;
if (isvector(Eyl))
    Eyh = hilbert(Eyl);
else
    Eyh = hilbert(Eyl(:,ceil(0.5*size(Eyl,2))));
end
numpnts = 2^(nextpow2(length(Eyh))+1);
omega = fftX(x./c, Eyh, numpnts);
%omegaaxis = omegal(:,1);
%omega = omegal(:,2).*conj(omegal(:,2));
%omega = omega./max(omega);
%omega = omega(0.5*numpnts:end);
end