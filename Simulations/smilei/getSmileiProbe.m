% getSmileiProbe.m
%
% Retrieves all Smilei probes from file .

function dat = getSmileiProbe(filename, id, field)

dinfo = hdf5info(filename);
% Spit out info about the file, if we are asked to! Return list of dumps
% and the fields that are saved in the file.
llist = {dinfo.GroupHierarchy.Datasets.Name}';
logm = cellfun(@(x) isnan(str2double(x(2:end))), llist);
dumplist = llist(~logm);
llist = {dinfo.GroupHierarchy.Attributes.Shortname};
fID = sum(strcmp(llist, 'fields').*(1:length(llist)));
dimsID = sum(strcmp(llist, 'dimension').*(1:length(llist)));
fields = dinfo.GroupHierarchy.Attributes(fID).Value.Data;
fields = strsplit(fields, ',')
if strcmp(id, '-info')
    dat.dumplist = dumplist;
    dat.fields = fields;
    return;
end

% But if we're after actual data....
if nargin==2 || isempty(field)
    fieldID = 1;
else
    strcmp(fields, field)
    fieldID = sum(strcmp(fields, field).*(1:length(fields)));
    if fieldID==0
        error(['Requested field ' field ' not present in file ' filename]);
    end
end

% Read in the data!
dims = dinfo.GroupHierarchy.Attributes(dimsID).Value;
probedata = h5read(filename, id);
if dims==0 % Point probe:
    
    
elseif dims==1 % Line probe;
    pos_ = h5read(filename, '/positions');
    dat.x = pos_(1,:);
    dat.y = pos_(2,:);
    dat.data = probedata(:,fieldID);
elseif dims==2 % Plane probe
    pos_ = h5read(filename, '/positions');
    dat.x = unique(pos_(1,:));
    dat.y = unique(pos_(2,:));
    [X,Y] = meshgrid(dat.x, dat.y);
    ss = scatteredInterpolant(pos_(1,:)', pos_(2,:)',  probedata(:,fieldID));
    dat.data = ss(X,Y);
end