% plotSmileiIntensity.m
%
% Retrieves a field from Smilei file. Most useful to run
% getSmileiFieldList.m to get a list of fields, files names and dump times
%
% KP DESY 2017

function dat = plotSmileiIntensity(filename, dumpID, field, ax)
Constants;

dat_ = getSmileiField(filename, dumpID, field);
[ny, nx, nz] = size(dat_.data);
% gridSpacing = h5readatt(filename, [dumpID '/' field], 'gridSpacing');
% dat_.time = h5readatt(filename, dumpID, 'time');
% field_ = h5read(filename, [dumpID '/' field]);
% [ny, nx, nz] = size(field_);
% dat_.data = field_; dat_.x = []; dat_.y = []; dat_.z = [];
% if nz==1 % 1D or 1D:
%     if nx == 1 % 1D sim! Check with a 1D sim how data is saved, though
%         dat_.x = 0:gridSpacing(1):(gridSpacing(1)*(nx-1));
%     else % 2D!
%         dat_.x = 0:gridSpacing(1):(gridSpacing(1)*(nx-1));
%         dat_.y = 0:gridSpacing(2):(gridSpacing(2)*(ny-1));
%     end
% else % Actually 3D!
%     % Do three D stuff here
%     dat_.x = 0:gridSpacing(1):(gridSpacing(1)*(nx-1));
%     dat_.y = 0:gridSpacing(2):(gridSpacing(2)*(ny-1));
%     dat_.z = 0:gridSpacing(3):(gridSpacing(3)*(nz-1));
% end

plotFlag = nargin == 4 && ishandle(ax) && strcmp(get(ax, 'Type'), 'axes');
if plotFlag
    cla(ax);
end
if ny == 1
    Iline = abs(hilbert(dat_.data).^2);
    dat_.data = 1e18*abs(Iline).^2/(0.8*0.856);
    if plotFlag; dat_.h = plot(ax, dat_.x, dat_.data);end
else
    if nz == 1
        Iim = hilbert(dat_.data');
        dat_.data = 1e18*abs(Iim').^2/(0.8*0.856);
        if plotFlag; dat_.h = imagesc(dat_.x, dat_.y, dat_.data, 'Parent', ax); end
    else
        % Figure out what to do for 3D
    end
end

if plotFlag; title(ax, sprintf('t = %2.2f', dat_.time)); end

dat = dat_;