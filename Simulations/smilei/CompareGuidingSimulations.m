% CompareGuidingSimulations.m
%
% Looks at the guiding simulations I've done for the EPS2017 invited
% talk/paper

%rootf = 'D:\guidsims';
rootf = 'C:\Users\kpoder\simulations\guidsims';
fols = dir(rootf); fols = fols(3:end);
% Find plasma densities and fnumbers automatically
fnos = zeros(size(fols)); nes = zeros(size(fols)); leglabels = cell(size(fols));
for k=1:length(fols)
    fnos(k) = str2double(fols(k).name(6:7));
    inds = strfind(fols(k).name, '_');
    if length(inds)==1
        nes(k) = str2double(fols(k).name(inds+1:end));
    else
        nes(k) = str2double(fols(k).name(inds(1)+1:inds(2)-1));
    end
    leglabels{k} = sprintf('f/%i, $n_e=%1.1f \\times 10^{18}$', fnos(k),...
        nes(k)*1e-24);
end
%

hfig = [figure(765) figure(766) figure(767) figure(768)]; 
for k=1:length(hfig); clf(hfig(k)); end;
axx = [axes('Parent', hfig(1), 'NextPlot', 'add'),...
    axes('Parent', hfig(2), 'NextPlot', 'add'),...
    axes('Parent', hfig(3), 'NextPlot', 'add')];

totEnDat = cell(size(fols));
spotsizedat = cell(size(fols));

kL = 2*pi/800e-9;

for k=1:length(fols)
    fdat = load(fullfile(rootf, fols(k).name, 'Ezdata'));
    xfactor = 0.97*fdat.dx/kL;
    yfactor = fdat.dy/kL*1e6;
    % ------------ Plot the total energy evolution
    totEnDat{k} = sum(fdat.trans_data(:,1:length(fdat.prop_times)), 1);
    plot(axx(1), fdat.prop_times*xfactor, totEnDat{k}, 'DisplayName', leglabels{k});
    % ------------ plot spot FWHM size for each slice in each simulation
    for s=2:length(fdat.prop_times)%size(fdat.trans_data, 2)
        mval = max(fdat.trans_data(:,s));
        %spotsizedat{k}(s) = sum(fdat.allInt(fdat.allInt(:,s)>0.5*mval,s));
        spotsizedat{k}(s) = widthfwhm(fdat.trans_data(:,s)');
    end
    plot(axx(2), fdat.prop_times*xfactor, spotsizedat{k}*yfactor, 'DisplayName', leglabels{k});
    % ------------ plot maximum intensity evolution 
    plot(axx(3), fdat.prop_times*xfactor, max(fdat.long_data(:,1:length(fdat.prop_times)),[],1), 'DisplayName', leglabels{k});
    
    % -------- Plot spot size 2D evolution
    axx(end+1) = subplot(length(fols), 1, k, 'Parent', hfig(end));
    imagesc(fdat.trans_data, 'Parent', axx(end));
    [ny, nx] = size(fdat.trans_data);
    axx(end).YLim = [-100 100] + 0.5*ny;
    axx(end).XLim = [0 350];
    axx(end).CLim = [0 1.8e22];
    colorbar;
    title(leglabels{k})
    % -------- Add pump depletion lengths
    for i=1:3
        hhh = axx(i).Children;
        plot(axx(i), c*45e-15*ncrit(800)*1e6/nes(k)*[1 1], [0 1e19], '--', 'Color', hhh(1).Color,...
            'DisplayName', '$L_\mathrm{dpl}=c\tau\omega_0^2/\omega_p^2$');
    end
end

axx(2).YLim = [0 55]; axx(2).XLim = [0 0.06];

legend(axx(1), 'show');
legend(axx(2), 'show');
legend(axx(3), 'show');
for k=1:length(hfig); make_latex(hfig); end;