% checkSmileiFocussing.m
%
% Checks the vacuum propagation simulation against what I asked it to do!
%
% Update 20170321 - seems like it does what I ask it to do!

datfol = 'D:\EPOCH_smilei\testrun_smilei_vac\data';
% Fields2 is Ez
dumplist = getSmileiDumpList(fullfile(datfol, 'Fields0.h5'));

spotsizes = [];
for i=1:length(dumplist)
    Ez = h5read(fullfile(datfol, 'Fields0.h5'), [dumplist{i} '/Ez']);
    hil = hilbert(me*omega0*c/qe*Ez');
    spotsizes(i,:) = sum(0.5*epsilon0*c*abs(hil').^2*1e-4,2);
end

figure
imagesc(spotsizes);