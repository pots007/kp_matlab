% plotSmileiAllIntensity_ne.m
%
% plots all fields in smilei simulation folder.

% Set paths
datafol = '.';
savfol = 'plots_ne';
append_filename = false; % Do we want to append the original dump name to image
save_collated_data = true;

if ~exist(savfol, 'dir'); mkdir(savfol); end;

% Set graphics
hfig = figure(753); clf(hfig); set(hfig, 'Position', [100 100 720, 960]);
colormap(jet_white(256));
locs = GetFigureArray(1, 2, [0.05 0.08 0.08 0.12], 0.05, 'down');

% And run
ff = getSmileiFieldList(datafol);
% And if we want to save the collated data...
if save_collated_data
    dat = plotSmileiIntensity(fullfile(datafol, ff(1).name), ff(1).dump_t, 'Ez');
    allInt = zeros(length(dat.y), length(ff));
end

for f=1:length(ff)
    clf(hfig);
    fprintf('Opening files %s\n', ff(f).name);
    savname = sprintf('dump_%05i', f);
    if exist(fullfile(savfol, [savname '.png']), 'file')
        fprintf('\t File %s already done. \n', ff(f).name);
        continue;
    end
    ax1 = axes('Parent', hfig, 'Position', locs(:,1));
    dat = plotSmileiIntensity(fullfile(datafol, ff(f).name), ff(f).dump_t, 'Ez', ax1);
    savname = sprintf('dump_%05i', f);
    if append_filename
        [~,fname,~] = fileparts(ff(f).name);
        savename = [savname '_' fname];
    end
    %xlabel(ax1, '$\xi\,[c/\omega_0]$');
    ylabel(ax1, '$y \,[c/\omega_0]$');
    cb = colorbar;
    ylabel(cb, 'Intensity $[\mathrm{W/cm}^2]$');
    title(ax1, sprintf('$t=%2.0f \\omega_0 \\equiv L=%2.1f \\mathrm{mm}$', dat.time,...
        dat.time/omega0*c*1e3));
    if save_collated_data
        allInt(:,f) = sum(dat.data,2);
    end
    % And now n_e
    ax2 = axes('Parent', hfig, 'Position', locs(:,2));
    dat = getSmileiField(fullfile(datafol, ff(f).name), ff(f).dump_t, 'Rho_electron');
    imagesc(dat.x, dat.y, -dat.data, 'Parent', ax2);
    xlabel(ax2, '$\xi\,[c/\omega_0]$');
    ylabel(ax2, '$y \,[c/\omega_0]$');
    cb = colorbar;
    ylabel(cb, '$n_e/n_c$');
    make_latex(hfig); setAllFonts(hfig, 16);
    % And save
    export_fig(fullfile(savfol, savname), '-png', '-nocrop', hfig);
    % And if we asked to save stuff...
    
end

if save_collated_data
    save('allIntData', 'allInt');
end