% getSmileiFieldList.m
%
% Smilei, when being instructed to dump one field per dump, isn't too
% clever about renaming these files. This function will return a list of
% timesteps and data contained in the folder, in list of files Fields*.h5
%
% KP DESY 2017

function dumplist = getSmileiFieldList(folder)
if nargin ==0; folder = '.'; end
% Get list of files first:
flist = dir(fullfile(folder, 'Fields*.h5'));
flist = dir(fullfile(folder, 'data*.h5'));

% Loop over files, check what they contain...
fnames = {flist.name};
dump_t = cell(size(fnames));
dump_f = cell(size(fnames));
for i=1:length(flist)
    try
        finfo = h5info(fullfile(folder, flist(i).name));
        dump_t{i} = finfo.Groups.Groups(1).Name;
        dump_f{i} = {finfo.Groups.Groups(1).Datasets.Name};
    catch err
        fprintf('Error parsing file %s: %s\n', flist(i).name, err.message);
    end
end
% Now root out the empty or corrupt files:
logm = ~cellfun(@isempty, dump_t);
fnames_l = fnames(logm); dump_t_l = dump_t(logm); dump_f_l = dump_f(logm);

[~, inds] = sort(dump_t_l);

dumplist = struct('name', fnames_l(inds),...
    'dump_t', dump_t_l(inds),...
    'dump_f', dump_f_l(inds));
