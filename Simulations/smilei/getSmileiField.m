% getSmileiField.m
%
% Retrieves a field from Smilei file. Most useful to run
% getSmileiFieldList.m to get a list of fields, files names and dump times
%
% KP DESY 2017

function dat = getSmileiField(filename, dumpID, field, ax)
Constants;

gridSpacing = h5readatt(filename, [dumpID '/' field], 'gridSpacing');
dat_.time = h5readatt(filename, dumpID, 'time');
field_ = h5read(filename, [dumpID '/' field]);
[ny, nx, nz] = size(field_);
dat_.data = field_; dat_.x = []; dat_.y = []; dat_.z = [];
if nz==1 % 1D or 1D:
    if nx == 1 % 1D sim! Check with a 1D sim how data is saved, though
        dat_.x = 0:gridSpacing(1):(gridSpacing(1)*(nx-1));
    else % 2D!
        dat_.x = 0:gridSpacing(1):(gridSpacing(1)*(nx-1));
        dat_.y = 0:gridSpacing(2):(gridSpacing(2)*(ny-1));
    end
else % Actually 3D!
    % Do three D stuff here
    dat_.x = 0:gridSpacing(1):(gridSpacing(1)*(nx-1));
    dat_.y = 0:gridSpacing(2):(gridSpacing(2)*(ny-1));
    dat_.z = 0:gridSpacing(3):(gridSpacing(3)*(nz-1));
end

if nargin == 4 && ishandle(ax) && strcmp(get(ax, 'Type'), 'axes')
    cla(ax);
    if ny == 1
        dat_.h = plot(ax, dat_.x, dat_.data);
    else
        if nz == 1
            dat.h = imagesc(dat_.x, dat_.y, dat_.data, 'Parent', ax);
        else 
            % Figure out what to do for 3D
        end
    end
end

dat = dat_;