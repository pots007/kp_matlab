% compEPOCH_smilei1.m
%
% Compares the test cases I ran for both EPOCH and smilei.
% Plot the intensity, n_e and x_px phasespace.

datfol = 'D:/EPOCH_smilei';
savfol = fullfile(datfol, 'plots'); saveplots = 1;
if ~exist(savfol, 'dir'); mkdir(savfol); end;
Efol = 'testrun_EPOCH/data';
Sfol = 'testrun_smilei/data';
flist = dir(fullfile(datfol, Efol, 'normal*.sdf'));
dumplist = getSmileiDumpList(fullfile(datfol, Sfol, 'Fields0.h5'));

locs = GetFigureArray(3, 2, [0.08 0.05 0.08 0.06], 0.05, 'across');

hfig = figure(783); clf(hfig);
set(hfig, 'Color', 'w', 'Position', [50 50 1300 700]);

for k=1:length(flist)
    clf(hfig);
    % First plot all things EPOCH related - know how to do this
    ax = [];
    ax(1) = axes('Parent', hfig, 'Position', locs(:,1));
    dataf = GetDataSDF(fullfile(datfol, Efol, flist(k).name));
    Exax = (dataf.Grid.Grid.x-max(dataf.Grid.Grid.x))*omega0/c;
    Eyax = dataf.Grid.Grid.y*omega0/c;
    E_Ez = dataf.Electric_Field.Ez.data;
    hil = hilbert(E_Ez);
    imagesc(Exax, Eyax, 0.5*epsilon0*c*abs(hil').^2*1e-4*1e-18, 'Parent', ax(1));
    axis(ax(1), 'image');
    cb = colorbar;
    ax(2) = axes('Parent', hfig, 'Position', locs(:,2));
    imagesc(Exax, Eyax, 1e-6*dataf.Derived.Number_Density.data'/ncrit(800), 'Parent', ax(2));
    axis(ax(2), 'image');
    ax(3) = axes('Parent', hfig, 'Position', locs(:,3));
    Plotdist_fn(fullfile(datfol, Efol, flist(k).name), 'electron', 'x_px');
    title(ax(3), '');
    % And now for Smilei!
    dinfo = hdf5info(fullfile(datfol, Sfol, 'Fields0.h5'));
    Sbox = dinfo.GroupHierarchy.Attributes(4).Value; % This is the box size
    
    % First Ez, and turn it into intensity
    ax(4) = axes('Parent', hfig, 'Position', locs(:,4));
    s_Ez = h5read(fullfile(datfol, Sfol, 'Fields0.h5'), [dumplist{k} '/Ez']);
    Sxax = linspace(-Sbox(1), 0, size(s_Ez, 2));
    Syax = linspace( -0.5*Sbox(2), 0.5*Sbox(2), size(s_Ez, 1));
    hil = hilbert(me*c*omega0/qe*s_Ez');
    imagesc(Sxax, Syax, 0.5*epsilon0*c*abs(hil').^2*1e-4*1e-18, 'Parent', ax(4));
    axis(ax(4), 'image');
    cb = colorbar;
    ax(5) = axes('Parent', hfig, 'Position', locs(:,5));
    s_ne = h5read(fullfile(datfol, Sfol, 'Fields3.h5'), [dumplist{k} '/Rho_electron']);
    imagesc(Sxax, Syax, -s_ne, 'Parent', ax(5));
    axis(ax(5), 'image');
    
    colormap(jet_white(256));
    set(ax(1:3), 'XAxisLocation', 'top');
    % Common c axis for ne
    set(ax([1 2 4 5]), 'XLim', [-600 0], 'YLim', [-300 300]);
    set(ax([2 5]), 'CLim', [0 0.075]);
    % And all the axis labelling
    ylabel(ax(1), '$y [\omega_0/c]$'); ylabel(ax(2), '$y [\omega_0/c]$');
    ylabel(ax(4), '$y [\omega_0/c]$'); ylabel(ax(5), '$y [\omega_0/c]$');
    xlabel(ax(1), '$x [\omega_0/c]$'); xlabel(ax(2), '$x [\omega_0/c]$');
    xlabel(ax(4), '$x [\omega_0/c]$'); xlabel(ax(5), '$x [\omega_0/c]$');
    % And time
    text(-575, -250, sprintf('$t = %2.0f \\,\\omega_0$', dataf.time*omega0), 'Parent', ax(1));
    make_latex(hfig); setAllFonts(hfig, 12);
    % And save the images!
    
    if saveplots
        export_fig(fullfile(savfol, sprintf('dump%04i', k)), '-png', '-nocrop', hfig);
    end
end


