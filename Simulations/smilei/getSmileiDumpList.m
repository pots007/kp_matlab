% getSmileiDumplist.m
%
% Returns a list of timesteps present in the current file

function dumpList = getSmileiDumpList(fname)

dinfo = hdf5info(fname);
try
    dumpList = {dinfo.GroupHierarchy.Groups.Name};
catch err
    dumpList = {dinfo.GroupHierarchy.Datasets.Name};
end
if strcmp(dumpList, '/data')
    dumpList = {dinfo.GroupHierarchy.Groups.Groups.Name};
end
 