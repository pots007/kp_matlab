% getSmileiAllIntensity.m
%
% Analyses a folder full of data, returning a file with transverse sizes
% and longitudinal cuts.

% Set paths
datafol = '.';

% Check if something is already saved - don't want to redo all the work
if exist(fullfile(datafol, 'Ezdata.mat'), 'file')
    load(fullfile(datafol, 'Ezdata.mat'));
else
    flist = cell(1);
    trans_data = zeros(length(dat.y), length(ff));
    long_data = zeros(length(dat.x), length(ff));
end

% And run
ff = getSmileiFieldList(datafol);

dat = plotSmileiIntensity(fullfile(datafol, ff(1).name), ff(1).dump_t, 'Ez');

for f=1:length(ff)
    tic
    fprintf('Opening file %s ...', ff(f).name);
    % Check if file already done...
    if sum(strcmp(ff(f).name, flist))
        fprintf('\t File %s already done. \n', ff(f).name);
        continue;
    end
    dat = plotSmileiIntensity(fullfile(datafol, ff(f).name), ff(f).dump_t, 'Ez');   
    trans_data(:,f) = sum(dat.data,2);
    long_data(:,f) = dat.data(round(0.5*length(dat.y)),:)';
    flist{f} = ff(f).name;
    fprintf(' in %2.2f s\n', toc);
end
dx = mean(diff(dat.x));
dy = mean(diff(dat.y));
prop_times = cellfun(@(x) str2double(x(7:end)), {ff.dump_t});

save(fullfile(datafol, 'Ezdata'), 'trans_data', 'long_data', 'dx', 'dy', 'prop_times', 'flist');
