% plotSmileiAllIntensity.m
%
% plots all fields in smilei simulation folder.

% Set paths
datafol = '.';
savfol = 'plots';
append_filename = false; % Do we want to append the original dump name to image
save_collated_data = true;
save_int_data_only = false;

if ~exist(savfol, 'dir'); mkdir(savfol); end;

% Set graphics
hfig = figure(753); clf(hfig); set(hfig, 'Position', [100 100 1280, 720]);
colormap(jet_white(256));
ax = gca;

% And run
ff = getSmileiFieldList(datafol);
% And if we want to save the collated data...
if save_collated_data
    dat = plotSmileiIntensity(fullfile(datafol, ff(1).name), ff(1).dump_t, 'Ez');
    allInt = zeros(length(dat.y), length(ff));
end

for f=1:length(ff)
    fprintf('Opening files %s\n', ff(f).name);
    if save_int_data_only
        dat = getSmileiField(fullfile(datafol, ff(f).name), ff(f).dump_t, 'Ez');
    else
        dat = plotSmileiIntensity(fullfile(datafol, ff(f).name), ff(f).dump_t, 'Ez', ax);
        savname = sprintf('dump_%05i', f);
        if append_filename
            [~,fname,~] = fileparts(ff(f).name);
            savename = [savname '_' fname];
        end
        xlabel(ax, '$\xi\,[c/\omega_0]$');
        ylabel(ax, '$y \,[c/\omega_0]$');
        cb = colorbar;
        ylabel(cb, 'Intensity $[\mathrm{W/cm}^2]$');
        title(ax, sprintf('$t=%2.0f \\omega_0 \\equiv L=%2.1f \\mathrm{mm}$', dat.time,...
            dat.time/omega0*c*1e3));
        make_latex(hfig); setAllFonts(hfig, 16);
        % And save
        export_fig(fullfile(savfol, savname), '-png', '-nocrop', hfig);
        % And if we asked to save stuff...
    end
    if save_collated_data
        allInt(:,f) = sum(dat.data,2);
    end
end

if save_collated_data
    save('allIntData', 'allInt');
end