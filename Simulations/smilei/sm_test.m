hfig = figure(100); clf(hfig);

%dat = hdf5read('data/Fields2.h5', '/0000015745/Ey');
dname = 'data/Fields3.h5';
dinfo = hdf5info(dname);
dtimes = {dinfo.GroupHierarchy.Groups.Name};
for i=61%:length(dtimes)
    dat = h5read(dname, [dtimes{i} '/Rho_electron']);
    imagesc(-dat);
    colormap(jet_white(256));
    colorbar;
    title(num2str(i));
    set(gca, 'CLim', [0 0.075]);
    drawnow;
end
%set(gca, 'CLim', [0 0.1]);
