% makeAccHastable
%
% This function will make a hashmap of all particles as they accelerate.
% The key is ID, the fields are x,y,z, px,py,pz, gamma.

%%

dataf = GetDataSDF('data/particles0017.sdf');

IDs = [];
IDs = dataf.Particles.ID.subset_highgamma.electron.data;

% Compare the sizes of one map for all variables and one for each.
% trackMap = containers.Map('KeyType', 'int64', 'ValueType', 'any');
trackMapx = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapy = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapz = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapPx = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapPy = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapPz = containers.Map('KeyType', 'int64', 'ValueType', 'single');
trackMapGamma = containers.Map('KeyType', 'int64', 'ValueType', 'single');
hsize = ceil(length(IDs)/0.75);
hashx = java.util.Hashtable(hsize, 0.75);
hashy = java.util.Hashtable(hsize, 0.75);
hashz = java.util.Hashtable(hsize, 0.75);
hashPx = java.util.Hashtable(hsize, 0.75);
hashPy = java.util.Hashtable(hsize, 0.75);
hashPz = java.util.Hashtable(hsize, 0.75);
hashGamma = java.util.Hashtable(hsize, 0.75);


% Test out the java version, then matlab version.
tic;
for p = 1:length(IDs)
    % ANd matlab implementation...
    trackMapx(IDs(p)) = dataf.Particles.ID.subset_highgamma.electron.grid.x(p);
    trackMapy(IDs(p)) = dataf.Particles.ID.subset_highgamma.electron.grid.y(p);
    trackMapz(IDs(p)) = dataf.Particles.ID.subset_highgamma.electron.grid.z(p);
    trackMapPx(IDs(p)) = dataf.Particles.Px.subset_highgamma.electron.data(p);
    trackMapPy(IDs(p)) = dataf.Particles.Py.subset_highgamma.electron.data(p);
    trackMapPz(IDs(p)) = dataf.Particles.Pz.subset_highgamma.electron.data(p);
    trackMapGamma(IDs(p)) = dataf.Particles.Gamma.subset_highgamma.electron.data(p);
end
tmatlab = toc;
tic
trackMapx2 = containers.Map(IDs, dataf.Particles.ID.subset_highgamma.electron.grid.x);
trackMapy2 = containers.Map(IDs, dataf.Particles.ID.subset_highgamma.electron.grid.y);
trackMapz2 = containers.Map(IDs, dataf.Particles.ID.subset_highgamma.electron.grid.z);
trackMapPx2 = containers.Map(IDs, dataf.Particles.Px.subset_highgamma.electron.data);
trackMapPy2 = containers.Map(IDs, dataf.Particles.Px.subset_highgamma.electron.data);
trackMapPz2 = containers.Map(IDs, dataf.Particles.Px.subset_highgamma.electron.data);
trackMapGamma2 = containers.Map(IDs, dataf.Particles.Gamma.subset_highgamma.electron.data);
tmatlab2 = toc;

tic
for p = 1:length(IDs)
    temp = trackItem;
    %temp = dataf.Particles.ID.subset_highgamma.electron.grid.x(p);
    hashx.put(IDs(p), dataf.Particles.ID.subset_highgamma.electron.grid.x(p));
    hashy.put(IDs(p), dataf.Particles.ID.subset_highgamma.electron.grid.y(p));
    hashz.put(IDs(p), dataf.Particles.ID.subset_highgamma.electron.grid.z(p));
    hashPx.put(IDs(p), dataf.Particles.Px.subset_highgamma.electron.data(p));
    hashPy.put(IDs(p), dataf.Particles.Py.subset_highgamma.electron.data(p));
    hashPz.put(IDs(p), dataf.Particles.Pz.subset_highgamma.electron.data(p));
    hashGamma.put(IDs(p), dataf.Particles.Gamma.subset_highgamma.electron.data(p));
end
tjava = toc;



fprintf('java version took %2.2f s\n', tjava);
fprintf('Matl version took %2.2f s\n', tmatlab);
fprintf('Mat2 version took %2.2f s\n', tmatlab2);