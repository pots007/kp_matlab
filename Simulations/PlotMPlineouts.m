load divmap.mat;
simtype = 'data';
Constants;
files = dir([simtype '/*.sdf']);
ne0 = 9*1.53e24;
figure(7)
set(7, 'WindowStyle', 'docked');
if (~(exist('Eyne', 'dir')==7))
    mkdir('Eyne');
end
plotall = false;
for i=1:length(files)
    if plotall
        %Plot all
    else
        if (exist(['Eyne/' files(i).name(1:4) '.png'], 'file'))
            continue;
        end
    end
    disp(['File ' files(i).name]);
    clf;
    dataf = GetDataSDF([simtype '/' files(i).name]);
    x = dataf.Grid.Grid.x;
    x = x(1:end-1);
    Ey = dataf.Electric_Field.Ey.data';
    Eyline = Ey(round(size(Ey,1)*0.5),:);
    Ehil = abs(hilbert(Eyline));
    a0 = qe*abs(Ehil)./(me*c*2*pi*c/8e-7);
    plot(x/c*1e15, a0);
    [PKS,locs] = findpeaks(double(a0), 'minpeakdistance', 1000);
    
    text(double(x(locs)/c*1e15), double(a0(locs)), strcat(num2str(round( x(locs)/c*1e15)), ' fs'))
    xlabel('Time / fs');
    ylabel('a0');
    title(['After ' num2str(round(dataf.time*1e15)) ' fs']);
    hold on;
    ne = dataf.Derived.Number_Density.electron.data';
    neline = smooth(ne(round(size(ne,1)*0.5),:),17);
    ax1 = gca;
    ax2 = axes('Position', get(ax1, 'Position'));
    line(x/c*1e15, neline/ne0, 'Parent', ax2, 'LineWidth', 1, 'Color', 'r');
    set(ax2, 'XAxisLocation', 'bottom',...
        'YAxisLocation', 'right', 'Color', 'none', 'XColor', 'k', 'YColor', 'r',...
        'XLim', get(ax1, 'XLim'), 'XTickLabel', []);
    set(get(ax2, 'YLabel'), 'String', '\delta n_e / n_0');
    setAllFonts(7,16)
    export_fig(['Eyne/' files(i).name(1:4)], '-transparent', '-nocrop', '-png', 7);
end