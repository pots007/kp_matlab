fol = 'data'
filen = dir([fol '/*.sdf']);
if (~(exist('plots', 'dir')==7))
    mkdir('plots');
end
figure(1);
set(1, 'WindowStyle', 'docked');
Ens = zeros(1, length(filen));
for i=1:length(filen)
    dataf = GetDataSDF([fol '/' filen(i).name]);
    subplot(2,2,1)
    x = dataf.Grid.Grid.x;
    x = x(1:length(x)-1);
    tim = num2str(dataf.time * 1e15);
    hE = plotEwithphase(dataf.Electric_Field.Ey.data, x, 2*pi*3e8/8e-7);
    title (['Electric field after ' tim ' fs']);
    set(get(hE(1),'XLabel'), 'String', 'Propagation distance (m)');
    subplot(2,2,2);
    plotEspectrumwithphase(dataf.Electric_Field.Ey.data, x, 2*pi*3e8/8e-7, 0);
    title (['Spectrum after ' tim ' fs']);
    subplot(2,2,3);
    plotEspectrumwithphase(dataf.Electric_Field.Ey.data, x, 2*pi*3e8/8e-7, 1);
    title (['Spectrum after ' tim ' fs']);
    subplot(2,2,4);
    plot(x, smooth(dataf.Derived.Number_Density.electron.data));
    title (['n_e after ' tim ' fs']);
    Ens(i) = sum(dataf.Electric_Field.Ey.data.^2);
    drawnow;
    %saveas(1, ['plots/' filen(i).name(1:4) '.png']);
    export_fig(['plots/' filen(i).name(1:4) '.pdf'], '-pdf',1);
end