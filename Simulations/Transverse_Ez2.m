load divmap.mat;
fold = 'data';
plotgraphs = true;

filen = dir([fold '/*_Ez.sdf']);
if (isempty(filen))
    filen = dir([fold '/normal*.sdf']);
end

if (~(exist('Ez', 'dir')==7))
    mkdir('Ez');
end
if (exist('Ez/transverse.mat','file')==2)
    load Ez/transverse.mat;
else
    transverse = cell(10000,6);
end

for i=2:length(filen)
    if (strcmp(transverse{i,1}, filen(i).name)==1)
        continue
    end
    transverse{i,1} = filen(i).name;
    %dataf = GetDataSDF([fold '/' filen(i).name]);
    dataf = GetDataSDFchoose([fold '/' filen(i).name], 'ez');
    Ez = dataf.Electric_Field.Ez.data';
%     if (isfield(dataf, 'Grid'))
%         y = dataf.Grid.Grid.y;
%         transverse{i,3} = y(1:length(y)-1);
%     end
    try
        grid = GetDataSDFchoose([fold '/' filen(i).name], 'grid');
        x = grid.Grid.Grid.x;
        y = grid.Grid.Grid.y;
        transverse{i,3} = x(1:length(x)-1);
        gridfact = (x(2)-x(1))/(y(2)-y(1));
    catch
        
    end
    %transverse{i,2} = sum(Ez.^2,2); % For true transverse, below longitudinal
    transverse{i,2} = abs(hilbert(Ez(round(0.5*size(Ez,1)),:))).^2;
    transverse{i,4} = sum(sum(Ez.^2));
    transverse{i,5} = dataf.time;
    %transverse{i,6} = widthfwhm(transverse{i,2}');
    transverse{i,6} = widthfwhm(sum(Ez.^2,2)')/gridfact;
    disp(['File ' filen(i).name]);
end
save('Ez/transverse.mat', 'transverse');

if (plotgraphs)
    figure(15)
    set(15, 'WindowStyle', 'docked');
    clf;
    subplot(1,2,1);
    focusing = zeros(length(transverse{2,2}),i);
    energies = NaN(1,i);
    times = zeros(1,i);
    fwhms = NaN(1,i);
    for m = 2:i
        focusing(:,m) = transverse{m,2}';%/max(transverse{m,2});
        energies(m) = transverse{m,4};
        times(m) = transverse{m,5};
        fwhms(m) = transverse{m,6};
    end
    y = [];
    m=2;
    while(isempty(y))
        y = transverse{m,3};
        m = m+1;
    end
    prop = times*3e8*1e3; %propagation distance in mm;
    imagesc(prop, y*1e6, focusing);
    hold on;
    plot(prop, 0.5*fwhms*(y(2)-y(1))*1e6, 'Linewidth', 1.5, 'Color', 'w');
    %plot(prop, -0.5*fwhms*(y(2)-y(1))*1e6, 'Linewidth', 1.5, 'Color', 'w');
    ylabel('Transverse dimension (\mum)');
    xlabel('Propagation distance (mm)');
    title('E_z spot size as a function of propagation distance');
    ylabel('longitudinal dimension (\mum)');
    title('Axial intensity as a function of propagation distance');
    hold off;
    set(gca, 'YDir', 'normal');
    subplot(1,2,2);
    energies = energies./max(energies);
    plot(prop, energies);
    xlabel('Propagation distance (mm)');
    ylabel('Normalised integrated field');
    title('Integrated E_z as a function of propagtion distance');
    export_fig('Ez/transverse.pdf', '-pdf',15);
end