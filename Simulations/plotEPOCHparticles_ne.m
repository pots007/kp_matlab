%% Get the data
simfolder = 'data4/';
filen = dir([simfolder 'particles*.sdf']);
ne_offset = 49;
ens = [100 200 500 1000 1500];
widths = NaN(length(filen),length(ens));
centrs = NaN(length(filen),length(ens));
propa = NaN(length(filen),1);
for i=150:length(filen)-1
    disp(['File ' num2str(i)]);
    specs = {'electron'};%{'ebeam', 'shell7'};
    for k=1:1
        try
            datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['grid/subset_highgamma/' specs{k}]);
            y = datp.Grid.Particles.subset_highgamma.(specs{k}).y*1e6; %in microns
            x = datp.Grid.Particles.subset_highgamma.(specs{k}).x;
            propa(i) = c*datp.time*1e3;
            datp = GetDataSDFchoose(sprintf('%s/particles%04i.sdf', simfolder,i), ['gamma/subset_highgamma/' specs{k}]);
            gamma = datp.Particles.Gamma.subset_highgamma.(specs{k}).data;
            for l=1:length(ens)
                logm = gamma>ens(3);
                if sum(logm)==0 continue; end
                xp = x(logm); yp = y(logm); maxx = max(xp);
                logm2 = xp > (maxx - 20e-6) & abs(yp) < 100;
                widths(i,l) = sqrt(var(yp(logm2)));
                centrs(i,l) = mean(yp(logm2));
            end
            dataf = GetDataSDFchoose(sprintf('%s/normal%04i.sdf', simfolder,i+ne_offset), 'grid');
            nex = dataf.Grid.Grid.x;
            ney = dataf.Grid.Grid.y*1e6;
            dataf = GetDataSDFchoose(sprintf('%s/normal%04i.sdf', simfolder,i+ne_offset), ['number_density/' specs{k}]);
            ne = dataf.Derived.Number_Density.(specs{k}).data';
            clf;
            imagesc(nex,ney,ne); colormap(flipud(gray(256)));
            set(gca, 'CLim', [0 0.8*max(ne(:))], 'NextPlot', 'add');
            plot(xp(logm2), yp(logm2), 'or', 'MarkerSize', 2);
            drawnow
            pause(0.1);    
        catch
            
        end
    end
end
