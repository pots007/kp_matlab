function makeRefractiveIndex(material, path)
if nargin==1
    path = './';
end
betaf = [path material '_beta.dat'];
deltaf = [path material '_delta.dat'];

beta = importdata(betaf);
delta = importdata(deltaf);
outf = fopen([path material '.dat'], 'w');
for i=1:length(beta)
    fprintf(outf, '%e\t%e\t%e\n', beta(i,1), beta(i,2), delta(i,2));
end
fclose(outf);