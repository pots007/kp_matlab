figure(7)
Constants;
dataf = GetDataSDF('data/0030.sdf');
x = dataf.Grid.Grid.x;
x = x(1:length(x)-1);
y = dataf.Grid.Grid.y;
y = y(1:length(y)-1);
time = dataf.time*1e15;
Ex = dataf.Electric_Field.Ex.data';
Ey = dataf.Electric_Field.Ey.data';
lineoutEx = Ex(floor(length(y)*0.5), :);
lineoutEy = Ey(floor(0.5*length(y)), :);
subplot(2,3,1)
imagesc(x, y, Ex)
title(['Ex after ' num2str(time) ' fs']);
colormap(divmap);
%set(gca, 'CLim', [-3e10 3e10]);
%colorbar;
subplot(2,3,2)
imagesc(x, y, Ey);
title(['Ey after ' num2str(time) ' fs']);
%set(gca, 'CLim', [-2e13 2e13]);
%colorbar;
subplot(2,3,3)

plotEwithphase(lineoutEy, x', 2*pi*3e8/8e-7);
%axis tight;
title(['Ey after ' num2str(time) ' fs']);

subplot(2,3,4)
phi = zeros(1,length(lineoutEx));
lineoutEx = fliplr(lineoutEx);
phi(1) = lineoutEx(1);
for i=2:length(phi)
   phi(i) = phi(i-1) + lineoutEx(i); 
end
%phi = trapz(lineoutEx);
phi = phi*(x(2) - x(1));
phi = fliplr(-phi);
lineoutEx = fliplr(lineoutEx);
AX = plotyy(x, phi, x, lineoutEx);
set(get(AX(1), 'Ylabel'), 'String', '$\phi (V)$', 'Interpreter', 'latex');
set(get(AX(2), 'Ylabel'), 'String', '$E_x (V/m)$', 'Interpreter', 'latex');

subplot(2,3,5)
[maxv maxind] = max(max(Ey.^2));
xmax = x(maxind);
h1 = plot(x, phi, 'b');%, x, envEy, 'r');
axis tight;
hold on;
h2 = plot([xmax xmax], get(gca, 'YLim'), 'k');
intlim = 1.5e23;
indi = (abs(hilbert(lineoutEy)).^2*0.5*c*epsilon0>intlim);
[xmin mini] = min(x(indi));
[xmax maxi] = max(x(indi));
h3 = plot([xmin xmin], get(gca, 'YLim'), 'r');
h4 = plot([xmax xmax], get(gca, 'YLim'), 'r');
hold off;
legend([h1 h2 h3], 'Potential', 'Peak intensity', ['I>' sprintf('%2.1e', intlim)], ...
    'Location', 'Southwest');

subplot(2,3,6)
title('n_e');
ne = dataf.Derived.Number_Density.electron.data';
hne = plotnewithlaser(x, y, ne, Ey);
axis tight;
title('n_e');

%export_fig(['plots/' files(i).name(1:4) '.png'], '-png',1);