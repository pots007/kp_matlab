% Looks through folders and checks whether there's any data to be plotted.

rfol = '/Volumes/KRISMOOSE3/Gem2015RampScan2/';
%rfol = 'E:\Kristjan\Documents\Uni\Imperial\EPOCH\GeminiSpotTests\';
fols = {'Gem2015RS2a/data/', 'Gem2015RS2b/data/'};

figure(77); set(77, 'Position', [100 100 720 980], 'Color', 'w');
locs = GetFigureArray(1, 3, 0.1, 0.05, 'down');
axs = zeros(1,3);
load('owncolourmaps');
ylims = [-40 40]*1e-6; clims = [0 35];
if ~(exist([rfol 'compa0'], 'dir'))
    mkdir([rfol 'compa0']);
end
F40fils = dir_names([rfol fols{1} 'normal*.sdf']);
F20fils = dir_names([rfol fols{2} 'normal*.sdf']);
ens = nan(2,1000);

for k=1:min([length(F40fils) length(F20fils)])
    
    if exist(sprintf('%scompa0/normal%04i.png', rfol, k), 'file')
        continue;
    end
    sprintf('%sSIsuppionS/SI_II/normal%04i.png', rfol, k)
    figure(77); clf;
    axs(1) = axes('Parent',77, 'Position', locs(:,1), 'NextPlot', 'add');
    dataf = GetDataSDFchoose(fullfile(rfol, fols{1}, F40fils{k}), 'grid'); 
    x40 = dataf.Grid.Grid.x(1:end-1);
    y40 = dataf.Grid.Grid.y(1:end-1);
    dataf = GetDataSDFchoose(fullfile(rfol, fols{1}, F40fils{k}), 'Ez');
    Ez = double(dataf.Electric_Field.Ez.data);
    I40 = 0.5*epsilon0*c*abs(hilbert(Ez).^2)';
    a0_40 = 0.856*0.8*sqrt(I40*1e-22);
    imagesc(x40, y40, I40*1e-22, 'Parent', axs(1));
    title('F 40')
    set(axs(1), 'XTickLabel', [], 'YLim', ylims, 'CLim', clims);
    ylabel('y / \mu m');
    cb = colorbar;
    set(cb, 'Position', [0.91 locs(2,1) 0.01 locs(4,1)]);
    drawnow;
    
    axs(2) = axes('Parent', 77, 'Position', locs(:,2), 'NextPlot', 'add');
    dataf = GetDataSDFchoose(fullfile(rfol, fols{2}, F20fils{k}), 'grid'); 
    x20 = dataf.Grid.Grid.x(1:end-1);
    y20 = dataf.Grid.Grid.y(1:end-1);
    dataf = GetDataSDFchoose(fullfile(rfol, fols{2}, F20fils{k}), 'Ez');
    Ez = double(dataf.Electric_Field.Ez.data);
    I20 = 0.5*epsilon0*c*abs(hilbert(Ez).^2)';
    a0_20 = 0.856*0.8*sqrt(I20*1e-22);
    imagesc(x20, y20, I20*1e-22, 'Parent', axs(2));
    title('F 20');
    set(axs(2), 'XTickLabel', [], 'YLim', ylims, 'CLim', clims);
    ylabel('y / \mu m');
    cb = colorbar;
    set(cb, 'Position', [0.91 locs(2,2) 0.01 locs(4,2)]);
    drawnow;
    
    axs(3) = axes('Parent',77, 'Position', locs(:,3));
    C = contourc(I40, 0.9*max(I40(:))*[1 1]);
    logm = C(1,:) < length(x40);
    % Index for lineout of highest intensity
    [~, ind] = min(abs(x40 - mean(x40(round(C(1,logm))))));
    plot(axs(1), x40(ind)*[1 1], [min(y40) max(y40)], '--k')
    lineoutF40 = a0_40(:,ind);
    FHB_F40 = sum(a0_40,2);
    
    C = contourc(I20, 0.9*max(I20(:))*[1 1]);
    logm = C(1,:) < length(x20);
    % Index for lineout of highest intensity
    [~, ind] = min(abs(x20 - mean(x20(round(C(1,logm))))));
    plot(axs(2), x20(ind)*[1 1], [min(y20) max(y20)], '--k')
    lineoutF20 = a0_20(:,ind);
    FHB_F20 = sum(a0_20,2);
    
    ytF40 = SetSmartUnit(y40); ytF20 = SetSmartUnit(y20);
    hs = plot(ytF40.t, lineoutF40, '--r', ytF20.t, lineoutF20, '--k');
    axs(4) = axes('Parent', 77, 'Position', locs(:,3), 'Color', 'none', 'XAxisLocation', 'top',...
        'YAxisLocation', 'right', 'Box', 'off');
    hold on;
    plot(axs(4), ytF40.t, FHB_F40, '-r', ytF20.t, FHB_F20, '-k');
    hold off;
    legend(hs, {'F 40', 'F 20'}, 'Location', 'NorthWest');
    %ylabel('Intensity / 10^{18} W/cm^2');
    ylabel(axs(3), 'a_0');
    xlabel(axs(3), ['y / ' ytF20.unit 'm']);
    linkaxes(axs(1,2), 'x');
    set(axs(3:4), 'XLim', [-100 100]);
    colormap(77, colmap.jet_white);
    % And add up the energies
    %C = contourc(I40, exp(-2)*max(I40(:))*[1 1]);
    %logm = C(1,:) < length(x40) & C(2,:) < length(y40);
    % A very rough one:
    ens(1,k) = sum(I40(I40>exp(-1)*max(I40(:))));
    ens(2,k) = sum(I20(I20>exp(-1)*max(I20(:))));
    
    setAllFonts(77,18);
    export_fig(sprintf('%scompa0/normal%04i.png', rfol, k), '-nocrop', '-png', 77);
end

logm = ~isnan(ens(1,:));
figure(78); set(78, 'Color', 'w');
hh = plot(1:sum(logm), ens(1,logm), '-r', 1:sum(logm), ens(2,logm), '--k');
legend(hh, {'F 40', 'F 20'}, 'Location', 'best');
