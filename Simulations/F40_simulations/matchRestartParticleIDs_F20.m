% matchRestartParticleIDs_F20.m
%
% This script will look at the discontuities of particle ID numbering
% between restart dumps. To be used for Gem3D_6 (F20) run. 
%
% We literally take the easiest approach, and look at which particle in the
% set of energies is the closest to where we would assume the particle
% would go in the time between dumps. We then make a hashmap of this, with
% the particle ID before the restart being the key to the new partcile ID
% after the restart.
 
%% This part does the only restart

dataf = '/Volumes/SimKris/Gem3D_6/data/';
predataf = GetDataSDF(fullfile(dataf, 'particles0037.sdf'));
postdataf = GetDataSDF(fullfile(dataf, 'particles0036.sdf'));
logm1 = predataf.Particles.Gamma.subset_highgamma.electron.data>1600;
logm2 = postdataf.Particles.Gamma.subset_highgamma.electron.data>1600;
rr1 = [predataf.Grid.Particles.subset_highgamma.electron.x(logm1),...
    predataf.Grid.Particles.subset_highgamma.electron.y(logm1),...
    predataf.Grid.Particles.subset_highgamma.electron.z(logm1)];
gammas1 = predataf.Particles.Gamma.subset_highgamma.electron.data(logm1);
IDs1 = predataf.Particles.ID.subset_highgamma.electron.data(logm1);
v1 = c*(1-1/gammas1.^2);
rr2 = [postdataf.Grid.Particles.subset_highgamma.electron.x(logm2),...
    postdataf.Grid.Particles.subset_highgamma.electron.y(logm2),...
    postdataf.Grid.Particles.subset_highgamma.electron.z(logm2)];
gammas2 = postdataf.Particles.Gamma.subset_highgamma.electron.data(logm2);
IDs2 = postdataf.Particles.ID.subset_highgamma.electron.data(logm2);
v2 = c*(1-1/gammas2.^2)';

dt = predataf.time-postdataf.time;
% Find the distances from particle X
 
ii = 3171;
ii =randi(size(rr1,1),1);
rrii = rr1(ii,:); rrii = repmat(rrii, size(rr2,1), 1);
 
dists = abs(sqrt(sum((rr2 - rrii).^2,2) - (v2*dt).^2));
% Try to be clever
plot(dists, 'x'); set(gca, 'YLim', [0 3e-7])
%
IDmap = containers.Map('Keytype', 'int64', 'Valuetype', 'int64');
mindists = [];
for ii=1:size(rr1,1)
    %ii = 1731;
    rrii = rr1(ii,:); rrii = repmat(rrii, size(rr2,1), 1);
    
    dists = abs(sqrt(sum((rr2 - rrii).^2,2) - (v2*dt).^2));
    [mindists(ii), minind] = min(dists);
    IDmap(IDs1(ii)) = IDs2(minind);
    % Try to be clever
    %plot(dists, 'x'); set(gca, 'YLim', [-15e-14 15e-14])
end

