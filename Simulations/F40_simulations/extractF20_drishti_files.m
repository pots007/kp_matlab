% extractF20_drishti_files.m
%
% This script will loop through density dumps and save them as Drishti
% compatible files, in a separate folder. The diagnostic is interpolated
% onto a fixed grid to make lif faster.

datfol = '/Volumes/SimKris/Gem3D_6';
flist = dir(fullfile(datfol, 'data/normal*.sdf'));
% Also write the project files...
%prjbase = getFileText(fullfile(getGDrive, 'epoch', 'Gem2015_3D', 'Gem3D_6', 'normal_ne_interp.xml'));

for i=2:1:length(flist)
    %fname = 'normal0050';
    fprintf('Working on file %3i, filename %s\n', i, flist(i).name);
    [~, fname, ~] = fileparts(flist(i).name);
    dataf = GetDataSDFchoose(fullfile(datfol, 'data', [fname '.sdf']), 'grid');
    x = dataf.Grid.Grid.x(1:end-1)*1e6;
    y = dataf.Grid.Grid.y(1:end-1)*1e6;
    z = dataf.Grid.Grid.z(1:end-1)*1e6;
    
    %xr = double(linspace(min(x), max(x), 200));
    %yr = linspace(-50, 50, 140*1.5);
    %zr = linspace(-50, 50, 140*1.5);
    % Use fixed, 0.5 micron voxels
    xr = double(min(x):0.5:max(x));
    yr = -50:0.5:50;
    zr = -50:0.5:50;
    
    dataf = GetDataSDFchoose(fullfile(datfol,  'data', [fname '.sdf']), 'number_density/electron');
    ne = dataf.Derived.Number_Density.electron.data*1e-6;
    ne_ = imgaussfilt3(ne, 1.5);
    dataf = GetDataSDFchoose(fullfile(datfol,  'data', [fname '.sdf']), 'Ez');
    Ez = dataf.Electric_Field.Ez.data;
    Ezh = zeros(size(Ez));
    for ii = 1:size(Ez,3)
        Ezh(:,:,ii) = abs(hilbert(Ez(:,:,ii)));
    end
    Ezh_ = imgaussfilt3(Ezh, 1.5);
    
    %
    
    [XX,YY,ZZ] = meshgrid(y, x, z);
    ne_ = interp3(XX,YY,ZZ,ne_, yr, xr', zr);
    ne_f = imgaussfilt3(ne_, 1.5);
    % And the I as well:
    Ezh_ = interp3(XX,YY,ZZ,Ezh_, yr, xr', zr);
    Ezh_f = imgaussfilt3(Ezh_, 1.5)*0.5*epsilon0*c;
    
    
    ne_f_y = mean(ne_f(:,99:101,:),2);
    
    ne_f2 = ne_f; 
    ne_f2 = cat(2, ne_f2, repmat(ne_f_y, [1 4 1]));
    %ne_f2(:,end+1,:) = ne_f_y;
    %ne_f2(:,end+1,:) = ne_f_y;
    %ne_f2(:,end+1,:) = ne_f_y;
    %ne_f2(:,end+1,:) = ne_f_y;
    ne_f2(:,end+1,:) = ones(size(ne_f_y), 'single')*4e18;
    
    ne_f_z = mean(ne_f2(:,:,99:101),3);
    %ne_f2(:,:, end+1) = ne_f_z; % This doesn't work!
    ne_f2 = cat(3, ne_f2, repmat(ne_f_z, [1 1 4]));
    ne_f2 = cat(3, ne_f2, ones(size(ne_f_z), 'single')*4e18);
    % And mask out the main thing
    ne_f2(:, 1:201, 1:201) = 0;
    ne_f2(:, 201:end, 201:end) = 0;
    ne_f2(ne_f2>60e18) = 60e18;
    %DrishtiWriteUcharVolume(ne_f, fullfile(datfol, 'drishti_ne', [fname '_ne_interp']), 1e18, 64e18);
    DrishtiWriteUcharVolume(ne_f, fullfile(datfol, 'drishti_ne', [fname '_ne_interp2']), 1e18, 64e18);
    DrishtiWriteUcharVolume(ne_f2, fullfile(datfol, 'drishti_ne', [fname '_ne_interp3']), 1e18, 64e18);
    DrishtiWriteUcharVolume(Ezh_f, fullfile(datfol, 'drishti_ne', [fname '_Ez_interp']), 1e18, 64e18);
    %ParaviewWriteUcharVolume2(ne_f, fullfile(datfol, 'drishti_ne', [fname '_ne_interp_pv']), 1e18, 256e18);
    %vtkwrite(fullfile(datfol, 'drishti_ne', [fname '_ne_interp_pv.vtk']), 'structured_points', 'ne', uint8(ne_f*1e-18), 'BINARY');
    %prjbase{12} = sprintf('<name>normal%04i_interp.pvl.nc</name>', i);
    % Hardcoded -> filename is on line 12
    %fid = fopen(fullfile(datfol, 'drishti_ne', [fname '_prj.xml']), 'w');
    %for k=1:length(prjbase)
    %    fprintf(fid, '%s\n', prjbase{k});
    %end
    %fclose(fid);
end
disp('Done');

%% And now, assuming all the saved images are in GDrive, add timestamps

imfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D', 'Gem3D_6', 'drishti_ne_proj');
sdflist = dir(fullfile(datfol, 'data', 'normal*.sdf'));
imlist = dir(fullfile(imfol, '*.png'));
for i=1:length(imlist)
    fprintf('Working on image %i out of %i\n', i, length(imlist));
    im = imread(fullfile(imfol, imlist(i).name));
    im = im(101:1100, 201:1300,:);
    tim = getEPOCHtime(fullfile(datfol, 'data', ['normal' imlist(i).name(5:8) '.sdf']));
    im = insertText(im, [30 30], sprintf('t = %2.1f ps', tim*1e12),...
        'BoxColor', 'w', 'FontSize', 36, 'Font', 'Arial');
    imwrite(im, fullfile(imfol, 'dump_times', imlist(i).name));
end