% compareF20_F40_sims_pulselengths.m
%
% Plots laser pulse length and peak intensity

if ismac
    rootfol = '/Volumes/SimKris';
elseif ispc
    rootfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
end
fols{1} = 'Gem3D_5'; % F/40
fols{2} = 'Gem3D_6'; % F/20
fols{3} = 'Gem3D_7'; % F/10

for k=1:3
    Ez_y{k} = load(fullfile(rootfol, fols{k}, 'data', 'Ez_all_y.mat'));
    %Ez_z{k} = load(fullfile(rootfol, fols{k}, 'data', 'Ez_all_z.mat'));
    %ne_y{k} = load(fullfile(rootfol, fols{k}, 'data', 'Number_Density_electron_all_y.mat'));
    %ne_z{k} = load(fullfile(rootfol, fols{k}, 'data', 'Number_Density_electron_all_z.mat'));
end

datsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
figsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');

%% Calculate things
%  find the bubble size in that places

av_cell = 1; % Averages over 3 cells 
av_nes = 1;  % Averages over 5 longitudinal cells
I_axis = cell(3,1);
I_axis_norm = cell(size(I_axis));
a0_max = cell(3,1);
tau_laser = cell(3,1);

for i=1:3
    I_axis{i} = zeros(length(Ez_y{i}.grid_t), size(Ez_y{i}.slicedata{1},1));
    I_axis_norm{i} = zeros(size(I_axis{i}));
    tau_laser{i} = zeros(length(Ez_y{i}.grid_t), 3);
    dx = mean(diff(Ez_y{i}.grid_x{1}));
    for k=1:size(I_axis{i})
        Ez_loc = Ez_y{i}.slicedata{k};
        [nx, ~] = size(Ez_loc);
        I_loc = 0.5*epsilon0*c*abs(hilbert(Ez_loc)).^2;
        mid_ind = round(0.5*size(I_loc,2));
        I_line = mean(I_loc(:, mid_ind-av_cell:mid_ind+av_cell),2);
        I_line = smooth(I_line);
        I_axis{i}(k,1:nx) = smooth(I_line);
        I_axis_norm{i}(k,1:nx) = I_line/max(I_line);
        fwhm_width = widthfwhm(I_line(700:end)')*dx/c*1e15;
        rms_length = widthCentral1e(I_line(700:end))*dx/c*1e15;
        if ~isempty(fwhm_width); tau_laser{i}(k,1) = fwhm_width; end
        tau_laser{i}(k,2) = rms_length;
    end
end

%% This plots the intensity evolution
hfig = figure(750); clf(hfig); hfig.Color = 'w';
if ismac
    hfig.Position = [200 200 1000 500];
elseif ispc
    hfig.Position = [100 100 1000 500];
end
locs = GetFigureArray(3, 1, [0.06 0.14 0.13 0.1], 0.02, 'across');
ax = [];
for i=1:3
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,i));
    imagesc(Ez_y{i}.grid_time*1e3*c, Ez_y{1}.grid_x{1}*1e6, 1e-4*I_axis{i}', 'Parent', ax(end));
    xlabel('Propagation / mm');
end

set(ax, 'YDir', 'normal', 'XLim', [0 12], 'CLim', [0 2e20], 'YLim', [-10 60],...
    'LineWidth', 2, 'Layer', 'top');
set(ax(2:3), 'YTickLabel', []);
ylabel(ax(1), '$\xi$ / micron');
title(ax(1), 'F/40'); title(ax(2), 'F/20'); title(ax(3), 'F/10');
cb = colorbar(ax(end), 'Position', [0.88 locs(2,1) 0.02 locs(4,1)], 'LineWidth', 2);
ylabel(cb, 'Intensity / $\mathrm{W\,cm}^{-2}$')

setAllFonts(hfig, 16); make_latex(hfig);

if saveplots
    export_fig(fullfile(figsavfol, 'LaserPulseLengths'), '-pdf', '-nocrop', hfig);
end

%% This plots the intensity evolution, with each step normalised
hfig = figure(751); clf(hfig); hfig.Color = 'w';
if ismac
    hfig.Position = [200 200 1000 500];
elseif ispc
    hfig.Position = [100 100 1000 500];
end
locs = GetFigureArray(3, 1, [0.06 0.14 0.13 0.1], 0.02, 'across');
ax = [];
for i=1:3
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,i));
    imagesc(Ez_y{i}.grid_time*1e3*c, Ez_y{1}.grid_x{1}*1e6, I_axis_norm{i}', 'Parent', ax(end));
    xlabel('Propagation / mm');
end

set(ax, 'YDir', 'normal', 'XLim', [0 12], 'CLim', [0 1], 'YLim', [-10 60],...
    'LineWidth', 2, 'Layer', 'top');
set(ax(2:3), 'YTickLabel', []);
ylabel(ax(1), '$\xi$ / micron');
title(ax(1), 'F/40'); title(ax(2), 'F/20'); title(ax(3), 'F/10');
cb = colorbar(ax(end), 'Position', [0.88 locs(2,1) 0.02 locs(4,1)], 'LineWidth', 2);
ylabel(cb, 'Intensity normalised to max of each dump / a.u.')
setAllFonts(hfig, 16); make_latex(hfig);

if saveplots
    export_fig(fullfile(figsavfol, 'LaserPulseLengthsNorm'), '-pdf', '-nocrop', hfig);
end

%% This plots the pulsewidth for different simulations
hfig = figure(752); clf(hfig); hfig.Color = 'w';
if ismac
    hfig.Position = [200 200 600 400];
elseif ispc
    hfig.Position = [100 100 800 500];
end
locs = GetFigureArray(1, 1, [0.08 0.1 0.15 0.15], 0.02, 'across');
ax = []; ax(end+1) = axes('Parent', hfig, 'Position', locs, 'NextPlot', 'add'); 
hh = [];
for i=1:3
    hh(end+1) = plot(ax, Ez_y{i}.grid_time*c*1e3, tau_laser{i}(:,1), '-', 'LineWidth', 1.5);
    %hh(end+1) = plot(ax, Ez_y{i}.grid_time*c*1e3, tau_laser{i}(:,2), '.-');
end

set(ax, 'YDir', 'normal', 'XLim', [0 12], 'YLim', [-1 60],...
    'LineWidth', 2, 'Layer', 'top', 'Box', 'on');
xlabel('Propagation / mm');
ylabel(ax, 'Pulselength / fs');
legend(hh, {'F/40', 'F/20', 'F/10'});
setAllFonts(hfig, 16); make_latex(hfig);
grid on; makeNiceGridHG2(gca);

if saveplots
    export_fig(fullfile(figsavfol, 'LaserPulseLengthsComp'), '-pdf', '-nocrop', hfig);
end



