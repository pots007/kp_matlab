% This function compiles and saves all the data we need to plot things
% about the F40, F20 and F10 simulations.

if ismac
    rootfol = '/Volumes/SimKris';
elseif ispc
    rootfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
end
fols{1} = 'Gem3D_5'; % F/40
fols{2} = 'Gem3D_6'; % F/20
fols{3} = 'Gem3D_7'; % F/10

for k=1:3
    Ez_y{k} = load(fullfile(rootfol, fols{k}, 'data', 'Ez_all_y.mat'));
    Ez_z{k} = load(fullfile(rootfol, fols{k}, 'data', 'Ez_all_z.mat'));
    %Ex_y{k} = load(fullfile(rootfol, fols{k}, 'data', 'Ex_all_y.mat'));
    %Ex_y{k} = load(fullfile(rootfol, fols{k}, 'data', 'Ex_all_y.mat'));
    %ne_y{k} = load(fullfile(rootfol, fols{k}, 'data', 'Number_Density_electron_all_y.mat'));
    %ne_z{k} = load(fullfile(rootfol, fols{k}, 'data', 'Number_Density_electron_all_z.mat'));
    Ez_x{k} = load(fullfile(rootfol, fols{k}, 'data', 'Ez_LongIntegral.mat'));
end
datsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
%% Try to look at how the spot size varies along the simulation.
% Make a uniform grid for both axes, interpolate all data onto that.
y_ax = (-200:0.25:200)*1e-6;
z_ax = y_ax;
hres_y_ax = (-70:0.20:70)*1e-6;
hres_rad = hres_y_ax(round(0.5*length(hres_y_ax)):end);
[HY, HZ] = meshgrid(hres_y_ax, hres_y_ax);
dy = mean(diff(y_ax)); dz = mean(diff(z_ax));

Ez_int_y = cell(3,1); Ez_int_y_fwhm = cell(3,1);
Ez_int_z = cell(3,1); Ez_int_z_fwhm = cell(3,1);
Ez_rint = cell(3,1);

% How far out in radial integral?
rlim = 300;

for i=1:3
    ndumps = length(Ez_x{i}.slicedata);
    Ez_int_y{i} = zeros(ndumps, length(y_ax));
    Ez_int_z{i} = zeros(ndumps, length(z_ax));
    Ez_int_y_fwhm{i} = zeros(ndumps,2);
    Ez_int_z_fwhm{i} = zeros(ndumps,2);
    Ez_rint{i} = zeros(ndumps,rlim);
    for k=1:ndumps
        dump_I = Ez_x{i}.slicedata{k};
        [ny, nz] = size(dump_I);
        % Make a uniform grid for both axes, interpolate all data onto that.
        Ez_int_y{i}(k,:) = interp1(Ez_x{i}.grid_y{k}(1:ny), dump_I(:, round(0.5*nz)), y_ax, 'linear', 0);
        Ez_int_z{i}(k,:) = interp1(Ez_x{i}.grid_z{k}(1:nz), dump_I(round(0.5*ny),:), z_ax, 'linear', 0);
        Ez_int_y_fwhm{i}(k,2) = widthfwhm(Ez_int_y{i}(k,:))*dy;
        Ez_int_z_fwhm{i}(k,2) = widthfwhm(Ez_int_z{i}(k,:))*dz;
        % And make a high resolution interpolation of the image to
        % calculate radial energy enclosed integral
        imm = interp2(Ez_x{i}.grid_y{k}(1:ny), Ez_x{i}.grid_z{k}(1:nz), dump_I', hres_y_ax, hres_y_ax', 'linear', 0);
        for r = 1:rlim
            vv = sum(sum(imm.*  ((HY.^2+HZ.^2)<hres_rad(r)^2)  ));
            Ez_rint{i}(k, r) = vv;
        end
    fprintf('Finished sim %i, dump %i\n', i, k);    
    end
    Ez_int_y_fwhm{i}(:,1) = Ez_x{i}.grid_time'*c*1e3;
    Ez_int_z_fwhm{i}(:,1) = Ez_x{i}.grid_time'*c*1e3;
    
end

%%
Ez_rint_norm = cell(size(Ez_rint));
for i=1:3
    ndumps = size(Ez_rint{i},1);
    Ez_rint_norm{i} = Ez_rint{i};
    for k=1:ndumps
        Ez_rint_norm{i}(k,:) = Ez_rint_norm{i}(k,:)./max(Ez_rint_norm{i}(k,:));
    end
end
save(fullfile(datsavfol, 'LongIntegralData'),...
    'Ez_int_y', 'Ez_int_y_fwhm', 'y_ax',...
    'Ez_int_z', 'Ez_int_z_fwhm',...
    'Ez_rint', 'Ez_rint_norm', 'hres_rad');
    