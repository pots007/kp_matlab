% analyseF20_F40_bubblesizes.m
%
% This script will load the analysed bubble data and try to get a value for
% the bubble size in both transverse planes as well as a longitudinal one. 
%
% Using the data saved by compareF20_F40_sims_bubblesizes.m

datsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
datsavname = fullfile(datsavfol, 'BubbleSizeData');
figsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
load(datsavname);

saveplots = 0;

% This plots the bubble evolution, along with the newly calculated lines.
%
% To ensure the taks stays doable, we focus on the first 5mm and ensure the
% code gives good results there.

hfig = figure(730); clf(hfig); hfig.Color = 'w';

hfig.Position = [100 100 900 900];

locs = GetFigureArray(2, 4, [0.06 0.1 0.1 0.1], 0.04, 'across');
ax = [];
bubble_sizes_y = cell(4,1);
bubble_sizes_z = cell(4,1);
bubble_sizes_xi = cell(4,1);
bubble_centres_xi = cell(4,1);

y_sub_logm = y_ax>-30e-6 & y_ax<-7.5e-6;
y_sub_ax = y_ax(y_sub_logm);

% Do some masking of the dephased beam to remove issues with the code
% finding that as the edge of the bubble... DIRTY!!!!
bub_t_y = bub_transverse_y;
bub_t_y{3}(55:end, 220:340) = 0;

for i=1:4
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,i*2-1), 'NextPlot', 'add');
    imagesc(prop_axis{i}*1e3*c, y_ax*1e6, bub_transverse_y{i}', 'Parent', ax(end));
    ylabel('y / micron');
    % And now the finding of the edge....
    % Pretty shit
    [~, t_inds] = min(abs(bub_transverse_y{i}(:,y_sub_logm)' - 1.75e24));
    % Somewhat better
    %[~, t_inds] = min(gradient(bub_transverse_y{i}'));
    % Somewhat better yet...
    for k=1:size(bub_transverse_y{i},1)
        tt = find(bub_t_y{i}(k,y_sub_logm)'>2e24, 1, 'last');
        if isempty(tt); t_inds(k) = 1; else t_inds(k) = tt; end
    end
    bubble_sizes_y{i} = abs(y_sub_ax(t_inds));
    plot(prop_axis{i}*1e3*c,  smooth(bubble_sizes_y{i})*1e6, '-k');
    plot(prop_axis{i}*1e3*c, -smooth(bubble_sizes_y{i})*1e6, '-k');
    
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,i*2), 'NextPlot', 'add');
    imagesc(prop_axis{i}*1e3*c, y_ax*1e6, bub_transverse_z{i}', 'Parent', ax(end));
    ylabel('z / micron');
    %[~, t_inds] = min(gradient(bub_transverse_z{i}'));
    for k=1:size(bub_transverse_z{i},1)
        tt = find(bub_transverse_z{i}(k,y_sub_logm)'>1.5e24, 1, 'last');
        if isempty(tt); t_inds(k) = 1; else t_inds(k) = tt; end
    end
    bubble_sizes_z{i} = abs(y_sub_ax(t_inds));
    plot(prop_axis{i}*1e3*c,  smooth(bubble_sizes_z{i})*1e6, '-k');
    plot(prop_axis{i}*1e3*c, -smooth(bubble_sizes_z{i})*1e6, '-k');
end

set(ax, 'YDir', 'normal', 'XLim', [0 12], 'CLim', [0 1.5e25], 'YLim', [-25 25],...
    'LineWidth', 1, 'Layer', 'top', 'XTick', 0:12, 'Box', 'on');
set(ax(1:6), 'XTickLabel', []);
set(ax(2:2:8), 'YAxisLocation', 'right');
xlabel(ax(7), 'Propagation / mm'); xlabel(ax(8), 'Propagation / mm');
title(ax(1), 'F/40, circ'); title(ax(2), 'F/40, circ');
title(ax(3), 'F/40, ell'); title(ax(4), 'F/40, ell');
title(ax(5), 'F/20, ell'); title(ax(6), 'F/20, ell');
title(ax(7), 'F/10, ell'); title(ax(8), 'F/10, ell');

setAllFonts(hfig, 16); make_latex(hfig);
colormap(brewermap(256, 'Blues'))

for i=1:8
    grid(ax(i), 'on'); drawnow; makeNiceGridHG2(ax(i));
end

% And save the extra data into the same file
save(datsavname, 'bubble_sizes_y', 'bubble_sizes_z', '-append');

if saveplots
    export_fig(fullfile(figsavfol, 'BubbleSizeEvolution_Sizes'), '-pdf', '-nocrop', hfig);
end

%% And now measure the bubble half-lengths as well

hfig = figure(731); clf(hfig); hfig.Color = 'w';
hfig.Position = [100 200 1000 400] + ismac*[1100 0 0 0];
locs = GetFigureArray(4, 1, [0.08 0.13 0.13 0.07], 0.02, 'across');
ax = [];
for i=1:4
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add', 'Box', 'on');
    %imagesc(prop_axis{i}*1e3*c, fliplr(Ex_y{i}.grid_x{1}*1e6), Ex_axis{i}', 'Parent', ax(end));
    imagesc(prop_axis{i}*1e3*c, fliplr(simgrid{i}*1e6), Ex_axis{i}', 'Parent', ax(end));
    %plot(prop_axis{i}*1e3*c, (Ex_y{i}.grid_x{1}(bub_centres{i})*1e6), '-k');
    bubble_centres_xi{i} = simgrid{i}(bub_centres{i});
    plot(prop_axis{i}*1e3*c, (bubble_centres_xi{i}*1e6), '-k');
    % Try to find the bubble end location
    % Not very elegant and indeed error prone
    [~, t_inds] = min(Ex_axis{i},[], 2);
    for k=1:length(prop_axis{i})
        tt = find(Ex_axis{i}(k, 1:(bub_centres{i}(k)-200))>0, 1, 'last')
        if ~isempty(tt); t_inds(k) = tt; end;
    end
    bubble_sizes_xi{i} = simgrid{i}(t_inds);
    plot(prop_axis{i}*1e3*c, bubble_sizes_xi{i}*1e6, '-g');
    xlabel('Propagation / mm');
end

set(ax, 'YDir', 'normal', 'XLim', [0 12], 'CLim', [-6e11 6e11], 'YLim', [-10 60],...
    'LineWidth', 1, 'Layer', 'top');
set(ax(2:4), 'YTickLabel', []);
ylabel(ax(1), 'y / micron');
title(ax(1), 'F/40, circ'); title(ax(2), 'F/40, ell'); 
title(ax(3), 'F/20, ell'); title(ax(4), 'F/10, ell');
cb = colorbar(ax(end), 'Position', [0.88 locs(2,1) 0.02 locs(4,1)]);
ylabel(cb, '$E_x $ / V/m')

setAllFonts(hfig, 16); make_latex(hfig);
colormap(brewermap(256, 'RdBu'));

% And save the extra data into the same file
save(datsavname, 'bubble_sizes_xi', 'bubble_centres_xi', '-append');

if saveplots
    export_fig(fullfile(figsavfol, 'AccFieldEvolution_Sizes'), '-pdf', '-nocrop', hfig);
end

%% And calculate bubble size and expansion rates

