% compareF20_F40_sims_a0s.m
%
% Calculates and compares the peak vector intensity values for differen
% simulations

rootfol = 'D:\Gem2015_3D';
fols{1} = 'Gem3D_4'; % F/40, circ
fols{end+1} = 'Gem3D_5'; % F/40, ell
fols{end+1} = 'Gem3D_6'; % F/20
fols{end+1} = 'Gem3D_7'; % F/10

datsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
figsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');

saveplots = 0

%% Calculations - get a0 values, also spot size at the peak intensity value

av_cell = 1; % Averages over 3 cells 
a0_y = cell(4,1);
a0_z = cell(4,1);
a0_peak = cell(4,1);
a0_peak_simple = cell(4,1);
Fp_long_peak = cell(4,1);
prop_axis = cell(4,1);

for i=1:4
    % And load the data
    clear Ez_y Ez_z;
    Ez_y{i} = load(fullfile(rootfol, fols{i}, 'data', 'Ez_all_y.mat'));
    Ez_z{i} = load(fullfile(rootfol, fols{i}, 'data', 'Ez_all_z.mat'));
    
    %a0_y{i} = zeros(length(Ez_y{i}.grid_t), size(Ez_y{i}.slicedata{1},1));
    %a0_z{i} = zeros(length(Ez_z{i}.grid_t), size(Ez_z{i}.slicedata{1},1));
    a0_peak{i} = zeros(length(Ez_y{i}.grid_time), 1);
    a0_peak_simple{i} = zeros(size(a0_peak{i}));
    Fp_long_peak{i} = zeros(size(a0_peak{i}));
    dx = mean(diff(Ez_z{i}.grid_x{1}));
    
    for k = 1:length(a0_peak{i})
        %zE = sum(abs(Ez_z.slicedata{i}).^2,1);
        %FWHMs(i,1) = widthfwhm(zE)*mean(diff(Ez_z.grid_t{i}));
        %yE = sum(abs(Ez_y.slicedata{i}).^2,1);
        %FWHMs(i,2) = widthfwhm(yE)*mean(diff(Ez_y.grid_t{i}));
        zE = Ez_z{i}.slicedata{k};
        zI = 0.5*epsilon0*c*abs(hilbert(zE)).^2;
        % This is the cheeky, fixed wavelength version
        a0_peak_simple{i}(k) = max(0.856*0.8*sqrt(zI(:,round(0.5*size(zI,2)))*1e-22));
        % This part actually calculated the changing wavelength as well!
        % The averaging of wavelength is done over one wavelength.
        [a0line,~] = getSim_a0(zE(:,round(0.5*size(zI,2))), Ez_z{i}.grid_x{k}, 30);
        a0_peak{i}(k) = max(a0line(1800:end-150)); % Cheekily only looking at the laser part
        % And calculate the peak longitudinal ponderomotive force
        Fp = gradient(a0line, dx);
        Fp_long_peak{i}(k) = max(Fp(1800:end-150));
        % Apart from file 69 which is fucked a bit
        if i==2 && k==69; a0_peak{i}(k) = max(a0line(1800:end-410)); end;
        %propa(i) = max(Ez_z.grid_x{i})*1e3;
    end
    prop_axis{i} = Ez_y{i}.grid_time;
end

save(fullfile(datsavfol, 'LaserIntensityData'), 'prop_axis', 'a0_peak', 'a0_peak_simple');

%% This plots the a0 evolution for different simulations
hfig = figure(760); clf(hfig); hfig.Color = 'w';
if ismac
    hfig.Position = [200 200 600 400];
elseif ispc
    hfig.Position = [100 100 600 400];
end
locs = GetFigureArray(1, 1, [0.08 0.1 0.15 0.15], 0.02, 'across');
ax = []; ax(end+1) = axes('Parent', hfig, 'Position', locs, 'NextPlot', 'add'); 
clear hh;
for i=1:4
    hh(i) = plot(ax, prop_axis{i}*c*1e3, a0_peak{i}, '-', 'LineWidth', 1.5);
    plot(ax, prop_axis{i}*c*1e3, a0_peak_simple{i}, '--', 'LineWidth', 1.5, 'Color', hh(i).Color);
end

set(ax, 'YDir', 'normal', 'XLim', [0 12], 'YLim', [1 18],...
    'LineWidth', 2, 'Layer', 'top', 'Box', 'on');
xlabel('Propagation / mm');
ylabel(ax, '$a_0$');
legend(hh, {'f/40, circ', 'f/40, ell', 'f/20, ell', 'f/10, ell'}, 'Location', 'northwest');
setAllFonts(hfig, 16); make_latex(hfig);
grid on; makeNiceGridHG2(gca);
if saveplots
    export_fig(fullfile(figsavfol, 'Laser_a0_evolution'), '-pdf', '-nocrop', hfig);
end

%% This plots the a0 evolution for different simulations
hfig = figure(761); clf(hfig); hfig.Color = 'w';
hfig.Position = [100 100 600 400];

locs = GetFigureArray(1, 1, [0.08 0.1 0.15 0.15], 0.02, 'across');
ax = []; ax(end+1) = axes('Parent', hfig, 'Position', locs, 'NextPlot', 'add'); 
clear hh;
for i=1:4
    hh(i) = plot(ax, prop_axis{i}*c*1e3, a0_peak{i}, '-', 'LineWidth', 1.5);
    %plot(ax, prop_axis{i}*c*1e3, a0_peak_simple{i}, '--', 'LineWidth', 1.5, 'Color', hh(i).Color);
end

set(ax, 'YDir', 'normal', 'XLim', [0 12], 'YLim', [1 18],...
    'LineWidth', 2, 'Layer', 'top', 'Box', 'on');
xlabel('Propagation / mm');
ylabel(ax, '$a_0$');
legend(hh, {'f/40, circ', 'f/40, ell', 'f/20, ell', 'f/10, ell'}, 'Location', 'northwest');
setAllFonts(hfig, 16); make_latex(hfig);
grid on; makeNiceGridHG2(gca);
if saveplots
    export_fig(fullfile(figsavfol, 'Laser_a0_evolution2'), '-pdf', '-nocrop', hfig);
end