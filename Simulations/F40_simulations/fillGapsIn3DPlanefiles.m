diags = dir('temp/*.mat');
for k=1:length(diags)
    % Open first half
    temp1 = load(fullfile('temp', diags(k).name));
    % Open latter half
    load(fullfile('temp2', diags(k).name));
    % Rejig things
    for i=1:93
        grid_x{i} = temp1.grid_x{i};
        grid_t{i} = temp1.grid_t{i};
        grid_time(i) = temp1.grid_time(i);
        slicedata{i} = temp1.slicedata{i};
    end
    save(fullfile('data', diags(k).name), 'grid_x', 'grid_time', 'grid_t', 'slicedata');
end
