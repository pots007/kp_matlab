% analyseF20_F40_BubbleExpansionRates.m
%
% Look at the expansion rate and how it links to injected charge. Also
% plots the size of the half cavity.

datsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
datsavname = fullfile(datsavfol, 'BubbleSizeData');
figsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
load(datsavname);
% And load e-beam energy evolution data as well.
load(fullfile(datsavfol, 'ElectronEnergyEvolution'));

rootfol = 'D:\Gem2015_3D';
clear fols;
fols{1} = 'Gem3D_4'; % F/40
fols{end+1} = 'Gem3D_5'; % F/40
fols{end+1} = 'Gem3D_6'; % F/20
fols{end+1} = 'Gem3D_7'; % F/10

%% Firstly, let's plot the bubble sizes: transverse and longitudinal, for all sims

hfig = figure(741); clf(hfig);
hfig.Position = [100 100 1000 500];

locs = GetFigureArray(4, 1, [0.08 0.04 0.12 0.08], 0.03, 'across');

min_E = 50;
clear ax hh;
for i=1:4
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'Nextplot', 'add', 'box', 'on');
    hh(i,1) = plot(ax(i), prop_axis{i}*c*1e3, (bubble_centres_xi{i}-bubble_sizes_xi{i})*1e6, '-k');
    hh(i,2) = plot(ax(i), prop_axis{i}*c*1e3, bubble_sizes_y{i}*1e6, '-r');
    hh(i,3) = plot(ax(i), prop_axis{i}*c*1e3, bubble_sizes_z{i}*1e6, '-m');
    Q_inj = sum(px_evol{i}(:, en_axis>min_E), 2);
    xlabel(ax(i), 'Propagation / mm')
end

set(ax, 'XLim', [0 6]);
ylabel(ax(1), 'y / micron');
title(ax(1), 'F/40, circ'); title(ax(2), 'F/40, ell'); 
title(ax(3), 'F/20, ell'); title(ax(4), 'F/10, ell');

%% Now in a slightly more useful way as well: comparing different runs!
hfig = figure(743); clf(hfig);
hfig.Position = [100 100 1000 500];

locs = GetFigureArray(3, 1, [0.08 0.04 0.12 0.08], 0.03, 'across');
cols = brewermap(8, 'Set1');
titls = {'Half-length', 'Half-y', 'Half-z'};

clear ax hh;
for i=1:3; 
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'Nextplot', 'add', 'box', 'on'); 
    title(ax(i), titls{i});
    xlabel(ax(i), 'Propagation / mm')
end

for i=1:4    
    hh(i,1) = plot(ax(1), prop_axis{i}*c*1e3, smooth(bubble_centres_xi{i}-bubble_sizes_xi{i})*1e6, '-', 'Color', cols(i,:));
    hh(i,2) = plot(ax(2), prop_axis{i}*c*1e3, smooth(bubble_sizes_y{i}*1e6), '-', 'Color', cols(i,:));
    hh(i,3) = plot(ax(3), prop_axis{i}*c*1e3, smooth(bubble_sizes_z{i}*1e6), '-', 'Color', cols(i,:));
end

for i=1:3
    legend(ax(i), hh(:,i), {'f/40, circ', 'f/40, ell', 'f/20, ell', 'f/10, ell'},...
        'Location', 'north', 'box', 'off');
    grid(ax(i), 'on');
end
set(hh, 'LineWidth', 1);
set(ax, 'XLim', [0 6], 'YLim', [10 25], 'LineWidth', 1, 'XTick', 0:6,...
    'YTick', 10:2.5:25);
set(ax(2:end), 'YTickLabel', []);
ylabel(ax(1), 'y / micron')
make_latex(hfig); setAllFonts(hfig, 16);

if saveplots
    export_fig(fullfile(figsavfol, 'BubbleSizeEvolution_comparison'), '-pdf', '-nocrop', hfig);
end

%% And the buble expansion with injected charge

hfig = figure(742); clf(hfig);
hfig.Position = [100 100 1000 300];

locs = GetFigureArray(4, 1, [0.12 0.02 0.19 0.2], 0.03, 'across');

min_E = 50;
clear ax hh;
for i=1:4
    ax(i) = axes('Parent', hfig, 'Position', locs(:,i), 'Nextplot', 'add', 'box', 'on');
    hh(i,1) = plot(ax(i), prop_axis{i}*c*1e3, gradient((bubble_centres_xi{i}-bubble_sizes_xi{i})*1e6), '-k');
    Q_inj = sum(px_evol{i}(:, en_axis>min_E), 2);
    load(fullfile(rootfol, fols{i}, 'Average_highE_Ex'));
    hh(i,2) = plot(ax(i), prop_axis{i}*c*1e3, Q_inj*1e-10, '-m');
    hh(i,3) = plot(ax(i), taxis*c*1e3, Ex_part_av*1e-12, '-r');
    hh(i,4) = plot(ax(i), taxis*c*1e3, Ex_min{i}*1e-12, '-g');
    xlabel(ax(i), 'Propagation / mm')
    grid(ax(i), 'on');
end

set(ax, 'XLim', [0 6], 'YLim', [-1.1 1.1], 'XTick', 0:6, 'LineWidth', 1);
set(ax(2:end), 'YTickLabel', []);
set(hh, 'LineWidth', 1);

clear t;
t(1) = text(-2, 0, '$E_x$ [$10^{12}\,\mathrm{V/m}$]', 'Parent', ax(1), 'Rotation', 90, 'Color', 'r');
t(end+1) = text(-3, 0, '$E_\mathrm{min}$ [$10^{12}\,\mathrm{V/m}$]', 'Parent', ax(1), 'Rotation', 90, 'Color', 'g');
t(end+1) = text(-4, 0, '$Q_{\mathcal{E}>50\,\mathrm{MeV}}$ [a.u.]', 'Parent', ax(1), 'Rotation', 90, 'Color', 'm');
t(end+1) = text(-5, 0, '$\mathrm{d}r/\mathrm{d}\xi$ [a.u.]', 'Parent', ax(1), 'Rotation', 90, 'Color', 'k');
set(t, 'VerticalAlignment', 'middle', 'HorizontalAlignment', 'center');
title(ax(1), 'F/40, circ'); title(ax(2), 'F/40, ell'); 
title(ax(3), 'F/20, ell'); title(ax(4), 'F/10, ell');
make_latex(hfig); setAllFonts(hfig, 15);

if saveplots
    export_fig(fullfile(figsavfol, 'AccFieldMax_epsilon_Q'), '-pdf', '-nocrop', hfig);
end