% matchRestartParticleIDs_F40.m
%
% This script will look at the discontuities of particle ID numbering
% between restart dumps. To be used for Gem3D_5 run. 
%
% We literally take the easiest approach, and look at which particle in the
% set of energies is the closest to where we would assume the particle
% would go in the time between dumps. We then make a hashmap of this, with
% the particle ID before the restart being the key to the new partcile ID
% after the restart.
 
%% This part does the latest restart
dataf = '/Volumes/SimKris/Gem3D_5/data/';
predataf = GetDataSDF(fullfile(dataf, 'particles0097.sdf'));
postdataf = GetDataSDF(fullfile(dataf, 'particles0098.sdf'));
logm1 = predataf.Particles.Gamma.subset_highgamma.electron.data>3100;
logm2 = postdataf.Particles.Gamma.subset_highgamma.electron.data>3100;
rr1 = [predataf.Grid.Particles.subset_highgamma.electron.x(logm1),...
    predataf.Grid.Particles.subset_highgamma.electron.y(logm1),...
    predataf.Grid.Particles.subset_highgamma.electron.z(logm1)];
gammas1 = predataf.Particles.Gamma.subset_highgamma.electron.data(logm1);
IDs1 = predataf.Particles.ID.subset_highgamma.electron.data(logm1);
v1 = c*(1-1/gammas1.^2);
rr2 = [postdataf.Grid.Particles.subset_highgamma.electron.x(logm2),...
    postdataf.Grid.Particles.subset_highgamma.electron.y(logm2),...
    postdataf.Grid.Particles.subset_highgamma.electron.z(logm2)];
gammas2 = postdataf.Particles.Gamma.subset_highgamma.electron.data(logm2);
IDs2 = postdataf.Particles.ID.subset_highgamma.electron.data(logm2);
v2 = c*(1-1/gammas2.^2)';
 
dt = predataf.time-postdataf.time;
% Find the distances from particle X
 
ii = 3171;
ii =randi(size(rr1,1),1);
rrii = rr1(ii,:); rrii = repmat(rrii, size(rr2,1), 1);
 
dists = abs(sqrt(sum((rr2 - rrii).^2,2) - (v2*dt).^2));
% Try to be clever
plot(dists, 'x'); set(gca, 'YLim', [0 3e-7])
%
IDmap = containers.Map('Keytype', 'int64', 'Valuetype', 'int64');
mindists = [];
for ii=1:size(rr1,1)
    %ii = 1731;
    rrii = rr1(ii,:); rrii = repmat(rrii, size(rr2,1), 1);
    
    dists = abs(sqrt(sum((rr2 - rrii).^2,2) - (v2*dt).^2));
    [mindists(ii), minind] = min(dists);
    IDmap(IDs1(ii)) = IDs2(minind);
    % Try to be clever
    %plot(dists, 'x'); set(gca, 'YLim', [-15e-14 15e-14])
end

%% This part does the middle restart
dataf = '/Volumes/SimKris/Gem3D_5/data/';
% Other way around as the correct key is in file 0054!
predataf = GetDataSDF(fullfile(dataf, 'particles0054.sdf'));
postdataf = GetDataSDF(fullfile(dataf, 'particles0053.sdf'));
logm1 = predataf.Particles.Gamma.subset_highgamma.electron.data>1800;
logm2 = postdataf.Particles.Gamma.subset_highgamma.electron.data>1800;
rr1 = [predataf.Grid.Particles.subset_highgamma.electron.x(logm1),...
    predataf.Grid.Particles.subset_highgamma.electron.y(logm1),...
    predataf.Grid.Particles.subset_highgamma.electron.z(logm1)];
gammas1 = predataf.Particles.Gamma.subset_highgamma.electron.data(logm1);
IDs1 = predataf.Particles.ID.subset_highgamma.electron.data(logm1);
v1 = c*(1-1/gammas1.^2);
rr2 = [postdataf.Grid.Particles.subset_highgamma.electron.x(logm2),...
    postdataf.Grid.Particles.subset_highgamma.electron.y(logm2),...
    postdataf.Grid.Particles.subset_highgamma.electron.z(logm2)];
gammas2 = postdataf.Particles.Gamma.subset_highgamma.electron.data(logm2);
IDs2 = postdataf.Particles.ID.subset_highgamma.electron.data(logm2);
v2 = c*(1-1/gammas2.^2)';
 
dt = predataf.time-postdataf.time;
% Find the distances from particle X
 
ii = 3171;
ii =randi(size(rr1,1),1);
rrii = rr1(ii,:); rrii = repmat(rrii, size(rr2,1), 1);
 
dists = abs(sqrt(sum((rr2 - rrii).^2,2) - (v2*dt).^2));
% Try to be clever
plot(dists, 'x'); set(gca, 'YLim', [0 3e-7])
%
IDmap = containers.Map('Keytype', 'int64', 'Valuetype', 'int64');
mindists = [];
for ii=1:size(rr1,1)
    %ii = 1731;
    rrii = rr1(ii,:); rrii = repmat(rrii, size(rr2,1), 1);
    
    dists = abs(sqrt(sum((rr2 - rrii).^2,2) - (v2*dt).^2));
    [mindists(ii), minind] = min(dists);
    IDmap(IDs1(ii)) = IDs2(minind);
    % Try to be clever
    %plot(dists, 'x'); set(gca, 'YLim', [-15e-14 15e-14])
end

%% And this does the earliest restart
% This is the trickiest one, I think there will be quite a few particles
% with gammas in the correct range. So I'll start by looking at the gammas
% of the interesting particles, and find a suitably matching range.

predataf = GetDataSDF(fullfile(dataf, 'particles0040.sdf'));
postdataf = GetDataSDF(fullfile(dataf, 'particles0039.sdf'));
logm1 = predataf.Particles.Gamma.subset_highgamma.electron.data>1000;
logm2 = postdataf.Particles.Gamma.subset_highgamma.electron.data>1000;
rr1 = [predataf.Grid.Particles.subset_highgamma.electron.x(logm1),...
    predataf.Grid.Particles.subset_highgamma.electron.y(logm1),...
    predataf.Grid.Particles.subset_highgamma.electron.z(logm1)];
gammas1 = predataf.Particles.Gamma.subset_highgamma.electron.data(logm1);
IDs1 = predataf.Particles.ID.subset_highgamma.electron.data(logm1);
v1 = c*(1-1/gammas1.^2);
rr2 = [postdataf.Grid.Particles.subset_highgamma.electron.x(logm2),...
    postdataf.Grid.Particles.subset_highgamma.electron.y(logm2),...
    postdataf.Grid.Particles.subset_highgamma.electron.z(logm2)];
gammas2 = postdataf.Particles.Gamma.subset_highgamma.electron.data(logm2);
IDs2 = postdataf.Particles.ID.subset_highgamma.electron.data(logm2);
v2 = c*(1-1/gammas2.^2)';
 
dt = predataf.time-postdataf.time;
% Find the distances from particle X
 %%
rs2 = load('IDchange_0053_0054');
% Make a reverse mapping of the 53->54 transition: the key is now the early
% set ID, mapping onto later restart one.
revIDmap = containers.Map('KeyType', 'int64', 'ValueType', 'int64');
rs2keys = rs2.IDmap.keys; rs2vals = rs2.IDmap.values;
for k=1:rs2.IDmap.length
    revIDmap(rs2vals{k}) = rs2keys{k};
end

ii = 3171;
ii =randi(size(rr1,1),1);
rrii = rr1(ii,:); rrii = repmat(rrii, size(rr2,1), 1);
 
dists = abs(sqrt(sum((rr2 - rrii).^2,2) - (v2*dt).^2));
% Try to be clever
plot(dists, 'x'); set(gca, 'YLim', [0 3e-7])
%
IDmap = containers.Map('Keytype', 'int64', 'Valuetype', 'int64');
mindists = [];
for ii=1:size(rr1,1)
    %ii = 1731;
    rrii = rr1(ii,:); rrii = repmat(rrii, size(rr2,1), 1);
    
    dists = abs(sqrt(sum((rr2 - rrii).^2,2) - (v2*dt).^2));
    [mindists(ii), minind] = min(dists);
    % Find the late time ID of the particle
    if revIDmap.isKey(IDs1(ii))
        ID_ = revIDmap(IDs1(ii));
        IDmap(ID_) = IDs2(minind);
    end
    % Try to be clever
    %plot(dists, 'x'); set(gca, 'YLim', [-15e-14 15e-14])
end
