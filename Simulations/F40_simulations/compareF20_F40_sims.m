% This is to compare the F40, F20 and F10 propagations

figsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
saveplots = 1;

% Run the data gathering file first
% getDataForComparison;

% Or if it has been run, just load the data:
load(fullfile(getGDrive, 'epoch', 'Gem2015_3D', 'LongIntegralData.mat'));

%% ---  Plot the evolution of spot size in both planes

hfig = figure(701); clf(hfig); hfig.Color = 'w';
locs = GetFigureArray(3, 2, [0.09 0.04 0.1 0.07], 0.02, 'down');
ax =[];
intfac = 0.5*epsilon0*c*1e-4;
for i=1:3
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,2*i-1), 'NextPlot', 'add');
    imagesc(Ez_int_y_fwhm{i}(:,1), y_ax*1e6, intfac*Ez_int_y{i}', 'Parent', ax(end));
    plot(ax(end), Ez_int_y_fwhm{i}(:,1), 0.5*1e6*Ez_int_y_fwhm{i}(:,2), '--k', 'LineWidth', 1.5);
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,2*i), 'NextPlot', 'add');
    imagesc(Ez_int_z_fwhm{i}(:,1), z_ax*1e6, intfac*Ez_int_z{i}', 'Parent', ax(end));
    plot(ax(end), Ez_int_z_fwhm{i}(:,1), 0.5*1e6*Ez_int_z_fwhm{i}(:,2), '--k', 'LineWidth', 1.5);
end
% Make it all nice
set(ax, 'XLim', [0 12], 'YLim', [-60 60], 'CLim', [0 3e22], 'LineWidth', 2, 'Layer', 'top', 'Box', 'on');
set(ax(1:2:6), 'XTickLabel', []); set(ax(3:end), 'YTickLabel', []);
title(ax(1), 'F 40'); title(ax(3), 'F 20'); title( ax(5), 'F 10');
ylabel(ax(1), 'y / micron'); ylabel(ax(2), 'z / micron');
xlabel(ax(2), 'x / mm'); xlabel(ax(4), 'x / mm'); xlabel(ax(6), 'x / mm');

setAllFonts(hfig, 16); make_latex(hfig);
if saveplots
    export_fig(fullfile(figsavfol, 'SpotSizeEvolutions'), '-pdf', '-nocrop', hfig); 
end

%% ----------     Plot radially enclosed energy variation

hfig = figure(702); clf(hfig); hfig.Color = 'w';
locs = GetFigureArray(1, 3, [0.09 0.14 0.1 0.07], 0.05, 'down');
ax =[];

for i=1:3
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add');
    imagesc(Ez_int_y_fwhm{i}(:,1), hres_rad*1e6, Ez_rint_norm{i}', 'Parent', ax(end));
    plot(ax(end), Ez_int_y_fwhm{i}(:,1), 0.5*1e6*Ez_int_y_fwhm{i}(:,2), '--k', 'LineWidth', 1.5);
    plot(ax(end), Ez_int_z_fwhm{i}(:,1), 0.5*1e6*Ez_int_z_fwhm{i}(:,2), '--k', 'LineWidth', 1.5);
end
% Make it all nice
set(ax, 'XLim', [0 12], 'YLim', [0 60], 'CLim', [0 1], 'LineWidth', 2, 'Layer', 'top', 'Box', 'on');
set(ax(1:2), 'XTickLabel', []); %set(ax(3:end), 'YTickLabel', []);
title(ax(1), 'F 40'); title(ax(2), 'F 20'); title( ax(3), 'F 10');
ylabel(ax(1), 'y / micron'); ylabel(ax(2), 'y / micron'); ylabel(ax(3), 'y / micron');
xlabel(ax(3), 'x / mm');% xlabel(ax(2), 'x / mm'); xlabel(ax(3), 'x / mm');

cbar = colorbar(ax(3));
set(cbar, 'LineWidth', 2, 'Position', [0.87 0.1 0.02 0.81])
ylabel(cbar, 'Energy enclosed within radius');

setAllFonts(hfig, 16); make_latex(hfig);
if saveplots
    export_fig(fullfile(figsavfol, 'EnclosedEnergyEvolutions'), '-pdf', '-nocrop', hfig); 
end

%% --------- And now energy enclosed within FWHM radius as a function of prop

hfig = figure(703); clf(hfig); hfig.Color = 'w';
set(gca, 'NextPlot', 'add'); hh = [];
% Find energy enclosed within radius for each sim
enclosed_E = cell(3,1);
for i=1:3
    enclosed_E{i} = zeros(size(Ez_int_y_fwhm{i}));
    enclosed_E{i}(:,1) = Ez_int_y_fwhm{i}(:,1);
    rlim = size(Ez_rint_norm{i},2);
    for n = 1:size(enclosed_E{i},1)
        r_val = 0.5*sqrt(Ez_int_y_fwhm{i}(n,2)*Ez_int_z_fwhm{i}(n,2));
        enclosed_E{i}(n,2) = interp1(hres_rad(1:rlim), Ez_rint_norm{i}(n,:), r_val);
    end
    %hh(i) = plot(enclosed_E{i}(:,1), enclosed_E{i}(:,2), '-');
    hh(i) = plot(Ez_int_y_fwhm{i}(:,2), enclosed_E{i}(:,2), '-');
end
legend(hh, {'F/40', 'F/20', 'F/10'}, 'Location', 'Southwest')
set(gca, 'Box', 'on', 'LineWidth', 2);

