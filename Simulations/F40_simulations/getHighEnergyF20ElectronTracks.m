% Plot histogram of a particular dump energies

ndump = 51;
mm = getSDFParticleData(sprintf('data/particles%04i.sdf', ndump), 'Gamma', 'electron');
%%
gammas = cell2mat(mm.map.values);%*0.511;
edges = 10:1:3000;
[spec, specaxis] = histcounts(gammas, edges);
hfig = figure(190); clf(hfig);
plot(specaxis(1:end-1), log10(spec));
% Now look at the top end:
logm = gammas>2100;
[specH, specaxisH] = histcounts(gammas(logm), edges);
plot(specaxisH(1:end-1), log10(specH));
IDs = mm.map.keys;
IDs_H = cell2mat(IDs(logm));

%% And now try to find all the data for these guys!
% Set up hashmaps for the collected data

rs1 = load('IDchange_0036_0037');

trackMap = containers.Map('KeyType', 'int64', 'ValueType', 'any');
track = struct;
track.x = [];
track.y = [];
track.z = [];
track.Px = [];
track.Py = [];
track.Pz = [];
track.Gamma = [];
track.times = [];

for k=1:length(IDs_H)
    trackMap(IDs_H(k)) = track;
end

flist = dir('data/particles*.sdf');

for i=5:length(flist)
    fprintf('Loading file %s\n', flist(i).name);
    mm = getSDFParticleDataAll(['data/' flist(i).name], 'electron');
    if isempty(mm.x); continue; end;
    fprintf('    Checking file %s\n', flist(i).name);
    for p=1:1:length(IDs_H)
        if i<37 % Keys have changed
            if rs1.IDmap.isKey(IDs_H(p))
                IDp = rs1.IDmap(IDs_H(p));
            else
                continue;
            end
        else
            IDp = IDs_H(p);
        end
        % If this particle already exists in the dump, collect it
        if mm.x.isKey(IDp)
            tt = trackMap(IDs_H(p)); % Use the initial key in the combined map
            tt.x(end+1) = mm.x(IDp); % Use the new, matched key in the dump.
            tt.y(end+1) = mm.y(IDp);
            tt.z(end+1) = mm.z(IDp);
            tt.Px(end+1) = mm.Px(IDp);
            tt.Py(end+1) = mm.Py(IDp);
            tt.Pz(end+1) = mm.Pz(IDp);
            tt.Gamma(end+1) = mm.Gamma(IDp);
            tt.times(end+1) = mm.time;
            trackMap(IDs_H(p)) = tt;
        end
    end
end

%%
kk = trackMap.values;
IDs_H = cell2mat(trackMap.keys);
kklog = cellfun(@(x) isempty(x.x), kk);
hfig = figure(191); clf(hfig);
hfig.Position = [100 100 600 1000];
nplots = 4;
locs = GetFigureArray(1, nplots, [0.04 0.05 0.1 0.2], 0.03, 'down');
axs = []; ylabs = {'$\gamma$', 'y / micron', 'dy/dx', 'd$\gamma$/dx'}; 

for k=1:nplots; 
    axs(k) = axes('Parent', hfig, 'Position', locs(:,k), 'NextPlot', 'add'); 
    ylabel(axs(k), ylabs{k});
end
set(gca, 'Nextplot', 'add')

rs1time = getEPOCHtime('data/normal0036.sdf');
skipno = 0; allno = 0;

for i=1:1:trackMap.length
    allno = allno +1;
    dgamma_dx = gradient(trackMap(IDs_H(i)).Gamma)./gradient(trackMap(IDs_H(i)).x);
    rs1ind = find(trackMap(IDs_H(i)).times==rs1time);
    skipflag = 0;
    if ~isempty(rs1ind)
        if dgamma_dx(rs1ind)>5.3e5 || dgamma_dx(rs1ind)<3e5 % || abs(dy_dx(rs1ind))>0.02
            skipflag = 1;
        end
    end
    skipno = skipno + skipflag;
    tt = trackMap(IDs_H(i));
    tt.skipflag = skipflag;
    trackMap(IDs_H(i)) = tt;
    if skipflag; continue; end
    continue;
    plot(axs(1), trackMap(IDs_H(i)).x, trackMap(IDs_H(i)).Gamma, '-x')
    plot(axs(2), trackMap(IDs_H(i)).x, trackMap(IDs_H(i)).y*1e6, '-x')
    plot(axs(3), trackMap(IDs_H(i)).x, gradient(trackMap(IDs_H(i)).y)./gradient(trackMap(IDs_H(i)).x), '-x')
    plot(axs(4), trackMap(IDs_H(i)).x, gradient(trackMap(IDs_H(i)).Gamma)./gradient(trackMap(IDs_H(i)).x), '-x')
end
fprintf('Filtered out %i particles out of %i\n', skipno, allno);

set(axs(1:end-1), 'XTickLabel', []);
xlabel('Propagation / m');
set(axs, 'Linewidth', 1, 'Box', 'on');
set(hfig, 'Color', 'w'); 
setAllFonts(hfig, 18); make_latex(hfig);

%% And now do some proper filtering...



%% Look at the field the electrons feel ac they accelerate
Ex_z = load('data/Ex_all_z.mat');

%% Find the axial electric fields, averaged over ncells
tm = load('trackMap_IDfixing_36_37_filtered');
nav = 0;

hfig = figure(192); clf(hfig);
set(gca, 'NextPlot', 'add', 'box', 'on', 'YLim', [-1e12 1e11]);
IDs_H = cell2mat(tm.trackMap.keys);
taxis = Ex_z.grid_time;
Ex_fields = nan(length(taxis), tm.trackMap.length);
for p=100:100:tm.trackMap.length
    if mod(p,100)==0; fprintf('Particle %i out of %i\n', p, tm.trackMap.length); end;
    tt = tm.trackMap(IDs_H(p));
    if tt.skipflag; continue; end;
    Ex_ = [];
    for k=1:length(Ex_z.grid_time)
        Ex_loc = Ex_z.slicedata{k}';
        [ny,~] = size(Ex_loc);
        yinds = (round(0.5*ny)-nav):1:(round(0.5*ny)+nav);
        
        tind = find(tt.times==Ex_z.grid_time(k));
        if isempty(tind); continue; end
        xloc = tt.x(tind);
        [~,x_ind] = min(abs(Ex_z.grid_x{k} - xloc));
        %Ex_times(k,1) = Ex_z.grid_time(k);
        %Ex_times(k,2) = mean(Ex_loc(yinds, x_ind));
        Ex_(end+1) = mean(Ex_loc(yinds, x_ind));
        Ex_(tind) = mean(Ex_loc(yinds, x_ind));
        %tt.Ex =
    end
    tt.Ex = Ex_;
    tm.trackMap(IDs_H(p)) = tt;
    %plot(tt.times*c, tt.Ex, '-x'); drawnow;
    Ex_fields(:,p) = interp1(tt.times, Ex_, taxis, 'linear', nan);
end
%%
Ex_fields2 = [];
for k=1:size(Ex_fields,2)
    if sum(isnan(Ex_fields(:,k)))<20
        Ex_fields2(:,end+1) = Ex_fields(:,k);
    end
end
Ex_part_av = mean(Ex_fields2, 2);
save('Average_highE_Ex', 'Ex_part_av', 'taxis');