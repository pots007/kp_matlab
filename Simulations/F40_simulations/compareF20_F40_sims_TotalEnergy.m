% compareF20_F40_sims_TotalEnergy.m
%
% Compare the total amount of energy in Ez field for a few different
% simulations

rootfol = 'D:\Gem2015_3D';

clear fols EzInt;
fols{1} = 'Gem3D_4'; % F/40, circular
fols{end+1} = 'Gem3D_5'; % F/40, ell
fols{end+1} = 'Gem3D_6'; % F/20, ell
fols{end+1} = 'Gem3D_7'; % F/10, ell
fols{end+1} = 'Gem3D_8'; % F/40, realistic
%fols{end+1} = 'Gem3D_9'; % F/20, realistic
nfols = length(fols);

for k=1:nfols
    fprintf('Reading data from folder %s\n', fols{k});
    EzInt{k} = load(fullfile(rootfol, fols{k}, 'data', 'Ez_LongIntegral.mat'));
end

datsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
figsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
saveplots = 0;

%% Get the data, and boom
% The slight complication is that the F/40 simulation get cut at some
% point, to preserve dataspace. Thus we first iterate over the grids, get
% the smallest common size, and the integrate.

totE_all = cell(nfols,1);
totE_comm = cell(size(totE_all));
prop_axis = cell(size(totE_all));

for i=1:nfols
    ndumps = length(EzInt{i}.slicedata);
    totE_all{i} = zeros(ndumps, 1);
    totE_comm{i} = zeros(ndumps, 1);
    prop_axis{i} = EzInt{i}.grid_time;
    % Find the smallest grid extent.
    ylims = cell2mat(cellfun(@(x) [min(x) max(x)], EzInt{i}.grid_y, 'UniformOutput', 0)');
    zlims = cell2mat(cellfun(@(x) [min(x) max(x)], EzInt{i}.grid_z, 'UniformOutput', 0)');
    ymin = max(ylims(:,1)); ymax = min(ylims(:,2));
    zmin = max(zlims(:,1)); zmax = min(zlims(:,2));
    for k=1:ndumps
        fprintf('Opening file %3i, folder %i\n',  k, i);
        totE_all{i}(k) = sum(EzInt{i}.slicedata{k}(:));        
        comm_Ez = EzInt{i}.slicedata{k}(EzInt{i}.grid_y{k}>ymin & EzInt{i}.grid_y{k}<ymax,...
            EzInt{i}.grid_z{k}>zmin & EzInt{i}.grid_z{k} < zmax);
        totE_comm{i}(k) = sum(comm_Ez(:));
    end
end

%% And now plot the energy evolution

hfig = figure(737); clf(hfig); hfig.Color = 'w';
hfig.Position = [200 200 600 400];

set(gca, 'NextPlot', 'add', 'LineWidth', 2, 'Layer', 'top', 'Box', 'on');
% How do I calculate the energy cutoff?

hh = [];
for i=1:5
    hh(i) = plot(prop_axis{i}*c*1e3, totE_comm{i}, '-', 'LineWidth', 2);
end
legend(hh, {'F/40, circ', 'F/40, ell', 'F/20, ell', 'F/10, ell'}, 'Location', 'southwest');
set(gca, 'XLim', [0 12], 'XTick', 0:12);
xlabel(gca, 'Propagation / mm'); ylabel(gca, '$\int{|E_z|^2}$');
setAllFonts(hfig, 16); make_latex(hfig);
grid(gca, 'on'); makeNiceGridHG2(gca);

if saveplots
    %export_fig(fullfile(figsavfol, 'ElectronEnergyEvolution2'), '-pdf', '-nocrop', hfig);
end