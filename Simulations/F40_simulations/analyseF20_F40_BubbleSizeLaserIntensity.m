% analyseF20_F40_BubbleSizeLaserIntensity.m
%
% Script that will plot the laser spot size, a0 and the bubble transverse
% and longitudinal half size against each other in various ways.

% First load the data
datsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
load(fullfile(datsavfol, 'BubbleSizeData'));
load(fullfile(datsavfol, 'LaserIntensityData'));
load(fullfile(datsavfol, 'LongIntegralData'));

hfig = figure(761); clf(hfig); hfig.Position = [100 100 900 400];
locs = GetFigureArray(2, 1, [0.1 0.05 0.14 0.05], 0.05, 'across');
clear hh axx;
kp = omegapcalc(3e18)/c;

saveplots = 0;

for i=1:2
    axx(i) = axes('Parent', hfig, 'Position', locs(:,i), 'Box', 'on');
    hh = plot(axx(i), prop_axis{i}*1e3*c,...
        [[smooth(simgrid{i}(bub_centres{i})-bubble_sizes_xi{i})';...
        smooth(bubble_sizes_y{i})';...
        smooth(bubble_sizes_z{i})']*1e6; ...
        2*sqrt(a0_peak{i}')*1e6/kp])
    xlabel(axx(i), 'Propagation distance / mm');
    title(axx(i), ['f/' num2str(20*(3-i))]);
    set(hh, 'LineWidth', 1.5);
    grid on; grid minor;
end
set(axx, 'LineWidth', 0.5, 'XLim', [0 12]);
hleg = legend(axx(1), hh, {'Half-length / micron', 'Radius in y / micron', 'Radius in z / micron', '$2\sqrt{a_0}$'},...
    'Location', 'Northwest', 'Position', [0.055 0.68 0.222, 0.2055]);
make_latex(hfig); setAllFonts(hfig, 13);

if saveplots
    export_fig(fullfile(datsavfol, 'Evolutions'), '-pdf', '-nocrop', hfig);
end

%% Try another plot: a0 and wz for f/20 and f/40

hfig = figure(7610); clf(hfig); hfig.Position = [100 100 600 400];
clear hh;
set(gca, 'NextPlot', 'add');

hh(1:2) = plot(prop_axis{1}*1e3*c,...
        [(smooth(simgrid{1}(bub_centres{1})-bubble_sizes_xi{1})')*1e6;...
        2*sqrt(a0_peak{1}')*1e6/kp]);
hh(3:4) = plot(prop_axis{2}*1e3*c,...
        [(smooth(simgrid{2}(bub_centres{2})-bubble_sizes_xi{2})')*1e6;...
        2*sqrt(a0_peak{2}')*1e6/kp]);
set(hh, 'LineWidth', 1.5);
set(hh(1:2), 'Color', 'r');
set(hh(3:4), 'Color', 'k');
set(hh([1 3]), 'LineStyle', '--');
grid on; grid minor;
set(gca, 'LineWidth', 0.5, 'XLim', [0 6], 'Box', 'on');
xlabel('Propagation distance / mm');
hleg = legend(hh, {'f/40 $r_z$', 'f/40 $a_0$', 'f/20 $r_z$', 'f/20 $a_0$'},...
    'Location', 'Northwest');
make_latex(hfig); setAllFonts(hfig, 13);

if saveplots
    export_fig(fullfile(datsavfol, 'Evolutions2'), '-pdf', '-nocrop', hfig);
end
