% extract3D_drishti_files.m
%
% This script will loop through density dumps and save them as Drishti
% compatible files, in a separate folder. The diagnostic is interpolated
% onto a fixed grid to make life faster.
%
% 

datfol = '/Volumes/SimKris/Gem3D_5';
flist = dir(fullfile(datfol, 'data/normal*.sdf'));
% Also write the project files...

for i=1:1:length(flist)
    %fname = 'normal0050';
    fprintf('Working on file %3i, filename %s\n', i, flist(i).name);
    [~, fname, ~] = fileparts(flist(i).name);
    dataf = GetDataSDFchoose(fullfile(datfol, 'data', [fname '.sdf']), 'grid');
    x = dataf.Grid.Grid.x(1:end-1)*1e6;
    y = dataf.Grid.Grid.y(1:end-1)*1e6;
    z = dataf.Grid.Grid.z(1:end-1)*1e6;
    
    % Use fixed, 0.5 micron voxels. Making this smaller will make
    % everything slower
    xr = double(min(x):0.5:max(x));
    yr = -50:0.5:50;
    zr = -50:0.5:50;
    
    dataf = GetDataSDFchoose(fullfile(datfol,  'data', [fname '.sdf']), 'number_density/electron');
    ne = dataf.Derived.Number_Density.electron.data*1e-6;
    % Doing a bit of filtering and smoothing as EPOCH is notoriously messy
    ne_ = imgaussfilt3(ne, 1.5);
    
    [XX,YY,ZZ] = meshgrid(y, x, z);
    % The try below is needed if some of the dumps have been cut down in
    % size to save spece.
    try
        ne_ = interp3(XX,YY,ZZ,ne_, yr, xr', zr);
    catch err
        ne_ = interp3(XX,YY,ZZ,ne_(1:end-1,1:end-1,1:end-1), yr, xr', zr);
    end
    % Again, a wee bit of filtering
    ne_f = imgaussfilt3(ne_, 1.5);
    % The lines below make a new volume ne_f2, which will have a few layers
    % filled with cuts along the central axis. This allows Drishti to
    % visualise projections in the same image
    ne_f_y = mean(ne_f(:,99:101,:),2); % Averageing over 3 cells, again EPOCH is messy
    ne_f2 = ne_f; 
    ne_f2 = cat(2, ne_f2, repmat(ne_f_y, [1 4 1]));
    ne_f2(:,end+1,:) = ones(size(ne_f_y), 'single')*4e18; % This make the visualisation look nicer
    
    ne_f_z = mean(ne_f2(:,:,99:101),3);
    ne_f2 = cat(3, ne_f2, repmat(ne_f_z, [1 1 4]));
    ne_f2 = cat(3, ne_f2, ones(size(ne_f_z), 'single')*4e18);
    % And mask out the main thing pulse from ne_f2
    ne_f2(:, 1:201, 1:201) = 0;
    ne_f2(:, 201:end, 201:end) = 0;
    ne_f2(ne_f2>60e18) = 60e18;
    % Using Juffalo's writing script; Using fixed limits for dynamic range
    % makes for nicely reusable Drishti setups.
    DrishtiWriteUcharVolume(ne_f, fullfile(datfol, 'drishti_ne', [fname '_ne_interp2']), 1e18, 64e18);
    DrishtiWriteUcharVolume(ne_f2, fullfile(datfol, 'drishti_ne', [fname '_ne_interp3']), 1e18, 64e18);
end
fprintf('Done\n');

%% And now, assuming all the saved images are in GDrive, add timestamps
% The code below will simply add a timestamp onto all images so we know
% what the progress of the simulation is.

imfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D', 'Gem3D_5', 'drishti_ne_proj');
imlist = dir(fullfile(imfol, '*.png'));
for i=1:length(imlist)
    fprintf('Working on image %i out of %i\n', i, length(imlist));
    im = imread(fullfile(imfol, imlist(i).name));
    % Doing a little bit of cropping - this part is up to the reader
    im = im(101:1100, 201:1300,:);
    % getEPOCHtime returns the dump time, if this function doesn't exist
    % ask KP for a copy
    tim = getEPOCHtime(fullfile(datfol, 'data', ['normal' imlist(i).name(5:8) '.sdf']));
    im = insertText(im, [30 30], sprintf('t = %2.1f ps', tim*1e12),...
        'BoxColor', 'w', 'FontSize', 36, 'Font', 'Arial');
    imwrite(im, fullfile(imfol, 'dump_times', imlist(i).name));
end
