% Plot the evolution of the two planes 

datfol = 'F:/Gem3D_9/data';
savfol = 'F:/Gem3D_9/y_vs_z';
if ~exist(savfol, 'dir'); mkdir(savfol); end
Ez_y = load(fullfile(datfol, 'Ez_all_y'));
Ez_z = load(fullfile(datfol, 'Ez_all_z'));
ne_y = load(fullfile(datfol, 'Number_density_electron_all_y'));
ne_z = load(fullfile(datfol, 'Number_density_electron_all_z'));

%%
locs = GetFigureArray(2, 2, [0.03 0.02 0.1 0.07], 0.06, 'down');
ax = [];
for i=1:length(Ez_z.slicedata);
    hfig = figure(98); clf(hfig); 
    set(hfig, 'Color', 'w', 'Position', [100 100 1280 720]);
    fprintf('Plotting for file %i\n', i);
    ax(1) = axes('Parent', hfig, 'Position', locs(:,1));
    Ezy = Ez_y.slicedata{i};
    imagesc(Ez_y.grid_x{i}*1e3, Ez_y.grid_t{i}*1e6, 0.5e-22*epsilon0*c*abs(hilbert(Ezy)').^2, 'Parent', ax(1));
    ylabel('y / micron')
    colorbar;
    ax(2) = axes('Parent', hfig, 'Position', locs(:,2));
    Ezz = Ez_z.slicedata{i};
    imagesc(Ez_z.grid_x{i}*1e3, Ez_z.grid_t{i}*1e6, 0.5e-22*epsilon0*c*abs(hilbert(Ezz)').^2, 'Parent', ax(2));
    ylabel('z / micron')
    colorbar;
    ax(3) = axes('Parent', hfig, 'Position', locs(:,3));
    imagesc(ne_y.grid_x{i}*1e3, ne_y.grid_t{i}*1e6, 1e-6*ne_y.slicedata{i}'/ncrit(800), 'Parent', ax(3));
    ax(4) = axes('Parent', hfig, 'Position', locs(:,4));
    imagesc(ne_z.grid_x{i}*1e3, ne_z.grid_t{i}*1e6, 1e-6*ne_z.slicedata{i}'/ncrit(800), 'Parent', ax(4));
    set(ax, 'YLim', [-40 40]);
    set(ax(3:4), 'CLim', [0 0.025]);
    
    setAllFonts(hfig, 16); colormap(parula);
    export_fig(fullfile(savfol, sprintf('normal%04i', i)), '-nocrop', '-png', hfig);
end