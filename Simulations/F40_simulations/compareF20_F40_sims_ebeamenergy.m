% compareF20_F40_sims_ebeamenergy.m
%
% Collects the data to plot electron beam energy evolution.

if ismac
    rootfol = '/Volumes/SimKris';
elseif ispc
    %rootfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
    rootfol = 'D:\Gem2015_3D';
end
fols{1} = 'Gem3D_4'; % F/40, circular
fols{2} = 'Gem3D_5'; % F/40, ell
fols{3} = 'Gem3D_6'; % F/20, ell
fols{4} = 'Gem3D_7'; % F/10, ell

for k=1:4
    Ex_y{k} = load(fullfile(rootfol, fols{k}, 'data', 'Ex_all_y.mat'));
end

datsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
figsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
saveplots = 0;

%% Do the calculations: get the ebeam spectrum and the field felt by max energy particles.

% Get the px data:
en_axis = 2:1:2000;
px_evol = cell(3,1);
max_el_ens = cell(3,1);
max_el_phase = cell(3,1);
Ex_max_phase = cell(3,1);
prop_axis = cell(size(px_evol));
for i=1:4
    flist = dir(fullfile(rootfol, fols{i}, 'data', 'normal*.sdf'));
    px_evol{i} = zeros(length(flist), length(en_axis));
    max_el_phase{i} = zeros(length(flist), length(Ex_y{i}.grid_x{1}));
    prop_axis{i} = zeros(length(flist), 1);
    Ex_max_phase{i} = nan(size(prop_axis{i}));
    max_el_ens{i} = zeros(size(prop_axis{i}));
    for k=1:length(flist)
        fprintf('Opening file %3i, folder %i\n',  k, i);
        dataf = GetDataSDFchoose(fullfile(rootfol, fols{i}, 'data', flist(k).name), 'x_px/electron');
        try
            px_phasespace = dataf.dist_fn.x_px.electron.data;
            px_dat = sum(px_phasespace, 1);
            px_grid = dataf.Grid.x_px.electron.y/(qe*1e6/c);
            
        catch err
            fprintf('\t error for file %i, fol %i: %s\n', k, i, err.message);
        end
        if i==3 % These files haven't been extracted...
            %dataf = GetDataSDFchoose(fullfile(rootfol, fols{i}, 'data', flist(k).name), 'x_px/electron');
            %px_dat = sum(dataf.dist_fn.x_px.electron.data, 1);
            dataf = GetDataSDFchoose(fullfile(rootfol, fols{i}, 'data', flist(k).name), 'grid/x_px/electron');
            px_grid = dataf.Grid.x_px.electron.y/(qe*1e6/c);
        end
        px_evol{i}(k,:) = interp1(px_grid', px_dat', en_axis, 'linear', 1e-5);
        prop_axis{i}(k) = dataf.time;
        % And now calculate the maximum energy.
        ind = find(px_evol{i}(k,:)>0.0004, 1, 'last');
        if ~isempty(ind); 
            max_el_ens{i}(k) = en_axis(ind); 
            % And now lets save a lineout of the phasespace at max energy:
            [nx, ~] = size(px_phasespace);
            logm = abs(px_grid-en_axis(ind)) < 1; 
            
            if sum(logm)~=1
                max_el_phase{i}(k,1:nx) = sum(px_phasespace(:,logm), 2);
            else % If only element matches, means we are dealing with the high energy end of phasespace
                max_el_phase{i}(k,1:nx) = sum(px_phasespace(:,end-2:end), 2);
            end
            % And now, finally, find the Ex field in the phase of the ebeam
            ind2 = find(max_el_phase{i}(k,:)>0, 1, 'last');
            if ~isempty(ind2)
                Ex_loc = Ex_y{i}.slicedata{k};
                [~,ny] = size(Ex_loc); ny2 = round(0.5*ny);
                av_cell = 1;
                Ex_max_phase{i}(k) = mean(Ex_loc(ind2, ny2-av_cell:ny2+av_cell));
            end
        end
    end
end

save(fullfile(datsavfol, 'ElectronEnergyEvolution'), 'px_evol', 'prop_axis',...
    'en_axis', 'max_el_ens', 'Ex_max_phase');

%% Now plot and analyse the evolution itself

hfig = figure(720); clf(hfig); hfig.Color = 'w';
hfig.Position = [200 200 1200 500];

locs = GetFigureArray(4, 1, [0.09 0.13 0.13 0.1], 0.04, 'across');
ax = [];
for i=1:4
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,i));
    imagesc(prop_axis{i}*1e3*c, en_axis, log10(px_evol{i}'), 'Parent', ax(end));
    xlabel('Propagation / mm');
    grid(ax(end), 'on');
    makeNiceGridHG2(ax(end)); drawnow;
end

set(ax, 'YDir', 'normal', 'XLim', [0 12], 'CLim', [5 11], 'YLim', [200 2000],...
    'LineWidth', 1, 'Layer', 'top');
set(ax(2:4), 'YTickLabel', []);
ylabel(ax(1), 'Energy / MeV');
title(ax(1), 'F/40, circular'); title(ax(2), 'F/40, elliptical'); 
title(ax(3), 'F/20, elliptical'); title(ax(4), 'F/10, elliptical');

cb = colorbar(ax(end), 'Position', [0.88 locs(2,1) 0.02 locs(4,1)]);
ylabel(cb, '$\log_{10} Q $ / a.u.')
setAllFonts(hfig, 16); make_latex(hfig);
colormap(brewermap(256, 'PuBuGn'))

if saveplots
    export_fig(fullfile(figsavfol, 'ElectronEnergyEvolution'), '-pdf', '-nocrop', hfig);
end

%% Now extract the peak energy from the spectra and plot that as a function of propagation

hfig = figure(721); clf(hfig); hfig.Color = 'w';
hfig.Position = [200 200 600 400];

set(gca, 'NextPlot', 'add', 'LineWidth', 2, 'Layer', 'top', 'Box', 'on');
% How do I calculate the energy cutoff?

hh = [];
for i=1:4
    hh(i) = plot(prop_axis{i}*c*1e3, max_el_ens{i}, 'LineWidth', 2);
end
legend(hh, {'F/40, circ', 'F/40, ell', 'F/20, ell', 'F/10, ell'}, 'Location', 'southeast');
set(gca, 'XLim', [0 12], 'XTick', 0:12);
xlabel(gca, 'Propagation / mm'); ylabel(gca, 'Energy / MeV');
setAllFonts(hfig, 16); make_latex(hfig);
grid(gca, 'on'); makeNiceGridHG2(gca);

if saveplots
    export_fig(fullfile(figsavfol, 'ElectronEnergyEvolution2'), '-pdf', '-nocrop', hfig);
end

%% And now plot the electric field felt by the max energy particles

hfig = figure(722); clf(hfig); hfig.Color = 'w';
hfig.Position = [200 200 600 400];

set(gca, 'NextPlot', 'add', 'LineWidth', 2, 'Layer', 'top', 'Box', 'on')
markers = 's<do';
hh = [];
for i=1:4
    %%hh(i) = plot(prop_axis{i}*c*1e3 -(i==1)*1.8, Ex_max_phase{i}, 'LineWidth', 2);
    hh(i) = scatter(prop_axis{i}*c*1e3 -(i==1)*1.8, Ex_max_phase{i}, 30, max_el_ens{i}, 'filled', markers(i));
end
legend(hh, {'F/40, circ', 'F/40, ell', 'F/20, ell', 'F/10, ell'}, 'Location', 'southeast');
set(gca, 'XLim', [0 12], 'YLim', [-10e11 2e11], 'XTick', 0:12)

xlabel(gca, 'Propagation / mm'); ylabel(gca, '$E_x$ / $\mathrm{V\,cm}^{-1}$');
setAllFonts(hfig, 16); make_latex(hfig);
grid(gca, 'on'); makeNiceGridHG2(gca);

if saveplots
    export_fig(fullfile(figsavfol, 'ElectronMaxEnergyField'), '-pdf', '-nocrop', hfig);
end
