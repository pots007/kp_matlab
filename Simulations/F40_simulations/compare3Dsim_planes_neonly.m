% compare3Dsim_planes_neonly.m
%
% Plot the evolution of the two planes 
% Plot the n_e only, with axis image to understand what's happening.

datfol = '/Volumes/SimKris/Gem3D_5/data';
savfol = '/Volumes/SimKris/Gem3D_5/y_vs_z2';
%Ez_y = load(fullfile(datfol, 'Ez_all_y'));
%Ez_z = load(fullfile(datfol, 'Ez_all_z'));
ne_y = load(fullfile(datfol, 'Number_density_electron_all_y'));
ne_z = load(fullfile(datfol, 'Number_density_electron_all_z'));

filt_kernel = 2;

%%
locs = GetFigureArray(2, 1, [0.18 0.08 0.1 0.08], 0.04, 'down');
ax = [];
for i=1:length(ne_z.slicedata);
    hfig = figure(99); clf(hfig); 
    set(hfig, 'Color', 'w', 'Position', [100 100 1200 720]);
    fprintf('Plotting for file %i\n', i);
    ax(1) = axes('Parent', hfig, 'Position', locs(:,1));
    imagesc(ne_y.grid_x{i}*1e6, ne_y.grid_t{i}*1e6, ...
        1e-6*imgaussfilt(ne_y.slicedata{i}', filt_kernel)/ncrit(800), 'Parent', ax(1));
    axis(ax(1), 'image');
    ax(2) = axes('Parent', hfig, 'Position', locs(:,2));
    imagesc(ne_z.grid_x{i}*1e6, ne_z.grid_t{i}*1e6, ...
        1e-6*imgaussfilt(ne_z.slicedata{i}', filt_kernel)/ncrit(800), 'Parent', ax(2));
    axis(ax(2), 'image');
    set(ax, 'YLim', [-40 40], 'CLim', [0 0.01], 'LineWidth', 2, 'Layer', 'top');
    set(ax(2), 'YAxisLocation', 'right');
    ylabel(ax(1), 'y / micron');
    ylabel(ax(2), 'z / micron');
    xlabel(ax(1), 'x / micron'); xlabel(ax(2), 'x / micron');
    cb = colorbar('Peer', ax(1), 'Location', 'North', 'LineWidth', 2);
    xlabel(cb, '$n_e / n_c$');
    set(cb, 'Position', [locs(1,1) 0.84 locs(3,1) 0.02], 'Ticks', (0:0.25:1)*0.01);
     cb = colorbar('Peer', ax(2), 'Location', 'North', 'LineWidth', 2);
    xlabel(cb, '$n_e / n_c$');
    set(cb, 'Position', [locs(1,2) 0.84 locs(3,2) 0.02], 'Ticks', (0:0.25:1)*0.01);
    setAllFonts(hfig, 24); make_latex(hfig);
    colormap(inferno);
    export_fig(fullfile(savfol, sprintf('normal%04i', i)), '-nocrop', '-png', hfig);
end