% compareF20_F40_sims_bubblesizes.m
%
% Collects the data to plot electron beam energy evolution.


%rootfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
rootfol = 'D:\Gem2015_3D';

clear fols;
fols{1} = 'Gem3D_4'; % F/40
fols{end+1} = 'Gem3D_5'; % F/40
fols{end+1} = 'Gem3D_6'; % F/20
fols{end+1} = 'Gem3D_7'; % F/10

for k=1:4
    %Ex_y{k} = load(fullfile(rootfol, fols{k}, 'data', 'Ex_all_y.mat'));
    %Ex_z{k} = load(fullfile(rootfol, fols{k}, 'data', 'Ex_all_z.mat'));
    %ne_y{k} = load(fullfile(rootfol, fols{k}, 'data', 'Number_Density_electron_all_y.mat'));
    %ne_z{k} = load(fullfile(rootfol, fols{k}, 'data', 'Number_Density_electron_all_z.mat'));
end

datsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');
datsavname = fullfile(datsavfol, 'BubbleSizeData');
figsavfol = fullfile(getGDrive, 'epoch', 'Gem2015_3D');

%% We now find the point of the ex field zero point and then
%  find the bubble size in that places

y_ax = (-70:0.25:70)*1e-6;
simgrid = [];
%for k=1:4; simgrid{k} =  Ex_y{k}.grid_x{1}; end;

if ~exist(datsavname, 'file')
    
    av_cell = 1; % Averages over 3 cells
    av_nes = 1;  % Averages over 5 longitudinal cells
    Ex_axis = cell(4,1);
    Ex_min = cell(4,1);
    bub_transverse_y = cell(4,1);
    bub_transverse_z = cell(4,1);
    bub_centres = cell(4,1);
    prop_axis = cell(4,1);
    
    for i=1:4
        % Since I don't hve infinite ram any more...
        clear Ex_y Ex_z ne_y ne_z
        Ex_y{i} = load(fullfile(rootfol, fols{i}, 'data', 'Ex_all_y.mat'));
        Ex_z{i} = load(fullfile(rootfol, fols{i}, 'data', 'Ex_all_z.mat'));
        ne_y{i} = load(fullfile(rootfol, fols{i}, 'data', 'Number_Density_electron_all_y.mat'));
        ne_z{i} = load(fullfile(rootfol, fols{i}, 'data', 'Number_Density_electron_all_z.mat'));
        
        simgrid{i} =  Ex_y{i}.grid_x{1};
        
        Ex_axis{i} = zeros(length(Ex_y{i}.grid_t), size(Ex_y{i}.slicedata{1},1));
        Ex_min{i} = zeros(size(Ex_y{i}.grid_t));
        bub_transverse_y{i} = zeros(length(Ex_y{i}.grid_t), length(y_ax));
        bub_transverse_z{i} = zeros(length(ne_z{i}.grid_t), length(y_ax));
        bub_centres{i} = zeros(size(Ex_y{i}.grid_t));
        
        for k=1:size(Ex_axis{i},1)
            Ex_loc = Ex_y{i}.slicedata{k};
            [nx, ~] = size(Ex_loc);
            mid_ind = round(0.5*size(Ex_loc,2));
            Ex_line = mean(Ex_loc(:, mid_ind-av_cell:mid_ind+av_cell),2);
            Ex_line = smooth(Ex_line);
            Ex_axis{i}(k,1:nx) = smooth(Ex_line);
            %[~, ind] = min(abs(Ex_line(1:1950)));
            ind = find(Ex_line(1:1750)<0, 1, 'last');
            bub_centres{i}(k) = ind;
            % And now try to figure out what the size of the bubble is - in y first
            ne_loc = ne_y{i}.slicedata{k};
            [~, ny] = size(ne_loc);
            ne_trans_line = mean(ne_loc(ind-av_nes:ind+av_nes,:),1);
            bub_transverse_y{i}(k,:) = interp1(ne_y{i}.grid_t{k}(1:ny), ne_trans_line, y_ax, 'linear', 0);
            % And now try to figure out what the size of the bubble is - and now z
            ne_loc = ne_z{i}.slicedata{k};
            [~, nz] = size(ne_loc);
            ne_trans_line = mean(ne_loc(ind-av_nes:ind+av_nes,:),1);
            bub_transverse_z{i}(k,:) = interp1(ne_z{i}.grid_t{k}(1:nz), ne_trans_line, y_ax, 'linear', 0);
            Ex_min{i}(k) = min(Ex_axis{i}(k,:));
        end
        bub_centres{i} = round(smooth(bub_centres{i}));
        prop_axis{i} = Ex_y{i}.grid_time;
    end
    
    % Cheeky bit of  saving the analysis data...
    save(datsavname, 'bub_transverse_y', 'bub_transverse_z',...
        'Ex_min', 'Ex_axis', 'bub_centres', 'prop_axis', 'y_ax', 'simgrid');
    
else
    load(datsavname);
end
%%
% This plots the bubble evolution
hfig = figure(730); clf(hfig); hfig.Color = 'w';
if ismac
    hfig.Position = [200 200 900 900];
end
locs = GetFigureArray(2, 3, [0.06 0.1 0.1 0.1], 0.04, 'across');
ax = [];
for i=1:3
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,i*2-1));
    imagesc(prop_axis{i}*1e3*c, y_ax*1e6, bub_transverse_y{i}', 'Parent', ax(end));
    ylabel('y / micron');
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,i*2));
    imagesc(prop_axis{i}*1e3*c, y_ax*1e6, bub_transverse_z{i}', 'Parent', ax(end));
    ylabel('z / micron');
end

set(ax, 'YDir', 'normal', 'XLim', [0 12], 'CLim', [0 1.5e25], 'YLim', [-25 25],...
    'LineWidth', 2, 'Layer', 'top', 'XTick', 0:12);
set(ax(1:4), 'XTickLabel', []);
set(ax(2:2:6), 'YAxisLocation', 'right');
xlabel(ax(6), 'Propagation / mm'); xlabel(ax(5), 'Propagation / mm');
title(ax(1), 'F/40'); title(ax(2), 'F/40');
title(ax(3), 'F/20'); title(ax(4), 'F/20');
title(ax(5), 'F/10'); title(ax(6), 'F/10');

setAllFonts(hfig, 16); make_latex(hfig);
colormap(brewermap(256, 'Blues'))

for i=1:6
    grid(ax(i), 'on'); drawnow; makeNiceGridHG2(ax(i));
end

if saveplots
    export_fig(fullfile(figsavfol, 'BubbleSizeEvolution'), '-pdf', '-nocrop', hfig);
end

%% This plots the bubble centre locations
hfig = figure(731); clf(hfig); hfig.Color = 'w';
if ismac
    hfig.Position = [1200 200 1000 500];
end
locs = GetFigureArray(3, 1, [0.08 0.13 0.13 0.07], 0.02, 'across');
ax = [];
for i=1:3
    ax(end+1) = axes('Parent', hfig, 'Position', locs(:,i), 'NextPlot', 'add', 'Box', 'on');
    %imagesc(prop_axis{i}*1e3*c, fliplr(Ex_y{i}.grid_x{1}*1e6), Ex_axis{i}', 'Parent', ax(end));
    imagesc(prop_axis{i}*1e3*c, fliplr(simgrid*1e6), Ex_axis{i}', 'Parent', ax(end));
    %plot(prop_axis{i}*1e3*c, (Ex_y{i}.grid_x{1}(bub_centres{i})*1e6), '-k');
    plot(prop_axis{i}*1e3*c, (simgrid(bub_centres{i})*1e6), '-k');
    xlabel('Propagation / mm');
end

set(ax, 'YDir', 'normal', 'XLim', [0 12], 'CLim', [-6e11 6e11], 'YLim', [-10 60],...
    'LineWidth', 2, 'Layer', 'top');
set(ax(2:3), 'YTickLabel', []);
ylabel(ax(1), 'y / micron');
title(ax(1), 'F/40'); title(ax(2), 'F/20'); title(ax(3), 'F/10');
cb = colorbar(ax(end), 'Position', [0.88 locs(2,1) 0.02 locs(4,1)]);
ylabel(cb, '$E_x $ / V/m')

setAllFonts(hfig, 16); make_latex(hfig);
colormap(brewermap(256, 'RdBu'));
if saveplots
    export_fig(fullfile(figsavfol, 'AccFieldEvolution'), '-pdf', '-nocrop', hfig);
end

% And this plots the maximum accelerating field felt by the particle.
% This plots the bubble centre locations
hfig = figure(732); clf(hfig); hfig.Color = 'w';
if ismac
    hfig.Position = [1200 800 600 400];
end
locs = GetFigureArray(1, 1, [0.08 0.1 0.15 0.15], 0.02, 'across');
ax = []; ax(end+1) = axes('Parent', hfig, 'Position', locs, 'NextPlot', 'add', 'Box', 'on');
hh = [];
for i=1:3
    hh(i) = plot(prop_axis{i}*1e3*c, smooth(Ex_min{i}, 4), '-', 'LineWidth', 2);
end

set(ax, 'YDir', 'normal', 'XLim', [0 12], 'YLim', [-12e11 0],...
    'LineWidth', 2, 'Layer', 'top');

ylabel(ax(1), '$E_x$ / $\mathrm{V\,m}^{-1}$');
xlabel('Propagation / mm');
legend(hh, {'F/40', 'F/20', 'F/10'}, 'Location', 'South');
grid(ax, 'on'); makeNiceGridHG2(ax);

setAllFonts(hfig, 16); make_latex(hfig);
if saveplots
    export_fig(fullfile(figsavfol, 'AccFieldMax'), '-pdf', '-nocrop', hfig);
end
