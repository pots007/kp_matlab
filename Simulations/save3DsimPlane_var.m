% save3DsimPlane_var.m
%
% For speeding up doing plane cut things, let's save these into arrays!
% This file works even if the different files have different spatial
% extents - case if some of them have been trimmed.

function save3DsimPlane_var(datfol, diag, plane, fstem)
% Get list of files we have
if nargin==3; fstem = 'normal'; end
flist = dir(fullfile(datfol, [fstem '*.sdf']));
% We assume the grid based diagnostic remain the same throughout the
% simulation!

tt = GetBlocksSDF(fullfile(datfol, flist(1).name))';
diag_file = {tt.id}';
if isempty(diag); diag = diag_file; end

% And now iterate over the diagnostics, get a plane, save it, bam.
% But ensure grid exists in each file as well.
for d=1:length(diag)
    diag_ = diag{d};
    if ~strcmpi(diag_, diag_file);
        fprintf('Diagnostic %s not present in file %s!\n', diag_, flist(1).name);
        %continue;
    end;
    
    % Load file 1 and allocate dataarrays!
    gridf = GetDataSDFchoose(fullfile(datfol, flist(1).name), 'grid');
    grid_x = cell(1,length(flist));
    grid_time = zeros(1,length(flist));
    grid_t = cell(1, length(flist));
    slicedata = cell(1, length(flist));
    
    
    indd = strfind(diag{d},'_');
    if length(indd)>1; diag_(indd(end)) = '/'; end;
    for f=1:length(flist)
        fprintf('Opening file %s for diagnostic %s\n', flist(f).name, diag_);
        gridf = GetDataSDFchoose(fullfile(datfol, flist(f).name), 'grid');
        try
            dataf = GetDataSDFchoose(fullfile(datfol, flist(f).name), diag_);
        catch err
            fprintf('Failed to get data for %s, diagnostic %s: %s\n', flist(f).name, diag_, err.message);
        end
        % Now use a few trys in case the grid size has changed
        grid_x{f} = gridf.Grid.Grid.x;
        grid_time(f) = gridf.time;
        if strcmp(plane, 'y')
            indc = round(0.5*length(gridf.Grid.Grid.y));
        else % Default to cutting perpendicular to z plane
            indc = round(0.5*length(gridf.Grid.Grid.z));
        end
        % Now we put the diagnostics into their slices:
        try
            switch diag_
                case 'Ex'
                    slice_ = dataf.Electric_Field.Ex.data;
                case 'Ey'
                    slice_ = dataf.Electric_Field.Ey.data;
                case 'Ez'
                    slice_ = dataf.Electric_Field.Ez.data;
                case 'Bx'
                    slice_ = dataf.Magnetic_Field.Bx.data;
                case 'By'
                    slice_ = dataf.Magnetic_Field.By.data;
                case 'Bz'
                    slice_ = dataf.Magnetic_Field.Bz.data;
                otherwise
                    % For number densities
                    if sum(strncmpi('Number_Density', diag_, 14))
                        indd = strfind(diag_, '/');
                        species = diag_(indd(end)+1:end);
                        slice_ = dataf.Derived.Number_Density.(species).data;
                    else
                        break;
                    end
            end
        catch err
            disp(err.message);
            continue;
        end
        if strcmp(plane, 'y')
            grid_t{f} = gridf.Grid.Grid.z;
            slice_ = squeeze(slice_(:,indc,:));
        else
            grid_t{f} = gridf.Grid.Grid.y;
            slice_ = slice_(:,:,indc);
        end
        % Now put it back into the slicedata matrix
        slicedata{f} = slice_;
    end
    
    diagn = diag{d};
    diagn(diagn==char(47)) = char(95);
    save(fullfile(datfol, [diagn '_all_' plane]), 'slicedata', 'grid_x', 'grid_t', 'grid_time');
end
