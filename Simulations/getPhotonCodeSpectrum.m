function p = getPhotonCodeSpectrum(simf)
%simf = 'poschirp4';
Constants;
fils = dir([simf '/dumps/*k.txt']);
pR = getPhotonCodeParams(simf);
propa = (1:length(fils))*pR.tdump*1e3*c;
dat = importdata([simf '/dumps/' fils(1).name]);
kdata = zeros(length(fils), size(dat,1));
kdata(1,:) = dat(:,2);%./max(dat(:,2));
kax = dat(:,1);
for i=2:length(fils)
    dat = importdata([simf '/dumps/' fils(i).name]);
    kdata(i,:) = dat(:,2);%./max(dat(:,2));
end
p.propa = propa; p.kax = kax; p.kdata = kdata';
%imagesc(propa, kax, kdata');