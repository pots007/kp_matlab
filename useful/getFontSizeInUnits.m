function P = getFontSizeInUnits(h, units)
% Get font size of a HG object in specified units
% P = getFontSizeInUnits(h, units)
% (c) Dane R. Austin 2012
old_u = get(h, 'FontUnits');
set(h, 'FontUnits', units);
P = get(h, 'FontSize');
set(h, 'FontUnits', old_u);