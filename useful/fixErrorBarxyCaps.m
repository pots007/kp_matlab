% fixErrorBarxyCaps.m
%
% Fix the cap widths for plots generated with errorbarxy
% Arguments are the handle generated by the function, and the width and
% height of the caps, in dataspace units
% 
% For the case of having error bars for only x or y, set the width for the
% missing error bars to [].

function fixErrorBarxyCaps(hh, capH, capV)

ydat = get(hh.hMain, 'Ydata');
xdat = get(hh.hMain, 'Xdata');
npnts = length(ydat);
nerrs = length(hh.hErrorbar);
if nerrs/npnts==6
    % capH = 0.3; capV = 0.04;
    for i=(npnts*1+1):(2*npnts); set(hh.hErrorbar(i), 'Ydata', ydat(i-1*npnts) + [-1 1]*capH); end
    for i=(npnts*2+1):(3*npnts); set(hh.hErrorbar(i), 'Ydata', ydat(i-2*npnts) + [-1 1]*capH); end
    %A nd x
    for i=(npnts*5+1):(6*npnts); set(hh.hErrorbar(i), 'Xdata', xdat(i-5*npnts) + [-1 1]*capV); end
    for i=(npnts*4+1):(5*npnts); set(hh.hErrorbar(i), 'Xdata', xdat(i-4*npnts) + [-1 1]*capV); end
elseif nerrs/npnts==3
    if isempty(capH)
        for i=(npnts*1+1):(2*npnts); set(hh.hErrorbar(i), 'Xdata', xdat(i-1*npnts) + [-1 1]*capV); end
        for i=(npnts*2+1):(3*npnts); set(hh.hErrorbar(i), 'Xdata', xdat(i-2*npnts) + [-1 1]*capV); end        
    elseif isempty(capV)
        for i=(npnts*1+1):(2*npnts); set(hh.hErrorbar(i), 'Ydata', xdat(i-1*npnts) + [-1 1]*capH); end
        for i=(npnts*2+1):(3*npnts); set(hh.hErrorbar(i), 'Ydata', xdat(i-2*npnts) + [-1 1]*capH); end        
    end
else
    fprintf('Something wrong here... :(\n');
end