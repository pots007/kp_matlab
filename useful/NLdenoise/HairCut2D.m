function new_im = HairCut2D(im,R1,R2,ratio)

% filter is 0 for r<R1 or r>R2
% filter is positive for R1<r<R2

if R2<2 
  err = 'error: R2<2'
end
if (R2-R1 < 2) 
  err = 'error: R2-R1 <2'
end

[vv hh]=size(im);

ratiopos = ratio;
rationeg = 1/ratio;

l = 2*R2+1;
a = linspace(-R2,R2,l);
F = sqrt(repmat(a,l,1).^2 + repmat(a',1,l).^2);
parfor lili = 1:l
  for coco = 1:l
      F(lili,coco) = wshape(F(lili,coco),R1,R2);
  end
end
F = F./(sum(sum(F)));

figure(10001)
imagesc(F)
 
am = weigthed_average(im,F);
bm = im./am;

h1 = (bm > ratiopos);
h2 = (bm<rationeg);
hair = h1+h2;
ok = ones(vv,hh)-hair;

new_im = ok.*im + hair.*am;

end

function wav_im = weigthed_average(image,w)
  
  [vv hh]   = size(image);
  [wv wh] = size(w);

  padded_im = padarray(image,[(wv-1)/2,(wh-1)/2],'symmetric','both');
  
  [pv ph]=size(padded_im); 
  
  am = zeros(vv,hh);
  
  for lili = 1:wv
    for coco = 1:wh
      am = am + w(lili,coco).*padded_im(lili:lili+vv-1,coco:coco+hh-1);    
    end
  end
  
  wav_im = am;
  
end

function WR = wshape(r,R1,R2)
  
  d = (R2-R1)/6;
  p1 = R1;
  p2 = p1+d;
  p3 = p2+d;
  p4 = p3+d;
  p5 = p4+d;
  p6 = p5+d;
  p7 = p6+d;
  
  f1 = 0;
  f2 = 1/5;
  f3 = 1;
  f4 = 1;
  f5 = 1/2;
  f6 = 1/5;
  f7 = 0;
  
  s1 = pi*(p2-p1)^2;
  s2 = pi*(p3-p2)^2;
  s3 = pi*(p4-p3)^2;
  s4 = pi*(p5-p4)^2;
  s5 = pi*(p6-p5)^2;
  s6 = pi*(p7-p6)^2;
  
  if      r<p1 
    WR = 0;
  elseif  r>=p7 
    WR = 0;
  elseif r<=p2 
    WR = (f1 + (r-p1)*(f2-f1)/(p2-p1))/s1;
  elseif r<p3 
    WR = (f2 + (r-p2)*(f3-f2)/(p3-p2))/s2;
  elseif r<p4 
    WR = (f3 + (r-p3)*(f4-f3)/(p4-p3))/s3;
  elseif r<p5 
    WR = (f4 + (r-p4)*(f5-f4)/(p5-p4))/s4;
  elseif r<p6 
    WR = (f5 + (r-p5)*(f6-f5)/(p6-p5))/s5;
  elseif r<p7 
    WR = (f6 + (r-p6)*(f7-f6)/(p7-p6))/s6;
  end
  
  calculatedWR = WR;
          
end
  
