function [HC_image,FNLD_Image_roi2,FNLD_weight_roi2,FNLD_noise_roi2,composit,OverDone_FNLD_Image_roi2] = G14_ShockInspector(shockimage)
% automatic treatment of filter-pack images for Gemini 2014
%   fast and dirthy 
tic
step=101
im101 = haircut2D(shockimage,3,11,4);
%im101 = haircut2D(im101,3,11,3.0);
im101 = haircut2D(im101,3,11,2.5);
%im101 = haircut2D(im101,3,11,2.0);
im101 = haircut2D(im101,3,11,1.75);
toc
[logstepnoise,stepnoise,fullnoise,a,b,c,d,e,f,g,h] = dothemath(im101,shockimage,shockimage);
figure(1), imagesc(shockimage), colormap(gray), title('original'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1011), imagesc(im101), colormap(gray), title('101HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1012), imagesc(logstepnoise), colormap(lines), title('101logstepnoise'), colorbar, drawnow; caxis([c d]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1013), imagesc(stepnoise), colormap(jet), title('101stepnoise'), colorbar, drawnow; caxis([0 1000]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1014), imagesc(fullnoise), colormap(jet), title('101fullnoise'), colorbar, drawnow; caxis([g h]),set(gca,'DataAspectRatio',[1 1 1]);
  
tic
step=102
im102 = haircut2D(im101,2,7,2.2);
%im102 = haircut2D(im102,2,7,2.0);
%im102 = haircut2D(im102,2,7,1.8);
im102 = haircut2D(im102,2,7,1.6);
%im102 = haircut2D(im102,2,7,1.5);
im102 = haircut2D(im102,2,7,1.4);

toc
[logstepnoise,stepnoise,fullnoise,a,b,c,d,e,f,g,h] = dothemath(im102,im101,shockimage);
figure(1), imagesc(shockimage), colormap(gray), title('original'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1021), imagesc(im102), colormap(gray), title('102HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1022), imagesc(logstepnoise), colormap(lines), title('102logstepnoise'), colorbar, drawnow; caxis([c d]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1023), imagesc(stepnoise), colormap(jet), title('102stepnoise'), colorbar, drawnow; caxis([0 800]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1024), imagesc(fullnoise), colormap(jet), title('102fullnoise'), colorbar, drawnow; caxis([g h]),set(gca,'DataAspectRatio',[1 1 1]);

tic
step=103
im103 = haircut2D(im102,1,5,1.40);
%im103 = haircut2D(im103,1,5,1.35);
%im103 = haircut2D(im103,1,5,1.30);
im103 = haircut2D(im103,1,5,1.275);
%im103 = haircut2D(im103,1,5,1.26);
im103 = haircut2D(im103,1,5,1.25);

toc
[logstepnoise,stepnoise,fullnoise,a,b,c,d,e,f,g,h] = dothemath(im103,im102,shockimage);
figure(1), imagesc(shockimage), colormap(gray), title('original'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1031), imagesc(im103), colormap(gray), title('103HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1032), imagesc(logstepnoise), colormap(lines), title('103logstepnoise'), colorbar, drawnow; caxis([c d]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1033), imagesc(stepnoise), colormap(jet), title('103stepnoise'), colorbar, drawnow; caxis([0 400]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1034), imagesc(fullnoise), colormap(jet), title('103fullnoise'), colorbar, drawnow; caxis([g h]),set(gca,'DataAspectRatio',[1 1 1]);

tic
step=104
im104 = haircut2D(im103,0,3,1.35);
%im104 = haircut2D(im104,0,3,1.29);
im104 = haircut2D(im104,0,3,1.25);
%im104 = haircut2D(im104,0,3,1.20);
im104 = haircut2D(im104,0,3,1.19);
toc
[logstepnoise,stepnoise,fullnoise,a,b,c,d,e,f,g,h] = dothemath(im104,im103,shockimage);
figure(1), imagesc(shockimage), colormap(gray), title('original'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1041), imagesc(im104), colormap(gray), title('104HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1042), imagesc(logstepnoise), colormap(lines), title('104logstepnoise'), colorbar, drawnow; caxis([c d]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1043), imagesc(stepnoise), colormap(jet), title('104stepnoise'), colorbar, drawnow; caxis([0 300]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1044), imagesc(fullnoise), colormap(jet), title('104fullnoise'), colorbar, drawnow; caxis([g h]),set(gca,'DataAspectRatio',[1 1 1]);

tic
step=105
im105 = haircut2D(im104,0,2,1.20);
%im105 = haircut2D(im105,0,3,1.15);
%im105 = haircut2D(im105,0,2,1.10);
im105 = haircut2D(im105,0,2,1.08);
%im105 = haircut2D(im105,0,2,1.06);
im105 = haircut2D(im105,0,2,1.05);
toc
[logstepnoise,stepnoise,fullnoise,a,b,c,d,e,f,g,h] = dothemath(im105,im104,shockimage);
figure(1), imagesc(shockimage), colormap(gray), title('original'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1051), imagesc(im105), colormap(gray), title('105HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1052), imagesc(logstepnoise), colormap(lines), title('105logstepnoise'), colorbar, drawnow; caxis([c d]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1053), imagesc(stepnoise), colormap(jet), title('105stepnoise'), colorbar, drawnow; caxis([0 250]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1054), imagesc(fullnoise), colormap(jet), title('105fullnoise'), colorbar, drawnow; caxis([g h]),set(gca,'DataAspectRatio',[1 1 1]);

% find (try to find) limit for rois and find the angle for the image
[L1,R1,U1,D1,L2,R2,U2,D2,L3,R3,U3,D3,angle_degrees] = ShockArea(im105);


%make the target vertical and cut the lines and columns with zeros
     vtarg_ori = imrotate(shockimage,angle_degrees,'bilinear','crop');
     figure(44001), imagesc(vtarg_ori), colormap(jet), title('vtarg_ori'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);

     vtarg_HC = imrotate(im105,angle_degrees,'bilinear','crop');
     figure(44002), imagesc(vtarg_HC), colormap(jet), title('vtarg_HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);

     size(vtarg_ori)
     vtarg_cut_ori = mfunc_cut_partially_black_lines_or_columns(vtarg_ori);
     figure(44003), imagesc(vtarg_cut_ori), colormap(jet), title('vtarg_cut_ori'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
     size(vtarg_cut_ori)
     size(vtarg_HC)
     vtarg_cut_HC = mfunc_cut_partially_black_lines_or_columns(vtarg_HC);
     figure(44004), imagesc(vtarg_cut_HC), colormap(jet), title('vtarg_cut_HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
     size(vtarg_cut_HC)

%find rois again this time with the target on vertical
% if should return angle = 0.0

     [L1,R1,U1,D1,L2,R2,U2,D2,L3,R3,U3,D3,angle_degrees] = ShockArea(vtarg_cut_HC);
     
     size(vtarg_cut_HC);  

     compim = vtarg_cut_ori;
     compim(U1:D1,L1:R1) = vtarg_cut_HC(U1:D1,L1:R1);
%     
     figure(21), imagesc(compim), colormap(gray), title('compim'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
% 
     compim2 = vtarg_cut_ori;
     compim2(U2:D2,L2:R2) = vtarg_cut_HC(U2:D2,L2:R2);
%     
     figure(22), imagesc(compim2), colormap(gray), title('compim2'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
     
     % this is the region of interest to be denoised and its original
     % version (with full noise)
     roi2cut = vtarg_cut_HC(U2:D2,L2:R2);
     ori2cut = vtarg_ori(U2:D2,L2:R2);
     
     
     % reduce the resolution of the image to 1/2 (seems ok for the shock
     % images) 
      roi2cutred=imresize(roi2cut,0.5);
      ori2cutred=imresize(ori2cut,0.5);
     
     
     
WindowSizeHalf = 80;
w1coef = 1;
%step 1
PatchSizeHalf = 3
sigmacoef = 45;
[denoised_image1,acc_weight1] = FNLD2D(roi2cutred,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);
   figure(1111), imagesc(roi2cutred), colormap(gray), title('roi2cut'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1112), imagesc(ori2cutred), colormap(gray), title('ori2cut'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1101), imagesc(denoised_image1), colormap(gray), title('fastNLD'), colorbar, drawnow, caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1102), imagesc(acc_weight1), colormap(gray), title('FastNLDW'), colorbar, drawnow, caxis([0 2]), set(gca,'DataAspectRatio',[1 1 1]);
   figure(1103), imagesc(denoised_image1-roi2cutred), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);

%step 2
PatchSizeHalf = 5
sigmacoef = 180;
[denoised_image2,acc_weight2] = FNLD2D(roi2cutred,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);

   figure(1201), imagesc(denoised_image2), colormap(gray), title('fastNLD'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1202), imagesc(acc_weight2), colormap(gray), title('FastNLDW'), colorbar, drawnow, caxis([0 10]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1203), imagesc(denoised_image2-roi2cutred), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);

%step 3
PatchSizeHalf = 7
sigmacoef = 200;
[denoised_image3,acc_weight3] = FNLD2D(roi2cutred,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);

   figure(1301), imagesc(denoised_image3), colormap(gray), title('fastNLD'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1302), imagesc(acc_weight3), colormap(gray), title('FastNLDW'), colorbar, drawnow, caxis([0 2]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1303), imagesc(denoised_image3-roi2cutred), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);

%step 4
PatchSizeHalf = 11
sigmacoef = 320;
[denoised_image4,acc_weight4] = FNLD2D(roi2cutred,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);

   figure(1401), imagesc(denoised_image4), colormap(gray), title('fastNLD'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1402), imagesc(acc_weight4), colormap(gray), title('FastNLDW'), colorbar, drawnow, caxis([0 2]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1403), imagesc(denoised_image4-roi2cutred), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);

%step 5
PatchSizeHalf = 23
sigmacoef = 1800;
[denoised_image5,acc_weight5] = FNLD2D(roi2cutred,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);

   figure(1501), imagesc(denoised_image5), colormap(gray), title('fastNLD'), colorbar, drawnow; caxis([a-a/10 b-b/10]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1502), imagesc(acc_weight5), colormap(gray), title('FastNLDW'), colorbar, drawnow, caxis([0 20]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1503), imagesc(denoised_image5-roi2cutred), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);


acc_weight = acc_weight1+acc_weight2+acc_weight3+acc_weight4;
%acc_weight = acc_weight5;
denoised_image = (denoised_image1.*acc_weight1+denoised_image2.*acc_weight2+denoised_image3.*acc_weight3+denoised_image4.*acc_weight4)./acc_weight;
%denoised_image = (denoised_image5.*acc_weight5)./acc_weight;

ndlnoise = denoised_image - roi2cutred;

M=max(max(denoised_image))
m=min(min(denoised_image))
a = m+(M-m)*0.15;
b = M-(M-m)*0.15;

   figure(1001), imagesc(denoised_image), colormap(gray), title('fastNLD'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1002), imagesc(acc_weight), colormap(gray), title('FastNLDW'), colorbar, drawnow, caxis([0 2]),set(gca,'DataAspectRatio',[1 1 1]);
   figure(1003), imagesc(ndlnoise), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
   

HC_image = im105;
FNLD_Image_roi2 = denoised_image;
FNLD_weight_roi2 = acc_weight;
FNLD_noise_roi2 = ndlnoise;

composit = compim;
denoised_bit = imresize(denoised_image,[D2-U2+1 R2-L2+1]);

composit(U2:D2,L2:R2) = denoised_bit;

OverDone_FNLD_Image_roi2 = denoised_image5;

end

