function [denoised_image] = G14_FilterPackInspector2(filterpackimage)

% function [HC_image,FNLD_Image,FNLD_weight,totRemNoise] = G14_FilterPackInspector(filterpackimage)

% function [HC_image,FNLD_Image,FNLD_weight,totRemNoise, OverDone_FNLD_Image] = G14_FilterPackInspector(filterpackimage)
% automatic treatment of filter-pack images for Gemini 2014
%   fast and dirthy 
tic
fprintf('step %i\n', 101); 
im101 = HairCut2D(filterpackimage,3,11,4);
im101 = HairCut2D(im101,3,11,3.0);
im101 = HairCut2D(im101,3,11,2.5);
im101 = HairCut2D(im101,3,11,2.0);
im101 = HairCut2D(im101,3,11,1.75);
toc
[logstepnoise,stepnoise,fullnoise,a,b,c,d,e,f,g,h] = dothemath(im101,filterpackimage,filterpackimage);
figure(1), imagesc(filterpackimage), colormap(gray), title('original'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1011), imagesc(im101), colormap(gray), title('101HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1012), imagesc(logstepnoise), colormap(lines), title('101logstepnoise'), colorbar, drawnow; caxis([c d]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1013), imagesc(stepnoise), colormap(jet), title('101stepnoise'), colorbar, drawnow; caxis([0 1000]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1014), imagesc(fullnoise), colormap(jet), title('101fullnoise'), colorbar, drawnow; caxis([g h]),set(gca,'DataAspectRatio',[1 1 1]);

%tic
fprintf('step %i\n', 102);
im102 = HairCut2D(im101,2,7,2.2);
im102 = HairCut2D(im102,2,7,2.0);
im102 = HairCut2D(im102,2,7,1.8);
im102 = HairCut2D(im102,2,7,1.6);
im102 = HairCut2D(im102,2,7,1.4);
im102 = HairCut2D(im102,2,7,1.3);
toc
[logstepnoise,stepnoise,fullnoise,a,b,c,d,e,f,g,h] = dothemath(im102,im101,filterpackimage);
figure(1), imagesc(filterpackimage), colormap(gray), title('original'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1011), imagesc(im102), colormap(gray), title('102HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1012), imagesc(logstepnoise), colormap(lines), title('102logstepnoise'), colorbar, drawnow; caxis([c d]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1013), imagesc(stepnoise), colormap(jet), title('102stepnoise'), colorbar, drawnow; caxis([0 800]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1014), imagesc(fullnoise), colormap(jet), title('102fullnoise'), colorbar, drawnow; caxis([g h]),set(gca,'DataAspectRatio',[1 1 1]);

%tic
fprintf('step %i\n', 103);
im103 = HairCut2D(im102,1,5,1.40);
im103 = HairCut2D(im103,1,5,1.35);
im103 = HairCut2D(im103,1,5,1.30);
im103 = HairCut2D(im103,1,5,1.275);
im103 = HairCut2D(im103,1,5,1.25);
im103 = HairCut2D(im103,1,5,1.24);
toc
[logstepnoise,stepnoise,fullnoise,a,b,c,d,e,f,g,h] = dothemath(im103,im102,filterpackimage);
figure(1), imagesc(filterpackimage), colormap(gray), title('original'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1011), imagesc(im103), colormap(gray), title('103HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1012), imagesc(logstepnoise), colormap(lines), title('103logstepnoise'), colorbar, drawnow; caxis([c d]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1013), imagesc(stepnoise), colormap(jet), title('103stepnoise'), colorbar, drawnow; caxis([0 400]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1014), imagesc(fullnoise), colormap(jet), title('103fullnoise'), colorbar, drawnow; caxis([g h]),set(gca,'DataAspectRatio',[1 1 1]);

%tic
fprintf('step %i\n', 104);
im104 = HairCut2D(im103,0,3,1.23);
im104 = HairCut2D(im104,0,3,1.22);
im104 = HairCut2D(im104,0,3,1.21);
im104 = HairCut2D(im104,0,3,1.20);
im104 = HairCut2D(im104,0,3,1.19);
im104 = HairCut2D(im104,0,3,1.18);
toc
[logstepnoise,stepnoise,fullnoise,a,b,c,d,e,f,g,h] = dothemath(im104,im103,filterpackimage);
figure(1), imagesc(filterpackimage), colormap(gray), title('original'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1011), imagesc(im104), colormap(gray), title('104HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1012), imagesc(logstepnoise), colormap(lines), title('104logstepnoise'), colorbar, drawnow; caxis([c d]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1013), imagesc(stepnoise), colormap(jet), title('104stepnoise'), colorbar, drawnow; caxis([0 300]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1014), imagesc(fullnoise), colormap(jet), title('104fullnoise'), colorbar, drawnow; caxis([g h]),set(gca,'DataAspectRatio',[1 1 1]);

%tic
fprintf('step %i\n', 105);
im105 = HairCut2D(im104,0,2,1.20);
im105 = HairCut2D(im105,0,3,1.15);
im105 = HairCut2D(im105,0,2,1.10);
im105 = HairCut2D(im105,0,2,1.08);
im105 = HairCut2D(im105,0,2,1.06);
im105 = HairCut2D(im105,0,2,1.05);
toc
[logstepnoise,stepnoise,fullnoise,a,b,c,d,e,f,g,h] = dothemath(im105,im104,filterpackimage);
figure(1), imagesc(filterpackimage), colormap(gray), title('original'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1011), imagesc(im105), colormap(gray), title('105HC'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%figure(1012), imagesc(logstepnoise), colormap(lines), title('105logstepnoise'), colorbar, drawnow; caxis([c d]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1013), imagesc(stepnoise), colormap(jet), title('105stepnoise'), colorbar, drawnow; caxis([0 250]),set(gca,'DataAspectRatio',[1 1 1]);
figure(1014), imagesc(fullnoise), colormap(jet), title('105fullnoise'), colorbar, drawnow; caxis([g h]),set(gca,'DataAspectRatio',[1 1 1]);
denoised_image = im105;
% im105 = filterpackimage;
% WindowSizeHalf = 20;
% 
% w1coef = 1;
% %step 1
% PatchSizeHalf = 3
% sigmacoef = 18;
% [denoised_image1,acc_weight1] = FNLD2D(im105,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);
% 
%    %figure(1101), imagesc(denoised_image1), colormap(gray), title('fastNLD'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%    %figure(1102), imagesc(acc_weight1), colormap(gray), title('FastNLDW'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
%    %figure(1103), imagesc(denoised_image1-im105), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
% 
% %step 2
% PatchSizeHalf = 5
% sigmacoef = 75;
% [denoised_image2,acc_weight2] = FNLD2D(im105,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);
% 
%    %figure(1201), imagesc(denoised_image2), colormap(gray), title('fastNLD'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%    %figure(1202), imagesc(acc_weight2), colormap(gray), title('FastNLDW'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
%    %figure(1203), imagesc(denoised_image2-im105), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
% 
% %step 3
% PatchSizeHalf = 7
% sigmacoef = 130;
% [denoised_image3,acc_weight3] = FNLD2D(im105,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);
% 
%    %figure(1301), imagesc(denoised_image3), colormap(gray), title('fastNLD'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%    %figure(1302), imagesc(acc_weight3), colormap(gray), title('FastNLDW'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
%    %figure(1303), imagesc(denoised_image3-im105), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
% 
% %step 4
% PatchSizeHalf = 11
% sigmacoef = 220;
% [denoised_image4,acc_weight4] = FNLD2D(im105,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);
% 
%    %figure(1401), imagesc(denoised_image4), colormap(gray), title('fastNLD'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%    %figure(1402), imagesc(acc_weight4), colormap(gray), title('FastNLDW'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
%    %figure(1403), imagesc(denoised_image4-im105), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
% 
% acc_weight = acc_weight1+acc_weight2+acc_weight3+acc_weight4;
% denoised_image = (denoised_image1.*acc_weight1+denoised_image2.*acc_weight2+denoised_image3.*acc_weight3+denoised_image4.*acc_weight4)./acc_weight;
% ndlnoise = im105 - denoised_image;
% 
% M=max(max(denoised_image))
% m=min(min(denoised_image))
% a = m+(M-m)*0.15;
% b = M-(M-m)*0.15;
% 
%    figure(1001), imagesc(denoised_image), colormap(gray), title('fastNLD'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%    figure(1002), imagesc(acc_weight), colormap(gray), title('FastNLDW'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
%    figure(1003), imagesc(ndlnoise), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
%    
%  HC_image = im105;
%  FNLD_Image = denoised_image;
%  FNLD_weight = acc_weight;  
%  totRemNoise = denoised_image-filterpackimage; 
%  
%  denoised_image = FNLD_Image;
 
%  %overdoing
% %step 2r
% PatchSizeHalf = 5
% sigmacoef = 80;
% [denoised_image2,acc_weight2] = FNLD2D(denoised_image,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);
% %step 3r
% PatchSizeHalf = 7
% sigmacoef = 135;
% [denoised_image3,acc_weight3] = FNLD2D(denoised_image,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);
% %step 4r
% PatchSizeHalf = 11
% sigmacoef = 225;
% [denoised_image4,acc_weight4] = FNLD2D(denoised_image,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);
% 
% denoised_image = (denoised_image2+denoised_image3+denoised_image4)/3;
% %ndlnoise = bb - denoised_image;
% %acc_weight = acc_weight1+acc_weight2+acc_weight3+acc_weight4;
% 
% acc_weight = acc_weight2+acc_weight3+acc_weight4;
% denoised_image = (denoised_image2.*acc_weight2+denoised_image3.*acc_weight3+denoised_image4.*acc_weight4)./acc_weight;
% ndlnoise = im105 - denoised_image;
% 
% M=max(max(denoised_image))
% m=min(min(denoised_image))
% 
%    figure(2001), imagesc(denoised_image), colormap(gray), title('fastNLD2'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%    figure(2002), imagesc(acc_weight), colormap(gray), title('FastNLDW2'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
%    figure(2003), imagesc(ndlnoise), colormap(gray), title('nld noise2'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
%     
% %step 3
% PatchSizeHalf = 7
% sigmacoef = 140;
% [denoised_image3,acc_weight3] = FNLD2D(denoised_image,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);
% %step 4
% PatchSizeHalf = 11
% sigmacoef = 230;
% [denoised_image4,acc_weight4] = FNLD2D(denoised_image,WindowSizeHalf,PatchSizeHalf,w1coef,sigmacoef);
% 
% acc_weight = acc_weight3+acc_weight4;
% denoised_image = (denoised_image3.*acc_weight3+denoised_image4.*acc_weight4)./acc_weight;
% ndlnoise = im105 - denoised_image;
% 
% M=max(max(denoised_image))
% m=min(min(denoised_image))
% 
% 
%    figure(3001), imagesc(denoised_image), colormap(gray), title('fastNLD'), colorbar, drawnow; caxis([a b]),set(gca,'DataAspectRatio',[1 1 1]);
%    figure(3002), imagesc(acc_weight), colormap(gray), title('FastNLDW'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]);
%    figure(3003), imagesc(ndlnoise), colormap(gray), title('nld noise'), colorbar, drawnow,set(gca,'DataAspectRatio',[1 1 1]); 
%    
% OverDone_FNLD_Image = denoised_image;



end

