function [logstepnoise,stepnoise,fullnoise,a,b,c,d,e,f,g,h] = dothemath(lastdenoise,previousdenoise,original)

        logstepnoise = log(abs(lastdenoise - previousdenoise+1));
        stepnoise  = previousdenoise - lastdenoise;
        fullnoise  = original - lastdenoise;
        m = min(min(lastdenoise));
        M = max(max(lastdenoise));
        a = m + (M-m)*0.15;
        b = M - (M-m)*0.15;
        m = min(min(logstepnoise));
        M = max(max(logstepnoise))/2;
        c = m + (M-m)*0.2;
        d = M - (M-m)*0.2;
        m = min(min(stepnoise));
        M = max(max(stepnoise))/2; 
        e = m + (M-m)*0.2;
        f = M - (M-m)*0.2;
        m = min(min(fullnoise));
        M = max(max(fullnoise))/5; 
        g = m + (M-m)*0.2;
        h = M - (M-m)*0.2;
end