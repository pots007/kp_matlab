function d2b = single2bin(val, stroutputflag)
if nargin == 1
    stroutputflag = 0;
end
a = val;
n = 16;         % number bits for integer part of your number      
m = 20;         % number bits for fraction part of your number
% binary number

d2b = [ fix(rem(fix(a)*pow2(-(n-1):0),2)), fix(rem( rem(a,1)*pow2(1:m),2))];

if stroutputflag
    
    d2b = mat2str(d2b');
end