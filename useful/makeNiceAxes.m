% makeNiceAxes.m
%
% Make nixe axes, the way I prefer them.

function axx = makeNiceAxes(hfig, pos)
axx = axes('Parent', hfig, 'NextPlot', 'add', 'Box', 'on', 'Linewidth', 2);
if nargin~=1 && ~isempty(pos)
    set(axx, 'Position', pos);
end
