function tout = SetSmartTimeUnit(timin)
t = timin;
if (log10(t)<-12)
    t = t*1e15;
    tunit = 'fs';
elseif (log10(max(t))<-9)
    t = t*1e12;
    tunit = 'ps';
elseif (log10(max(t))<-6)
    t = t*1e9;
    tunit = 'ns';
elseif (log10(max(t))<-3)
    t = t*1e6;
    tunit = 'us';
elseif (log10(max(t))<0)
    t = t*1e3;
    tunit = 'ms';
else
    t = t;
    tunit = 's';
end
tout.t = t;
tout.unit = tunit;