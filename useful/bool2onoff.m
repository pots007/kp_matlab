
function s = bool2onoff(v)
if length(v)==1
    if v
        s = 'on';
    else
        s = 'off';
    end
else
    % Update April 2016 to accept matrices
    v = logical(v);
    s = cell(size(v));
    [s{~v}] = deal('off');
    [s{v}] = deal('on');
end

