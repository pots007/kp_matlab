%My plotyy to not have to use matlab's inbuilt one. should end up nicer.

function q = myplotyy(ax, varargin)
if (nargin < 4)
    disp('Needs two y datasets')
    return;
elseif nargin >= 4
    x = varargin{1};
    y1 = varargin{2};
    y2 = varargin{3};
end
opt1 = '-k'; opt2 = '-r';
if nargin == 5
    opt1 = varargin{4};
elseif nargin == 6
    opt2 = varargin{5};
end
%Remove all other axes except base - it's a mess but maybe works
axs = findobj('Type', 'Axes', 'Parent', get(ax, 'Parent'));
for i=1:length(axs)
    if axs(i)~=ax
        delete(axs(i)) 
    end
end
h1 = plot(ax, x, y1, opt1);
ax2 = axes('Units', get(ax, 'Units'), 'Position', get(ax, 'Position'), ...
    'Parent', get(ax, 'Parent'),...
    'Color', 'none', 'Box', 'off', 'YAxisLocation', 'right', ...
    'XAxisLocation', 'top', 'XColor', 'k', 'YColor', opt2(end));
hold on;
h2 = plot(ax2, x,y2, opt2);
hold off;
set(ax2, 'XTick', []);
set(ax2, 'XLim', get(ax, 'XLim'));
q.ax1 = ax; q.ax2 = ax2; 
q.h1 = h1; q.h2 = h2;
