%function reportfigure(hfig, type, format, filen)

function reportfigure(hfig, type, format, filen)
[pathstr,~,~] = fileparts(which('make_path_Kris.m'));
[dpath,~,~] = fileparts(pathstr);
%reportfol = 'D:\Kristjan\Documents\�likool\Dropbox\Transfer_report\';
currentfol = 'Goa_presentation';
reportfol = [dpath '/' currentfol '/'];
ax = get(hfig, 'CurrentAxes');
%ax = gca;
switch (type)
    case 'halfpage'
        set(hfig, 'Position', [100 100 600 450]);
        fsiz = 14;
    case 'fullwidth'
        set(hfig, 'Position', [100 100 800 600]);
        fsiz = 14;
    otherwise
        disp('Not a valid figure type');
        return;
end
set(ax, 'FontSize', fsiz);
set(get(ax, 'YLabel'), 'FontSize', fsiz);
set(get(ax, 'XLabel'), 'FontSize', fsiz);
%save without title, but put it back afterwards.
titl = get(get(ax, 'Title'), 'String');
set(get(ax, 'Title'), 'String',  '');
if (~isempty(findobj(get(hfig,'Children'),'Tag','Colorbar')))
    set(findobj(get(hfig,'Children'),'Tag','Colorbar'), 'FontSize', fsiz);
    try
        set(get(findobj(get(hfig,'Children'),'Tag','Colorbar'), 'YLabel'), ...
            'FontSize', fsiz);
    catch
        
    end
end
%Do the saving
if strcmp(format, 'pdf')
    export_fig([reportfol filen '.pdf'], '-pdf', ...
        '-nocrop', '-transparent', hfig);
else if strcmp(format, 'png')
        export_fig([reportfol filen '.png'], '-png', ...
            '-nocrop', '-transparent', hfig);
        
    else
        disp('Format not supported');
    end
end
title(titl);
end