% makeCheckerboard.m
%
% Plot a checkerboard on an axes.

hfig = figure(8934285); clf(hfig);
ax = gca;
N = [5,50];
C = invhilb(50) < 0;
C = C(1:5,:);
imagesc(C);
colormap(gray)
axis off;
set(gca, 'Position', [0 0 1 1]);
set(hfig, 'Position', [100 100 N(2)*40 N(1)*40]);
savfol = fullfile('/home/kpoder', 'DESYcloud', 'Experiments', '2017_EspecUpgrade');
export_fig(fullfile(savfol, 'checkerboard'), '-pdf', '-nocrop', hfig);