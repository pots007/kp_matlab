% Return 1 is number is odd

function ret = isodd(num)
ret = logical(mod(num,2));