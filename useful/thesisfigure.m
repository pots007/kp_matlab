%function thesisfigure(hfig, type, filen, chapter, format, [figheigth])

function thesisfigure(hfig, type, filen, chapter, format, figheight, renderer)
if nargin == 4
    format = 'pdf';
end
if nargin ~= 6
    figheight = 450;
end
if nargin ~= 7
    renderer = 'painters';
end
savefol = fullfile(getDropboxPath, 'Writing', 'Thesis', chapter);

ax = get(hfig, 'CurrentAxes');
%set(ax, 'Box', 'on', 'LineWidth', 2);
%ax = gca;
switch (type)
    case 'halfpage'
        set(hfig, 'Position', [100 100 600 figheight], 'Color', 'w');
        fsiz = 22;
    case 'fullwidth'
        set(hfig, 'Position', [100 100 1000 figheight], 'Color', 'w');
        fsiz = 18;
    otherwise
        disp('Not a valid figure type');
        return;
end
make_latex(handle(hfig)); setAllFonts(hfig, fsiz);
%save without title, but put it back afterwards.
titl = get(get(ax, 'Title'), 'String');
title(ax, '');
format = ['-' format];
if strcmp(format, '-pdf') || strcmp(format, '-png')
    %export_fig(fullfile(savefol, filen), format, '-nocrop', ['-' renderer], hfig);
    disp('Thesis is done, so not overwriting previously saved files!!!');
else
    disp('Format not supported');
end
title(titl);
end