classdef AxisScaleContextMenu < hgsetget
    properties
        menu
        X
        Y
        lw
        marker
        XY % both
        C
        CM
        equal_aspect_ratio
    end
    
    methods 
        function obj = AxisScaleContextMenu(parent)
            if ~exist('parent', 'var')
                parent = gcf;
            end
            obj.menu = uicontextmenu('Parent', parent, 'Callback', @(s, e) callback(obj));
            obj.X = obj.makeXYAxis(uimenu(obj.menu, 'Label', 'X axis'), 'X');
            obj.Y = obj.makeXYAxis(uimenu(obj.menu, 'Label', 'Y axis'), 'Y');
            obj.lw = obj.selectLineWidth(uimenu(obj.menu, 'Label', 'set LineWidth'));
            obj.marker = obj.selectMarker(uimenu(obj.menu, 'Label', 'set Marker'));
            uimenu(obj.menu,'Label','Set X&Y limits auto','Callback',@(s,e)setXYAuto(obj));
            uimenu(obj.menu,'Label','Set X&Y limits tight','Callback',@(s,e)setXYTight(obj));
            obj.equal_aspect_ratio=uimenu(obj.menu, 'Label', 'Equal aspect ratio', 'Callback', @(s, e) equalAspectRatioCallback(obj));
            obj.C = obj.makeCAxis(uimenu(obj.menu, 'Label', 'Color axis'));
            obj.CM = obj.selectColormap(uimenu(obj.menu, 'Label', 'Colormap'));
        end
        function setXYTight(obj)
            lims=objbounds(findall(gca));
            set(gca,'XLim',lims([1 2]),'YLim',lims([3 4]));
        end
        function setXYAuto(obj)
            set(gca,'XLimMode','auto','YLimMode','auto');
        end        
        function equalAspectRatioCallback(obj)
            if onoff2bool(get(obj.equal_aspect_ratio,'Checked'))
                set(gca,'DataAspectRatioMode','auto','PlotBoxAspectRatioMode','auto');
                set(obj.equal_aspect_ratio, 'Checked', 'off');
            else
                p=getPositionInUnits(gca,'pixels');
                set(gca,'DataAspectRatio',[1 1 1],'PlotBoxAspectRatio',[1,p(4)/p(3),1]);
                set(obj.equal_aspect_ratio, 'Checked', 'on');
            end
        end        
        function manualCallback(obj, axis)
            if onoff2bool(get(obj.(axis).manual, 'Checked'))
                set(gca, [axis 'LimMode'], 'auto');
                set(obj.(axis).manual, 'Checked', 'off');
            else
                set(gca, [axis 'LimMode'], 'manual');
                set(obj.(axis).manual, 'Checked', 'on');
            end
        end
        
        function limitsCallback(obj, axis)
            axisLimitAdjusterDlg(gca, axis);
        end
        
        function limitsCallback_minmax(obj, minmax)
            set(gca, 'CLim', minmax)
        end
        
        
        function logCallback(obj, axis)
           if onoff2bool(get(obj.(axis).log, 'Checked'))
                set(gca, [axis 'Scale'], 'linear');
                %set(obj.(axis).log, 'Checked', 'off');
            else
                set(gca, [axis 'Scale'], 'log');
                %set(obj.(axis).log, 'Checked', 'on');
            end 
        end
        
        function callback(obj)
            obj.setupXYAxis(obj.X, 'X');
            obj.setupXYAxis(obj.Y, 'Y');
            obj.setupCAxis(obj.C);
            set(obj.equal_aspect_ratio,'Checked',bool2onoff(strcmp(get(gca,'DataAspectRatioMode'),'manual')));
        end
   
        function h = makeXYAxis(obj, m, axis)
            h.menu = m;
            h.manual = uimenu(m, 'Label', 'Manual', 'Callback', @(s, e) manualCallback(obj, axis));
            h.lim = uimenu(m, 'Label', 'Limits ...', 'Callback', @(s, e) limitsCallback(obj, axis));
            h.log = uimenu(m, 'Label', 'Logarithmic', 'Callback', @(s, e) logCallback(obj, axis));
        end
        
        function h = selectLineWidth(obj, m)
            h.lw_1 = uimenu(m, 'Label', '1', 'Callback', @(s,e)setLineWidth(obj, 1));
            h.lw_2 = uimenu(m, 'Label', '2', 'Callback', @(s,e)setLineWidth(obj, 2));
            h.lw_3 = uimenu(m, 'Label', '3', 'Callback', @(s,e)setLineWidth(obj, 3));
            h.lw_4 = uimenu(m, 'Label', '3', 'Callback', @(s,e)setLineWidth(obj, 4));
        end
        function setLineWidth(obj, lw)
            c=get(gca, 'Children');
            set(c, 'LineWidth', lw)
        end
        function h = selectMarker(obj, m)
            h.marker_none = uimenu(m, 'Label', 'none', 'Callback', @(s,e)setMarker(obj, 'none'));
            h.marker_1    = uimenu(m, 'Label', '.', 'Callback', @(s,e)setMarker(obj, '.'));
            h.marker_2    = uimenu(m, 'Label', 'o', 'Callback', @(s,e)setMarker(obj, 'o'));
        end
        function setMarker(obj, marker)
            c=findobj(gca,'Type','line');
            %c=get(gca, 'Children');
            set(c, 'Marker', marker)
        end
        
        
        function h = makeCAxis(obj, m)
            h.menu = m;
            h.manual = uimenu(m, 'Label', 'Manual', 'Callback', @(s, e) manualCallback(obj, 'C'));
            h.lim = uimenu(m, 'Label', 'Limits ...', 'Callback', @(s, e) limitsCallback(obj, 'C'));
            h.lim_01    = uimenu(m, 'Label', '0 -- 1', 'Callback', @(s, e) limitsCallback_minmax(obj, [0 1]));
            h.lim_08bit = uimenu(m, 'Label', '0 -- 08 bit', 'Callback', @(s, e) limitsCallback_minmax(obj, [0 2^8]));
            h.lim_10bit = uimenu(m, 'Label', '0 -- 10 bit', 'Callback', @(s, e) limitsCallback_minmax(obj, [0 2^10]));
            h.lim_12bit = uimenu(m, 'Label', '0 -- 12 bit', 'Callback', @(s, e) limitsCallback_minmax(obj, [0 2^12]));
            h.lim_14bit = uimenu(m, 'Label', '0 -- 14 bit', 'Callback', @(s, e) limitsCallback_minmax(obj, [0 2^14]));
            h.lim_16bit = uimenu(m, 'Label', '0 -- 16 bit', 'Callback', @(s, e) limitsCallback_minmax(obj, [0 2^16]));
            
        end
        
        function h = selectColormap(obj, m)
            h.menu = m;
            h.cm=[];
            h.cm(end+1) = uimenu(m, 'Label', 'jet',      'Callback', @(s,e)set_colormap(obj, 'jet'));
            h.cm(end+1) = uimenu(m, 'Label', 'jet_white','Callback', @(s,e)set_colormap(obj, 'jet_white')); 
            h.cm(end+1) = uimenu(m, 'Label', 'jet_black','Callback', @(s,e)set_colormap(obj, 'jet_black'));  
            h.cm(end+1) = uimenu(m, 'Label', 'hot',      'Callback', @(s,e)set_colormap(obj, 'hot'));  
            h.cm(end+1) = uimenu(m, 'Label', 'hot_white','Callback', @(s,e)set_colormap(obj, 'hot_white'));  
            h.cm(end+1) = uimenu(m, 'Label', 'fireice',  'Callback', @(s,e)set_colormap(obj, 'fireice'));
            h.cm(end+1) = uimenu(m, 'Label', 'cool',     'Callback', @(s,e)set_colormap(obj, 'cool'));  
            h.cm(end+1) = uimenu(m, 'Label', 'bone',     'Callback', @(s,e)set_colormap(obj, 'bone'));  
            h.cm(end+1) = uimenu(m, 'Label', '1-bone',   'Callback', @(s,e)set_colormap(obj, '1-bone'));  
        end
        function set_colormap(obj, cm)
            switch cm
                case 'jet_white' % custom colormaps first
                    load('owncolourmaps');
                    colormap(colmap.jet_white)
                case 'jet_black'
                    load('owncolourmaps');
                    colormap(colmap.jet_black)
                case 'hot_white'
                    load('owncolourmaps');
                    colormap(colmap.hot_white)
                case 'fireice'
                    load('owncolourmaps');
                    colormap(colmap.fireice)
                case '1-bone'
                    colormap(1-bone)
                otherwise
                    colormap(cm) % set a matlab standard colormap
            end
        end
        
    end
    
    methods (Static)        
        function setupXYAxis(h, axis)
            set(h.manual, 'Checked', bool2onoff(strcmp(get(gca, [axis 'LimMode']), 'manual')));
            set(h.log, 'Checked', bool2onoff(strcmp(get(gca, [axis 'Scale']), 'log')));
        end
        
        function setupCAxis(h)
            set(h.manual, 'Checked', bool2onoff(strcmp(get(gca, 'CLimMode'), 'manual')));
        end
    end
end