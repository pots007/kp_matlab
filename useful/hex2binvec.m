function out = hex2binvec(inp)
%inp = '0cd'
if ~ischar(inp)
    error('Not a char array')
end
l = length(inp);
out = char(zeros(4*l,1));
for i=1:l
    out((i-1)*4+1:i*4) = hex2bin(inp(i));
end
out = out';