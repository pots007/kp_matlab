% Quickie emulation of the imfamous ImageJ control+K lineout function.
% If you have an image plotted, calling controlK returns both the lineout
% data and plots the lineout in a new figure window.
% Can do control+alt+K by changing dir to 2:
% dir = 1 bins vertically,
% dir = 2 bins horizontally.
% Can also plot against actual figure pixels instead of pixel in bin by
% setting figunits = true.

function [out,xax] = controlK(dir, figunits, fig)
if nargin == 0
    fig = gcf;
    dir = 1;
    figunits = false;
elseif nargin == 1
    fig = gcf;
    figunits = false;
elseif nargin == 2
    fig = gcf;
end
hdat = findobj(get(gca(fig), 'Children'), 'Type', 'image');
cdata = get(hdat, 'CData');
r = getrect(fig);
r = round(r);
if r(1) < 1; r(1) = 1; end
if r(2) < 1; r(2) = 1; end
if r(3)+r(1) > size(cdata,2); r(3) = size(cdata,2)-r(1); end
if r(4)+r(2) > size(cdata,1); r(4) = size(cdata,1)-r(2); end
subframe = cdata(r(2):r(2)+r(4),r(1):r(1)+r(3)); 
out = sum(subframe,dir)/size(subframe,dir);
f2 = figure;
ax = axes('Parent', f2);
if figunits
    xax = linspace(r(dir), r(dir) + r(dir+2), length(out));
else
    xax = linspace(1,length(out), length(out));
end
plot(ax, xax, out);