% relayServer.m
%
% Small unit to relay data between the pump and other things in the world
%
% Mainly written for our little scrollpump, so uses CR as terminator
%
% Accepts all connections on port 30000

classdef relayServer < handle
    
    properties
        clientCon
        serverCon
    end
    
    methods
        function o = relayServer(clientIP, clientPort)
            
            o.clientCon = tcpip(clientIP, clientPort, 'BytesAvailableFcnMode',...
                'terminator', 'BytesAvailableFcn',...
                @o.incomingFcn, 'Terminator', 'CR');
            o.serverCon = tcpip('0.0.0.0', 30000, 'NetworkRole', 'Server',...
                'BytesAvailableFcnMode', 'terminator',... 
                'BytesAvailableFcn', @o.queryFcn);
            fopen(o.clientCon);
            fprintf('Machine connection open, waiting for incoming...\n');
            fopen(o.serverCon);
            fprintf('Someone connected...\n');
        end
        
        function incomingFcn(o,~,~)
            % Executed when bytes are received from the device
            data = fgetl(o.clientCon);
            fprintf(o.serverCon, data);
        end
        
        function queryFcn(o,~,~)
            % Executed when bytes are received from someone asking for data
            data = fgetl(o.serverCon);
            fprintf(o.clientCon, data);
        end
        
        function delete(o)
            fclose(o.clientCon);
            fclose(o.serverCon);
        end
    end
end
