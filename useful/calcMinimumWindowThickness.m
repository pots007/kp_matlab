% calcMinimumWindowThickness.m
%
% Returns minimum thickness for a round vacuum window, given it is round.
%
% All inputs in SI.

function t = calcMinimumWindowThickness(deltaP, radius, safetyfactor, material)

% Parse arguments....
if isempty(deltaP); deltaP = 101325; end
if isempty(radius); radius = 0.01; end
if nargin < 3
    if isempty(safetyfactor); safetyfactor = 5; end
    material = 'fused silica';
end
if nargin < 4 || isempty(material)
    material = 'fused silica';
end

% Select material
% List from http://www.ispoptics.com/articles/8/DESIGN%20OF%20PRESSURE%20WINDOW
% NB modulus of rupture in units of PSI - need to convert later.
matList = {'BaF2',  3900;...
    'CaF2',         5300;...
    'CdTe',         3200;...
    'CsI',          820;...
    'Fused Silica',	7000;...
    'GaAs',         20000;...
    'Ge',           10500;...
    'KBr',          160;...
    'KCl',          340;...
    'KRS-5',        4000;...
    'LiF',          1600;...
    'MgF2',         7200;...
    'NaCl',         350;...
    'Sapphire', 	65000;...
    'Si',           18100;...
    'ZnSe',         8000;...
    'ZnS',          14900;...
    'ZnS Cleartran',8700};

ind = cellfun(@(x) ~isempty(strfind(upper(x), upper(material))), matList(:,1));

t = sqrt(1.1*deltaP*radius.^2*safetyfactor/(matList{ind,2}*6894.76));