%Gives parameters for ISO large pitch threads
%Good ol' Wikipedia to thank for

function thread = GetISOThread(diameter, type)
%diameter = 33;
%type = 'coarse';
Dmaj = diameter;

threads = [1.0 0.25 0.20;
1.2 0.25 0.20;
1.4 0.30 0.20;
1.6 0.35 0.20;
1.8 0.35 0.20;
2.0 0.40 0.25;
2.5 0.45 0.35;
3.0 0.50 0.35;
3.5 0.60 0.35;
4.0 0.70 0.50;
5.0 0.80 0.50;
6.0 1.00 0.75;
7.0 1.00 0.75;
8.0 1.25 0.00;
10.0 1.50 0.00;
12.0 1.75 0.00;
14.0 2.00 1.50;
16.0 2.00 1.50;
18.0 2.50 0.00;
20.0 2.50 0.00;
22.0 2.50 0.00;
24.0 3.00 2.00;
27.0 3.00 2.00;
30.0 3.50 2.00;
33.0 3.50 2.00;
36.0 4.00 3.00;
39.0 4.00 3.00;
42.0 4.50 3.00;
45.0 4.50 3.00;
48.0 5.00 3.00;
52.0 5.00 4.00;
56.0 5.50 4.00;
60.0 5.50 4.00;
64.0 6.00 4.00];

inde = sum((threads(:,1)==Dmaj).*((1:size(threads,1))'));
if (inde == 0)
    disp('Not a standard diameter. Options are:')
    fprintf('%1.1f \n', threads(:,1));
    return;
end
if (strcmp(type, 'fine'))
    P = threads(inde,3);
elseif (strcmp(type, 'coarse'))
    P = threads(inde,2);
else
    disp('Unknown thread type. Options are ,fine, or ,coarse,');
    return;
end
H = cosd(30)*P;
Dmin = Dmaj - 2*5*H/8;
Dp = Dmaj - 2*3*H/8;
thread.Dmaj = Dmaj;
thread.Dmin = Dmin;
thread.Dp = Dp;
thread.pitch = P;
thread.height = H;

