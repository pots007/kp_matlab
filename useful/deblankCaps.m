% deblankCaps.m
%
% Deblank and make the start of every word capital

function strout = deblankCaps(strin)
displayName = allwords(strtrim(strin));
for k=1:length(displayName); displayName{k}(1) = upper(displayName{k}(1)); end
strout = strjoin(displayName, '');
