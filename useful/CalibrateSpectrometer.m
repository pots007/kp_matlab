% tries fitting wavelengths to all found peaks
% Fits third order poly to peaks and chooses best fit.
% Finally returns spline for that fit

function q = CalibrateSpectrometer(data, lamp)
switch upper(lamp)
    case 'HG'
        lines = [365.48 404.65 435.83 507.1 546.075];
    case 'AR'
        
    otherwise
        disp('No lines for this lamp');
        return;
end
if ischar(data)
    data = imread(data);
end
figure(9090)
if (size(data,1)~=1 || size(data,2)~=1) 
    imagesc(data)
    disp('Select box to average')
    r = getrect(9090); x = round(r(1)); y = round(r(2)); w = round(r(3)); h = round(r(4));
    pixax = x:1:(x+w);
    pixels = mean(data(y:y+h,x:x+w));
    plot(pixax, pixels)
else
    plot(data)
end
[~,y] = ginput(1);
[pks, locs] = findpeaks(pixels, 'minpeakheight', y, 'minpeakdistance', 20);
locs = locs + x;
totpos = nchoosek(locs, length(lines));
maxr = 0;
for i=1:size(totpos,1)
    [f,gof] = fit(totpos(i,:)', lines', 'poly3');
    %fprintf('%2.4f \t %2.4f\n', gof.rsquare, gof.adjrsquare);
    if (gof.adjrsquare > maxr) ind = i; end
end
totpos(ind,:)
%f = fit(totpos(ind,:)', lines', 'poly3');
f = fit(totpos(ind,:)', lines', 'cubicspline');
q.pixel = 1:1:size(data,2);
q.lambda = feval(f,q.pixel)';
plot(q.pixel, q.lambda);
hold on;
plot(totpos(ind,:), lines, 'xr');
hold off;