% parseOFX.m
%
% A silly function to try to extract useful data from Natwest statements
% Shoddy way of reading XML files
% Very sorry about this, future me.
%
% Written in Jan 2017

function fdat = parseOFX(filename)
% First check if file exists and whatnot
if ~exist(filename, 'file'); error('No file exists'); end;
% Now get all of the file:
ffil = getFileText(filename);


% First find all accounts this statement covers: STMTRS
lIDs = cellfun(@(x) strfind(x, '<STMTRS>'), ffil, 'UniformOutput', false);
lIDss = ~cellfun(@isempty, lIDs').*(1:length(ffil));
lIDst = lIDss(lIDss~=0);
lIDs = cellfun(@(x) strfind(x, '</STMTRS>'), ffil, 'UniformOutput', false);
lIDss = ~cellfun(@isempty, lIDs').*(1:length(ffil));
lIDen = lIDss(lIDss~=0);
% Check if we have an equal number of account starts and ends:
if numel(lIDst)~=numel(lIDen);
    error('File seems to be malformed: odd number of account IDs');
end
trans = struct;
tCH = {'TRNTYPE', 'DTPOSTED', 'TRNAMT', 'FITID', 'NAME', 'MEMO'};
for acc = 1:numel(lIDst)
    facc = ffil(lIDst(acc):lIDen(acc))';
    % Find account number. Not nice, being hardcoded and whatnot, but
    % hopefully the file format doesn't change too much.
    fID = cellfun(@(s) ~isempty(strfind(s, 'BANKID')), facc).*(1:length(facc));
    sortcode = facc{sum(fID)};
    sortcode = str2double(sortcode(9:14));
    fID = cellfun(@(s) ~isempty(strfind(s, 'ACCTID')), facc).*(1:length(facc));
    accno = facc{sum(fID)};
    accno = str2double(accno(9:16));
    % Start iterating through, find lines between STMTTRN and /STMTTRN
    tbeg = cellfun(@(s) ~isempty(strfind(s, '<STMTTRN>')), facc).*(1:length(facc));
    tend = cellfun(@(s) ~isempty(strfind(s, '</STMTTRN>')), facc).*(1:length(facc));
    tbeg = tbeg(tbeg~=0); tend = tend(tend~=0);
    if numel(tbeg)~=numel(tend)
        error('File seems to be malformed: odd number of TransIDs');
    end
    for tID = 1:length(tbeg)
        ftran = facc(tbeg(tID)+1:tend(tID)-1)
        
    end
end
fdat = [tbeg; tend];
end