% addShortJournalNames.m

% This function loops through a bibtex data file and for all article type
% entries, it adds a field called shortjournal with the abbreviation of the
% journal name.

function addShortJournalNames(bibfile)
%bibfile = 'E:\Kristjan\Documents\Uni\Dropbox\Writing\Thesis\Biblio\library.bib';
bibdat = getFileText(bibfile);
% Find which lines contain the @article tokens
logm = strfind(bibdat, '@');
logm = cellfun(@isempty, logm, 'UniformOutput', 0);
logm = (~cell2mat(logm)').*(1:length(logm));
logm = logm(logm~=0);
% And now extract all articles
articles = cell(length(logm)-1,1);
for i=1:length(articles)
    temp = cell(logm(i+1)-logm(i)-1,1);
    for k=0:length(temp)
        temp{k+1} = bibdat{logm(i)+k};
    end
    articles{i} = temp;
end
% And now go through each one, and add the shortjournal to them. If the
% function can't find an abbreviation, also make an error list that
% contains the citation jey and the journal name
errors = cell(10,2);
errind = 1;
ftemp = load('JourAbbrData'); jourabbr = ftemp.JourAbbrList;
for i=1:length(articles)
    if mod(i,10)==0; fprintf('Article %i\n', i); end;
    article = articles{i};
    logm = strfind(article, 'journal =');
    logm = cellfun(@isempty, logm, 'UniformOutput', 0);
    logm = (~cell2mat(logm)').*(1:length(logm));
    ind = sum(logm);
    if ind==0
        errors{errind,1} = article{1};
        %errors{errind,2} = article{ind};
        errind = errind + 1;
        continue;
    end
    lin = strsplit(article{ind}, {'{' '}'});
    try
        abbr = getJournalAbbreviation(lin{2}, jourabbr);
        if strcmpi(lin{2}, 'Nature'); abbr = 'Nature'; end;
    catch err
        errors{errind,1} = article{1};
        errors{errind,2} = article{ind};
        errors{errind,3} = err.message;
        errind = errind + 1;
        continue;
    end
    jourline = ['shortjournal = {' abbr '},'];
    article = [article(1:ind); {jourline}; article(ind+1:end)];
    articles{i} = article;
end
%%
% And now write these back to a new file...
[pp, ff, ee] = fileparts(bibfile);

fid = fopen(fullfile(pp, [ff '+short' ee]), 'W');
for art=1:length(articles)
    fprintf('Writing article %i\n', art);
    article = articles{art};
    for tt=1:length(article)
        fprintf(fid, '%s\n', article{tt});
    end
end
fclose(fid);