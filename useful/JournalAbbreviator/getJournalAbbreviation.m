% getJournalAbbreviation.m

% This returns the abbreviation of a journal name

function abbr = getJournalAbbreviation(jname, abbrlist)
if nargin==1
    load('JourAbbrData');
elseif nargin==2
    JourAbbrList = abbrlist;
end
% This doesn't work for any Nature journal...
%logm = strfind(JourAbbrList(:,1), upper(jname));
logm = strcmpi(JourAbbrList(:,1), jname);
%logm = cellfun(@isempty, logm, 'UniformOutput', 0);
%logm = (~cell2mat(logm)');
if sum(logm)==0 % No journal by this name
    % This could mean we had a hyphen or colon mixed up: try again without those
    % So compare only the words in the names
    jname_ = regexprep(jname,'[^\w'']','');
    jourlist_ = regexprep(JourAbbrList(:,1),'[^\w'']','');
    logm_ = strcmpi(jourlist_, jname_);
    if sum(logm_)==0
        switch jname
            case 'JETP Letters'
                abbr = 'JETP Lett';
            case 'Scientific Reports'
                abbr = 'Sci Rep';
            case 'Physical Review X'
                abbr = 'Phys Rev X';
            case 'Philosophical Transactions of the Royal Society A'
                abbr = 'Phil Trans R Soc A';
            otherwise
                error('No journal name found. Check name please!');
        end
    else
        abbr = JourAbbrList(logm_,2);
    end
else
    abbr = JourAbbrList(logm,2);
end
abbr = regexprep(lower(abbr),'(\<[a-z])','${upper($1)}');
if length(abbr)==1;
    abbr = abbr{1};
else
    switch jname
        case 'Nature'
            abbr = 'Nature';
        case 'Laser Physics'
            abbr = 'Laser Phys';
        case 'Plasma Physics Reports'
            abbr = 'Plas Phys Rep';
        case 'Proceedings of the Royal Society A: Mathematical, Physical and Engineering Sciences'
            abbr = 'P Roy Soc A: Math Phy';
        case 'Medical Physics'
            abbr = 'Med Phys';
    end
end
% And some cases where it just fecks up
switch jname
    case 'JETP Letters'
        abbr = 'JETP Lett';
    case 'Scientific Reports'
        abbr = 'Sci Rep';
    case 'Physical Review X'
        abbr = 'Phys Rev X';
    case 'Journal of Experimental and Theoretical Physics'
        abbr = 'JETP';
end
% Also need to ensure IEEE stays IEEE
if strfind(jname, 'IEEE')
    abbr(1:4) = 'IEEE';
    %disp(abbr)
end
end