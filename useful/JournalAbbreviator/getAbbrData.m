% getAbbrData.m

% Downloads all the files needed, from
% http://www.efm.leeds.ac.uk/~mark/ISIabbr

function getAbbrData
% Save all the files first
[savpath,~,~] = fileparts(which('getAbbrData.m'));
if ~exist(fullfile(savpath, 'data'), 'dir')
    mkdir(fullfile(savpath, 'data'));
end
for i=65:90
    fprintf('Downloading data for %c\n', i);
    websave(fullfile(savpath, 'data', ['data' char(i) '.html']),...
        ['https://images.webofknowledge.com/WOK46/help/WOS/' char(i) '_abrvjt.html']);
%['http://www.efm.leeds.ac.uk/~mark/ISIabbr/' char(i) '_abrvjt.html']);
end
end