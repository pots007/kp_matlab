% This file looks through the list and makes a database of the journal
% names
function JourAbbrList = makeAbbrDatabase
% Get data first;
getAbbrData;
[datfol,~,~] = fileparts(which('makeAbbrDatabase.m'));
% Assign the matrix, but it will end up being bigger most likely!
JourAbbrList = cell(5000,2);
it = 1;
for i=65:90
    fprintf('Compiling %c\n', i);
    ll = getFileText(fullfile(datfol, 'data', ['data' char(i) '.html']));
    lll = cell2mat(ll');
    % splits off the header
    [jlist,~] = strsplit(lll,{'<DT>','<DD>'});
    % Now split off the title and abbreviations
    jlistL = cell2mat(jlist(2:end));
    [jlistG,~] = strsplit(jlistL, '</B>');
    jlistG = jlistG(1:end-1); % This splits off last HTML tags
    % We now a cell matrix where each element is a journal name, followed by
    % <B> and a tab and then the abbreviation. time to filter these out and
    % make a structure!
    for k=1:length(jlistG)
        jj = strsplit(jlistG{k}, '<B>');
        JourAbbrList{it,1} = jj{1};
        JourAbbrList{it,2} = strtrim(jj{2});
        it = it + 1;
    end
end
% Adding Nature comms - it doesn't exist there!!!!!!
JourAbbrList(end+1,:) = {'NATURE COMMUNICATIONS' 'NAT COMMS'};

save(fullfile(datfol, 'JourAbbrData.mat'), 'JourAbbrList');

