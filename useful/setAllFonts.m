function setAllFonts(figurehandle, fontsize)
hfig = figurehandle;
fs = fontsize;
if ismac; fs = fs*(96/72); end
AX = findobj(get(hfig, 'Children'), 'Type', 'Axes');
set(findall(hfig, 'Type', 'text'), 'FontSize', fs);
set(findall(hfig, 'Type', 'axes'), 'FontSize', fs);
% for i=1:length(AX)
%     ax = AX(i);
%     set(ax, 'FontSize', fs);
% end
if (~isempty(findobj(get(hfig,'Children'),'Tag','Colorbar')))
    set(findobj(get(hfig,'Children'),'Tag','Colorbar'), 'FontSize', fs);
    try
        set(get(findobj(get(hfig,'Children'),'Tag','Colorbar'), 'YLabel'), ...
            'FontSize', fs);
    catch
        
    end
    
end