function tout = SetSmartUnit(timin)
t = timin;
if (log10(max(t))<-12)
    t = t*1e15;
    tunit = 'f';
elseif (log10(max(t))<-9)
    t = t*1e12;
    tunit = 'p';
elseif (log10(max(t))<-6)
    t = t*1e9;
    tunit = 'n';
elseif (log10(max(t))<-3)
    t = t*1e6;
    tunit = 'u';
elseif (log10(max(t))<0)
    t = t*1e3;
    tunit = 'm';
else
    t = t;
    tunit = '';
end
tout.t = t;
tout.unit = tunit;