function path_ = getGDrive
compname = getComputerName;
if strcmp(compname, 'msteeter.pp.ph.ic.ac.uk')
    path_ = '/Users/kristjanpoder/Google Drive/';
elseif strcmp(compname, 'timothy')
    path_ = 'E:\Kristjan\Google Drive\';
elseif strcmp(compname, 'flalap01')
    path_ = 'C:\Users\kpoder\Google Drive';
elseif strcmp(compname, 'flalap1-ubuntu')
    path_ = '/media/kpoder/DATA/GDrive';
elseif contains(compname, 'kristjans-macbook-pro')
    path_ = '/Users/kpoder/Google Drive';
else 
    path_ = '/Users/kpoder/Google Drive';
end