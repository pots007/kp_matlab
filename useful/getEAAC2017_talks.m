% getEAAC2017_talks.m
% 
% Small scritp to download all talks from EAAC 2017 Indico page

hlink = 'https://agenda.infn.it/conferenceOtherViews.py?confId=12611&view=standardshort';
savfol = fullfile('C:', 'Users', 'kpoder', 'ownCloud', 'Reports', '2017_EAAC_talks_aut');
if ~exist(savfol, 'dir'); mkdir(savfol); end;
websave(fullfile(savfol, 'talks_all.txt'), hlink);

flines = getFileText(fullfile(savfol, 'talks_all.txt'));

%%
textKey = 'https://agenda.infn.it/getFile.py/access?contribId=';
logm = cellfun(@(x) ~isempty(strfind(x, textKey)), flines, 'UniformOutput', 1);
logm_ext = logm'.*(1:length(logm));
logm_ext = logm_ext(logm_ext~=0);
hlinks = flines(logm);
for k=1:length(hlinks)
    fprintf('Working on talk %2i out of %i\n', k, length(hlinks));
    dels = strfind(hlinks{k}, '"');
    linkAddr = hlinks{k}(dels(1)+1:dels(2)-1);
    dels1 = strfind(linkAddr, '=');
    dels2 = strfind(linkAddr, '&');
    % Now figure out the extension
    extLine = flines{logm_ext(k)+1};
    if strfind(extLine, 'powerpoint')
        exten = 'pptx';
    elseif strfind(extLine, 'pdf')
        exten = 'pdf';
    elseif strfind(extLine, 'impress')
        exten = 'odp';
    else
        exten = '';
    end
    contrID = ['contrID_' linkAddr(dels1+1:dels2-1) '.' exten];
    fprintf('\t About to start download of %s ...', contrID);
    try
        if ~exist(fullfile(savfol, contrID), 'file')
            websave(fullfile(savfol, contrID), linkAddr);
            fprintf(' Done.\n');
        else
            fprintf(' File already exists! Skipping.\n');
        end
    catch 
        fprintf('Error getting file for %s\n', contrID)
    end
end
fprintf('Done\n');
