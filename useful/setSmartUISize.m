function [width,height,min_width,min_height]=setSmartUISize(h)
% Compute and st sensible size for UI element(s)
% (c) Dane R. Austin 2013
if numel(h) == 0
    width = []; % all changed from 0 to [] 19/4/2013
    height = [];
    min_width = [];
    min_height = [];
    return;
end
if numel(h) > 1
    width = zeros(size(h));
    height = zeros(size(h));
    min_width = zeros(size(h));
    min_height = zeros(size(h));
    for hi = 1:numel(h)
        [width(hi),height(hi),min_width(hi),min_height(hi)]=setSmartUISize(h(hi));
    end
    return;
end
h = double(h);
if ~(isappdata(h,'SmartWidth')&&isappdata(h,'SmartHeight')&&isappdata(h,'SmartMinWidth')&&isappdata(h,'SmartMinHeight'))
    switch get(h, 'Type')
        case 'uicontrol'
            font_size = getFontSizeInUnits(h, 'pixels');
            switch get(h, 'Style')
                case {'pushbutton', 'text', 'popupmenu', 'checkbox', 'togglebutton', 'radiobutton'}  
                    height = font_size + 15;
                    switch get(h, 'Style')
                        case 'checkbox'
                            extra_width = 25;
                        case 'radiobutton'
                            extra_width = 25;
                        case 'popupmenu'
                            extra_width = 60;
                        case {'pushbutton', 'togglebutton'}
                            extra_width = 15;
                        otherwise
                            extra_width = 10;
                    end                    
                    String = get(h, 'String');
                    if iscell(String)
                        len = max(cellfun(@length, String));
                    else
                        len = length(String);
                    end
                    width = 0.6*font_size*len + extra_width;
                    min_width = width;
                    min_height = height;
                case 'edit'
                    width = -1;
                    height = font_size + 15;
                    min_width = font_size * 3;
                    min_height = font_size;
                case 'frame'
                    width = 1;
                    height = 1;
                    min_width = 1;
                    min_height = 1;
                case 'listbox'
                    width = 50;
                    min_width = 50;
                    height = 50;
                    min_height = 50;
                otherwise
                    error(sprintf('Unknown style %s', get(h, 'Style')));
            end
        case 'axes'
            height = -1;
            width = -1;
            min_width = 100;
            min_height = 100;
        case 'uipanel'
            Children=get(h, 'Children');
            [widths,heights,min_widths,min_heights]=setSmartUISize(Children(end:-1:1));
            if isappdata(h, 'Container')
                c = getappdata(h, 'Container');
                if isa(c, 'ActiveXContainer')
                    width = -1;
                    height = -1;
                    min_width = 40;
                    min_height = 40;
                elseif isempty(get(c, 'Children'))
                    width = 0;
                    height = 0;
                    min_width = 0;
                    min_height = 0;
                else
                    if isa(c, 'uiextras.HBox')
                        if any(widths < 0)
                            width = min(widths);
                        else
                            width = sum(widths)+c.Spacing*(numel(widths)-1);
                        end
                        if any(heights < 0)
                            height = min(heights);
                        else
                            height = max(heights);
                        end
                        min_width = sum(min_widths)+c.Spacing*(numel(widths)-1);
                        min_height = max(min_heights);
                        c.Sizes = widths;
                        c.MinimumSizes = min_widths;
                    elseif isa(c, 'uiextras.VBox')
                        if any(widths < 0)
                            width = min(widths);
                        else
                            width = max(widths);
                        end
                        if any(heights < 0)
                            height = min(heights);
                        else
                            height = sum(heights)+c.Spacing*(numel(heights)-1);
                        end
                        min_width = max(min_widths);
                        min_height = sum(min_heights)+c.Spacing*(numel(heights)-1);
                        c.Sizes = heights;
                        c.MinimumSizes = min_heights;
                    elseif isa(c, 'uiextras.Grid')
                        rows = length(c.RowSizes);
                        columns = length(c.ColumnSizes);
                        widths = reshape(widths, rows, columns);
                        heights = reshape(heights, rows, columns);
                        min_widths = reshape(min_widths, rows, columns);
                        min_heights = reshape(min_heights, rows, columns);
                        min_width = sum(max(min_widths, [], 1));
                        min_height = sum(max(min_heights, [], 2));
                        if any(any(widths < 0))
                            width = min(min(widths));
                        else
                            width = sum(max(widths, [], 1));
                        end
                        if any(any(heights < 0))
                            height = min(min(heights));
                        else
                            height = sum(max(heights, [], 2));
                        end
                        rows = length(c.RowSizes);
                        columns = length(c.ColumnSizes);
                        widths = reshape(widths, rows, columns);
                        heights = reshape(heights, rows, columns);
                        min_widths = reshape(min_widths, rows, columns);
                        min_heights = reshape(min_heights, rows, columns);

                        row_sizes = max(heights, [], 2);
                        some_neg = any(heights < 0, 2);
                        row_sizes(some_neg) = min(heights(some_neg, :), [], 2);
                        c.RowSizes = row_sizes;

                        col_sizes = max(widths, [], 1);
                        some_neg = any(widths < 0, 1);
                        col_sizes(some_neg) = min(widths(:, some_neg), [], 1);
                        c.ColumnSizes = col_sizes;
                    elseif isa(c, 'uiextras.Panel') || isa(c, 'uiextras.BoxPanel')
                        if widths > 0
                            width = widths + 2*c.BorderWidth + 10 + 2*c.Padding;
                        else
                            width = widths;
                        end
                        if heights > 0
                            height = heights + 2*c.BorderWidth + 15 + 2*c.Padding;
                        else
                            height = heights;
                        end
                        min_width = min_widths + 2*c.Padding+10;
                        min_height = min_heights + 2*c.Padding+10;
                        width = max(width);
                        height = max(height);
                        min_width = max(min_width);
                        min_height = max(min_height);
                        if isa(c, 'uiextras.BoxPanel') && c.IsMinimized
                            height = 20;
                            min_height = 20;
                        end
                    elseif isa(c, 'uiextras.TabPanel')
                        if any(widths < 0) 
                            width = min(widths);
                        else
                            width = max(widths);
                        end
                        if any(heights < 0)
                            height = min(heights);
                        else
                            height = max(heights);
                        end
                        min_width = max(min_widths);
                        min_height = max(min_heights);
                    else
                        error(sprintf('Unknown Container class %s', class(c)));
                    end
                end
            else
                if length(get(h, 'Children')) > 1
                    width = -1;
                    height = -1;
                    min_width = 100;
                    min_height = 100;
                else
                    BorderWidth = get(h, 'BorderWidth');
                    if widths > 0
                        width = widths + 2*BorderWidth + 10;
                    else
                        width = widths;
                    end
                    if heights > 0
                        height = heights + 2*BorderWidth + 10;
                    else
                        height = heights;
                    end
                    min_width = min_widths + 2*BorderWidth+10;
                    min_height = min_heights + 2*BorderWidth+10;
                end
            end
        case 'hgjavacomponent'
            switch get(h, 'Tag')
                case 'javax.swing.JSpinner'
                    if isappdata(h, 'SmartWidth')
                        width = getappdata(h, 'SmartWidth');
                        min_width = width;
                    else
                        width = 60;
                        min_width = 60;
                    end
                    height = 20;
                    min_height = 20;
                otherwise
                    error(sprintf('Unknown Tag %s', get(h, 'Tag')));
            end
        otherwise
            error(sprintf('Unknown Type %s', get(h, 'Type')));
    end
end
if ~isappdata(h,'SmartWidth')
   setappdata(h,'SmartWidth',width);
else
    width=getappdata(h,'SmartWidth');
end
if ~isappdata(h,'SmartHeight')
   setappdata(h,'SmartHeight',height);    
else
    height=getappdata(h,'SmartHeight');
end
if ~isappdata(h,'SmartMinWidth')
   setappdata(h,'SmartMinWidth',min_width);
else
    min_width=getappdata(h,'SmartMinWidth');
end
if ~isappdata(h,'SmartMinHeight')
   setappdata(h,'SmartMinHeight',min_height);    
else
   min_height=getappdata(h,'SmartMinHeight');
end


