function name = getComputerName()
% GETCOMPUTERNAME returns the name of the computer (hostname)
% name = getComputerName()
%
% WARN: output string is converted to lower case
%
%
% See also SYSTEM, GETENV, ISPC, ISUNIX
%
% m j m a r i n j (AT) y a h o o (DOT) e s
% (c) MJMJ/2007
% MOD: MJMJ/2013

[ret, name] = system('hostname');   

if ret ~= 0
   if ispc
      name = getenv('COMPUTERNAME');
   else      
      name = getenv('HOSTNAME');      
   end
end
% Fix for this retarded thing on flashfwdcon6 machine...
tempv = strsplit(name, '\n');
if length(tempv)>2
    indss = strfind(name, char(10));
    name = name(indss(1)+1:end);
end
name = strtrim(lower(name));