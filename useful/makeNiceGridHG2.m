% makeNiceGridHG2.m
%
% Small utility to make nice grid lines
%
% makeNiceGridHG2(ax, majorlinewidth, majorlinestyle, minorlinewidth, minorlinestyle)


function [xg,yg] = makeNiceGridHG2(ax, majorlinewidth, majorlinestyle, minorlinewidth, minorlinestyle)
% Ensure everything has been drawn.
drawnow;

if nargin<5
    minorlinestyle = '--';
    if nargin<4
        minorlinewidth = 0.75;
        if nargin<3
            majorlinestyle = '-';
            if nargin<2
                majorlinewidth = 1.25;
            end
        end
    end
end
    
xg = get(ax, 'XGridHandle'); yg = get(ax, 'YGridHandle');
set([xg, yg], 'GridLineStyle', majorlinestyle, 'LineWidth', majorlinewidth,...
    'MinorGridLineStyle', minorlinestyle, 'MinorLineWidth', minorlinewidth);

