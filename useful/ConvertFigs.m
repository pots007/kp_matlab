files = dir('*.fig');
for i=1:length(files)
   tic
   namel = files(i).name(1:length(files(i).name)); 
   existflag = dir(['png\' namel(1:length(namel)-3) 'png']);
   if (~isempty(existflag))
       continue
   else
       h1 = open(namel);
       %set(h1, 'WindowStyle', 'docked');
       export_fig(['png/' namel(1:length(namel)-4)], '-transparent', '-nocrop', '-png', h1);
       close(h1);
   end
   disp([num2str(length(files)-i) ' files left, approx ' num2str(toc*(length(files)-i)) 's']);
end