% Return 1 is number is even

function ret = iseven(num)
ret = ~logical(mod(num,2));