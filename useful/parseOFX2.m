% parseOFX2.m
%
% Actually useful version, using xml2struct to read the files.
% The downloaded files have a bad header, so will automatically rewrite
% that.
% Written in Jan 2017

function facc = parseOFX2(filename)
% First check if file exists and whatnot
if ~exist(filename, 'file'); error('No file exists'); end;
% First, try to parse it as XML to start with:
try
    dataf = xml2struct(filename);
catch err
    fprintf('No dice, man. Inserting xml header and removing some lines\n');
    dataf = getFileText(filename);
    iter = 1;
    for i = 1:length(dataf)
        if isempty(strfind(dataf{i}, '<'))
            dataf{i} = '';
        end
    end
    dataf{1} = '<?xml version=''1.0'' encoding=''utf-8''?>';
    fid = fopen(filename, 'w');
    for i=1:length(dataf)
        fprintf(fid, '%s\n', dataf{i});
    end
    dataf = xml2struct(filename);
end

accs = dataf.OFX.BANKMSGSRSV1.STMTTRNRS;
naccs = length(accs);
fprintf('The file contains data for %i accounts.\n', naccs);
for i=1:naccs
    tacc = accs{i}.STMTRS;
    facc{i}.Currency = tacc.CURDEF.Text;
    facc{i}.AccountType = tacc.BANKACCTFROM.ACCTTYPE.Text;
    facc{i}.SortCode = str2double(tacc.BANKACCTFROM.BANKID.Text);
    facc{i}.AccountNumber = str2double(tacc.BANKACCTFROM.ACCTID.Text);
    trlist = tacc.BANKTRANLIST.STMTTRN;
    ntrans = length(trlist);
    for k = 1:ntrans
        facc{i}.Transactions(k).ID = trlist{k}.FITID.Text;
        facc{i}.Transactions(k).Type = trlist{k}.TRNTYPE.Text;
        facc{i}.Transactions(k).Date = trlist{k}.DTPOSTED.Text;
        facc{i}.Transactions(k).Amount = str2double(trlist{k}.TRNAMT.Text);
        facc{i}.Transactions(k).Name = trlist{k}.NAME.Text;
        if isfield(trlist{k}, 'MEMO')
            facc{i}.Transactions(k).Memo = trlist{k}.MEMO.Text;
        else
            facc{i}.Transactions(k).Memo = '-';
        end
    end
end
end