%returns position array for figure array.
% Update March 2015 - if margin is 4 element vector, the margins will be
% [top right bottom left] 
% Update Dec 2017 - if separation is 2 element vector, the separations will
% be [horizontal vertical]

function out = GetFigureArray(nx, ny, margin, separation, dir)
%sep = 0.05;
flag = [];
if (strcmp(dir, 'across'))
    flag = 1;
end
if (strcmp(dir, 'down'))
    flag = 2;
end
if (isempty(flag))
    flag = 2;
end
if length(margin)==1
    margin = ones(1,4)*margin;
end
if length(separation)==1
    separation = ones(1,2)*separation;
end
%xwidth = (1-2*margin - (nx-1)*separation)/nx;
xwidth = (1-margin(2)-margin(4) - (nx-1)*separation(1))/nx;
%yheight = (1-2*margin - (ny-1)*separation)/ny;
yheight = (1-margin(1)-margin(3) - (ny-1)*separation(2))/ny;
if (flag == 1)
    xloc0 = zeros(1,nx);
    xloc0(1) = margin(4);
    for i=2:nx
        xloc0(i) = xloc0(i-1) + xwidth + separation(1);
    end
    xloc = xloc0;
    if(ny>1)
        for i=2:ny
            xloc = [xloc xloc0];
        end
    end
    
    yloc0 = zeros(1, ny);
    yloc = zeros(1, nx*ny);
    yloc0(1) = 1 - margin(1) - yheight;
    temp = ones(1,nx).*yloc0(1);
    yloc(1:nx) = temp;
    for j=2:ny
        yloc0(j) = yloc0(j-1) - separation(2) - yheight;
        temp = ones(1,nx).*yloc0(j);
        yloc((j-1)*nx+1 : j*nx) = temp;
    end
    width = ones(1, nx*ny).*xwidth;
    height = ones(1, nx*ny).*yheight;
end


if (flag == 2)
    yloc0 = zeros(1,ny);
    yloc0(1) = 1 - margin(1) - yheight;
    for i=2:ny
        yloc0(i) = yloc0(i-1) - yheight - separation(2);
    end
    yloc = yloc0;
    if(nx>1)
        for i=2:nx
            yloc = [yloc yloc0];
        end
    end
    
    xloc0 = zeros(1, nx);
    xloc = zeros(1, nx*ny);
    xloc0(1) =  margin(4);
    temp = ones(1,ny).*xloc0(1);
    xloc(1:ny) = temp;
    for j=2:nx
        xloc0(j) = xloc0(j-1) + separation(1) + xwidth;
        temp = ones(1,ny).*xloc0(j);
        xloc((j-1)*ny+1 : j*ny) = temp;
    end
    width = ones(1, nx*ny).*xwidth;
    height = ones(1, nx*ny).*yheight;
end


out = [xloc; yloc; width; height];

end
