% draw circle

function drawcircle(R, x0, y0)
theta = linspace(0, 2*pi, 250);
hold on;
for i=1:length(theta)
    plot(R*cos(theta(i))-x0, R*sin(theta(i))-y0, 'w');
end
hold off;