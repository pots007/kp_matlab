% Get the highlighted string from listbox and popupmenu

function out = getlistboxstring(hobj)
strings = get(hobj, 'String');
num = get(hobj, 'Value');
out = strings{num};