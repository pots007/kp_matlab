% To use with ESTAR etc
% energies in MeV !!!!

function make_energy_grid(filename, minen, maxen, npnts, logflag)
if nargin==3
    logflag = true;
end
if minen>maxen
    disp('Minimum energy larger than max! error.');
    return;
end
if ~logflag
    ens = linspace(minen, maxen, npnts);
else
    pwrs = linspace(log10(minen), log10(maxen), npnts);
    ens = 10.^(pwrs);
end
fid = fopen(filename, 'w');
for i=1:length(ens)
    fprintf(fid, '%e\n', ens(i));
end
fclose(fid);