function c = cold_to_hot_colormap_nl(m,r1,g1,b1,r2,g2,b2)

% this makes a linear colormap between two rgb colors 
% with white in the middle 
% the colors go linarly
% if you dont provide the colors is fine 
% if you dont provide m is fine (m is the munber of shade)
% if you provide the color and not the mumber if fails 

% the rgb colors are between 0 and 255


if nargin < 1, m = size(get(gcf,'colormap'),1); end

if nargin < 6
  r1=92;
  g1=151;
  b1=230;
  
  r2=230;
  g2=92;
  b2=115;
end


if (mod(m,2) == 0)
  hm=m/2;
else
    hm=(m+1)/2;
end

%light blue to redish

% r1=linspace(92,255,hm);
% g1=linspace(151,255,hm);
% b1=linspace(230,255,hm);
% 
% r2=linspace(255,230,hm);
% g2=linspace(255,92,hm);
% b2=linspace(255,115,hm);


%violet to terracota 

rv1=linspace(r1,254,hm); %avoid 255 to not make it transparent
gv1=linspace(g1,254,hm); % whem we make withe transparent in presentations
bv1=linspace(b1,254,hm);

rv2=linspace(254,r2,hm);
gv2=linspace(254,g2,hm);
bv2=linspace(254,b2,hm);


rv2(1)=[];
gv2(1)=[];
bv2(1)=[];

rv=[rv1,rv2];
gv=[gv1,gv2];
bv=[bv1,bv2];

rv=rv';
gv=gv';
bv=bv';

c= [rv gv bv]/255.0;





