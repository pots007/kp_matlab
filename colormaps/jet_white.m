% jet_white.m

% Interpolates the useful jet_white colormap to N levels.

function outmap = jet_white(N)
load('owncolourmaps');
inax = 1:size(colmap.jet_white,1);
outax = linspace(1, size(colmap.jet_white,1), N);
outmap = interp1(inax', colmap.jet_white, outax');