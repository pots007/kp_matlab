function colmaps = make_colormaps
l = load('owncolourmaps');
colmaps = cell(8,2);
colmaps{1,1} = l.colmap.hot_white; colmaps{1,2} = 'hot_white';
colmaps{2,1} = l.colmap.jet_white; colmaps{2,2} = 'jet_white';
colmaps{3,1} = l.colmap.jet_black; colmaps{3,2} = 'jet_black';
colmaps{4,1} = l.colmap.fireice;   colmaps{4,2} = 'fireice';
colmaps{5,1} = bone(128);          colmaps{5,2} = 'bone';
colmaps{6,1} = 1-bone(128);        colmaps{6,2} = 'invbone';
colmaps{7,1} = gray(128);          colmaps{7,2} = 'gray';
l = load('invfire');
colmaps{8,1} = l.iijcm;            colmaps{8,2} = 'invfire';
end